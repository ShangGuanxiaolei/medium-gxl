package cn.hds.hvmall;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
//Transactional 测试完成后回滚
//@Transactional
public class HdsMediumHvmallApplicationTests {

	protected Logger log = LoggerFactory.getLogger(this.getClass());

	@Test
	public void contextLoads() {
	}

}
