package cn.hds.hvmall.mapper.order;

import cn.hds.hvmall.HdsMediumHvmallApplicationTests;
import cn.hds.hvmall.enums.OrderStatus;
import cn.hds.hvmall.type.PromotionType;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;

/**
 * @author wangxinhua
 * @date 2019-05-12
 * @since 1.0
 */
@Slf4j
public class OrderAnalyseMapperTest extends HdsMediumHvmallApplicationTests {

    private OrderAnalyseMapper orderAnalyseMapper;

    @Autowired
    public void setOrderAnalyseMapper(OrderAnalyseMapper orderAnalyseMapper) {
        this.orderAnalyseMapper = orderAnalyseMapper;
    }

    @Test
    public void testSalesTotalFee() {
        final LocalDateTime now = LocalDateTime.now();
        log.info("{}", orderAnalyseMapper.selectSalesTotalFee(now.minusDays(2000), now, PromotionType.FLASHSALE));
    }

    @Test
    public void testNewUsersTotal() {
        final LocalDateTime now = LocalDateTime.now();
        log.info("{}", orderAnalyseMapper.selectNewUsersTotal(now.minusDays(2000), now));
    }

    @Test
    public void testsListOrder() {
        final LocalDateTime now = LocalDateTime.now();
        orderAnalyseMapper.ListPromotionOrder(now.minusDays(10), now, OrderStatus.PAID)
                .forEach(o -> log.info("order: {}", o));
    }
}