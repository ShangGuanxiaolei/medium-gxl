package cn.hds.hvmall.service.order;

import cn.hds.hvmall.HdsMediumHvmallApplicationTests;
import cn.hds.hvmall.controller.order.OrderAnalyseController;
import cn.hds.hvmall.type.PromotionType;
import com.alibaba.fastjson.JSON;
import io.vavr.collection.Seq;
import lombok.val;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;

import java.text.ParseException;
import java.time.LocalDateTime;

/**
 * @author wangxinhua
 * @date 2019-05-12
 * @since 1.0
 */
@ActiveProfiles("uat")
public class OrderAnalyseServiceTest extends HdsMediumHvmallApplicationTests {

    private OrderAnalyseService orderAnalyseService;

    @Autowired
    public void setOrderAnalyseService(OrderAnalyseService orderAnalyseService) {
        this.orderAnalyseService = orderAnalyseService;
    }

    //    @Test
//    public void allAnalyse() {
//        val now = LocalDateTime.now();
//        Seq<OrderAnalyseVO> analyseVOS = orderAnalyseService.allAnalyse(now.minusDays(10), now);
//        analyseVOS.forEach(s -> log.info("{}", s));
//    }
    @Autowired
    OrderAnalyseController orderAnalyseController;

    @Test
    public void typeAnalyse() {


        System.out.println(
                JSON.toJSONString(
                        orderAnalyseController.type(
                                "2019-05-10", "2019-05-15", "ALL")));

        System.out.println(
                JSON.toJSONString(
                        orderAnalyseController.type(
                                "2019-05-10", "2019-05-15", "FLASHSALE")));

        System.out.println(
                JSON.toJSONString(
                        orderAnalyseController.type(
                                "2019-05-10", "2019-05-15", PromotionType.RESERVE.toString())));
    }
}
