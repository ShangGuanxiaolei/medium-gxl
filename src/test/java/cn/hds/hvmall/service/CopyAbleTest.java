package cn.hds.hvmall.service;

import cn.hds.hvmall.HdsMediumHvmallApplicationTests;
import cn.hds.hvmall.entity.sku.Sku;
import cn.hds.hvmall.mapper.cuxiao.*;
import cn.hds.hvmall.service.list.SkuService;
import cn.hds.hvmall.utils.list.GlobalErrorCode;
import cn.hds.hvmall.utils.list.qiniu.BizException;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.function.Supplier;

/**
 * @author wangxinhua
 * @version 1.0.0 $ 2019/3/28
 */
@ActiveProfiles("uat")
public class CopyAbleTest extends HdsMediumHvmallApplicationTests {

    private final static Logger LOGGER = LoggerFactory.getLogger(CopyAbleTest.class);

    private SkuService skuService;
    @Autowired
    private XquarkProductCopyMapper xquarkProductCopyMapper;
    @Autowired
    XquarkSkuCopyMapper xquarkSkuCopyMapper;
    @Autowired
    public void setSkuService(SkuService skuService) {
        this.skuService = skuService;
    }
    @Autowired
    PromotionBaseInfoCopyMapper   promotionBaseInfoCopyMapper;


    @Test
    public void testCopySkuByCode() {
        skuCody(() -> skuService.loadByCode("264992HW"));
    }

    @Test
    public void testCopySkuById() {
        skuCody(() -> skuService.loadById(2L));
    }
    @Test
    public  void testPid(){
        XquarkProductWithBLOBs xquarkProductWithBLOBs = xquarkProductCopyMapper.selectByPrimaryKey(40l);
//        xquarkProductWithBLOBs.setParent(46l);
        xquarkProductWithBLOBs.setId(null);
        int i  = xquarkProductCopyMapper.insertSelective(xquarkProductWithBLOBs);
//        XquarkProductWithBLOBs sss = xquarkProductCopyMapper.selectByPrimaryKey(40l);
//        xquarkSkuCopyMapper.selectByPrimaryKey(33L);
//        PromotionBaseInfo promotionBaseInfo = promotionBaseInfoCopyMapper.selectByPrimaryKey(1047302647490547938l);




        System.out.println("sssss" );


    }



    /**
     * 复制sku示例
     * @param skuSupplier 提供sku的函数
     */
    private void skuCody(Supplier<Optional<Sku>> skuSupplier) {
        Sku source = skuSupplier.get().orElseThrow(BizException.build(GlobalErrorCode.INVALID_ARGUMENT, "sku不存在"));
        Sku copy = skuService.copy(source, Sku.class, s -> {
            // 修改复制厚的sku属性
//            s.setProductId("111111");
            s.setSkuCode("kafuka");
            s.setPrice(BigDecimal.valueOf(200));
            s.setAmount(200);
        });
        LOGGER.info("{}", copy);
    }
}