package cn.hds.hvmall.service.bargain.impl;

import cn.hds.hvmall.HdsMediumHvmallApplicationTests;
import cn.hds.hvmall.entity.bargain.BargainListVO;
import cn.hds.hvmall.entity.bargain.BargainSku;
import cn.hds.hvmall.entity.bargain.SearchCondation;
import cn.hds.hvmall.service.bargain.PromotionBargainService;
import cn.hds.hvmall.utils.DateUtils;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author wangxinhua
 * @date 2019-04-08
 * @since 1.0
 */
public class PromotionBargainServiceImplTest extends HdsMediumHvmallApplicationTests {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private PromotionBargainService bargainService;

    @Autowired
    public void setBargainService(PromotionBargainService bargainService) {
        this.bargainService = bargainService;
    }

    @Test
    public void searchList() {
        SearchCondation cond = new SearchCondation();
        cond.setPageNum(0);
        cond.setPageSize(8);
        Map<String, Object> ret = bargainService.searchList(cond);
        @SuppressWarnings("unchecked")
        List<BargainListVO> bargainList = (List<BargainListVO>) ret.get("list");
        Long count = (Long) ret.get("count");
        logger.info("bargainList: {} ", bargainList);
        logger.info("count: {}", count);
        Assert.assertTrue(count > 0);
    }

    /**
     * 测试新增砍价
     */
    @Test
    @Transactional
    @Rollback(false)
    public void addBargain() {
        final BargainListVO bargainVO = new BargainListVO();
        final String pdId = "229", skuId = "229";

        final Date now = new Date();
        final BargainSku sku = new BargainSku();
        sku.setPriceStart(BigDecimal.valueOf(10));
        sku.setPriceEnd(BigDecimal.valueOf(20));
        sku.setSkuId(skuId);
        sku.setStock(100);

        bargainVO.setProductId(pdId);
        bargainVO.setProductName("test-bargain-pd");
        bargainVO.setPromotionName("test-bargain-pn");
        bargainVO.setStartTime(now);
        bargainVO.setEndTime(DateUtils.addDay(now, 10));
        bargainVO.setSupportLimit(10);

        bargainVO.setSkus(Collections.singletonList(sku));
        Boolean ret = bargainService.addBargain(bargainVO);
        Assert.assertTrue(ret);
    }

    @Test
    public void selectInfo() {
        String id = "6";
        BargainListVO bargainListVO = bargainService.selectInfo(id);
        logger.info("bargain: {}", bargainListVO);
        logger.info("skus: {}", bargainListVO.getSkus());
        Assert.assertNotNull(bargainListVO);
    }

    @Test
    public void updateBargain() {
        final String id = "6";
        final BargainListVO bargainListVO = bargainService.selectInfo(id);
        Assert.assertEquals("test-bargain-pn", bargainListVO.getPromotionName());
        final String after = "test-bargain-pg-1";
        bargainListVO.setPromotionName(after);
        bargainService.updateBargain(bargainListVO);

        BargainListVO bargainListVO1 = bargainService.selectInfo(id);
        Assert.assertEquals(after, bargainListVO1.getPromotionName());
    }

    @Test
    public void changeBargainState() {
    }
}