package cn.hds.hvmall.pojo.promotion;

/**
 * @author wangxinhua
 * @date 2019-04-23
 * @since 1.0
 */
public interface PromotionSkuItem {

    Long getPromotionId();

    Long getSkuId();

    Long getProductId();

    void setAmount(Long amount);

    void setLimitAmount(Long amount);

    void setSales(Long sales);

}
