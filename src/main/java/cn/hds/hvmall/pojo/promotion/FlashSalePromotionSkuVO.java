package cn.hds.hvmall.pojo.promotion;

import cn.hds.hvmall.utils.Transformer;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * 秒杀多规格
 *
 * @author wangxinhua
 * @date 2019-04-23
 * @since 1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class FlashSalePromotionSkuVO extends Promotion {

    private List<PromotionFlashSale> skus;

    public List<FlashSaleProductWrapper> getProducts() {
        return Optional.ofNullable(skus)
                .map(s -> s.stream().collect(
                        Collectors.groupingBy(Transformer.fromBean(FlashSaleProductWrapper.class)))
                ).orElseGet(HashMap::new)
                .entrySet()
                .stream()
                .map(e -> {
                    FlashSaleProductWrapper w = e.getKey();
                    w.setSkus(e.getValue());
                    return w;
                }).collect(Collectors.toList());
    }


}
