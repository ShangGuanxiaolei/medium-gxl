package cn.hds.hvmall.pojo.promotion;

import cn.hds.hvmall.entity.piece.PromotionPgPrice;
import cn.hds.hvmall.enums.ProductStatus;
import cn.hds.hvmall.utils.list.qiniu.json.JsonResourceUrlSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * @author wangxinhua
 */
public class PromotionFlashSale extends PromotionProduct {

  private static final long serialVersionUID = -8128953293309159481L;

  private String productName;

  private String productImg;

  private ProductStatus productStatus;

  private String orgiSpec;

  private BigDecimal orgiPrice;

  private BigDecimal orgipoint;

  private Integer orgiAmount;

  public String getProductName() {
    return productName;
  }

  public void setProductName(String productName) {
    this.productName = productName;
  }

  @JsonSerialize(using = JsonResourceUrlSerializer.class)
  public String getProductImg() {
    return productImg;
  }

  public void setProductImg(String productImg) {
    this.productImg = productImg;
  }

  public ProductStatus getProductStatus() {
    return productStatus;
  }

  public String getOrgiSpec() {
    return orgiSpec;
  }

  public void setOrgiSpec(String orgiSpec) {
    this.orgiSpec = orgiSpec;
  }

  public Integer getOrgiAmount() {
    return orgiAmount;
  }

  public void setOrgiAmount(Integer orgiAmount) {
    this.orgiAmount = orgiAmount;
  }

  public String getProductStatusStr() {
    if (productStatus == null) {
      return null;
    }
    return productStatus.getNamecn();
  }

  public void setProductStatus(ProductStatus productStatus) {
    this.productStatus = productStatus;
  }

  /**
   * 价格体系
   * TODO 考虑移到PromotionProduct中
   */
  private PromotionPgPrice skuPricing;

  public PromotionPgPrice getSkuPricing() {
    return skuPricing;
  }

  public void setSkuPricing(PromotionPgPrice skuPricing) {
    this.skuPricing = skuPricing;
  }

  public BigDecimal getOrgiPrice() {
    return orgiPrice;
  }

  public void setOrgiPrice(BigDecimal orgiPrice) {
    this.orgiPrice = orgiPrice;
  }

  public BigDecimal getOrgiConversionPrice() {
      if (orgiPrice == null || orgipoint == null) {
        return null;
      }
      return orgiPrice.subtract(orgipoint.divide(BigDecimal.TEN, 2, RoundingMode.HALF_EVEN));
  }

  public BigDecimal getOrgipoint() {
    return orgipoint;
  }

  public void setOrgipoint(BigDecimal orgipoint) {
    this.orgipoint = orgipoint;
  }
}