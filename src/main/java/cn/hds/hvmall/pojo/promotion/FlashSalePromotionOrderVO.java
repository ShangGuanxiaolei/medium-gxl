package cn.hds.hvmall.pojo.promotion;

import cn.hds.hvmall.enums.OrderStatus;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by wangxinhua on 17-12-5. DESC:
 */
public class FlashSalePromotionOrderVO extends FlashSalePromotionUserVO {

  private Long orderId;

  private String orderNo;

  private String userName;

  private String userPhone;

  private BigDecimal orderFee;

  private BigDecimal orderDiscountFee;

  private OrderStatus orderStatus;

  private Date applyTime;

  public Long getOrderId() {
    return orderId;
  }

  public void setOrderId(Long orderId) {
    this.orderId = orderId;
  }

  public String getOrderNo() {
    return orderNo;
  }

  public void setOrderNo(String orderNo) {
    this.orderNo = orderNo;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getUserPhone() {
    return userPhone;
  }

  public void setUserPhone(String userPhone) {
    this.userPhone = userPhone;
  }

  public BigDecimal getOrderFee() {
    return orderFee;
  }

  public void setOrderFee(BigDecimal orderFee) {
    this.orderFee = orderFee;
  }

  public BigDecimal getOrderDiscountFee() {
    return orderDiscountFee;
  }

  public void setOrderDiscountFee(BigDecimal orderDiscountFee) {
    this.orderDiscountFee = orderDiscountFee;
  }

  public OrderStatus getOrderStatus() {
    return orderStatus;
  }

  public void setOrderStatus(OrderStatus orderStatus) {
    this.orderStatus = orderStatus;
  }

  public Date getApplyTime() {
    return applyTime;
  }

  public void setApplyTime(Date applyTime) {
    this.applyTime = applyTime;
  }
}
