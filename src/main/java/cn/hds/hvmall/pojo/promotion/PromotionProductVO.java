package cn.hds.hvmall.pojo.promotion;

import cn.hds.hvmall.pojo.prod.ProductBasicVO;

import java.math.BigDecimal;

/**
 * Created by wangxinhua on 17-11-21. DESC:
 */
public interface PromotionProductVO extends PromotionAble {

  /**
   * 获取活动商品id
   *
   * @return 活动商品id
   */
  Long getId();

  /**
   * 获取关联商品
   *
   * @return {@link ProductBasicVO} 关联商VO
   */
  ProductBasicVO getProduct();

  /**
   * 获取关联活动
   *
   * @return 关联活动对象
   */
  Promotion getPromotion();

  /**
   * 查询关联活动是否已经结束
   *
   * @return close or not
   */
  Boolean getPromotionClosed();

  /**
   * 获取活动库存
   *
   * @return 活动库存
   */
  Long getAmount();

  /**
   * 获取活动价格
   *
   * @return 活动价格
   */
  BigDecimal getPromotionPrice();

}
