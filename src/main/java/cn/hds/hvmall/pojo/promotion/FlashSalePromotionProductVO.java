package cn.hds.hvmall.pojo.promotion;

import cn.hds.hvmall.pojo.prod.ProductBasicVO;
import cn.hds.hvmall.type.PromotionType;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Optional;

/** Created by wangxinhua on 17-11-23. DESC: */
public class FlashSalePromotionProductVO extends PromotionFlashSale implements PromotionProductVO {

  private static final long serialVersionUID = 4788160701515287419L;

  private ProductBasicVO product;

  private Promotion promotion;

  private Long limitAmount;

  private int selfOperated;

  public int getSelfOperated() {
    return selfOperated;
  }

  public void setSelfOperated(int selfOperated) {
    this.selfOperated = selfOperated;
  }

  @Override
  public ProductBasicVO getProduct() {
    return product;
  }

  public void setProduct(ProductBasicVO product) {
    this.product = product;
    if(this.product != null){
      product.setSelfOperated(this.selfOperated);
    }
  }

  @Override
  public Promotion getPromotion() {
    return promotion;
  }

  public void setPromotion(Promotion promotion) {
    this.promotion = promotion;
    if(this.promotion != null){
      promotion.setSelfOperated(this.getSelfOperated());
    }
  }

  @Override
  public Boolean getPromotionClosed() {
    return promotion.getClosed();
  }

  @Override
  public String getTitle() {
    return promotion.getTitle();
  }

  @Override
  public Date getValidFrom() {
    return promotion.getValidFrom();
  }

  @Override
  public Date getValidTo() {
    return promotion.getValidTo();
  }

  @Override
  public boolean isValid(Date date) {
    final Date now = Optional.ofNullable(date).orElseGet(Date::new);
    return Optional.ofNullable(getValidFrom())
        .flatMap(
            from -> Optional.ofNullable(getValidTo()).map(to -> now.after(from) && now.before(to)))
        .orElse(false);
  }

  @Override
  public boolean isValid() {
      return isValid(null);
  }

  @Override
  public PromotionType getPromotionType() {
      return promotion.getPromotionType();
  }

  @Override
  public BigDecimal getPromotionPrice() {
    if (promotion.getPromotionType().equals(PromotionType.FLASHSALE)) {
      BigDecimal price = product.getPrice();
      BigDecimal discount = getDiscount();
      if (price == null || discount == null) {
        return BigDecimal.ZERO;
      }
      return price.multiply(discount).divide(BigDecimal.valueOf(10), 2, BigDecimal.ROUND_HALF_EVEN);
    }
    return BigDecimal.ZERO;
  }

  public Long getLimitAmount() {
    return limitAmount;
  }

  public void setLimitAmount(Long limitAmount) {
    this.limitAmount = limitAmount;
  }
}
