package cn.hds.hvmall.pojo.promotion;

import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 * User: WangXiao
 * Date: 2019/6/21
 * Time: 16:37
 * Description: 促销导入返回需要的信息
 */
public class PromotionMessage {
    //商品id
    private Long pid;

    //商品skuCode
    private String skuCode;

    //商品名称
    private String name;

    //商品规格
    private String spec;

    //商品库存
    private Integer stock;

    //商品价格
    private BigDecimal originalPrice;

    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }

    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpec() {
        return spec;
    }

    public void setSpec(String spec) {
        this.spec = spec;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public BigDecimal getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(BigDecimal originalPrice) {
        this.originalPrice = originalPrice;
    }
}