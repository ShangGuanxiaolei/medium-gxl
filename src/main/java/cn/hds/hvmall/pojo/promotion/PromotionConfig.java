package cn.hds.hvmall.pojo.promotion;

/**
 * Created with IDEA
 * author:Yarm.Yang
 * Date:2019/1/25
 * Time:10:44
 * Des:
 */
public class PromotionConfig {

    private Integer id;
    private String configName;//配置名
    private String configValue;//配置值
    private String configType;//配置类型
    private String createdAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getConfigName() {
        return configName;
    }

    public void setConfigName(String configName) {
        this.configName = configName;
    }

    public String getConfigValue() {
        return configValue;
    }

    public void setConfigValue(String configValue) {
        this.configValue = configValue;
    }

    public String getConfigType() {
        return configType;
    }

    public void setConfigType(String configType) {
        this.configType = configType;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }
}