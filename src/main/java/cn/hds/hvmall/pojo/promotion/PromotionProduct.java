package cn.hds.hvmall.pojo.promotion;

import cn.hds.hvmall.entity.BaseEntityArchivable;
import com.google.common.base.Optional;

import java.math.BigDecimal;
import java.text.DecimalFormat;

/**
 * Created by wangxinhua on 17-11-21. DESC:
 */
public abstract class PromotionProduct extends BaseEntityArchivable implements PromotionSkuItem {

  private static final long serialVersionUID = -4416918346258397820L;

  private final static BigDecimal HUNDRED_SCALA = BigDecimal.valueOf(100);

  /**
   * 活动id
   */
  private Long promotionId;

  /**
   * 活动商品id
   */
  private Long productId;

  private Long skuId;

  /**
   * 活动优惠价
   */
  private BigDecimal discount = BigDecimal.ZERO;

  /**
   * 是否删除
   */
  private Boolean archive;

  /**
   * 活动商品库存
   */
  private Long amount;

  private Long limitAmount;

  /**
   * 活动商品销量
   */
  private Long sales = 0L;

  public Long getPromotionId() {
    return promotionId;
  }

  public void setPromotionId(Long promotionId) {
    this.promotionId = promotionId;
  }

  public Long getProductId() {
    return productId;
  }

  public void setProductId(Long productId) {
    this.productId = productId;
  }

  public Long getSkuId() {
    return skuId;
  }

  public void setSkuId(Long skuId) {
    this.skuId = skuId;
  }

  public BigDecimal getDiscount() {
    return discount;
  }

  public void setDiscount(BigDecimal discount) {
    this.discount = discount;
  }

  @Override
  public Boolean getArchive() {
    return archive;
  }

  @Override
  public void setArchive(Boolean archive) {
    this.archive = archive;
  }

  public Long getAmount() {
    return amount;
  }

  public void setAmount(Long amount) {
    this.amount = amount;
  }

  public Long getLimitAmount() {
    return limitAmount;
  }

  @Override
  public void setLimitAmount(Long limitAmount) {
    this.limitAmount = limitAmount;
  }

  public Long getSales() {
    return sales;
  }

  public void setSales(Long sales) {
    this.sales = sales;
  }

  public BigDecimal getPercent() {
    amount = Optional.fromNullable(amount).or(0L);
    sales = Optional.fromNullable(sales).or(0L);
    BigDecimal bSales = BigDecimal.valueOf(sales);
    BigDecimal bAmount = BigDecimal.valueOf(amount);
    BigDecimal total = bSales.add(bAmount);
    if (total.signum() == 0) {
      return BigDecimal.ZERO;
    }
    return bSales.divide(total, 2, BigDecimal.ROUND_HALF_EVEN);
  }

  public String getPercentStr() {
    return new DecimalFormat("#.##").format(getPercent()
        .multiply(HUNDRED_SCALA)).concat("%");
  }

  public static void main(String[] args) {
    PromotionFlashSale f = new PromotionFlashSale();
    f.setSales(2L);
    f.setAmount(11L);
//    f.setAmount(1);
    System.out.println(f.getPercentStr());
  }

}
