package cn.hds.hvmall.pojo.promotion;

import cn.hds.hvmall.utils.list.qiniu.json.JsonResourceUrlSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

import java.util.List;

/**
 * @author wangxinhua
 * @since 1.0
 */
@Data
public class FlashSaleProductWrapper {

    private Long productId;

    private String productName;

    @JsonSerialize(using = JsonResourceUrlSerializer.class)
    private String productImg;

    private List<PromotionFlashSale> skus;

}
