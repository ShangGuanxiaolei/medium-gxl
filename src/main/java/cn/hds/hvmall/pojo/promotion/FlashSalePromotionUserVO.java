package cn.hds.hvmall.pojo.promotion;

import cn.hds.hvmall.enums.FlashSaleApplyStatus;
import cn.hds.hvmall.utils.list.qiniu.json.JsonResourceUrlSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.Date;

/**
 * Created by wangxinhua on 17-11-23. DESC:
 */
public class FlashSalePromotionUserVO {

  private String id;

  private String promotionId;

  private String productId;

  private String promotionName;

  private String productName;

  private FlashSaleApplyStatus status;

  @JsonSerialize(using = JsonResourceUrlSerializer.class)
  private String productImg;

  private Date validFrom;

  private Date validTo;

  private Integer amount;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getPromotionId() {
    return promotionId;
  }

  public void setPromotionId(String promotionId) {
    this.promotionId = promotionId;
  }

  public String getProductId() {
    return productId;
  }

  public void setProductId(String productId) {
    this.productId = productId;
  }

  public String getPromotionName() {
    return promotionName;
  }

  public void setPromotionName(String promotionName) {
    this.promotionName = promotionName;
  }

  public String getProductName() {
    return productName;
  }

  public void setProductName(String productName) {
    this.productName = productName;
  }

  public String getProductImg() {
    return productImg;
  }

  public void setProductImg(String productImg) {
    this.productImg = productImg;
  }

  public FlashSaleApplyStatus getStatus() {
    return status;
  }

  public void setStatus(FlashSaleApplyStatus status) {
    this.status = status;
  }

  public Date getValidFrom() {
    return validFrom;
  }

  public void setValidFrom(Date validFrom) {
    this.validFrom = validFrom;
  }

  public Date getValidTo() {
    return validTo;
  }

  public void setValidTo(Date validTo) {
    this.validTo = validTo;
  }

  public Integer getAmount() {
    return amount;
  }

  public void setAmount(Integer amount) {
    this.amount = amount;
  }

}
