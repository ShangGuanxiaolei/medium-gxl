package cn.hds.hvmall.pojo.lottery;

import cn.hds.hvmall.entity.lottery.LotteryProbability;
import cn.hds.hvmall.entity.lottery.PromotionLottery;

import java.util.List;

/**
 * 关联抽奖活动跟奖品及概率
 * @author wangxinhua
 * @date 2019-04-23
 * @since 1.0
 */
public class PromotionLotteryVO extends PromotionLottery {

    private List<LotteryProbability> probabilities;

    public List<LotteryProbability> getProbabilities() {
        return probabilities;
    }

    public void setProbabilities(List<LotteryProbability> probabilities) {
        this.probabilities = probabilities;
    }
}
