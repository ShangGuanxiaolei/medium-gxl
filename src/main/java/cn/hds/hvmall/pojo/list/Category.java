package cn.hds.hvmall.pojo.list;

import java.util.List;

/**
 * @Author: zzl
 * @Date: 2018/9/12 14:34
 * @Version
 */
public class Category {

  private String id;

  private String name;

  private String parentId;

  private List<Category> categories;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getParentId() {
    return parentId;
  }

  public void setParentId(String parentId) {
    this.parentId = parentId;
  }

  public List<Category> getCategories() {
    return categories;
  }

  public void setCategories(List<Category> categories) {
    this.categories = categories;
  }
}
