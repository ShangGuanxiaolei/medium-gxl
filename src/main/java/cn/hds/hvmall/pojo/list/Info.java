package cn.hds.hvmall.pojo.list;

import java.util.Objects;

/**
 * @Author: zzl
 * @Date: 2018/9/11 14:55
 * @Version
 * @describe 入参参数封装对象
 */
public class Info {


  /**
   * 商品供应商
   */
  private String spu;
  /**
   * 商品分类
   */
  private String allclass;
  /**
   * 商品品牌
   */
  private String brand;
  /**
   * 商品状态
   */
  private String goodstate;
  /**
   * 商品名称
   */
  private String skuname;
  /**
   * 商品编码
   */
  private String skucode;

  public String getSpu() {
    return spu;
  }

  public void setSpu(String spu) {
    this.spu = spu;
  }

  public String getAllclass() {
    return allclass;
  }

  public void setAllclass(String allclass) {
    this.allclass = allclass;
  }

  public String getBrand() {
    return brand;
  }

  public void setBrand(String brand) {
    this.brand = brand;
  }

  public String getGoodstate() {
    return goodstate;
  }

  public void setGoodstate(String goodstate) {
    this.goodstate = goodstate;
  }

  public String getSkuname() {
    return skuname;
  }

  public void setSkuname(String skuname) {
    this.skuname = skuname;
  }

  public String getSkucode() {
    return skucode;
  }

  public void setSkucode(String skucode) {
    this.skucode = skucode;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Info info = (Info) o;
    return Objects.equals(spu, info.spu) &&
        Objects.equals(allclass, info.allclass) &&
        Objects.equals(brand, info.brand) &&
        Objects.equals(goodstate, info.goodstate) &&
        Objects.equals(skuname, info.skuname) &&
        Objects.equals(skucode, info.skucode);
  }


}
