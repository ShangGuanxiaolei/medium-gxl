package cn.hds.hvmall.pojo.list;

import java.io.Serializable;
import java.util.Date;

public class Term implements Serializable {

  private String id;

  private String name;

  private Date createdAt;

  private Date updatedAt;

  private static final long serialVersionUID = 5499098729123662464L;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  public Date getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(Date updatedAt) {
    this.updatedAt = updatedAt;
  }
}
