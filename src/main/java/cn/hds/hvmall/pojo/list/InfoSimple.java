package cn.hds.hvmall.pojo.list;

import java.util.Objects;

/**
 * @Author: zzl
 * @Date: 2018/9/12 16:41
 * @Version
 */
public class InfoSimple {

  /**
   * 分页页码
   */
  private int pageNum;

	/**
   * 商品供应商编码
   */
  private String supcode;

  /**
   * 商品状态
   */
  private String goodstate;

  /**
   * 商品名称
   *
   */
  private String skuname;

  /**
   * 商品编码
   */
  private String skucode;

  /**
   * 商品分类
   */
  private int category;

  /**
   * 商品品牌
   */
  private int brand;

  /**
   * 商品id
   */
  private Integer productId;

  public String getSupcode() {
    return supcode;
  }

  public void setSupcode(String supcode) {
    this.supcode = supcode;
  }

  public String getGoodstate() {
    return goodstate;
  }

  public void setGoodstate(String goodstate) {
    this.goodstate = goodstate;
  }

  public String getSkuname() {
    return skuname;
  }

  public void setSkuname(String skuname) {
    this.skuname = skuname;
  }

  public String getSkucode() {
    return skucode;
  }

  public void setSkucode(String skucode) {
    this.skucode = skucode;
  }

  public int getPageNum() {
    return pageNum;
  }

  public void setPageNum(int pageNum) {
    this.pageNum = pageNum;
  }

  public int getCategory() {
    return category;
  }

  public void setCategory(int category) {
    this.category = category;
  }

  public int getBrand() {
    return brand;
  }

  public void setBrand(int brand) {
    this.brand = brand;
  }

  public Integer getProductId() {
    return productId;
  }

  public void setProductId(Integer productId) {
    this.productId = productId;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    InfoSimple that = (InfoSimple) o;
    return pageNum == that.pageNum &&
        Objects.equals(supcode, that.supcode) &&
        Objects.equals(goodstate, that.goodstate) &&
        Objects.equals(skuname, that.skuname) &&
        Objects.equals(skucode, that.skucode) &&
        Objects.equals(category, that.category) &&
        Objects.equals(brand, that.brand);
  }

}
