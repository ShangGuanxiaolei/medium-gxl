package cn.hds.hvmall.pojo.list;

public class SearchTerm {

	/**
   * 商品供应商编码
   */
  private String supCode;

  /**
   * 商品状态
   */
  private String status;

  /**
   * 商品名称
   *
   */
  private String name;

  /**
   * 商品编码
   */
  private String skuCode;

  /**
   * 商品分类
   */
  private Integer category;

  /**
   * 商品id
   */
  private Integer productId;

  public String getSupCode() {
    return supCode;
  }

  public void setSupCode(String supCode) {
    this.supCode = supCode;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSkuCode() {
    return skuCode;
  }

  public void setSkuCode(String skuCode) {
    this.skuCode = skuCode;
  }

  public Integer getCategory() {
    return category;
  }

  public void setCategory(Integer category) {
    this.category = category;
  }

  public Integer getProductId() {
    return productId;
  }

  public void setProductId(Integer productId) {
    this.productId = productId;
  }
}
