package cn.hds.hvmall.pojo.list;

public enum CategoryStatus {
  ONSALE, // 上架
  INSTOCK // 下架
}
