package cn.hds.hvmall.pojo.list;

import cn.hds.hvmall.base.BaseExecuteResult;

/**
 * @Author: zzl
 * @Date: 2018/9/14 14:30
 * @Version
 */
public class ExecuteResult<T> extends BaseExecuteResult {

  /**
   * 按照查询条件统计商品总数,用以计算分页页数
   */
  private int count;

  public int getCount() {
    return count;
  }

  public void setCount(int count) {
    this.count = count;
  }
}
