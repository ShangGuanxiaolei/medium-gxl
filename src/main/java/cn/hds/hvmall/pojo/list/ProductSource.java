package cn.hds.hvmall.pojo.list;

/**
 * 商品来源
 *
 * @author tonghu
 */
public enum ProductSource {
  SPIDER,  // 来自爬虫
  NEWSPIDER, // 新爬虫,
  CLIENT,  // 来自客户端，
  WEB,  // h5页面
  SF, // 顺丰优选
  BLP, //菠萝波
  LP,  //蓝漂
  BD,   //棒答
  YP  //甄选壹品
}
