package cn.hds.hvmall.pojo.list;

public class RecommendProductInfo extends ProductInfo{

	private long recommendId;   //推荐ID

	private String recommendName;    //推荐名称

	public Long getRecommendId() {
		return recommendId;
	}

	public void setRecommendId(long recommendId) {
		this.recommendId = recommendId;
	}

	public String getRecommendName() {
		return recommendName;
	}

	public void setRecommendName(String recommendName) {
		this.recommendName = recommendName;
	}

	public String getRecommendCode() {
		return recommendCode;
	}

	public void setRecommendCode(String recommendCode) {
		this.recommendCode = recommendCode;
	}

	private String recommendCode;    //推荐码


}
