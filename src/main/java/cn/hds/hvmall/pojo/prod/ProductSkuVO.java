package cn.hds.hvmall.pojo.prod;

import cn.hds.hvmall.entity.product.Product;
import cn.hds.hvmall.entity.sku.Sku;

import java.util.List;

/**
 * 商品sku组合
 * @author wangxinhua
 * @version 1.0.0 $ 2019/3/28
 */
public class ProductSkuVO extends Product {

    private List<Sku> skuList;

    public List<Sku> getSkuList() {
        return skuList;
    }

    public void setSkuList(List<Sku> skuList) {
        this.skuList = skuList;
    }
}
