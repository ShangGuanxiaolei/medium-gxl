package cn.hds.hvmall.pojo.bargain;

import cn.hds.hvmall.entity.bargain.PromotionBargainRule;
import cn.hds.hvmall.enums.IdentityType;
import lombok.NonNull;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author wangxinhua
 * @since 1.0
 */
public class BargainRuleVO {

    /**
     * 新人发起最小折扣
     */
    @NotNull
    private Integer launchDiscountNewMin;

    /**
     * 新人发起砍价最大折扣
     */
    @NotNull
    private Integer launchDiscountNewMax;

    /**
     * 老用户发起砍价最小折扣
     */
    @NotNull
    private Integer launchDiscountOldMin;

    /**
     * 老用户发起砍价最大折扣
     */
    @NotNull
    private Integer launchDiscountOldMax;

    /**
     * 新人参与最小折扣
     */
    @NotNull
    private Integer supportDiscountNewMin;

    /**
     * 新人参与砍价最大折扣
     */
    @NotNull
    private Integer supportDiscountNewMax;

    /**
     * 老用户参与砍价最小折扣
     */
    @NotNull
    private Integer supportDiscountOldMin;

    /**
     * 老用户参与砍价最大折扣
     */
    @NotNull
    private Integer supportDiscountOldMax;

    /**
     * 新用户助力德分奖励
     */
    @NotNull
    private Integer supportPointNew;

    /**
     * 老用户助力德分奖励
     */
    private Integer supportPointOld;

    /**
     * 助力用户次数
     */
    private Integer maxCutTime;

    public BargainRuleVO() {
    }

    /**
     * 根据数据库配置的规则构造前端VO, 只取有身份的跟没身份的两种配置
     *
     * @param ruleFromDb 数据库配置的数据
     */
    public BargainRuleVO(@NonNull List<PromotionBargainRule> ruleFromDb) {
        final Map<IdentityType, List<PromotionBargainRule>> collect =
                ruleFromDb.stream()
                        .collect(Collectors.groupingBy(r -> r.getCareerLevelType().getIdentityType()));
        // 当前只有有身份跟没身份两种情况
        if (collect.size() != 2) {
            throw new IllegalArgumentException("规则配置不正确");
        }
        // 多条只取一条, 默认配置的时候取相同的值
        final PromotionBargainRule ruleForNew = collect.get(IdentityType.NEW).get(0);
        final PromotionBargainRule ruleForOld = collect.get(IdentityType.VIP).get(0);

        this.launchDiscountNewMin = ruleForNew.getLaunchDiscountMin();
        this.launchDiscountNewMax = ruleForNew.getLaunchDiscountMax();
        this.launchDiscountOldMin = ruleForOld.getLaunchDiscountMin();
        this.launchDiscountOldMax = ruleForOld.getLaunchDiscountMax();
        this.supportDiscountNewMin = ruleForNew.getSupportDiscountMin();
        this.supportDiscountNewMax = ruleForNew.getSupportDiscountMax();
        this.supportDiscountOldMin = ruleForOld.getSupportDiscountMin();
        this.supportDiscountOldMax = ruleForOld.getSupportDiscountMax();
        this.supportPointNew = ruleForNew.getSupportPoint();
        this.supportPointOld = ruleForOld.getSupportPoint();
        Integer maxCutTimeForNew = ruleForNew.getMaxCutTime();
        Integer maxCutTimeForOld = ruleForOld.getMaxCutTime();
        if (!maxCutTimeForNew.equals(maxCutTimeForOld)) {
            throw new IllegalArgumentException("助力次数需要相同");
        }
        this.maxCutTime = maxCutTimeForNew;
    }

    public Integer getLaunchDiscountNewMin() {
        return launchDiscountNewMin;
    }

    public Integer getLaunchDiscountNewMax() {
        return launchDiscountNewMax;
    }

    public Integer getLaunchDiscountOldMin() {
        return launchDiscountOldMin;
    }

    public Integer getLaunchDiscountOldMax() {
        return launchDiscountOldMax;
    }

    public Integer getSupportDiscountNewMin() {
        return supportDiscountNewMin;
    }

    public Integer getSupportDiscountNewMax() {
        return supportDiscountNewMax;
    }

    public Integer getSupportDiscountOldMin() {
        return supportDiscountOldMin;
    }

    public Integer getSupportDiscountOldMax() {
        return supportDiscountOldMax;
    }

    public Integer getSupportPointNew() {
        return supportPointNew;
    }

    public Integer getSupportPointOld() {
        return supportPointOld;
    }

    public void setLaunchDiscountNewMin(Integer launchDiscountNewMin) {
        this.launchDiscountNewMin = launchDiscountNewMin;
    }

    public void setLaunchDiscountNewMax(Integer launchDiscountNewMax) {
        this.launchDiscountNewMax = launchDiscountNewMax;
    }

    public void setLaunchDiscountOldMin(Integer launchDiscountOldMin) {
        this.launchDiscountOldMin = launchDiscountOldMin;
    }

    public void setLaunchDiscountOldMax(Integer launchDiscountOldMax) {
        this.launchDiscountOldMax = launchDiscountOldMax;
    }

    public void setSupportDiscountNewMin(Integer supportDiscountNewMin) {
        this.supportDiscountNewMin = supportDiscountNewMin;
    }

    public void setSupportDiscountNewMax(Integer supportDiscountNewMax) {
        this.supportDiscountNewMax = supportDiscountNewMax;
    }

    public void setSupportDiscountOldMin(Integer supportDiscountOldMin) {
        this.supportDiscountOldMin = supportDiscountOldMin;
    }

    public void setSupportDiscountOldMax(Integer supportDiscountOldMax) {
        this.supportDiscountOldMax = supportDiscountOldMax;
    }

    public void setSupportPointNew(Integer supportPointNew) {
        this.supportPointNew = supportPointNew;
    }

    public void setSupportPointOld(Integer supportPointOld) {
        this.supportPointOld = supportPointOld;
    }

    public Integer getMaxCutTime() {
        return maxCutTime;
    }

    public void setMaxCutTime(Integer maxCutTime) {
        this.maxCutTime = maxCutTime;
    }
}
