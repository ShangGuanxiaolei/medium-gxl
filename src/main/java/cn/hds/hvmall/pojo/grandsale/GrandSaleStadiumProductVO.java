package cn.hds.hvmall.pojo.grandsale;

import cn.hds.hvmall.pojo.prod.ProductSkuVO;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author gxl
 */
public class GrandSaleStadiumProductVO {

  /**
   * 分类
   */
  @NotNull(message = "分类不能为空")
  private String category;

  /**
   * 商品id
   */
  @NotNull(message = "商品不能为空")
  private ProductSkuVO productSkuVO;

  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  public ProductSkuVO getProductSkuVO() {
    return productSkuVO;
  }

  public void setProductSkuVO(ProductSkuVO productSkuVO) {
    this.productSkuVO = productSkuVO;
  }
}