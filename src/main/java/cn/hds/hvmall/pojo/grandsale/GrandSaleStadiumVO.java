package cn.hds.hvmall.pojo.grandsale;

import javax.validation.constraints.NotNull;

import java.util.List;

/**
 * @author gxl
 */
public class GrandSaleStadiumVO {

    /**
     * 关联大促活动id
     */
    @NotNull(message = "大促活动id不能为空")
    private Integer grandSaleId;

    /**
     * 分会场标题
     */
    @NotNull(message = "标题不能为空")
    private String title;

    /**
     * 分会场活动开始时间
     */
    @NotNull(message = "开始时间不能为空")
    private String beginTime;

    /**
     * 分会场活动结束时间
     */
    @NotNull(message = "结束时间不能为空")
    private String endTime;

    /**
     * 分会场按钮图片地址
     */
    @NotNull(message = "按钮图片不能为空")
    private String iconUrl;

    /**
     * 分会场banner图片地址
     */
    @NotNull(message = "banner图片不能为空")
    private List<String> bannerUrl;

    /**
     * 是否包邮，0->不包邮，1->包邮
     */
    @NotNull(message = "请选择是否包邮")
    private Boolean isFreePostage;

    /**
     * 是否计算收益，0->不计算，1->计算
     */
    @NotNull(message = "请选择是否计算收益")
    private Boolean isCountEarning;

    /**
     * 分会场类型
     */
    @NotNull(message = "分会场类型不能为空")
    private String type;

    /**
     * 分会场商品
     */
    @NotNull(message = "分会场商品不能为空")
    private List<GrandSaleStadiumProductVO> productVO;

    /**
     * 分会场分享文案
     */
    private String shareContent;

    /**
     * 分会场入口预约展示时间
     */
    private String appointmentShowTime;

    /**
     * 是否展示首页分会场入口，0->不展示，1->展示
     */
    private Integer isShowEntrance;

    /**
     * 分会场入口图片
     */
    private String entranceImg;

    public String getAppointmentShowTime() {
        return appointmentShowTime;
    }

    public void setAppointmentShowTime(String appointmentShowTime) {
        this.appointmentShowTime = appointmentShowTime;
    }

    public Integer getIsShowEntrance() {
        return isShowEntrance;
    }

    public void setIsShowEntrance(Integer isShowEntrance) {
        this.isShowEntrance = isShowEntrance;
    }

    public String getEntranceImg() {
        return entranceImg;
    }

    public void setEntranceImg(String entranceImg) {
        this.entranceImg = entranceImg;
    }

    public Integer getGrandSaleId() {
        return grandSaleId;
    }

    public void setGrandSaleId(Integer grandSaleId) {
        this.grandSaleId = grandSaleId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(String beginTime) {
        this.beginTime = beginTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public List<String> getBannerUrl() {
        return bannerUrl;
    }

    public void setBannerUrl(List<String> bannerUrl) {
        this.bannerUrl = bannerUrl;
    }

    public Boolean getIsFreePostage() {
        return isFreePostage;
    }

    public void setIsFreePostage(Boolean isFreePostage) {
        this.isFreePostage = isFreePostage;
    }

    public Boolean getIsCountEarning() {
        return isCountEarning;
    }

    public void setIsCountEarning(Boolean isCountEarning) {
        this.isCountEarning = isCountEarning;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<GrandSaleStadiumProductVO> getProductVO() {
        return productVO;
    }

    public void setProductVO(List<GrandSaleStadiumProductVO> productVO) {
        this.productVO = productVO;
    }

    public String getShareContent() {
        return shareContent;
    }

    public void setShareContent(String shareContent) {
        this.shareContent = shareContent;
    }
}