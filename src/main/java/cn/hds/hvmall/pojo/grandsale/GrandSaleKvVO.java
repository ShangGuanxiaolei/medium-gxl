package cn.hds.hvmall.pojo.grandsale;

import javax.validation.constraints.NotNull;

/**
 * @author gxl
 */
public class GrandSaleKvVO {

    /**
     * 大型促销活动表id
     */
    @NotNull(message = "促销活动id不能为空")
    private Integer grandSaleId;

    /**
     * 主KV标题
     */
    @NotNull(message = "标题不能为空")
    private String title;

    /**
     * 主KV显示开始时间
     */
    @NotNull(message = "开始时间不能为空")
    private String beginTime;

    /**
     * 主KV显示结束时间
     */
    @NotNull(message = "结束时间不能为空")
    private String endTime;

    /**
     * 主KV显示图片
     */
    @NotNull(message = "图片地址不能为空")
    private String image;

    /**
     * 主KV点击跳转目标
     */
    private String target;

    /**
     * 主KV点击跳转目标类型
     */
    private String targetType;

    public Integer getGrandSaleId() {
        return grandSaleId;
    }

    public void setGrandSaleId(Integer grandSaleId) {
        this.grandSaleId = grandSaleId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(String beginTime) {
        this.beginTime = beginTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getTargetType() {
        return targetType;
    }

    public void setTargetType(String targetType) {
        this.targetType = targetType;
    }
}