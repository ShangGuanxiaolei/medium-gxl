package cn.hds.hvmall.pojo.grandsale;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @ClassName: GrandSaleShareVO
 * @Description: TODO
 * @Author: Kwonghom
 * @CreateDate: 2019/6/4 16:46
 * @Version: 1.0
 **/
public class GrandSaleMainShareVO {

    private Long id;

    @NotNull(message = "主会场分享文案不能为空")
    private String mainShareContent;

    @NotNull(message = "主会场开始时间不能为空")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date mainStartAt;

    @NotNull(message = "主会场结束时间不能为空")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date mainEndAt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMainShareContent() {
        return mainShareContent;
    }

    public void setMainShareContent(String mainShareContent) {
        this.mainShareContent = mainShareContent;
    }

    public Date getMainStartAt() {
        return mainStartAt;
    }

    public void setMainStartAt(Date mainStartAt) {
        this.mainStartAt = mainStartAt;
    }

    public Date getMainEndAt() {
        return mainEndAt;
    }

    public void setMainEndAt(Date mainEndAt) {
        this.mainEndAt = mainEndAt;
    }
}
