package cn.hds.hvmall.pojo.grandsale;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author luqing
 * @since 2019-05-08
 */
@Getter
@Setter
@ToString
public class ProductIdSkuCodeDTO {
  private String productId;
  private String skuCode;
}
