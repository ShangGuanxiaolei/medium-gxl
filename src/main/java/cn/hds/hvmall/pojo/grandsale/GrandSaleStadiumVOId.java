package cn.hds.hvmall.pojo.grandsale;

/**
 * @author gxl
 */
public class GrandSaleStadiumVOId extends GrandSaleStadiumVO {

  /**
   * 分会场id
   */
  private Integer stadiumId;

  public Integer getStadiumId() {
    return stadiumId;
  }

  public void setStadiumId(Integer stadiumId) {
    this.stadiumId = stadiumId;
  }
}