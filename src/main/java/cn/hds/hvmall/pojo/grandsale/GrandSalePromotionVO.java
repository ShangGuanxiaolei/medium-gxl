package cn.hds.hvmall.pojo.grandsale;

import javax.validation.constraints.NotNull;

/**
 * @author gxl
 */
public class GrandSalePromotionVO {

    /**
     * 促销活动名称
     */
    @NotNull(message = "活动名称不能为空")
    private String name;

    /**
     * 活动开始时间
     */
    @NotNull(message = "活动开始时间不能为空")
    private String beginTime;

    /**
     * 活动结束时间
     */
    @NotNull(message = "活动结束时间不能为空")
    private String endTime;

    /**
     * 首页图片地址
     */
    @NotNull(message = "首页图片地址不能为空")
    private String homePageBanner;

    /**
     * 详情页图片地址
     */
    @NotNull(message = "详情页图片地址不能为空")
    private String detailsPageBanner;

    /**
     * 支付成功页图片地址
     */
    @NotNull(message = "支付成功页图片地址不能为空")
    private String successPageBanner;

    /**
     * 首页活动飘窗Icon图片地址
     */
    @NotNull(message = "首页活动飘窗Icon不能为空")
    private String homePageGiftIconBanner;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(String beginTime) {
        this.beginTime = beginTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getHomePageBanner() {
        return homePageBanner;
    }

    public void setHomePageBanner(String homePageBanner) {
        this.homePageBanner = homePageBanner;
    }

    public String getDetailsPageBanner() {
        return detailsPageBanner;
    }

    public void setDetailsPageBanner(String detailsPageBanner) {
        this.detailsPageBanner = detailsPageBanner;
    }

    public String getSuccessPageBanner() {
        return successPageBanner;
    }

    public void setSuccessPageBanner(String successPageBanner) {
        this.successPageBanner = successPageBanner;
    }

    public String getHomePageGiftIconBanner() {
        return homePageGiftIconBanner;
    }

    public void setHomePageGiftIconBanner(String homePageGiftIconBanner) {
        this.homePageGiftIconBanner = homePageGiftIconBanner;
    }
}