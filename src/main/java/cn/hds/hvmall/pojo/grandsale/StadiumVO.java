package cn.hds.hvmall.pojo.grandsale;

import lombok.*;

import java.math.BigDecimal;

/**
 * @author luqing
 * @since 2019-05-07
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
public class StadiumVO {
    private String termName;
    private String skuCode;
    private Integer buyLimit;
    private BigDecimal price;
    private BigDecimal convertPrice;
    private BigDecimal deductionDPoint;
    private BigDecimal point;
    private BigDecimal promoAmt;
    private BigDecimal serverAmt;
    private BigDecimal netWorth;
    private Integer amount;
}
