package cn.hds.hvmall.pojo.grandsale;

import cn.hds.hvmall.type.PromotionType;

import javax.validation.constraints.NotNull;

/**
 * @author gxl
 */
public class GrandSaleModuleVO {

    /**
     * 大型促销活动表id
     */
    @NotNull(message = "促销活动id不能为空")
    private Integer grandSaleId;

    /**
     * 关联的活动id
     */
    @NotNull(message = "关联活动id不能为空")
    private String promotionId;

    /**
     * 关联的活动类型
     */
    @NotNull(message = "关联活动类型不能为空")
    private PromotionType promotionType;

    public Integer getGrandSaleId() {
        return grandSaleId;
    }

    public void setGrandSaleId(Integer grandSaleId) {
        this.grandSaleId = grandSaleId;
    }

    public String getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(String promotionId) {
        this.promotionId = promotionId;
    }

    public PromotionType getPromotionType() {
        return promotionType;
    }

    public void setPromotionType(PromotionType promotionType) {
        this.promotionType = promotionType;
    }
}