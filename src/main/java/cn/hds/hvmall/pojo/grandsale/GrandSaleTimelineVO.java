package cn.hds.hvmall.pojo.grandsale;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @author gxl
 */
public class GrandSaleTimelineVO {

    /**
     * 大型促销活动表id
     */
    @NotNull(message = "促销活动id不能为空")
    private Integer grandSaleId;

    /**
     * 活动节点名称
     */
    @NotNull(message = "活动节点名称不能为空")
    private String name;

    /**
     * 活动时间节点
     */
    @NotNull(message = "活动时间节点不能为空")
    private String timeNode;

    /**
     * 活动持续时间（天为单位）
     */
    @NotNull(message = "节点时间不能为空")
    private String duration;

    /**
     * 是否在首页展示时间轴，0->不展示，1->展示
     */
    private Boolean isShowTimeline;

    /**
     * 时间轴背景图片
     */
    private String timelineImg;

    /**
     * 活动开始时间
     */
    private String beginTime;

    /**
     * 活动结束时间
     */
    private String endTime;

    public String getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(String beginTime) {
        this.beginTime = beginTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public Boolean getShowTimeline() {
        return isShowTimeline;
    }

    public void setShowTimeline(Boolean showTimeline) {
        isShowTimeline = showTimeline;
    }

    public String getTimelineImg() {
        return timelineImg;
    }

    public void setTimelineImg(String timelineImg) {
        this.timelineImg = timelineImg;
    }

    public Integer getGrandSaleId() {
        return grandSaleId;
    }

    public void setGrandSaleId(Integer grandSaleId) {
        this.grandSaleId = grandSaleId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTimeNode() {
        return timeNode;
    }

    public void setTimeNode(String timeNode) {
        this.timeNode = timeNode;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }
}