package cn.hds.hvmall.pojo.grandsale;

/**
 * Created with IntelliJ IDEA.
 * User: WangXiao
 * Date: 2019/5/30
 * Time: 11:39
 * Description:
 */
public class SortArr {
    private String promotionId;

    private Integer sortIng;

    public String getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(String promotionId) {
        this.promotionId = promotionId;
    }

    public Integer getSortIng() {
        return sortIng;
    }

    public void setSortIng(Integer sortIng) {
        this.sortIng = sortIng;
    }
}