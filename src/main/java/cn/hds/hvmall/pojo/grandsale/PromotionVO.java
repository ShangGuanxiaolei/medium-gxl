package cn.hds.hvmall.pojo.grandsale;

import java.util.Date;

/**
 * @author gxl
 */
public class PromotionVO {

  /**
   * 活动名称
   */
  private String name;

  /**
   * 活动id
   */
  private String promotionId;

  /**
   * 开始时间
   */
  private Date beginTime;

  /**
   * 结束时间
   */
  private Date endTime;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getPromotionId() {
    return promotionId;
  }

  public void setPromotionId(String promotionId) {
    this.promotionId = promotionId;
  }

  public Date getBeginTime() {
    return beginTime;
  }

  public void setBeginTime(Date beginTime) {
    this.beginTime = beginTime;
  }

  public Date getEndTime() {
    return endTime;
  }

  public void setEndTime(Date endTime) {
    this.endTime = endTime;
  }
}
