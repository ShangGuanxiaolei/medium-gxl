package cn.hds.hvmall.pojo.grandsale;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author gxl
 */
public class GrandSaleStadiumVOMap {

    private Integer id;

    /**
     * 关联大促活动id
     */
    private Integer grandSaleId;

    /**
     * 分会场标题
     */
    private String title;

    /**
     * 分会场活动开始时间
     */
    private Date beginTime;

    /**
     * 分会场活动结束时间
     */
    private Date endTime;

    /**
     * 分会场前端排序
     */
    private Integer sort;

    /**
     * 分会场按钮图片地址
     */
    private String iconUrl;

    /**
     * 分会场banner图片地址
     */
    private String[] bannerUrl;

    /**
     * 是否包邮，0->不包邮，1->包邮
     */
    private Boolean isFreePostage;

    /**
     * 是否计算收益，0->不计算，1->计算
     */
    private Boolean isCountEarning;

    /**
     * 分会场类型
     */
    private String type;

    /**
     * 分会场分享文案
     */
    private String shareContent;

    /**
     * 商品map
     */
    private List<Map<String, Object>> productMap;

    /**
     * 分会场入口预约展示时间
     */
    private Date appointmentShowTime;

    /**
     * 是否展示首页分会场入口，0->不展示，1->展示
     */
    private Integer isShowEntrance;

    /**
     * 分会场入口图片
     */
    private String entranceImg;

    public Date getAppointmentShowTime() {
        return appointmentShowTime;
    }

    public void setAppointmentShowTime(Date appointmentShowTime) {
        this.appointmentShowTime = appointmentShowTime;
    }

    public Integer getIsShowEntrance() {
        return isShowEntrance;
    }

    public void setIsShowEntrance(Integer isShowEntrance) {
        this.isShowEntrance = isShowEntrance;
    }

    public String getEntranceImg() {
        return entranceImg;
    }

    public void setEntranceImg(String entranceImg) {
        this.entranceImg = entranceImg;
    }

    public List<Map<String, Object>> getProductMap() {
        return productMap;
    }

    public void setProductMap(List<Map<String, Object>> productMap) {
        this.productMap = productMap;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getGrandSaleId() {
        return grandSaleId;
    }

    public void setGrandSaleId(Integer grandSaleId) {
        this.grandSaleId = grandSaleId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(Date beginTime) {
        this.beginTime = beginTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public String[] getBannerUrl() {
        return bannerUrl;
    }

    public void setBannerUrl(String[] bannerUrl) {
        this.bannerUrl = bannerUrl;
    }

    public Boolean getIsFreePostage() {
        return isFreePostage;
    }

    public void setIsFreePostage(Boolean isFreePostage) {
        this.isFreePostage = isFreePostage;
    }

    public Boolean getIsCountEarning() {
        return isCountEarning;
    }

    public void setIsCountEarning(Boolean isCountEarning) {
        this.isCountEarning = isCountEarning;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getShareContent() {
        return shareContent;
    }

    public void setShareContent(String shareContent) {
        this.shareContent = shareContent;
    }
}