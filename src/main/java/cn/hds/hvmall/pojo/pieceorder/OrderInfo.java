package cn.hds.hvmall.pojo.pieceorder;

import java.util.Date;

/**
 * 封装入参json
 *
 * @Author: zzl
 * @Date: 2018/9/17 17:52
 * @Version
 */
public class OrderInfo {

  /**
   * 订单id
   */
  private String orderNo;
  /**
   * 订单创建时间
   */
  private Date createdAt;
  /**
   * 订单状态
   */
  private String status;
  /**
   * 订单类型,order表中的
   */
  private String orderType;
  /**
   * 买家姓名
   * (姓名/手机号):一个输入框里可以输入姓名也可以输入手机号
   * 前台判断如果是手机号,就赋值给buyerphone,然后buyername给null,反之亦然
   */
  private String buyerName;
  /**
   * 买家手机号
   */
  private String buyerPhone;
  /**
   * 供应商
   */
  private String supplier;
  /**
   * 当前页码
   */
  private int pageNum;

  public int getPageNum() {
    return pageNum;
  }

  public void setPageNum(int pageNum) {
    this.pageNum = pageNum;
  }

  public String getOrderNo() {
    return orderNo;
  }

  public void setOrderNo(String orderNo) {
    this.orderNo = orderNo;
  }

  public Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getOrderType() {
    return orderType;
  }

  public void setOrderType(String orderType) {
    this.orderType = orderType;
  }

  public String getBuyerName() {
    return buyerName;
  }

  public void setBuyerName(String buyerName) {
    this.buyerName = buyerName;
  }

  public String getBuyerPhone() {
    return buyerPhone;
  }

  public void setBuyerPhone(String buyerPhone) {
    this.buyerPhone = buyerPhone;
  }

  public String getSupplier() {
    return supplier;
  }

  public void setSupplier(String supplier) {
    this.supplier = supplier;
  }


}
