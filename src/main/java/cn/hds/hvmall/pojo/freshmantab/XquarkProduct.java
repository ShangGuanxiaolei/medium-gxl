package cn.hds.hvmall.pojo.freshmantab;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * xquark_product
 * @author 
 */
public class XquarkProduct implements Serializable {
    /**
     * 主键ID
     */
    private Long id;

    /**
     * 商品编码
     */
    private String code;

    private String encode;

    private String u8Encode;

    /**
     * 产品型号
     */
    private String model;

    /**
     * 商品类型
     */
    private String type;

    /**
     * 滤芯等级
     */
    private Integer level;

    private Integer priority;

    /**
     * 滤芯额外规格
     */
    private String filterSpec;

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 店铺ID
     */
    private Long shopId;

    /**
     * 商品名称
     */
    private String name;

    /**
     * 商品图片
     */
    private String img;

    /**
     * 主图宽度
     */
    private Integer imgWidth;

    /**
     * 主图高度
     */
    private Integer imgHeight;

    /**
     * 图片描述
     */
    private String descImg;

    /**
     * 商品状态:DRAFT=草稿,FORSALE=预售,INSTOCK=下架,ONSALE=上架
     */
    private String status;

    /**
     * 原价
     */
    private BigDecimal marketPrice;

    /**
     * 折扣价
     */
    private BigDecimal price;

    private BigDecimal originalPrice;

    /**
     * 库存
     */
    private Long amount;

    /**
     * 销售量
     */
    private Long sales;

    /**
     * 逻辑删除：0=正常
     */
    private Boolean archive;

    /**
     * 是否推荐：0=不推荐，1=推荐
     */
    private Byte recommend;

    /**
     * 推荐时间
     */
    private Date recommendAt;

    /**
     * 折扣
     */
    private BigDecimal discount;

    /**
     * 是否单商品分佣
     */
    private Boolean isCommission;

    /**
     * 单商品分佣百分比
     */
    private BigDecimal commissionRate;

    /**
     * 预售时间
     */
    private Date forsaleAt;

    /**
     * 上架时间
     */
    private Date onsaleAt;

    /**
     * 下架时间
     */
    private Date instockAt;

    /**
     * 创建时间
     */
    private Date createdAt;

    /**
     * 更新时间
     */
    private Date updatedAt;

    /**
     * 假销量
     */
    private Long fakeSales;

    /**
     * 是否延迟收货：0=否，1=是
     */
    private Byte isdelay;

    /**
     * 延迟收货天数
     */
    private Integer delaydays;

    /**
     * 是否锁定：0=不锁定，1=锁定
     */
    private Boolean updateLock;

    /**
     * 同步标识：1=同步到第三方平台的商品已审核通过
     */
    private String synchronousflag;

    /**
     * 第三方商品ID
     */
    private Long thirdItemId;

    private Long partnerProductId;

    /**
     * 来源 spider, api, import
     */
    private String source;

    /**
     * 是否正在活动优惠中(0/1) 当前由活动开始和结束时 改写
     */
    private Byte inActivity;

    /**
     * 商品是否跨境。1：是，0：否
     */
    private Boolean isCrossBorder;

    /**
     * 是否启用富文本
     */
    private Boolean enableDesc;

    private Boolean isDistribution;

    private Long sourceProductId;

    private Long sourceShopId;

    private Long feature0;

    private Long feature1;

    private BigDecimal oneyuanPostage;

    private Boolean oneyuanPurchase;

    private BigDecimal specialrate;

    /**
     * 是否团购商品
     */
    private Boolean isGroupon;

    /**
     * 退货地址
     */
    private String refundAddress;

    /**
     * 退货联系方式
     */
    private String refundTel;

    /**
     * 退货收货人
     */
    private String refundName;

    /**
     * 运费设置
     */
    private String logisticsType;

    /**
     * 统一运费价格
     */
    private BigDecimal uniformValue;

    /**
     * 运费模版id
     */
    private Long templateValue;

    /**
     * 是否为赠品
     */
    private Byte gift;

    /**
     * 商品重量
     */
    private Integer weight;

    /**
     * 商品拥有的规则属性（如颜色，大小等）
     */
    private String attributes;

    /**
     * 积分兑换比例
     */
    private Boolean yundouScale;

    /**
     * 最小积分抵扣价格
     */
    private BigDecimal minYundouPrice;

    /**
     * 商品可见状态
     */
    private String availableStatus;

    /**
     * 商品的审核状态
     */
    private String reviewStatus;

    /**
     * 商品的高度
     */
    private Double height;

    /**
     * 商品宽度
     */
    private Double width;

    /**
     * 商品长度
     */
    private Double length;

    /**
     * 商品类型
     */
    private String kind;

    /**
     * 商品购买返利积分
     */
    private Long point;

    private BigDecimal deductionDPoint;

    /**
     * 是否支持退款
     */
    private Boolean supportRefund;

    /**
     * 每箱多少个
     */
    private Integer numInPackage;

    /**
     * 商品条形码
     */
    private String barCode;

    /**
     * 商品所属仓库
     */
    private Long wareHouseId;

    /**
     * 装箱类型
     */
    private Long packageId;

    /**
     * 外部商家编码
     */
    private String outerId;

    /**
     * 商品数量
     */
    private Integer num;

    /**
     * 商品所在仓库编号
     */
    private String whseCode;

    /**
     * 平台规格ID
     */
    private String skuId;

    /**
     * 规格外部商家编码
     */
    private String skuOuterId;

    /**
     * 规格价格
     */
    private Long skuPrice;

    /**
     * 规格数量
     */
    private Integer skuQuantity;

    /**
     * 规格名称
     */
    private String skuName;

    /**
     * 规格属性
     */
    private String skuProperty;

    /**
     * 规格图片URL
     */
    private String skuPictureUrl;

    /**
     * 净值
     */
    private BigDecimal netWorth;

    /**
     * 供应商
     */
    private Long supplierId;

    private Integer sfairline;

    private Integer sfshipping;

    private Integer merchantNumber;

    /**
     * 推广费
     */
    private BigDecimal promoAmt;

    /**
     * 服务费
     */
    private BigDecimal serverAmt;

    /**
     * 区域配送省份,默认"全国配送"
     */
    private String deliveryRegion;

    /**
     * 是否 1:自营，0:非自营
     */
    private Integer selfOperated;

    /**
     * 产品来源id
     */
    private Long sourceId;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getEncode() {
        return encode;
    }

    public void setEncode(String encode) {
        this.encode = encode;
    }

    public String getU8Encode() {
        return u8Encode;
    }

    public void setU8Encode(String u8Encode) {
        this.u8Encode = u8Encode;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public String getFilterSpec() {
        return filterSpec;
    }

    public void setFilterSpec(String filterSpec) {
        this.filterSpec = filterSpec;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public Integer getImgWidth() {
        return imgWidth;
    }

    public void setImgWidth(Integer imgWidth) {
        this.imgWidth = imgWidth;
    }

    public Integer getImgHeight() {
        return imgHeight;
    }

    public void setImgHeight(Integer imgHeight) {
        this.imgHeight = imgHeight;
    }

    public String getDescImg() {
        return descImg;
    }

    public void setDescImg(String descImg) {
        this.descImg = descImg;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public BigDecimal getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(BigDecimal marketPrice) {
        this.marketPrice = marketPrice;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(BigDecimal originalPrice) {
        this.originalPrice = originalPrice;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public Long getSales() {
        return sales;
    }

    public void setSales(Long sales) {
        this.sales = sales;
    }

    public Boolean getArchive() {
        return archive;
    }

    public void setArchive(Boolean archive) {
        this.archive = archive;
    }

    public Byte getRecommend() {
        return recommend;
    }

    public void setRecommend(Byte recommend) {
        this.recommend = recommend;
    }

    public Date getRecommendAt() {
        return recommendAt;
    }

    public void setRecommendAt(Date recommendAt) {
        this.recommendAt = recommendAt;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public Boolean getIsCommission() {
        return isCommission;
    }

    public void setIsCommission(Boolean isCommission) {
        this.isCommission = isCommission;
    }

    public BigDecimal getCommissionRate() {
        return commissionRate;
    }

    public void setCommissionRate(BigDecimal commissionRate) {
        this.commissionRate = commissionRate;
    }

    public Date getForsaleAt() {
        return forsaleAt;
    }

    public void setForsaleAt(Date forsaleAt) {
        this.forsaleAt = forsaleAt;
    }

    public Date getOnsaleAt() {
        return onsaleAt;
    }

    public void setOnsaleAt(Date onsaleAt) {
        this.onsaleAt = onsaleAt;
    }

    public Date getInstockAt() {
        return instockAt;
    }

    public void setInstockAt(Date instockAt) {
        this.instockAt = instockAt;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Long getFakeSales() {
        return fakeSales;
    }

    public void setFakeSales(Long fakeSales) {
        this.fakeSales = fakeSales;
    }

    public Byte getIsdelay() {
        return isdelay;
    }

    public void setIsdelay(Byte isdelay) {
        this.isdelay = isdelay;
    }

    public Integer getDelaydays() {
        return delaydays;
    }

    public void setDelaydays(Integer delaydays) {
        this.delaydays = delaydays;
    }

    public Boolean getUpdateLock() {
        return updateLock;
    }

    public void setUpdateLock(Boolean updateLock) {
        this.updateLock = updateLock;
    }

    public String getSynchronousflag() {
        return synchronousflag;
    }

    public void setSynchronousflag(String synchronousflag) {
        this.synchronousflag = synchronousflag;
    }

    public Long getThirdItemId() {
        return thirdItemId;
    }

    public void setThirdItemId(Long thirdItemId) {
        this.thirdItemId = thirdItemId;
    }

    public Long getPartnerProductId() {
        return partnerProductId;
    }

    public void setPartnerProductId(Long partnerProductId) {
        this.partnerProductId = partnerProductId;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public Byte getInActivity() {
        return inActivity;
    }

    public void setInActivity(Byte inActivity) {
        this.inActivity = inActivity;
    }

    public Boolean getIsCrossBorder() {
        return isCrossBorder;
    }

    public void setIsCrossBorder(Boolean isCrossBorder) {
        this.isCrossBorder = isCrossBorder;
    }

    public Boolean getEnableDesc() {
        return enableDesc;
    }

    public void setEnableDesc(Boolean enableDesc) {
        this.enableDesc = enableDesc;
    }

    public Boolean getIsDistribution() {
        return isDistribution;
    }

    public void setIsDistribution(Boolean isDistribution) {
        this.isDistribution = isDistribution;
    }

    public Long getSourceProductId() {
        return sourceProductId;
    }

    public void setSourceProductId(Long sourceProductId) {
        this.sourceProductId = sourceProductId;
    }

    public Long getSourceShopId() {
        return sourceShopId;
    }

    public void setSourceShopId(Long sourceShopId) {
        this.sourceShopId = sourceShopId;
    }

    public Long getFeature0() {
        return feature0;
    }

    public void setFeature0(Long feature0) {
        this.feature0 = feature0;
    }

    public Long getFeature1() {
        return feature1;
    }

    public void setFeature1(Long feature1) {
        this.feature1 = feature1;
    }

    public BigDecimal getOneyuanPostage() {
        return oneyuanPostage;
    }

    public void setOneyuanPostage(BigDecimal oneyuanPostage) {
        this.oneyuanPostage = oneyuanPostage;
    }

    public Boolean getOneyuanPurchase() {
        return oneyuanPurchase;
    }

    public void setOneyuanPurchase(Boolean oneyuanPurchase) {
        this.oneyuanPurchase = oneyuanPurchase;
    }

    public BigDecimal getSpecialrate() {
        return specialrate;
    }

    public void setSpecialrate(BigDecimal specialrate) {
        this.specialrate = specialrate;
    }

    public Boolean getIsGroupon() {
        return isGroupon;
    }

    public void setIsGroupon(Boolean isGroupon) {
        this.isGroupon = isGroupon;
    }

    public String getRefundAddress() {
        return refundAddress;
    }

    public void setRefundAddress(String refundAddress) {
        this.refundAddress = refundAddress;
    }

    public String getRefundTel() {
        return refundTel;
    }

    public void setRefundTel(String refundTel) {
        this.refundTel = refundTel;
    }

    public String getRefundName() {
        return refundName;
    }

    public void setRefundName(String refundName) {
        this.refundName = refundName;
    }

    public String getLogisticsType() {
        return logisticsType;
    }

    public void setLogisticsType(String logisticsType) {
        this.logisticsType = logisticsType;
    }

    public BigDecimal getUniformValue() {
        return uniformValue;
    }

    public void setUniformValue(BigDecimal uniformValue) {
        this.uniformValue = uniformValue;
    }

    public Long getTemplateValue() {
        return templateValue;
    }

    public void setTemplateValue(Long templateValue) {
        this.templateValue = templateValue;
    }

    public Byte getGift() {
        return gift;
    }

    public void setGift(Byte gift) {
        this.gift = gift;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public String getAttributes() {
        return attributes;
    }

    public void setAttributes(String attributes) {
        this.attributes = attributes;
    }

    public Boolean getYundouScale() {
        return yundouScale;
    }

    public void setYundouScale(Boolean yundouScale) {
        this.yundouScale = yundouScale;
    }

    public BigDecimal getMinYundouPrice() {
        return minYundouPrice;
    }

    public void setMinYundouPrice(BigDecimal minYundouPrice) {
        this.minYundouPrice = minYundouPrice;
    }

    public String getAvailableStatus() {
        return availableStatus;
    }

    public void setAvailableStatus(String availableStatus) {
        this.availableStatus = availableStatus;
    }

    public String getReviewStatus() {
        return reviewStatus;
    }

    public void setReviewStatus(String reviewStatus) {
        this.reviewStatus = reviewStatus;
    }

    public Double getHeight() {
        return height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public Double getWidth() {
        return width;
    }

    public void setWidth(Double width) {
        this.width = width;
    }

    public Double getLength() {
        return length;
    }

    public void setLength(Double length) {
        this.length = length;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public Long getPoint() {
        return point;
    }

    public void setPoint(Long point) {
        this.point = point;
    }

    public BigDecimal getDeductionDPoint() {
        return deductionDPoint;
    }

    public void setDeductionDPoint(BigDecimal deductionDPoint) {
        this.deductionDPoint = deductionDPoint;
    }

    public Boolean getSupportRefund() {
        return supportRefund;
    }

    public void setSupportRefund(Boolean supportRefund) {
        this.supportRefund = supportRefund;
    }

    public Integer getNumInPackage() {
        return numInPackage;
    }

    public void setNumInPackage(Integer numInPackage) {
        this.numInPackage = numInPackage;
    }

    public String getBarCode() {
        return barCode;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

    public Long getWareHouseId() {
        return wareHouseId;
    }

    public void setWareHouseId(Long wareHouseId) {
        this.wareHouseId = wareHouseId;
    }

    public Long getPackageId() {
        return packageId;
    }

    public void setPackageId(Long packageId) {
        this.packageId = packageId;
    }

    public String getOuterId() {
        return outerId;
    }

    public void setOuterId(String outerId) {
        this.outerId = outerId;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public String getWhseCode() {
        return whseCode;
    }

    public void setWhseCode(String whseCode) {
        this.whseCode = whseCode;
    }

    public String getSkuId() {
        return skuId;
    }

    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    public String getSkuOuterId() {
        return skuOuterId;
    }

    public void setSkuOuterId(String skuOuterId) {
        this.skuOuterId = skuOuterId;
    }

    public Long getSkuPrice() {
        return skuPrice;
    }

    public void setSkuPrice(Long skuPrice) {
        this.skuPrice = skuPrice;
    }

    public Integer getSkuQuantity() {
        return skuQuantity;
    }

    public void setSkuQuantity(Integer skuQuantity) {
        this.skuQuantity = skuQuantity;
    }

    public String getSkuName() {
        return skuName;
    }

    public void setSkuName(String skuName) {
        this.skuName = skuName;
    }

    public String getSkuProperty() {
        return skuProperty;
    }

    public void setSkuProperty(String skuProperty) {
        this.skuProperty = skuProperty;
    }

    public String getSkuPictureUrl() {
        return skuPictureUrl;
    }

    public void setSkuPictureUrl(String skuPictureUrl) {
        this.skuPictureUrl = skuPictureUrl;
    }

    public BigDecimal getNetWorth() {
        return netWorth;
    }

    public void setNetWorth(BigDecimal netWorth) {
        this.netWorth = netWorth;
    }

    public Long getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(Long supplierId) {
        this.supplierId = supplierId;
    }

    public Integer getSfairline() {
        return sfairline;
    }

    public void setSfairline(Integer sfairline) {
        this.sfairline = sfairline;
    }

    public Integer getSfshipping() {
        return sfshipping;
    }

    public void setSfshipping(Integer sfshipping) {
        this.sfshipping = sfshipping;
    }

    public Integer getMerchantNumber() {
        return merchantNumber;
    }

    public void setMerchantNumber(Integer merchantNumber) {
        this.merchantNumber = merchantNumber;
    }

    public BigDecimal getPromoAmt() {
        return promoAmt;
    }

    public void setPromoAmt(BigDecimal promoAmt) {
        this.promoAmt = promoAmt;
    }

    public BigDecimal getServerAmt() {
        return serverAmt;
    }

    public void setServerAmt(BigDecimal serverAmt) {
        this.serverAmt = serverAmt;
    }

    public String getDeliveryRegion() {
        return deliveryRegion;
    }

    public void setDeliveryRegion(String deliveryRegion) {
        this.deliveryRegion = deliveryRegion;
    }

    public Integer getSelfOperated() {
        return selfOperated;
    }

    public void setSelfOperated(Integer selfOperated) {
        this.selfOperated = selfOperated;
    }

    public Long getSourceId() {
        return sourceId;
    }

    public void setSourceId(Long sourceId) {
        this.sourceId = sourceId;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        XquarkProduct other = (XquarkProduct) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getCode() == null ? other.getCode() == null : this.getCode().equals(other.getCode()))
            && (this.getEncode() == null ? other.getEncode() == null : this.getEncode().equals(other.getEncode()))
            && (this.getU8Encode() == null ? other.getU8Encode() == null : this.getU8Encode().equals(other.getU8Encode()))
            && (this.getModel() == null ? other.getModel() == null : this.getModel().equals(other.getModel()))
            && (this.getType() == null ? other.getType() == null : this.getType().equals(other.getType()))
            && (this.getLevel() == null ? other.getLevel() == null : this.getLevel().equals(other.getLevel()))
            && (this.getPriority() == null ? other.getPriority() == null : this.getPriority().equals(other.getPriority()))
            && (this.getFilterSpec() == null ? other.getFilterSpec() == null : this.getFilterSpec().equals(other.getFilterSpec()))
            && (this.getUserId() == null ? other.getUserId() == null : this.getUserId().equals(other.getUserId()))
            && (this.getShopId() == null ? other.getShopId() == null : this.getShopId().equals(other.getShopId()))
            && (this.getName() == null ? other.getName() == null : this.getName().equals(other.getName()))
            && (this.getImg() == null ? other.getImg() == null : this.getImg().equals(other.getImg()))
            && (this.getImgWidth() == null ? other.getImgWidth() == null : this.getImgWidth().equals(other.getImgWidth()))
            && (this.getImgHeight() == null ? other.getImgHeight() == null : this.getImgHeight().equals(other.getImgHeight()))
            && (this.getDescImg() == null ? other.getDescImg() == null : this.getDescImg().equals(other.getDescImg()))
            && (this.getStatus() == null ? other.getStatus() == null : this.getStatus().equals(other.getStatus()))
            && (this.getMarketPrice() == null ? other.getMarketPrice() == null : this.getMarketPrice().equals(other.getMarketPrice()))
            && (this.getPrice() == null ? other.getPrice() == null : this.getPrice().equals(other.getPrice()))
            && (this.getOriginalPrice() == null ? other.getOriginalPrice() == null : this.getOriginalPrice().equals(other.getOriginalPrice()))
            && (this.getAmount() == null ? other.getAmount() == null : this.getAmount().equals(other.getAmount()))
            && (this.getSales() == null ? other.getSales() == null : this.getSales().equals(other.getSales()))
            && (this.getArchive() == null ? other.getArchive() == null : this.getArchive().equals(other.getArchive()))
            && (this.getRecommend() == null ? other.getRecommend() == null : this.getRecommend().equals(other.getRecommend()))
            && (this.getRecommendAt() == null ? other.getRecommendAt() == null : this.getRecommendAt().equals(other.getRecommendAt()))
            && (this.getDiscount() == null ? other.getDiscount() == null : this.getDiscount().equals(other.getDiscount()))
            && (this.getIsCommission() == null ? other.getIsCommission() == null : this.getIsCommission().equals(other.getIsCommission()))
            && (this.getCommissionRate() == null ? other.getCommissionRate() == null : this.getCommissionRate().equals(other.getCommissionRate()))
            && (this.getForsaleAt() == null ? other.getForsaleAt() == null : this.getForsaleAt().equals(other.getForsaleAt()))
            && (this.getOnsaleAt() == null ? other.getOnsaleAt() == null : this.getOnsaleAt().equals(other.getOnsaleAt()))
            && (this.getInstockAt() == null ? other.getInstockAt() == null : this.getInstockAt().equals(other.getInstockAt()))
            && (this.getCreatedAt() == null ? other.getCreatedAt() == null : this.getCreatedAt().equals(other.getCreatedAt()))
            && (this.getUpdatedAt() == null ? other.getUpdatedAt() == null : this.getUpdatedAt().equals(other.getUpdatedAt()))
            && (this.getFakeSales() == null ? other.getFakeSales() == null : this.getFakeSales().equals(other.getFakeSales()))
            && (this.getIsdelay() == null ? other.getIsdelay() == null : this.getIsdelay().equals(other.getIsdelay()))
            && (this.getDelaydays() == null ? other.getDelaydays() == null : this.getDelaydays().equals(other.getDelaydays()))
            && (this.getUpdateLock() == null ? other.getUpdateLock() == null : this.getUpdateLock().equals(other.getUpdateLock()))
            && (this.getSynchronousflag() == null ? other.getSynchronousflag() == null : this.getSynchronousflag().equals(other.getSynchronousflag()))
            && (this.getThirdItemId() == null ? other.getThirdItemId() == null : this.getThirdItemId().equals(other.getThirdItemId()))
            && (this.getPartnerProductId() == null ? other.getPartnerProductId() == null : this.getPartnerProductId().equals(other.getPartnerProductId()))
            && (this.getSource() == null ? other.getSource() == null : this.getSource().equals(other.getSource()))
            && (this.getInActivity() == null ? other.getInActivity() == null : this.getInActivity().equals(other.getInActivity()))
            && (this.getIsCrossBorder() == null ? other.getIsCrossBorder() == null : this.getIsCrossBorder().equals(other.getIsCrossBorder()))
            && (this.getEnableDesc() == null ? other.getEnableDesc() == null : this.getEnableDesc().equals(other.getEnableDesc()))
            && (this.getIsDistribution() == null ? other.getIsDistribution() == null : this.getIsDistribution().equals(other.getIsDistribution()))
            && (this.getSourceProductId() == null ? other.getSourceProductId() == null : this.getSourceProductId().equals(other.getSourceProductId()))
            && (this.getSourceShopId() == null ? other.getSourceShopId() == null : this.getSourceShopId().equals(other.getSourceShopId()))
            && (this.getFeature0() == null ? other.getFeature0() == null : this.getFeature0().equals(other.getFeature0()))
            && (this.getFeature1() == null ? other.getFeature1() == null : this.getFeature1().equals(other.getFeature1()))
            && (this.getOneyuanPostage() == null ? other.getOneyuanPostage() == null : this.getOneyuanPostage().equals(other.getOneyuanPostage()))
            && (this.getOneyuanPurchase() == null ? other.getOneyuanPurchase() == null : this.getOneyuanPurchase().equals(other.getOneyuanPurchase()))
            && (this.getSpecialrate() == null ? other.getSpecialrate() == null : this.getSpecialrate().equals(other.getSpecialrate()))
            && (this.getIsGroupon() == null ? other.getIsGroupon() == null : this.getIsGroupon().equals(other.getIsGroupon()))
            && (this.getRefundAddress() == null ? other.getRefundAddress() == null : this.getRefundAddress().equals(other.getRefundAddress()))
            && (this.getRefundTel() == null ? other.getRefundTel() == null : this.getRefundTel().equals(other.getRefundTel()))
            && (this.getRefundName() == null ? other.getRefundName() == null : this.getRefundName().equals(other.getRefundName()))
            && (this.getLogisticsType() == null ? other.getLogisticsType() == null : this.getLogisticsType().equals(other.getLogisticsType()))
            && (this.getUniformValue() == null ? other.getUniformValue() == null : this.getUniformValue().equals(other.getUniformValue()))
            && (this.getTemplateValue() == null ? other.getTemplateValue() == null : this.getTemplateValue().equals(other.getTemplateValue()))
            && (this.getGift() == null ? other.getGift() == null : this.getGift().equals(other.getGift()))
            && (this.getWeight() == null ? other.getWeight() == null : this.getWeight().equals(other.getWeight()))
            && (this.getAttributes() == null ? other.getAttributes() == null : this.getAttributes().equals(other.getAttributes()))
            && (this.getYundouScale() == null ? other.getYundouScale() == null : this.getYundouScale().equals(other.getYundouScale()))
            && (this.getMinYundouPrice() == null ? other.getMinYundouPrice() == null : this.getMinYundouPrice().equals(other.getMinYundouPrice()))
            && (this.getAvailableStatus() == null ? other.getAvailableStatus() == null : this.getAvailableStatus().equals(other.getAvailableStatus()))
            && (this.getReviewStatus() == null ? other.getReviewStatus() == null : this.getReviewStatus().equals(other.getReviewStatus()))
            && (this.getHeight() == null ? other.getHeight() == null : this.getHeight().equals(other.getHeight()))
            && (this.getWidth() == null ? other.getWidth() == null : this.getWidth().equals(other.getWidth()))
            && (this.getLength() == null ? other.getLength() == null : this.getLength().equals(other.getLength()))
            && (this.getKind() == null ? other.getKind() == null : this.getKind().equals(other.getKind()))
            && (this.getPoint() == null ? other.getPoint() == null : this.getPoint().equals(other.getPoint()))
            && (this.getDeductionDPoint() == null ? other.getDeductionDPoint() == null : this.getDeductionDPoint().equals(other.getDeductionDPoint()))
            && (this.getSupportRefund() == null ? other.getSupportRefund() == null : this.getSupportRefund().equals(other.getSupportRefund()))
            && (this.getNumInPackage() == null ? other.getNumInPackage() == null : this.getNumInPackage().equals(other.getNumInPackage()))
            && (this.getBarCode() == null ? other.getBarCode() == null : this.getBarCode().equals(other.getBarCode()))
            && (this.getWareHouseId() == null ? other.getWareHouseId() == null : this.getWareHouseId().equals(other.getWareHouseId()))
            && (this.getPackageId() == null ? other.getPackageId() == null : this.getPackageId().equals(other.getPackageId()))
            && (this.getOuterId() == null ? other.getOuterId() == null : this.getOuterId().equals(other.getOuterId()))
            && (this.getNum() == null ? other.getNum() == null : this.getNum().equals(other.getNum()))
            && (this.getWhseCode() == null ? other.getWhseCode() == null : this.getWhseCode().equals(other.getWhseCode()))
            && (this.getSkuId() == null ? other.getSkuId() == null : this.getSkuId().equals(other.getSkuId()))
            && (this.getSkuOuterId() == null ? other.getSkuOuterId() == null : this.getSkuOuterId().equals(other.getSkuOuterId()))
            && (this.getSkuPrice() == null ? other.getSkuPrice() == null : this.getSkuPrice().equals(other.getSkuPrice()))
            && (this.getSkuQuantity() == null ? other.getSkuQuantity() == null : this.getSkuQuantity().equals(other.getSkuQuantity()))
            && (this.getSkuName() == null ? other.getSkuName() == null : this.getSkuName().equals(other.getSkuName()))
            && (this.getSkuProperty() == null ? other.getSkuProperty() == null : this.getSkuProperty().equals(other.getSkuProperty()))
            && (this.getSkuPictureUrl() == null ? other.getSkuPictureUrl() == null : this.getSkuPictureUrl().equals(other.getSkuPictureUrl()))
            && (this.getNetWorth() == null ? other.getNetWorth() == null : this.getNetWorth().equals(other.getNetWorth()))
            && (this.getSupplierId() == null ? other.getSupplierId() == null : this.getSupplierId().equals(other.getSupplierId()))
            && (this.getSfairline() == null ? other.getSfairline() == null : this.getSfairline().equals(other.getSfairline()))
            && (this.getSfshipping() == null ? other.getSfshipping() == null : this.getSfshipping().equals(other.getSfshipping()))
            && (this.getMerchantNumber() == null ? other.getMerchantNumber() == null : this.getMerchantNumber().equals(other.getMerchantNumber()))
            && (this.getPromoAmt() == null ? other.getPromoAmt() == null : this.getPromoAmt().equals(other.getPromoAmt()))
            && (this.getServerAmt() == null ? other.getServerAmt() == null : this.getServerAmt().equals(other.getServerAmt()))
            && (this.getDeliveryRegion() == null ? other.getDeliveryRegion() == null : this.getDeliveryRegion().equals(other.getDeliveryRegion()))
            && (this.getSelfOperated() == null ? other.getSelfOperated() == null : this.getSelfOperated().equals(other.getSelfOperated()))
            && (this.getSourceId() == null ? other.getSourceId() == null : this.getSourceId().equals(other.getSourceId()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getCode() == null) ? 0 : getCode().hashCode());
        result = prime * result + ((getEncode() == null) ? 0 : getEncode().hashCode());
        result = prime * result + ((getU8Encode() == null) ? 0 : getU8Encode().hashCode());
        result = prime * result + ((getModel() == null) ? 0 : getModel().hashCode());
        result = prime * result + ((getType() == null) ? 0 : getType().hashCode());
        result = prime * result + ((getLevel() == null) ? 0 : getLevel().hashCode());
        result = prime * result + ((getPriority() == null) ? 0 : getPriority().hashCode());
        result = prime * result + ((getFilterSpec() == null) ? 0 : getFilterSpec().hashCode());
        result = prime * result + ((getUserId() == null) ? 0 : getUserId().hashCode());
        result = prime * result + ((getShopId() == null) ? 0 : getShopId().hashCode());
        result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
        result = prime * result + ((getImg() == null) ? 0 : getImg().hashCode());
        result = prime * result + ((getImgWidth() == null) ? 0 : getImgWidth().hashCode());
        result = prime * result + ((getImgHeight() == null) ? 0 : getImgHeight().hashCode());
        result = prime * result + ((getDescImg() == null) ? 0 : getDescImg().hashCode());
        result = prime * result + ((getStatus() == null) ? 0 : getStatus().hashCode());
        result = prime * result + ((getMarketPrice() == null) ? 0 : getMarketPrice().hashCode());
        result = prime * result + ((getPrice() == null) ? 0 : getPrice().hashCode());
        result = prime * result + ((getOriginalPrice() == null) ? 0 : getOriginalPrice().hashCode());
        result = prime * result + ((getAmount() == null) ? 0 : getAmount().hashCode());
        result = prime * result + ((getSales() == null) ? 0 : getSales().hashCode());
        result = prime * result + ((getArchive() == null) ? 0 : getArchive().hashCode());
        result = prime * result + ((getRecommend() == null) ? 0 : getRecommend().hashCode());
        result = prime * result + ((getRecommendAt() == null) ? 0 : getRecommendAt().hashCode());
        result = prime * result + ((getDiscount() == null) ? 0 : getDiscount().hashCode());
        result = prime * result + ((getIsCommission() == null) ? 0 : getIsCommission().hashCode());
        result = prime * result + ((getCommissionRate() == null) ? 0 : getCommissionRate().hashCode());
        result = prime * result + ((getForsaleAt() == null) ? 0 : getForsaleAt().hashCode());
        result = prime * result + ((getOnsaleAt() == null) ? 0 : getOnsaleAt().hashCode());
        result = prime * result + ((getInstockAt() == null) ? 0 : getInstockAt().hashCode());
        result = prime * result + ((getCreatedAt() == null) ? 0 : getCreatedAt().hashCode());
        result = prime * result + ((getUpdatedAt() == null) ? 0 : getUpdatedAt().hashCode());
        result = prime * result + ((getFakeSales() == null) ? 0 : getFakeSales().hashCode());
        result = prime * result + ((getIsdelay() == null) ? 0 : getIsdelay().hashCode());
        result = prime * result + ((getDelaydays() == null) ? 0 : getDelaydays().hashCode());
        result = prime * result + ((getUpdateLock() == null) ? 0 : getUpdateLock().hashCode());
        result = prime * result + ((getSynchronousflag() == null) ? 0 : getSynchronousflag().hashCode());
        result = prime * result + ((getThirdItemId() == null) ? 0 : getThirdItemId().hashCode());
        result = prime * result + ((getPartnerProductId() == null) ? 0 : getPartnerProductId().hashCode());
        result = prime * result + ((getSource() == null) ? 0 : getSource().hashCode());
        result = prime * result + ((getInActivity() == null) ? 0 : getInActivity().hashCode());
        result = prime * result + ((getIsCrossBorder() == null) ? 0 : getIsCrossBorder().hashCode());
        result = prime * result + ((getEnableDesc() == null) ? 0 : getEnableDesc().hashCode());
        result = prime * result + ((getIsDistribution() == null) ? 0 : getIsDistribution().hashCode());
        result = prime * result + ((getSourceProductId() == null) ? 0 : getSourceProductId().hashCode());
        result = prime * result + ((getSourceShopId() == null) ? 0 : getSourceShopId().hashCode());
        result = prime * result + ((getFeature0() == null) ? 0 : getFeature0().hashCode());
        result = prime * result + ((getFeature1() == null) ? 0 : getFeature1().hashCode());
        result = prime * result + ((getOneyuanPostage() == null) ? 0 : getOneyuanPostage().hashCode());
        result = prime * result + ((getOneyuanPurchase() == null) ? 0 : getOneyuanPurchase().hashCode());
        result = prime * result + ((getSpecialrate() == null) ? 0 : getSpecialrate().hashCode());
        result = prime * result + ((getIsGroupon() == null) ? 0 : getIsGroupon().hashCode());
        result = prime * result + ((getRefundAddress() == null) ? 0 : getRefundAddress().hashCode());
        result = prime * result + ((getRefundTel() == null) ? 0 : getRefundTel().hashCode());
        result = prime * result + ((getRefundName() == null) ? 0 : getRefundName().hashCode());
        result = prime * result + ((getLogisticsType() == null) ? 0 : getLogisticsType().hashCode());
        result = prime * result + ((getUniformValue() == null) ? 0 : getUniformValue().hashCode());
        result = prime * result + ((getTemplateValue() == null) ? 0 : getTemplateValue().hashCode());
        result = prime * result + ((getGift() == null) ? 0 : getGift().hashCode());
        result = prime * result + ((getWeight() == null) ? 0 : getWeight().hashCode());
        result = prime * result + ((getAttributes() == null) ? 0 : getAttributes().hashCode());
        result = prime * result + ((getYundouScale() == null) ? 0 : getYundouScale().hashCode());
        result = prime * result + ((getMinYundouPrice() == null) ? 0 : getMinYundouPrice().hashCode());
        result = prime * result + ((getAvailableStatus() == null) ? 0 : getAvailableStatus().hashCode());
        result = prime * result + ((getReviewStatus() == null) ? 0 : getReviewStatus().hashCode());
        result = prime * result + ((getHeight() == null) ? 0 : getHeight().hashCode());
        result = prime * result + ((getWidth() == null) ? 0 : getWidth().hashCode());
        result = prime * result + ((getLength() == null) ? 0 : getLength().hashCode());
        result = prime * result + ((getKind() == null) ? 0 : getKind().hashCode());
        result = prime * result + ((getPoint() == null) ? 0 : getPoint().hashCode());
        result = prime * result + ((getDeductionDPoint() == null) ? 0 : getDeductionDPoint().hashCode());
        result = prime * result + ((getSupportRefund() == null) ? 0 : getSupportRefund().hashCode());
        result = prime * result + ((getNumInPackage() == null) ? 0 : getNumInPackage().hashCode());
        result = prime * result + ((getBarCode() == null) ? 0 : getBarCode().hashCode());
        result = prime * result + ((getWareHouseId() == null) ? 0 : getWareHouseId().hashCode());
        result = prime * result + ((getPackageId() == null) ? 0 : getPackageId().hashCode());
        result = prime * result + ((getOuterId() == null) ? 0 : getOuterId().hashCode());
        result = prime * result + ((getNum() == null) ? 0 : getNum().hashCode());
        result = prime * result + ((getWhseCode() == null) ? 0 : getWhseCode().hashCode());
        result = prime * result + ((getSkuId() == null) ? 0 : getSkuId().hashCode());
        result = prime * result + ((getSkuOuterId() == null) ? 0 : getSkuOuterId().hashCode());
        result = prime * result + ((getSkuPrice() == null) ? 0 : getSkuPrice().hashCode());
        result = prime * result + ((getSkuQuantity() == null) ? 0 : getSkuQuantity().hashCode());
        result = prime * result + ((getSkuName() == null) ? 0 : getSkuName().hashCode());
        result = prime * result + ((getSkuProperty() == null) ? 0 : getSkuProperty().hashCode());
        result = prime * result + ((getSkuPictureUrl() == null) ? 0 : getSkuPictureUrl().hashCode());
        result = prime * result + ((getNetWorth() == null) ? 0 : getNetWorth().hashCode());
        result = prime * result + ((getSupplierId() == null) ? 0 : getSupplierId().hashCode());
        result = prime * result + ((getSfairline() == null) ? 0 : getSfairline().hashCode());
        result = prime * result + ((getSfshipping() == null) ? 0 : getSfshipping().hashCode());
        result = prime * result + ((getMerchantNumber() == null) ? 0 : getMerchantNumber().hashCode());
        result = prime * result + ((getPromoAmt() == null) ? 0 : getPromoAmt().hashCode());
        result = prime * result + ((getServerAmt() == null) ? 0 : getServerAmt().hashCode());
        result = prime * result + ((getDeliveryRegion() == null) ? 0 : getDeliveryRegion().hashCode());
        result = prime * result + ((getSelfOperated() == null) ? 0 : getSelfOperated().hashCode());
        result = prime * result + ((getSourceId() == null) ? 0 : getSourceId().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", code=").append(code);
        sb.append(", encode=").append(encode);
        sb.append(", u8Encode=").append(u8Encode);
        sb.append(", model=").append(model);
        sb.append(", type=").append(type);
        sb.append(", level=").append(level);
        sb.append(", priority=").append(priority);
        sb.append(", filterSpec=").append(filterSpec);
        sb.append(", userId=").append(userId);
        sb.append(", shopId=").append(shopId);
        sb.append(", name=").append(name);
        sb.append(", img=").append(img);
        sb.append(", imgWidth=").append(imgWidth);
        sb.append(", imgHeight=").append(imgHeight);
        sb.append(", descImg=").append(descImg);
        sb.append(", status=").append(status);
        sb.append(", marketPrice=").append(marketPrice);
        sb.append(", price=").append(price);
        sb.append(", originalPrice=").append(originalPrice);
        sb.append(", amount=").append(amount);
        sb.append(", sales=").append(sales);
        sb.append(", archive=").append(archive);
        sb.append(", recommend=").append(recommend);
        sb.append(", recommendAt=").append(recommendAt);
        sb.append(", discount=").append(discount);
        sb.append(", isCommission=").append(isCommission);
        sb.append(", commissionRate=").append(commissionRate);
        sb.append(", forsaleAt=").append(forsaleAt);
        sb.append(", onsaleAt=").append(onsaleAt);
        sb.append(", instockAt=").append(instockAt);
        sb.append(", createdAt=").append(createdAt);
        sb.append(", updatedAt=").append(updatedAt);
        sb.append(", fakeSales=").append(fakeSales);
        sb.append(", isdelay=").append(isdelay);
        sb.append(", delaydays=").append(delaydays);
        sb.append(", updateLock=").append(updateLock);
        sb.append(", synchronousflag=").append(synchronousflag);
        sb.append(", thirdItemId=").append(thirdItemId);
        sb.append(", partnerProductId=").append(partnerProductId);
        sb.append(", source=").append(source);
        sb.append(", inActivity=").append(inActivity);
        sb.append(", isCrossBorder=").append(isCrossBorder);
        sb.append(", enableDesc=").append(enableDesc);
        sb.append(", isDistribution=").append(isDistribution);
        sb.append(", sourceProductId=").append(sourceProductId);
        sb.append(", sourceShopId=").append(sourceShopId);
        sb.append(", feature0=").append(feature0);
        sb.append(", feature1=").append(feature1);
        sb.append(", oneyuanPostage=").append(oneyuanPostage);
        sb.append(", oneyuanPurchase=").append(oneyuanPurchase);
        sb.append(", specialrate=").append(specialrate);
        sb.append(", isGroupon=").append(isGroupon);
        sb.append(", refundAddress=").append(refundAddress);
        sb.append(", refundTel=").append(refundTel);
        sb.append(", refundName=").append(refundName);
        sb.append(", logisticsType=").append(logisticsType);
        sb.append(", uniformValue=").append(uniformValue);
        sb.append(", templateValue=").append(templateValue);
        sb.append(", gift=").append(gift);
        sb.append(", weight=").append(weight);
        sb.append(", attributes=").append(attributes);
        sb.append(", yundouScale=").append(yundouScale);
        sb.append(", minYundouPrice=").append(minYundouPrice);
        sb.append(", availableStatus=").append(availableStatus);
        sb.append(", reviewStatus=").append(reviewStatus);
        sb.append(", height=").append(height);
        sb.append(", width=").append(width);
        sb.append(", length=").append(length);
        sb.append(", kind=").append(kind);
        sb.append(", point=").append(point);
        sb.append(", deductionDPoint=").append(deductionDPoint);
        sb.append(", supportRefund=").append(supportRefund);
        sb.append(", numInPackage=").append(numInPackage);
        sb.append(", barCode=").append(barCode);
        sb.append(", wareHouseId=").append(wareHouseId);
        sb.append(", packageId=").append(packageId);
        sb.append(", outerId=").append(outerId);
        sb.append(", num=").append(num);
        sb.append(", whseCode=").append(whseCode);
        sb.append(", skuId=").append(skuId);
        sb.append(", skuOuterId=").append(skuOuterId);
        sb.append(", skuPrice=").append(skuPrice);
        sb.append(", skuQuantity=").append(skuQuantity);
        sb.append(", skuName=").append(skuName);
        sb.append(", skuProperty=").append(skuProperty);
        sb.append(", skuPictureUrl=").append(skuPictureUrl);
        sb.append(", netWorth=").append(netWorth);
        sb.append(", supplierId=").append(supplierId);
        sb.append(", sfairline=").append(sfairline);
        sb.append(", sfshipping=").append(sfshipping);
        sb.append(", merchantNumber=").append(merchantNumber);
        sb.append(", promoAmt=").append(promoAmt);
        sb.append(", serverAmt=").append(serverAmt);
        sb.append(", deliveryRegion=").append(deliveryRegion);
        sb.append(", selfOperated=").append(selfOperated);
        sb.append(", sourceId=").append(sourceId);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}