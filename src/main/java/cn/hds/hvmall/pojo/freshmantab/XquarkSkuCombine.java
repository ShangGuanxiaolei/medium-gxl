package cn.hds.hvmall.pojo.freshmantab;

import net.sf.jsqlparser.expression.DateTimeLiteralExpression;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: WangXiao
 * Date: 2019/5/7
 * Time: 16:21
 * Description: 套装
 */
public class XquarkSkuCombine {

    private Long id;

    private Long productId;

    private Long skuId;

    private Date validFrom;

    private Date validTo;

    private Date createdAt;

    private Date updatedAt;

    private Integer archive;

    public Integer getArchive() {
        return archive;
    }

    public void setArchive(Integer archive) {
        this.archive = archive;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    public Date getValidTo() {
        return validTo;
    }

    public void setValidTo(Date validTo) {
        this.validTo = validTo;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}