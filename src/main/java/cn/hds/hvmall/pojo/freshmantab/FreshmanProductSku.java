package cn.hds.hvmall.pojo.freshmantab;

import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 * User: WangXiao
 * Date: 2019/4/12
 * Time: 11:56
 * Description: 新客配置价格POJO
 */
public class FreshmanProductSku {

    private String skuCode;

    private String spec;//商品规格

    private Integer sourceStock;//商品源库存.如果是源商品的话没有复制

    private String sourceSkuCode;

    private Long productIdsku;

    private Integer amount;//源SKU库存

    private BigDecimal sourcePrice;//源价格;

    private BigDecimal sourceConversionPrice;//源兑换价

    private BigDecimal freshManPrice;//专区价格

    private BigDecimal freshPoint;//专区德分

    private BigDecimal freshConversionPrice;//专区兑换价

    private BigDecimal reduction;//专区立减

    private Integer freshStock;//专区库存

    private Integer newFreshStock;//前端需要的字段

    private BigDecimal promoAmt;//专区推广费

    private BigDecimal serverAmt;//专区服务费

    private BigDecimal netWorth;//专区净值

    private Integer status;

    public Integer getSales() {
        return sales;
    }

    public void setSales(Integer sales) {
        this.sales = sales;
    }

    private Integer sales;

    public Integer getNewFreshStock() {
        return newFreshStock;
    }

    public void setNewFreshStock(Integer newFreshStock) {
        this.newFreshStock = newFreshStock;
    }

    public Integer getSourceStock() {
        return sourceStock;
    }

    public void setSourceStock(Integer sourceStock) {
        this.sourceStock = sourceStock;
    }

    public String getSourceSkuCode() {
        return sourceSkuCode;
    }

    public void setSourceSkuCode(String sourceSkuCode) {
        this.sourceSkuCode = sourceSkuCode;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Long getProductIdsku() {
        return productIdsku;
    }

    public void setProductIdsku(Long productIdsku) {
        this.productIdsku = productIdsku;
    }

    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    public String getSpec() {
        return spec;
    }

    public void setSpec(String spec) {
        this.spec = spec;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public BigDecimal getSourcePrice() {
        return sourcePrice;
    }

    public void setSourcePrice(BigDecimal sourcePrice) {
        this.sourcePrice = sourcePrice;
    }

    public BigDecimal getSourceConversionPrice() {
        return sourceConversionPrice;
    }

    public void setSourceConversionPrice(BigDecimal sourceConversionPrice) {
        this.sourceConversionPrice = sourceConversionPrice;
    }

    public BigDecimal getFreshManPrice() {
        return freshManPrice;
    }

    public void setFreshManPrice(BigDecimal freshManPrice) {
        this.freshManPrice = freshManPrice;
    }

    public BigDecimal getFreshPoint() {
        return freshPoint;
    }

    public void setFreshPoint(BigDecimal freshPoint) {
        this.freshPoint = freshPoint;
    }

    public BigDecimal getFreshConversionPrice() {
        return freshConversionPrice;
    }

    public void setFreshConversionPrice(BigDecimal freshConversionPrice) {
        this.freshConversionPrice = freshConversionPrice;
    }


    public BigDecimal getReduction() {
        return reduction;
    }

    public void setReduction(BigDecimal reduction) {
        this.reduction = reduction;
    }

    public Integer getFreshStock() {
        return freshStock;
    }

    public void setFreshStock(Integer freshStock) {
        this.freshStock = freshStock;
    }

    public BigDecimal getPromoAmt() {
        return promoAmt;
    }

    public void setPromoAmt(BigDecimal promoAmt) {
        this.promoAmt = promoAmt;
    }

    public BigDecimal getServerAmt() {
        return serverAmt;
    }

    public void setServerAmt(BigDecimal serverAmt) {
        this.serverAmt = serverAmt;
    }

    public BigDecimal getNetWorth() {
        return netWorth;
    }

    public void setNetWorth(BigDecimal netWorth) {
        this.netWorth = netWorth;
    }
}