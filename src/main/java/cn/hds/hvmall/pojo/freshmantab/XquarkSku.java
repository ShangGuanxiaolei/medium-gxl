package cn.hds.hvmall.pojo.freshmantab;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * xquark_sku
 * @author 
 */
public class XquarkSku implements Serializable {
    /**
     * 主键ID
     */
    private Long id;

    /**
     * 商品ID
     */
    private Long productId;

    /**
     * 型号名称
     */
    private String spec;

    /**
     * 原价
     */
    private BigDecimal marketPrice;

    /**
     * 折扣价
     */
    private BigDecimal price;

    /**
     * 原始价格（用户录入价格）
     */
    private BigDecimal originalPrice;

    /**
     * 库存
     */
    private Integer amount;

    /**
     * sku排序
     */
    private Integer skuOrder;

    /**
     * 是否删除：0=正常，1=删除
     */
    private Boolean archive;

    /**
     * 型号维度1
     */
    private String spec1;

    /**
     * 型号维度2
     */
    private String spec2;

    /**
     * 型号维度3
     */
    private String spec3;

    /**
     * 型号维度4
     */
    private String spec4;

    /**
     * 型号维度5
     */
    private String spec5;

    /**
     * 创建时间
     */
    private Date createdAt;

    /**
     * 更新时间
     */
    private Date updatedAt;

    private Long partnerProductId;

    /**
     * 是否正在活动优惠中(0/1)
     */
    private Byte inActivity;

    private String code;

    private String thirdSkuId;

    /**
     * 商品唯一标识码
     */
    private String skuCode;

    /**
     * 商品标识来源
     */
    private String skuCodeResources;

    private Long sourceSkuId;

    /**
     * 商品sku拥有的属性值（如白色，L码等）
     */
    private String attributes;

    private Integer secureAmount;

    private BigDecimal point;

    private BigDecimal deductionDPoint;

    /**
     * 商品净值
     */
    private BigDecimal netWorth;

    /**
     * 商品条形码
     */
    private String barCode;

    private BigDecimal partnerProductPrice;

    /**
     * 推广费
     */
    private BigDecimal promoAmt;

    /**
     * 服务费
     */
    private BigDecimal serverAmt;

    /**
     * sku图片
     */
    private String skuImg;

    /**
     * 高度
     */
    private Double height;

    /**
     * 宽度
     */
    private Double width;

    /**
     * 长度
     */
    private Double length;

    /**
     * 装箱数
     */
    private Integer numInPackage;

    /**
     * 重量
     */
    private Integer weight;

    /**
     * 原父sku_code
     */
    private String sourceSkuCode;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getSpec() {
        return spec;
    }

    public void setSpec(String spec) {
        this.spec = spec;
    }

    public BigDecimal getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(BigDecimal marketPrice) {
        this.marketPrice = marketPrice;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(BigDecimal originalPrice) {
        this.originalPrice = originalPrice;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getSkuOrder() {
        return skuOrder;
    }

    public void setSkuOrder(Integer skuOrder) {
        this.skuOrder = skuOrder;
    }

    public Boolean getArchive() {
        return archive;
    }

    public void setArchive(Boolean archive) {
        this.archive = archive;
    }

    public String getSpec1() {
        return spec1;
    }

    public void setSpec1(String spec1) {
        this.spec1 = spec1;
    }

    public String getSpec2() {
        return spec2;
    }

    public void setSpec2(String spec2) {
        this.spec2 = spec2;
    }

    public String getSpec3() {
        return spec3;
    }

    public void setSpec3(String spec3) {
        this.spec3 = spec3;
    }

    public String getSpec4() {
        return spec4;
    }

    public void setSpec4(String spec4) {
        this.spec4 = spec4;
    }

    public String getSpec5() {
        return spec5;
    }

    public void setSpec5(String spec5) {
        this.spec5 = spec5;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Long getPartnerProductId() {
        return partnerProductId;
    }

    public void setPartnerProductId(Long partnerProductId) {
        this.partnerProductId = partnerProductId;
    }

    public Byte getInActivity() {
        return inActivity;
    }

    public void setInActivity(Byte inActivity) {
        this.inActivity = inActivity;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getThirdSkuId() {
        return thirdSkuId;
    }

    public void setThirdSkuId(String thirdSkuId) {
        this.thirdSkuId = thirdSkuId;
    }

    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    public String getSkuCodeResources() {
        return skuCodeResources;
    }

    public void setSkuCodeResources(String skuCodeResources) {
        this.skuCodeResources = skuCodeResources;
    }

    public Long getSourceSkuId() {
        return sourceSkuId;
    }

    public void setSourceSkuId(Long sourceSkuId) {
        this.sourceSkuId = sourceSkuId;
    }

    public String getAttributes() {
        return attributes;
    }

    public void setAttributes(String attributes) {
        this.attributes = attributes;
    }

    public Integer getSecureAmount() {
        return secureAmount;
    }

    public void setSecureAmount(Integer secureAmount) {
        this.secureAmount = secureAmount;
    }

    public BigDecimal getPoint() {
        return point;
    }

    public void setPoint(BigDecimal point) {
        this.point = point;
    }

    public BigDecimal getDeductionDPoint() {
        return deductionDPoint;
    }

    public void setDeductionDPoint(BigDecimal deductionDPoint) {
        this.deductionDPoint = deductionDPoint;
    }

    public BigDecimal getNetWorth() {
        return netWorth;
    }

    public void setNetWorth(BigDecimal netWorth) {
        this.netWorth = netWorth;
    }

    public String getBarCode() {
        return barCode;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

    public BigDecimal getPartnerProductPrice() {
        return partnerProductPrice;
    }

    public void setPartnerProductPrice(BigDecimal partnerProductPrice) {
        this.partnerProductPrice = partnerProductPrice;
    }

    public BigDecimal getPromoAmt() {
        return promoAmt;
    }

    public void setPromoAmt(BigDecimal promoAmt) {
        this.promoAmt = promoAmt;
    }

    public BigDecimal getServerAmt() {
        return serverAmt;
    }

    public void setServerAmt(BigDecimal serverAmt) {
        this.serverAmt = serverAmt;
    }

    public String getSkuImg() {
        return skuImg;
    }

    public void setSkuImg(String skuImg) {
        this.skuImg = skuImg;
    }

    public Double getHeight() {
        return height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public Double getWidth() {
        return width;
    }

    public void setWidth(Double width) {
        this.width = width;
    }

    public Double getLength() {
        return length;
    }

    public void setLength(Double length) {
        this.length = length;
    }

    public Integer getNumInPackage() {
        return numInPackage;
    }

    public void setNumInPackage(Integer numInPackage) {
        this.numInPackage = numInPackage;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public String getSourceSkuCode() {
        return sourceSkuCode;
    }

    public void setSourceSkuCode(String sourceSkuCode) {
        this.sourceSkuCode = sourceSkuCode;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        XquarkSku other = (XquarkSku) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getProductId() == null ? other.getProductId() == null : this.getProductId().equals(other.getProductId()))
            && (this.getSpec() == null ? other.getSpec() == null : this.getSpec().equals(other.getSpec()))
            && (this.getMarketPrice() == null ? other.getMarketPrice() == null : this.getMarketPrice().equals(other.getMarketPrice()))
            && (this.getPrice() == null ? other.getPrice() == null : this.getPrice().equals(other.getPrice()))
            && (this.getOriginalPrice() == null ? other.getOriginalPrice() == null : this.getOriginalPrice().equals(other.getOriginalPrice()))
            && (this.getAmount() == null ? other.getAmount() == null : this.getAmount().equals(other.getAmount()))
            && (this.getSkuOrder() == null ? other.getSkuOrder() == null : this.getSkuOrder().equals(other.getSkuOrder()))
            && (this.getArchive() == null ? other.getArchive() == null : this.getArchive().equals(other.getArchive()))
            && (this.getSpec1() == null ? other.getSpec1() == null : this.getSpec1().equals(other.getSpec1()))
            && (this.getSpec2() == null ? other.getSpec2() == null : this.getSpec2().equals(other.getSpec2()))
            && (this.getSpec3() == null ? other.getSpec3() == null : this.getSpec3().equals(other.getSpec3()))
            && (this.getSpec4() == null ? other.getSpec4() == null : this.getSpec4().equals(other.getSpec4()))
            && (this.getSpec5() == null ? other.getSpec5() == null : this.getSpec5().equals(other.getSpec5()))
            && (this.getCreatedAt() == null ? other.getCreatedAt() == null : this.getCreatedAt().equals(other.getCreatedAt()))
            && (this.getUpdatedAt() == null ? other.getUpdatedAt() == null : this.getUpdatedAt().equals(other.getUpdatedAt()))
            && (this.getPartnerProductId() == null ? other.getPartnerProductId() == null : this.getPartnerProductId().equals(other.getPartnerProductId()))
            && (this.getInActivity() == null ? other.getInActivity() == null : this.getInActivity().equals(other.getInActivity()))
            && (this.getCode() == null ? other.getCode() == null : this.getCode().equals(other.getCode()))
            && (this.getThirdSkuId() == null ? other.getThirdSkuId() == null : this.getThirdSkuId().equals(other.getThirdSkuId()))
            && (this.getSkuCode() == null ? other.getSkuCode() == null : this.getSkuCode().equals(other.getSkuCode()))
            && (this.getSkuCodeResources() == null ? other.getSkuCodeResources() == null : this.getSkuCodeResources().equals(other.getSkuCodeResources()))
            && (this.getSourceSkuId() == null ? other.getSourceSkuId() == null : this.getSourceSkuId().equals(other.getSourceSkuId()))
            && (this.getAttributes() == null ? other.getAttributes() == null : this.getAttributes().equals(other.getAttributes()))
            && (this.getSecureAmount() == null ? other.getSecureAmount() == null : this.getSecureAmount().equals(other.getSecureAmount()))
            && (this.getPoint() == null ? other.getPoint() == null : this.getPoint().equals(other.getPoint()))
            && (this.getDeductionDPoint() == null ? other.getDeductionDPoint() == null : this.getDeductionDPoint().equals(other.getDeductionDPoint()))
            && (this.getNetWorth() == null ? other.getNetWorth() == null : this.getNetWorth().equals(other.getNetWorth()))
            && (this.getBarCode() == null ? other.getBarCode() == null : this.getBarCode().equals(other.getBarCode()))
            && (this.getPartnerProductPrice() == null ? other.getPartnerProductPrice() == null : this.getPartnerProductPrice().equals(other.getPartnerProductPrice()))
            && (this.getPromoAmt() == null ? other.getPromoAmt() == null : this.getPromoAmt().equals(other.getPromoAmt()))
            && (this.getServerAmt() == null ? other.getServerAmt() == null : this.getServerAmt().equals(other.getServerAmt()))
            && (this.getSkuImg() == null ? other.getSkuImg() == null : this.getSkuImg().equals(other.getSkuImg()))
            && (this.getHeight() == null ? other.getHeight() == null : this.getHeight().equals(other.getHeight()))
            && (this.getWidth() == null ? other.getWidth() == null : this.getWidth().equals(other.getWidth()))
            && (this.getLength() == null ? other.getLength() == null : this.getLength().equals(other.getLength()))
            && (this.getNumInPackage() == null ? other.getNumInPackage() == null : this.getNumInPackage().equals(other.getNumInPackage()))
            && (this.getWeight() == null ? other.getWeight() == null : this.getWeight().equals(other.getWeight()))
            && (this.getSourceSkuCode() == null ? other.getSourceSkuCode() == null : this.getSourceSkuCode().equals(other.getSourceSkuCode()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getProductId() == null) ? 0 : getProductId().hashCode());
        result = prime * result + ((getSpec() == null) ? 0 : getSpec().hashCode());
        result = prime * result + ((getMarketPrice() == null) ? 0 : getMarketPrice().hashCode());
        result = prime * result + ((getPrice() == null) ? 0 : getPrice().hashCode());
        result = prime * result + ((getOriginalPrice() == null) ? 0 : getOriginalPrice().hashCode());
        result = prime * result + ((getAmount() == null) ? 0 : getAmount().hashCode());
        result = prime * result + ((getSkuOrder() == null) ? 0 : getSkuOrder().hashCode());
        result = prime * result + ((getArchive() == null) ? 0 : getArchive().hashCode());
        result = prime * result + ((getSpec1() == null) ? 0 : getSpec1().hashCode());
        result = prime * result + ((getSpec2() == null) ? 0 : getSpec2().hashCode());
        result = prime * result + ((getSpec3() == null) ? 0 : getSpec3().hashCode());
        result = prime * result + ((getSpec4() == null) ? 0 : getSpec4().hashCode());
        result = prime * result + ((getSpec5() == null) ? 0 : getSpec5().hashCode());
        result = prime * result + ((getCreatedAt() == null) ? 0 : getCreatedAt().hashCode());
        result = prime * result + ((getUpdatedAt() == null) ? 0 : getUpdatedAt().hashCode());
        result = prime * result + ((getPartnerProductId() == null) ? 0 : getPartnerProductId().hashCode());
        result = prime * result + ((getInActivity() == null) ? 0 : getInActivity().hashCode());
        result = prime * result + ((getCode() == null) ? 0 : getCode().hashCode());
        result = prime * result + ((getThirdSkuId() == null) ? 0 : getThirdSkuId().hashCode());
        result = prime * result + ((getSkuCode() == null) ? 0 : getSkuCode().hashCode());
        result = prime * result + ((getSkuCodeResources() == null) ? 0 : getSkuCodeResources().hashCode());
        result = prime * result + ((getSourceSkuId() == null) ? 0 : getSourceSkuId().hashCode());
        result = prime * result + ((getAttributes() == null) ? 0 : getAttributes().hashCode());
        result = prime * result + ((getSecureAmount() == null) ? 0 : getSecureAmount().hashCode());
        result = prime * result + ((getPoint() == null) ? 0 : getPoint().hashCode());
        result = prime * result + ((getDeductionDPoint() == null) ? 0 : getDeductionDPoint().hashCode());
        result = prime * result + ((getNetWorth() == null) ? 0 : getNetWorth().hashCode());
        result = prime * result + ((getBarCode() == null) ? 0 : getBarCode().hashCode());
        result = prime * result + ((getPartnerProductPrice() == null) ? 0 : getPartnerProductPrice().hashCode());
        result = prime * result + ((getPromoAmt() == null) ? 0 : getPromoAmt().hashCode());
        result = prime * result + ((getServerAmt() == null) ? 0 : getServerAmt().hashCode());
        result = prime * result + ((getSkuImg() == null) ? 0 : getSkuImg().hashCode());
        result = prime * result + ((getHeight() == null) ? 0 : getHeight().hashCode());
        result = prime * result + ((getWidth() == null) ? 0 : getWidth().hashCode());
        result = prime * result + ((getLength() == null) ? 0 : getLength().hashCode());
        result = prime * result + ((getNumInPackage() == null) ? 0 : getNumInPackage().hashCode());
        result = prime * result + ((getWeight() == null) ? 0 : getWeight().hashCode());
        result = prime * result + ((getSourceSkuCode() == null) ? 0 : getSourceSkuCode().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", productId=").append(productId);
        sb.append(", spec=").append(spec);
        sb.append(", marketPrice=").append(marketPrice);
        sb.append(", price=").append(price);
        sb.append(", originalPrice=").append(originalPrice);
        sb.append(", amount=").append(amount);
        sb.append(", skuOrder=").append(skuOrder);
        sb.append(", archive=").append(archive);
        sb.append(", spec1=").append(spec1);
        sb.append(", spec2=").append(spec2);
        sb.append(", spec3=").append(spec3);
        sb.append(", spec4=").append(spec4);
        sb.append(", spec5=").append(spec5);
        sb.append(", createdAt=").append(createdAt);
        sb.append(", updatedAt=").append(updatedAt);
        sb.append(", partnerProductId=").append(partnerProductId);
        sb.append(", inActivity=").append(inActivity);
        sb.append(", code=").append(code);
        sb.append(", thirdSkuId=").append(thirdSkuId);
        sb.append(", skuCode=").append(skuCode);
        sb.append(", skuCodeResources=").append(skuCodeResources);
        sb.append(", sourceSkuId=").append(sourceSkuId);
        sb.append(", attributes=").append(attributes);
        sb.append(", secureAmount=").append(secureAmount);
        sb.append(", point=").append(point);
        sb.append(", deductionDPoint=").append(deductionDPoint);
        sb.append(", netWorth=").append(netWorth);
        sb.append(", barCode=").append(barCode);
        sb.append(", partnerProductPrice=").append(partnerProductPrice);
        sb.append(", promoAmt=").append(promoAmt);
        sb.append(", serverAmt=").append(serverAmt);
        sb.append(", skuImg=").append(skuImg);
        sb.append(", height=").append(height);
        sb.append(", width=").append(width);
        sb.append(", length=").append(length);
        sb.append(", numInPackage=").append(numInPackage);
        sb.append(", weight=").append(weight);
        sb.append(", sourceSkuCode=").append(sourceSkuCode);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}