package cn.hds.hvmall.pojo.freshmantab;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: WangXiao
 * Date: 2019/5/9
 * Time: 14:21
 * Description: 属于套装单品表
 */
public class XquarkSkuCombineExtra {

    private Long id;

    private Long masterId;

    private Long slaveId;

    private Long slaveProductId;

    private Integer amount;

    private Date createdAt;

    private Date updatedAt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMasterId() {
        return masterId;
    }

    public void setMasterId(Long masterId) {
        this.masterId = masterId;
    }

    public Long getSlaveId() {
        return slaveId;
    }

    public void setSlaveId(Long slaveId) {
        this.slaveId = slaveId;
    }

    public Long getSlaveProductId() {
        return slaveProductId;
    }

    public void setSlaveProductId(Long slaveProductId) {
        this.slaveProductId = slaveProductId;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}