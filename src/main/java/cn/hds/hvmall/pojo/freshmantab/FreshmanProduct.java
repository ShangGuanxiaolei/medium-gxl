package cn.hds.hvmall.pojo.freshmantab;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class FreshmanProduct {
    private Long id;

    private Long tabId;

    private String productId;

    private String fpSkuCode;

    private Long sourceId;

    private String productName;

    private String freshSkuCode;//将SKUCODE  xquarkfreshmanProduct表中

    private Integer stock;

    private BigDecimal price;//价格

    private BigDecimal conversionPrice;//兑换价

    private BigDecimal freshprice;//划线价格

    private BigDecimal exchangePrice;

    private Integer exchangePoint;

    private Integer status;

    private Date createdAt;

    private Date updatedAt;

    private int count;//mybatis判断Id是否存在

    private List<FreshmanProductSku> freshmanProductSkus;

    public String getFpSkuCode() {
        return fpSkuCode;
    }

    public void setFpSkuCode(String fpSkuCode) {
        this.fpSkuCode = fpSkuCode;
    }

    public Long getSourceId() {
        return sourceId;
    }

    public void setSourceId(Long sourceId) {
        this.sourceId = sourceId;
    }

    public String getFreshSkuCode() {
        return freshSkuCode;
    }

    public void setFreshSkuCode(String freshSkuCode) {
        this.freshSkuCode = freshSkuCode;
    }

    public List<FreshmanProductSku> getFreshmanProductSkus() {
        return freshmanProductSkus;
    }

    public void setFreshmanProductSkus(List<FreshmanProductSku> freshmanProductSkus) {
        this.freshmanProductSkus = freshmanProductSkus;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTabId() {
        return tabId;
    }

    public void setTabId(Long tabId) {
        this.tabId = tabId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId == null ? null : productId.trim();
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName == null ? null : productName.trim();
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getExchangePrice() {
        return exchangePrice;
    }

    public void setExchangePrice(BigDecimal exchangePrice) {
        this.exchangePrice = exchangePrice;
    }

    public Integer getExchangePoint() {
        return exchangePoint;
    }

    public void setExchangePoint(Integer exchangePoint) {
        this.exchangePoint = exchangePoint;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public BigDecimal getConversionPrice() {
        return conversionPrice;
    }

    public void setConversionPrice(BigDecimal conversionPrice) {
        this.conversionPrice = conversionPrice;
    }

    public BigDecimal getFreshprice() {
        return freshprice;
    }

    public void setFreshprice(BigDecimal freshprice) {
        this.freshprice = freshprice;
    }
}