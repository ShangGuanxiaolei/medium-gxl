package cn.hds.hvmall.pojo.freshmantab;

import java.io.Serializable;

/**
 * xquark_product
 * @author 
 */
public class XquarkProductWithBLOBs extends XquarkProduct implements Serializable {
    /**
     * 商品描述
     */
    private String description;

    /**
     * 商品的h5详情
     */
    private String detailH5;

    private static final long serialVersionUID = 1L;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDetailH5() {
        return detailH5;
    }

    public void setDetailH5(String detailH5) {
        this.detailH5 = detailH5;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        XquarkProductWithBLOBs other = (XquarkProductWithBLOBs) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getCode() == null ? other.getCode() == null : this.getCode().equals(other.getCode()))
            && (this.getEncode() == null ? other.getEncode() == null : this.getEncode().equals(other.getEncode()))
            && (this.getU8Encode() == null ? other.getU8Encode() == null : this.getU8Encode().equals(other.getU8Encode()))
            && (this.getModel() == null ? other.getModel() == null : this.getModel().equals(other.getModel()))
            && (this.getType() == null ? other.getType() == null : this.getType().equals(other.getType()))
            && (this.getLevel() == null ? other.getLevel() == null : this.getLevel().equals(other.getLevel()))
            && (this.getPriority() == null ? other.getPriority() == null : this.getPriority().equals(other.getPriority()))
            && (this.getFilterSpec() == null ? other.getFilterSpec() == null : this.getFilterSpec().equals(other.getFilterSpec()))
            && (this.getUserId() == null ? other.getUserId() == null : this.getUserId().equals(other.getUserId()))
            && (this.getShopId() == null ? other.getShopId() == null : this.getShopId().equals(other.getShopId()))
            && (this.getName() == null ? other.getName() == null : this.getName().equals(other.getName()))
            && (this.getImg() == null ? other.getImg() == null : this.getImg().equals(other.getImg()))
            && (this.getImgWidth() == null ? other.getImgWidth() == null : this.getImgWidth().equals(other.getImgWidth()))
            && (this.getImgHeight() == null ? other.getImgHeight() == null : this.getImgHeight().equals(other.getImgHeight()))
            && (this.getDescImg() == null ? other.getDescImg() == null : this.getDescImg().equals(other.getDescImg()))
            && (this.getStatus() == null ? other.getStatus() == null : this.getStatus().equals(other.getStatus()))
            && (this.getMarketPrice() == null ? other.getMarketPrice() == null : this.getMarketPrice().equals(other.getMarketPrice()))
            && (this.getPrice() == null ? other.getPrice() == null : this.getPrice().equals(other.getPrice()))
            && (this.getOriginalPrice() == null ? other.getOriginalPrice() == null : this.getOriginalPrice().equals(other.getOriginalPrice()))
            && (this.getAmount() == null ? other.getAmount() == null : this.getAmount().equals(other.getAmount()))
            && (this.getSales() == null ? other.getSales() == null : this.getSales().equals(other.getSales()))
            && (this.getArchive() == null ? other.getArchive() == null : this.getArchive().equals(other.getArchive()))
            && (this.getRecommend() == null ? other.getRecommend() == null : this.getRecommend().equals(other.getRecommend()))
            && (this.getRecommendAt() == null ? other.getRecommendAt() == null : this.getRecommendAt().equals(other.getRecommendAt()))
            && (this.getDiscount() == null ? other.getDiscount() == null : this.getDiscount().equals(other.getDiscount()))
            && (this.getIsCommission() == null ? other.getIsCommission() == null : this.getIsCommission().equals(other.getIsCommission()))
            && (this.getCommissionRate() == null ? other.getCommissionRate() == null : this.getCommissionRate().equals(other.getCommissionRate()))
            && (this.getForsaleAt() == null ? other.getForsaleAt() == null : this.getForsaleAt().equals(other.getForsaleAt()))
            && (this.getOnsaleAt() == null ? other.getOnsaleAt() == null : this.getOnsaleAt().equals(other.getOnsaleAt()))
            && (this.getInstockAt() == null ? other.getInstockAt() == null : this.getInstockAt().equals(other.getInstockAt()))
            && (this.getCreatedAt() == null ? other.getCreatedAt() == null : this.getCreatedAt().equals(other.getCreatedAt()))
            && (this.getUpdatedAt() == null ? other.getUpdatedAt() == null : this.getUpdatedAt().equals(other.getUpdatedAt()))
            && (this.getFakeSales() == null ? other.getFakeSales() == null : this.getFakeSales().equals(other.getFakeSales()))
            && (this.getIsdelay() == null ? other.getIsdelay() == null : this.getIsdelay().equals(other.getIsdelay()))
            && (this.getDelaydays() == null ? other.getDelaydays() == null : this.getDelaydays().equals(other.getDelaydays()))
            && (this.getUpdateLock() == null ? other.getUpdateLock() == null : this.getUpdateLock().equals(other.getUpdateLock()))
            && (this.getSynchronousflag() == null ? other.getSynchronousflag() == null : this.getSynchronousflag().equals(other.getSynchronousflag()))
            && (this.getThirdItemId() == null ? other.getThirdItemId() == null : this.getThirdItemId().equals(other.getThirdItemId()))
            && (this.getPartnerProductId() == null ? other.getPartnerProductId() == null : this.getPartnerProductId().equals(other.getPartnerProductId()))
            && (this.getSource() == null ? other.getSource() == null : this.getSource().equals(other.getSource()))
            && (this.getInActivity() == null ? other.getInActivity() == null : this.getInActivity().equals(other.getInActivity()))
            && (this.getIsCrossBorder() == null ? other.getIsCrossBorder() == null : this.getIsCrossBorder().equals(other.getIsCrossBorder()))
            && (this.getEnableDesc() == null ? other.getEnableDesc() == null : this.getEnableDesc().equals(other.getEnableDesc()))
            && (this.getIsDistribution() == null ? other.getIsDistribution() == null : this.getIsDistribution().equals(other.getIsDistribution()))
            && (this.getSourceProductId() == null ? other.getSourceProductId() == null : this.getSourceProductId().equals(other.getSourceProductId()))
            && (this.getSourceShopId() == null ? other.getSourceShopId() == null : this.getSourceShopId().equals(other.getSourceShopId()))
            && (this.getFeature0() == null ? other.getFeature0() == null : this.getFeature0().equals(other.getFeature0()))
            && (this.getFeature1() == null ? other.getFeature1() == null : this.getFeature1().equals(other.getFeature1()))
            && (this.getOneyuanPostage() == null ? other.getOneyuanPostage() == null : this.getOneyuanPostage().equals(other.getOneyuanPostage()))
            && (this.getOneyuanPurchase() == null ? other.getOneyuanPurchase() == null : this.getOneyuanPurchase().equals(other.getOneyuanPurchase()))
            && (this.getSpecialrate() == null ? other.getSpecialrate() == null : this.getSpecialrate().equals(other.getSpecialrate()))
            && (this.getIsGroupon() == null ? other.getIsGroupon() == null : this.getIsGroupon().equals(other.getIsGroupon()))
            && (this.getRefundAddress() == null ? other.getRefundAddress() == null : this.getRefundAddress().equals(other.getRefundAddress()))
            && (this.getRefundTel() == null ? other.getRefundTel() == null : this.getRefundTel().equals(other.getRefundTel()))
            && (this.getRefundName() == null ? other.getRefundName() == null : this.getRefundName().equals(other.getRefundName()))
            && (this.getLogisticsType() == null ? other.getLogisticsType() == null : this.getLogisticsType().equals(other.getLogisticsType()))
            && (this.getUniformValue() == null ? other.getUniformValue() == null : this.getUniformValue().equals(other.getUniformValue()))
            && (this.getTemplateValue() == null ? other.getTemplateValue() == null : this.getTemplateValue().equals(other.getTemplateValue()))
            && (this.getGift() == null ? other.getGift() == null : this.getGift().equals(other.getGift()))
            && (this.getWeight() == null ? other.getWeight() == null : this.getWeight().equals(other.getWeight()))
            && (this.getAttributes() == null ? other.getAttributes() == null : this.getAttributes().equals(other.getAttributes()))
            && (this.getYundouScale() == null ? other.getYundouScale() == null : this.getYundouScale().equals(other.getYundouScale()))
            && (this.getMinYundouPrice() == null ? other.getMinYundouPrice() == null : this.getMinYundouPrice().equals(other.getMinYundouPrice()))
            && (this.getAvailableStatus() == null ? other.getAvailableStatus() == null : this.getAvailableStatus().equals(other.getAvailableStatus()))
            && (this.getReviewStatus() == null ? other.getReviewStatus() == null : this.getReviewStatus().equals(other.getReviewStatus()))
            && (this.getHeight() == null ? other.getHeight() == null : this.getHeight().equals(other.getHeight()))
            && (this.getWidth() == null ? other.getWidth() == null : this.getWidth().equals(other.getWidth()))
            && (this.getLength() == null ? other.getLength() == null : this.getLength().equals(other.getLength()))
            && (this.getKind() == null ? other.getKind() == null : this.getKind().equals(other.getKind()))
            && (this.getPoint() == null ? other.getPoint() == null : this.getPoint().equals(other.getPoint()))
            && (this.getDeductionDPoint() == null ? other.getDeductionDPoint() == null : this.getDeductionDPoint().equals(other.getDeductionDPoint()))
            && (this.getSupportRefund() == null ? other.getSupportRefund() == null : this.getSupportRefund().equals(other.getSupportRefund()))
            && (this.getNumInPackage() == null ? other.getNumInPackage() == null : this.getNumInPackage().equals(other.getNumInPackage()))
            && (this.getBarCode() == null ? other.getBarCode() == null : this.getBarCode().equals(other.getBarCode()))
            && (this.getWareHouseId() == null ? other.getWareHouseId() == null : this.getWareHouseId().equals(other.getWareHouseId()))
            && (this.getPackageId() == null ? other.getPackageId() == null : this.getPackageId().equals(other.getPackageId()))
            && (this.getOuterId() == null ? other.getOuterId() == null : this.getOuterId().equals(other.getOuterId()))
            && (this.getNum() == null ? other.getNum() == null : this.getNum().equals(other.getNum()))
            && (this.getWhseCode() == null ? other.getWhseCode() == null : this.getWhseCode().equals(other.getWhseCode()))
            && (this.getSkuId() == null ? other.getSkuId() == null : this.getSkuId().equals(other.getSkuId()))
            && (this.getSkuOuterId() == null ? other.getSkuOuterId() == null : this.getSkuOuterId().equals(other.getSkuOuterId()))
            && (this.getSkuPrice() == null ? other.getSkuPrice() == null : this.getSkuPrice().equals(other.getSkuPrice()))
            && (this.getSkuQuantity() == null ? other.getSkuQuantity() == null : this.getSkuQuantity().equals(other.getSkuQuantity()))
            && (this.getSkuName() == null ? other.getSkuName() == null : this.getSkuName().equals(other.getSkuName()))
            && (this.getSkuProperty() == null ? other.getSkuProperty() == null : this.getSkuProperty().equals(other.getSkuProperty()))
            && (this.getSkuPictureUrl() == null ? other.getSkuPictureUrl() == null : this.getSkuPictureUrl().equals(other.getSkuPictureUrl()))
            && (this.getNetWorth() == null ? other.getNetWorth() == null : this.getNetWorth().equals(other.getNetWorth()))
            && (this.getSupplierId() == null ? other.getSupplierId() == null : this.getSupplierId().equals(other.getSupplierId()))
            && (this.getSfairline() == null ? other.getSfairline() == null : this.getSfairline().equals(other.getSfairline()))
            && (this.getSfshipping() == null ? other.getSfshipping() == null : this.getSfshipping().equals(other.getSfshipping()))
            && (this.getMerchantNumber() == null ? other.getMerchantNumber() == null : this.getMerchantNumber().equals(other.getMerchantNumber()))
            && (this.getPromoAmt() == null ? other.getPromoAmt() == null : this.getPromoAmt().equals(other.getPromoAmt()))
            && (this.getServerAmt() == null ? other.getServerAmt() == null : this.getServerAmt().equals(other.getServerAmt()))
            && (this.getDeliveryRegion() == null ? other.getDeliveryRegion() == null : this.getDeliveryRegion().equals(other.getDeliveryRegion()))
            && (this.getSelfOperated() == null ? other.getSelfOperated() == null : this.getSelfOperated().equals(other.getSelfOperated()))
            && (this.getSourceId() == null ? other.getSourceId() == null : this.getSourceId().equals(other.getSourceId()))
            && (this.getDescription() == null ? other.getDescription() == null : this.getDescription().equals(other.getDescription()))
            && (this.getDetailH5() == null ? other.getDetailH5() == null : this.getDetailH5().equals(other.getDetailH5()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getCode() == null) ? 0 : getCode().hashCode());
        result = prime * result + ((getEncode() == null) ? 0 : getEncode().hashCode());
        result = prime * result + ((getU8Encode() == null) ? 0 : getU8Encode().hashCode());
        result = prime * result + ((getModel() == null) ? 0 : getModel().hashCode());
        result = prime * result + ((getType() == null) ? 0 : getType().hashCode());
        result = prime * result + ((getLevel() == null) ? 0 : getLevel().hashCode());
        result = prime * result + ((getPriority() == null) ? 0 : getPriority().hashCode());
        result = prime * result + ((getFilterSpec() == null) ? 0 : getFilterSpec().hashCode());
        result = prime * result + ((getUserId() == null) ? 0 : getUserId().hashCode());
        result = prime * result + ((getShopId() == null) ? 0 : getShopId().hashCode());
        result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
        result = prime * result + ((getImg() == null) ? 0 : getImg().hashCode());
        result = prime * result + ((getImgWidth() == null) ? 0 : getImgWidth().hashCode());
        result = prime * result + ((getImgHeight() == null) ? 0 : getImgHeight().hashCode());
        result = prime * result + ((getDescImg() == null) ? 0 : getDescImg().hashCode());
        result = prime * result + ((getStatus() == null) ? 0 : getStatus().hashCode());
        result = prime * result + ((getMarketPrice() == null) ? 0 : getMarketPrice().hashCode());
        result = prime * result + ((getPrice() == null) ? 0 : getPrice().hashCode());
        result = prime * result + ((getOriginalPrice() == null) ? 0 : getOriginalPrice().hashCode());
        result = prime * result + ((getAmount() == null) ? 0 : getAmount().hashCode());
        result = prime * result + ((getSales() == null) ? 0 : getSales().hashCode());
        result = prime * result + ((getArchive() == null) ? 0 : getArchive().hashCode());
        result = prime * result + ((getRecommend() == null) ? 0 : getRecommend().hashCode());
        result = prime * result + ((getRecommendAt() == null) ? 0 : getRecommendAt().hashCode());
        result = prime * result + ((getDiscount() == null) ? 0 : getDiscount().hashCode());
        result = prime * result + ((getIsCommission() == null) ? 0 : getIsCommission().hashCode());
        result = prime * result + ((getCommissionRate() == null) ? 0 : getCommissionRate().hashCode());
        result = prime * result + ((getForsaleAt() == null) ? 0 : getForsaleAt().hashCode());
        result = prime * result + ((getOnsaleAt() == null) ? 0 : getOnsaleAt().hashCode());
        result = prime * result + ((getInstockAt() == null) ? 0 : getInstockAt().hashCode());
        result = prime * result + ((getCreatedAt() == null) ? 0 : getCreatedAt().hashCode());
        result = prime * result + ((getUpdatedAt() == null) ? 0 : getUpdatedAt().hashCode());
        result = prime * result + ((getFakeSales() == null) ? 0 : getFakeSales().hashCode());
        result = prime * result + ((getIsdelay() == null) ? 0 : getIsdelay().hashCode());
        result = prime * result + ((getDelaydays() == null) ? 0 : getDelaydays().hashCode());
        result = prime * result + ((getUpdateLock() == null) ? 0 : getUpdateLock().hashCode());
        result = prime * result + ((getSynchronousflag() == null) ? 0 : getSynchronousflag().hashCode());
        result = prime * result + ((getThirdItemId() == null) ? 0 : getThirdItemId().hashCode());
        result = prime * result + ((getPartnerProductId() == null) ? 0 : getPartnerProductId().hashCode());
        result = prime * result + ((getSource() == null) ? 0 : getSource().hashCode());
        result = prime * result + ((getInActivity() == null) ? 0 : getInActivity().hashCode());
        result = prime * result + ((getIsCrossBorder() == null) ? 0 : getIsCrossBorder().hashCode());
        result = prime * result + ((getEnableDesc() == null) ? 0 : getEnableDesc().hashCode());
        result = prime * result + ((getIsDistribution() == null) ? 0 : getIsDistribution().hashCode());
        result = prime * result + ((getSourceProductId() == null) ? 0 : getSourceProductId().hashCode());
        result = prime * result + ((getSourceShopId() == null) ? 0 : getSourceShopId().hashCode());
        result = prime * result + ((getFeature0() == null) ? 0 : getFeature0().hashCode());
        result = prime * result + ((getFeature1() == null) ? 0 : getFeature1().hashCode());
        result = prime * result + ((getOneyuanPostage() == null) ? 0 : getOneyuanPostage().hashCode());
        result = prime * result + ((getOneyuanPurchase() == null) ? 0 : getOneyuanPurchase().hashCode());
        result = prime * result + ((getSpecialrate() == null) ? 0 : getSpecialrate().hashCode());
        result = prime * result + ((getIsGroupon() == null) ? 0 : getIsGroupon().hashCode());
        result = prime * result + ((getRefundAddress() == null) ? 0 : getRefundAddress().hashCode());
        result = prime * result + ((getRefundTel() == null) ? 0 : getRefundTel().hashCode());
        result = prime * result + ((getRefundName() == null) ? 0 : getRefundName().hashCode());
        result = prime * result + ((getLogisticsType() == null) ? 0 : getLogisticsType().hashCode());
        result = prime * result + ((getUniformValue() == null) ? 0 : getUniformValue().hashCode());
        result = prime * result + ((getTemplateValue() == null) ? 0 : getTemplateValue().hashCode());
        result = prime * result + ((getGift() == null) ? 0 : getGift().hashCode());
        result = prime * result + ((getWeight() == null) ? 0 : getWeight().hashCode());
        result = prime * result + ((getAttributes() == null) ? 0 : getAttributes().hashCode());
        result = prime * result + ((getYundouScale() == null) ? 0 : getYundouScale().hashCode());
        result = prime * result + ((getMinYundouPrice() == null) ? 0 : getMinYundouPrice().hashCode());
        result = prime * result + ((getAvailableStatus() == null) ? 0 : getAvailableStatus().hashCode());
        result = prime * result + ((getReviewStatus() == null) ? 0 : getReviewStatus().hashCode());
        result = prime * result + ((getHeight() == null) ? 0 : getHeight().hashCode());
        result = prime * result + ((getWidth() == null) ? 0 : getWidth().hashCode());
        result = prime * result + ((getLength() == null) ? 0 : getLength().hashCode());
        result = prime * result + ((getKind() == null) ? 0 : getKind().hashCode());
        result = prime * result + ((getPoint() == null) ? 0 : getPoint().hashCode());
        result = prime * result + ((getDeductionDPoint() == null) ? 0 : getDeductionDPoint().hashCode());
        result = prime * result + ((getSupportRefund() == null) ? 0 : getSupportRefund().hashCode());
        result = prime * result + ((getNumInPackage() == null) ? 0 : getNumInPackage().hashCode());
        result = prime * result + ((getBarCode() == null) ? 0 : getBarCode().hashCode());
        result = prime * result + ((getWareHouseId() == null) ? 0 : getWareHouseId().hashCode());
        result = prime * result + ((getPackageId() == null) ? 0 : getPackageId().hashCode());
        result = prime * result + ((getOuterId() == null) ? 0 : getOuterId().hashCode());
        result = prime * result + ((getNum() == null) ? 0 : getNum().hashCode());
        result = prime * result + ((getWhseCode() == null) ? 0 : getWhseCode().hashCode());
        result = prime * result + ((getSkuId() == null) ? 0 : getSkuId().hashCode());
        result = prime * result + ((getSkuOuterId() == null) ? 0 : getSkuOuterId().hashCode());
        result = prime * result + ((getSkuPrice() == null) ? 0 : getSkuPrice().hashCode());
        result = prime * result + ((getSkuQuantity() == null) ? 0 : getSkuQuantity().hashCode());
        result = prime * result + ((getSkuName() == null) ? 0 : getSkuName().hashCode());
        result = prime * result + ((getSkuProperty() == null) ? 0 : getSkuProperty().hashCode());
        result = prime * result + ((getSkuPictureUrl() == null) ? 0 : getSkuPictureUrl().hashCode());
        result = prime * result + ((getNetWorth() == null) ? 0 : getNetWorth().hashCode());
        result = prime * result + ((getSupplierId() == null) ? 0 : getSupplierId().hashCode());
        result = prime * result + ((getSfairline() == null) ? 0 : getSfairline().hashCode());
        result = prime * result + ((getSfshipping() == null) ? 0 : getSfshipping().hashCode());
        result = prime * result + ((getMerchantNumber() == null) ? 0 : getMerchantNumber().hashCode());
        result = prime * result + ((getPromoAmt() == null) ? 0 : getPromoAmt().hashCode());
        result = prime * result + ((getServerAmt() == null) ? 0 : getServerAmt().hashCode());
        result = prime * result + ((getDeliveryRegion() == null) ? 0 : getDeliveryRegion().hashCode());
        result = prime * result + ((getSelfOperated() == null) ? 0 : getSelfOperated().hashCode());
        result = prime * result + ((getSourceId() == null) ? 0 : getSourceId().hashCode());
        result = prime * result + ((getDescription() == null) ? 0 : getDescription().hashCode());
        result = prime * result + ((getDetailH5() == null) ? 0 : getDetailH5().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", description=").append(description);
        sb.append(", detailH5=").append(detailH5);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}