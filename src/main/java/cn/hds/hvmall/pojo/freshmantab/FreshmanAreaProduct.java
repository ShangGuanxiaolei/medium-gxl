package cn.hds.hvmall.pojo.freshmantab;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: WangXiao
 * Date: 2019/3/6
 * Time: 10:04
 * Description:
 */
public class FreshmanAreaProduct {

    private String tabId;

    private String name;

    private List<FreshmanProduct> freshmanProducts;//tab子对象

    public String getTabId() {
        return tabId;
    }

    public void setTabId(String tabId) {
        this.tabId = tabId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<FreshmanProduct> getFreshmanProducts() {
        return freshmanProducts;
    }

    public void setFreshmanProducts(List<FreshmanProduct> freshmanProducts) {
        this.freshmanProducts = freshmanProducts;
    }
}