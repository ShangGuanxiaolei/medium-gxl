package cn.hds.hvmall.pojo;


import cn.hds.hvmall.utils.list.GlobalErrorCode;

public class ResponseObject<T> {

  private GlobalErrorCode status = GlobalErrorCode.SUCESS;
  private String moreInfo = "";
  private T data;

  /**
   * 正常返回，有数据
   */
  public ResponseObject(T data) {
    this.data = data;
  }

  public ResponseObject(T data, String moreInfo, GlobalErrorCode status) {
    this.data = data;
    this.moreInfo = moreInfo;
    this.status = status;
  }

  public ResponseObject(T data, GlobalErrorCode status) {
    this.data = data;
    this.status = status;
  }

  /**
   * 正常返回，无数据
   */
  public ResponseObject() {
  }

  /**
   * 错误状态返回
   */
  public ResponseObject(GlobalErrorCode status) {
    this.moreInfo = status.getError();
    this.status = status;
  }

  /**
   * 错误状态返回
   */
  public ResponseObject(String moreInfo, GlobalErrorCode status) {
    this.moreInfo = moreInfo;
    this.status = status;
  }

  public void setMoreInfo(String moreInfo) {
    this.moreInfo = moreInfo;
  }

  public T getData() {
    return data;
  }

  public void setData(T data) {
    this.data = data;
  }

  public int getErrorCode() {
    return status.getErrorCode();
  }

  public String getError() {
    return status.getError();
  }

  public String getMoreInfo() {
    return moreInfo;
  }
}
