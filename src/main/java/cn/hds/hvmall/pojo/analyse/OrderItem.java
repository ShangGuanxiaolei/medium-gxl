package cn.hds.hvmall.pojo.analyse;

import cn.hds.hvmall.type.PromotionType;
import lombok.Data;

import java.math.BigDecimal;

/**
 * 数据统计orderItem
 * @author wangxinhua
 * @date 2019-05-12
 * @since 1.0
 */
@Data
public class OrderItem {

    private BigDecimal price;

    private PromotionType promotionType;

}
