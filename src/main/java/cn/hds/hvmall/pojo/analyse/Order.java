package cn.hds.hvmall.pojo.analyse;

import lombok.Data;
import lombok.NonNull;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.function.Function;

/**
 * 数据统计订单对象
 * @author wangxinhua
 * @date 2019-05-12
 * @since 1.0
 */
@Data
public class Order {

    private final static DateTimeFormatter DAY_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    private String orderNo;

    /**
     *
     */
    private BigDecimal totalFee;

    /**
     * 订单项
     */
    private List<OrderItem> items;

    /**
     * 下单前的最后一次身份变更记录
     */
    private String lastDirection;

    private LocalDateTime createdAt;

    private LocalDateTime paidAt;

    /**
     * 付款时用户的身份
     */
    public boolean getIsNew() {
        return lastDirection == null || lastDirection.startsWith("WEAK");
    }

    /**
     * 所有订单项的总金额
     */
    public BigDecimal getTotalFee() {
        return items.stream().map(OrderItem::getPrice)
                .reduce(BigDecimal::add)
                .orElse(BigDecimal.ZERO);
    }

    public String getDayOf(@NonNull Function<Order, LocalDateTime> func) {
        final LocalDateTime date = func.apply(this);
        if (date == null) {
            throw new IllegalStateException("apply date is null");
        }
        return date.format(DAY_FORMATTER);
    }


}
