package cn.hds.hvmall.pojo.activity;
import java.sql.Timestamp;
import java.util.Date;
/**
 * @author liuyandong
 * @date 2018/10/31 14:04
 */
public class PtDetail {
    /**
     * 拼团编码
     */
    private String productName;
    private String groupCode;
    /**
     * 拼团会员id
     */
    private int memberId;
    private String memberName;
    private String orderNo;
    private String pizNo;
    private Timestamp ptSuccessTime;

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public int getMemberId() {
        return memberId;
    }

    public void setMemberId(int memberId) {
        this.memberId = memberId;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getPizNo() {
        return pizNo;
    }

    public void setPizNo(String pizNo) {
        this.pizNo = pizNo;
    }

    public Timestamp getPtSuccessTime() {
        return ptSuccessTime;
    }

    public void setPtSuccessTime(Timestamp ptSuccessTime) {
        this.ptSuccessTime = ptSuccessTime;
    }
}
