package cn.hds.hvmall.pojo.activity;

import java.sql.Timestamp;
import java.util.Date;

/**
 * 封装查询结果
 *
 * @Author: zzl
 * @Date: 2018/9/19 10:38
 * @Version
 */
public class ActivityVO {

  /**
   * 拼团编码
   */
  private String pieceGroupTranCode;

  public String getPieceStatus() {
    return pieceStatus;
  }

  public void setPieceStatus(String pieceStatus) {
    this.pieceStatus = pieceStatus;
  }

  /*
    拼团状态
    *
    * */
 private String pieceStatus;
  /**
   * 商品名称
   */
  private String productName;

  /**
   * 开团人CPID
   */
  private int groupOpenMemberId;

  /**
   * 开团人昵称
   */
  private String groupOpenMemberNickName;

  /**
   * 团长CPID
   */
  private int groupHeadId;
  private String groupHeadIdName;


  /**
   * 成团人数
   */
  private String pieceGroupNum;

  /**
   * 开团时间
   */
  private Timestamp groupOpenTime;

  /**
   * 成团时间
   * promotion_pg_info中的updated_at
   * 先看组团状态,只有是已成团时才能用该时间,未成团时成团时间给空
   *
   */
  private Timestamp promotionPgInfoUpdatedAt;




  public String getPieceGroupTranCode() {
    return pieceGroupTranCode;
  }

  public void setPieceGroupTranCode(String pieceGroupTranCode) {
    this.pieceGroupTranCode = pieceGroupTranCode;
  }

  public String getProductName() {
    return productName;
  }

  public void setProductName(String productName) {
    this.productName = productName;
  }

  public int getGroupOpenMemberId() {
    return groupOpenMemberId;
  }

  public void setGroupOpenMemberId(int groupOpenMemberId) {
    this.groupOpenMemberId = groupOpenMemberId;
  }

  public String getGroupOpenMemberNickName() {
    return groupOpenMemberNickName;
  }

  public void setGroupOpenMemberNickName(String groupOpenMemberNickName) {
    this.groupOpenMemberNickName = groupOpenMemberNickName;
  }

  public int getGroupHeadId() {
    return groupHeadId;
  }

  public void setGroupHeadId(int groupHeadId) {
    this.groupHeadId = groupHeadId;
  }



  public String getPieceGroupNum() {
    return pieceGroupNum;
  }

  public void setPieceGroupNum(String pieceGroupNum) {
    this.pieceGroupNum = pieceGroupNum;
  }

  public Date getGroupOpenTime() {
    return groupOpenTime;
  }

  public void setGroupOpenTime(Timestamp groupOpenTime) {
    this.groupOpenTime = (Timestamp) groupOpenTime;
  }

  public Date getPromotionPgInfoUpdatedAt() {
    return promotionPgInfoUpdatedAt;
  }

  public void setPromotionPgInfoUpdatedAt(Timestamp promotionPgInfoUpdatedAt) {
    this.promotionPgInfoUpdatedAt = (Timestamp) promotionPgInfoUpdatedAt;
  }

  public void setGroupHeadIdName(String groupHeadIdName) {
    this.groupHeadIdName = groupHeadIdName;
  }

  public String getGroupHeadIdName() {
    return groupHeadIdName;
  }
}
