package cn.hds.hvmall.pojo.activity;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.sql.Timestamp;
import java.util.Date;

/**
 * 封装入参参数
 *
 * @Author: zzl
 * @Date: 2018/9/19 13:42
 * @Version
 */
public class ActivityInfo {

  /**
   * 活动状态
   */
  private int pStatus;

  /**
   * 开团时间
   */
  private Date openTime;

  /**
   * 成团时间
   */
  private Date DoneTime;

  /**
   * 拼团编码
   */
  private String groupCode;

  /**
   * 拼团会员id
   */
  private String memberId;

  /**
   * 分页页码
   *
   */
  private int pageNum;

  public int getPageNum() {
    return pageNum;
  }

  public void setPageNum(int pageNum) {
    this.pageNum = pageNum;
  }

  public int getpStatus() {
    return pStatus;
  }

  public void setpStatus(int pStatus) {
    this.pStatus = pStatus;
  }

  public Date getOpenTime() {
    return openTime;
  }

  public void setOpenTime(Timestamp openTime) {
    this.openTime = openTime;
  }

  public Date getDoneTime() {
    return DoneTime;
  }

  public void setDoneTime(Date doneTime) {
    DoneTime = doneTime;
  }

  public String getGroupCode() {
    return groupCode;
  }

  public void setGroupCode(String groupCode) {
    this.groupCode = groupCode;
  }

  public String getMemberId() {
    return memberId;
  }

  public void setMemberId(String memberId) {
    this.memberId = memberId;
  }

  @Override
  public String toString() {
    return "ActivityInfo{" +
        "pStatus='" + pStatus + '\'' +
        ", openTime=" + openTime +
        ", DoneTime=" + DoneTime +
        ", groupCode='" + groupCode + '\'' +
        ", memberId='" + memberId + '\'' +
        ", pageNum=" + pageNum +
        '}';
  }
}
