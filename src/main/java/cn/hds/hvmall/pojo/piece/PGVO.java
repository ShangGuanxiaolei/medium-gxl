package cn.hds.hvmall.pojo.piece;

import java.util.List;
import java.util.Map;

/**
 * 后台活动创建视图实体inner
 * @author qiuchuyi
 * @date 2018/9/3 14:50
 */
public class PGVO {
    //先从需求页面基本字段开始
    //增加pCode属性,用于判断同一商品的多个活动是否有时间重叠
    private String pCode;
    //是否包邮
    private Integer isDeliveryReduction;
    //活动名称
    private String pName;
    //活动时间
    private String pGTime;
    //成团有效时间
    private Integer pEffectTime;
    //成团后允许插队的时间
    private Integer jumpQueueTime;
    //组团资格
    private String orgGroupQua;
    //参团资格,
    private String joinGroupQua;
    //成团奖励参数
    private List<PartSpec> partSpecs;
    //每次限购
    private Integer skuLimit;
    //多规格商品列表
    private List<PgSku> pgSkus;
    //商品信息
    private List<Map<String,Object>> pgProduct;
    //每人限购
    private Integer BuyLimit;

    public Integer getBuyLimit() {
        return BuyLimit;
    }

    public void setBuyLimit(Integer buyLimit) {
        BuyLimit = buyLimit;
    }

    public String getpName() {
        return pName;
    }

    public void setpName(String pName) {
        this.pName = pName;
    }

    public String getpGTime() {
        return pGTime;
    }

    public void setpGTime(String pGTime) {
        this.pGTime = pGTime;
    }

    public Integer getpEffectTime() {
        return pEffectTime;
    }

    public void setpEffectTime(Integer pEffectTime) {
        this.pEffectTime = pEffectTime;
    }

    public String getOrgGroupQua() {
        return orgGroupQua;
    }

    public void setOrgGroupQua(String orgGroupQua) {
        this.orgGroupQua = orgGroupQua;
    }

    public String getJoinGroupQua() {
        return joinGroupQua;
    }

    public void setJoinGroupQua(String joinGroupQua) {
        this.joinGroupQua = joinGroupQua;
    }

    public List<PartSpec> getPartSpecs() {
        return partSpecs;
    }

    public void setPartSpecs(List<PartSpec> partSpecs) {
        this.partSpecs = partSpecs;
    }

    public Integer getIsDeliveryReduction() {
        return isDeliveryReduction;
    }

    public void setIsDeliveryReduction(Integer isDeliveryReduction) {
        this.isDeliveryReduction = isDeliveryReduction;
    }

    public Integer getSkuLimit() {
        return skuLimit;
    }

    public void setSkuLimit(Integer skuLimit) {
        this.skuLimit = skuLimit;
    }

    public List<PgSku> getPgSkus() {
        return pgSkus;
    }

    public void setPgSkus(List<PgSku> pgSkus) {
        this.pgSkus = pgSkus;
    }

    public String getpCode() {
        return pCode;
    }

    public void setpCode(String pCode) {
        this.pCode = pCode;
    }

    public List<Map<String, Object>> getPgProduct() {
        return pgProduct;
    }

    public void setPgProduct(List<Map<String, Object>> pgProduct) {
        this.pgProduct = pgProduct;
    }

    public Integer getJumpQueueTime() {
        return jumpQueueTime;
    }

    public void setJumpQueueTime(Integer jumpQueueTime) {
        this.jumpQueueTime = jumpQueueTime;
    }
}
