package cn.hds.hvmall.pojo.piece;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 拼团三期调整字段后返回的视图类
 */
public class BackPGThreeVO {
    private String id;
    //活动名称
    private String pName;
    private String pCode;
    //商品id
    private String productId;
    //商品名称
    private String name;
    //售价
    private String price;
    //开始时间
    private Date effectFrom;
    //结束时间
    private Date effectTo;
    //成有效时间
    private Integer pieceEffectTime;
    //状态
    private Integer pStatus;
    //成团人数
    private String pieceGroupNum;
    //拼团价格
    private String promotionPrice;
    //活动库存
    private Integer pSkuNum;
    private int page = 0;// 当前页
    private int pageCount;//总条数
    private int pageSize = 10;// 每页显示记录数
    private int startNo;
    private int totalPage;//总页数

    public String getpName() {
        return pName;
    }

    public void setpName(String pName) {
        this.pName = pName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getpCode() {
        return pCode;
    }

    public void setpCode(String pCode) {
        this.pCode = pCode;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public Date getEffectFrom() {
        return effectFrom;
    }

    public void setEffectFrom(Date effectFrom) {
        this.effectFrom = effectFrom;
    }

    public Date getEffectTo() {
        return effectTo;
    }

    public void setEffectTo(Date effectTo) {
        this.effectTo = effectTo;
    }

    public Integer getPieceEffectTime() {
        return pieceEffectTime;
    }

    public void setPieceEffectTime(Integer pieceEffectTime) {
        this.pieceEffectTime = pieceEffectTime;
    }

    public Integer getpStatus() {
        return pStatus;
    }

    public void setpStatus(Integer pStatus) {
        this.pStatus = pStatus;
    }

    public String getPieceGroupNum() {
        return pieceGroupNum;
    }

    public void setPieceGroupNum(String pieceGroupNum) {
        this.pieceGroupNum = pieceGroupNum;
    }

    public String getPromotionPrice() {
        return promotionPrice;
    }

    public void setPromotionPrice(String promotionPrice) {
        this.promotionPrice = promotionPrice;
    }

    public Integer getpSkuNum() {
        return pSkuNum;
    }

    public void setpSkuNum(Integer pSkuNum) {
        this.pSkuNum = pSkuNum;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageCount() {
        return pageCount;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getStartNo() {
        return startNo;
    }

    public void setStartNo(int startNo) {
        this.startNo = startNo;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

}

