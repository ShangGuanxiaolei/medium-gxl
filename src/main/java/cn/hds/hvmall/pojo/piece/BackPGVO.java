package cn.hds.hvmall.pojo.piece;

import com.alibaba.druid.util.DaemonThreadFactory;
import io.swagger.models.auth.In;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author qiuchuyi
 * 后台活动视图实体outer
 * @date 2018/9/5 9:46
 */
public class BackPGVO {
    //依据测试的要求添加一个字段
    /**
     * promotion_base_info
     */
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    //前端隐藏属性，活动编号
    private String pCode;

    public String getpCode() {
        return pCode;
    }

    public void setpCode(String pCode) {
        this.pCode = pCode;
    }

    //商品id
    private String productId;
    //商品名称
    private String name;
    //售价
    private String price;
    //德分
    private Integer deductionDPoint;
    //折扣
    private String skuDiscount;
    //开始时间
    private Date effectFrom;
    //结束时间
    private Date effectTo;
    //成有效时间
    private Integer pieceEffectTime;
    //拼团价
    private BigDecimal promotionPrice;
    //成团人数
    private String pieceGroupNum;
    //状态
    private Integer pStatus;
    private int page = 0;// 当前页
    private int pageCount;//总条数
    private int pageSize = 10;// 每页显示记录数
    private int startNo;
    private int totalPage;//总页数

    public int getPageCount() {
        return pageCount;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getStartNo() {
        return startNo;
    }

    public void setStartNo(int startNo) {
        this.startNo = startNo;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    public int getPage() {

        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }


    public Integer getDeductionDPoint() {
        return deductionDPoint;
    }

    public void setDeductionDPoint(Integer deductionDPoint) {
        this.deductionDPoint = deductionDPoint;
    }

    public Date getEffectFrom() {
        return effectFrom;
    }

    public void setEffectFrom(Date effectFrom) {
        this.effectFrom = effectFrom;
    }

    public Date getEffectTo() {
        return effectTo;
    }

    public void setEffectTo(Date effectTo) {
        this.effectTo = effectTo;
    }

    public Integer getPieceEffectTime() {
        return pieceEffectTime;
    }

    public void setPieceEffectTime(Integer pieceEffectTime) {
        this.pieceEffectTime = pieceEffectTime;
    }

    public BigDecimal getPromotionPrice() {
        return promotionPrice;
    }

    public void setPromotionPrice(BigDecimal promotionPrice) {
        this.promotionPrice = promotionPrice;
    }

    public String getPieceGroupNum() {
        return pieceGroupNum;
    }

    public void setPieceGroupNum(String pieceGroupNum) {
        this.pieceGroupNum = pieceGroupNum;
    }

    public Integer getpStatus() {
        return pStatus;
    }

    public void setpStatus(Integer pStatus) {
        this.pStatus = pStatus;
    }

    @Override
    public String toString() {
        return "BackPGVO{" +
                "pCode='" + pCode + '\'' +
                ", productId='" + productId + '\'' +
                ", name='" + name + '\'' +
                ", price='" + price + '\'' +
                ", deductionDPoint=" + deductionDPoint +
                ", skuDiscount=" + skuDiscount +
                ", effectFrom=" + effectFrom +
                ", effectTo=" + effectTo +
                ", pieceEffectTime=" + pieceEffectTime +
                ", pieceGroupNum='" + pieceGroupNum + '\'' +
                ", pStatus=" + pStatus +
                '}';
    }

    public String getSkuDiscount() {
        return skuDiscount;
    }

    public void setSkuDiscount(String skuDiscount) {
        this.skuDiscount = skuDiscount;
    }
}
