package cn.hds.hvmall.pojo.piece;

import java.util.Date;

/**
 * @Author: zzl
 * @Date: 2018/9/19 22:17
 * @Version
 */
public class EditVO {

  private String pCode;

  private String productId;

  private String name;

  private double price;

  private double deductionDPoint;

  private double skuDiscount;

  private Date effectFrom;

  private Date effectTo;

  private int pieceEffectTime;

  private int pieceGroupNum;

  private String pStatus;

  public String getpCode() {
    return pCode;
  }

  public void setpCode(String pCode) {
    this.pCode = pCode;
  }

  public String getProductId() {
    return productId;
  }

  public void setProductId(String productId) {
    this.productId = productId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public double getPrice() {
    return price;
  }

  public void setPrice(double price) {
    this.price = price;
  }

  public double getDeductionDPoint() {
    return deductionDPoint;
  }

  public void setDeductionDPoint(double deductionDPoint) {
    this.deductionDPoint = deductionDPoint;
  }

  public double getSkuDiscount() {
    return skuDiscount;
  }

  public void setSkuDiscount(double skuDiscount) {
    this.skuDiscount = skuDiscount;
  }

  public Date getEffectFrom() {
    return effectFrom;
  }

  public void setEffectFrom(Date effectFrom) {
    this.effectFrom = effectFrom;
  }

  public Date getEffectTo() {
    return effectTo;
  }

  public void setEffectTo(Date effectTo) {
    this.effectTo = effectTo;
  }

  public int getPieceEffectTime() {
    return pieceEffectTime;
  }

  public void setPieceEffectTime(int pieceEffectTime) {
    this.pieceEffectTime = pieceEffectTime;
  }

  public int getPieceGroupNum() {
    return pieceGroupNum;
  }

  public void setPieceGroupNum(int pieceGroupNum) {
    this.pieceGroupNum = pieceGroupNum;
  }

  public String getpStatus() {
    return pStatus;
  }

  public void setpStatus(String pStatus) {
    this.pStatus = pStatus;
  }

  @Override
  public String toString() {
    return "EditVO{" +
        "pCode='" + pCode + '\'' +
        ", productId='" + productId + '\'' +
        ", name='" + name + '\'' +
        ", price=" + price +
        ", deductionDPoint=" + deductionDPoint +
        ", skuDiscount=" + skuDiscount +
        ", effectFrom=" + effectFrom +
        ", effectTo=" + effectTo +
        ", pieceEffectTime=" + pieceEffectTime +
        ", pieceGroupNum=" + pieceGroupNum +
        ", pStatus='" + pStatus + '\'' +
        '}';
  }
}
