package cn.hds.hvmall.pojo.piece;

import org.springframework.data.annotation.Id;

/**
 * @author qiuchuyi
 * @date 2018/9/3 14:50
 */
//@Table(name = "xxoo_stock")
public class GroupStock {
    @Id
    private Long skuId;
    //活动库存
    private Integer groupStock;
    //总库存
    private Integer groupStockTotal;
    //库存
    private Integer stock;

    @Override
    public String toString() {
        return "Stock{" +
                "skuId=" + skuId +
                ", groupStock=" + groupStock +
                ", groupStockTotal=" + groupStockTotal +
                ", stock=" + stock +
                '}';
    }

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    public Integer getGroupStock() {
        return groupStock;
    }

    public void setGroupStock(Integer groupStock) {
        this.groupStock = groupStock;
    }

    public Integer getGroupStockTotal() {
        return groupStockTotal;
    }

    public void setGroupStockTotal(Integer groupStockTotal) {
        this.groupStockTotal = groupStockTotal;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }
}
