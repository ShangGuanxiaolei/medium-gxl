package cn.hds.hvmall.pojo.piece;

/**
 * @author qiuchuyi
 * @date 2018/9/5 19:46
 */
public class PgSku {
    //Xquark_sku 表id
    private Long sId;
    //sku编号
    private String skuCode;
    //产品编号
    private String productId;
    //活动sku数量，后台输入那个
    private Integer pSkuNum;

    public Long getsId() {
        return sId;
    }

    public void setsId(Long sId) {
        this.sId = sId;
    }

    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Integer getpSkuNum() {
        return pSkuNum;
    }

    public void setpSkuNum(Integer pSkuNum) {
        this.pSkuNum = pSkuNum;
    }

    @Override
    public String toString() {
        return "PgSku{" +
                "sId=" + sId +
                ", skuCode='" + skuCode + '\'' +
                ", productId='" + productId + '\'' +
                ", pSkuNum=" + pSkuNum +
                '}';
    }
}
