package cn.hds.hvmall.pojo.piece;

import java.math.BigDecimal;

public class PartSpec {
    /**
     * 成团人数
     */
    private Integer pieceGroupNum;

    /**
     * 活动价格
     */
    private BigDecimal promotionPrice;

    /**
     *  兑换德分
     */
    private BigDecimal point;

    /**
     * 立减
     */
    private BigDecimal reduction;

    /**
     * 净值
     */
    private BigDecimal netWorth;

    /**
     * 推广费
     */
    private BigDecimal promoAmt;

    /**
     * 服务费
     */
    private BigDecimal serverAmt;

    /**
     * 新人免单
     */
    private Boolean newFree = false;

    /**
     * 拼主价次数设置类型 0->不设置,1->统一设置,2->分别设置
     */
    private Integer unifiedSetup;

    /**
     * 拼主价,设置类型为统一设置时使用
     */
    private BigDecimal memberPrice;

    /**
     * 拼主价次数
     */
    private Integer frequency;

    /**
     * 纯白人
     */
    private String pureWhite;

    /**
     * 纯白人拼主价
     */
    private BigDecimal pureMemberPrice;

    /**
     * 纯白人拼主价次数
     */
    private Integer pureFrequency;

    /**
     * 白人
     */
    private String white;

    /**
     * 白人拼主价
     */
    private BigDecimal whiteMemberPrice;

    /**
     * 白人拼主价次数
     */
    private Integer whiteFrequency;

    /**
     * VIP
     */
    private String vip;

    /**
     * VIP拼主价
     */
    private BigDecimal vipMemberPrice;

    /**
     * VIP拼主价次数
     */
    private Integer vipFrequency;

    /**
     * 店主
     */
    private String merchant;

    /**
     * 店主拼主价
     */
    private BigDecimal merchantMemberPrice;

    /**
     * 店主拼主价次数
     */
    private Integer merchantFrequency;

    public Integer getPieceGroupNum() {
        return pieceGroupNum;
    }

    public void setPieceGroupNum(Integer pieceGroupNum) {
        this.pieceGroupNum = pieceGroupNum;
    }

    public BigDecimal getPromotionPrice() {
        return promotionPrice;
    }

    public void setPromotionPrice(BigDecimal promotionPrice) {
        this.promotionPrice = promotionPrice;
    }

    public BigDecimal getPoint() {
        return point;
    }

    public void setPoint(BigDecimal point) {
        this.point = point;
    }

    public BigDecimal getReduction() {
        return reduction;
    }

    public void setReduction(BigDecimal reduction) {
        this.reduction = reduction;
    }

    public BigDecimal getNetWorth() {
        return netWorth;
    }

    public void setNetWorth(BigDecimal netWorth) {
        this.netWorth = netWorth;
    }

    public BigDecimal getPromoAmt() {
        return promoAmt;
    }

    public void setPromoAmt(BigDecimal promoAmt) {
        this.promoAmt = promoAmt;
    }

    public BigDecimal getServerAmt() {
        return serverAmt;
    }

    public void setServerAmt(BigDecimal serverAmt) {
        this.serverAmt = serverAmt;
    }

    public Boolean getNewFree() {
        return newFree;
    }

    public void setNewFree(Boolean newFree) {
        this.newFree = newFree;
    }

    public Integer getUnifiedSetup() {
        return unifiedSetup;
    }

    public void setUnifiedSetup(Integer unifiedSetup) {
        this.unifiedSetup = unifiedSetup;
    }

    public BigDecimal getMemberPrice() {
        return memberPrice;
    }

    public void setMemberPrice(BigDecimal memberPrice) {
        this.memberPrice = memberPrice;
    }

    public Integer getFrequency() {
        return frequency;
    }

    public void setFrequency(Integer frequency) {
        this.frequency = frequency;
    }

    public BigDecimal getPureMemberPrice() {
        return pureMemberPrice;
    }

    public void setPureMemberPrice(BigDecimal pureMemberPrice) {
        this.pureMemberPrice = pureMemberPrice;
    }

    public Integer getPureFrequency() {
        return pureFrequency;
    }

    public void setPureFrequency(Integer pureFrequency) {
        this.pureFrequency = pureFrequency;
    }

    public BigDecimal getWhiteMemberPrice() {
        return whiteMemberPrice;
    }

    public void setWhiteMemberPrice(BigDecimal whiteMemberPrice) {
        this.whiteMemberPrice = whiteMemberPrice;
    }

    public Integer getWhiteFrequency() {
        return whiteFrequency;
    }

    public void setWhiteFrequency(Integer whiteFrequency) {
        this.whiteFrequency = whiteFrequency;
    }

    public BigDecimal getVipMemberPrice() {
        return vipMemberPrice;
    }

    public void setVipMemberPrice(BigDecimal vipMemberPrice) {
        this.vipMemberPrice = vipMemberPrice;
    }

    public Integer getVipFrequency() {
        return vipFrequency;
    }

    public void setVipFrequency(Integer vipFrequency) {
        this.vipFrequency = vipFrequency;
    }

    public BigDecimal getMerchantMemberPrice() {
        return merchantMemberPrice;
    }

    public void setMerchantMemberPrice(BigDecimal merchantMemberPrice) {
        this.merchantMemberPrice = merchantMemberPrice;
    }

    public Integer getMerchantFrequency() {
        return merchantFrequency;
    }

    public void setMerchantFrequency(Integer merchantFrequency) {
        this.merchantFrequency = merchantFrequency;
    }

    public String getPureWhite() {
        return pureWhite;
    }

    public void setPureWhite(String pureWhite) {
        this.pureWhite = pureWhite;
    }

    public String getWhite() {
        return white;
    }

    public void setWhite(String white) {
        this.white = white;
    }

    public String getVip() {
        return vip;
    }

    public void setVip(String vip) {
        this.vip = vip;
    }

    public String getMerchant() {
        return merchant;
    }

    public void setMerchant(String merchant) {
        this.merchant = merchant;
    }

}
