package cn.hds.hvmall.pojo.whitecustomer;

/**
 * Created with IDEA
 * author:Yarm.Yang
 * Date:2019/1/22
 * Time:10:01
 * Des:
 */
public class WhiteListExcelVo {
    private String pCode;
    private String cpId;

    public String getpCode() {
        return pCode;
    }

    public void setpCode(String pCode) {
        this.pCode = pCode;
    }

    public String getCpId() {
        return cpId;
    }

    public void setCpId(String cpId) {
        this.cpId = cpId;
    }
}