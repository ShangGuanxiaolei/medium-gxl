package cn.hds.hvmall.config;

import org.apache.catalina.filters.CorsFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;


/**
 * cors跨域请求处理
 * @类名: CorsConfig.java
 * @描述: TODO .
 * @程序猿: Jrain Chen
 * @日期: 2018年8月16日
 * @版本号: V1.0 .
 */
@Configuration
public class CorsConfig {
	
	
	@Bean
	public FilterRegistrationBean corsFilter() {
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		CorsConfiguration config = new CorsConfiguration();
		config.setAllowCredentials(true);
		// 设置你要允许的网站域名，如果全允许则设为 *
		config.addAllowedOrigin("http://localhost:4200");
		// 如果要限制 HEADER 或 METHOD 请自行更改
		config.addAllowedHeader("*");
		config.addAllowedMethod("*");
		source.registerCorsConfiguration("/**", config);
		FilterRegistrationBean bean = new FilterRegistrationBean(new CorsFilter());
		// 这个顺序很重要哦，为避免麻烦请设置在最前
		bean.setOrder(0);
		return bean;
	}
}