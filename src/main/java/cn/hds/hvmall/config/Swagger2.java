package cn.hds.hvmall.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * 配置
 * @类名: Swagger2.java
 * @描述: TODO .
 * @程序猿: Jrain Chen
 * @日期: 2018年8月10日
 * @版本号: V1.0 .
 */
@Configuration
@EnableSwagger2
public class Swagger2{    

	@Bean
	public Docket createRestApi() {
		return new Docket(DocumentationType.SWAGGER_2)
				.apiInfo(apiInfo())
				.select()
				.apis(RequestHandlerSelectors.basePackage("cn.hds"))
				.paths(PathSelectors.any())
				.build();
		}

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("在线api文档")
                .description("SpringBoot中使用Swagger2构建RESTful API")
                .termsOfServiceUrl("http://localhost:8080/swagger-ui.html")
                .contact("汉微后台服务")
                .version("2.0")
                .build();
    	}
}