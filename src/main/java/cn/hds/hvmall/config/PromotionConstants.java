package cn.hds.hvmall.config;

/**
 * created by
 *
 * @author wangxinhua at 18-7-4 下午1:45
 */
public class PromotionConstants {

  public static String FLASH_SALE_LOCKER = "flash_sale_locker";

  public static String DISTRIBUTED_LOCKER = "distributed_locker";

  /**
   * 限时抢购库存
   */
  public static String FLASH_SALE_AMOUNT_KEY = "fsak";

  public static String FLASH_SALE_TOTAL_AMOUNT_KEY = "fstak";

  /**
   * 单人限购商品数量
   */
  public static String FLASH_SALE_LIMIT_AMOUNT_KEY = "fslak";

  /**
   * 用户已购买key
   */
  public static String getUserAmountKey(String userId, String promotionId) {
    return userId + ":" + promotionId;
  }

  public static String getFlashSaleAmountKey(String promotionId, String skuId) {
    return promotionId + ":sku" + skuId + ":amount";
  }

  public static String getFlashSaleTotalAmountKey(String promotionId, String skuId) {
    return promotionId + ":sku" + skuId + ":totalAmount";
  }

  public static String getFlashSaleLimitAmountKey(String promotionId, String productId) {
    return promotionId + ":product" + productId + ":limitAmount";
  }

  public static String getCommonAmountKey(String promotionId, String productId) {
    return promotionId +"_"+ productId + ":amount";
  }

  /**
   * 拼团剩余人数
   */
  public static String getRestTranMemberKey(String tranCode) {
    return tranCode + ":restMember";
  }

  public static String getPromotionPriceNum(String cpid, String detailCode) {
    final String promotionPriceNumTemplate = "%s:cpid:%s:detailCode";
    return String.format(promotionPriceNumTemplate, cpid, detailCode);
  }


  /**
   * 控制每个团最多的可免单人数
   */
  public static String getFreePayMemberKey(String tranCode) {
    return tranCode + ":freePayMember";
  }

  /**
   * 总拼团人数
   */
  public static String getTotalTranMemberKey(String tranCode) {
    return tranCode + ":totalMember";
  }

  /**
   * 订单占用详情
   */
  public static String getTranOrderDetailKey(String tranCode, String order) {
    return tranCode + ":" + order;
  }

}
