package cn.hds.hvmall.config;

import com.baomidou.mybatisplus.plugins.PaginationInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * plus插件配置
 * @类名: MyBatisPlusConfig.java
 * @描述: TODO .
 * @程序猿: Jrain Chen
 * @日期: 2018年8月10日
 * @版本号: V1.0 .
 */
@Configuration
public class MyBatisPlusConfig {

    @Bean
    public PaginationInterceptor paginationInterceptor() {
    	PaginationInterceptor page = new PaginationInterceptor();
        page.setDialectType("mysql");
        return page;
    }
   
}
