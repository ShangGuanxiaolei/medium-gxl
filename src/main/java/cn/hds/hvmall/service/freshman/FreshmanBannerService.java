package cn.hds.hvmall.service.freshman;

import cn.hds.hvmall.base.PageBase;
import cn.hds.hvmall.entity.freshman.Banner;
import cn.hds.hvmall.entity.freshman.BannerInfo;
import cn.hds.hvmall.entity.freshman.BannerProduct;
import cn.hds.hvmall.entity.freshman.ProductType;
import cn.hds.hvmall.pojo.list.ExecuteResult;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface FreshmanBannerService {

    /**
     * 添加Banner
     * @return
     */
    ExecuteResult<Integer> insertBanner(String json);


    /**
     * 根据条件查询商品信息
     * @return
     */
    ExecuteResult<PageBase<Banner>> selectProductInfo(BannerProduct bannerProduct);

    /**
     * 查询商品类型
     * @param parentId
     * @return
     */
    ExecuteResult<List<ProductType>> selectType(Integer parentId);

    /**
     * 查询品牌
     * @return
     */
    ExecuteResult<List<ProductType>> selectBrand ();

    /**
     * 查询Banner信息
     * @return
     */
    ExecuteResult<List<BannerInfo>> selectBannerInfo();

    /**
     * 修改Banner信息
     * @param bannerInfo
     * @return
     */
    ExecuteResult<Boolean> updateBannerInfo(BannerInfo bannerInfo);
}
