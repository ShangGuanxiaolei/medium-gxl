package cn.hds.hvmall.service.freshman;



import cn.hds.hvmall.base.BaseExecuteResult;
import cn.hds.hvmall.entity.freshman.FreshmanModule;
import cn.hds.hvmall.entity.freshman.FreshmanSaleCount;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.List;

public interface FreshmanModuleService {

    List<FreshmanModule> selectFreshmanModule();

    boolean updateStatus(String json);

    BaseExecuteResult<?> sort(String json);

    boolean updateUser(String userName, String id);

}