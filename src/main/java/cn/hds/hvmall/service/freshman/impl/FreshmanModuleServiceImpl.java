package cn.hds.hvmall.service.freshman.impl;

import cn.hds.hvmall.base.BaseExecuteResult;
import cn.hds.hvmall.entity.freshman.FreshmanModule;
import cn.hds.hvmall.mapper.freshman.FreshmanModuleMapper;
import cn.hds.hvmall.service.freshman.FreshmanModuleService;
import cn.hds.hvmall.utils.ConstantUtil;
import cn.hds.hvmall.utils.JSONUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.List;
import java.util.Map;

import static java.time.OffsetDateTime.now;

@Service("freshmanModuleService")
public class FreshmanModuleServiceImpl implements FreshmanModuleService {

    private static final Logger logger = LoggerFactory.getLogger(FreshmanModuleServiceImpl.class);

    @Autowired
    private FreshmanModuleMapper freshmanModuleMapper;


    @Override
    public List<FreshmanModule> selectFreshmanModule() {
        return freshmanModuleMapper.selectFreshmanModule();
    }

    /**
     * 禁用、启用按钮
     * @param json
     * @return
     */
    @Override
    public boolean updateStatus(String json) {
        Map<String,Object> map=JSONUtil.toMap(json);
        String id=map.get("id").toString();
        String userName=map.get("userName").toString();
        boolean a=false;
        int status;
        status=freshmanModuleMapper.selectStatusById(id);
        if(status==1){
            status=0;
             a=freshmanModuleMapper.updateStatus(status,id);
            updateUser(userName,id);
            logger.info("禁用按钮修改成功，操作人："+userName+",操作时间："+now());
            return a;
        }
        if(status==0){
            status=1;
             a=freshmanModuleMapper.updateStatus(status,id);
            updateUser(userName,id);
            logger.info("启用按钮修改成功，操作人："+userName+",操作时间："+now());
            return a;
        }
        return a;
    }


    /**
     * 问题排序
     * @param json
     * @return
     */
    @Override
    @Transactional
    public BaseExecuteResult<?> sort(String json) {
        BaseExecuteResult<Object> result=null;
        try {
            Map<String, Object> map = JSONUtil.toMap(json);
            //当前分类id
            String NowSortNum =map.get("nowSortNum").toString();
            String Id=map.get("id").toString();
            String sort=map.get("sort").toString();
            String userName=map.get("userName").toString();

            //下一个分类id
            Integer NextId=0;
            Integer NextSortNum=0;

            //计算下个分类id
            List<Integer> IdList=freshmanModuleMapper.selectId();

            List<Integer> NextSortNumList=freshmanModuleMapper.selectSortNum();
            for (int i = 0; i < IdList.size(); i++) {
                Integer DetailId=IdList.get(i);
                String StringSort=DetailId.toString()+".0";

                if (StringSort.equals(Id)) {
                    //当排序为向下的时候
                    if(sort.equals("down")) {
                        NextId = IdList.get(i + 1);
                        NextSortNum=NextSortNumList.get(i+1);
                        break;
                    }
                    //当排序为向上的时候
                    else if(sort.equals("up")){
                        NextId=IdList.get(i-1);
                        NextSortNum=NextSortNumList.get(i-1);
                        break;
                    }
                }
            }

            //把编号为themePresent的id设为0
            Integer num = freshmanModuleMapper.updateZeroBySortNum(Id);
            if (num != 1) {
                //update失败
                result = new BaseExecuteResult<>(ConstantUtil.failed,
                        ConstantUtil.ResponseError.THEME_ID_SORT_ERROR.getCode(),
                        ConstantUtil.ResponseError.THEME_ID_SORT_ERROR.toString());
            }
            //再把编号为themePresent的id和themeNext的id对调,后台刷新页面即可看到效果
            Integer num1 = freshmanModuleMapper.updateNowSortByNext(NowSortNum,NextId);
            if (num1 != 1) {
                //update失败
                result = new BaseExecuteResult<>(ConstantUtil.failed,
                        ConstantUtil.ResponseError.THEME_ID_SORT_ERROR.getCode(),
                        ConstantUtil.ResponseError.THEME_ID_SORT_ERROR.toString());
            }
            Integer num2 = freshmanModuleMapper.updatNextByZero(NextSortNum);
            if (num2 != 1) {
                //update失败
                result = new BaseExecuteResult<>(ConstantUtil.failed,
                        ConstantUtil.ResponseError.THEME_ID_SORT_ERROR.getCode(),
                        ConstantUtil.ResponseError.THEME_ID_SORT_ERROR.toString());
            }
            //排序成功
            result = new BaseExecuteResult<>(ConstantUtil.success,
                    ConstantUtil.ResponseMSG.THEME_ID_SORT_OK.getCn());
            freshmanModuleMapper.updateUser(userName,Id);
            freshmanModuleMapper.updateUser(userName,NextId.toString());
            logger.info("排序修改成功，操作人："+userName+",操作时间："+now());
        }catch (Exception e){
            e.printStackTrace();
            logger.info(ConstantUtil.ERROR_FORMAT, this.getClass().getSimpleName(), "sort", e.getMessage());
            result = new BaseExecuteResult<Object>(
                    ConstantUtil.failed,
                    ConstantUtil.ResponseError.SYS_ERROR.getCode(),
                    ConstantUtil.ResponseError.SYS_ERROR.toString());
            // 针对多条数据操作需要手动开启事务
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        }
        return result;
    }

    /**
     * 修改人姓名
     * @param id
     * @return
     */
    @Override
    public boolean updateUser(String userName,String id) {
        boolean a=freshmanModuleMapper.updateUser(userName,id);
        return a;
    }



}
