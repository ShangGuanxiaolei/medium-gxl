package cn.hds.hvmall.service.freshman.impl;

import cn.hds.hvmall.base.BaseExecuteResult;
import cn.hds.hvmall.base.PageBase;
import cn.hds.hvmall.entity.freshman.Banner;
import cn.hds.hvmall.entity.freshman.BannerInfo;
import cn.hds.hvmall.entity.freshman.BannerProduct;
import cn.hds.hvmall.entity.freshman.ProductType;
import cn.hds.hvmall.mapper.freshman.FreshmanBannerMapper;
import cn.hds.hvmall.pojo.freshmantab.FreshmanProductSku;
import cn.hds.hvmall.pojo.list.ExecuteResult;
import cn.hds.hvmall.service.freshman.FreshmanBannerService;
import cn.hds.hvmall.service.freshman.FreshmanModuleService;
import cn.hds.hvmall.utils.ConstantUtil;
import cn.hds.hvmall.utils.JSONUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.time.OffsetDateTime.now;

@Service("freshmanBannerService")
public class FreshmanBannerServiceImpl implements FreshmanBannerService {

    private static final Logger logger = LoggerFactory.getLogger(FreshmanBannerServiceImpl.class);

    @Autowired
    private FreshmanBannerMapper freshmanBannerMapper;

    @Autowired
    private FreshmanModuleService freshmanModuleService;


    /**
     * 添加Banner
     * @param json
     * @return
     */
    @Override
    public ExecuteResult<Integer> insertBanner(String json) {
        ExecuteResult<Integer> result=new ExecuteResult<>();
        try {
            Map<String, Object> map = JSONUtil.toMap(json);
            String imgUrl=map.get("imgUrl").toString();
            String startAt=map.get("startAt").toString();
            String endAt=map.get("endAt").toString();
            int redirectType=(Integer)map.get("redirectType");
            String redirectUrl=map.get("redirectUrl").toString();
            int status=(Integer) map.get("status");

            boolean a=freshmanBannerMapper.insertBanner(imgUrl,startAt,endAt,redirectType,redirectUrl,status);
            result.setResult(a);
            result.setIsSuccess(ConstantUtil.success);
        }catch (Exception e){
            logger.error("入参有误！",e);
            result.setIsSuccess(ConstantUtil.failed);
            result.setErrorCode(ConstantUtil.ResponseError.SYS_ERROR.getCode());
            result.setErrorMsg(ConstantUtil.ResponseError.SYS_ERROR.toString());
        }
        return result;
    }

    /**
     * 根据条件查询商品信息
     * @return
     */
    @Override
    public ExecuteResult<PageBase<Banner>> selectProductInfo(BannerProduct bannerProduct) {
        ExecuteResult<PageBase<Banner>> result=new ExecuteResult<>();
//        Map<String,Object> map=JSONUtil.toMap(json);
//        BannerProduct bannerProduct = JSONUtil.toBean(json, BannerProduct.class);

//        Integer productId=null;
//        String productName="";
//        Integer typeId=null;
//        Integer brandId=null;
//        Integer pageNo=null;
//        Integer pageSize=null;
//        String productList="";
//        if(null!=map.get("productId")){
//            productId=(Integer) map.get("productId");
//        }
//        if(null!=map.get("productList")){
//            productList=map.get("productList").toString();
//        }
//        if(null!=map.get("productName")){
//            productName=map.get("productName").toString();
//        }
//        if(null!=map.get("typeId")){
//            typeId=(Integer) map.get("typeId");
//        }
//        if(null!=map.get("brandId")){
//            brandId=(Integer) map.get("brandId");
//        }
//        if(null!=map.get("pageNo")){
//            pageNo=(Integer) mapsd.get("pageNo");
//        }
//        if(null!=map.get("pageSize")){
//            pageSize=(Integer) map.get("pageSize");
//        }
        PageBase<Banner> pageBase=new PageBase<>(bannerProduct.getPageSize(),0,bannerProduct.getPageNo());
        int offSet= (bannerProduct.getPageNo()-1)*bannerProduct.getPageSize();
        List<Banner> selectProductInfo=freshmanBannerMapper.selectProductInfo(bannerProduct.getProductId(),bannerProduct.getProductName(),bannerProduct.getTypeId(),bannerProduct.getBrandId(),bannerProduct.getProductList(),offSet,bannerProduct.getPageSize());
        for (Banner banner : selectProductInfo) {
            List<FreshmanProductSku> freshmanProductSkus = banner.getFreshmanProductSkus();
            for (FreshmanProductSku productSkus : freshmanProductSkus) {
                productSkus.setFreshStock(null);
                productSkus.setNewFreshStock(null);
                productSkus.setNetWorth(null);
                productSkus.setServerAmt(null);
                productSkus.setFreshPoint(null);
                productSkus.setFreshManPrice(null);
                productSkus.setFreshConversionPrice(null);
                productSkus.setReduction(null);
                productSkus.setPromoAmt(null);
            }
        }
        int count=freshmanBannerMapper.selectProductInfoCount(bannerProduct.getProductId(),bannerProduct.getProductName(),bannerProduct.getTypeId(),bannerProduct.getBrandId(),bannerProduct.getProductList());
        int totalPage = count % bannerProduct.getPageSize() == 0 ? count / bannerProduct.getPageSize() : count / bannerProduct.getPageSize() + 1;

        pageBase.setTotalPages(totalPage);
        pageBase.setPageNum(offSet);
        pageBase.setTotalCount(count);
        pageBase.setPageSize(bannerProduct.getPageSize());
        pageBase.setResultList(selectProductInfo);
        result.setResult(pageBase);
        return result;
    }

    /**
     * 查询类型
     * @param parentId
     * @return
     */
    @Override
    public ExecuteResult<List<ProductType>> selectType(Integer parentId) {
        ExecuteResult<List<ProductType>> result=new ExecuteResult<>();
        if(parentId!=null){
            List<ProductType> detailType=freshmanBannerMapper.selectDetailType(parentId);
            result.setResult(detailType);
        }else{
            List<ProductType> parentType=freshmanBannerMapper.selectParentType();
            result.setResult(parentType);
        }
        return result;
    }

    /**
     * 查询品牌
     * @return
     */
    @Override
    public ExecuteResult<List<ProductType>> selectBrand() {
        ExecuteResult<List<ProductType>> result=new ExecuteResult<>();
        List<ProductType> list=freshmanBannerMapper.selectBrand();
        if(list.size()>0){
            result.setIsSuccess(ConstantUtil.success);
            result.setCount(list.size());
            result.setResult(list);
        }else{
            logger.error("查询出错！");
            result.setIsSuccess(ConstantUtil.failed);
            result.setErrorCode(ConstantUtil.ResponseError.FAILED.getCode());
            result.setErrorMsg(ConstantUtil.ResponseError.FAILED.toString());
        }
        return result;
    }

    /**
     * 查询Banner信息
     * @return
     */
    @Override
    public ExecuteResult<List<BannerInfo>> selectBannerInfo() {
        ExecuteResult<List<BannerInfo>> result=new ExecuteResult<>();
        List<BannerInfo> list=freshmanBannerMapper.selectBannerInfo();
        if(list.size()>0){
            result.setIsSuccess(ConstantUtil.success);
            result.setCount(list.size());
            result.setResult(list);
        }else{
            logger.error("查询出错！");
            result.setIsSuccess(ConstantUtil.failed);
            result.setErrorCode(ConstantUtil.ResponseError.FAILED.getCode());
            result.setErrorMsg(ConstantUtil.ResponseError.FAILED.toString());
        }
        return result;
    }

    /**
     * 修改Banner信息
     * @param bannerInfo
     * @return
     */
    @Override
    public ExecuteResult<Boolean> updateBannerInfo(BannerInfo bannerInfo) {
        ExecuteResult<Boolean> result=new ExecuteResult<>();
        boolean a=freshmanBannerMapper.updateBannerInfo(bannerInfo);
        freshmanModuleService.updateUser(bannerInfo.getUserName(),bannerInfo.getModuleId());
        logger.info("banner信息修改成功，操作人："+bannerInfo.getUserName()+",操作时间："+now());
        result.setResult(a);
        return result;
    }


}
