package cn.hds.hvmall.service.flashsale;

import cn.hds.hvmall.base.RedisService;
import cn.hds.hvmall.config.PromotionConstants;
import cn.hds.hvmall.entity.piece.PromotionPgPrice;
import cn.hds.hvmall.entity.promotion.ActivityExclusive;
import cn.hds.hvmall.enums.FlashSaleApplyStatus;
import cn.hds.hvmall.mapper.flashsale.PromotionFlashSaleMapper;
import cn.hds.hvmall.mapper.piece.PromotionPgPriceMapper;
import cn.hds.hvmall.mapper.promotion.PromotionExclusiveMapper;
import cn.hds.hvmall.pojo.promotion.*;
import cn.hds.hvmall.service.list.ProductService;
import cn.hds.hvmall.service.promotion.PromotionService;
import cn.hds.hvmall.service.stockcheck.PromotionStockCheckService;
import cn.hds.hvmall.type.PromotionType;
import cn.hds.hvmall.utils.list.GlobalErrorCode;
import cn.hds.hvmall.utils.list.qiniu.BizException;
import cn.hds.hvmall.utils.list.qiniu.IdTypeHandler;
import com.google.common.base.Preconditions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static cn.hds.hvmall.type.PromotionType.FLASHSALE;

/**
 * Created by wangxinhua on 17-11-23. DESC:
 */
@Service
public class FlashSalePromotionProductService {

    private final PromotionFlashSaleMapper promotionFlashSaleMapper;

    private final ProductService productService;

    private final RedisService redisService;

    private final PromotionService promotionService;

    private final PromotionPgPriceMapper pgPriceMapper;

    private final PromotionExclusiveMapper exclusiveMapper;

    private final PromotionStockCheckService promotionStockCheckService;


    @Autowired
    public FlashSalePromotionProductService(
            ProductService productService,
            PromotionFlashSaleMapper promotionFlashSaleMapper,
            RedisService redisService,
            PromotionService promotionService, PromotionPgPriceMapper pgPriceMapper, PromotionExclusiveMapper exclusiveMapper,
            PromotionStockCheckService promotionStockCheckService) {
        this.productService = productService;
        this.promotionFlashSaleMapper = promotionFlashSaleMapper;
        this.redisService = redisService;
        this.promotionService = promotionService;
        this.pgPriceMapper = pgPriceMapper;
        this.exclusiveMapper = exclusiveMapper;
        this.promotionStockCheckService=promotionStockCheckService;
    }

    public boolean inPromotion(Long productId, Long skuId) {
        return promotionFlashSaleMapper.inPromotion(productId, skuId);
    }

    public boolean inPromotion(String promotionId, String productId) {
        return promotionFlashSaleMapper.exists(promotionId, productId);
    }

    public PromotionProduct loadPromotionProduct(Long id) {
        return promotionFlashSaleMapper.selectByPrimaryKey(id);
    }

    public PromotionProductVO loadPromotionProductByPId(String pId) {
        FlashSalePromotionProductVO result = promotionFlashSaleMapper.selectVOByProductId(pId, true);
        if (result == null) {
            return null;
        }

        wrapperRedisAmt(result.getPromotion(), result);
        return result;
    }

    private void wrapperRedisAmt(Promotion promotion, PromotionSkuItem result) {
        final String promotionId = promotion.getIdEncoded();
        final String skuId = IdTypeHandler.encode(result.getSkuId());
        final String productId = IdTypeHandler.encode(result.getProductId());
        String flashSaleAmountKey = PromotionConstants.getFlashSaleAmountKey(promotionId, skuId);
        String flashSaleLimitKey = PromotionConstants.getFlashSaleLimitAmountKey(promotionId, productId);
        String flashSaleTotalAmountKey = PromotionConstants
                .getFlashSaleTotalAmountKey(promotion.getIdEncoded(), skuId);
        Integer amount = Optional.ofNullable(redisService.get(flashSaleAmountKey)).map(Integer::valueOf).orElse(0);
        Integer limitAmount = Optional.ofNullable(redisService.get(flashSaleLimitKey)).map(Integer::valueOf).orElse(0);
        Integer totalAmount = Optional.ofNullable(redisService.get(flashSaleTotalAmountKey)).map(Integer::valueOf).orElse(0);
        // 从redis中取库存
        result.setAmount(Long.valueOf(amount));
        result.setLimitAmount(Long.valueOf(limitAmount));
        if (totalAmount != 0) {
            result.setSales((long) (totalAmount - amount));
        }
    }

    public PromotionProductVO loadPromotionProductByProductId(String pId) {
        FlashSalePromotionProductVO result = promotionFlashSaleMapper.selectPromotionProductByProductId(pId, true);
        if (result == null) {
            return null;
        }
        wrapperRedisAmt(result.getPromotion(), result);
        return result;
    }

    public PromotionProduct loadPromotionProduct(String promotionId, String productId) {
        return promotionFlashSaleMapper.selectByPromotionIdAndProductId(promotionId, productId);
    }

    public FlashSalePromotionSkuVO loadPromotionSkuVO(String id) {
        FlashSalePromotionSkuVO ret = promotionFlashSaleMapper.selectSkuVOByPrimaryKey(id);
        this.setFlashSaleAfter(ret);
        return ret;
    }

    private void setFlashSaleAfter(FlashSalePromotionSkuVO flashSale) {
        if (flashSale == null) {
            return;
        }
        List<PromotionFlashSale> skus = flashSale.getSkus();
        if (CollectionUtils.isEmpty(skus)) {
            return;
        }
        skus.forEach(s -> {
            wrapperRedisAmt(flashSale, s);
            s.setSkuPricing(pgPriceMapper.selectByPromotionIdAndSkuId(s.getPromotionId(), s.getSkuId()));
        });
    }

    public PromotionProductVO loadPromotionProductVO(String id) {
        FlashSalePromotionProductVO result = promotionFlashSaleMapper.selectVOByPrimaryKey(id);
        if (result == null) {
            return null;
        }
        wrapperRedisAmt(result.getPromotion(), result);
        return result;
    }

    public PromotionProductVO loadPromotionProductVO(String promotionId, String productId) {
        return promotionFlashSaleMapper.selectVOByPromotionIdAndProductId(promotionId, productId);
    }

    public List<PromotionFlashSale> listPromotionProducts(String id, String order,
                                                          Direction direction, Pageable pageable) {
        return promotionFlashSaleMapper.listPromotionFlashSale(id, order, direction, pageable);
    }

    public List<FlashSalePromotionProductVO> listPromotionProductVOs(String id, String order,
                                                                     Direction direction, Pageable pageable) {
        return promotionFlashSaleMapper.listPromotionFlashSaleVO(id, order, direction, pageable);
    }

    public Long countProducts(String promotionId) {
        return promotionFlashSaleMapper.count(promotionId);
    }

    public boolean updateStock(String orderId, int qty) {
        return promotionFlashSaleMapper.updateStock(orderId, qty) > 0;
    }

    public BigDecimal getPromotionPrice(String promotionProductId, String userId) {
        FlashSalePromotionProductVO product = (FlashSalePromotionProductVO)
                this.loadPromotionProductVO(promotionProductId);
        if (product == null) {
            throw new BizException(GlobalErrorCode.NOT_FOUND, "活动商品不存在");
        }
        return product.getDiscount();
    }

    /**
     * 保存关联对象
     *
     * @param flashSale 活动商品关联对象
     * @return true or false
     */
    public boolean saveFlashSale(PromotionFlashSale flashSale) {
        Long productId = flashSale.getProductId();
        Long skuId = flashSale.getSkuId();
        if (inPromotion(productId, skuId)) {
            String skuCode=promotionFlashSaleMapper.selectSkuCodeById(skuId);
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "该商品已经参与了抢购活动或添加多次,skuId:"+skuId+",skuCode:"+skuCode);
        }
        if (flashSale.getDiscount() == null) {
            flashSale.setDiscount(BigDecimal.ZERO);
        }

        // 同时保存价格体系配置
        pgPriceMapper.insert(flashSale.getSkuPricing());

        return promotionFlashSaleMapper.insert(flashSale) > 0;
    }

    /**
     * 更新活动商品关联对象
     *
     * @param flashSale 活动商品关联对象
     * @return true or false
     */
    public boolean updateFlashSale(PromotionFlashSale flashSale) {
        // 同步更新价格体系
        Long id = flashSale.getId();
        if (id == null) {
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "价格体系id不能为空");
        }
        pgPriceMapper.updateByPromotionIdAndSkuId(flashSale.getSkuPricing());
        return promotionFlashSaleMapper.updateByPromotionIdAndSkuId(flashSale) > 0;
    }

    @Transactional
    public boolean updatePromotionProduct(Promotion promotion,
                                          List<PromotionFlashSale> promotionProducts, boolean force) {
        Preconditions.checkNotNull(promotion);
        Preconditions.checkState(!CollectionUtils.isEmpty(promotionProducts));

        boolean promotionUpdateRet;
        boolean promotionProductUpdateRet;

        Long pId = promotion.getId();
        if (pId == null) {
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "活动id不能为空");
        }

        Promotion dbPromotion = promotionService.load(pId);
        if (dbPromotion == null) {
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "要更新的活动不存在");
        }

        promotionUpdateRet = promotionService.update(promotion);

        // 强制更新所有字段, 否则不更新价格体系字段
        if (force) {
            for (PromotionFlashSale f : promotionProducts) {
                Long fId = f.getId();
                if (fId == null) {
                    throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "活动配置id不能为空");
                }
                f.setPromotionId(pId);
                PromotionPgPrice skuPricing = f.getSkuPricing();
                if (skuPricing != null) {
                    skuPricing.setPromotionId(pId);
                }
                Long productId = f.getProductId();
                if (!productService.exists(productId)) {
                    throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "商品不存在");
                }
                promotionProductUpdateRet = updateFlashSale(f);
                boolean ret = promotionUpdateRet && promotionProductUpdateRet;
                if (!ret) {
                    throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "商品 " + productId + " 更新失败");
                }
                //校验库存
                String promotionIdString = promotion.getIdEncoded();
                String decodePromotionId=String.valueOf(IdTypeHandler.decode(promotionIdString));
                promotionStockCheckService.stockCheck(f.getAmount().intValue(),f.getSkuId().intValue(),decodePromotionId,"FLASESALE",null);


            }
        }
        return true;
    }

    /**
     * 保存活动及活动商品
     *
     * @param promotion         活动
     * @param promotionProducts 活动关联的多个商品
     * @return true or false
     */
    @Transactional
    public boolean savePromotionProduct(Promotion promotion,
                                        List<PromotionFlashSale> promotionProducts) {
        Preconditions.checkNotNull(promotion);
        Preconditions.checkState(!CollectionUtils.isEmpty(promotionProducts));

        boolean promotionSaveResult;
        boolean promotionProductSaveResult;

        promotion.setType(FLASHSALE);
        promotionSaveResult = promotionService.save(promotion);
        Long promotionId = promotion.getId();

        for (PromotionFlashSale promotionProduct : promotionProducts) {
            Long productId = promotionProduct.getProductId();
            if (!productService.exists(productId)) {
                throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "商品不存在");
            }

            promotionProduct.setPromotionId(promotionId);
            promotionProduct.getSkuPricing().setPromotionId(promotionId);
            promotionProductSaveResult = saveFlashSale(promotionProduct);
            //校验库存
            String promotionIdString = promotion.getIdEncoded();
            String decodePromotionId=String.valueOf(IdTypeHandler.decode(promotionIdString));
            promotionStockCheckService.stockCheck(promotionProduct.getAmount().intValue(),promotionProduct.getSkuId().intValue(),decodePromotionId,"FLASESALE",null);

//            } else {
//                Promotion dbPromotion = promotionService.load(promotionProduct.getPromotionId());
//                if (dbPromotion == null) {
//                    throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "要更新的活动不存在");
//                }
//                Date now = new Date();
//                Date validFrom = dbPromotion.getValidFrom();
//                Date validTo = dbPromotion.getValidTo();
//
//                if (now.before(validTo) && now.after(validFrom) && !dbPromotion.getClosed()) {
//                    throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "活动处于进行状态, 无法修改");
//                } else {
//                    promotionSaveResudlt = promotionService.update(promotion);
//                    promotionProductSaveResult = updateFlashSale(promotionProduct);
//                }
//            }
            boolean ret = promotionSaveResult && promotionProductSaveResult;
            if (!ret) {
                throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "商品 " + productId + " 保存失败");
            }
            // 过滤掉普通商品
            boolean needForce = exclusiveMapper.selectExistsByProductId(productId);
            if (needForce) {
                // 已经存在则强制更新
                exclusiveMapper.overWriteStatus(productId, "FLASHSALE", 1);
            } else {
                exclusiveMapper.insertActivityProduct(new ActivityExclusive(productId, "FLASHSALE"));
            }
        }
        return true;
    }

    /**
     * 查询用户vo
     *
     * @param id 活动商品id
     * @return 活动商品用户vo
     */
    public FlashSalePromotionUserVO loadUserVO(String id, String userId) {
        return promotionFlashSaleMapper.selectUserVOByPrimaryKey(id, userId);
    }

    /**
     * 查询所有商品数量
     *
     * @return 商品数量
     */
    public Long countAllProducts() {
        return promotionFlashSaleMapper.countAll();
    }

    public Long countAllByKey(String batchNo, PromotionType promotionType, String promotionStatus) {
        return promotionFlashSaleMapper.countAllByKey(batchNo, promotionType, promotionStatus);
    }

    /**
     * 查询所有商品数量
     *
     * @param isAvailable 是否只查询有效的商品
     * @return 商品数量
     */
    public Long countFlashSaleProducts(Boolean isAvailable) {
        return promotionFlashSaleMapper.countFlashSaleAll(isAvailable);
    }


    /**
     * 查询所有抢购活动商品
     *
     * @param pageable 分页对象
     * @return 限购商品集合
     */
    public List<FlashSalePromotionProductVO> listAllPromotionProductVOs(Pageable pageable) {
        return promotionFlashSaleMapper.listAllPromotionFlashSaleVO(pageable);
    }

    public List<Long> listAllProductId(Long promotionId) {
        return promotionFlashSaleMapper.listAllProductId(promotionId);
    }

    /**
     * 根据关键字查询活动
     *
     * @param batchNo         批次号
     * @param promotionType   活动类型
     * @param promotionStatus 活动状态
     * @param pageable        分页
     * @return List<FlashSalePromotionProductVO>
     */
    public List<FlashSalePromotionProductVO> searchByKeyWord(String batchNo,
                                                             PromotionType promotionType,
                                                             String promotionStatus,
                                                             Pageable pageable) {
        return promotionFlashSaleMapper.searchByKeyWord(batchNo, promotionType, promotionStatus, pageable);
    }

    /**
     * 所有秒杀抢购活动商品
     *
     * @param grandSaleId 大型促销活动id
     * @return 限购商品集合
     */
    public List<FlashSalePromotionProductVO> listProductVOs(Integer grandSaleId) {
        return promotionFlashSaleMapper.listFlashSaleVO(grandSaleId, FLASHSALE);
    }

    /**
     * 所有秒杀抢购活动商品
     *
     * @param order       排序 默认按销量及时间排序
     * @param direction   排序方向
     * @param pageable    分页对象
     * @param isAvailable 是否在当前时间内有效, 过滤掉失效的商品
     * @return 限购商品集合
     */
    public List<FlashSalePromotionProductVO> listProductVOs(String order,
                                                            Direction direction, Pageable pageable, Boolean isAvailable) {
        return promotionFlashSaleMapper
                .listFlashSaleVO(order, direction, pageable, isAvailable);
    }

    /**
     * 查询秒杀活动, 按sku维度
     *
     * @param order       排序顺序
     * @param direction   方向
     * @param pageable    分页
     * @param isAvailable 活动是否有效
     * @return skuVO
     */
    public List<FlashSalePromotionSkuVO> listPromotionSkuVOs(String order,
                                                             Direction direction, Pageable pageable, Boolean isAvailable) {
        List<FlashSalePromotionSkuVO> ret = promotionFlashSaleMapper
                .listPromotionSkuVOs(order, direction, pageable, isAvailable);
        // 设置每个活动的价格体系
        ret.forEach(this::setFlashSaleAfter);
        return ret;
    }

    public List<Promotion> listPromotion(String order,
                                         Direction direction, Pageable pageable, Boolean isAvailable) {
        return promotionFlashSaleMapper
                .listPromotion(order, direction, pageable, isAvailable);
    }

    public Long countPromotionSkuVOs(Boolean isAvailable) {
        return promotionFlashSaleMapper.countPromotionSkuVOs(isAvailable);
    }

    /**
     * 查询一元专区活动商品
     *
     * @return 专区商品集合
     */
    public List<FlashSalePromotionProductVO> listAllSaleZoneProductVOs() {
        return promotionFlashSaleMapper.listAllPromotionSaleZoneVO();
    }

    /**
     * 查询一元专区活动商品
     *
     * @param isAvailable true 查询有有效
     * @return 返回总记录数
     */
    public Long listAllSaleZone(Boolean isAvailable) {
        return promotionFlashSaleMapper.countSaleZoneAll(isAvailable);
    }

    /**
     * 查询所有订单关联的抢购活动申请列表
     *
     * @param userId    买家id, 可不传，查询所有订单
     * @param order     排序
     * @param direction 方向
     * @param pageable  分页
     * @return list of {@link FlashSalePromotionOrderVO} or empty list
     */
    public List<FlashSalePromotionOrderVO> listOrderVO(String userId, String order,
                                                       Direction direction, Pageable pageable) {
        return promotionFlashSaleMapper.listOrderVO(userId, order, direction, pageable);
    }

    /**
     * 查询所有订单关联的抢购活动申请列表
     *
     * @param order     排序
     * @param direction 方向
     * @param pageable  分页
     * @return list of {@link FlashSalePromotionOrderVO} or empty list
     */
    public List<FlashSalePromotionOrderVO> listOrderVO(String order, Direction direction,
                                                       Pageable pageable) {
        return listOrderVO(null, order, direction, pageable);
    }

    /**
     * 查询订单关联的抢购活动列表申请数量
     *
     * @param userId 买家id 可不传，查询所有订单
     * @return 订单数量
     */
    public Long countOrderVO(String userId) {
        return promotionFlashSaleMapper.countOrderVO(userId);
    }

    /**
     * 查询订单关联的抢购活动列表申请数量
     *
     * @return 订单数量
     */
    public Long countOrderVO() {
        return countOrderVO(null);
    }

    public PromotionProduct loadPromotionProductByPid(String productId, Boolean validOnly) {
        return promotionFlashSaleMapper.selectVOByProductId(productId, validOnly);
    }

    public Date loadPromotionValidFromIfExists(String productId) {
        return promotionFlashSaleMapper.selectPromotionValidFrom(productId);
    }

    public Optional<FlashSalePromotionProductVO> load(String promotionId, String productId) {
        final FlashSalePromotionProductVO f = (FlashSalePromotionProductVO) this.loadPromotionProductVO(promotionId, productId);
        return Optional.ofNullable(f);
    }

    public boolean close(Long id) {
        Promotion promotion = promotionService.load(id);
        if (promotion == null) {
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "活动不存在");
        }
        if (promotion.getClosed()) {
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "活动已经为结束状态,请勿重复提交");
        }
        changeExclusStatus(promotion.getId(), 0);
        return promotionService.close(id);
    }

    public boolean deletedFlashSale(Long id){
        Promotion promotion = promotionService.load(id);
        if (promotion == null) {
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "活动不存在");
        }
        return promotionService.deletedFlashSale(id);
    }

    private void changeExclusStatus(Long promotionId, Integer status) {
        this.listAllProductId(promotionId)
                .forEach(pId -> exclusiveMapper.updateStatus(pId, "FLASHSALE", status));
    }

    public boolean unClose(Long id) {
        Promotion promotion = promotionService.load(id);
        if (promotion == null) {
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "活动不存在");
        }
        if (!promotion.getClosed()) {
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "当前活动进行不，不需要重新发布");
        }
        boolean valid = promotion.isValid();
        if (!valid) {
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "活动已失效, 不能发布");
        }
        changeExclusStatus(promotion.getId(), 1);
        return promotionService.unClose(id);
    }

    public boolean updateStatus(String id, String status) {
        FlashSaleApplyStatus statusEnum = FlashSaleApplyStatus.valueOf(status);
        return promotionFlashSaleMapper.updateFlashSaleStatus(id, statusEnum) > 0;
    }

    public String selectSystemSkuId(){
        String s = promotionFlashSaleMapper.selectSystemSkuId();
        return s;
    }
    public List<Long> selectXquarkSkuId(String skuCode){
        List<Long> skuId = promotionFlashSaleMapper.selectXquarkSkuId(skuCode);
        return skuId;
    }
}
