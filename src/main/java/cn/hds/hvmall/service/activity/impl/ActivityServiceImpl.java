package cn.hds.hvmall.service.activity.impl;

import cn.hds.hvmall.mapper.activity.ActivityMapper;
import cn.hds.hvmall.pojo.activity.ActivityInfo;
import cn.hds.hvmall.pojo.activity.ActivityVO;
import cn.hds.hvmall.pojo.activity.PtDetail;
import cn.hds.hvmall.pojo.list.ExecuteResult;
import cn.hds.hvmall.service.activity.ActivityService;

import java.util.ArrayList;
import java.util.List;

import cn.hds.hvmall.utils.list.GlobalErrorCode;
import cn.hds.hvmall.utils.list.qiniu.BizException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("activityService")
public class ActivityServiceImpl implements ActivityService {

  private final ActivityMapper activityMapper;

  @Autowired
  public ActivityServiceImpl(ActivityMapper activityMapper) {
    this.activityMapper = activityMapper;
  }

  @Override
  public ExecuteResult<List<ActivityVO>> searchActivity(ActivityInfo activityInfo, int pageSize) {
    int pageNumber = (activityInfo.getPageNum() - 1) * pageSize;
    List<ActivityVO> list = activityMapper.searchActivity(activityInfo, pageNumber, pageSize);
    int count = activityMapper.countActivity(activityInfo);
    ExecuteResult<List<ActivityVO>> result = new ExecuteResult<>();
    result.setResult(list);
    result.setCount(count);
    return result;
  }

  /**
   * 监控拼团活动
   *
   * @param activityInfo activityInfo
   * @param pageSize     pageSize
   * @return ExecuteResult
   */
  @Override
  public ExecuteResult<List<ActivityVO>> searchPtActivity(ActivityInfo activityInfo, int pageSize) {
    int pageNumber = (activityInfo.getPageNum() - 1) * pageSize;
    ExecuteResult<List<ActivityVO>> result = new ExecuteResult<>();
    List<ActivityVO> list = activityMapper.searchPtActivity(activityInfo, pageNumber, pageSize);

    if (list.size() == 0) {
      result.setIsSuccess(0);
    } else {
      result.setIsSuccess(1);
    }
    for (ActivityVO aList : list) {
      aList.setProductName(activityMapper.searchSkuname(aList.getPieceGroupTranCode()));
      aList.setGroupOpenMemberNickName(activityMapper.searchUserName(aList.getGroupOpenMemberId() + ""));
      aList.setGroupHeadIdName(activityMapper.searchUserName(aList.getGroupHeadId() + ""));
      //成团状态才有成团时间 赋值成团时间
      if (aList.getPieceStatus().equals("2")) {
        aList.setPromotionPgInfoUpdatedAt(activityMapper.successGroupTime(aList.getPieceGroupTranCode()));
      }
    }
    int count = activityMapper.countActivity(activityInfo);
    result.setResult(list);
    result.setCount(count);
    return result;
  }

  @Override
  public ExecuteResult<List<PtDetail>> searchPtOrder(String code) {
    List<PtDetail> list = activityMapper.searchPtOrder(code);
    ExecuteResult<List<PtDetail>> result = new ExecuteResult<>();
    if (list.size() == 0) {
      result.setIsSuccess(0);
    } else {
      result.setIsSuccess(1);
    }
    for (PtDetail aList : list) {
      aList.setMemberName(activityMapper.searchUserName(aList.getMemberId() + ""));
      aList.setProductName(activityMapper.searchSkuname(aList.getGroupCode()));
      aList.setPtSuccessTime(activityMapper.searchPtTime(aList.getGroupCode(), aList.getMemberId() + ""));
    }
    result.setCount(list.size());
    result.setResult(list);
    return result;
  }
}
