package cn.hds.hvmall.service.activity;

import cn.hds.hvmall.pojo.activity.ActivityInfo;
import cn.hds.hvmall.pojo.activity.ActivityVO;
import cn.hds.hvmall.pojo.activity.PtDetail;
import cn.hds.hvmall.pojo.list.ExecuteResult;
import java.util.List;

/**
 * @Author: zzl
 * @Date: 2018/9/19 10:36
 * @Version
 */
public interface ActivityService {

  /**
   * 根据条件查询活动
   * @param infoSimple
   * @param pageSize
   * @return
   */
  ExecuteResult<List<ActivityVO>> searchActivity(ActivityInfo infoSimple, int pageSize);

  ExecuteResult<List<ActivityVO>> searchPtActivity(ActivityInfo activityInfo, int i);
  ExecuteResult<List<PtDetail>> searchPtOrder(String ptCode);
}
