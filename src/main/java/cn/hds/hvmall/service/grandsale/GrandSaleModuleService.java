package cn.hds.hvmall.service.grandsale;

import cn.hds.hvmall.entity.grandsale.GrandSaleModule;
import cn.hds.hvmall.pojo.grandsale.GrandSaleModuleVO;
import cn.hds.hvmall.pojo.grandsale.PromotionVO;
import cn.hds.hvmall.service.grandsale.vo.GrandSaleModuleSortVO;
import cn.hds.hvmall.type.PromotionType;

import java.util.List;
import java.util.Map;

public interface GrandSaleModuleService {
  int deleteByPrimaryKey(Integer id);

  int insert(GrandSaleModuleVO grandSaleModuleVO);

  int insertSelective(GrandSaleModule record);

  GrandSaleModule selectByPrimaryKey(Integer id);

  Map selectAll(Integer grandSaleId);

  List<PromotionVO> selectPromotion(PromotionType promotionType);

  int updateByPrimaryKeySelective(GrandSaleModule record);

  int updateByPrimaryKey(GrandSaleModule record);

  boolean updateStatusById(String promotionId, Integer status, PromotionType promotionType);

  boolean updateSortByType(GrandSaleModuleSortVO grandSaleModuleSortVO);

  /**
   * 将活动绑定到会场中
   * @param promotionType 活动类型
   * @param promotionId 活动id
   * @return 绑定结果
   */
  int bindGrand(PromotionType promotionType,Long promotionId);
}