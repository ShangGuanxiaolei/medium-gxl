package cn.hds.hvmall.service.grandsale.impl;

import cn.hds.hvmall.base.RedisService;
import cn.hds.hvmall.entity.BaseEntityImpl;
import cn.hds.hvmall.entity.category.CategoryForPromotion;
import cn.hds.hvmall.entity.grandsale.GrandSaleShare;
import cn.hds.hvmall.entity.grandsale.GrandSaleStadium;
import cn.hds.hvmall.entity.list.ProductSkuSimpleVO;
import cn.hds.hvmall.entity.product.Product;
import cn.hds.hvmall.entity.sku.Sku;
import cn.hds.hvmall.enums.ProductStatus;
import cn.hds.hvmall.mapper.grandsale.GrandSalePromotionMapper;
import cn.hds.hvmall.mapper.grandsale.GrandSaleShareMapper;
import cn.hds.hvmall.mapper.freshmantab.FreshManXquarkSkuCopyMapper;
import cn.hds.hvmall.mapper.freshmantab.FreshmanMapper;
import cn.hds.hvmall.mapper.grandsale.GrandSaleStadiumMapper;
import cn.hds.hvmall.mapper.stockCheck.PromotionStockCheckMapper;
import cn.hds.hvmall.pojo.grandsale.*;
import cn.hds.hvmall.pojo.freshmantab.XquarkSkuCombine;
import cn.hds.hvmall.pojo.freshmantab.XquarkSkuCombineExtra;
import cn.hds.hvmall.pojo.grandsale.GrandSaleStadiumProductVO;
import cn.hds.hvmall.pojo.grandsale.GrandSaleStadiumVO;
import cn.hds.hvmall.pojo.grandsale.GrandSaleStadiumVOId;
import cn.hds.hvmall.pojo.grandsale.GrandSaleStadiumVOMap;
import cn.hds.hvmall.pojo.list.ProductSource;
import cn.hds.hvmall.pojo.list.Taxonomy;
import cn.hds.hvmall.pojo.list.TermRelationship;
import cn.hds.hvmall.pojo.prod.ProductSkuVO;
import cn.hds.hvmall.service.CopyAble;
import cn.hds.hvmall.service.category.CategoryService;
import cn.hds.hvmall.service.grandsale.GrandSaleStadiumService;
import cn.hds.hvmall.service.grandsale.vo.GrandSaleStadiumSortVO;
import cn.hds.hvmall.service.grandsale.vo.StadiumSortVO;
import cn.hds.hvmall.service.list.ProductService;
import cn.hds.hvmall.service.list.SkuService;
import cn.hds.hvmall.service.stockcheck.PromotionStockCheckService;
import cn.hds.hvmall.type.StadiumType;
import cn.hds.hvmall.utils.BeanUtil;
import cn.hds.hvmall.utils.DateUtils;
import cn.hds.hvmall.utils.excel.ColumnMapping;
import cn.hds.hvmall.utils.excel.ExcelUtils;
import cn.hds.hvmall.utils.list.GlobalErrorCode;
import cn.hds.hvmall.utils.list.qiniu.BizException;
import cn.hds.hvmall.utils.list.qiniu.IdTypeHandler;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import net.minidev.json.writer.BeansMapper;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;

@Slf4j
@Service
public class GrandSaleStadiumServiceImpl implements GrandSaleStadiumService, CopyAble {

    private final static String SUFFIX = "_old";

    private final static String STADIUM_TYPE = "MAIN";

    private final GrandSaleStadiumMapper grandSaleStadiumMapper;

    private final CategoryService categoryService;

    private final ProductService productService;

    private final SkuService skuService;
    @Autowired
    private FreshmanMapper freshmanMapper;

    @Autowired
    private FreshManXquarkSkuCopyMapper freshManXquarkSkuCopyMapper;

    private final GrandSaleShareMapper grandSaleShareMapper;

    private GrandSalePromotionMapper grandSalePromotionMapper;

    private final PromotionStockCheckService stockCheckService;

    @Autowired
    private RedisService redisService;

    @Autowired
    private PromotionStockCheckMapper stockCheckMapper;

    @Autowired
    public GrandSaleStadiumServiceImpl(GrandSaleStadiumMapper grandSaleStadiumMapper,
                                     CategoryService categoryService,
                                     ProductService productService,
                                     SkuService skuService,
                                     GrandSaleShareMapper grandSaleShareMapper,
                                     GrandSalePromotionMapper grandSalePromotionMapper,
                                     PromotionStockCheckService stockCheckService) {
    this.grandSaleStadiumMapper = grandSaleStadiumMapper;
    this.categoryService = categoryService;
    this.productService = productService;
    this.skuService = skuService;
    this.grandSaleShareMapper = grandSaleShareMapper;
    this.grandSalePromotionMapper = grandSalePromotionMapper;
    this.stockCheckService=stockCheckService;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteByPrimaryKey(Integer id) {
        //删除分会场的商品分类，商品及sku暂不删除，避免其他地方使用
        GrandSaleStadium grandSaleStadium = grandSaleStadiumMapper.selectOne(id);
        if (grandSaleStadium == null) {
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "会场信息不存在");
        }
        String type = grandSaleStadium.getType();
        if (StringUtils.isBlank(type)) {
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "会场类型为空");
        }
        //删除之前将分会场所属商品变更为下架状态
        Integer grandSaleId = grandSaleStadium.getGrandSaleId();
        List<CategoryForPromotion> categoryForPromotions = categoryService.selectCategoryName(type, grandSaleId);
        for (CategoryForPromotion categoryForPromotion : categoryForPromotions) {
            String categoryId = categoryForPromotion.getId();
            Integer integer = grandSaleStadiumMapper.updateStadiumInstock(type, categoryId);
            if(integer == 0){
                throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "分会场设置商品下架失败");
            }
        }

        if (!(categoryService.deleteCategory(type) > 0)) {
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "删除分类失败");
        }
        // 失效分会场分享文案
        disableStadiumShare(grandSaleStadium);
        return grandSaleStadiumMapper.deleteByPrimaryKey(id);
    }

    private void disableStadiumShare(GrandSaleStadium grandSaleStadium) {
        GrandSaleShare grandSaleShare = grandSaleShareMapper.selectStadiumByType(grandSaleStadium.getType());
        if (grandSaleShare != null && grandSaleShare.getStartAt() != null && grandSaleShare.getEndAt() != null) {
            final Date date = new Date();
            Date shareStartAt = grandSaleShare.getStartAt().before(date) ? grandSaleShare.getStartAt() : date;
            Date shareEndAt = grandSaleShare.getEndAt().after(date) ? date : grandSaleShare.getEndAt();
            grandSaleShare.setStartAt(shareStartAt);
            grandSaleShare.setEndAt(shareEndAt);
            grandSaleShare.setContent("");
            grandSaleShareMapper.updateByPrimaryKey(grandSaleShare);
        }
    }

    @Override
    @Transactional
    public int insert(GrandSaleStadiumVO grandSaleStadiumVO) {
        String type = grandSaleStadiumVO.getType();
        if (grandSaleStadiumMapper.selectByType(type) > 0) {
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "不能创建相同类型的分会场活动，请选择其他类型");
        }
        GrandSaleStadium grandSaleStadium = getGrandSaleStadium(grandSaleStadiumVO);
        insertCategory(grandSaleStadiumVO);
        insertOrUpdateSaleShare(grandSaleStadium);
        return grandSaleStadiumMapper.insert(grandSaleStadium);
    }

    /**
     * 新增分会场活动配置关联分享文案
     * @param grandSaleStadium 分会场活动配置
     */
    private void insertOrUpdateSaleShare(GrandSaleStadium grandSaleStadium) {
        GrandSaleShare grandSaleShare = grandSaleShareMapper.selectStadiumByType(grandSaleStadium.getType());
        if (grandSaleShare == null) {
            grandSaleShare = new GrandSaleShare();
            grandSaleShare.setType(grandSaleStadium.getType());
            grandSaleShare.setContent(grandSaleStadium.getShareContent());
            grandSaleShare.setStartAt(grandSaleStadium.getBeginTime());
            grandSaleShare.setEndAt(grandSaleStadium.getEndTime());
            grandSaleShareMapper.insert(grandSaleShare);
        } else {
            grandSaleShare.setContent(grandSaleStadium.getShareContent());
            grandSaleShare.setStartAt(grandSaleStadium.getBeginTime());
            grandSaleShare.setEndAt(grandSaleStadium.getEndTime());
            grandSaleShareMapper.updateByPrimaryKey(grandSaleShare);
        }
    }

    @Override
    @Transactional
    public int insertSelective(GrandSaleStadium record) {
        return grandSaleStadiumMapper.insertSelective(record);
    }

    @Override
    public GrandSaleStadiumVOMap selectOne(Integer stadiumId, Pageable pageable) {
        GrandSaleStadium grandSaleStadium = grandSaleStadiumMapper.selectOne(stadiumId);
        GrandSaleStadiumVOMap grandSaleStadiumVOMap = new GrandSaleStadiumVOMap();
        BeanUtils.copyProperties(grandSaleStadium, grandSaleStadiumVOMap);
        String bannerUrl = grandSaleStadium.getBannerUrl();
        String[] bannerUrlList = bannerUrl.split(",");
        grandSaleStadiumVOMap.setBannerUrl(bannerUrlList);
        String type = grandSaleStadium.getType();
        Integer grandSaleId = grandSaleStadium.getGrandSaleId();
        List<CategoryForPromotion> categoryForPromotions = categoryService.selectCategoryName(type, grandSaleId);
        List<Map<String, Object>> list = new ArrayList<>();
        for (CategoryForPromotion categoryForPromotion :
                categoryForPromotions) {
            String name = categoryForPromotion.getName();
            String categoryId = categoryForPromotion.getId();
            Map<String, Object> map = new HashMap<>();
            List<ProductSkuSimpleVO> productSkuSimpleVOList = productService.searchPromotionProduct(type, categoryId, pageable);
            for (ProductSkuSimpleVO productSkuSimpleVO :
                    productSkuSimpleVOList) {
                String sourceSkuCode = productSkuSimpleVO.getSourceSkuCode();
                Optional<Sku> sku = skuService.loadByCode(sourceSkuCode);
                Supplier<Optional<Sku>> supplier = () -> sku;
                Sku sourceSku = supplier.get().orElseThrow(BizException.build(GlobalErrorCode.UNKNOWN, "商品规格信息不存在"));
                productSkuSimpleVO.setSkuAmount(sourceSku.getAmount());
                productSkuSimpleVO.setSkuPrice(sourceSku.getPrice());
                BigDecimal convertPrice = productSkuSimpleVO.getPrice()
                        .multiply(BigDecimal.TEN)
                        .subtract(productSkuSimpleVO.getDeductionDPoint())
                        .divide(BigDecimal.TEN, BigDecimal.ROUND_HALF_EVEN);
                productSkuSimpleVO.setConvertPrice(convertPrice);
            }
            Map<String, Object> tempMap = new HashMap<>();
            tempMap.put("category", name);
            tempMap.put("product", productSkuSimpleVOList);
            map.put("productList", tempMap);
            map.put("count", productService.countPromotionProduct(type, categoryId));
            list.add(map);
        }
        GrandSaleShare grandSaleShare = grandSaleShareMapper.selectStadiumByType(grandSaleStadiumVOMap.getType());
        if (grandSaleShare != null)
            grandSaleStadiumVOMap.setShareContent(grandSaleShare.getContent());
        grandSaleStadiumVOMap.setProductMap(list);
        return grandSaleStadiumVOMap;
    }

    @Override
    public List<GrandSaleStadium> selectAllByGrandSaleId(Integer grandSaleId) {
        List<GrandSaleStadium> grandSaleStadiums = grandSaleStadiumMapper.selectAllByGrandSaleId(grandSaleId);
        for (GrandSaleStadium grandSaleStadium :
                grandSaleStadiums) {
            String bannerUrl = grandSaleStadium.getBannerUrl();
            if (StringUtils.isNotBlank(bannerUrl)) {
                String[] banner = bannerUrl.split(",");
                grandSaleStadium.setBannerUrl(banner[0]);
            }
        }
        return grandSaleStadiums;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKeySelective(GrandSaleStadium record) {
        return grandSaleStadiumMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateByPrimaryKey(GrandSaleStadium record) {
        return grandSaleStadiumMapper.updateByPrimaryKey(record);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int update(GrandSaleStadiumVOId record) {
        String type = record.getType();
        if (StringUtils.isBlank(type)) {
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "分会场类型不能为空");
        }
        //定义类型存储
        String oldType = type + SUFFIX;

        this.deleteTermRelationship(type,oldType);

        //调用更新接口
        this.updateCategory(record, oldType);

        //删除原始商品数据
        if (!(categoryService.deleteProduct(oldType) > 0)) {
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "修改数据失败");
        }
        GrandSaleStadium grandSaleStadium = getGrandSaleStadium(record);
        grandSaleStadium.setId(record.getStadiumId());
        int result = grandSaleStadiumMapper.updateByPrimaryKeySelective(grandSaleStadium);
        insertOrUpdateSaleShare(grandSaleStadium);
        if (!(result > 0)) {
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "更新分会场信息失败");
        }
        return result;
    }

    private void deleteTermRelationship(String type,String oldType){
        if (oldType == null){
            oldType = type + SUFFIX;
        }
        //执行更新类型操作，避免删除时删除了新增的数据
        List<TermRelationship> termRelationships = categoryService.selectByObjType(type);
        for (TermRelationship termRelationship : termRelationships) {
            String id = termRelationship.getId();
            if (StringUtils.isBlank(id)) {
                throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "relationshipId不能为空");
            }
            if (!(categoryService.updateTermRelationshipByType(oldType, id) > 0)) {
                throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "更新类型失败");
            }
        }
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateGrandSaleStadium(GrandSaleStadiumVOId record){
        ArrayList<String> oldProductIds = new ArrayList<>();
        Integer grandSaleId = record.getGrandSaleId();
        String type = record.getType();
        List<CategoryForPromotion> categoryForPromotions = categoryService.selectCategoryName(type, grandSaleId);
        for (CategoryForPromotion categoryForPromotion : categoryForPromotions) {
            String categoryId = categoryForPromotion.getId();
            List<String> stadiumProductId = grandSaleStadiumMapper.getStadiumProductId(type, categoryId);
            for (String s : stadiumProductId) {
                oldProductIds.add(s);
            }
        }

        ArrayList<Object> newProductIds = new ArrayList<>();
        List<GrandSaleStadiumProductVO> productVO = record.getProductVO();
        for (GrandSaleStadiumProductVO grandSaleStadiumProductVO : productVO) {
            ProductSkuVO productSkuVO = grandSaleStadiumProductVO.getProductSkuVO();
            List<Sku> skuList = productSkuVO.getSkuList();
            for (Sku sku : skuList) {
                String productId = sku.getProductId();
                newProductIds.add(productId);
            }
        }
        oldProductIds.removeAll(newProductIds);
        for (String oldProductId : oldProductIds) {
            grandSaleStadiumMapper.updateStadiumProductId(oldProductId);
        }
        // 校验type不为空
        Optional.of(record)
                .filter(r -> StringUtils.isNotBlank(r.getType()))
                .orElseThrow(BizException.build(GlobalErrorCode.INTERNAL_ERROR, "分会场类型不能为空"));

        //执行更新类型操作，避免删除时删除了新增的数据
        this.deleteTermRelationship(record.getType(),null);
        if (categoryService.deleteCategory(record.getType()+SUFFIX)<=0) {
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "修改数据失败");
        }

        // 获取是否已经被复制过 如果已经被复制过 则取source_sku_code
        // 否则获取他们的skuCode
        record.getProductVO().forEach(s -> {
            s.getProductSkuVO().getSkuList().stream()
                    .map(sku -> {
                        StadiumVO stadiumVO = new StadiumVO();
                        String skuCode = "";
                        // 导入编辑操作
                        if (StringUtils.isBlank(sku.getProductId()) || StringUtils.isBlank(sku.getSkuId())) {
                            Optional<Sku> importSku = skuService.loadByCode(sku.getSkuCode());
                            Supplier<Optional<Sku>> supplier = () -> importSku;
                            Sku sourceSku = supplier.get().orElseThrow(BizException.build(GlobalErrorCode.UNKNOWN, "商品规格信息不存在, sku_code=" + sku.getSkuCode()));
                            skuCode = sourceSku.getSkuCode();
                            if (sku.getAmount() > sourceSku.getAmount())
                                throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "导入商品库存不能大于原商品库存,skuCode=" + sku.getSkuCode() +  " 的原商品库存skuAmount=" + sourceSku.getAmount());
                        }
                        if (StringUtils.isBlank(skuCode))
                            skuCode = skuService.selectSkuCode(sku);
                        BeanUtils.copyProperties(sku, stadiumVO);
                        stadiumVO.setSkuCode(skuCode.replace(record.getType() + record.getGrandSaleId(), ""));
                        stadiumVO.setTermName(s.getCategory());
                        return stadiumVO;
                    }).collect(Collectors.toMap(
                            StadiumVO::getTermName,
                            stadiumVO -> Lists.newArrayList(Collections.singletonList(stadiumVO)),
                            (v1, v2) -> {
                                v1.addAll(v2);
                                return v1;
                            }
                    )).forEach((k,v) -> copyOrUpdateImport(k,StadiumType.valueOf(record.getType()),v,record.getGrandSaleId()));
        });
        GrandSaleStadium grandSaleStadium = getGrandSaleStadium(record);
        grandSaleStadium.setId(record.getStadiumId());
        grandSaleStadiumMapper.updateByPrimaryKeySelective(grandSaleStadium);
        insertOrUpdateSaleShare(grandSaleStadium);
    }

    @Override
    public boolean updateSortById(GrandSaleStadiumSortVO grandSaleStadiumSortVO) {
        List<StadiumSortVO> stadiumSortVOS = grandSaleStadiumSortVO.getStadiumSortVOS();
        boolean flag = true;
        for (StadiumSortVO stadiumSortVO :
                stadiumSortVOS) {
            Integer id = stadiumSortVO.getId();
            Integer sort = stadiumSortVO.getSort();
            if (!(grandSaleStadiumMapper.updateSortById(id, sort) > 0)) {
                flag = false;
            }
        }
        return flag;
    }

    @Override
    public boolean save(BaseEntityImpl baseEntity) {
        return false;
    }

    @Override
    @Transactional
    public int insertGrandSaleShare(GrandSaleMainShareVO[] grandSaleMainShareVOS) {
        GrandSaleMainShareVO firstGrandSaleMainShareVO = grandSaleMainShareVOS[0];
//        boolean b = grandSaleShareMapper.selectMainByDate(firstGrandSaleMainShareVO.getMainStartAt());
        Date lastDate = grandSaleShareMapper.selectLastMainShare();
        try {
//            if (lastDate == null || firstGrandSaleMainShareVO.getMainStartAt().getTime()==lastDate.getTime()) {
                for (GrandSaleMainShareVO grandSaleMainShareVO : grandSaleMainShareVOS) {
                    GrandSaleShare grandSaleShare = new GrandSaleShare();
                    grandSaleShare.setStartAt(grandSaleMainShareVO.getMainStartAt());
                    grandSaleShare.setEndAt(grandSaleMainShareVO.getMainEndAt());
                    grandSaleShare.setContent(grandSaleMainShareVO.getMainShareContent());
                    grandSaleShare.setType(STADIUM_TYPE);
                    grandSaleShareMapper.insert(grandSaleShare);
                }
                return 1;
//            } else {
//                return 0;
//            }
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public Map<String, Object> selectAllMainShare(Pageable pageable) {
        final Long aLong = grandSaleShareMapper.countAllMainShare();
        List<GrandSaleShare> grandSaleShares = grandSaleShareMapper.selectAllMainShare(pageable);
        Map aRetMap = new HashMap();
        aRetMap.put("mainShares", grandSaleShares);
        aRetMap.put("totalCount", aLong);
        return aRetMap;
    }

    @Override
    public int updateGrandSaleMainShare(GrandSaleMainShareVO grandSaleMainShareVO) {
        GrandSaleShare grandSaleShare = grandSaleShareMapper.selectByPrimaryKey(grandSaleMainShareVO.getId());
        if (grandSaleShare != null)
            grandSaleShare.setContent(grandSaleMainShareVO.getMainShareContent());
        return grandSaleShareMapper.updateByPrimaryKey(grandSaleShare);
    }

    /**
     * 最晚的主会场分享文案
     * @return
     */
    @Override
    public Date selectLastMainShare() {
        Date date = grandSaleShareMapper.selectLastMainShare();
        String time;
        if (date == null) {
            redisService.expireAt("localTime", org.apache.commons.lang3.time.DateUtils.addSeconds(new Date(), 30));
            time = redisService.get("localTime");
            if (StringUtils.isBlank(time)) {
                String dateStr = DateUtils.formatDate(new Date(), "yyyy-MM-dd HH:mm:ss");
                redisService.set("localTime", dateStr);
                time = redisService.get("localTime");
            }
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            try {
                date = dateFormat.parse(time);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return date;
    }

    /**
     * 封装获取分会场对象
     *
     * @param grandSaleStadiumVO 分会场对象VO
     * @return 分会场对象
     */
    private GrandSaleStadium getGrandSaleStadium(GrandSaleStadiumVO grandSaleStadiumVO) {
        Date beginTime = DateUtils.stringFormatDate(grandSaleStadiumVO.getBeginTime());
        Date endTime = DateUtils.stringFormatDate(grandSaleStadiumVO.getEndTime());
        Date appointmentShowTime=DateUtils.stringFormatDate(grandSaleStadiumVO.getAppointmentShowTime());
        Date now = new Date();
        if (endTime.before(now)) {
            //活动时间已结束
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "活动结束时间不能早于当前时间");
        }
        List<String> bannerUrl = grandSaleStadiumVO.getBannerUrl();
        GrandSaleStadium grandSaleStadium = new GrandSaleStadium();
        BeanUtils.copyProperties(grandSaleStadiumVO, grandSaleStadium);
        grandSaleStadium.setBeginTime(beginTime);
        grandSaleStadium.setEndTime(endTime);
        grandSaleStadium.setAppointmentShowTime(appointmentShowTime);
        grandSaleStadium.setBannerUrl(StringUtils.join(bannerUrl.toArray(), ","));
        return grandSaleStadium;
    }

    /**
     * 保存分类
     *
     * @param name 分类名称
     * @return 添加成功的分类对象
     */
    private CategoryForPromotion save(String name) {
        return categoryService.save(name, Taxonomy.ACTIVITY, null, "17481953", "6102910", ProductSource.CLIENT, null);
    }

    /**
     * 封装插入分类商品数据
     *
     * @param grandSaleStadiumVO 分会场vo对象
     */
    private void insertCategory(GrandSaleStadiumVO grandSaleStadiumVO) {
        String type = grandSaleStadiumVO.getType();
        List<GrandSaleStadiumProductVO> productVO = grandSaleStadiumVO.getProductVO();
        Integer grandSaleId = grandSaleStadiumVO.getGrandSaleId();
        for (GrandSaleStadiumProductVO grandSaleStadiumProductVO :
                productVO) {
            ProductSkuVO productSkuVO = grandSaleStadiumProductVO.getProductSkuVO();
            List<Sku> skuList = productSkuVO.getSkuList();
            String category = grandSaleStadiumProductVO.getCategory();
            //保存分类
            CategoryForPromotion categoryForPromotion = save(category);
            Set<String> productIds = null;
            Set<String> skuIdList = null;
            Product newProduct = null;
            for (Sku sku :
                    skuList) {
                //商品id可能重复
                String productId = sku.getProductId();
                // 导入编辑操作
                if (StringUtils.isBlank(productId) || StringUtils.isBlank(sku.getSkuId())) {
                    Optional<Sku> importSku = skuService.loadByCode(sku.getSkuCode());
                    Supplier<Optional<Sku>> supplier = () -> importSku;
                    Sku sourceSku = supplier.get().orElseThrow(BizException.build(GlobalErrorCode.UNKNOWN, "商品规格信息不存在, sku_code=" + sku.getSkuCode()));
                    // 设置 productId
                    productId = sourceSku.getProductId();
                    if (sku.getAmount() > sourceSku.getAmount())
                        throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "导入商品库存不能大于原商品库存,skuCode=" + sku.getSkuCode() +  " 的原商品库存skuAmount=" + sourceSku.getAmount());
                    //设置 skuId
                    sku.setSkuId(String.valueOf(sourceSku.getId()));

                }
                //校验库存
                stockCheckService.stockCheck(sku.getAmount(),Integer.parseInt(sku.getSkuId()),null,"",null);
                newProduct = copyProduct(productIds, newProduct, sku, productId, categoryForPromotion, type, grandSaleId);
                String newProductId =
                        Optional.of(newProduct.getId().toString())
                                .orElseThrow(BizException.build(GlobalErrorCode.INVALID_ARGUMENT, "商品不存在"));
                //保存复制的sku
                copySku(skuIdList, type, sku, newProductId, grandSaleId);

                //查询原商品是否是套装商品
                Boolean combineProduct = freshmanMapper.isCombineProduct(newProduct.getSourceId());
                if(combineProduct==true){
                    cn.hds.hvmall.pojo.freshmantab.XquarkSku xquarkSku = freshManXquarkSkuCopyMapper.selectSkuIdByProductId(newProduct.getId());
                    XquarkSkuCombine xquarkSkuCombine = new XquarkSkuCombine();
                    xquarkSkuCombine.setProductId(newProduct.getId());
                    xquarkSkuCombine.setSkuId(xquarkSku.getId());
                    //插入套装表xquark_sku_combine
                    int i4 = freshmanMapper.insertXquarkSkuCombine(xquarkSkuCombine);
                    if (i4==0){
                        throw  new BizException(GlobalErrorCode.valueOf("插入xquark_sku_combine表数据失败:"+newProduct.getId()));
                    }
                    XquarkSkuCombine sourceCombineId = freshmanMapper.selectCombineIdByProductId(newProduct.getSourceId());
                    List<XquarkSkuCombineExtra> xquarkSkuCombineExtras = freshmanMapper.selectCombineExtraByMasterId(sourceCombineId.getId());
                    for (XquarkSkuCombineExtra xquarkSkuCombineExtra : xquarkSkuCombineExtras) {
                        XquarkSkuCombineExtra xquarkSkuCombineExtra1 = new XquarkSkuCombineExtra();
                        xquarkSkuCombineExtra1.setMasterId(xquarkSkuCombine.getId());
                        xquarkSkuCombineExtra1.setSlaveId(xquarkSkuCombineExtra.getSlaveId());
                        xquarkSkuCombineExtra1.setAmount(xquarkSkuCombineExtra.getAmount());
                        Integer integer2 = freshmanMapper.insertCombineExtra(xquarkSkuCombineExtra1);
                        if (integer2==0){
                            throw  new BizException(GlobalErrorCode.valueOf("插入xquark_sku_combine_extra表数据失败:"+newProduct.getId()));
                        }
                    }
                }

                //已经添加过的商品id加入到集合中
                productIds = new HashSet<>();
                productIds.add(productId);
                skuIdList = new HashSet<>();
                skuIdList.add(sku.getSkuId());
            }
        }
    }

    /**
     * 封装更新分类商品数据
     *
     * @param grandSaleStadiumVO 分会场vo对象
     * @param oldType            修改后的分会场类型
     */
    private void updateCategory(GrandSaleStadiumVO grandSaleStadiumVO, String oldType) {
        String type = grandSaleStadiumVO.getType();
        //删除之前先查询所有分类数据
        List<TermRelationship> termRelationships = categoryService.selectByObjType(oldType);
        //删除原始分类数据
        if (!(categoryService.deleteCategory(oldType) > 0)) {
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "修改数据失败");
        }
        List<GrandSaleStadiumProductVO> productVO = grandSaleStadiumVO.getProductVO();
        Integer grandSaleId = grandSaleStadiumVO.getGrandSaleId();
        for (GrandSaleStadiumProductVO grandSaleStadiumProductVO :
                productVO) {
            ProductSkuVO productSkuVO = grandSaleStadiumProductVO.getProductSkuVO();
            List<Sku> skuList = productSkuVO.getSkuList();
            String category = grandSaleStadiumProductVO.getCategory();
            //保存分类
            CategoryForPromotion categoryForPromotion = save(category);

            Set<String> productIds = null;
            Set<String> skuIdList = null;
            Set<String> productIdList = new HashSet<>();
            Product newProduct = null;
            for (TermRelationship termRelationship :
                    termRelationships) {
                String objId = termRelationship.getObjId();
                productIdList.add(objId);
            }

            for (Sku sku : skuList) {
                String productId = sku.getProductId();
                //存在的商品执行更新操作，否则执行添加操作
                if (productIdList.contains(productId)) {
                    updateProduct(productIds, sku, productId, categoryForPromotion, type, grandSaleId);
                    updateSku(sku);
                } else {
                    newProduct = copyProduct(productIds, newProduct, sku, productId, categoryForPromotion, type, grandSaleId);
                    String newProductId =
                            Optional.of(newProduct.getId().toString())
                                    .orElseThrow(BizException.build(GlobalErrorCode.INVALID_ARGUMENT, "商品不存在"));
                    //保存复制的sku
                    copySku(skuIdList, type, sku, newProductId, grandSaleId);
                }
                //已经添加过的商品id加入到集合中
                productIds = new HashSet<>();
                productIds.add(productId);
                skuIdList = new HashSet<>();
                skuIdList.add(sku.getSkuId());
            }
        }
    }

  private Product copyProduct(Set<String> productIds, Product newProduct,
                              Sku sku, String productId,
                              CategoryForPromotion categoryForPromotion,
                              String type, Integer grandSaleId) {
    //商品id不存在集合中才执行操作
    if (productIds == null || productIds.isEmpty() || !productIds.contains(productId)) {
      Supplier<Optional<Product>> product = () -> productService.load(Long.valueOf(productId));
      newProduct =
              product.get().orElseThrow(BizException.build(GlobalErrorCode.INVALID_ARGUMENT, "商品不存在"));
      Integer buyLimit = sku.getBuyLimit();
      newProduct.setBuyLimit(Optional.ofNullable(buyLimit).orElse(0));
      newProduct.setStatus(ProductStatus.ONSALE);//修改为已上架
      newProduct.setPrice(Optional.ofNullable(sku.getPrice()).orElse(newProduct.getPrice()));
      newProduct.setDeductionDPoint(Optional.ofNullable(sku.getDeductionDPoint()).orElse(newProduct.getDeductionDPoint()));
      newProduct.setSourceId(Long.valueOf(productId));//设置源商品id
      //保存复制的product
      productService.savePd(newProduct);
      Long newProductId =
              Optional.of(newProduct.getId())
                      .orElseThrow(BizException.build(GlobalErrorCode.INVALID_ARGUMENT, "商品不存在"));
      String code = IdTypeHandler.encode(newProductId);
      newProduct.setCode(code);
      //更新复制后code
      productService.updateByPrimaryKeySelective(newProduct);
      //保存分类
      categoryService.addProductCategory(newProductId.toString(), categoryForPromotion.getId(), type, grandSaleId);

    }
    return newProduct;
  }

  private void copySku(Set<String> skuIdList, String type, Sku sku, String newProductId, Integer grandSaleId) {
    if (skuIdList == null || skuIdList.isEmpty() || !skuIdList.contains(sku.getSkuId())) {
      String skuId = sku.getSkuId();
      Supplier<Optional<Sku>> supplier = () -> skuService.loadById(Long.valueOf(skuId));
      Sku oldSku = supplier.get().orElseThrow(BizException.build(GlobalErrorCode.INVALID_ARGUMENT, "sku不存在"));
      final String skuCode = oldSku.getSkuCode();
      final String oldSkuCode = skuCode + type + grandSaleId;
      // 检查本场活动是否已经添加
      skuService.loadByCode(oldSkuCode).ifPresent(s -> {
          throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "规格 " + skuCode + " 已经添加到该会场，请勿重复添加");
      });
//        Optional.of(skuService.checkIsNotDelete(oldSkuCode)).filter(s -> s).ifPresent(s -> {
//            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "规格 " + skuCode + " 已经添加到该会场，请勿重复添加");
//        });
      Sku newSku = skuService.copy(oldSku, Sku.class, s -> {
        s.setProductId(newProductId);//设置复制后的productId
        s.setAmount(Optional.ofNullable(sku.getAmount()).orElse(oldSku.getAmount()));
        s.setPrice(Optional.ofNullable(sku.getPrice()).orElse(oldSku.getPrice()));
        s.setNetWorth(Optional.ofNullable(sku.getNetWorth()).orElse(oldSku.getNetWorth()));
        s.setPoint(Optional.ofNullable(sku.getPoint()).orElse(oldSku.getPoint()));
        s.setPromoAmt(Optional.ofNullable(sku.getPromoAmt()).orElse(oldSku.getPromoAmt()));
        s.setDeductionDPoint(Optional.ofNullable(sku.getDeductionDPoint()).orElse(oldSku.getDeductionDPoint()));
        s.setServerAmt(Optional.ofNullable(sku.getServerAmt()).orElse(oldSku.getServerAmt()));
        s.setPromoAmt(Optional.ofNullable(sku.getPromoAmt()).orElse(oldSku.getServerAmt()));
        s.setSecureAmount(0);
        s.setSkuCode(oldSkuCode);//加上活动类型后缀生成新的sku_code
        s.setSourceSkuCode(oldSku.getSkuCode());//设置原skuCode
        s.setPromotionType(type);
      });
      Long newSkuId = newSku.getId();
      String code = IdTypeHandler.encode(newSkuId);
      newSku.setCode(code);
      skuService.update(newSku);
      log.info("保存成功{}", newSku);
    }
  }

    private void updateProduct(Set<String> productIds, Sku sku, String productId,
                               CategoryForPromotion categoryForPromotion,
                               String type, Integer grandSaleId) {
        //商品id不存在集合中才执行操作
        if (productIds == null || productIds.isEmpty() || !productIds.contains(productId)) {
            Supplier<Optional<Product>> product = () -> productService.load(Long.valueOf(productId));
            Product newProduct =
                    product.get().orElseThrow(BizException.build(GlobalErrorCode.INVALID_ARGUMENT, "商品不存在"));
            Integer buyLimit = sku.getBuyLimit();
            newProduct.setBuyLimit(Optional.ofNullable(buyLimit).orElse(0));
            newProduct.setPrice(Optional.ofNullable(sku.getPrice()).orElse(newProduct.getPrice()));
            newProduct.setDeductionDPoint(Optional.ofNullable(sku.getDeductionDPoint()).orElse(newProduct.getDeductionDPoint()));
            //更新product
            productService.updateByPrimaryKeySelective(newProduct);
            String newProductId =
                    Optional.of(newProduct.getId().toString())
                            .orElseThrow(BizException.build(GlobalErrorCode.INVALID_ARGUMENT, "商品不存在"));
            //添加分类
            categoryService.addProductCategory(newProductId, categoryForPromotion.getId(), type, grandSaleId);
        }
    }

    private void updateSku(Sku sku) {
        //更新sku
        String skuId = sku.getSkuId();
        Supplier<Optional<Sku>> supplier = () -> skuService.loadById(Long.valueOf(skuId));
        Sku oldSku = supplier.get().orElseThrow(BizException.build(GlobalErrorCode.INVALID_ARGUMENT, "sku不存在"));
        Sku newSku = skuService.update(oldSku, Sku.class, s -> {
            s.setAmount(Optional.ofNullable(sku.getAmount()).orElse(oldSku.getAmount()));
            s.setPrice(Optional.ofNullable(sku.getPrice()).orElse(oldSku.getPrice()));
            s.setNetWorth(Optional.ofNullable(sku.getNetWorth()).orElse(oldSku.getNetWorth()));
            s.setPoint(Optional.ofNullable(sku.getPoint()).orElse(oldSku.getPoint()));
            s.setPromoAmt(Optional.ofNullable(sku.getPromoAmt()).orElse(oldSku.getPromoAmt()));
            s.setDeductionDPoint(Optional.ofNullable(sku.getDeductionDPoint()).orElse(oldSku.getDeductionDPoint()));
            s.setServerAmt(Optional.ofNullable(sku.getServerAmt()).orElse(oldSku.getServerAmt()));
            s.setPromoAmt(Optional.ofNullable(sku.getPromoAmt()).orElse(oldSku.getServerAmt()));
            s.setSecureAmount(0);
        });
        log.info("更新成功{}", newSku);
    }


    private Map<String, ColumnMapping> createColumnMapping() {
        Map<String, ColumnMapping> map = new HashMap<>(3);
        map.put("类目名", new ColumnMapping<>("termName", ColumnMapping.STRING_TO_STRING));
        map.put("skuCode", new ColumnMapping<>("skuCode", ColumnMapping.STRING_TO_STRING));
        map.put("限购数量", new ColumnMapping<>("buyLimit", ColumnMapping.STRING_TO_INTEGER));
        map.put("活动价", new ColumnMapping<>("price", ColumnMapping.STRING_TO_DECIMAL));
        map.put("兑换价", new ColumnMapping<>("convertPrice", ColumnMapping.STRING_TO_DECIMAL));
        map.put("德分", new ColumnMapping<>("deductionDPoint", ColumnMapping.STRING_TO_DECIMAL));
        map.put("立减", new ColumnMapping<>("point", ColumnMapping.STRING_TO_DECIMAL));
        map.put("推广费", new ColumnMapping<>("promoAmt", ColumnMapping.STRING_TO_DECIMAL));
        map.put("服务费", new ColumnMapping<>("serverAmt", ColumnMapping.STRING_TO_DECIMAL));
        map.put("净值", new ColumnMapping<>("netWorth", ColumnMapping.STRING_TO_DECIMAL));
        map.put("活动库存", new ColumnMapping<>("amount", ColumnMapping.STRING_TO_INTEGER));
        return map;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void importData(InputStream is, Integer grandId) throws IOException, InvalidFormatException {

        boolean b = grandSalePromotionMapper.existsGrandSalePromotionById(grandId);
        if (!b) {
            throw BizException.build(GlobalErrorCode.INTERNAL_ERROR, "需要导入的主会场id:" + grandId + " 不存在").get();
        }

        log.info("开始导入分会场商品数据");

        Workbook workbook = ExcelUtils.createCommonWorkbook(is);

        Map<Sheet, StadiumType> sheetMap = Maps.newHashMap();

        for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
            Sheet sheetAt = workbook.getSheetAt(i);
            StadiumType stadiumType = StadiumType.valueOfDesc(sheetAt.getSheetName());
            log.info("sheet页:{},{},品类:{}", i + 1, sheetAt.getSheetName(), stadiumType);
            if (Objects.equals(stadiumType, null) || sheetAt.getLastRowNum() == 0) {
                log.debug("sheet页 {} 数据或品类不存在,跳过导入", sheetAt.getSheetName());
                continue;
            }

            Optional.of(grandSaleStadiumMapper.checkStadiumIsCreate(String.valueOf(stadiumType), grandId))
                    .filter(s -> s)
                    .orElseThrow(BizException.build(GlobalErrorCode.INTERNAL_ERROR, stadiumType.desc + "分会场还未被创建,请先创建对应的分会场"));

            sheetMap.put(sheetAt, stadiumType);

        }

        sheetMap.forEach((sheet, st) -> {
            List<StadiumVO> stadium = ExcelUtils.excelImport(sheet, createColumnMapping(), StadiumVO.class);

            Map<String, List<StadiumVO>> stadiumMap = stadium.stream().collect(
                    Collectors.toMap(
                            StadiumVO::getTermName,
                            s -> Lists.newArrayList(Collections.singletonList(s)),
                            (v1, v2) -> {
                                v1.addAll(v2);
                                return v1;
                            }
                    ));

            //key : categoryName value:stadiumVO
            for (Map.Entry<String, List<StadiumVO>> entry : stadiumMap.entrySet()) {
                copyOrUpdateImport(entry.getKey(), st, entry.getValue(), grandId);
            }
        });

    }

    public List<ProductSkuVO> updateImport(
            StadiumType stadiumType,
            Map<String, StadiumVO> skuCodeMap,
            List<ProductSkuVO> productSkuVOS,
            Integer grandId
    ) {
        List<ProductSkuVO> list = Lists.newArrayList();
        Optional.of(productSkuVOS).orElseGet(Lists::newArrayList).forEach(
                s -> s.getSkuList().forEach(
                        source -> skuService.update(
                                source,
                                Sku.class,
                                c -> {
                                    StadiumVO stadiumVO = skuCodeMap.get(c.getSourceSkuCode());
                                    if (stadiumVO == null){
                                        return;
                                    }
                                    // 设置限购
                                    if (stadiumVO.getBuyLimit() < 0)
                                        stadiumVO.setBuyLimit(0);
                                    s.setBuyLimit(stadiumVO.getBuyLimit());

                                    // 设置sku
                                    BeanUtils.copyProperties(stadiumVO, c, BeanUtil.getNullPropertyNames(stadiumVO));

                                    // 设置活动库存
                                    if (c.getAmount() < 0)
                                        c.setAmount(0);

                                    c.setSourceSkuCode(c.getSkuCode());
                                    // skuCode 生成规则
                                    c.setSkuCode(c.getSkuCode() + stadiumType.toString() + grandId);
                                    c.setPromotionType(stadiumType.toString());
                                    c.setAmount(null);
                                    // 更新过的productSkuVO;
                                    list.add(s);
                                })
                )
        );
        Optional.of(productSkuVOS).orElseGet(Lists::newArrayList).forEach(
                p -> {
                    p.setStatus(ProductStatus.ONSALE);
                    productService.update(p);
                }
        );

        return list;
    }

    /**
     * 通过sku复制或更新 分会场商品数据
     *
     * @param categoryName  分类名称
     * @param stadiumType   分会场
     * @param stadiumVOList 导入的数据
     * @param grandId       主会场id
     */
    public void copyOrUpdateImport(
            String categoryName,
            StadiumType stadiumType,
            List<StadiumVO> stadiumVOList,
            Integer grandId
    ) {
        // 校验 term 和 category 是否已经创建 实际校验category的name 保存订单逻辑还需要进行修改
        CategoryForPromotion categoryForPromotion = save(categoryName);

        Map<String, StadiumVO> skuCodeMap = stadiumVOList.stream().collect(Collectors.toMap(StadiumVO::getSkuCode, s -> s));

        // 获取所有需要进行更新的 ProductSkuVO
        Optional<List<ProductSkuVO>> productSkuVOS = skuService.loadAlreadyCopyBySkuCodes(skuCodeMap.keySet(), grandId);
        // 更新导入的数据 返回更新后的数据
        List<ProductSkuVO> isUpdate = updateImport(stadiumType, skuCodeMap, productSkuVOS.orElseGet(ArrayList::new), grandId);

        // 移除已经更新过的数据
        isUpdate.forEach(
                psv -> psv.getSkuList().forEach(
                        sku -> skuCodeMap.remove(sku.getSourceSkuCode())
                )
        );

        // 复制导入
        Optional<List<ProductSkuVO>> productSkuVOS1 = skuService.copyImport(stadiumType, skuCodeMap, grandId);



        // 获取所有的导入
        List<ProductSkuVO> productSkuVOList = productSkuVOS1.orElseGet(LinkedList::new);
        // 增加所有的导入
        productSkuVOList.addAll(isUpdate);

        // 为完成更新和复制的sku绑定term_relationship
        productSkuVOList.forEach(
                s -> categoryService.addProductCategory(
                        s.getId().toString(),
                        categoryForPromotion.getId(),
                        stadiumType.toString(),
                        grandId
                )
        );


        for (Map.Entry<String, StadiumVO> m : skuCodeMap.entrySet()) {

            //校验库存
            int skuId=freshmanMapper.selectSourceSkuIdBySkuCode(m.getKey());
            stockCheckService.stockCheck(m.getValue().getAmount(),skuId,grandId.toString(),"STADIUM",stadiumType.name());

        }


        productSkuVOList.stream().distinct().forEach(
                p -> productService.updateByPrimaryKeySelective(p)
        );
    }

}