package cn.hds.hvmall.service.grandsale;

import cn.hds.hvmall.entity.grandsale.GrandSaleTimeline;
import cn.hds.hvmall.entity.grandsale.GrandSaleTimelineConfig;
import cn.hds.hvmall.pojo.grandsale.GrandSaleTimelineVO;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface GrandSaleTimelineService {
  int deleteByPrimaryKey(Integer id);

  Boolean insert(GrandSaleTimelineConfig grandSaleTimelineConfig);

  int insertSelective(GrandSaleTimeline record);

  GrandSaleTimeline selectByPrimaryKey(Integer id);

  GrandSaleTimelineConfig selectAllByGrandSaleId(Integer grandSaleId, Pageable pageable);

  int updateByPrimaryKeySelective(GrandSaleTimeline record);

  int updateByPrimaryKey(GrandSaleTimeline record);

  int logicalDelete(Integer id);

}