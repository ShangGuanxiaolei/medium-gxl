package cn.hds.hvmall.service.grandsale;

import cn.hds.hvmall.entity.grandsale.GrandSalePromotion;
import cn.hds.hvmall.pojo.grandsale.GrandSalePromotionVO;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

public interface GrandSalePromotionService {
  int deleteByPrimaryKey(Integer id);

  int insert(GrandSalePromotionVO record);

  int insertSelective(GrandSalePromotion record);

  GrandSalePromotion selectByPrimaryKey(Integer id);

  List<GrandSalePromotion> selectEffectivePromotion();

  List<GrandSalePromotion> selectAll(Pageable pageable);

  Map selectPromotionType();

  Map selectStadiumType();

  int updateByPrimaryKeySelective(GrandSalePromotion record);

  int updateByPrimaryKey(GrandSalePromotion record);

  int updateStatusById(Integer id);

  int deleteGrandSaleById(Integer id);
}