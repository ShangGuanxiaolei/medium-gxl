package cn.hds.hvmall.service.grandsale;

import cn.hds.hvmall.entity.grandsale.GrandSaleStadium;
import cn.hds.hvmall.pojo.grandsale.GrandSaleMainShareVO;
import cn.hds.hvmall.pojo.grandsale.GrandSaleStadiumVO;
import cn.hds.hvmall.pojo.grandsale.GrandSaleStadiumVOId;
import cn.hds.hvmall.pojo.grandsale.GrandSaleStadiumVOMap;
import cn.hds.hvmall.service.grandsale.vo.GrandSaleStadiumSortVO;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.data.domain.Pageable;
import java.util.Date;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

public interface GrandSaleStadiumService {
  int deleteByPrimaryKey(Integer id);

  int insert(GrandSaleStadiumVO grandSaleStadiumVO);

  int insertSelective(GrandSaleStadium record);

  GrandSaleStadiumVOMap selectOne(Integer stadiumId, Pageable pageable);

  List<GrandSaleStadium> selectAllByGrandSaleId(Integer grandSaleId);

  int updateByPrimaryKeySelective(GrandSaleStadium record);

  int updateByPrimaryKey(GrandSaleStadium record);

  int update(GrandSaleStadiumVOId record);

  boolean updateSortById(GrandSaleStadiumSortVO grandSaleStadiumSortVO);

  void updateGrandSaleStadium(GrandSaleStadiumVOId record);

  void importData(InputStream is,Integer grandId) throws IOException, InvalidFormatException;

  int insertGrandSaleShare(GrandSaleMainShareVO[] grandSaleMainShareVOS);

  Map<String, Object> selectAllMainShare(Pageable pageable);

  int updateGrandSaleMainShare(GrandSaleMainShareVO grandSaleMainShareVO);

  Date selectLastMainShare();
}