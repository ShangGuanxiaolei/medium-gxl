package cn.hds.hvmall.service.grandsale.vo;

import javax.validation.constraints.NotNull;

/**
 * @author gxl
 */
public class StadiumSortVO {

  /**
   * 分会场id
   */
  @NotNull(message = "活动类型不能为空")
  private Integer id;

  /**
   * 前端模块排序
   */
  @NotNull(message = "分会场排序不能为空")
  private Integer sort;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Integer getSort() {
    return sort;
  }

  public void setSort(Integer sort) {
    this.sort = sort;
  }
}
