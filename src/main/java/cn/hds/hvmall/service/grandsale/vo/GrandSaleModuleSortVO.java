package cn.hds.hvmall.service.grandsale.vo;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author gxl
 */
public class GrandSaleModuleSortVO {

  /**
   * 活动模块排序VO
   */
  @NotNull(message = "活动模块排序列表不能为空")
  private List<ModuleSortVO> moduleSortVOList;

  public List<ModuleSortVO> getModuleSortVOList() {
    return moduleSortVOList;
  }

  public void setModuleSortVOList(List<ModuleSortVO> moduleSortVOList) {
    this.moduleSortVOList = moduleSortVOList;
  }
}
