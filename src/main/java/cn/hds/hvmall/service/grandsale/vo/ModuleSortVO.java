package cn.hds.hvmall.service.grandsale.vo;

import cn.hds.hvmall.pojo.grandsale.SortArr;
import cn.hds.hvmall.type.PromotionType;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author gxl
 */
public class ModuleSortVO {

  /**
   * 活动模块id
   */
  @NotNull(message = "活动类型不能为空")
  private PromotionType promotionType;

  /**
   * 前端模块排序
   */
  @NotNull(message = "模块排序不能为空")
  private Integer sort;

  private List<SortArr> sortArr;

  public List<SortArr> getSortArr() {
    return sortArr;
  }

  public void setSortArr(List<SortArr> sortArr) {
    this.sortArr = sortArr;
  }

  public PromotionType getPromotionType() {
    return promotionType;
  }

  public void setPromotionType(PromotionType promotionType) {
    this.promotionType = promotionType;
  }

  public Integer getSort() {
    return sort;
  }

  public void setSort(Integer sort) {
    this.sort = sort;
  }
}
