package cn.hds.hvmall.service.grandsale.vo;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author gxl
 */
public class GrandSaleStadiumSortVO {

  /**
   * 活动模块排序VO
   */
  @NotNull(message = "活动模块排序列表不能为空")
  private List<StadiumSortVO> stadiumSortVOS;

  public List<StadiumSortVO> getStadiumSortVOS() {
    return stadiumSortVOS;
  }

  public void setStadiumSortVOS(List<StadiumSortVO> stadiumSortVOS) {
    this.stadiumSortVOS = stadiumSortVOS;
  }
}
