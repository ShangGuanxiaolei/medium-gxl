package cn.hds.hvmall.service.grandsale.impl;

import cn.hds.hvmall.entity.grandsale.GrandSalePromotion;
import cn.hds.hvmall.entity.grandsale.GrandSaleStadium;
import cn.hds.hvmall.mapper.grandsale.GrandSalePromotionMapper;
import cn.hds.hvmall.pojo.grandsale.GrandSalePromotionVO;
import cn.hds.hvmall.service.grandsale.GrandSalePromotionService;
import cn.hds.hvmall.service.grandsale.GrandSaleStadiumService;
import cn.hds.hvmall.type.PromotionType;
import cn.hds.hvmall.type.StadiumType;
import cn.hds.hvmall.utils.DateUtils;
import cn.hds.hvmall.utils.list.GlobalErrorCode;
import cn.hds.hvmall.utils.list.qiniu.BizException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class GrandSalePromotionServiceImpl implements GrandSalePromotionService {

  private final GrandSalePromotionMapper grandSalePromotionMapper;

  private final GrandSaleStadiumService grandSaleStadiumService;

  @Autowired
  public GrandSalePromotionServiceImpl(GrandSalePromotionMapper grandSalePromotionMapper,
                                       GrandSaleStadiumService grandSaleStadiumService) {
    this.grandSalePromotionMapper = grandSalePromotionMapper;
    this.grandSaleStadiumService = grandSaleStadiumService;
  }

  @Override
  @Transactional
  public int deleteByPrimaryKey(Integer id) {
    return grandSalePromotionMapper.deleteByPrimaryKey(id);
  }

  @Override
  @Transactional
  public int insert(GrandSalePromotionVO record) {
    Date beginTime = DateUtils.stringFormatDate(record.getBeginTime());
    Date endTime = DateUtils.stringFormatDate(record.getEndTime());
    Date date = new Date();
    if (beginTime.before(date) && endTime.after(date)) {
      if (grandSalePromotionMapper.selectStartedPromotion() != null) {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "同时只能有一个活动在进行中");
      }
    }
    checkPromotionTime(beginTime, endTime);
    GrandSalePromotion grandSalePromotion = new GrandSalePromotion();
    BeanUtils.copyProperties(record, grandSalePromotion);
    grandSalePromotion.setBeginTime(beginTime);
    grandSalePromotion.setEndTime(endTime);
    setStatus(grandSalePromotion);
    return grandSalePromotionMapper.insert(grandSalePromotion);
  }

  @Override
  @Transactional
  public int insertSelective(GrandSalePromotion record) {
    return grandSalePromotionMapper.insertSelective(record);
  }

  @Override
  public GrandSalePromotion selectByPrimaryKey(Integer id) {
    return grandSalePromotionMapper.selectByPrimaryKey(id);
  }

  @Override
  public List<GrandSalePromotion> selectEffectivePromotion() {
    return grandSalePromotionMapper.selectEffectivePromotion();
  }

  @Override
  public List<GrandSalePromotion> selectAll(Pageable pageable) {
    return grandSalePromotionMapper.selectAll(pageable);
  }

  @Override
  public Map selectPromotionType() {
    PromotionType[] promotionTypes = PromotionType.values();
    PromotionType key;
    Map<PromotionType, Object> map = new HashMap<>();
    for (PromotionType promotionType :
         promotionTypes) {
      key = promotionType;
      if (PromotionType.LOTTERY_DRAW.compareTo(key) == 0 || PromotionType.PACKET_RAIN.compareTo(key) == 0 ||
              PromotionType.PIECE_GROUP.compareTo(key) == 0 || PromotionType.MEETING_GIFT.compareTo(key) == 0
      ) {
        continue;
      }
      map.put(key, promotionType.getcName());
    }
    return map;
  }

  @Override
  public Map selectStadiumType() {
    StadiumType[] stadiumTypes = StadiumType.values();
    StadiumType key;
    Map<StadiumType, Object> map = new HashMap<>();
    for (StadiumType stadiumType :
            stadiumTypes) {
      key = stadiumType;
      map.put(key, stadiumType.desc);
    }
    return map;
  }

  @Override
  @Transactional
  public int updateByPrimaryKeySelective(GrandSalePromotion record) {
    return grandSalePromotionMapper.updateByPrimaryKeySelective(record);
  }

  @Override
  @Transactional
  public int updateByPrimaryKey(GrandSalePromotion record) {
    return grandSalePromotionMapper.updateByPrimaryKey(record);
  }

  @Override
  @Transactional
  public int updateStatusById(Integer id) {
    List<GrandSaleStadium> grandSaleStadiums = grandSaleStadiumService.selectAllByGrandSaleId(id);
    for (GrandSaleStadium grandSaleStadium:
        grandSaleStadiums) {
      Integer stadiumId = grandSaleStadium.getId();
      if (stadiumId == null) {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "分会场id为空");
      }
      grandSaleStadiumService.deleteByPrimaryKey(stadiumId);
    }
    return grandSalePromotionMapper.updateStatusById(id);
  }

  @Override
  public int deleteGrandSaleById(Integer id) {
    return grandSalePromotionMapper.deleteGrandSaleById(id);
  }

  /**
   * 设置活动状态
   * @param record 520活动
   */
  private void setStatus(GrandSalePromotion record) {
    Date beginTime = record.getBeginTime();
    Date endTime = record.getEndTime();
    Date date = new Date();
    boolean effectiveDate = DateUtils.isEffectiveDate(date, beginTime, endTime);
    if (effectiveDate) {
      record.setStatus(1); //进行中
    } else {
      if (date.after(endTime)) {
        record.setStatus(2); //已结束
      } else {
        record.setStatus(0); //未开始
      }
    }
  }

  /**
   * 检查活动时间是否冲突
   * @param beginTime 开始时间
   * @param endTime 结束时间
   */
  private void checkPromotionTime(Date beginTime, Date endTime) {
    if (endTime.before(beginTime)) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "开始时间不能晚于结束时间");
    }
    List<GrandSalePromotion> grandSalePromotions = this.selectEffectivePromotion();
    for (GrandSalePromotion grandSalePromotion:
            grandSalePromotions) {
      Date beginTime1 = grandSalePromotion.getBeginTime();
      Date endTime1 = grandSalePromotion.getEndTime();
      if (beginTime.before(beginTime1) && endTime.after(endTime1)) {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "活动时间不能与已有活动冲突");
      }
      boolean effectiveBegin =
              DateUtils.isEffectiveDate(beginTime, beginTime1, endTime1);
      boolean effectiveEnd =
              DateUtils.isEffectiveDate(endTime, beginTime1, endTime1);
      if (effectiveBegin || effectiveEnd) {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "活动时间不能与已有活动冲突");
      }
    }
  }
}