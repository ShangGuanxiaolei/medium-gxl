package cn.hds.hvmall.service.grandsale.vo;

import java.util.Date;

/**
 * @author gxl
 */
public class GrandSalePromotionModuleVO {
  /**
   * 关联的活动id
   */
  private String promotionId;

  /**
   * 活动名称
   */
  private String promotionName;

  /**
   * 开始时间
   */
  private Date beginTime;

  /**
   * 结束时间
   */
  private Date endTime;

  /**
   * 前端模块排序
   */
  private Integer sort;

  /**
   * 活动模块状态:0->,未开始,1->活动中,2->自动结束,3->人工下线
   */
  private Integer status;

  private Integer sortIng;

  public Integer getSortIng() {
    return sortIng;
  }

  public void setSortIng(Integer sortIng) {
    this.sortIng = sortIng;
  }

  public String getPromotionId() {
    return promotionId;
  }

  public void setPromotionId(String promotionId) {
    this.promotionId = promotionId;
  }

  public String getPromotionName() {
    return promotionName;
  }

  public void setPromotionName(String promotionName) {
    this.promotionName = promotionName;
  }

  public Date getBeginTime() {
    return beginTime;
  }

  public void setBeginTime(Date beginTime) {
    this.beginTime = beginTime;
  }

  public Date getEndTime() {
    return endTime;
  }

  public void setEndTime(Date endTime) {
    this.endTime = endTime;
  }

  public Integer getSort() {
    return sort;
  }

  public void setSort(Integer sort) {
    this.sort = sort;
  }

  public Integer getStatus() {
    return status;
  }

  public void setStatus(Integer status) {
    this.status = status;
  }
}
