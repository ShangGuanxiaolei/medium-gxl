package cn.hds.hvmall.service.grandsale.impl;

import cn.hds.hvmall.entity.grandsale.GrandSaleKv;
import cn.hds.hvmall.mapper.grandsale.GrandSaleKvMapper;
import cn.hds.hvmall.pojo.grandsale.GrandSaleKvVO;
import cn.hds.hvmall.service.grandsale.GrandSaleKvService;
import cn.hds.hvmall.utils.DateUtils;
import cn.hds.hvmall.utils.list.GlobalErrorCode;
import cn.hds.hvmall.utils.list.qiniu.BizException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
public class GrandSaleKvServiceImpl implements GrandSaleKvService {

  private final GrandSaleKvMapper grandSaleKvMapper;

  @Autowired
  public GrandSaleKvServiceImpl(GrandSaleKvMapper grandSaleKvMapper) {
    this.grandSaleKvMapper = grandSaleKvMapper;
  }

  @Override
  @Transactional
  public int deleteByPrimaryKey(Integer id) {
    return grandSaleKvMapper.deleteByPrimaryKey(id);
  }

  @Override
  @Transactional
  public int insert(GrandSaleKvVO grandSaleKvVO) {
    Date beginTime = DateUtils.stringFormatDate(grandSaleKvVO.getBeginTime());
    Date endTime = DateUtils.stringFormatDate(grandSaleKvVO.getEndTime());
    if (endTime.before(new Date())) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "结束时间需要晚于当前时间");
    }
    GrandSaleKv grandSaleKv = new GrandSaleKv();
    BeanUtils.copyProperties(grandSaleKvVO, grandSaleKv);
    grandSaleKv.setBeginTime(beginTime);
    grandSaleKv.setEndTime(endTime);
    return grandSaleKvMapper.insert(grandSaleKv);
  }

  @Override
  @Transactional
  public int insertSelective(GrandSaleKv record) {
    return grandSaleKvMapper.insertSelective(record);
  }

  @Override
  public GrandSaleKv selectByPrimaryKey(Integer id) {
    return grandSaleKvMapper.selectByPrimaryKey(id);
  }

  @Override
  public List<GrandSaleKv> selectAllByGrandSaleId(Integer grandSaleId, Pageable pageable) {
    return grandSaleKvMapper.selectAllByGrandSaleId(grandSaleId, pageable);
  }

  @Override
  public int countByGrandSaleId(Integer grandSaleId) {
    return grandSaleKvMapper.countByGrandSaleId(grandSaleId);
  }

  @Override
  @Transactional
  public int updateByPrimaryKeySelective(GrandSaleKv record) {
    return grandSaleKvMapper.updateByPrimaryKeySelective(record);
  }

  @Override
  @Transactional
  public int updateByPrimaryKey(GrandSaleKv record) {
    return grandSaleKvMapper.updateByPrimaryKey(record);
  }

  @Override
  @Transactional
  public int logicalDelete(Integer id) {
    return grandSaleKvMapper.logicalDelete(id);
  }
}