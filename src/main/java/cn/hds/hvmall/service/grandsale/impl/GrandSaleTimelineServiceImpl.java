package cn.hds.hvmall.service.grandsale.impl;

import cn.hds.hvmall.entity.grandsale.GrandSaleTimeline;
import cn.hds.hvmall.entity.grandsale.GrandSaleTimelineConfig;
import cn.hds.hvmall.mapper.grandsale.GrandSaleTimelineMapper;
import cn.hds.hvmall.pojo.grandsale.GrandSaleTimelineVO;
import cn.hds.hvmall.service.grandsale.GrandSaleTimelineService;
import cn.hds.hvmall.utils.DateUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
public class GrandSaleTimelineServiceImpl implements GrandSaleTimelineService {

  private final GrandSaleTimelineMapper grandSaleTimelineMapper;

  @Autowired
  public GrandSaleTimelineServiceImpl(GrandSaleTimelineMapper grandSaleTimelineMapper) {
    this.grandSaleTimelineMapper = grandSaleTimelineMapper;
  }

  @Override
  @Transactional
  public int deleteByPrimaryKey(Integer id) {
    return grandSaleTimelineMapper.deleteByPrimaryKey(id);
  }

  @Override
  @Transactional
  public Boolean insert(GrandSaleTimelineConfig grandSaleTimelineConfig) {
    Date beginTime = DateUtils.stringFormatDate(grandSaleTimelineConfig.getBeginTime());
    Date endTime=DateUtils.stringFormatDate(grandSaleTimelineConfig.getEndTime());
    Boolean istrue = false;
    int insertConfig = 0;
//    GrandSaleTimelineConfig allconfig=selectAllByGrandSaleId(grandSaleTimelineConfig.getGrandSaleId(),null);
      GrandSaleTimelineConfig config = grandSaleTimelineMapper.selectTimelinConfigByGrandSaleId(grandSaleTimelineConfig.getGrandSaleId());
      if(config==null){
        insertConfig = grandSaleTimelineMapper.insertConfig(grandSaleTimelineConfig.getGrandSaleId(), beginTime, endTime, grandSaleTimelineConfig.getShowTimeline(), grandSaleTimelineConfig.getTimelineImg());
      }else{
        insertConfig=grandSaleTimelineMapper.updateTimelineConfig(grandSaleTimelineConfig.getGrandSaleId(), beginTime, endTime, grandSaleTimelineConfig.getShowTimeline(), grandSaleTimelineConfig.getTimelineImg());
      }
      List<GrandSaleTimeline> grandSaleTimelineList = grandSaleTimelineConfig.getGrandSaleTimelineList();
      int timelineSize = 0;
      for (int i = 0; i < grandSaleTimelineList.size(); i++) {
        grandSaleTimelineMapper.insert(grandSaleTimelineList.get(i));
        timelineSize++;
      }
      if (insertConfig > 0 && timelineSize == grandSaleTimelineList.size()) {
        istrue = true;
      }
    return istrue;
  }

  @Override
  @Transactional
  public int insertSelective(GrandSaleTimeline record) {
    return grandSaleTimelineMapper.insertSelective(record);
  }

  @Override
  public GrandSaleTimeline selectByPrimaryKey(Integer id) {
    return grandSaleTimelineMapper.selectByPrimaryKey(id);
  }

  @Override
  public GrandSaleTimelineConfig  selectAllByGrandSaleId(Integer grandSaleId, Pageable pageable) {
    List<GrandSaleTimeline> timelines=grandSaleTimelineMapper.selectAllByGrandSaleId(grandSaleId, pageable);
    GrandSaleTimelineConfig config=new GrandSaleTimelineConfig();

    if(grandSaleTimelineMapper.selectTimelinConfigByGrandSaleId(grandSaleId)==null){
      config.setGrandSaleTimelineList(timelines);
      return config;
    }else{
      config=grandSaleTimelineMapper.selectTimelinConfigByGrandSaleId(grandSaleId);
      config.setGrandSaleTimelineList(timelines);
    }
    return config;
  }

  @Override
  @Transactional
  public int updateByPrimaryKeySelective(GrandSaleTimeline record) {
    return grandSaleTimelineMapper.updateByPrimaryKeySelective(record);
  }

  @Override
  @Transactional
  public int updateByPrimaryKey(GrandSaleTimeline record) {
    return grandSaleTimelineMapper.updateByPrimaryKey(record);
  }

  @Override
  @Transactional
  public int logicalDelete(Integer id) {
    return grandSaleTimelineMapper.logicalDelete(id);
  }
}