package cn.hds.hvmall.service.grandsale;

import cn.hds.hvmall.entity.grandsale.GrandSaleKv;
import cn.hds.hvmall.pojo.grandsale.GrandSaleKvVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface GrandSaleKvService {
  int deleteByPrimaryKey(Integer id);

  int insert(GrandSaleKvVO grandSaleKvVO);

  int insertSelective(GrandSaleKv record);

  GrandSaleKv selectByPrimaryKey(Integer id);

  List<GrandSaleKv> selectAllByGrandSaleId(Integer grandSaleId, Pageable pageable);

  int countByGrandSaleId(Integer grandSaleId);

  int updateByPrimaryKeySelective(GrandSaleKv record);

  int updateByPrimaryKey(GrandSaleKv record);

  int logicalDelete(Integer id);
}