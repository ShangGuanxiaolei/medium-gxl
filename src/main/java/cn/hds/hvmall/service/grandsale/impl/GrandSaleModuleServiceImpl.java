package cn.hds.hvmall.service.grandsale.impl;

import cn.hds.hvmall.entity.grandsale.GrandSaleModule;
import cn.hds.hvmall.mapper.flashsale.PromotionFlashSaleMapper;
import cn.hds.hvmall.mapper.grandsale.GrandSaleModuleMapper;
import cn.hds.hvmall.mapper.grandsale.GrandSalePromotionMapper;
import cn.hds.hvmall.mapper.piece.PromotionBaseInfoMapper;
import cn.hds.hvmall.mapper.promotion.PromotionExclusiveMapper;
import cn.hds.hvmall.pojo.grandsale.GrandSaleModuleVO;
import cn.hds.hvmall.pojo.grandsale.PromotionVO;
import cn.hds.hvmall.pojo.grandsale.SortArr;
import cn.hds.hvmall.service.grandsale.GrandSaleModuleService;
import cn.hds.hvmall.service.grandsale.vo.GrandSaleModuleSortVO;
import cn.hds.hvmall.service.grandsale.vo.GrandSalePromotionModuleVO;
import cn.hds.hvmall.service.grandsale.vo.ModuleSortVO;
import cn.hds.hvmall.service.piece.PGService;
import cn.hds.hvmall.service.promotion.PromotionService;
import cn.hds.hvmall.service.promotionactivity.ProductSaleService;
import cn.hds.hvmall.service.reserve.ReserveService;
import cn.hds.hvmall.type.PromotionType;
import cn.hds.hvmall.utils.list.GlobalErrorCode;
import cn.hds.hvmall.utils.list.qiniu.BizException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class GrandSaleModuleServiceImpl implements GrandSaleModuleService {

  private final GrandSaleModuleMapper grandSaleModuleMapper;

  private final PromotionBaseInfoMapper promotionBaseInfoMapper;

  private final ReserveService reserveService;

  private final ProductSaleService productSaleService;

  private final PromotionFlashSaleMapper promotionFlashSaleMapper;

  private final PromotionService promotionService;

  private final PGService pgService;

  private final GrandSalePromotionMapper grandSalePromotionMapper;

  @Autowired
  public GrandSaleModuleServiceImpl(GrandSaleModuleMapper grandSaleModuleMapper,
                                    PromotionBaseInfoMapper promotionBaseInfoMapper,
                                    ReserveService reserveService,
                                    ProductSaleService productSaleService,
                                    PromotionFlashSaleMapper promotionFlashSaleMapper,
                                    PromotionService promotionService,
                                    PGService pgService,
                                    GrandSalePromotionMapper grandSalePromotionMapper) {
    this.grandSaleModuleMapper = grandSaleModuleMapper;
    this.promotionBaseInfoMapper = promotionBaseInfoMapper;
    this.reserveService = reserveService;
    this.productSaleService = productSaleService;
    this.promotionFlashSaleMapper = promotionFlashSaleMapper;
    this.promotionService = promotionService;
    this.pgService = pgService;
    this.grandSalePromotionMapper = grandSalePromotionMapper;
  }

  @Override
  @Transactional
  public int deleteByPrimaryKey(Integer id) {
    return grandSaleModuleMapper.deleteByPrimaryKey(id);
  }

  @Override
  @Transactional
  public int insert(GrandSaleModuleVO grandSaleModuleVO) {
    GrandSaleModule grandSaleModule = new GrandSaleModule();
    BeanUtils.copyProperties(grandSaleModuleVO, grandSaleModule);
    GrandSaleModule module = grandSaleModuleMapper.selectOneByGrandSaleId(grandSaleModuleVO.getGrandSaleId());
    if (module != null) {//已有活动模块，取出排序值
      grandSaleModule.setSort(module.getSort());
    }
    return grandSaleModuleMapper.insert(grandSaleModule);
  }

  @Override
  @Transactional
  public int insertSelective(GrandSaleModule record) {
    return grandSaleModuleMapper.insertSelective(record);
  }

  @Override
  public GrandSaleModule selectByPrimaryKey(Integer id) {
    return grandSaleModuleMapper.selectByPrimaryKey(id);
  }

  @Override
  public Map selectAll(Integer grandSaleId) {
    List<GrandSaleModule> grandSaleModules = grandSaleModuleMapper.selectAll(grandSaleId);
    PromotionType key;
    List<GrandSalePromotionModuleVO> listTemp;
    Map<PromotionType, List<GrandSalePromotionModuleVO>> map = new HashMap<>();
    for (GrandSaleModule grandSaleModule :
         grandSaleModules) {
      String promotionId = grandSaleModule.getPromotionId();
      PromotionType promotionType = grandSaleModule.getPromotionType();
      key = promotionType;
      GrandSalePromotionModuleVO grandSalePromotionModuleVO = new GrandSalePromotionModuleVO();
      listTemp = map.computeIfAbsent(key, k -> new ArrayList<>());
      setModuleProperties(grandSaleModule, promotionId, promotionType, grandSalePromotionModuleVO);
      listTemp.add(grandSalePromotionModuleVO);
    }
    return map;
  }

  @Override
  public List<PromotionVO> selectPromotion(PromotionType promotionType) {
    List<PromotionVO> promotionVOS;
    switch (promotionType) {
      case PIECE:
        promotionVOS = grandSaleModuleMapper.selectAllEffective();
        break;
      case FLASHSALE:
        promotionVOS = promotionFlashSaleMapper.selectEffectivePromotion();
        break;
      case RESERVE:
        promotionVOS = reserveService.getEffectivePromotion();
        break;
      case PRODUCT_SALES:
        promotionVOS = productSaleService.selectEffectSalePromotion();
        break;
      default:
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "活动类型不正确");
    }
    return promotionVOS;
  }

  private List<PromotionVO> selectEffectivePromotion(PromotionType promotionType) {
    List<PromotionVO> promotionVOS;
    switch (promotionType) {
      case PIECE:
        promotionVOS = grandSaleModuleMapper.selectAllEffective2();
        break;
      case FLASHSALE:
        promotionVOS = promotionFlashSaleMapper.selectEffectivePromotion2();
        break;
      case RESERVE:
        promotionVOS = reserveService.getEffectivePromotion2();
        break;
      case PRODUCT_SALES:
        promotionVOS = productSaleService.selectEffectSalePromotion2();
        break;
      default:
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "活动类型不正确");
    }
    return promotionVOS;
  }

  @Override
  @Transactional
  public int updateByPrimaryKeySelective(GrandSaleModule record) {
    return grandSaleModuleMapper.updateByPrimaryKeySelective(record);
  }

  @Override
  @Transactional
  public int updateByPrimaryKey(GrandSaleModule record) {
    return grandSaleModuleMapper.updateByPrimaryKey(record);
  }

  @Override
  @Transactional
  public boolean updateStatusById(String promotionId, Integer status, PromotionType promotionType) {
    //查询出有效活动
    List<PromotionVO> promotionVOS = selectEffectivePromotion(promotionType);
    if (promotionVOS != null && !promotionVOS.isEmpty()) {
      List<String> promotionIds = new ArrayList<>();
      for (PromotionVO promotionVO:
              promotionVOS) {
        promotionIds.add(promotionVO.getPromotionId());
      }
      //判断与有效活动id相同
      if (promotionIds.contains(promotionId)) {
        boolean result = false;
        //失效活动处理
        if (status == 3) {
          //结束对应类型活动
          switch (promotionType) {
            case PIECE:
              result = pgService.updateActivityExclusiveStatus(promotionId);
              break;
            case FLASHSALE:
              result = promotionService.close(Long.valueOf(promotionId));
              break;
            case RESERVE:
              result = reserveService.endReserveStatus(Long.valueOf(promotionId));
              break;
            case PRODUCT_SALES:
              result = productSaleService.endProductSales(promotionId);
              break;
            default:
              throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "活动类型不正确");
          }
        }
        //修改活动状态
        return grandSaleModuleMapper.updateStatusById(promotionId, status) > 0 && result;
      } else {//不包含指定活动id代表活动已失效
        if (status == 3) {
          return grandSaleModuleMapper.updateStatusById(promotionId, status) > 0;
        } else {
          throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "活动已失效不能发布，请失效该模块");
        }
      }
    } else {
      //活动已失效
      if (status == 3) {
        return grandSaleModuleMapper.updateStatusById(promotionId, status) > 0;
      } else {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "活动已失效不能发布，请失效该模块");
      }
    }
  }

  @Override
  public boolean updateSortByType(GrandSaleModuleSortVO grandSaleModuleSortVO) {
    List<ModuleSortVO> moduleSortVOList = grandSaleModuleSortVO.getModuleSortVOList();
    boolean flag = true;
    for (ModuleSortVO moduleSortVO:
         moduleSortVOList) {
      PromotionType promotionType = moduleSortVO.getPromotionType();
      Integer sort = moduleSortVO.getSort();
      List<SortArr> sortArr = moduleSortVO.getSortArr();
      if(null!=sortArr){
        for (SortArr arr : sortArr) {
        String promotionId = arr.getPromotionId();
        Integer sortIng = arr.getSortIng();
        int i = grandSaleModuleMapper.updateFlashSaleSortByPromotionId(promotionId, sortIng);
        if(!(i>0)){
          flag = false;
        }
      }}
      if (!(grandSaleModuleMapper.updateSortByType(promotionType, sort) > 0)) {
        flag = false;
      }
    }
    return flag;
  }

  /**
   * 设置模块属性值
   * @param grandSaleModule 活动模块
   * @param promotionId 活动id
   * @param promotionType 活动类型
   * @param grandSalePromotionModuleVO 活动模块VO
   */
  private void setModuleProperties(GrandSaleModule grandSaleModule, String promotionId, PromotionType promotionType, GrandSalePromotionModuleVO grandSalePromotionModuleVO) {
    grandSalePromotionModuleVO.setPromotionId(promotionId);
    grandSalePromotionModuleVO.setStatus(grandSaleModule.getStatus());
    grandSalePromotionModuleVO.setSort(grandSaleModule.getSort());
    grandSalePromotionModuleVO.setSortIng(grandSaleModule.getSortIng());
    Date date = new Date();
    PromotionVO promotionVO;
    switch (promotionType) {
      case PIECE:
        promotionVO = promotionBaseInfoMapper.selectPromotionVOByPCode(promotionId);
        if (promotionVO != null) {
          grandSalePromotionModuleVO.setPromotionName(promotionVO.getName());
          grandSalePromotionModuleVO.setBeginTime(promotionVO.getBeginTime());
          grandSalePromotionModuleVO.setEndTime(promotionVO.getEndTime());
        }
        break;
      case FLASHSALE:
        promotionVO = promotionFlashSaleMapper.selectByPromotionId(promotionId);
        if (promotionVO != null) {
          grandSalePromotionModuleVO.setPromotionName(promotionVO.getName());
          grandSalePromotionModuleVO.setBeginTime(promotionVO.getBeginTime());
          grandSalePromotionModuleVO.setEndTime(promotionVO.getEndTime());
        }
        break;
      case PACKET_RAIN:
        grandSalePromotionModuleVO.setPromotionName("红包雨");
        grandSalePromotionModuleVO.setBeginTime(date);
        grandSalePromotionModuleVO.setEndTime(date);
        break;
      case LOTTERY_DRAW:
        grandSalePromotionModuleVO.setPromotionName("抽奖");
        grandSalePromotionModuleVO.setBeginTime(date);
        grandSalePromotionModuleVO.setEndTime(date);
        break;
      case RESERVE:
        promotionVO = reserveService.selectByPromotionId(promotionId);
        if (promotionVO != null) {
          grandSalePromotionModuleVO.setPromotionName(promotionVO.getName());
          grandSalePromotionModuleVO.setBeginTime(promotionVO.getBeginTime());
          grandSalePromotionModuleVO.setEndTime(promotionVO.getEndTime());
        }
        break;
      case PRODUCT_SALES:
        promotionVO = productSaleService.selectByPCode(promotionId);
        if (promotionVO != null) {
          grandSalePromotionModuleVO.setPromotionName(promotionVO.getName());
          grandSalePromotionModuleVO.setBeginTime(promotionVO.getBeginTime());
          grandSalePromotionModuleVO.setEndTime(promotionVO.getEndTime());
        }
        break;
      default:
        grandSalePromotionModuleVO.setPromotionName("活动模块");
        grandSalePromotionModuleVO.setBeginTime(date);
        grandSalePromotionModuleVO.setEndTime(date);
        break;
    }
  }

  @Override
  public int bindGrand(PromotionType promotionType,Long promotionId){
    return grandSalePromotionMapper.existsActivityGrand() ? grandSaleModuleMapper.bindGrand(promotionType,promotionId) : 0;
  }
}