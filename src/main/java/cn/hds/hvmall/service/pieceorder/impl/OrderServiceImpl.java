package cn.hds.hvmall.service.pieceorder.impl;

import cn.hds.hvmall.entity.pieceorder.OrderSearchVO;
import cn.hds.hvmall.mapper.pieceorder.OrderMapper;
import cn.hds.hvmall.pojo.list.ExecuteResult;
import cn.hds.hvmall.pojo.pieceorder.OrderInfo;
import cn.hds.hvmall.service.pieceorder.OrderService;
import cn.hds.hvmall.utils.ConstantUtil;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author: zzl
 * @Date: 2018/9/17 9:48
 * @Version
 */
@Service("orderService")
public class OrderServiceImpl implements OrderService {
  private static final Logger logger = LoggerFactory.getLogger(OrderServiceImpl.class);

  @Autowired
  private OrderMapper orderMapper;

  @Override
  public ExecuteResult<List<OrderSearchVO>> searchOrder(OrderInfo orderInfo, int pageSize) {
    int pageNumber = (orderInfo.getPageNum()-1)*pageSize;
    ExecuteResult<List<OrderSearchVO>> result = null;
    try {
      List<OrderSearchVO> list = orderMapper.searchOrder(orderInfo,pageNumber,pageSize);
      int count = orderMapper.countOrder(orderInfo);
      result = new ExecuteResult<>();
      result.setCount(count);
      result.setResult(list);
      result.setIsSuccess(ConstantUtil.success);
    } catch (Exception e) {
      logger.error("系统错误,入参格式有误!",e);
      result.setIsSuccess(ConstantUtil.failed);
      result.setErrorCode(ConstantUtil.ResponseError.SYS_ERROR.getCode());
      result.setErrorMsg(ConstantUtil.ResponseError.SYS_ERROR.toString());
    }

    return result;
  }
}
