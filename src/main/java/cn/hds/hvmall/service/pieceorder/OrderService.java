package cn.hds.hvmall.service.pieceorder;

import cn.hds.hvmall.base.BaseExecuteResult;
import cn.hds.hvmall.entity.pieceorder.OrderSearchVO;
import cn.hds.hvmall.pojo.list.ExecuteResult;
import cn.hds.hvmall.pojo.pieceorder.OrderInfo;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * @Author: zzl
 * @Date: 2018/9/17 9:47
 * @Version
 */
public interface OrderService {

  /**
   * 通过指定条件筛选订单
   * 条件封装在orderinfo中
   * @param orderInfo
   * @param pageSize
   * @return
   */
  ExecuteResult<List<OrderSearchVO>> searchOrder(OrderInfo orderInfo,int pageSize);

}
