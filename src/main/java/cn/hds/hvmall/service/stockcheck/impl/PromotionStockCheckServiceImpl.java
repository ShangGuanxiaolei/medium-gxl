package cn.hds.hvmall.service.stockcheck.impl;

import cn.hds.hvmall.base.RedisService;
import cn.hds.hvmall.config.PromotionConstants;
import cn.hds.hvmall.entity.category.CategoryForPromotion;
import cn.hds.hvmall.entity.grandsale.GrandSaleStadium;
import cn.hds.hvmall.entity.stockcheck.FlashSale;
import cn.hds.hvmall.mapper.grandsale.GrandSaleStadiumMapper;
import cn.hds.hvmall.mapper.stockCheck.PromotionStockCheckMapper;
import cn.hds.hvmall.service.category.CategoryService;
import cn.hds.hvmall.service.freshman.impl.FreshmanBannerServiceImpl;
import cn.hds.hvmall.service.list.ProductService;
import cn.hds.hvmall.service.stockcheck.PromotionStockCheckService;
import cn.hds.hvmall.utils.list.GlobalErrorCode;
import cn.hds.hvmall.utils.list.qiniu.BizException;
import cn.hds.hvmall.utils.list.qiniu.IdTypeHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("promotionStockCheckService")
public class PromotionStockCheckServiceImpl implements PromotionStockCheckService {

    private static final Logger logger = LoggerFactory.getLogger(PromotionStockCheckServiceImpl.class);

    @Autowired
    private PromotionStockCheckMapper promotionStockCheckMapper;

    @Autowired
    private RedisService redisService;

    @Autowired
    private GrandSaleStadiumMapper grandSaleStadiumMapper;


    /**
     * 库存校验
     * @param amount
     * @param skuId
     * @return
     */
    @Override
    public Boolean stockCheck(int amount, int skuId,String promotionId,String promotionType,String stadiumType) {

        int allAmount=0;
        logger.info("库存校验入参：amount:"+amount+",skuId:"+skuId+",promotionId:"+promotionId+",promotionType:"+promotionType+",stadiumType:"+stadiumType);
        //查询即将开始以及正在进行的秒杀活动
        List<FlashSale> selectFlaseSale=promotionStockCheckMapper.selectFlashSale(null);
        int flashSaleSkuAmount=0;
        for (FlashSale flashSale : selectFlaseSale) {

            int flashSaleSkuId=flashSale.getSkuId();
            if(flashSaleSkuId==skuId){
                String enCodeskuId = IdTypeHandler.encode(flashSale.getSkuId());
                String promotionId2 =IdTypeHandler.encode(flashSale.getPromotionId());
                String flashSaleSkuAmountKey = PromotionConstants.getFlashSaleAmountKey(promotionId2, enCodeskuId);
                String key =redisService.get(flashSaleSkuAmountKey);
                logger.info("秒杀活动库存校验的promotionId:"+flashSale.getPromotionId()+"skuId:"+flashSale.getSkuId()+"的key:"+flashSaleSkuAmountKey+",值为："+key);
                if(key!=null){
                    flashSaleSkuAmount+=Integer.parseInt(redisService.get(flashSaleSkuAmountKey));
                }
            }
        }
        logger.info("秒杀活动中的skuId为："+skuId+",秒杀中的活动占用库存为(包括现在的skuId的库存)："+flashSaleSkuAmount);
        allAmount +=flashSaleSkuAmount;

        //查询skuCode在套装中的库存
        boolean isCombine=promotionStockCheckMapper.selectIsBindedSlave(skuId);
        int bindedSlaveAmount=promotionStockCheckMapper.sumBindedSlaveAmount(skuId);
        if(isCombine){
            allAmount+=bindedSlaveAmount;
        }

        //查询抽奖设置的件数
        int lotterAmount=promotionStockCheckMapper.selectLotteryAmount(skuId,null);
        allAmount+=lotterAmount;

        //根据skuCode查询拼团商品剩余库存
        int pieceAmount=promotionStockCheckMapper.selectPieceAmountBySkuCode(skuId,null);
        allAmount+=pieceAmount;

        //根据skuCode查询新人、折扣、分会场的商品剩余库存
        int freshmanAndDiscountAndStadiumAmount=promotionStockCheckMapper.selectFreshmanAndDiscountAndStadiumAmount(skuId);
        allAmount+=freshmanAndDiscountAndStadiumAmount;

        //总库存
        int skuAmount=promotionStockCheckMapper.selectAmountBySkuCode(skuId);

        int userflashAmount=0;
        int userlotterAmount=0;
        int userPieceAmount=0;
        int userProductSaleAmount=0;
        switch (promotionType){
            case "FLASESALE":
                String enCodeskuId = IdTypeHandler.encode(skuId);
                String promotionId2 =IdTypeHandler.encode(Integer.parseInt(promotionId));
                String flashSaleSkuAmountKey = PromotionConstants.getFlashSaleAmountKey(promotionId2, enCodeskuId);
                String key =redisService.get(flashSaleSkuAmountKey);
                logger.info("秒杀修改活动库存校验的promotionId:"+promotionId+"skuId:"+skuId+"的key:"+flashSaleSkuAmountKey+",值为："+key);
                if(key!=null){
                    allAmount-=Integer.parseInt(redisService.get(flashSaleSkuAmountKey));
                    userflashAmount=flashSaleSkuAmount-(Integer.parseInt(redisService.get(flashSaleSkuAmountKey)));
                }
                break;
            case "LOTTER":
                allAmount-=promotionStockCheckMapper.selectLotteryAmount(skuId,promotionId);
                userlotterAmount=lotterAmount-(promotionStockCheckMapper.selectLotteryAmount(skuId,promotionId));
                break;
            case "PIECE":
                allAmount-=promotionStockCheckMapper.selectPieceAmountBySkuCode(skuId,promotionId);
                userPieceAmount=pieceAmount-(promotionStockCheckMapper.selectPieceAmountBySkuCode(skuId,promotionId));
                break;
            case "PRODUCTSALE":
                allAmount-=promotionStockCheckMapper.selectProductSaleAmount(promotionId,skuId);
                userProductSaleAmount=freshmanAndDiscountAndStadiumAmount-(promotionStockCheckMapper.selectProductSaleAmount(promotionId,skuId));
                break;
            case "FRESHMAN":
                allAmount-=promotionStockCheckMapper.selectFreshmanAmount(skuId);
                userProductSaleAmount=freshmanAndDiscountAndStadiumAmount-(promotionStockCheckMapper.selectFreshmanAmount(skuId));
                break;
            case "STADIUM2":
                int stadium=Integer.parseInt(promotionId);
                GrandSaleStadium grandSaleStadium = grandSaleStadiumMapper.selectOne(stadium);
                if(grandSaleStadium==null){
                    throw new BizException(GlobalErrorCode.UNKNOWN,"分会场已失效或已下架");
                }
                String type = grandSaleStadium.getType();
                Integer grandSaleId = grandSaleStadium.getGrandSaleId();
                int stadiumAmount2 = promotionStockCheckMapper.selectStadiumAmount(type,grandSaleId,skuId);
                allAmount-=stadiumAmount2;
                userProductSaleAmount=freshmanAndDiscountAndStadiumAmount-stadiumAmount2;
                break;
            case "STADIUM":
                int stadiumAmount = promotionStockCheckMapper.selectStadiumAmount(stadiumType,Integer.parseInt(promotionId),skuId);
                allAmount-=stadiumAmount;
                userProductSaleAmount=freshmanAndDiscountAndStadiumAmount-stadiumAmount;
                break;
        }
        //校验可用库存是否够用(总库存-活动中的库存>=需要用的库存)
        logger.info("总库存为："+skuAmount+",活动中的库存为："+allAmount+"，amount"+amount);
        if((skuAmount-allAmount)>=amount){
            return true;
        }else{
            if (userflashAmount==0)
                userflashAmount=flashSaleSkuAmount;
            if (userlotterAmount==0)
                userlotterAmount=lotterAmount;
            if (userPieceAmount==0)
                userPieceAmount=pieceAmount;
            if (userProductSaleAmount==0)
                userProductSaleAmount=freshmanAndDiscountAndStadiumAmount;
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,"输入的库存大于可用库存，skuId:"+skuId+",总库存为："+skuAmount+",秒杀："+userflashAmount+"个，套装："+bindedSlaveAmount+"个，抽奖："+userlotterAmount+"个，拼团："+userPieceAmount+"个，新人、折扣、分会场总共："+userProductSaleAmount+"个");
        }
    }
}
