package cn.hds.hvmall.service.stockcheck;

import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author LiHaoYang
 * @ClassName PromotionStockCheckService
 * @date 2019/6/27 0027
 */
public interface PromotionStockCheckService {


    Boolean stockCheck(int amount, int skuId, String promotionId,String promotionType,String stadiumType);

}
