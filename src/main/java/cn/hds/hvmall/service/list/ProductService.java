package cn.hds.hvmall.service.list;

import cn.hds.hvmall.base.BaseExecuteResult;
import cn.hds.hvmall.entity.list.*;
import cn.hds.hvmall.entity.product.Product;
import cn.hds.hvmall.pojo.list.Category;
import cn.hds.hvmall.pojo.list.ExecuteResult;
import cn.hds.hvmall.pojo.list.InfoSimple;
import cn.hds.hvmall.pojo.list.SearchTerm;
import cn.hds.hvmall.pojo.prod.ProductSkuVO;
import cn.hds.hvmall.service.CopyAble;
import cn.hds.hvmall.service.UpdateAble;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import java.util.*;

/**
 * @Author: zzl
 * @Date: 2018/9/11 14:25
 * @Version
 */
public interface ProductService extends CopyAble<ProductSkuVO>, UpdateAble<ProductSkuVO> {
  /**
   * 依据条件搜索商品
   */
  ExecuteResult<List<ProductSearchSimple>> searchProduct(InfoSimple info, int pageSize);

  /**
   * 根据条件查询Sku
   * @param searchTerm 查询条件封装
   * @param pageable 分页数据
   * @return 商品sku列表
   */
  Map searchSku(SearchTerm searchTerm, Pageable pageable);

  /**
   * 分页查询分会场商品数据
   *
   * @param type 会场类型
   * @param categoryId 分类id
   * @param page 分页
   * @return 分会场商品列表
   */
  List<ProductSkuSimpleVO> searchPromotionProduct(String type, String categoryId, @Param("page") Pageable page);

  /**
   * 统计数量分页查询分会场商品数据
   * @param type 分页
   * @param categoryId 分类id
   * @return 分会场商品列表
   */
  int countPromotionProduct(@Param("page") String type, String categoryId);

  /**
   * 查询供应商名称列表
   */
  BaseExecuteResult<List<Supplier>> searchSupplier();

  /**
   * 查询商品分类列表
   */
  BaseExecuteResult<List<Category>> searchCategory();

  /**
   * 查询商品品牌列表
   */
  BaseExecuteResult<List<Brand>> searchBrand();

  /**
   * 保存商品
   * @param product 商品对象
   * @return 保存结果
   */
  boolean savePd(Product product);

  /**
   * 保存商品-sku封装对象
   * @param productSkuVO 商品-sku对象
   * @return 保存结果
   */
  @Override
  boolean save(ProductSkuVO productSkuVO);

  /**
   * 根据id查询商品
   * @param id 商品id
   * @return 商品对象
   */
  Optional<Product> load(Long id);

  Optional<List<Product>> loadByIds(Collection<String> ids);


  /**
   * 根据id查询商品-sku对象
   * @param id 商品id
   * @return 查询结果
   */
  Optional<ProductSkuVO> loadSkuVO(Long id);

  boolean exists(Long id);

  /**
   * 更新商品记录
   * @param record 商品记录
   * @return 更新结果
   */
  boolean updateByPrimaryKeySelective(Product record);

}
