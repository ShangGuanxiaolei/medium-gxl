package cn.hds.hvmall.service.list;

import cn.hds.hvmall.entity.sku.Sku;
import cn.hds.hvmall.pojo.grandsale.ProductIdSkuCodeDTO;
import cn.hds.hvmall.pojo.grandsale.StadiumVO;
import cn.hds.hvmall.pojo.prod.ProductSkuVO;
import cn.hds.hvmall.service.CopyAble;
import cn.hds.hvmall.service.UpdateAble;
import cn.hds.hvmall.type.StadiumType;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author wangxinhua
 * @version 1.0.0 $ 2019/3/28
 */
public interface SkuService extends CopyAble<Sku>, UpdateAble<Sku> {

    /**
     * 根据id查询
     * @param id id
     * @return 查询到的sku
     */
    Optional<Sku> loadById(Long id);

    /**
     * 根据skuCode查询
     * @param skuCode sku编码
     * @return 查询结果
     */
    Optional<Sku> loadByCode(String skuCode);

    boolean checkIsNotDelete(String skuCode);

    String selectSkuCode(Sku sku);


    Optional<List<ProductIdSkuCodeDTO>> loadProductIdSkuCodeDTOs(Collection<String> skuCodes);

    Optional<List<Long>> loadProductBySkuCodes(Collection<String> skuCodes);


    Optional<List<ProductSkuVO>> loadAlreadyCopyBySkuCodes(Collection<String> skuCodes,Integer grandId);

    Optional<List<ProductSkuVO>> copyImport(StadiumType stadiumType, Map<String, StadiumVO> maps,Integer grandId);

}
