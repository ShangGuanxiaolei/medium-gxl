package cn.hds.hvmall.service.list.impl;

import cn.hds.hvmall.base.BaseExecuteResult;
import cn.hds.hvmall.entity.list.*;
import cn.hds.hvmall.entity.product.Product;
import cn.hds.hvmall.entity.sku.Sku;
import cn.hds.hvmall.mapper.list.ProductMapper;
import cn.hds.hvmall.mapper.sku.SkuMapper;
import cn.hds.hvmall.pojo.list.Category;
import cn.hds.hvmall.pojo.list.ExecuteResult;
import cn.hds.hvmall.pojo.list.InfoSimple;
import cn.hds.hvmall.pojo.list.SearchTerm;
import cn.hds.hvmall.pojo.prod.ProductSkuVO;
import cn.hds.hvmall.service.list.ProductService;
import cn.hds.hvmall.utils.ConstantUtil;
import cn.hds.hvmall.utils.Transformer;
import cn.hds.hvmall.utils.list.GlobalErrorCode;
import cn.hds.hvmall.utils.list.qiniu.BizException;
import cn.hds.hvmall.utils.list.qiniu.IdTypeHandler;
import com.google.common.base.Preconditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service("productService")
public class ProductServiceImpl implements ProductService {
    private static final Logger logger = LoggerFactory.getLogger(ProductServiceImpl.class);

    private final ProductMapper productMapper;
    private final SkuMapper skuMapper;

    @Autowired
    public ProductServiceImpl(ProductMapper productMapper, SkuMapper skuMapper) {
        this.productMapper = productMapper;
        this.skuMapper = skuMapper;
    }

    @Override
    public ExecuteResult<List<ProductSearchSimple>> searchProduct(InfoSimple info, int pageSize) {
        int pageNumber = (info.getPageNum() - 1) * pageSize;
        ExecuteResult<List<ProductSearchSimple>> result = new ExecuteResult<>();
        try {
            result.setCount(productMapper.countProduct(info));
            List<ProductSearchSimple> productSearchSimples = productMapper.searchProduct(info, pageNumber, pageSize);
            for (ProductSearchSimple productSearchSimple :
                    productSearchSimples) {
                List<SkuList> skuLists = productMapper.searchSku(productSearchSimple.getProductId());
                //设置商品规格
                productSearchSimple.setSkuList(skuLists);
                List<Double> skuPriceList = new ArrayList<>();
                int amount = 0;
                for (SkuList skuList :
                        skuLists) {
                    amount += skuList.getSkuAmount();
                    //将每个规格的价格加入到列表中
                    skuPriceList.add(skuList.getSkuPrice());
                }
                String price = getPrice(skuPriceList);
                //设置sku价格
                productSearchSimple.setPrice(price);
                //设置库存为所有规格库存总和
                productSearchSimple.setAmount(amount);
            }
            //返回新的vo集合
            result.setResult(productSearchSimples);
            result.setIsSuccess(ConstantUtil.success);
        } catch (Exception e) {
            logger.error("系统错误,入参格式有误!", e);
            result.setIsSuccess(ConstantUtil.failed);
            result.setErrorCode(ConstantUtil.ResponseError.SYS_ERROR.getCode());
            result.setErrorMsg(ConstantUtil.ResponseError.SYS_ERROR.toString());
        }
        return result;
    }

    @Override
    public Map searchSku(SearchTerm searchTerm, Pageable pageable) {
        List<ProductSkuSimple> productSkuSimples = productMapper.searchProductSku(searchTerm, pageable);
        Map<String, Object> map = new HashMap<>();
        map.put("productSkuSimples", productSkuSimples);
        map.put("count", productMapper.countProductSku(searchTerm));
        return map;
    }

    @Override
    public List<ProductSkuSimpleVO> searchPromotionProduct(String type, String categoryId, Pageable page) {
        return productMapper.searchPromotionProduct(type, categoryId, page);
    }

    @Override
    public int countPromotionProduct(String type, String categoryId) {
        return productMapper.countPromotionProduct(type, categoryId);
    }

    /**
     * 返回价格的最小值~最大值区间
     *
     * @param priceList 价格列表
     * @return String
     */
    private String getPrice(List<Double> priceList) {
        Double max = Collections.max(priceList);
        Double min = Collections.min(priceList);
        if (max.equals(min)) {
            return min + "";
        } else {//多规格显示价格区间
            return min + "~" + max;
        }
    }


    @Override
    public BaseExecuteResult<List<Supplier>> searchSupplier() {
        BaseExecuteResult<List<Supplier>> result = new BaseExecuteResult<>();
        try {
            List<Supplier> list = productMapper.selectSupplier();
            result.setResult(list);
            result.setIsSuccess(ConstantUtil.success);
        } catch (Exception e) {
            e.printStackTrace();
            result.setIsSuccess(ConstantUtil.failed);
            result.setErrorCode(ConstantUtil.ResponseError.OBJECTNULL.getCode());
            result.setErrorMsg(ConstantUtil.ResponseError.OBJECTNULL.toString());
        }
        return result;
    }


    @Override
    public BaseExecuteResult<List<Category>> searchCategory() {
        BaseExecuteResult<List<Category>> result = new BaseExecuteResult<>();
        try {
            List<Category> list = productMapper.selectCategory();
            for (Category category :
                    list) {
                if (null == category.getId()) {
                    throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "一级分类id不存在");
                }
                List<Category> categories = productMapper.selectTwoCategory(category.getId());
                category.setCategories(categories);
            }
            result.setResult(list);
            result.setIsSuccess(ConstantUtil.success);
        } catch (Exception e) {
            e.printStackTrace();
            result.setIsSuccess(ConstantUtil.failed);
            result.setErrorCode(ConstantUtil.ResponseError.SYS_ERROR.getCode());
            result.setErrorMsg(ConstantUtil.ResponseError.SYS_ERROR.toString());
        }
        return result;
    }

    @Override
    public BaseExecuteResult<List<Brand>> searchBrand() {
        BaseExecuteResult<List<Brand>> result = new BaseExecuteResult<>();
        try {
            List<Brand> list = productMapper.selectBrand();
            result.setResult(list);
            result.setIsSuccess(ConstantUtil.success);
        } catch (Exception e) {
            e.printStackTrace();
            result.setIsSuccess(ConstantUtil.failed);
            result.setErrorCode(ConstantUtil.ResponseError.SYS_ERROR.getCode());
            result.setErrorMsg(ConstantUtil.ResponseError.SYS_ERROR.toString());
        }
        return result;
    }

    @Override
    public boolean savePd(Product product) {
        return productMapper.insert(product) > 0;
    }

    @Override
    public Optional<Product> load(Long id) {
        return Optional.ofNullable(productMapper.selectByPrimaryKey(id));
    }

    @Override
    public Optional<List<Product>> loadByIds(Collection<String> ids) {
        return Optional.empty();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean save(ProductSkuVO productSkuVO) {
        Preconditions.checkNotNull(productSkuVO, "productSkuVO不能为空");
        boolean pdRet = savePd(productSkuVO);
        if (!pdRet) {
            return false;
        }
        productSkuVO.setCode(IdTypeHandler.encode(productSkuVO.getId()));
        boolean updateCode = update(productSkuVO);

        if (!updateCode){
            return false;
        }

        List<Sku> skuList = productSkuVO.getSkuList();
        // 循环保存sku
        // FIXME 在sql层实现一条sql直接保存该实体
        Optional<Integer> first = skuList.stream()
                .map(sku-> {
                    sku.setProductId(Long.toString(productSkuVO.getId()));
                    return skuMapper.insert(sku);
                }).filter(i -> i == 0).findFirst();
        if (first.isPresent()) {
            // 有保存失败的记录，则抛出异常回滚事物
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "sku记录保存失败");
        }
        return true;
    }

    @Override
    public Optional<ProductSkuVO> loadSkuVO(Long id) {
        // FIXME 使用性能更好的关联查询
        return load(id).map(Transformer.fromBean(ProductSkuVO.class))
                .map(p -> {
                    p.setSkuList(skuMapper.selectByProductId(id));
                    return p;
                });
    }

    @Override
    public boolean exists(Long id) {
        return productMapper.exists(id);
    }

    @Override
    public boolean updateByPrimaryKeySelective(Product record) {
        return productMapper.updateByPrimaryKeySelective(record) > 0;
    }

    @Override
    public boolean update(ProductSkuVO productSkuVO) {
        return productMapper.updateByPrimaryKeySelective(productSkuVO)>0;
    }
}
