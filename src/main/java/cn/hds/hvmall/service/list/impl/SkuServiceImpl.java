package cn.hds.hvmall.service.list.impl;

import cn.hds.hvmall.entity.sku.Sku;
import cn.hds.hvmall.enums.ProductStatus;
import cn.hds.hvmall.mapper.freshmantab.FreshManXquarkSkuCopyMapper;
import cn.hds.hvmall.mapper.freshmantab.FreshmanMapper;
import cn.hds.hvmall.mapper.sku.SkuMapper;
import cn.hds.hvmall.pojo.freshmantab.XquarkSkuCombine;
import cn.hds.hvmall.pojo.freshmantab.XquarkSkuCombineExtra;
import cn.hds.hvmall.pojo.grandsale.ProductIdSkuCodeDTO;
import cn.hds.hvmall.pojo.grandsale.StadiumVO;
import cn.hds.hvmall.pojo.prod.ProductSkuVO;
import cn.hds.hvmall.service.grandsale.impl.GrandSaleStadiumServiceImpl;
import cn.hds.hvmall.service.list.ProductService;
import cn.hds.hvmall.service.list.SkuService;
import cn.hds.hvmall.type.StadiumType;
import cn.hds.hvmall.utils.BeanUtil;
import cn.hds.hvmall.utils.list.GlobalErrorCode;
import cn.hds.hvmall.utils.list.qiniu.BizException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author wangxinhua
 * @version 1.0.0 $ 2019/3/28
 */
@Service
public class SkuServiceImpl implements SkuService {

    private final SkuMapper skuMapper;

    private final ProductService productService;


    @Autowired
    private FreshmanMapper freshmanMapper;

    @Autowired
    private FreshManXquarkSkuCopyMapper freshManXquarkSkuCopyMapper;

    @Autowired
    public SkuServiceImpl(SkuMapper skuMapper,ProductService productService) {
        this.skuMapper = skuMapper;
        this.productService = productService;
    }

    @Override
    public boolean save(Sku sku) {
        return skuMapper.insert(sku) > 0;
    }

    @Override
    public Optional<Sku> loadById(Long id) {
        return Optional.ofNullable(skuMapper.selectByPrimaryKey(id));
    }

    @Override
    public Optional<Sku> loadByCode(String skuCode) {
        return Optional.ofNullable(skuMapper.selectBySkuCode(skuCode));
    }

    @Override
    public Optional<List<ProductIdSkuCodeDTO>> loadProductIdSkuCodeDTOs(Collection<String> skuCodes) {
        return Optional.empty();
    }

    @Override
    public Optional<List<Long>> loadProductBySkuCodes(Collection<String> skuCodes) {
        return Optional.ofNullable(skuMapper.loadProductBySkuCodes(skuCodes));
    }

    @Override
    public Optional<List<ProductSkuVO>> loadAlreadyCopyBySkuCodes(Collection<String> skuCodes,Integer grandId) {
        // 已经更新过copy过的 skuCode 当前主会场skuCode的规则为 sourceSkuCode + promotionType + grandId
        List<Sku> skuList = skuMapper.loadAlreadyCopyBySkuCodes(skuCodes,grandId);
        List<ProductSkuVO> productSkuVOList = skuList.stream().map(Sku::getProductId)
                .distinct()
                .map(s -> productService.loadSkuVO(Long.parseLong(s))
                        .orElseThrow(BizException.build(GlobalErrorCode.INTERNAL_ERROR,"该商品不存在")))
                .collect(Collectors.toList());

      return Optional.of(productSkuVOList);
    }

    @Override
    public Optional<List<ProductSkuVO>> copyImport(StadiumType stadiumType,Map<String, StadiumVO> maps,Integer grandId) {
        if (CollectionUtils.isEmpty(maps)) {
            return Optional.empty();
        }

        // 查找product是否已经被复制过 如果已经复制过只复制 sku
        List<ProductSkuVO> collect = loadProductBySkuCodes((maps.keySet())).orElseGet(ArrayList::new).stream()
                .map(s -> productService.loadSkuVO(s)
                        .orElseThrow(BizException.build(GlobalErrorCode.INVALID_ARGUMENT,"productId" + s + "不存在"))
                ).map(s -> productService.copy(s, ProductSkuVO.class, c -> {

                        // 如果没有复制过
                        // 处理product
                        c.setSourceId(c.getId());
                        c.setCreatedAt(new Date());
                        c.setUpdatedAt(new Date());
                        c.setStatus(ProductStatus.ONSALE);

                        List<Sku> list = new ArrayList<>();

                        // 处理skuList
                        c.getSkuList().forEach(sku -> {
                            StadiumVO stadiumVO = maps.get(sku.getSkuCode());
                            if (Objects.isNull(stadiumVO)) {
                                return;
                            }
                            BeanUtils.copyProperties(stadiumVO, sku, BeanUtil.getNullPropertyNames(stadiumVO));
                            sku.setId(null);
                            sku.setPromotionType(stadiumType.toString());
                            sku.setSourceSkuCode(sku.getSkuCode());
                            // skuCode 生成规则
                            sku.setSkuCode(sku.getSkuCode() + stadiumType.toString() + grandId);
                            sku.setSecureAmount(0);
                            list.add(sku);
                        });

                        c.setSkuList(list);

                        // 设置价格属性
                        Optional<Sku> min = c.getSkuList().stream().min(Comparator.comparing(Sku::getPrice));
                        StadiumVO sv = maps.get(min.orElseThrow(BizException.build(GlobalErrorCode.INVALID_ARGUMENT, "skuCode不存在")).getSourceSkuCode());
                        //设置活动库存
                        if (sv.getAmount() < 0)
                            sv.setAmount(0);
                        //设置限购
                        if (sv.getBuyLimit() < 0)
                            sv.setBuyLimit(0);
                        c.setBuyLimit(sv.getBuyLimit());
                        //修改为已上架
                        c.setStatus(ProductStatus.ONSALE);
                        c.setPrice(sv.getPrice());
                        c.setDeductionDPoint(sv.getDeductionDPoint());
                        // 设置id为null
                        c.setId(null);
                    })).collect(Collectors.toList());
        //判断商品是否为套装,如果是套装,复制原套装combine信息
        for (ProductSkuVO productSkuVO : collect) {
            Long sourceId = productSkuVO.getSourceId();
            Long id = productSkuVO.getId();
            //查询原商品是否是套装商品
            Boolean combineProduct = freshmanMapper.isCombineProduct(sourceId);
            if(combineProduct==true){
                cn.hds.hvmall.pojo.freshmantab.XquarkSku xquarkSku = freshManXquarkSkuCopyMapper.selectSkuIdByProductId(id);
                XquarkSkuCombine xquarkSkuCombine = new XquarkSkuCombine();
                xquarkSkuCombine.setProductId(id);
                xquarkSkuCombine.setSkuId(xquarkSku.getId());
                //插入套装表xquark_sku_combine
                int i4 = freshmanMapper.insertXquarkSkuCombine(xquarkSkuCombine);
                if (i4==0){
                    throw  new BizException(GlobalErrorCode.valueOf("插入xquark_sku_combine表数据失败:"+id));
                }
                XquarkSkuCombine sourceCombineId = freshmanMapper.selectCombineIdByProductId(sourceId);
                List<XquarkSkuCombineExtra> xquarkSkuCombineExtras = freshmanMapper.selectCombineExtraByMasterId(sourceCombineId.getId());
                for (XquarkSkuCombineExtra xquarkSkuCombineExtra : xquarkSkuCombineExtras) {
                    XquarkSkuCombineExtra xquarkSkuCombineExtra1 = new XquarkSkuCombineExtra();
                    xquarkSkuCombineExtra1.setMasterId(xquarkSkuCombine.getId());
                    xquarkSkuCombineExtra1.setSlaveId(xquarkSkuCombineExtra.getSlaveId());
                    xquarkSkuCombineExtra1.setAmount(xquarkSkuCombineExtra.getAmount());
                    Integer integer2 = freshmanMapper.insertCombineExtra(xquarkSkuCombineExtra1);
                    if (integer2==0){
                        throw  new BizException(GlobalErrorCode.valueOf("插入xquark_sku_combine_extra表数据失败:"+id));
                    }
                }
            }
        }
        return Optional.of(collect);
    }

    @Override
    public boolean update(Sku record) {
        return skuMapper.updateByPrimaryKeySelective(record) > 0;
    }

    @Override
    public boolean checkIsNotDelete(String skuCode) {
        return skuMapper.checkIsNotDelete(skuCode);
    }

    @Override
    public String selectSkuCode(Sku sku){
        return skuMapper.selectSkuCode(Long.valueOf(sku.getSkuId()));
    }
}
