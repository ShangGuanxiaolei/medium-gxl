package cn.hds.hvmall.service.promotion;

import cn.hds.hvmall.base.PageBase;
import cn.hds.hvmall.entity.piece.PromotionBaseInfo;
import cn.hds.hvmall.entity.promotion.PromotionInviteCode;
import cn.hds.hvmall.entity.promotion.PromotionProduct;
import cn.hds.hvmall.entity.promotion.PromotionVo;
import cn.hds.hvmall.pojo.promotion.Promotion;
import com.github.pagehelper.PageInfo;

import java.util.Date;
import java.util.Map;

/**
 * @author: create by daiweiwei
 * @version: v1.0
 * @description:
 * @date: 2018/10/18 14:39
 */
public interface PromotionService {

    boolean save(Promotion promotion);

    Promotion load(Long id);

    boolean update(Promotion promotion);

    String addPromotion(PromotionVo promotionVo);

    PageInfo getPromotionProduct(PromotionProduct page);

    Map getPromotion(String pCode, int pageNum, int pageSize);

    String editPromotion(PromotionVo promotionVo);

    String editPromotionProduct(PromotionProduct page);

    String deletePromotionProduct(String skuCode, String projectCode);

    PageInfo getAllProducts(String projectCode, int pageNum, int pageSize);

    /**
     * 活动配置表分页查询活动信息
     * @param startNum
     * @param pageSize
     * @return
     */
    PageBase<PromotionBaseInfo> queryPromotionBaseInfoList (String pName,String pStatus,String productName,String skuCode,int startNum, int pageSize);

    /**
     * 根据pCode更新活动状态
     * @param pCode
     * @param pStatus
     * @return
     */
    Boolean updatePromotionStatusByPcode(String pCode,String pStatus);

    /**
     * 删除大会活动
     * @param pCode
     * @return
     */
    boolean DetelPromotion(String pCode);


    int initInviteCode(String pCode, int codeNum);

    /**
     *根据参数查询邀请码信息
     * @param pCode
     * @param cpid
     * @param code
     * @param pageNo
     * @param pageSize
     * @return
     */
    PageBase<PromotionInviteCode> selectAllBypCode(String pCode, String cpid, String code, int pageNo,  int pageSize);
    /**
     * 功能描述: 追加大會活動庫存
     * @author Luxiaoling
     * @date 2019/1/16 17:33
     * @param  pCode, skuCode, addStock
     * @return  java.lang.String
     */
    String addMeetingStock(String pCode, String skuCode, Integer addStock);

    /**
     * 功能描述: 大会活动单品上下架
     * @param pCode
     * @param skuCode
     * @return
     */
    String meetingProductGrounding(String pCode, String skuCode,String groundType);

    Promotion loadBySkuId(Long productId, Long skuId);

    Promotion loadBySkuId(Long productId, Long skuId, Date date);

    boolean close(Long id);

    boolean deletedFlashSale(Long id);

    boolean unClose(Long id);
}
