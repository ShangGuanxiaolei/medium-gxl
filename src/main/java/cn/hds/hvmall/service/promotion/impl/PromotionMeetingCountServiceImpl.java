package cn.hds.hvmall.service.promotion.impl;

import cn.hds.hvmall.base.BaseExecuteResult;
import cn.hds.hvmall.entity.piece.OrderListVO;
import cn.hds.hvmall.entity.promotion.MeetingInfoVo;
import cn.hds.hvmall.entity.promotion.PromotionMeetingCountInfo;
import cn.hds.hvmall.mapper.piece.PromotionMeetingCountInfoMapper;
import cn.hds.hvmall.pojo.activity.ActivityVO;
import cn.hds.hvmall.pojo.list.ExecuteResult;
import cn.hds.hvmall.service.promotion.PromotionMeetingCountService;
import cn.hds.hvmall.utils.JSONUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@Service("promotionMeetingCountService")
public class PromotionMeetingCountServiceImpl implements PromotionMeetingCountService {


    @Autowired
    private PromotionMeetingCountInfoMapper promotionMeetingCountInfoMapper;

    @Override
    public PromotionMeetingCountInfo selectMeetingInfo(String pCode) {
        PromotionMeetingCountInfo promotionMeetingCountInfo=new PromotionMeetingCountInfo();
        try {
            promotionMeetingCountInfo.setEffectFrom(promotionMeetingCountInfoMapper.selectTimeBypCode(pCode).getEffectFrom());
            promotionMeetingCountInfo.setEffectTo(promotionMeetingCountInfoMapper.selectTimeBypCode(pCode).getEffectTo());
            promotionMeetingCountInfo.setpName(promotionMeetingCountInfoMapper.selectTimeBypCode(pCode).getpName());
            promotionMeetingCountInfo.setStock(promotionMeetingCountInfoMapper.selectStock(pCode));
            promotionMeetingCountInfo.setPaidOrderCount(promotionMeetingCountInfoMapper.selectPaidOrder(pCode));
            promotionMeetingCountInfo.setFailOrderCount(promotionMeetingCountInfoMapper.selectFailOrder(pCode));
            List<OrderListVO> list=promotionMeetingCountInfoMapper.selectOrderCountAndStatus(pCode);
            int paid=0;
            int shipped=0;
            for (int i = 0; i < list.size(); i++) {
                String status=list.get(i).getStatus();
                if(status.equals("SUBMITTED")){
                    promotionMeetingCountInfo.setSubmit(list.get(i).getCount());
                }
                if(status.equals("PAID")){
                    paid+=list.get(i).getCount();
                }
                if(status.equals("DELIVERY")){
                    paid+=list.get(i).getCount();
                    shipped+=list.get(i).getCount();
                }
                if(status.equals("REISSUING")){
                    paid+=list.get(i).getCount();
                    shipped+=list.get(i).getCount();
                }
                if(status.equals("SHIPPED")){
                    paid+=list.get(i).getCount();
                    shipped+=list.get(i).getCount();
                }
                if(status.equals("SUCCESS")){
                    paid+=list.get(i).getCount();
                    shipped+=list.get(i).getCount();
                }
                if(status.equals("COMMENT")){
                    paid+=list.get(i).getCount();
                    shipped+=list.get(i).getCount();
                }
                if(status.equals("CANCELLED")){
                    promotionMeetingCountInfo.setCancelled(list.get(i).getCount());
                }

            }
            promotionMeetingCountInfo.setPaid(paid);
            promotionMeetingCountInfo.setShipped(shipped);
            promotionMeetingCountInfo.setHourRanks(promotionMeetingCountInfoMapper.selectHourTime(pCode));

        }catch (Exception e){
            e.printStackTrace();

        }
        return promotionMeetingCountInfo;
    }

    @Override
    public BaseExecuteResult<List<MeetingInfoVo>> selectMeeting(String json) {
        ExecuteResult<List<MeetingInfoVo>> result = new ExecuteResult<>();
        Map<String, Object> map = JSONUtil.toMap(json);
        String pCode=map.get("pCode").toString();
        String  cpid = "";
        String  orderNo="";
        if(null!=map.get("cpid")){
            cpid= map.get("cpid").toString();
        }
        if(null!=map.get("orderNo")){
            orderNo= map.get("orderNo").toString();
        }
        List<MeetingInfoVo> list=promotionMeetingCountInfoMapper.selectMeetingInfo(pCode,cpid,orderNo);
        List<MeetingInfoVo> list1=promotionMeetingCountInfoMapper.selectGiftAmount(pCode,cpid,orderNo);
        if(list.size()==0){
            //throw new BizException(GlobalErrorCode.NOT_FOUND,"未找到该项目");
            result.setIsSuccess(0);
        }else{
            result.setIsSuccess(1);
        }
        for (int j = 0; j <list1.size() ; j++) {
            String orderCode=list.get(j).getOrderId();

            for (int i = 0; i < list.size(); i++) {
                String orderCode2 = list.get(i).getOrderId();
                    if(orderCode2.equals(orderCode)){
                        list.get(i).setGiftAmount(list1.get(j).getAmount());
                    }

                int count = list.get(i).getAmount();
                BigDecimal price = list.get(i).getMarkPrice();
                BigDecimal priceAll = price.multiply(new BigDecimal(count)).setScale(2, BigDecimal.ROUND_HALF_UP);
                list.get(i).setPrice(priceAll.toString());
            }


        }
        result.setResult(list);
        return result;
    }
}
