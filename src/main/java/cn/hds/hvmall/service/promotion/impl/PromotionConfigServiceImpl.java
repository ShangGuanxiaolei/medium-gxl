package cn.hds.hvmall.service.promotion.impl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.hds.hvmall.entity.promotion.PromotionConfigVo;
import cn.hds.hvmall.mapper.promotion.PromotionConfigMapper;
import cn.hds.hvmall.pojo.promotion.PromotionConfig;
import cn.hds.hvmall.service.promotion.PromotionConfigService;

/**
 * Created with IDEA
 * author:Yarm.Yang
 * Date:2019/1/25
 * Time:10:59
 * Des:活动配置
 */
@Service
public class PromotionConfigServiceImpl implements PromotionConfigService {

    @Autowired
    private PromotionConfigMapper promotionConfigMapper;
    @Override
    public int insert(PromotionConfig p) {
        if(p == null){
            return 0;
        }
        if(StringUtils.isBlank(p.getConfigName())){
            return 0;
        }
        //校验存在
        boolean b = this.promotionConfigMapper.checkConfigName(p.getConfigName());
        if(b){//存在
            return 0;
        }
        PromotionConfigVo pcVO = new PromotionConfigVo();
        pcVO.setConfigName(p.getConfigName());
        pcVO.setConfigType(p.getConfigType()); //拼团
        pcVO.setConfigValue(p.getConfigValue());
        int count = promotionConfigMapper.insertByVo(pcVO);
        return count;
    }

    @Override
    public int update(PromotionConfig p) {
        if(p == null){
            return 0;
        }
        if(StringUtils.isBlank(p.getConfigName())){
            return 0;
        }
        PromotionConfigVo pcVO = new PromotionConfigVo();
        pcVO.setConfigName(p.getConfigName());
        pcVO.setConfigType(p.getConfigType()); //拼团
        pcVO.setConfigValue(p.getConfigValue());
        int count = this.promotionConfigMapper.updateByVo(pcVO);
        return count;
    }

    @Override
    public PromotionConfig select(PromotionConfig p) {
        PromotionConfigVo pcVO = new PromotionConfigVo();
        pcVO.setConfigName(p.getConfigName());
        pcVO.setConfigType(p.getConfigType()); //拼团
        PromotionConfigVo pVo = this.promotionConfigMapper.selectByVo(pcVO);
        PromotionConfig promotionConfig= this.convertPromotionConfigVo(pVo);
        return promotionConfig;
    }

    /**
     * 转换数据
     * @param pVo
     * @return
     */
    private PromotionConfig convertPromotionConfigVo(PromotionConfigVo pVo) {
        if(pVo == null){
            return null;
        }
        PromotionConfig pc = new PromotionConfig();
        pc.setId(pVo.getId());
        pc.setConfigName(pVo.getConfigName());
        pc.setConfigValue(pVo.getConfigValue());
        pc.setConfigType(pVo.getConfigType());
        pc.setCreatedAt(pVo.getCreatedAt());
        return pc;
    }
}