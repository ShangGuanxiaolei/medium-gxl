package cn.hds.hvmall.service.promotion;

import cn.hds.hvmall.pojo.promotion.PromotionConfig;

/**
 * Created with IDEA
 * author:Yarm.Yang
 * Date:2019/1/25
 * Time:10:59
 * Des:
 */
public interface PromotionConfigService {
    int insert(PromotionConfig p);

    int update(PromotionConfig p);

    PromotionConfig select(PromotionConfig p);
}