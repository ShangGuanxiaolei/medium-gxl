package cn.hds.hvmall.service.promotion;

import cn.hds.hvmall.base.BaseExecuteResult;
import cn.hds.hvmall.entity.promotion.MeetingInfoVo;
import cn.hds.hvmall.entity.promotion.PromotionMeetingCountInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PromotionMeetingCountService {


    PromotionMeetingCountInfo selectMeetingInfo(String pCode);

    BaseExecuteResult<List<MeetingInfoVo>> selectMeeting(String json);
}
