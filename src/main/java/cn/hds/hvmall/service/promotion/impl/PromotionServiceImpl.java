package cn.hds.hvmall.service.promotion.impl;

import cn.hds.hvmall.base.PageBase;
import cn.hds.hvmall.base.RedisService;
import cn.hds.hvmall.config.PromotionConstants;
import cn.hds.hvmall.entity.piece.PromotionBaseInfo;
import cn.hds.hvmall.entity.piece.PromotionTempStock;
import cn.hds.hvmall.entity.promotion.ActivityExclusive;
import cn.hds.hvmall.entity.promotion.PromotionInviteCode;
import cn.hds.hvmall.entity.promotion.PromotionProduct;
import cn.hds.hvmall.entity.promotion.PromotionVo;
import cn.hds.hvmall.entity.whitecustomer.PromotionWhiteListVo;
import cn.hds.hvmall.mapper.invite.PromotionInviteCodeMapper;
import cn.hds.hvmall.mapper.list.ProductMapper;
import cn.hds.hvmall.mapper.piece.PromotionBaseInfoMapper;
import cn.hds.hvmall.mapper.piece.PromotionTempStockMapper;
import cn.hds.hvmall.mapper.promotion.*;
import cn.hds.hvmall.mapper.whitecustomer.PromotionWhiteListMapper;
import cn.hds.hvmall.pojo.promotion.Promotion;
import cn.hds.hvmall.service.invite.PromotionInviteCodeService;
import cn.hds.hvmall.service.promotion.PromotionService;
import cn.hds.hvmall.utils.cache.RedisLock;
import cn.hds.hvmall.utils.list.GlobalErrorCode;
import cn.hds.hvmall.utils.list.qiniu.BizException;
import cn.hds.hvmall.utils.piece.UUIDGenerator;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @ClassName PromotionServiceImpl
 * @Description 活动基本信息服务层
 * @Author lxl
 * @Date 2018/10/19 10:48
 * @Version 1.0
 **/
@Service
@Transactional
public class PromotionServiceImpl implements PromotionService{

    private static final Logger logger = LoggerFactory.getLogger(PromotionServiceImpl.class);

    @Autowired
    private PromotionBaseInfoMapper proBaseInfoMapper;
    @Autowired
    private PromotionMapper promotionMapper;

    @Autowired
    private PromotionConfigMapper promotionConfigMapper;

    @Autowired
    private PromotionSkuMapper promotionSkuMapper;

    @Autowired
    private PromotionSkuGiftMapper promotionSkuGiftMapper;

    @Autowired
    private PromotionTempStockMapper promotionTempStockMapper;

    @Autowired
    private ProductMapper productMapper;

    @Autowired
    private PromotionExclusiveMapper promotionExclusiveMapper;

    @Autowired
    private PromotionInviteCodeService promotionInviteCodeService;

    @Autowired
    private PromotionInviteCodeMapper promotionInviteCodeMapper;

    @Autowired
    private PromotionWhiteListMapper promotionWhiteListMapper;

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private RedisService redisService;

    @Override
    public boolean save(Promotion promotion) {
        if (promotion.getDiscount() == null) {
            // 新的活动配置不使用discount字段，使用price
            promotion.setDiscount(BigDecimal.ZERO);
        }
        return promotionMapper.insertHVPromotion(promotion) > 0;
    }

    @Override
    public Promotion load(Long id) {
        return promotionMapper.selectByPrimaryKey(id);
    }

    @Override
    public boolean update(Promotion promotion) {
        return promotionMapper.update(promotion) > 0;
    }

    @Override
    public String addPromotion(PromotionVo promotionVo) {
        //插入活动基本信息
        String pCode = UUIDGenerator.getUUID();
        promotionVo.setProjectCode(pCode);

        //活动类型 状态码
        String type = "meeting"+UUIDGenerator.getOrderIdByUUId();
        promotionVo.setType(type);
        promotionVo.setStatus(3);

        promotionVo.setCreateTime(new Date());
        promotionVo.setIsDeleted(1);

        //插入活动基本信息
        int num1 = promotionMapper.insert(promotionVo);

        if(num1 != 1){
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,"活动基本信息错误错误");
        }
        //插入活动配置信息
        List<Map<String,String>> configs = promotionVo.getConfigs();
        String pName = promotionVo.getPrmotionName();
        Map<String,String> map1 = new HashMap<>();
        map1.put("configName","meetingBannerTitle");
        map1.put("configValue",pName);
        Map<String,String> map2 = new HashMap<>();
        map2.put("configName","meetingListTitle");
        map2.put("configValue",pName);
        Map<String,String> map3 = new HashMap<>();
        map3.put("configName","SHOW_COUNT_DOWN");
        map3.put("configValue","0");
        Map<String,String> map4 = new HashMap<>();
        map4.put("configName","SOLD_OUT_TITLE");
        map4.put("configValue","即将到货");
        configs.add(map1);
        configs.add(map2);
        configs.add(map3);
        configs.add(map4);
        for (Map<String, String> config : configs) {
            //基本信息与配置关联
            config.put("configType",type);
        }
        int num2 = promotionConfigMapper.insert(configs);

        //插入活动商品信息
        List<Map<String,String>> products = promotionVo.getProducts();
        for (Map<String,String> product : products) {
            product.put("pCode",pCode);
            product.put("isDeleted","0");

            String gift = product.get("gift");

            //添加活动状态
            ActivityExclusive activityExclusive = new ActivityExclusive();
            activityExclusive.setStatus(1);
            activityExclusive.setProductId(Long.parseLong(product.get("productId")));
            activityExclusive.setProjectCode(pCode);
            activityExclusive.setCreatedAt(new Date());
            promotionExclusiveMapper.insertActivityProduct(activityExclusive);
            //插入商品信息
            int num3 = promotionSkuMapper.insert(product);
            if(!gift.equals("0")){
                //插入赠品信息
                int num4 = promotionSkuGiftMapper.insert(product);
            }
            //插入库存信息
            int num5 = promotionTempStockMapper.insertOne(product);

            if(!checkIsDbConfig()) {
                //插入redis库存
                String activitySkuAmountKey = PromotionConstants.getCommonAmountKey(pCode, product.get("skuCode"));
                redisService.set(activitySkuAmountKey, product.get("skuNum"));
            }
        }
        //promotion_whitelist表中加入一条数据用于是否启用白名单功能
        this.insert4WhiteByPcode(pCode);
        return "项目新增成功";
    }

    private void insert4WhiteByPcode(String pCode) {
        boolean b = promotionWhiteListMapper.checkPcodeExist(pCode);
        if(!b){
            PromotionWhiteListVo p = new PromotionWhiteListVo();
            p.setpCode(pCode);
            p.setStatus(1);//默认先开启白名单
            p.setCpId(9999999l);//7个9 为标记
            promotionWhiteListMapper.insert(p);
        }
    }

    @Override
    public Map getPromotion(String pCode, int pageNum, int pageSize) {
        Map param = new HashMap();
        param.put("pCode",pCode);
        //基本信息
        Map promotionBaseInfo = promotionMapper.getBaseInfo(param);
        if(promotionBaseInfo == null){
            throw new BizException(GlobalErrorCode.NOT_FOUND,"未找到该项目");
        }
        param.put("configType",promotionBaseInfo.get("p_type"));
        //配置信息
        List<Map> configs = promotionConfigMapper.getPromotionConfigs(param);
        //产品信息
        List<Map> products = promotionSkuMapper.getAllSku(param);
        promotionBaseInfo.put("configs",configs);
        promotionBaseInfo.put("products",products);

        return promotionBaseInfo;
    }

    @Override
    @Transactional
    public String editPromotion(PromotionVo promotionVo) {
        int num1 = promotionMapper.editPromotion(promotionVo);
        List<Map<String, String>> configs = promotionVo.getConfigs();
        String pName = promotionVo.getPrmotionName();
        Map<String,String> map1 = new HashMap<>();
        map1.put("configName","meetingBannerTitle");
        map1.put("configValue",pName);
        Map<String,String> map2 = new HashMap<>();
        map2.put("configName","meetingListTitle");
        map2.put("configValue",pName);
        configs.add(map1);
        configs.add(map2);
        for (Map<String, String> config : configs) {
            //基本信息与配置关联
            config.put("configType",promotionVo.getType());
        }
        for (int i = 0; i < configs.size(); i++) {
            promotionConfigMapper.update(configs.get(i));
        }

        List<Map<String,String>> products = promotionVo.getProducts();
        Map map = new HashMap();
        String projectCode = promotionVo.getProjectCode();
        if(projectCode == null){
            logger.error("大会修改失败,未传入项目编码");
            throw new BizException(GlobalErrorCode.UNKNOWN,"未传入项目编码");
        }
        map.put("projectCode",projectCode);
        promotionSkuMapper.delete(map);
        promotionSkuGiftMapper.delete(map);
        ActivityExclusive activityExclusive = new ActivityExclusive();
        activityExclusive.setProjectCode(projectCode);
        promotionExclusiveMapper.deleteProducts(activityExclusive);
        //插入活动商品信息

        for (Map<String,String> product : products) {
            product.put("pCode",projectCode);
            //product.put("isDeleted","1");
            String gift = product.get("gift");
            //插入商品信息
            int num3 = promotionSkuMapper.insert(product);
            //插入商品状态
            ActivityExclusive activityExclusive1 = new ActivityExclusive();
            activityExclusive1.setStatus(1);
            activityExclusive1.setProductId(Long.parseLong(product.get("productId")));
            activityExclusive1.setProjectCode(projectCode);
            activityExclusive1.setCreatedAt(new Date());
            promotionExclusiveMapper.insertActivityProduct(activityExclusive1);
            if(!gift.equals("0")){
                //插入赠品信息
                int num4 = promotionSkuGiftMapper.insert(product);
            }

            //更新库存信息
            updateTempStock(product,promotionVo.getStatus());
        }

        return "活动信息修改成功";
    }

    private  void updateTempStock(Map<String, String> product,int status) {
        Map<String, Object> temp = new HashMap<>();
        temp.put("p_code", product.get("pCode"));
        temp.put("sku_code", product.get("skuCode"));

        List<PromotionTempStock> tempStocks = promotionTempStockMapper.selectByMap(temp);
        int skuNum = Integer.parseInt(product.get("skuNum"));

        if(tempStocks.size()>0){
            for (PromotionTempStock stock : tempStocks) {
                if (skuNum > stock.getP_sku_num()) {
                    stock.setP_usable_sku_num(stock.getP_usable_sku_num() + skuNum - stock.getP_sku_num());
                } else if (skuNum < stock.getP_sku_num()) {
                    int usable = stock.getP_usable_sku_num() - (stock.getP_sku_num() - skuNum);
//                if(usable>=0){
                    stock.setP_usable_sku_num(stock.getP_usable_sku_num() - (stock.getP_sku_num() - skuNum));
//                }
//                throw new BizException(GlobalErrorCode.UNKNOWN,"若减小该商品库存，则商品实际使用库存小于0！");
//
                }
                stock.setRestriction(Integer.valueOf(product.get("restriction")));
                stock.setIs_deleted(stock.getIs_deleted());
                stock.setP_sku_num(skuNum);

                if(!checkIsDbConfig() && 3==status){
                    //更新redis库存
                    String activitySkuAmountKey = PromotionConstants.getCommonAmountKey(stock.getP_code(), stock.getSku_code());
                    redisService.set(activitySkuAmountKey,String.valueOf(stock.getP_sku_num()));
                }

                // 更新数据库
                promotionTempStockMapper.updateTempStock(stock);
            }
        }else {
            if(!checkIsDbConfig()){
                //插入redis库存
                String activitySkuAmountKey = PromotionConstants.getCommonAmountKey(product.get("pCode"), product.get("skuCode"));
                redisService.set(activitySkuAmountKey,product.get("skuNum"));
            }
            promotionTempStockMapper.insertOne(product);
        }
    }

    @Override
    public String editPromotionProduct(PromotionProduct product) {
        //修改买一赠一，是否橱窗显示
        int num = promotionSkuMapper.editPromotionProduct(product);

//        if(!checkIsDbConfig()){
//            //插入redis库存
//
//            String activitySkuAmountKey = PromotionConstants.getCommonAmountKey(product.getProjectCode(), product.getSkuCode());
//            redisService.set(activitySkuAmountKey,String.valueOf(product.getSkuNum()));
//        }
        //修改库存和限购数量
        int num1 = promotionTempStockMapper.editPromotionTempStock(product);
        int gift = product.getGift();
        int num3 = promotionSkuGiftMapper.deleteOne(product);
        if(gift == 1){
            Map map = new HashMap();
            map.put("pCode",product.getProjectCode());
            map.put("skuCode",product.getSkuCode());
            map.put("productId",product.getCode());
            promotionSkuGiftMapper.insert(map);
        }

        return "商品信息修改成功";
    }

    @Override
    public String deletePromotionProduct(String skuCode, String projectCode) {
        if(projectCode == null){
            logger.error("删除单个商品失败,未传入项目编码");
            throw new BizException(GlobalErrorCode.UNKNOWN,"活动编码为空");
        }
        Map map = new HashMap();
        map.put("skuCode",skuCode);
        map.put("projectCode",projectCode);
        if(!checkIsDbConfig()){
            //插入redis库存

            String activitySkuAmountKey = PromotionConstants.getCommonAmountKey(projectCode, skuCode);
            redisTemplate.delete(activitySkuAmountKey);
        }
        //删除基本信息
        promotionSkuMapper.delete(map);
        //删除赠品
        promotionSkuGiftMapper.delete(map);

        //删除库存
        promotionTempStockMapper.deleteStock(map);
        return "商品信息删除成功";
    }

    @Override
    public PageInfo getAllProducts(String projectCode, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        List<Map> list=promotionSkuGiftMapper.getAllProducts(projectCode);
        PageInfo pageInfo = new PageInfo(list);
        return pageInfo;
    }

    @Override
    public PageInfo getPromotionProduct(PromotionProduct page) {
        PageHelper.startPage(page.getPageNum(),page.getPageSize());
        List<Map> products = productMapper.getPromotionProduct(page);
        PageInfo pageInfo = new PageInfo(products);
        return pageInfo;
    }

    /**
     * 活动配置表分页查询活动信息
     * @param pageNum
     * @param pageSize
     * @return
     */
    @Override
    public PageBase<PromotionBaseInfo> queryPromotionBaseInfoList(String pName,String pStatus,String productName,String skuCode,int pageNum, int pageSize) {
        PageBase<PromotionBaseInfo> promotionBaseInfoList = new PageBase<>(pageSize,0,pageNum);
        int offSet= (pageNum-1)*pageSize;
        List<PromotionBaseInfo> promotionBaseInfos = proBaseInfoMapper.queryPromotionBaseInfoList(pName,pStatus,productName,skuCode,offSet, pageSize);
        int baseInfoCount = proBaseInfoMapper.queryPromotionBaseInfoCount(pName,pStatus,productName,skuCode);
        for (PromotionBaseInfo promotionBaseInfo : promotionBaseInfos) {
            String type = promotionBaseInfo.getpType();
            String pCode = promotionBaseInfo.getpCode();
            Map map = new HashMap();
            map.put("configType",type);
            List<Map> lists = promotionConfigMapper.getPromotionConfigs(map);
            for (int i = 0; i < lists.size(); i++) {
                String configName = lists.get(i).get("config_name").toString();
                String configValue = lists.get(i).get("config_value").toString();
                switch (configName){
                    case "isCode":
                        promotionBaseInfo.setIsCode(configValue);
                        if(configValue.equals("on")){

                            int amount = promotionInviteCodeMapper.getPromotionInviteCodeNum(pCode);
                            if(amount>0){
                                promotionBaseInfo.setInitCode("true");
                            }else {
                                promotionBaseInfo.setInitCode("false");
                            }
                        }else {
                            promotionBaseInfo.setInitCode("false");
                        }
                        break;
                    case "top_value":
                        if(null == configValue || configValue.equals("")){
                            configValue = "不限额度";
                        }
                        promotionBaseInfo.setTop_value(configValue);
                        break;
                    case "isWMS":
                        promotionBaseInfo.setIsWMS(configValue);
                        break;
                    case "showType":
                        promotionBaseInfo.setShowType(configValue);
                        break;
                    default:
                        break;
                }
            }

            //大会是否开启白名单的状态
            this.addPromotionWhiteStatus(promotionBaseInfo);
        }
        promotionBaseInfoList.setResultList(promotionBaseInfos);
        promotionBaseInfoList.setTotalCount(baseInfoCount);
        return promotionBaseInfoList;
    }

    private void addPromotionWhiteStatus(PromotionBaseInfo promotionBaseInfo){
        //加大会是否开启白名单的状态,魔法值七个9即cpId
        PromotionWhiteListVo p = new PromotionWhiteListVo();
        p.setCpId(9999999L);
        p.setpCode(promotionBaseInfo.getpCode());
        List<PromotionWhiteListVo> pwVOList = this.promotionWhiteListMapper.selectByCpIdAndPcode(p);
        if(pwVOList != null && !pwVOList.isEmpty()){
            promotionBaseInfo.setpCodeStatus(pwVOList.get(0).getStatus());
        }else {
            promotionBaseInfo.setpCodeStatus(0);
        }
    }
    /**
     * 根据pCode更新活动状态
     * @param pCode
     * @param pStatus
     * @return
     */
    @Override
    public Boolean updatePromotionStatusByPcode(String pCode, String pStatus) {
        int result = proBaseInfoMapper.updatePStatus(pCode, pStatus);
        //更新活动商品状态
        ActivityExclusive activityExclusive = new ActivityExclusive();
        activityExclusive.setProjectCode(pCode);
        activityExclusive.setProductId(null);
        activityExclusive.setCreatedAt(new Date());
        switch (pStatus){
            case "5":
                //活动上线
                activityExclusive.setStatus(1);
                break;
            case "6":
                //活动下线
                activityExclusive.setStatus(0);
                // 活动商品失效
                promotionTempStockMapper.disablePromotionTempStock(pCode);
                break;
            default:
                break;
        }
        promotionExclusiveMapper.updateActivityExclusive(activityExclusive);

        if(result>0){
            return true;
        }
        return false;
    }

    @Override
    @Transactional
    public boolean DetelPromotion(String pCode) {
            boolean a=false;
        try {
            int deletedPromotion=proBaseInfoMapper.DetelPromotion(pCode);
            int deletedSkus=proBaseInfoMapper.DetelSkus(pCode);
            int deletedTempStock=proBaseInfoMapper.DetelTempStock(pCode);
            if(deletedPromotion>0 && deletedSkus>0 && deletedTempStock>0){
                a=true;
                return a;
            }

        }catch (Exception e){
            e.printStackTrace();
            // 针对多条数据操作需要手动开启事务
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        }
        return a;
    }

    @Override
    public int initInviteCode(String pCode, int codeNum) {
        int num = promotionInviteCodeService.insertInviteCode(pCode,codeNum);
        return num;
    }

    @Override
    public PageBase<PromotionInviteCode> selectAllBypCode(String pCode, String cpid, String code, int pageNo, int pageSize) {
        int count=promotionInviteCodeMapper.selectAllBypCodeCount(pCode,cpid,code);

        PageBase<PromotionInviteCode> pageBase=new PageBase<>(pageSize,count,pageNo);
        int totalPage = count % pageSize == 0 ? count
                / pageSize : count / pageSize + 1;
        pageBase.setTotalPages(totalPage);
        int offSet= (pageNo-1)*pageSize;
        pageBase.setOffSet(offSet);
        List<PromotionInviteCode> promotionInviteCodeList=promotionInviteCodeMapper.selectAllBypCode(pCode,cpid,code,offSet,pageSize);

        pageBase.setResultList(promotionInviteCodeList);
        pageBase.setTotalCount(count);


        return pageBase;
    }

    /**
     * 功能描述: 追加大會活動庫存
     * @author Luxiaoling
     * @date 2019/1/16 17:33
     * @param  pCode, skuCode, addStock
     * @return  java.lang.String
     */
    @Override
    @Transactional
    public String addMeetingStock(String pCode, String skuCode, Integer addStock) {

        //更新db库存
        PromotionProduct product = new PromotionProduct();
        product.setProjectCode(pCode);
        product.setSkuCode(skuCode);
        product.setAddStock(addStock);
        Map<String, Object> temp = new HashMap<>();
        temp.put("p_code", pCode);
        temp.put("sku_code", skuCode);
        List<PromotionTempStock> tempStocks = promotionTempStockMapper.selectByMap(temp);
        if(tempStocks.size()>0 && (tempStocks.get(0).getP_usable_sku_num()+addStock<0)){
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "库存不足");

        }
        int updateStock = promotionTempStockMapper.addPromotionTempStock(product);

        if(!checkIsDbConfig()){
            //更新redis库存
            String activitySkuAmountKey = PromotionConstants.getCommonAmountKey(pCode, skuCode);
            if (null==redisService.get(activitySkuAmountKey)) {
                throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "库存不足");
            }
            int beforeAmount = Integer.parseInt(redisService.get(activitySkuAmountKey));
            logger.debug( skuCode+" 库存追加前redis库存剩余："+beforeAmount);
            if(beforeAmount+addStock<0){
                throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "库存不足");

            }
            RedisLock lock =
                    new RedisLock(redisTemplate, PromotionConstants.DISTRIBUTED_LOCKER, 10000, 20000);
            try {
                if (lock.lock()) {
                    // 加用户购买数量, 减总库存数量
                    redisService.increment(activitySkuAmountKey, addStock);

                }
            } catch (InterruptedException e) {
                throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "库存修改失败", e);
            } finally {
                lock.unlock();
            }
            logger.debug( skuCode+" redis库存剩余："+redisService.get(activitySkuAmountKey));

        }

        return "活动库存追加成功";
    }

    /**
     * 功能描述: 大会活动单品上下架
     * @param pCode
     * @param skuCode
     * @return
     */
    @Override
    @Transactional
    public String meetingProductGrounding(String pCode, String skuCode,String groundType) {
        Map map = new HashMap();
        map.put("skuCode",skuCode);
        map.put("pCode",pCode);
        map.put("groundType",groundType);
        //如果是下架且是redis配置需要将redis的可用库存同步到db
        if("0".equals(groundType) && !checkIsDbConfig()){
            String activitySkuAmountKey = PromotionConstants.getCommonAmountKey(pCode, skuCode);
            PromotionTempStock stock = new PromotionTempStock();
            stock.setP_code(pCode);
            stock.setSku_code(skuCode);

            int currentAmount = Integer.parseInt(redisService.get(activitySkuAmountKey));
            stock.setP_usable_sku_num(currentAmount);
            promotionTempStockMapper.updateTempStock(stock);
        }
        promotionSkuMapper.meetingProductGrounding(map);
        if("0".equals(groundType)){
            return "活动单品下架成功";
        }else{
            return "活动单品上架成功";
        }
    }

    //检查活动库存配置是否为DB，不是db则为redis
    Boolean checkIsDbConfig(){
        Map param = new HashMap();
        param.put("configName","lock_type");
        //数据库配置信息
        Map dbConfig = promotionConfigMapper.getPromotionConfigByName(param);
        String lock_type = "DB";
        if (null != dbConfig) {
            lock_type = (String) dbConfig.get("config_value");
        }
        if("DB".equals(lock_type)){
            return true;
        }else{
            return false;
        }
    }

    @Override
    public Promotion loadBySkuId(Long productId, Long skuId) {
        return loadBySkuId(productId, skuId, null);
    }

    @Override
    public Promotion loadBySkuId(Long productId, Long skuId, Date date) {
        return promotionMapper.selectByProductId(productId, skuId, date);
    }

    @Override
    public boolean close(Long id) {
        return promotionMapper.close(id) > 0;
    }

    @Override
    public boolean deletedFlashSale(Long id) {
        return promotionMapper.deletedFlashSale(id)>0;
    }

    @Override
    public boolean unClose(Long id) {
        return promotionMapper.unClose(id) > 0;
    }
}
