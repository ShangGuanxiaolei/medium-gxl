package cn.hds.hvmall.service.lottery;

import cn.hds.hvmall.controller.lottery.LotteryPromotionForm;
import cn.hds.hvmall.entity.lottery.ActivityForm;
import cn.hds.hvmall.entity.lottery.LotteryResult;

import java.util.Map;

public interface LotteryService {

	/**
	 * 新增抽奖
	 * @param form
	 * @return
	 */
	boolean save(LotteryPromotionForm form);

	/**
	 * 删除抽奖
	 */
	boolean deletePromotion(Long id);

	/**
	 * 根据活动id查询活动详情
	 */
	LotteryResult getDetailMessage(Long id);

	/**
	 * 活动清单
	 */
	Map<String,Object> getActivityList(ActivityForm activityForm);

	/**
	 * 结束活动状态
	 */
	boolean endActivityStatus(Long promotionId);

	/**
	 * 更新抽奖活动
	 */
	boolean updatePromotion(LotteryPromotionForm form);
}
