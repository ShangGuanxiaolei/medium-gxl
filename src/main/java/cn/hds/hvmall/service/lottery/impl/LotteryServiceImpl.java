package cn.hds.hvmall.service.lottery.impl;

import cn.hds.hvmall.controller.lottery.LotteryPromotionForm;
import cn.hds.hvmall.entity.lottery.*;
import cn.hds.hvmall.entity.rain.PromotionPacketListVO;
import cn.hds.hvmall.entity.rain.RainTimResult;
import cn.hds.hvmall.mapper.LotteryProbabilityMapper;
import cn.hds.hvmall.mapper.PromotionLotteryMapper;
import cn.hds.hvmall.mapper.grandsale.GrandSaleModuleMapper;
import cn.hds.hvmall.mapper.rain.PromotionListMapper;
import cn.hds.hvmall.mapper.stockCheck.PromotionStockCheckMapper;
import cn.hds.hvmall.pojo.lottery.PromotionLotteryVO;
import cn.hds.hvmall.service.grandsale.GrandSaleModuleService;
import cn.hds.hvmall.service.lottery.LotteryService;
import cn.hds.hvmall.service.stockcheck.PromotionStockCheckService;
import cn.hds.hvmall.type.PromotionType;
import cn.hds.hvmall.utils.list.GlobalErrorCode;
import cn.hds.hvmall.utils.list.qiniu.BizException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class LotteryServiceImpl implements LotteryService {

	private final PromotionListMapper promotionListMapper;

	private final PromotionLotteryMapper promotionLotteryMapper;

	private final LotteryProbabilityMapper lotteryProbabilityMapper;

	private final GrandSaleModuleService grandSaleModuleService;

	private final GrandSaleModuleMapper grandSaleModuleMapper;

	private final PromotionStockCheckService promotionStockCheckService;

	private final PromotionStockCheckMapper promotionStockCheckMapper;

	@Autowired
	public LotteryServiceImpl(
			PromotionListMapper promotionListMapper,
			PromotionLotteryMapper promotionLotteryMapper,
			LotteryProbabilityMapper lotteryProbabilityMapper,
			GrandSaleModuleService grandSaleModuleService,
			GrandSaleModuleMapper grandSaleModuleMapper,
			PromotionStockCheckService promotionStockCheckService,
			PromotionStockCheckMapper promotionStockCheckMapper
	) {
		this.promotionListMapper = promotionListMapper;
		this.promotionLotteryMapper = promotionLotteryMapper;
		this.lotteryProbabilityMapper = lotteryProbabilityMapper;
		this.grandSaleModuleService = grandSaleModuleService;
		this.grandSaleModuleMapper = grandSaleModuleMapper;
		this.promotionStockCheckService=promotionStockCheckService;
		this.promotionStockCheckMapper=promotionStockCheckMapper;
	}

	@Override
	@Transactional
	public boolean save(LotteryPromotionForm form) {
		if(form == null){
			throw new BizException(GlobalErrorCode.INTERNAL_ERROR,"活动信息不存在，添加活动信息错误");
		}
		// 查询抽奖所有场次时间
		List<RainTimResult> timeList = promotionListMapper.getUnClosedTimeList(1);
		if(timeList.size() != 0){
			for (RainTimResult timResult : timeList) {
				if((form.getTriggerStartTime().getTime() <= timResult.getTriggerEndTime().getTime() &&
								form.getTriggerStartTime().getTime()  >= timResult.getTriggerStartTime().getTime()) ||
								(form.getTriggerEndTime().getTime() <= timResult.getTriggerEndTime().getTime() &&
												form.getTriggerEndTime().getTime()  >= timResult.getTriggerStartTime().getTime())	){
					throw new BizException(GlobalErrorCode.INTERNAL_ERROR,"添加抽奖活动时间不能交叉");
				}
			}
		}
		if(promotionListMapper.insertRain(form) <= 0){
			throw new BizException(GlobalErrorCode.INTERNAL_ERROR,"添加活动基本信息失败");
		}
		Long promotionId = form.getId();
		//　添加抽奖当天活动的信息
		List<PromotionLotteryVO> lotteryVOList = form.getLotteryVOList();
		for (PromotionLotteryVO lottery : lotteryVOList) {
			lottery.setPromotionListId(promotionId);
			if(promotionLotteryMapper.insert(lottery) <= 0){
				throw new BizException(GlobalErrorCode.INTERNAL_ERROR,"添加当天活动配置失败");
			}
			Long promotionLotteryId = lottery.getId();
			List<LotteryProbability> probabilities = lottery.getProbabilities();
			for (LotteryProbability lotteryProbability : probabilities) {
				lotteryProbability.setPromotionLotteryId(promotionLotteryId);
				if(lotteryProbability.getSkuId()!=null) {
					int skuId = Integer.parseInt(lotteryProbability.getSkuId());
					//校验库存
					promotionStockCheckService.stockCheck(lotteryProbability.getAmount(),skuId,null,"",null);
				}

				if(lotteryProbabilityMapper.insert(lotteryProbability) <= 0){
					throw new BizException(GlobalErrorCode.INTERNAL_ERROR,"添加配置活动概率失败");
				}

			}
		}

		grandSaleModuleService.bindGrand(PromotionType.LOTTERY_DRAW,promotionId);

		return true;
	}

	@Override
	@Transactional
	public boolean deletePromotion(Long id) {
		if(id <= 0){
			throw new BizException(GlobalErrorCode.INTERNAL_ERROR,"活动id不存在");
		}
		// 根据活动id查询出抽奖id
		List<Long> lotteryList = promotionListMapper.selectPromotionLotteryId(id);
		for (Long lotteryId : lotteryList) {
			List<Long> probabilityIdList = lotteryProbabilityMapper.selectProbabilityId(lotteryId);
			if(lotteryId == null || probabilityIdList.size() == 0){
				throw new BizException(GlobalErrorCode.INTERNAL_ERROR,"查询的抽奖相关的id不存在");
			}
			for (Long probabilityId : probabilityIdList) {
				if(lotteryProbabilityMapper.deletePromotionLotteryProbability(probabilityId) <= 0){
					throw new BizException(GlobalErrorCode.INTERNAL_ERROR,"删除抽奖概率失败");
				}
			}
		}
			if(promotionListMapper.deletePromotionListById(id) <= 0 ||
							promotionLotteryMapper.deletePromotionLottery(id) <= 0){
				throw new BizException(GlobalErrorCode.INTERNAL_ERROR,"删除抽奖活动失败");
			}
		return true;
	}

	@Override
	public LotteryResult getDetailMessage(Long id) {
		LotteryResult lotteryResult = new LotteryResult();
		if(id <= 0){
			throw new BizException(GlobalErrorCode.INTERNAL_ERROR,"活动id不存在");
		}
		PromotionPacketListVO packetListVO = promotionListMapper.findById(id);
		// 查询当天活动配置信息
		List<PromotionLottery> promotionLotteries = promotionLotteryMapper.selectLottery(id);
		if(packetListVO == null || promotionLotteries.size() == 0){
			throw new BizException(GlobalErrorCode.INTERNAL_ERROR,"活动不存在");
		}
		List<PromotionLottery> promotionLotteryList = new ArrayList<>();
		for (PromotionLottery lottery : promotionLotteries) {
			Long lotteryId = lottery.getLotteryId();
			List<LotteryProbability> lotteryProbabilities = lotteryProbabilityMapper.selectByLotteryId(lotteryId);
			lottery.setLotteryProbabilityList(lotteryProbabilities);
			promotionLotteryList.add(lottery);
		}
		packetListVO.setPromotionLottery(promotionLotteryList);
		lotteryResult.setPromotionPacketListVO(packetListVO);
		return lotteryResult;
	}

	@Override
	public Map<String,Object> getActivityList(ActivityForm activityForm) {
		int pageIndex = activityForm.getPageIndex();
		int pageSize = activityForm.getPageSize();
		int countIndex = (pageIndex - 1) * pageSize;
		Map<String,Object> resultMap = new HashMap<>();
		List<ActivityList> list = promotionListMapper.getActivityList(activityForm,countIndex,pageSize);
		Integer activityCount = promotionListMapper.getActivityCount(activityForm);
		resultMap.put("activityList",list);
		resultMap.put("activityCount",activityCount);
		return resultMap;
	}

	@Override
	public boolean endActivityStatus(Long promotionId) {
		if(promotionId <= 0){
			throw new BizException(GlobalErrorCode.INTERNAL_ERROR,"活动id不存在");
		}
		int result = promotionListMapper.updateActivityStatus(promotionId);
		grandSaleModuleMapper.updateStatusById(String.valueOf(promotionId),3);
		if(result <= 0){
			throw new BizException(GlobalErrorCode.INTERNAL_ERROR,"修改活动状态失败");
		}
		return true;
	}

	@Override
	@Transactional
	public boolean updatePromotion(LotteryPromotionForm form) {
		if(form == null || form.getId() == null){
			throw new BizException(GlobalErrorCode.INTERNAL_ERROR,"活动id不能为空");
		}
		List<PromotionLotteryVO> lotteryVOList = form.getLotteryVOList();
		for (PromotionLotteryVO lottery : lotteryVOList) {
			// 更新
			if(promotionListMapper.updatePromotionList(form) <= 0 ||
							promotionLotteryMapper.updateByPrimaryKeySelective(lottery)<=0){
				throw new BizException(GlobalErrorCode.INTERNAL_ERROR,"更新抽奖活动异常");
			}
			List<LotteryProbability> probabilities = lottery.getProbabilities();
			for (LotteryProbability probability : probabilities) {
				int skuId=Integer.parseInt(probability.getSkuId());
				//校验库存
				promotionStockCheckService.stockCheck(probability.getAmount(),skuId,probability.getProbabilityId().toString(),"LOTTER",null);
				if(lotteryProbabilityMapper.updateByPrimaryKeySelective(probability) <= 0){
					throw new BizException(GlobalErrorCode.INTERNAL_ERROR,"更新活动异常");
				}
			}
		}
		return true;
	}
}
