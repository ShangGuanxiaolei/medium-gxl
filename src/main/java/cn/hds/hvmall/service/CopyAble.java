package cn.hds.hvmall.service;

import cn.hds.hvmall.entity.BaseEntityImpl;
import cn.hds.hvmall.utils.Transformer;
import cn.hds.hvmall.utils.list.GlobalErrorCode;
import cn.hds.hvmall.utils.list.qiniu.BizException;
import com.google.common.base.Preconditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * @author wangxinhua
 * @version 1.0.0 $ 2019/3/28
 */
public interface CopyAble<T extends BaseEntityImpl> {

    Logger LOGGER = LoggerFactory.getLogger(CopyAble.class);

    /**
     * 复制基础实体
     * 使用默认的 {@link CopyAble#save(BaseEntityImpl)} save保存
     *
     * @param source   源实体
     * @param clazz    T的字节码类型, 用来执行反射复制
     * @param modifier 属性修改方法
     * @return 复制后的实体
     */
    default T copy(T source, Class<T> clazz, Consumer<T> modifier) {
        return copy(source, clazz, modifier, this::save);
    }

    /**
     * 复制基础实体
     * 同时执行保存操作
     *
     * @param source   源实体
     * @param clazz    T的字节码类型, 用来执行反射复制
     * @param modifier 属性修改方法
     * @param saver    保存方法
     * @return 复制后的实体
     */
    default T copy(T source, Class<T> clazz, Consumer<T> modifier, Function<T, Boolean> saver) {
        Preconditions.checkNotNull(source, "源对象不能为空");
        // 清空掉id，需要保存后重新子增
        final T dest = Transformer.fromBean(source, clazz);
        if (modifier != null) {
            // 在此处修改自定义的属性
            modifier.accept(dest);
            dest.setId(null);
            dest.setCreatedAt(new Date());
            dest.setUpdatedAt(new Date());
        }
        if (saver == null) {
            LOGGER.warn("保存实体方法未定义, 不执行保存方法");
            return dest;
        }
        final Boolean ret = saver.apply(dest);
        if (!ret) {
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "数据复制失败");
        }
        LOGGER.info("对象复制成功 {} -> {}", source, dest);
        return dest;
    }

    /**
     * 保存属性, 目标service必须实现改方法
     * @param t 待保存对象
     * @return 保存结果
     */
    boolean save(T t);

}
