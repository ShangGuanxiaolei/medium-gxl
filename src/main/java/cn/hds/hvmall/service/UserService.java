package cn.hds.hvmall.service;

import com.baomidou.mybatisplus.service.IService;

import cn.hds.hvmall.base.BaseExecuteResult;
import cn.hds.hvmall.entity.User;

/**
 * 用户服务层
 * @类名: UserService.java
 * @描述: TODO .
 * @程序猿: Jrain Chen
 * @日期: 2018年8月10日
 * @版本号: V1.0 .
 */
public interface UserService extends IService<User>{
	/**
	 * 根据手机号查询用户信息
	 * @Description: 
	 * @Title: queryUserByPhone
	 * @author: Jrain Chen
	 * @date: 2018年8月15日
	 * @param json
	 * @return
	 */
	 BaseExecuteResult<Object> queryUserByPhone(String json);

}
