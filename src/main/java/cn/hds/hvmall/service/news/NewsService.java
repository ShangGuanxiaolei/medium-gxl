package cn.hds.hvmall.service.news;

import cn.hds.hvmall.base.PageBase;
import cn.hds.hvmall.entity.news.NewsDetail;
import cn.hds.hvmall.entity.news.NewsInfo;
import cn.hds.hvmall.entity.news.NewsType;
import cn.hds.hvmall.pojo.list.ExecuteResult;

import java.util.List;
import java.util.Map;

public interface NewsService {

    /**
     * 查询新闻类型
     * @return
     */
    List<NewsType> selectNewsType();

    /**
     * 新增新闻类型
     * @param typeName
     * @return
     */
    boolean insertNewsType(String typeName);

    /**
     * 查询文章列表
     * @return
     */
    PageBase<NewsDetail> selectNewsDetailList(String typeId,String status,Integer pageNo,Integer pageSize );

    /**
     * 修改单个文章的状态
     * @return
     */
    boolean updateNewsStatus(Integer status,Integer newsId);

    /**
     * 新增文章详情
     * @return
     */
    boolean insertNewsDetail(Integer typeId,Integer status,String title,String img,String showTime,String mediaSources,String content,String releaseUser,Integer newsId);

    /**
     * 查询新闻具体信息
     * @return
     */
    NewsDetail selectNewsDetail(Integer detailId);

    /**
     * 根据typeId查询新闻列表
     * @param typeId
     * @return
     */
    List<NewsInfo> selectNewsListByType();
}
