package cn.hds.hvmall.service.news.impl;

import cn.hds.hvmall.base.PageBase;
import cn.hds.hvmall.entity.news.NewsDetail;
import cn.hds.hvmall.entity.news.NewsInfo;
import cn.hds.hvmall.entity.news.NewsType;
import cn.hds.hvmall.mapper.news.NewsMapper;
import cn.hds.hvmall.service.news.NewsService;
import cn.hds.hvmall.utils.list.GlobalErrorCode;
import cn.hds.hvmall.utils.list.qiniu.BizException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service("newsService")
public class NewsServiceImpl implements NewsService {

    @Autowired
    private NewsMapper newsMapper;

    @Override
    public List<NewsType> selectNewsType() {
        return newsMapper.selectNewsType();
    }

    @Override
    public boolean insertNewsType(String typeName) {
        return newsMapper.insertNewsType(typeName);
    }

    @Override
    public PageBase<NewsDetail> selectNewsDetailList(String  typeId,String  status,Integer pageNo,Integer pageSize) {

        int offSet= (pageNo-1)*pageSize;
        PageBase<NewsDetail> pageBase=new PageBase<>(pageSize,0,pageNo);

        int count=newsMapper.selectNewsDetailCount(typeId,status);
        int totalPage = count % pageSize == 0 ? count / pageSize : count / pageSize + 1;
        List<NewsDetail> newsDetails= newsMapper.selectNewsDetailList(typeId,status,offSet,pageSize);

        pageBase.setTotalPages(totalPage);
        pageBase.setPageNum(offSet);
        pageBase.setTotalCount(count);
        pageBase.setPageSize(pageSize);
        pageBase.setResultList(newsDetails);

        return pageBase;
    }

    @Override
    public boolean updateNewsStatus(Integer status,Integer newsId) {
        return newsMapper.updateNewsStatus(status,newsId);
    }

    @Override
    public boolean insertNewsDetail(Integer typeId,Integer status,String title,String img,String showTime,String mediaSources,String content,String releaseUser,Integer newsId) {


        if(null==typeId || "".equals(typeId)){
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "文章分类不能为空");
        }

        if (null==title || "".equals(title)){
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "文章标题不能为空");
        }

        if (null==content || "".equals(content)){
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "文章内容不能为空");
        }

        if(null!=newsId && !"".equals(newsId)){
            return newsMapper.updateNewsDetail(typeId,title,img,status,showTime,mediaSources,content,newsId);
        }

        return newsMapper.insertNewsDetail(typeId,title,img,status,showTime,mediaSources,content,releaseUser);
    }

    @Override
    public NewsDetail selectNewsDetail(Integer detailId) {
        NewsDetail newsDetail=newsMapper.selectNewsDetail(detailId);
        return newsDetail;
    }

    @Override
    public List<NewsInfo> selectNewsListByType() {
        List<NewsInfo> newsInfoList=new ArrayList<>();
        List<NewsType> newsTypeList=newsMapper.selectNewsType();
        for (int i = 0; i < newsTypeList.size(); i++) {
            NewsInfo newsInfo=new NewsInfo();
            List<NewsDetail> newsDetailList=newsMapper.selectNewsListByType(newsTypeList.get(i).getId());
            if(newsDetailList.size()>0) {
                newsInfo.setType(newsTypeList.get(i).getTypeName());
                newsInfo.setDetailList(newsDetailList);
                newsInfoList.add(newsInfo);
            }
        }
        return newsInfoList;
    }
}
