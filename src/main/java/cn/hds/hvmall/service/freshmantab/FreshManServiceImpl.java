package cn.hds.hvmall.service.freshmantab;

import cn.hds.hvmall.mapper.freshmantab.*;
import cn.hds.hvmall.pojo.freshmantab.*;
import cn.hds.hvmall.service.freshman.FreshmanModuleService;
import cn.hds.hvmall.service.freshman.impl.FreshmanBannerServiceImpl;
import cn.hds.hvmall.service.stockcheck.PromotionStockCheckService;
import cn.hds.hvmall.utils.list.GlobalErrorCode;
import cn.hds.hvmall.utils.list.qiniu.BizException;
import cn.hds.hvmall.utils.list.qiniu.IdTypeHandler;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static java.time.OffsetDateTime.now;

/**
 * Created with IntelliJ IDEA.
 * User: WangXiao
 * Date: 2019/3/5
 * Time: 16:31
 * Description:
 */
@Service
public class FreshManServiceImpl {

    private static final Logger logger = LoggerFactory.getLogger(FreshmanBannerServiceImpl.class);

    @Autowired
    private FreshmanMapper freshmanMapper;

    @Autowired
    private FreshManServiceImpl freshManService;

    @Autowired
    private FreshManXquarkProductCopyMapper xquarkProductCopyMapper;

    @Autowired
    private FreshManPromotionExclusiveCopyMapper promotionExclusiveCopyMapper;

    @Autowired
    private FreshManXquarkSkuCopyMapper freshManXquarkSkuCopyMapper;

    @Autowired
    private FreshManPromotionSkusCopyMapper promotionSkusCopyMapper;

    @Autowired
    private FreshmanModuleService freshmanModuleService;

    @Autowired
    private PromotionStockCheckService promotionStockCheckService;


    public Integer UpdateFreshManConfig(String value,Integer id){
        int i = freshmanMapper.updateFreshConfig(value, id);
        return i;
    }

    public Integer updateFreshAreattab(String name,Integer tabid){
        int i = freshmanMapper.updateFreshAreattab(name, tabid);
        return i;
    }
    public Integer updateFreshProduct(FreshmanProduct product){
        int i = freshmanMapper.updateFreshProduct(product);
        return i;
    }

    public List<FreshmanAreaProduct> queryTabProduct(){
        List<FreshmanAreaProduct> freshmanAreaProducts = freshmanMapper.queryTabProduct();
        return freshmanAreaProducts;
    }

    public List<Map<String,String>> queryTabtitle(){
        List<Map<String, String>> maps = freshmanMapper.queryTitle();
        return maps;
    }

    public Integer deleteTabProduct(String productId,Long tabId){
        int i = freshmanMapper.deleteFreshProduct(productId,tabId);
        return i;
    }

    public Integer AddTabProduct (FreshmanProduct freshmanProduct){
        int i = freshmanMapper.AddTabProduct(freshmanProduct);
        return i;
    }
    public Integer insertFreshProduct (FreshmanProduct freshmanProduct){
        int i = freshmanMapper.insertFreshProduct(freshmanProduct);
        return i;
    }
    public Integer insertPromotionExclusive (FreshmanProduct freshmanProduct){
        int i = freshmanMapper.insertPromotionExclusive(freshmanProduct);
        return i;
    }

    public Integer deletePromotionExclusive (String productId){
        int i = freshmanMapper.deletePromotionExclusive(productId);
        return i;
    }

    @Transactional(rollbackFor = BizException.class)
    public void inertOrUpdateProduct(Map map){
        //更新专区标题表
        System.out.println(map);
        String titele = (String)map.get("title");
        String subtitle = (String)map.get("subtitle");
        String buylimit = (String)map.get("buylimit");
        String postfree = (String)map.get("postfree");
        String moduleId=map.get("moduleId").toString();
        String userName=map.get("userName").toString();
        if(null!=titele){
            freshManService.UpdateFreshManConfig(titele,2);
        }
        if(null!=subtitle){
            freshManService.UpdateFreshManConfig(subtitle,3);
        }
        if(null!=buylimit){
            freshManService.UpdateFreshManConfig(buylimit,1);
        }
        if(null!=postfree){
            freshManService.UpdateFreshManConfig(postfree,4);
        }
        //更新tab表
        //更新添加商品商品
        Map<String,Object> tabname1 =(Map<String,Object>) map.get("tabname1");
        if(null!=tabname1){
            String name1 = (String)tabname1.get("name");
            List<FreshmanProduct> tabnameProduct1 = JSONArray.parseArray(JSON.toJSONString(tabname1.get("freshmanProducts")),FreshmanProduct.class);
            freshManService.updateFreshAreattab(name1,0);
            if(null!=tabnameProduct1){
                for (FreshmanProduct freshmanProduct1 : tabnameProduct1) {
                    addTable(freshmanProduct1,0L,moduleId,userName);
                }
            }
        }


        Map<String,Object> tabname2 =(Map<String,Object>) map.get("tabname2");
        if(null!=tabname2){
            String name2 = (String)tabname2.get("name");
            List<FreshmanProduct> tabnameProduct2 = JSONArray.parseArray(JSON.toJSONString(tabname2.get("freshmanProducts")),FreshmanProduct.class);
            freshManService.updateFreshAreattab(name2,1);
            if(null!=tabnameProduct2){
                for (FreshmanProduct freshmanProduct2 : tabnameProduct2) {
                    addTable(freshmanProduct2,1L,moduleId,userName);
                }
            }
        }


        Map<String,Object> tabname3 =(Map<String,Object>) map.get("tabname3");
        if(null!=tabname3){
            String name3 = (String)tabname3.get("name");
            List<FreshmanProduct> tabnameProduct3 = JSONArray.parseArray(JSON.toJSONString(tabname3.get("freshmanProducts")),FreshmanProduct.class);
            freshManService.updateFreshAreattab(name3,2);
            if(null!=tabnameProduct3){
                for (FreshmanProduct freshmanProduct3 : tabnameProduct3) {
                    addTable(freshmanProduct3,2L,moduleId,userName);
                }
            }
        }


        Map<String,Object> tabname4 =(Map<String,Object>) map.get("tabname4");
        if(null!=tabname4){
            String name4 = (String)tabname4.get("name");
            List<FreshmanProduct> tabnameProduct4 = JSONArray.parseArray(JSON.toJSONString(tabname4.get("freshmanProducts")),FreshmanProduct.class);
            freshManService.updateFreshAreattab(name4,3);
            if(null!=tabnameProduct4){
                for (FreshmanProduct freshmanProduct4 : tabnameProduct4) {
                    addTable(freshmanProduct4,3L,moduleId,userName);
                }
            }
        }
    }
    public  void  addTable(FreshmanProduct freshmanProduct1,Long tabId,String moduleId,String userName ){
        List<FreshmanProductSku> freshmanProductSkus1 = freshmanProduct1.getFreshmanProductSkus();
        for (FreshmanProductSku freshmanProductSku : freshmanProductSkus1) {
            if(freshmanProductSku.getStatus()==0&&(null==freshmanProduct1.getFpSkuCode()||"".equals(freshmanProduct1.getFpSkuCode()))) {
                freshmanMapper.deleteFreshProduct(freshmanProduct1.getProductId(), freshmanProduct1.getTabId());
                freshmanMapper.deletePromotionExculByProductId(freshmanProduct1.getProductId());
                }
            }
        if(null!=freshmanProduct1.getFpSkuCode()||!"".equals(freshmanProduct1.getFpSkuCode())) {
            //查询源Product表信息数据复制一品多价数据...插入Xquark_product表
            XquarkProductWithBLOBs xquarkProductWithBLOBs = xquarkProductCopyMapper.selectByPrimaryKey(Long.valueOf(freshmanProduct1.getProductId()));
            xquarkProductWithBLOBs.setSourceId(Long.valueOf(freshmanProduct1.getProductId()));
            xquarkProductWithBLOBs.setId(null);
            xquarkProductWithBLOBs.setStatus("ONSALE");
            xquarkProductWithBLOBs.setCreatedAt(new Date());
            xquarkProductWithBLOBs.setUpdatedAt(new Date());
            if(null==freshmanProduct1.getSourceId()||"".equals(freshmanProduct1.getSourceId())){
                int i = xquarkProductCopyMapper.insertSelective(xquarkProductWithBLOBs);
                if (i==0){
                    throw  new BizException(GlobalErrorCode.valueOf("XquarkProduct复制product信息失败:"+xquarkProductWithBLOBs.getId()));
                }
                //更新加密product商品code
                String Code = IdTypeHandler.encode(xquarkProductWithBLOBs.getId());
                int i1 = xquarkProductCopyMapper.updateCodeById(Code,xquarkProductWithBLOBs.getId());
                if (i1==0){
                    throw  new BizException(GlobalErrorCode.valueOf("XquarkProduct更新productCode信息失败:"+xquarkProductWithBLOBs.getId()));
                }
                //活动商品过滤表..插入promotion_exclusive表
                PromotionExclusive promotionExclusive  = new PromotionExclusive();
                promotionExclusive.setpCode("FreshMan");
                promotionExclusive.setProductId(xquarkProductWithBLOBs.getId());
                promotionExclusive.setStatus(1);
                promotionExclusive.setCreatedAt(new Date());
                int i2  = promotionExclusiveCopyMapper.insertSelective(promotionExclusive);
                if(i2==0){
                    throw  new BizException(GlobalErrorCode.valueOf("添加promotionExclusive表失败:"+xquarkProductWithBLOBs.getId()));
                }
                List<FreshmanProductSku> freshmanProductSkus = freshmanProduct1.getFreshmanProductSkus();
                for (FreshmanProductSku productSkus : freshmanProductSkus) {
                    List<XquarkSku> bySkuCode = freshManXquarkSkuCopyMapper.findBySkuCode(productSkus.getSkuCode());
                    if(null==bySkuCode){
                        throw  new BizException(GlobalErrorCode.NOT_FOUND,"复制失败，未找到对应sku");
                    }
                    XquarkSku xquarkSku = bySkuCode.get(0);
                    xquarkSku.setProductId(xquarkProductWithBLOBs.getId());
                    String substring = UUID.randomUUID().toString().replaceAll("-", "").substring(0, 10);
                    String skucode = substring+"FM";
                    List<XquarkSku> bySkuCode1 = freshManXquarkSkuCopyMapper.findBySkuCode(skucode);
                    while (0!=bySkuCode1.size()){
                        skucode=UUID.randomUUID().toString().replaceAll("-", "").substring(0, 10);
                        skucode=skucode+"FM";
                    }
                    xquarkSku.setSkuCode(skucode);
                    xquarkSku.setSecureAmount(0);
                    xquarkSku.setSourceSkuCode(productSkus.getSkuCode());
                    if(null!=productSkus.getFreshManPrice()){xquarkSku.setPrice(productSkus.getFreshManPrice());}//将专区价格复制到SKU表
                    if(null!=productSkus.getFreshPoint()){xquarkSku.setDeductionDPoint(productSkus.getFreshPoint());}//将专区设置德分复制到SKU表
                    if(null!=productSkus.getFreshStock()){
                        Integer skuId = freshmanMapper.selectSourceSkuIdBySkuCode(productSkus.getSkuCode());
                        //校验原库存数量
                        Boolean result = promotionStockCheckService.stockCheck(productSkus.getFreshStock(), skuId,null,"",null);
                        if(result == true){
                            xquarkSku.setAmount(productSkus.getFreshStock());//将活动库存复制到SKU表
                        }
                    }
                    if(null!=productSkus.getReduction()){xquarkSku.setPoint(productSkus.getReduction());}//将专区立减复制到SKU表
                    if(null!=productSkus.getPromoAmt()){xquarkSku.setPromoAmt(productSkus.getPromoAmt());}//将专区推广费复制到SKU表
                    if(null!=productSkus.getServerAmt()){xquarkSku.setServerAmt(productSkus.getServerAmt());}//将专区服务费复制到SKU表
                    if(null!=productSkus.getNetWorth()){xquarkSku.setNetWorth(productSkus.getNetWorth());}//将专区净值复制到SKU表
                    xquarkSku.setCreatedAt(new Date());
                    xquarkSku.setUpdatedAt(new Date());
                    //复制sku...插入xquark_sku表
                    if(productSkus.getStatus()!=0){
                        int i3 = freshManXquarkSkuCopyMapper.insert(xquarkSku);
                        if(i3==0){
                            throw  new BizException(GlobalErrorCode.valueOf("添加xquarkSku表失败:"+xquarkProductWithBLOBs.getId()));
                        }
                    }
                    //更新SKUCode加密字段
                    String code = IdTypeHandler.encode(xquarkSku.getId());
                    int i4 = freshManXquarkSkuCopyMapper.updateSkuCodeById(code,xquarkSku.getId());
                    if(i4==0){
                        throw  new BizException(GlobalErrorCode.valueOf("更新xquarkSkuCode表失败:"+xquarkProductWithBLOBs.getId()));
                    }
                    //插入promotion_skus表
                    FreshmanPromotionSku freshmanPromotionSku = new FreshmanPromotionSku();
                    freshmanPromotionSku.setProductId(xquarkProductWithBLOBs.getId().toString());
                    freshmanPromotionSku.setpCode("Freshman");
                    freshmanPromotionSku.setSkuCode(xquarkSku.getSkuCode());
                    if(null!=productSkus.getFreshStock()){freshmanPromotionSku.setAmount(productSkus.getFreshStock());}
                    freshmanPromotionSku.setCreatedAt(new Date());
                    freshmanPromotionSku.setIsDeleted((byte) 1);
//                        Integer integer2 = promotionSkusCopyMapper.queryPromotionSkuByProductIdSku(xquarkProductWithBLOBs.getId().toString(),freshmanProduct1.getProductId(), xquarkSku.getSkuCode());
                    if(productSkus.getStatus()!=0){
                        int i5 = promotionSkusCopyMapper.insertSelective(freshmanPromotionSku);
                        if(i5==0){
                            throw  new BizException(GlobalErrorCode.valueOf("添加promotionSkus表失败:"+xquarkProductWithBLOBs.getId()));
                        }
                    }

                    //插入xquark_freshman_product表
                    if(productSkus.getStatus()!=0){
                        FreshmanProduct freshmanProduct = new FreshmanProduct();
                        freshmanProduct.setProductId(xquarkProductWithBLOBs.getId().toString());
                        freshmanProduct.setFreshSkuCode(xquarkSku.getSkuCode());
                        freshmanProduct.setStatus(productSkus.getStatus());
                        freshmanProduct.setTabId(tabId);
                        freshmanProduct.setProductName(freshmanProduct1.getProductName());
                        freshmanProduct.setStock(productSkus.getFreshStock());
                        freshmanProduct.setPrice(productSkus.getFreshManPrice());
                        Integer integer = freshManService.AddTabProduct(freshmanProduct);
                        if (integer==0){
                            throw  new BizException(GlobalErrorCode.valueOf("商品更新失败:"+freshmanProduct.getProductName()));
                        }
                    }
                }
                int i3 = freshmanMapper.updateProductAmount(xquarkProductWithBLOBs.getId());
                if (i3==0){
                    throw  new BizException(GlobalErrorCode.valueOf("更新XquarkProduct表库存amount失败:"+xquarkProductWithBLOBs.getId()));
                }
                Boolean combineProduct = freshmanMapper.isCombineProduct(xquarkProductWithBLOBs.getSourceId());
                if(combineProduct==true){
                    XquarkSku xquarkSku = freshManXquarkSkuCopyMapper.selectSkuIdByProductId(xquarkProductWithBLOBs.getId());
                    XquarkSkuCombine xquarkSkuCombine = new XquarkSkuCombine();
                    xquarkSkuCombine.setProductId(xquarkProductWithBLOBs.getId());
                    xquarkSkuCombine.setSkuId(xquarkSku.getId());
                    //插入套装表xquark_sku_combine
                    int i4 = freshmanMapper.insertXquarkSkuCombine(xquarkSkuCombine);
                    if (i4==0){
                        throw  new BizException(GlobalErrorCode.valueOf("插入xquark_sku_combine表数据失败:"+xquarkProductWithBLOBs.getId()));
                    }
                    XquarkSkuCombine sourceCombineId = freshmanMapper.selectCombineIdByProductId(xquarkProductWithBLOBs.getSourceId());
                    List<XquarkSkuCombineExtra> xquarkSkuCombineExtras = freshmanMapper.selectCombineExtraByMasterId(sourceCombineId.getId());
                    for (XquarkSkuCombineExtra xquarkSkuCombineExtra : xquarkSkuCombineExtras) {
                        XquarkSkuCombineExtra xquarkSkuCombineExtra1 = new XquarkSkuCombineExtra();
                        xquarkSkuCombineExtra1.setMasterId(xquarkSkuCombine.getId());
                        xquarkSkuCombineExtra1.setSlaveId(xquarkSkuCombineExtra.getSlaveId());
                        xquarkSkuCombineExtra1.setAmount(xquarkSkuCombineExtra.getAmount());
                        Integer integer = freshmanMapper.insertCombineExtra(xquarkSkuCombineExtra1);
                        if (integer==0){
                            throw  new BizException(GlobalErrorCode.valueOf("插入xquark_sku_combine_extra表数据失败:"+xquarkProductWithBLOBs.getId()));
                        }
                    }
                }
                        freshmanModuleService.updateUser(userName,moduleId);
                        logger.info("新人商品信息修改成功，操作人："+userName+",操作时间："+now());
//                            //更新xquark_sku复制的商品信息
//                            int i = freshManXquarkSkuCopyMapper.updateByProductIdSkuCode(xquarkSku);
//                            if(i==0){
//                                throw  new BizException(GlobalErrorCode.valueOf("更新xquarkSku表失败:"+xquarkProductWithBLOBs.getId()));
//                            }
//                            updateStock(xquarkSku.getProductId(),xquarkSku.getSkuCode(),productSkus.getFreshStock());
//
//
//                            int i = promotionSkusCopyMapper.updatePromotionByProductSku(freshmanPromotionSku);
//                            if(i==0){
//                                throw  new BizException(GlobalErrorCode.valueOf("更新promotionSkus表失败:"+xquarkProductWithBLOBs.getId()));
//                            }
            }else{
                List<FreshmanProductSku> freshmanProductSkus = freshmanProduct1.getFreshmanProductSkus();
                for (FreshmanProductSku productSkus : freshmanProductSkus) {
                    XquarkSku xquarkSku = new XquarkSku();
                    xquarkSku.setSecureAmount(0);
                    xquarkSku.setProductId(productSkus.getProductIdsku());
                    xquarkSku.setSkuCode(productSkus.getSkuCode());
                    if(null!=productSkus.getFreshManPrice()){xquarkSku.setPrice(productSkus.getFreshManPrice());}//将专区价格复制到SKU表
                    if(null!=productSkus.getFreshPoint()){xquarkSku.setDeductionDPoint(productSkus.getFreshPoint());}//将专区设置德分复制到SKU表
//                    if(null!=productSkus.getFreshStock()){xquarkSku.setAmount(productSkus.getFreshStock());}//将活动库存复制到SKU表
                    if(null!=productSkus.getReduction()){xquarkSku.setPoint(productSkus.getReduction());}//将专区立减复制到SKU表
                    if(null!=productSkus.getPromoAmt()){xquarkSku.setPromoAmt(productSkus.getPromoAmt());}//将专区推广费复制到SKU表
                    if(null!=productSkus.getServerAmt()){xquarkSku.setServerAmt(productSkus.getServerAmt());}//将专区服务费复制到SKU表
                    if(null!=productSkus.getNetWorth()){xquarkSku.setNetWorth(productSkus.getNetWorth());}//将专区净值复制到SKU表
                    xquarkSku.setUpdatedAt(new Date());
                    int i = freshManXquarkSkuCopyMapper.updateByProductIdSkuCode(xquarkSku);
                    if(i==0){
                        throw  new BizException(GlobalErrorCode.valueOf("更新xquarkSku表失败:"+productSkus.getProductIdsku()));
                    }
                    //更新Xquark_sku表库存
//                    updateStock(productSkus.getProductIdsku(),xquarkSku.getSkuCode(),productSkus.getFreshStock());

                    FreshmanPromotionSku freshmanPromotionSku = new FreshmanPromotionSku();
                    freshmanPromotionSku.setProductId(productSkus.getProductIdsku().toString());
                    freshmanPromotionSku.setpCode("Freshman");
                    freshmanPromotionSku.setSkuCode(productSkus.getSkuCode());
                    if(null!=productSkus.getFreshStock()){freshmanPromotionSku.setAmount(productSkus.getFreshStock());}
                    freshmanPromotionSku.setUpdatedAt(new Date());
                    freshmanPromotionSku.setIsDeleted((byte) 1);
                    int i8 = promotionSkusCopyMapper.updatePromotionByProductSku(freshmanPromotionSku);
                    if(i8==0){
                        throw  new BizException(GlobalErrorCode.valueOf("更新promotionSkus表失败:"+productSkus.getProductIdsku()));
                    }
                    FreshmanProduct freshmanProduct = new FreshmanProduct();
                    freshmanProduct.setProductId(productSkus.getProductIdsku().toString());
                    freshmanProduct.setFreshSkuCode(xquarkSku.getSkuCode());
                    freshmanProduct.setStatus(productSkus.getStatus());
                    freshmanProduct.setTabId(tabId);
                    freshmanProduct.setProductName(freshmanProduct1.getProductName());
                    freshmanProduct.setStock(productSkus.getFreshStock());
                    freshmanProduct.setPrice(productSkus.getFreshManPrice());
                    freshmanProduct.setUpdatedAt(new Date());
                    int i1 = freshmanMapper.updateFreshProductByCode(freshmanProduct);
                    if(i1==0){
                        throw  new BizException(GlobalErrorCode.valueOf("更新xquark_freshman_product表失败:"+productSkus.getProductIdsku()));
                    }

                    List<FreshmanProduct> freshmanProducts = freshmanMapper.queryProductByid(productSkus.getProductIdsku().toString());
                        Boolean flag = false;
                        for (int i7 = 0; i7 < freshmanProducts.size(); i7++) {
                            if(freshmanProducts.get(i7).getStatus()==1){
                                flag=true;
                            }
                        }
                        if(!flag){
                            int i9 = xquarkProductCopyMapper.updateProductINSTOCK(productSkus.getProductIdsku().toString());
                            if(i9==0){
                                throw  new BizException(GlobalErrorCode.valueOf("更新XquarkProduct下架状态失败:"+productSkus.getProductIdsku()));
                            }
                        }
                }

                int i2 = freshmanMapper.updateProductAmount(freshmanProduct1.getId());
                if (i2==0){
                    throw  new BizException(GlobalErrorCode.valueOf("更新XquarkProduct表库存amount失败:"+freshmanProduct1.getId()));
                }
                List<FreshmanProduct> freshmanProducts = freshmanMapper.queryProductByid(freshmanProduct1.getId().toString());
                XquarkProductWithBLOBs xquarkProductWithBLOBs1 = xquarkProductCopyMapper.selectByPrimaryKey(freshmanProduct1.getId());
                for (int i7 = 0; i7 < freshmanProducts.size(); i7++) {
                    if(freshmanProducts.get(i7).getStatus()==1&&xquarkProductWithBLOBs1.getAmount()>0&&xquarkProductWithBLOBs1.getStatus().equals("INSTOCK")){
                        int i = xquarkProductCopyMapper.updateProductONSALE(freshmanProduct1.getId());
                        if (i==0){
                            throw  new BizException(GlobalErrorCode.valueOf("更新XquarkProduct表上架状态失败:"+freshmanProduct1.getId()));
                        }
                    }
                }
                freshmanModuleService.updateUser(userName,moduleId);
                logger.info("新人商品信息修改成功，操作人："+userName+",操作时间："+now());
            }
        }
    }


//    /**
//     * 返回能否保存
//     * @param skuCode
//     * @param count
//     * @return
//     */
//    public  boolean isSave(String skuCode,int count){
//            String FirstSkuCode=freshmanMapper.selectSourceSkuCodeBySkuCode(skuCode);
//            int skuCodeAmount=freshmanMapper.selectSkuCodeAmount(skuCode);
//            skuCodeAmount+=count;
////            int updateAmount=freshmanMapper.updateAmount(count,skuCode);
//            boolean a=false;
//                int sourceCodeAmount=freshmanMapper.selectSourceSkuCodeAmount(FirstSkuCode);
//                if(skuCodeAmount<=sourceCodeAmount && skuCodeAmount>=0){
//                    a=true;
//            }
//            return a;
//    }
    @Transactional
    public synchronized Integer updateStock(Integer id, Integer stock, Integer skuId){
        Integer skuAmount = freshManXquarkSkuCopyMapper.getSkuAmount(id, skuId);
        Integer integer = freshManXquarkSkuCopyMapper.updateStockByskuCodeAndId(id, stock, skuId);
        if(integer==0){
            throw  new BizException(GlobalErrorCode.valueOf("更新XquarkSku库存失败:"+id));
        }
        return integer;
    }
}