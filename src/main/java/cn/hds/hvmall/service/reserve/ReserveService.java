package cn.hds.hvmall.service.reserve;

import cn.hds.hvmall.entity.list.ReserveForm;
import cn.hds.hvmall.pojo.grandsale.PromotionVO;

import java.util.List;
import java.util.Map;

public interface ReserveService {

	//添加预约
	Boolean addReserve(ReserveForm reserveForm);

	/**
	 * 获取预约列表
	 */
	Map<String,Object> getReserveList(String name, Integer status, int pageIndex, int pageSize);

	/**
	 * 删除预约
	 */
	boolean deleteReserveById(Long reserveId);

	/**
	 * 终止预约
	 */
	boolean endReserveStatus(Long reserveId);
	/**
	 * 启用预约
	 */
	boolean startReserveStatus(Long reserveId);
	/**
	 * 编辑预约活动
	 */
	boolean updatePromotionReserve(ReserveForm reserveForm);

	/**
	 * 520活动需要的方法
	 */
	List<PromotionVO> getEffectivePromotion();

	List<PromotionVO> getEffectivePromotion2();

	/**
	 * 查询预售活动
	 * @param id 预售活动id
	 * @return 预售活动对象
	 */
	PromotionVO selectByPromotionId(String id);

	/**
	 * 根据活动id查询预售活动状态
	 * @param id 活动id
	 * @return 活动对象
	 */
	Integer selectStatusByPromotionId(String id);

}
