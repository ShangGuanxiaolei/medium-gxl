package cn.hds.hvmall.service.reserve.impl;

import cn.hds.hvmall.entity.list.ReserveForm;
import cn.hds.hvmall.entity.list.ReserveResultList;
import cn.hds.hvmall.entity.reserve.PromotionReserve;
import cn.hds.hvmall.entity.reserve.PromotionReserveProduct;
import cn.hds.hvmall.mapper.piece.XquarkSkuMapper;
import cn.hds.hvmall.mapper.reserve.PromotionReserveMapper;
import cn.hds.hvmall.mapper.reserve.PromotionReserveProductMapper;
import cn.hds.hvmall.pojo.grandsale.PromotionVO;
import cn.hds.hvmall.service.reserve.ReserveService;
import cn.hds.hvmall.utils.list.GlobalErrorCode;
import cn.hds.hvmall.utils.list.qiniu.BizException;
import cn.hds.hvmall.utils.piece.UUIDGenerator;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Service
public class ReserveServiceImpl implements ReserveService {

	private final PromotionReserveMapper promotionReserveMapper;

	private final PromotionReserveProductMapper promotionReserveProductMapper;

  private final XquarkSkuMapper skuMapper;

	@Autowired
	public ReserveServiceImpl(PromotionReserveMapper promotionReserveMapper, PromotionReserveProductMapper promotionReserveProductMapper, XquarkSkuMapper skuMapper) {
		this.promotionReserveMapper = promotionReserveMapper;
		this.promotionReserveProductMapper = promotionReserveProductMapper;
		this.skuMapper = skuMapper;
	}

	@Override
	@Transactional
	public Boolean addReserve(ReserveForm reserveForm) {
		if (reserveForm == null) {
			throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "添加的预约信息不能为空");
		}
		String code = UUIDGenerator.getUUID();
		PromotionReserve promotionReserve = reserveForm.getPromotionReserve();
		promotionReserve.setReserveCode(code);
			promotionReserve.setStatus(1);
			//校验
		int i = promotionReserveMapper.addReserve(promotionReserve);
		//通过id查询出来id
		Long reserveId = promotionReserveMapper.selectIdByCode(code);
		//批量添加预约商品
		int result = 0;
		List<PromotionReserveProduct> reserveProductList = reserveForm.getReserveProductList();
		List<Long> skuList = new ArrayList<>();
		//查询表中所有的商品的skuId
		List<Long> skuLists = promotionReserveProductMapper.selectAllSkuId();
		for (PromotionReserveProduct promotionReserveProduct : reserveProductList) {
			if(promotionReserveProduct == null || promotionReserveProduct.getBuyLimit() == null || promotionReserveProduct.getReserveLimit() == null
							|| promotionReserveProduct.getAmountLimit() == null || promotionReserveProduct.getConversionPrice() == null || promotionReserveProduct.getPreReserveLimit() == null ||
							promotionReserveProduct.getReservePrice() == null){
				throw new BizException(GlobalErrorCode.INTERNAL_ERROR,"商品信息不能为空");
			}
			if(promotionReserveProduct.getBuyLimit() <= 0 || promotionReserveProduct.getReserveLimit() <=0
				 || promotionReserveProduct.getAmountLimit() <=0 || promotionReserveProduct.getPreReserveLimit() < 0){
					throw new BizException(GlobalErrorCode.INTERNAL_ERROR,"商品信息不能小于等于０或者商品信息不能为空");
			}
			if(promotionReserveProduct.getConversionPrice().compareTo(BigDecimal.ZERO) <= 0 ||
							promotionReserveProduct.getReservePrice().compareTo(BigDecimal.ZERO) <= 0){
				throw new BizException(GlobalErrorCode.INTERNAL_ERROR,"商品的预约价或者兑换价不能小于或者等于０");
			}
			if(promotionReserveProduct.getReservePrice().compareTo(promotionReserveProduct.getConversionPrice()) == -1){
				throw new BizException(GlobalErrorCode.INTERNAL_ERROR,"兑换价不能大于预约价");
			}
			String categoryName = promotionReserveProduct.getCategoryName();
			if(StringUtils.isEmpty(categoryName)){
				throw new BizException(GlobalErrorCode.INTERNAL_ERROR,"类目名称不能为空");
			}
			if(skuLists.contains(promotionReserveProduct.getSkuId())){
				throw new BizException(GlobalErrorCode.INTERNAL_ERROR,promotionReserveProduct.getSkuId()+"该商品已经在预约活动中，不能重复添加");
			}
			//扣减sku的库存  查询出sku的库存
			Integer skuAmount = skuMapper.selectSkuAmount(promotionReserveProduct.getSkuId());
			if(skuAmount < promotionReserveProduct.getAmountLimit()){
				//　配置的活动库存上限大于sku的库存
				throw new BizException(GlobalErrorCode.INTERNAL_ERROR,"配置的活动库存不能大于sku的库存");
			}
			//根据配置的活动库存扣减相应的sku的库存
			int updateAmount = skuAmount - promotionReserveProduct.getAmountLimit();
			int res = skuMapper.updateSkuAmount(updateAmount, promotionReserveProduct.getSkuId());
			if(res <= 0){
				throw new BizException(GlobalErrorCode.INTERNAL_ERROR,"修改sku的库存失败");
			}
			//每人限购数不能大于活动库存上限
			if(promotionReserveProduct.getBuyLimit() > promotionReserveProduct.getAmountLimit()){
				throw new BizException(GlobalErrorCode.INTERNAL_ERROR,"每人限购数不能大于活动库存");
			}
			skuList.add(promotionReserveProduct.getSkuId());
			promotionReserveProduct.setReserveId(reserveId);
			result = promotionReserveProductMapper.addReserveProductList(promotionReserveProduct);
		}
		Set<Long> skuSet = new HashSet<>(skuList);
		if(skuSet.size() != skuList.size()){
			throw new BizException(GlobalErrorCode.INTERNAL_ERROR,"同一个活动不能添加相同的sku商品");
		}
		if(i <= 0 || result <= 0){
			throw new BizException(GlobalErrorCode.INTERNAL_ERROR,"添加预约预售失败");
		}else{
			return true;
		}
	}

	@Transactional
	@Override
	public Map<String,Object> getReserveList(String name, Integer status,int pageIndex,int pageSize) {
		int rowIndex = pageIndex <= 0 ? 1:((pageIndex - 1) * pageSize);
		List<ReserveResultList> reserveList = promotionReserveMapper.getReserveList(name, status,rowIndex,pageSize);
		Integer reserveListCount = promotionReserveMapper.getReserveListCount(name, status);
		Map<String,Object> reserveMap = new HashMap<>();
		reserveMap.put("reserveList",reserveList);
		reserveMap.put("reserveListCount",reserveListCount);
		return reserveMap;
	}

	@Override
	@Transactional
	public boolean deleteReserveById(Long reserveId) {
		if(reserveId <= 0){
			throw new BizException(GlobalErrorCode.INTERNAL_ERROR,"预约活动id不存在");
		}
		//校验在活动期间的商品不能删除
		if(promotionReserveMapper.deleteReserveById(reserveId) <= 0 ||
						promotionReserveProductMapper.deleteReserve(reserveId) <= 0){
				throw new BizException(GlobalErrorCode.INTERNAL_ERROR,"删除预约活动失败");
		}
		return true;
	}

	@Override
	@Transactional
	public boolean endReserveStatus(Long reserveId) {
		if(reserveId <= 0){
			throw new BizException(GlobalErrorCode.INTERNAL_ERROR,"预约活动id不存在");
		}
		//根据id查询活动状态
		/*Integer status = promotionReserveMapper.selectStatusById(reserveId);
		if(status == null || status == 3 || status == 4){
			throw new BizException(GlobalErrorCode.INTERNAL_ERROR,"该活动已结束");
		}
		//根据活动id查询出所以的skuId
		List<Long> skuIdList = promotionReserveProductMapper.selectSKuIdById(reserveId);
		for (Long skuId : skuIdList) {
			// 根据每个skuId查询各自对应的活动库存
			Integer amount = promotionReserveProductMapper.selectResersveAmount(reserveId, skuId);
			// 根据skuId查询出sku的库存
			Integer skuAmount = skuMapper.selectSkuAmount(skuId);
			// 计算出要改变的sku的库存
			int resultAmount = skuAmount + amount;
			// 将sku的库存还回去
			if(skuMapper.updateSkuAmount(resultAmount, skuId) <= 0 ||
							promotionReserveProductMapper.updatePromotionAmount(reserveId,skuId,0) <= 0){
				throw new BizException(GlobalErrorCode.INTERNAL_ERROR,"更新库存失败");
			}
		}*/
		// 把所有该活动的订单状态置为取消
		promotionReserveMapper.updateStatusByPromotionId(reserveId, "CANCELLED", "SUBMITTED");
		if(promotionReserveMapper.updateStatusById(reserveId) <= 0){
			throw new BizException(GlobalErrorCode.INTERNAL_ERROR,"终止活动状态失败");
		}
		return true;
	}

	@Override
	@Transactional
	public boolean startReserveStatus(Long reserveId) {
		if(reserveId <= 0){
			throw new BizException(GlobalErrorCode.INTERNAL_ERROR,"该预约不存在");
		}
		/*List<PromotionAmount> promotionAmountList = promotionForm.getPromotionAmountList();
		for (PromotionAmount promotionAmount : promotionAmountList) {
			Long skuId = promotionAmount.getSkuId();
			int pAmount = promotionAmount.getpAmount();
			if(reserveId <= 0){
				throw new BizException(GlobalErrorCode.INTERNAL_ERROR,"预约不存在");
			}
			int i = promotionReserveProductMapper.updatePromotionAmount(reserveId, skuId, pAmount);
			if(i <= 0){
				throw new BizException(GlobalErrorCode.INTERNAL_ERROR,"设置库存失败");
			}
			// 根据skuId查询sku的库存
			Integer skuAmount = skuMapper.selectSkuAmount(skuId);
			if(skuAmount < pAmount){
				throw new BizException(GlobalErrorCode.INTERNAL_ERROR,"设置的活动库存不能超过sku库存");
			}
		}*/
		Integer status = promotionReserveMapper.selectStatusById(reserveId);
			if(status == null || status != 4){
				throw new BizException(GlobalErrorCode.INTERNAL_ERROR,"只有终止的活动才可以启用");
			}
			if(promotionReserveMapper.updateStartStatus(reserveId) <= 0){
				throw new BizException(GlobalErrorCode.INTERNAL_ERROR,"启用活动状态失败");
			}
		return true;
	}

	@Override
	@Transactional
	public boolean updatePromotionReserve(ReserveForm reserveForm) {
			if(reserveForm == null || reserveForm.getPromotionReserve() == null || reserveForm.getReserveProductList().size() == 0){
				throw new BizException(GlobalErrorCode.INTERNAL_ERROR,"预约信息不能为空");
			}
			PromotionReserve promotionReserve = reserveForm.getPromotionReserve();
			if(promotionReserveMapper.updatePromotionReserve(promotionReserve) <= 0){
				throw new BizException(GlobalErrorCode.INTERNAL_ERROR,"预约预售活动更新失败");
			}
			List<PromotionReserveProduct> promotionReserveProductList = reserveForm.getReserveProductList();
			for (PromotionReserveProduct promotionReserveProduct : promotionReserveProductList) {
				if(promotionReserveProductMapper.updateReserveProduct(promotionReserveProduct) <= 0){
					throw new BizException(GlobalErrorCode.INTERNAL_ERROR,"更新商品信息失败");
				}
			}
			return true;
	}

	@Override
	public List<PromotionVO> getEffectivePromotion() {
		return promotionReserveMapper.getEffectivePromotion();
	}

	@Override
	public List<PromotionVO> getEffectivePromotion2() {
		return promotionReserveMapper.getEffectivePromotion2();
	}

  @Override
  public PromotionVO selectByPromotionId(String id) {
    return promotionReserveMapper.selectByPromotionId(id);
  }

	@Override
	public Integer selectStatusByPromotionId(String id) {
		return promotionReserveMapper.selectStatusByPromotionId(id);
	}


}
