package cn.hds.hvmall.service.reserve.impl;

import cn.hds.hvmall.entity.list.*;
import cn.hds.hvmall.entity.reserve.PromotionAmountInfo;
import cn.hds.hvmall.entity.reserve.ReserveProductForm;
import cn.hds.hvmall.mapper.list.ProductMapper;
import cn.hds.hvmall.mapper.piece.XquarkSkuMapper;
import cn.hds.hvmall.mapper.reserve.PromotionReserveMapper;
import cn.hds.hvmall.mapper.reserve.PromotionReserveProductMapper;
import cn.hds.hvmall.service.reserve.ReserveProductService;
import cn.hds.hvmall.utils.list.GlobalErrorCode;
import cn.hds.hvmall.utils.list.qiniu.BizException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class ReserveProductServiceImpl implements ReserveProductService {

	@Autowired
	private ProductMapper productMapper;

	@Autowired
	private PromotionReserveProductMapper promotionReserveProductMapper;

	@Autowired
	private PromotionReserveMapper promotionReserveMapper;

	@Autowired
	private XquarkSkuMapper skuMapper;


	@Override
	@Transactional
	@SuppressWarnings("unchecked")
	public Map<String,Object> searchProduct(ReserveProductForm reserveProductForm) {
		int pageIndex = reserveProductForm.getPageIndex();
		int pageSize = reserveProductForm.getPageSize();
		int rowIndex = pageIndex <= 0 ? 1:((pageIndex - 1) * pageSize);
		List<SearchProductResult> productResultList = productMapper.selectReserveProduct(reserveProductForm,rowIndex,pageSize);
		int productCount = productMapper.selectReserveProductCount(reserveProductForm);
		HashMap resultMap = new HashMap();
		resultMap.put("productResultList",productResultList);
		resultMap.put("productCount",productCount);
		return resultMap;
	}

	@Override
	public boolean deleteReserveProduct(String reserveId,Long skuId) {
		int result = promotionReserveProductMapper.deleteReserveProduct(reserveId, skuId);
		if(result <= 0){
			throw new BizException(GlobalErrorCode.INTERNAL_ERROR,"逻辑删除失败");
		}
		return true;
	}

	@Override
	public ReserveDetailResult getReserveResultById(Long reserveId) {
		if(reserveId <= 0){
			throw new BizException(GlobalErrorCode.INTERNAL_ERROR,"预约活动id不存在");
		}
		//根据预约的id查询
		ReserveDetailResult reserveDetailResult = new ReserveDetailResult();
		ReserveResult reserveResult = promotionReserveMapper.getPromotionReserveById(reserveId);
		if(reserveResult == null){
			throw new BizException(GlobalErrorCode.INTERNAL_ERROR,"预约信息为空");
		}
		//通过id查询类目名称
		List<String> categoryList = promotionReserveProductMapper.getCategoryList(reserveId);
		List<ReCategoryList> reCategoryList = new ArrayList<>();
		for (String category : categoryList) {
			//通过类目名称查商品
			ReCategoryList reCategory = new ReCategoryList();
			List<Long> idList = promotionReserveProductMapper.getProductIdsByReserveAndCategory(category, reserveId);
			reCategory.setCategoryName(category);
			List<ReserveProductDetailInfo> productResults = new ArrayList<>();
			for (Long skuId : idList) {
				ReserveProductDetailInfo reserveProductDetailInfo = promotionReserveProductMapper.getProductDetailInfo(reserveId, skuId);
				if(reserveProductDetailInfo == null){
					throw new BizException(GlobalErrorCode.INTERNAL_ERROR,"该预约预售商品不存在");
				}
				reserveProductDetailInfo.setReserveId(reserveId);
				reserveProductDetailInfo.setSkuId(skuId);
				productResults.add(reserveProductDetailInfo);
			}
			reCategory.setReserveProductDetailInfo(productResults);
			reCategoryList.add(reCategory);
		}
		reserveDetailResult.setReCategoryList(reCategoryList);
		reserveDetailResult.setReserveResult(reserveResult);

		return reserveDetailResult;
	}

	@Override
	public boolean updateReserveAmount(Long reserveId, Long skuId,int pAmount) {
		if(reserveId <= 0){
			throw new BizException(GlobalErrorCode.INTERNAL_ERROR,"预约预售id不存在");
		}
		//通过skuId和预约id查询商品的
		Integer reserveAmount = promotionReserveProductMapper.selectResersveAmount(reserveId,skuId);
		if(reserveAmount == null){
			throw new BizException(GlobalErrorCode.INTERNAL_ERROR,"预约预售活动库存不存在");
		}
		int result = reserveAmount + pAmount;
		//查询商品的sku库存
		Integer skuAmount = skuMapper.selectSkuAmount(skuId);
		// 原来的sku库存
		int res = skuAmount + reserveAmount;
		//校验
		if(result > res || result <= 0){
			throw new BizException(GlobalErrorCode.INTERNAL_ERROR,"修改库存不能超过商品sku的库存或者不能使得活动库存小于０");
		}
		int skuAmountNew = skuAmount - pAmount;
		//修改
		if(skuMapper.updateSkuAmount(skuAmountNew,skuId) <= 0 ||
						promotionReserveProductMapper.updateReserveAmount(reserveId,skuId,result)<=0){
			throw new BizException(GlobalErrorCode.INTERNAL_ERROR,"修改库存失败");
		}
		return true;
	}

	@Override
	public boolean checkDelatePromotion(Long reserveId, List<Long> skuList) {
		//查询该活动是在活动中
		Date beginTime =  promotionReserveProductMapper.selectPromotionBegin(reserveId);
		Date endTime = promotionReserveProductMapper.selectPromotionEnd(reserveId);
		if(new Date().before(endTime) && new Date().after(beginTime)){
			//如果当前时间是在活动期间内,查询该活动的所有skuId
		//	List<Long> skus = promotionReserveProductMapper.selectAllSkuId(reserveId);
			/*for (Long skuId : skuList) {
				if(skus.contains(skuId)){
					throw new BizException(GlobalErrorCode.INTERNAL_ERROR,"在活动期间的商品不支持删除");
				}
			}*/
		}
		return true;
	}

	@Override
	public Map<String,Object> getProductAmount(Long reserveId) {
		//查询出所有的skuid
		List<Long> skuList = promotionReserveProductMapper.selectSKuIdById(reserveId);
		int skuSize = skuList.size();
		List<PromotionAmountInfo> amountList = new ArrayList<>();
		for (Long skuId : skuList) {
			PromotionAmountInfo promotionAmountInfo = new PromotionAmountInfo();
			//通过skuId查询商品的信息
			Integer skuAmount = skuMapper.selectSkuAmount(skuId);
			// 查询商品名称
			promotionAmountInfo.setSkuId(skuId);
			promotionAmountInfo.setAmount(skuAmount);
			// 查询商品id
			Long productId = skuMapper.selectProductIdBySkuId(skuId);
			String productName = productMapper.selectProductNameById(productId);
			promotionAmountInfo.setProductName(productName);
			amountList.add(promotionAmountInfo);
		}
		HashMap<String,Object> amountMap = new HashMap<>();
		amountMap.put("amountList",amountList);
		amountMap.put("count",skuSize);
		return amountMap;
	}
}
