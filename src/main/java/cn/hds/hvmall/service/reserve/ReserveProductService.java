package cn.hds.hvmall.service.reserve;
import cn.hds.hvmall.entity.list.ReserveDetailResult;
import cn.hds.hvmall.entity.reserve.PromotionAmountInfo;
import cn.hds.hvmall.entity.reserve.ReserveProductForm;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.springframework.security.access.method.P;

import java.util.List;
import java.util.Map;

public interface ReserveProductService {

	//搜索商品
	Map<String,Object> searchProduct(ReserveProductForm reserveProductForm);

	/**
	 * 删除预约预售商品
	 * @param 　skuId skuId
	 * @return int
	 */
	boolean deleteReserveProduct(String reserveId,Long skuId);

	/**
	 * 根据id查询需要的数据
	 */
	ReserveDetailResult getReserveResultById(Long reserveId);

	/**
	 * 修改预约预售活动库存
	 */
	boolean updateReserveAmount(Long reserveId,Long skuId,int pAmount);

	/**
	 * 校验在活动中的商品不能删除
	 */
	boolean checkDelatePromotion(Long reserveId, List<Long> skuList);

	/**
	 * 查询商品sku的库存
	 */
	Map<String,Object> getProductAmount(Long reserveId);

}
