package cn.hds.hvmall.service.mail.impl;

import cn.hds.hvmall.entity.freshman.FreshmanSaleCount;
import cn.hds.hvmall.enums.ProductStatus;
import cn.hds.hvmall.mapper.promotion.PromotionExclusiveMapper;
import cn.hds.hvmall.service.mail.SendMailService;
import cn.hds.hvmall.utils.excel.ExcelToEmail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class SendMailServiceImpl implements SendMailService {
    private final Logger logger = LoggerFactory.getLogger(SendMailServiceImpl.class);

    @Value("${spring.mail.username}")
    private String from;

    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private PromotionExclusiveMapper promotionExclusiveMapper;

    private static final String subject = "新人礼套装商品以及VIP套装商品库存及销量信息";
    private static final String content = "Dear all,\n\t\t\t这是新人礼套装商品以及VIP套装商品库存及销量的信息，请注意查收~";



    @Override
    public List<FreshmanSaleCount> selectFreshmanOrderSales() {

        List<FreshmanSaleCount> freshmanSaleCountList=promotionExclusiveMapper.selectFreshmanOrderSales();
        for (int i = 0; i < freshmanSaleCountList.size(); i++) {
            String status=promotionExclusiveMapper.selectStatusByProductId(Integer.parseInt( freshmanSaleCountList.get(i).getProductId()));
            String isStatus="true";
            if(status.equals( ProductStatus.INSTOCK.name()) || status.equals(ProductStatus.SOLDOUT.name())){
                isStatus="false";
            }
            int amount=promotionExclusiveMapper.selectAmountByProductId(isStatus,Integer.parseInt(freshmanSaleCountList.get(i).getProductId()));
            freshmanSaleCountList.get(i).setAmount(amount);
        }
        return freshmanSaleCountList;

    }

    @Override
    public List<FreshmanSaleCount> selectVipSaleInfo() {
        List<FreshmanSaleCount> vipSaleCountList=promotionExclusiveMapper.selectVipSaleInfo();
        return vipSaleCountList;
    }

    @Override
    @Transactional
    public void vipAndFreshmanSalesCount() {
        //得到long类型当前时间
        long l= System.currentTimeMillis();
        //new日期对象
        Date date = new Date(l);
        //转换提日期输出格式
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DAY_OF_MONTH, 1);
        String nyr1=dateFormat.format(c.getTime());
        String nyr = dateFormat.format(date);

        String sendTime=promotionExclusiveMapper.selectSendTime();
        String isSend=promotionExclusiveMapper.selectIsSend();
        if(isSend.equals("0") && sendTime.equals(nyr)) {
            boolean updateIsSend=promotionExclusiveMapper.updateIsSend("1");
            boolean updateSendTime=promotionExclusiveMapper.updateSendTime(nyr1);

            List<String> title = new ArrayList<>();
            title.add("商品Id");
            title.add("商品名称");
            title.add("剩余库存");
            title.add("skuId");
            title.add("skuCode");
            title.add("当月销量");
            title.add("昨日销量");

            String emails = promotionExclusiveMapper.selectEmailsTo();
            List<String> toList = Arrays.asList(emails.split(","));
            String emailsCC = promotionExclusiveMapper.selectEmailsCC();
            List<String> ccList = Arrays.asList(emailsCC.split(","));

            String freshmanSale = ".\\src\\main\\resources\\excel\\新人套装销量信息表" + nyr + ".xls";
            String vipSale = ".\\src\\main\\resources\\excel\\VIP套装销量信息表" + nyr + ".xls";

            List<String> filePath = Arrays.asList(freshmanSale, vipSale);
            try {
                for (int i = 0; i < filePath.size(); i++) {
                    if (i == 0) {
                        ExcelToEmail.toExcel(title, selectFreshmanOrderSales(), filePath.get(i));
                    } else {
                        ExcelToEmail.toExcel(title, selectVipSaleInfo(), filePath.get(i));
                    }
                }

                sendAttachmentMail(toList, ccList, subject + nyr, content, filePath);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void sendSimpleMail(List<String> toList, String subject, String content) throws AddressException {

        logger.info("发送HTML邮件开始：{},{},{}", toList, subject, content);
        MimeMessage message = mailSender.createMimeMessage();

        MimeMessageHelper helper;
        try {
            helper = new MimeMessageHelper(message, false);
            helper.setFrom(from);
            helper.setTo(toList.get(0));
            helper.setSubject(subject);
            helper.setText(content);
            mailSender.send(message);
            logger.info("发送邮件成功");
        } catch (MessagingException e) {
            logger.error("发送邮件失败：", e);
        }
    }

    @Override
    public void sendHtmlMail(List<String> toList, String subject, String content) {

        logger.info("发送HTML邮件开始：{},{},{}", toList, subject, content);
        MimeMessage message = mailSender.createMimeMessage();

        MimeMessageHelper helper;
        try {
            InternetAddress[] toAddress = getAddress(toList);
            helper = new MimeMessageHelper(message, true);
            helper.setFrom(from);
            helper.setTo(toAddress);
            helper.setSubject(subject);
            helper.setText(content);
            mailSender.send(message);
            logger.info("发送HTML邮件成功");
        } catch (MessagingException e) {
            logger.error("发送HTML邮件失败：", e);
        }
    }
    @Override
    public void sendAttachmentMail(List<String> toList,List<String> ccList, String subject, String content, List<String> filePath) {

        System.setProperty("mail.mime.splitlongparameters","false");
        System.setProperty("mail.mime.charset","UTF-8");

        logger.info("发送带附件邮件开始：{},{},{},{}", toList, subject, content, filePath);
        MimeMessage message = mailSender.createMimeMessage();

        MimeMessageHelper helper;
        try {
            InternetAddress[] toAddress = getAddress(toList);
            message.addRecipients(Message.RecipientType.TO, toAddress);
            // 设置抄送
            if(ccList.size()>0 && ccList.get(0)!="") {
                InternetAddress[] toAddressCC = getAddress(ccList);
                message.addRecipients(Message.RecipientType.CC, toAddressCC);
            }
            helper = new MimeMessageHelper(message, true);
            helper.setFrom(from);
            helper.setTo(toAddress);
            helper.setSubject(subject);
            helper.setText(content, true);
            for (int i = 0; i < filePath.size(); i++) {
            FileSystemResource file = new FileSystemResource(new File(filePath.get(i)));
            String [] fileNames=file.getFilename().split("cel");
                String fileName="";
            if(fileNames.length>1) {
                fileName = fileNames[1];
            }else{
                fileName=fileNames[0];
            }
            helper.addAttachment(fileName, file);//添加附件，可多次调用该方法添加多个附件
            }

            mailSender.send(message);
            logger.info("发送带附件邮件成功");

        } catch (MessagingException e) {
            //得到long类型当前时间
            long l= System.currentTimeMillis();
            //new日期对象
            Date date = new Date(l);
            //转换提日期输出格式
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
            String nyr = dateFormat.format(date);
            boolean updateSendTime=promotionExclusiveMapper.updateSendTime(nyr);
            logger.error("发送带附件邮件失败", e);
        }finally {
            boolean updateIsSend=promotionExclusiveMapper.updateIsSend("0");
        }

    }
    /**
     * 获取邮件地址
     * @param list
     * @return
     */
    private InternetAddress[] getAddress(List<String> list) throws AddressException {
        InternetAddress[] ccAddress = null;
        if(list != null){
            ccAddress = new InternetAddress[list.size()];
            for(int i = 0; i < list.size(); i++){
                String emailAddress = list.get(i);
                new InternetAddress(emailAddress);
                ccAddress[i]=new InternetAddress(emailAddress);
            }
        }
        return ccAddress;
    }
    @Override
    public void sendInlineResourceMail(List<String> toList, String subject, String content, String rscPath, String rscId) {

        logger.info("发送带图片邮件开始：{},{},{},{},{}", toList, subject, content, rscPath, rscId);
        MimeMessage message = mailSender.createMimeMessage();

        MimeMessageHelper helper;
        try {
            helper = new MimeMessageHelper(message, true);
            InternetAddress[] toAddress = getAddress(toList);
            helper.setFrom(from);
            helper.setTo(toAddress);
            helper.setSubject(subject);
            helper.setText(content, true);
            FileSystemResource res = new FileSystemResource(new File(rscPath));
            helper.addInline(rscId, res);//重复使用添加多个图片
            mailSender.send(message);
            logger.info("发送带图片邮件成功");
        } catch (MessagingException e) {
            logger.error("发送带图片邮件失败", e);
        }
    }
}
