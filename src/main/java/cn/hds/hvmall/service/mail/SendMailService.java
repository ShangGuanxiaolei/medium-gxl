package cn.hds.hvmall.service.mail;

import cn.hds.hvmall.entity.freshman.FreshmanSaleCount;

import javax.mail.internet.AddressException;
import java.util.List;

public interface SendMailService {



    List<FreshmanSaleCount> selectFreshmanOrderSales();

    List<FreshmanSaleCount> selectVipSaleInfo();

    void vipAndFreshmanSalesCount();


    /**
     *发送普通文本邮件
     * @param toList 收件人列表
     * @param subject
     * @param content
     */
    void sendSimpleMail(List<String> toList, String subject, String content) throws AddressException;
    /**
     * 发送HTML邮件
     * @param toList
     * @param subject 主题
     * @param content 内容（可以包含<html>等标签）
     */
    void sendHtmlMail(List<String> toList, String subject, String content);
    /**
     * 发送带附件的邮件
     * @param toList 收件人列表
     * @param subject 主题
     * @param content 内容
     * @param filePath 附件路径
     */
    void sendAttachmentMail(List<String> toList,List<String> ccList, String subject, String content, List<String> filePath);
    /**
     * 发送带图片的邮件
     * @param toList 收件人列表
     * @param subject 主题
     * @param content 文本
     * @param rscPath 图片路径
     * @param rscId 图片ID，用于在<img>标签中使用，从而显示图片
     */
    void sendInlineResourceMail(List<String> toList, String subject, String content, String rscPath, String rscId);




}
