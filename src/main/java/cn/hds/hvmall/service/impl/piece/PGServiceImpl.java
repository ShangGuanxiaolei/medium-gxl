package cn.hds.hvmall.service.impl.piece;

import cn.hds.hvmall.base.BaseExecuteResult;
import cn.hds.hvmall.entity.list.ProductAmountInfo;
import cn.hds.hvmall.entity.list.SkuList;
import cn.hds.hvmall.entity.piece.*;
import cn.hds.hvmall.entity.promotion.ActivityExclusive;
import cn.hds.hvmall.mapper.list.ProductMapper;
import cn.hds.hvmall.mapper.piece.*;
import cn.hds.hvmall.mapper.promotion.PromotionExclusiveMapper;
import cn.hds.hvmall.mapper.promotion.PromotionSkuMapper;
import cn.hds.hvmall.mapper.stockCheck.PromotionStockCheckMapper;
import cn.hds.hvmall.pojo.piece.BackPGThreeVO;
import cn.hds.hvmall.pojo.piece.PGVO;
import cn.hds.hvmall.pojo.piece.PartSpec;
import cn.hds.hvmall.pojo.piece.PgSku;
import cn.hds.hvmall.service.impl.UserServiceImpl;
import cn.hds.hvmall.service.piece.PGService;
import cn.hds.hvmall.service.piece.PrefixLocker;
import cn.hds.hvmall.service.stockcheck.PromotionStockCheckService;
import cn.hds.hvmall.utils.DateUtils;
import cn.hds.hvmall.utils.list.GlobalErrorCode;
import cn.hds.hvmall.utils.list.qiniu.BizException;
import cn.hds.hvmall.utils.list.qiniu.ResourceResolver;
import cn.hds.hvmall.utils.list.qiniu.SpringContextUtil;
import cn.hds.hvmall.utils.piece.UUIDGenerator;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;

@Slf4j
@Service
@Transactional
public class PGServiceImpl implements PGService {

  private static final Logger log = LoggerFactory.getLogger(UserServiceImpl.class);


  /**
   * 拼主价不设置
   */
  private static final int NOTSETUP = 0;

  /**
   * 拼主价统一设置
   */
  private static final int UNIFIEDSETUP = 1;

  /**
   * 拼主价分别设置
   */
  private static final int PARTSETUP = 2;

  private final PromotionBaseInfoMapper promotionBaseInfoMapper;
  private final PromotionPgDetailMapper promotionPgDetailMapper;
  private final PromotionRewardsMapper promotionRewardsMapper;
  private final PromotionPgMapper promotionPgMapper;
  private final PromotionSkusMapper promotionSkusMapper;
  private final ComplexQueryMapper complexQueryMapper;
  private final PromotionSkuMapper promotionSkuMapper;
  private final PromotionExclusiveMapper promotionExclusiveMapper;
  private final PrefixLocker prefixLocker;// 人工预锁库存
  private final EditMapper editMapper;
  private final PromotionPgPriceMapper promotionPgPriceMapper;
  private final PromotionPgMasterPriceMapper promotionPgMasterPriceMapper;
  private final XquarkSkuMapper xquarkSkuMapper;
  private final ProductMapper productMapper;
  private final PromotionStockCheckService stockCheckService;
  private final PromotionStockCheckMapper stockCheckMapper;

  private static final String PIECE = "piece";

  @Autowired
  public PGServiceImpl(PromotionBaseInfoMapper promotionBaseInfoMapper, PromotionPgDetailMapper promotionPgDetailMapper,
                       PromotionRewardsMapper promotionRewardsMapper, PromotionPgMapper promotionPgMapper,
                       PromotionSkusMapper promotionSkusMapper, ComplexQueryMapper complexQueryMapper,
                       PromotionSkuMapper promotionSkuMapper, PromotionExclusiveMapper promotionExclusiveMapper,
                       PrefixLocker prefixLocker, EditMapper editMapper,
                       PromotionPgPriceMapper promotionPgPriceMapper, PromotionPgMasterPriceMapper promotionPgMasterPriceMapper,
                       XquarkSkuMapper xquarkSkuMapper, ProductMapper productMapper,PromotionStockCheckService stockCheckService,PromotionStockCheckMapper stockCheckMapper) {
    this.promotionBaseInfoMapper = promotionBaseInfoMapper;
    this.promotionPgDetailMapper = promotionPgDetailMapper;
    this.promotionRewardsMapper = promotionRewardsMapper;
    this.promotionPgMapper = promotionPgMapper;
    this.promotionSkusMapper = promotionSkusMapper;
    this.complexQueryMapper = complexQueryMapper;
    this.promotionSkuMapper = promotionSkuMapper;
    this.promotionExclusiveMapper = promotionExclusiveMapper;
    this.prefixLocker = prefixLocker;
    this.editMapper = editMapper;
    this.promotionPgPriceMapper = promotionPgPriceMapper;
    this.promotionPgMasterPriceMapper = promotionPgMasterPriceMapper;
    this.xquarkSkuMapper = xquarkSkuMapper;
    this.productMapper = productMapper;
    this.stockCheckService=stockCheckService;
    this.stockCheckMapper=stockCheckMapper;
  }

  @Override
  public void create(PGVO pgVO) {
    //定义临时变量存储当前时间
    Date date = new Date();
    try {
      List<PartSpec> partSpecs = pgVO.getPartSpecs();
      if (null == partSpecs || partSpecs.size() == 0) {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "拼团价格体系参数不能为空");
      }

      //判断名称是否已存在,存在则创建失败
      final Boolean duplicatePromotion = promotionBaseInfoMapper.isDuplicatePromotion(pgVO.getPgSkus().get(0).getProductId());
      if (duplicatePromotion) {
        throw new BizException(GlobalErrorCode.DUPLICATE_PROMOTION_PRODUCT);
      }

      //封装插入promotion_base_info
      PromotionBaseInfo promotionBaseInfo = new PromotionBaseInfo();
      if (StringUtils.isNoneBlank(pgVO.getpName())) {
        promotionBaseInfo.setpName(pgVO.getpName());
      } else {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "活动名称不能为空");
      }

      if (StringUtils.isNoneBlank(pgVO.getpGTime())) {
        String[] times = pgVO.getpGTime().split("/");
        promotionBaseInfo.setEffectFrom(DateUtils.strToTimestamp(times[0]));
        promotionBaseInfo.setEffectTo(DateUtils.strToTimestamp(times[1]));
      } else {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "活动时间不能为空");
      }

      //生成pCode
      String pCode = UUIDGenerator.getUUID();
      promotionBaseInfo.setpType(PIECE);
      promotionBaseInfo.setpCode(pCode);
      promotionBaseInfo.setBuyLimit(pgVO.getBuyLimit());
      promotionBaseInfo.setpStatus(2);
      promotionBaseInfo.setAuditor(null);
      promotionBaseInfo.setIsDeleted(1);
      promotionBaseInfoMapper.insertPromotionBaseInfo(promotionBaseInfo);

      //封装插入promotion_pg
      PromotionPg promotionPg = new PromotionPg();
      if (pgVO.getpEffectTime() != null) {
        promotionPg.setPieceEffectTime(pgVO.getpEffectTime());
      } else {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "成团时间不能为空");
      }

      Integer isDeliveryReduction = pgVO.getIsDeliveryReduction();
      if (isDeliveryReduction != null && (isDeliveryReduction == 0 || isDeliveryReduction == 1)) {
        promotionPg.setIsDeliveryReduction(isDeliveryReduction);
      } else {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "是否计算运费参数错误,isDeliveryReduction:" + isDeliveryReduction);
      }

      Integer jumpTime = pgVO.getJumpQueueTime();
      if (jumpTime != null) {
        promotionPg.setJumpQueueTime(jumpTime);
      }

      String orgGroupQua = pgVO.getOrgGroupQua();
      if (StringUtils.isNoneBlank(orgGroupQua)) {
        promotionPg.setOrgGroupQua(orgGroupQua);
      } else {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "组团资格不能为空");
      }

      String joinGroupQua = pgVO.getJoinGroupQua();
      if (StringUtils.isNoneBlank(joinGroupQua)) {
        promotionPg.setJoinGroupQua(joinGroupQua);
      } else {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "参团资格不能为空");
      }

      Integer skuLimit = pgVO.getSkuLimit();
      if (skuLimit != null && skuLimit >= 0) {
        promotionPg.setSkuLimit(skuLimit);
      } else {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "限购件数不能为空或为负数");
      }

      promotionPg.setpCode(pCode);
      promotionPg.setIsDeleted(1);
      promotionPg.setCreatedAt(date);
      promotionPg.setUpdatedAt(date);
      promotionPgMapper.insert(promotionPg);

      insertPromotionPgDetail(partSpecs, pCode, date);

      PromotionSkus promotionSkus = new PromotionSkus();
      List<PgSku> pgSkuList = pgVO.getPgSkus();
      //拼团三期支持多规格，循环插入数据，考虑优化
      if (!pgSkuList.isEmpty()) {
        List<SkuList> skuLists = productMapper.searchSku(Integer.parseInt(pgSkuList.get(0).getProductId()));
        //设置商品规格
        List<Double> skuPriceList = new ArrayList<>();
        for (SkuList skuList :
                skuLists) {
          //将每个规格的价格加入到列表中
          skuPriceList.add(Optional.of(skuList.getSkuPrice()).orElse(0D));
        }
        Double min = Collections.min(skuPriceList);
        for (PartSpec partSpec : partSpecs) {
          BigDecimal promotionPrice = Optional.of(partSpec.getPromotionPrice()).orElse(BigDecimal.ZERO);
          if (promotionPrice.compareTo(BigDecimal.valueOf(min)) >= 0) {
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "拼团价不能大于等于商品原价");
          }
        }
        for (PgSku pgSku :
                pgSkuList) {
          promotionSkus.setId(null);
          String productId = pgSku.getProductId();
          Long skuId = pgSku.getsId();
          Integer skuNum = pgSku.getpSkuNum();
          //校验库存
          stockCheckService.stockCheck(skuNum,skuId.intValue(),null,"",null);
          if (null == productId || null == skuId || null == skuNum) {
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "商品规格信息不能为空");
          }
          String skuCode = xquarkSkuMapper.selectSkuCodeById(skuId);
          if (null == skuCode) {
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "skuCode为空");
          }
          promotionSkus.setProductId(productId);
          promotionSkus.setSkuCode(skuCode);
          promotionSkus.setpCode(pCode);
          promotionSkus.setCreatedAt(date);
          promotionSkus.setUpdatedAt(date);
          promotionSkus.setIsDeleted(1);
          promotionSkus.setSkuNum(skuNum);
          promotionSkusMapper.insert(promotionSkus);
        }
      }
    } catch (RuntimeException e) {
      log.error("拼团创建异常", e);
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, e.getMessage());
    }
  }

  @Override
  public BaseExecuteResult<List<BackPGThreeVO>> queryBackPGVOList(Map<String, Object> selectConditions) {
    BaseExecuteResult<List<BackPGThreeVO>> result = new BaseExecuteResult<>();
    try {
      //商品id
      String productId = (String) selectConditions.get("productId");
      //商品名称
      String productName = (String) selectConditions.get("productName");
      //活动状态
      String status = (String) selectConditions.get("pStatus");
      Integer pStatus = null;
      if (StringUtils.isNotBlank(status)) {
        pStatus = Integer.parseInt(status);
      }
      //分页页码
      int page = Integer.parseInt(selectConditions.get("page").toString());
      //分页大小
      int pageSize = Integer.parseInt(selectConditions.get("pageSize").toString());
      if (StringUtils.isNotBlank(status)) {
        pStatus = Integer.parseInt(status);
      }
      page = (page - 1) * pageSize;

      //查询基本活动信息
      List<BackPGThreeVO> backPGVOS =
              complexQueryMapper.queryPGInventory(productName, productId, pStatus, page, pageSize);

      //根据pCode获取详细配置
      for (BackPGThreeVO backPGVO : backPGVOS) {
        String pCode = backPGVO.getpCode();
        if (null == pCode) {
          throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "pCode为空");
        }
        //获取拼团活动详情
        List<PromotionPgDetail> promotionPgDetails = promotionPgDetailMapper.selectByPCode(pCode);

        StringBuilder pieceGroupNum = new StringBuilder();
        List<BigDecimal> promotionPriceList = new ArrayList<>();
        int pSkuNum = 0;

        for (PromotionPgDetail promotionPgDetail : promotionPgDetails) {
          String pDetailCode = promotionPgDetail.getPDetailCode();
          if (null == pDetailCode) {
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "pDetailCode为空");
          }
          pieceGroupNum.append(promotionPgDetail.getPieceGroupNum()).append("/");
          PromotionPgPrice promotionPgPrice = promotionPgPriceMapper.selectByDetailCode(pDetailCode);
          BigDecimal promotionPrice = Optional.ofNullable(promotionPgPrice)
                  .map(BigDecimal -> promotionPgPrice.getPromotionPrice())
                  .orElse(BigDecimal.ZERO);
          promotionPriceList.add(promotionPrice);
        }

        //活动库存
        List<Integer> skuNum = promotionSkuMapper.getSkuNum(pCode);
        if (null == skuNum || skuNum.size() <= 0) {
          skuNum = promotionSkuMapper.selectPromotionSkusByPCode(pCode);
        }

        //累加sku库存
        for (int i : skuNum) {
          pSkuNum += i;
        }

        //活动库存
        backPGVO.setpSkuNum(pSkuNum);
        //成团人数
        backPGVO.setPieceGroupNum(pieceGroupNum + "");
        //拼团价格的最大值及最小值
        BigDecimal max = Collections.max(promotionPriceList);
        BigDecimal min = Collections.min(promotionPriceList);
        if (max.equals(min)) {
          backPGVO.setPromotionPrice(min + "");
        } else {//多规格显示价格区间
          backPGVO.setPromotionPrice(min + "~" + max);
        }

        int listCount = complexQueryMapper.listCount(productName, productId, pStatus);
        if (listCount > 0) {
          int totalPage = (listCount % pageSize == 0) ? (listCount / pageSize) : (listCount / pageSize + 1);
          backPGVO.setPageCount(listCount);
          backPGVO.setTotalPage(totalPage);
          backPGVO.setPage(page);
          backPGVO.setPageSize(pageSize);
        } else {
          backPGVO.setPageCount(0);
          backPGVO.setTotalPage(0);
        }
      }
      result.setIsSuccess(200);
      result.setResult(backPGVOS);
      return result;
    } catch (Exception e) {
      log.error("查询拼团清单异常: ", e);
      result.setErrorCode("500");
      result.setErrorMsg("查询拼团清单异常" + e.getMessage());
      return result;
    }
  }

  /**
   * 发布拼团活动
   */
  @Override
  public BaseExecuteResult<Object> publishPieceGroup(String pCode, String statusCode) {
    BaseExecuteResult<Object> result = new BaseExecuteResult<>();
    try {
      //  判断是否已到生效时间,
      // ① 若未到生效时间则写状态为4,
      // ② 若超过失效时间则写状态为7,
      // ③ 否则写状态为5
      // statusCode = 4/5/7

      EffectTimePo effectTimePo = promotionBaseInfoMapper.selectEffectTimeFromAndEffectTimeTo(pCode);
      if (null == effectTimePo || null == effectTimePo.getEffectTo() || null == effectTimePo.getEffectFrom()) {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "活动时间配置为空");
      }
      Date effectTo = effectTimePo.getEffectTo();
      Date date = new Date();
      if (date.after(effectTo)) {
        statusCode = "7";
        result.setIsSuccess(200);
        result.setErrorCode("end");
        result.setErrorMsg("Success");
        result.setExecuteType("update");
      } else {
        //根据pCode查询promotionSkus的skuNum和productID
        List<PromotionSkus> promotionSkusList = promotionSkusMapper.selectPromotionSkusInfoByPCode(pCode);

        if (promotionSkusList == null || promotionSkusList.size() <= 0) {
          throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "活动sku配置不存在");
        }
        String productId = promotionSkusList.get(0).getProductId();
        if (StringUtils.isBlank(productId)) {
          throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "商品id为空");
        }

        ActivityExclusive activityExclusive = new ActivityExclusive();
        activityExclusive.setStatus(1);
        activityExclusive.setProductId(Long.parseLong(productId));
        activityExclusive.setProjectCode(pCode);
        activityExclusive.setCreatedAt(date);
        promotionExclusiveMapper.insertActivityProduct(activityExclusive);

        for (PromotionSkus promotionSkus :
            promotionSkusList) {
          if (promotionSkus.getSkuNum() <= 0) {
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "活动库存不能小于等于0");
          }
          //预锁库存
          PrefixLockerBean prefixLockerBean = new PrefixLockerBean();
          prefixLockerBean.setSkuCode(promotionSkus.getSkuCode());
          prefixLockerBean.setP_code(pCode);
          prefixLockerBean.setP_sku_num(promotionSkus.getSkuNum());
          boolean prefixLockerResult = prefixLocker.prefixLocker(prefixLockerBean);
          if (!prefixLockerResult) {
            log.error("发布活动预锁库存失败");
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "活动商品skuCode:"+promotionSkus.getSkuCode()+",库存不足或活动库存大于商品库存");
          }
        }
      }

      Integer effC = promotionBaseInfoMapper.updatePStatus(pCode, statusCode);
      if (effC == null || effC != 1) {
        result.setIsSuccess(500);
        result.setErrorCode("error");
        result.setErrorMsg("更新活动状态异常");
        result.setExecuteType("error");
        log.error("更新活动状态异常");
        return result;
      }

      if ("5".equals(statusCode)) {
        result.setIsSuccess(200);
        result.setErrorCode("started");
        result.setErrorMsg("Success");
        result.setExecuteType("update");
      }
      return result;
    } catch (Exception e) {
      result.setIsSuccess(500);
      result.setErrorCode("error");
      result.setErrorMsg(e.getMessage());
      result.setExecuteType("error");
      log.error("更新活动状态异常", e);
      return result;
    }
  }

  /**
   * 根据pCode查询拼团活动信息
   */
  @Override
  public BaseExecuteResult<List<GroupBaseInfoVO>> select(String pCode) {
    BaseExecuteResult<List<GroupBaseInfoVO>> result = new BaseExecuteResult<>();
    List<GroupBaseInfoVO> groupBaseInfoVO = editMapper.select(pCode);
    result.setResult(groupBaseInfoVO);
    return result;
  }

  /**
   * 待完成!!!!!!!!!!!!!!! 根据pCode和传入的参数对象update相应字段
   */
  @Override
  public BaseExecuteResult<Object> update(EditGroupBaseInfoVO info) {
    BaseExecuteResult<Object> result = new BaseExecuteResult<>();
    //需要判断3/5/10人团这里是做了怎样的变更
    //删除一个 3/5/10  --> 3/5/0   如果是删除一个,只需将少的那个配置逻辑删除
    //增加一个 3/0/0   --> 3/10/0  如果是增加需要在promotion_pg_detail表中增加一条
    //替换一个 3/0/0   --> 0/5/0  逻辑删除+新增
    //根据PCode查出原拼团详细配置
    //装拼团人数
    ArrayList<Integer> listOld1 = new ArrayList<>();
    List<GroupBaseInfoVO> groupBaseInfoVO = editMapper.select(info.getpCode());
    for (GroupBaseInfoVO list : groupBaseInfoVO) {
      listOld1.add(list.getPieceGroupNum());
    }
    //修改情况:
    //1.在原有的基础上修改,不新增
    //5.有多条,在原有的基础上修改时,有的不改
    //2.新增,上一条不更改
    //3.新增,上一条修改
    //4.在行数>1的情况下,去掉X行,
    //修改情况:
    //不新增  update即可
    //新增  判断原数据是否变更,不变update
    //变更,insert
    //删除  先删除,再判断原数据是否变更
    //判断数据是否变更,先判断成团人数,再判断拼团折扣
    //让前端将成团人数以3/5/0的形式传进来
    //折扣以8折/7.5/0的形式传进来,后台要乘以10
    //获得传进来的拼团详细配置 信息
    String pGroupNum = info.getPieceGroupNum();
    String[] listNew1 = pGroupNum.split("/");
    String sDiscount = info.getSkuDiscount();
    String[] listNew2 = sDiscount.split("/");

    //在原有的基础上修改,不新增

    for (int i = 0; i < listNew1.length; i++) {
      //如果新传进来的不是0,且原来值为0,则新增一条.
      if (!listNew1[i].equals("0") && listOld1.get(i) == 0) {
        //在promotion_pg_detail表中增加一条

        String pDetailCode = UUIDGenerator.getUUID();
        String pCode = info.getpCode();
        int pieceGroupNum = Integer.parseInt(listNew1[i]);
        int skuDiscount = Integer.parseInt(listNew2[i]) * 10;
        //通过pCode查promotion_pg中的creator和updator
        List<String> name = editMapper.selectPromotionPG(pCode);
        String creator = name.get(1);
        String updator = name.get(2);
        PromotionPgDetailSimple promotionPgDetailSimple = new PromotionPgDetailSimple(
                pDetailCode, pCode, pieceGroupNum, skuDiscount, creator, updator);
        editMapper.insertPromotionPGDetail(promotionPgDetailSimple);
      }
    }

    return result;
  }

  @Override
  public PGVO getInfo(String pCode) {
    //获取基本信息
    PGVO pgvo = promotionBaseInfoMapper.selectByProCode(pCode);
    if (null == pgvo) {
      throw new BizException(GlobalErrorCode.NOT_FOUND, "获取拼团信息失败，pCode不存在");
    }
    //获取商品信息
    List<Map<String, Object>> products = promotionBaseInfoMapper.getPgProducts(pCode);
    ResourceResolver resourceFacade = (ResourceResolver) SpringContextUtil
            .getBean("resourceFacade");
    for (Map<String, Object> product : products) {
      String url = product.get("img").toString();
      product.put("img", resourceFacade.resolveUrl(url));
    }
    pgvo.setPgProduct(products);
    //获取拼团方案信息
    //拼团三期拼团方案放置位置变更,使用p_detail_code查找
    List<PartSpec> specList = new ArrayList<>();
    List<PromotionPgDetail> promotionPgDetails = promotionPgDetailMapper.selectByPCode(pCode);
    for (PromotionPgDetail promotionPgDetail : promotionPgDetails) {
      String pDetailCode = promotionPgDetail.getPDetailCode();
      if (null == pDetailCode) {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "pDetailCode为空");
      }
      PartSpec partSpec = promotionBaseInfoMapper.getPartSpec(pDetailCode);
      if (partSpec != null) {
        if (null != partSpec.getUnifiedSetup() && partSpec.getUnifiedSetup() != 0) {
          PromotionPgMasterPrice masterPrice = promotionBaseInfoMapper.getMasterPrice(pDetailCode);
          if (null != masterPrice) {
            BeanUtils.copyProperties(masterPrice, partSpec);
          }
        }
        specList.add(partSpec);
      }
    }
    // 简单处理下编辑时前端展示问题
    PartSpec partSpec = new PartSpec();
    if (specList.size() == 1) {
      specList.add(partSpec);
      specList.add(partSpec);
    }
    if (specList.size() == 2) {
      specList.add(partSpec);
    }
    pgvo.setPartSpecs(specList);

    return pgvo;
  }

  @Override
  public String updateInfo(PGVO pgvo) {
    Date date = new Date();
    String pCode = pgvo.getpCode();
    PromotionBaseInfo promotionBaseInfo = new PromotionBaseInfo();
    promotionBaseInfo.setpName(pgvo.getpName());
    promotionBaseInfo.setpCode(pCode);
    promotionBaseInfo.setBuyLimit(pgvo.getBuyLimit());
    String[] times = pgvo.getpGTime().split("/");
    promotionBaseInfo.setEffectFrom(DateUtils.strToTimestamp(times[0]));
    promotionBaseInfo.setEffectTo(DateUtils.strToTimestamp(times[1]));
    promotionBaseInfo.setUpdatedAt(date);
    if (promotionBaseInfoMapper.updateBaseInfo(promotionBaseInfo) <= 0) {
      throw new BizException(GlobalErrorCode.UNKNOWN, "更新项目基本信息错误");
    }

    PromotionPg promotionPg = new PromotionPg();
    promotionPg.setpCode(pCode);
    promotionPg.setIsDeliveryReduction(pgvo.getIsDeliveryReduction());
    promotionPg.setPieceEffectTime(pgvo.getpEffectTime());
    promotionPg.setOrgGroupQua(pgvo.getOrgGroupQua());
    promotionPg.setJoinGroupQua(pgvo.getJoinGroupQua());
    promotionPg.setJumpQueueTime(pgvo.getJumpQueueTime());
    promotionPg.setSkuLimit(pgvo.getSkuLimit());
    promotionPg.setUpdatedAt(date);
    if (promotionBaseInfoMapper.updatePg(promotionPg) <= 0) {
      throw new BizException(GlobalErrorCode.UNKNOWN, "更新拼团配置信息错误");
    }

    Map<String, String> map = new HashMap<>();
    map.put("p_code", pCode);
    List<PromotionSkus> skuList = promotionSkuMapper.selectByPCode(map);
    if (null == skuList) {
      throw new BizException(GlobalErrorCode.UNKNOWN, "活动sku数据不存在");
    }

    List<Map<String, Object>> products = pgvo.getPgProduct();
    for (PromotionSkus promotionSku : skuList) {
      if (null == promotionSku.getSkuCode()) {
        throw new BizException(GlobalErrorCode.UNKNOWN, "活动商品skuCode为空");
      }
      for (Map<String, Object> skuMap : products) {
        if (StringUtils.isBlank(skuMap.get("skuCode").toString())
                || StringUtils.isBlank(skuMap.get("pSkuNum").toString())) {
          throw new BizException(GlobalErrorCode.UNKNOWN, "pSkuNum,skuCode为空,");
        }

        if (promotionSku.getSkuCode().equals(skuMap.get("skuCode"))) {
          promotionSku.setSkuNum(Integer.parseInt(skuMap.get("pSkuNum").toString()));
          promotionSku.setUpdatedAt(date);
          //校验库存
          int skuId=stockCheckMapper.selectSkuIdBySkuCode(promotionSku.getSkuCode());
          stockCheckService.stockCheck(promotionSku.getSkuNum(),skuId,promotionSku.getpCode(),"PIECE",null);

          if (promotionSkuMapper.updateByPromotionSkus(promotionSku) <= 0) {
            throw new BizException(GlobalErrorCode.UNKNOWN, "更新活动库存失败");
          }
        }
      }
    }

    List<PromotionPgDetail> promotionPgDetails = promotionPgDetailMapper.selectByPCode(pCode);
    for (PromotionPgDetail promotionPgDetail : promotionPgDetails) {
      String pDetailCode = promotionPgDetail.getPDetailCode();
      if (null == pDetailCode) {
        throw new BizException(GlobalErrorCode.UNKNOWN, "pDetailCode为空");
      }
      PromotionPgMasterPrice promotionPgMasterPrice = promotionPgMasterPriceMapper.selectByPDetailCode(pDetailCode);
      PromotionPgPrice promotionPgPrice = promotionPgPriceMapper.selectByDetailCode(pDetailCode);
      if (null != promotionPgPrice) {
        if (promotionPgPriceMapper.deleteByPDetailCode(pDetailCode) <= 0) {
          throw new BizException(GlobalErrorCode.UNKNOWN, "删除活动价格体系失败");
        }
      }
      if (null != promotionPgMasterPrice) {
        if (promotionPgMasterPriceMapper.deleteByPDetailCode(pDetailCode) <= 0) {
          throw new BizException(GlobalErrorCode.UNKNOWN, "删除活动价格体系失败");
        }
      }
    }

    //删除所有配置
    if (promotionPgDetailMapper.deleteByPCode(pCode) <= 0 || promotionRewardsMapper.deleteByPCode(pCode) <= 0) {
      throw new BizException(GlobalErrorCode.UNKNOWN, "删除活动配置失败");
    }

    if (null == pgvo.getPartSpecs()) {
      throw new BizException(GlobalErrorCode.UNKNOWN, "价格体系参数为空");
    }

    insertPromotionPgDetail(pgvo.getPartSpecs(), pCode, date);//直接使用添加的 / 方法
    return "修改成功";
  }


  @Override
  public boolean updateActivityExclusiveStatus(String pCode) {
    int status = promotionExclusiveMapper.updateActivityExclusiveStatus(0, pCode);
    int pStatus = promotionExclusiveMapper.updateBaseInfoStatus(pCode);
    return status > 0 && pStatus > 0;
  }

  /**
   * 插入promotionPgDetail
   *
   * @param partSpecs partSpecs
   * @param pCode     pCode
   * @param date      date
   */
  private void insertPromotionPgDetail(List<PartSpec> partSpecs, String pCode, Date date) {
    for (PartSpec partSpec : partSpecs) {
      Integer unifiedSetup = partSpec.getUnifiedSetup();
      Integer pieceGroupNum = partSpec.getPieceGroupNum();
      BigDecimal promotionPrice = partSpec.getPromotionPrice();
      BigDecimal point = partSpec.getPoint();
      BigDecimal reduction = partSpec.getReduction();
      BigDecimal netWorth = partSpec.getNetWorth();
      BigDecimal promoAmt = partSpec.getPromoAmt();
      BigDecimal serverAmt = partSpec.getServerAmt();
      Boolean newFree = partSpec.getNewFree();

      if (unifiedSetup == null) {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "请勾选拼主价类型");
      }

      if (pieceGroupNum == null || pieceGroupNum < 0) {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "请输入正确的成团人数");
      }

      if (promotionPrice == null || promotionPrice.signum() < 0) {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "请输入正确的拼团价");
      }

      if (newFree == null) {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "新人免单参数不能为空");
      }

      if (reduction != null && reduction.signum() < 0) {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "立减不能为负数");
      }

      if (netWorth != null && netWorth.signum() < 0) {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "净值不能为负数");
      }

      if (promoAmt != null && promoAmt.signum() < 0) {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "推广费不能为负数");
      }

      if (serverAmt != null && serverAmt.signum() < 0) {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "服务费不能为负数");
      }

      if (point != null) {
        if (point.signum() < 0) {
          throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "德分不能为负数");
        }
        if (point.add(Optional.ofNullable(reduction).orElse(BigDecimal.ZERO).multiply(BigDecimal.TEN))
                .compareTo(promotionPrice.multiply(BigDecimal.TEN)) > 0) {
          throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "兑换德分不能大于拼团价加立减值");
        }
      }

      //封装插入promotion_pg_detail
      String pDetailCode = UUIDGenerator.getUUID();
      PromotionPgDetail promotionPgDetail = new PromotionPgDetail();
      promotionPgDetail.setPDetailCode(pDetailCode);
      promotionPgDetail.setpCode(pCode);
      promotionPgDetail.setPieceGroupNum(pieceGroupNum);
      promotionPgDetail.setCreatedAt(date);
      promotionPgDetail.setUpdatedAt(date);
      promotionPgDetailMapper.insert(promotionPgDetail);

      PromotionPgPrice promotionPgPrice = new PromotionPgPrice();
      promotionPgPrice.setpDetailCode(pDetailCode);
      promotionPgPrice.setPromotionPrice(promotionPrice);
      promotionPgPrice.setPoint(point);
      promotionPgPrice.setReduction(reduction);
      promotionPgPrice.setNetWorth(netWorth);
      promotionPgPrice.setPromoAmt(promoAmt);
      promotionPgPrice.setServerAmt(serverAmt);
      promotionPgPrice.setNewFree(newFree);
      promotionPgPrice.setUnifiedSetup(unifiedSetup);
      promotionPgPrice.setCreatedAt(date);
      promotionPgPrice.setUpdatedAt(date);
      promotionPgPriceMapper.insertSelective(promotionPgPrice);

      insertPromotionPgMasterPrice(unifiedSetup, partSpec, pDetailCode, date);

      //封装插入promotion_rewards
      PromotionRewards promotionRewards = new PromotionRewards();
      promotionRewards.setpCode(pCode);
      promotionRewards.setpType(PIECE);
      promotionRewards.setCreatedAt(date);
      promotionRewards.setUpdatedAt(date);
      promotionRewards.setpDetailCode(pDetailCode);
      promotionRewards.setRewardCode(UUIDGenerator.getUUID());
      promotionRewards.setRewardCondition("1");
      promotionRewards.setRewardRule("1");
      promotionRewards.setCreatedAt(date);
      promotionRewards.setUpdatedAt(date);
      promotionRewards.setRewardType("1");
      promotionRewards.setRewardSubType("1");
      promotionRewardsMapper.insert(promotionRewards);
    }
  }

  @Override
  public List<GroupAmount> getGroupAmount(String pCode) {
    if (StringUtils.isEmpty(pCode)) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "pCode不能为空");
    }
    List<GroupAmount> productAmountBypCode = editMapper.getProductAmountBypCode(pCode);
    if (productAmountBypCode.size() == 0) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "没有对应商品");
    }
    return productAmountBypCode;
  }

  @Override
  public boolean updateAmount(List<ProductAmountInfo> infoList) {
    if (infoList.size() == 0) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "活动的库存信息不存在");
    }
    for (ProductAmountInfo amountInfo : infoList) {
      if (amountInfo != null) {
        String pCode = amountInfo.getpCode();
        Integer skuId = amountInfo.getSkuId();
        String skuCode = editMapper.selectSkuCodeBySkuId(skuId);
        if (StringUtils.isEmpty(skuCode)) {
          throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "skuCode不能为空");
        }
        Integer newSkuNum = amountInfo.getpAmount();
        if (newSkuNum == null) {
          throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "修改的库存不能为空");
        }
        Integer amount = editMapper.selectAmountBySkuCode(skuCode);
        if (amount == null) {
          throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "商品信息不存在");
        }
        Integer oldSkuNum = editMapper.selectOldNum(skuCode, pCode);
        if (oldSkuNum == null) {
          throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "活动库存信息不存在");
        }
        int resultSkuNum = oldSkuNum + newSkuNum;
        stockCheckService.stockCheck(resultSkuNum,skuId,pCode,"PIECE",null);
        if (resultSkuNum <= 0 || amount - newSkuNum < 0) {
          throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "更改的库存不符合条件");
        }

        if (amount - resultSkuNum < 0) {
          throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "活动库存不能大于商品最大库存");
        }

        if (editMapper.updatePromotionSkuNum(skuCode, resultSkuNum) <= 0) {
          throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "更新活动库存失败");
        }
        Integer status = editMapper.checkPromotionStatus(pCode);
        if (status == null) {
          status = 0;
        }
        if (status == 5) {
          int usableSkuNum = editMapper.selectUsableSkuNum(skuCode, pCode);
          int result = usableSkuNum + newSkuNum;
          if (result <= 0 || result > resultSkuNum) {
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "可用活动库存小于０或者大于活动库存");
          }
          if (editMapper.updatePromotionTempStock(skuCode, resultSkuNum) <= 0 ||
                  editMapper.updateUsableSkuNum(skuCode, result) <= 0) {
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "更新活动库存失败");
          }
          if (!xquarkSkuMapper.updateXquarkSku(amount - newSkuNum, skuCode)) {
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "更新商品Sku库存失败");
          }
        }
      } else {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "修改的库存不能为空");
      }
    }
    return true;
  }

  @Override
  public boolean deleteActivity(String pCode) {
    int i = promotionExclusiveMapper.deleteActivity(pCode);
    return i>0;
  }

  /**
   * 封装插入活动价格体系
   *
   * @param unifiedSetup unifiedSetup
   * @param partSpec     partSpec
   * @param pDetailCode  pDetailCode
   * @param date         date
   */
  private void insertPromotionPgMasterPrice(Integer unifiedSetup, PartSpec partSpec, String pDetailCode, Date date) {
    if (unifiedSetup == NOTSETUP) {
      log.info("没有设置拼主价");
      return;
    }

    if (unifiedSetup == UNIFIEDSETUP) {
      BigDecimal memberPrice = partSpec.getMemberPrice();
      Integer frequency = partSpec.getFrequency();
      checkThrowException(memberPrice);
      checkThrowException(frequency);

      PromotionPgMasterPrice promotionPgMasterPrice = new PromotionPgMasterPrice();
      promotionPgMasterPrice.setpDetailCode(pDetailCode);
      promotionPgMasterPrice.setMemberPrice(memberPrice);
      promotionPgMasterPrice.setFrequency(frequency);
      promotionPgMasterPrice.setCreatedAt(date);
      promotionPgMasterPrice.setUpdatedAt(date);
      promotionPgMasterPriceMapper.insertSelective(promotionPgMasterPrice);
      return;
    }

    if (unifiedSetup == PARTSETUP) {
      BigDecimal pureMemberPrice = partSpec.getPureMemberPrice();
      Integer pureFrequency = partSpec.getPureFrequency();
      BigDecimal whiteMemberPrice = partSpec.getWhiteMemberPrice();
      Integer whiteFrequency = partSpec.getWhiteFrequency();
      BigDecimal vipMemberPrice = partSpec.getVipMemberPrice();
      Integer vipFrequency = partSpec.getVipFrequency();
      BigDecimal merchantMemberPrice = partSpec.getMerchantMemberPrice();
      Integer merchantFrequency = partSpec.getMerchantFrequency();

      checkThrowException(pureMemberPrice);
      checkThrowException(whiteMemberPrice);
      checkThrowException(vipMemberPrice);
      checkThrowException(merchantMemberPrice);
      checkThrowException(pureFrequency);
      checkThrowException(whiteFrequency);
      checkThrowException(vipFrequency);
      checkThrowException(merchantFrequency);

      PromotionPgMasterPrice promotionPgMasterPrice = new PromotionPgMasterPrice();
      promotionPgMasterPrice.setpDetailCode(pDetailCode);
      promotionPgMasterPrice.setPureMemberPrice(pureMemberPrice);
      promotionPgMasterPrice.setPureFrequency(pureFrequency);
      promotionPgMasterPrice.setWhiteMemberPrice(whiteMemberPrice);
      promotionPgMasterPrice.setWhiteFrequency(whiteFrequency);
      promotionPgMasterPrice.setVipMemberPrice(vipMemberPrice);
      promotionPgMasterPrice.setVipFrequency(vipFrequency);
      promotionPgMasterPrice.setMerchantMemberPrice(merchantMemberPrice);
      promotionPgMasterPrice.setMerchantFrequency(merchantFrequency);
      promotionPgMasterPrice.setCreatedAt(date);
      promotionPgMasterPrice.setUpdatedAt(date);
      promotionPgMasterPriceMapper.insertSelective(promotionPgMasterPrice);
    }
  }

  /**
   * 负值校验
   *
   * @param o Object
   */
  private void checkThrowException(Object o) {
    if (o != null) {
      if (o instanceof Integer) {
        if ((Integer) o < 0) {
          throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "拼主价次数不能为负数");
        }
      }
      if (o instanceof BigDecimal) {
        BigDecimal b = (BigDecimal) o;
        if (b.signum() < 0) {
          throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "拼主价不能为负数");
        }
      }
    }
  }

}
