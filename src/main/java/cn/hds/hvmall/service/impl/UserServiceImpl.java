package cn.hds.hvmall.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import cn.hds.hvmall.base.BaseExecuteResult;
import cn.hds.hvmall.entity.User;
import cn.hds.hvmall.mapper.UsersMapper;
import cn.hds.hvmall.service.UserService;
import cn.hds.hvmall.utils.ConstantUtil;
import cn.hds.hvmall.utils.JSONUtil;





/**
 * 用户服务实现层
 * @类名: UserServiceImpl.java
 * @描述: TODO .
 * @程序猿: Jrain Chen
 * @日期: 2018年8月10日
 * @版本号: V1.0 .
 */
@Service
public class UserServiceImpl extends ServiceImpl<UsersMapper, User> implements UserService  {
	private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
	@Autowired
	private UsersMapper usersMapper;

	/** 根据手机号查询用户信息
     * @方法名: queryUserByPhone .
     * @描述: TODO .
     * @程序猿: chenjingwu .
     * @返回值: BaseExecuteResult<Object> .
     * @日期: 2017年3月14日 上午11:15:35 .
     * @throws
     */
	@Transactional
	@Override
	public BaseExecuteResult<Object> queryUserByPhone(String json) {
		logger.info(ConstantUtil.IN_PARAMETER_FORMAT, this.getClass().getSimpleName(), "queryUserByPhone", json);
		BaseExecuteResult<Object> result = null;
		try {
			List<User> findUsers =  this.usersMapper.selectByMap(JSONUtil.toMap(json));
			result = new BaseExecuteResult<Object>(ConstantUtil.success, findUsers);
		} catch (Exception e) {
			e.printStackTrace();
			logger.info(ConstantUtil.ERROR_FORMAT, this.getClass().getSimpleName(), "queryUserByPhone", e.getMessage());
			result = new BaseExecuteResult<Object>(ConstantUtil.failed, ConstantUtil.ResponseError.SYS_ERROR.getCode(),
					ConstantUtil.ResponseError.SYS_ERROR.toString());
			// 针对多条数据操作需要手动开启事务
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
		logger.info(ConstantUtil.OUT_PARAMETER_FORMAT, this.getClass().getSimpleName(), "queryUserByPhone", result);
		return result;
	}
}
