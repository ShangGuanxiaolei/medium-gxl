package cn.hds.hvmall.service.order;

import lombok.*;

import java.math.BigDecimal;

/**
 * @author luqing
 * @since 2019-05-14
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class TotalDTO {
    private String date;
    private Integer total;
    private BigDecimal totalFee;
}
