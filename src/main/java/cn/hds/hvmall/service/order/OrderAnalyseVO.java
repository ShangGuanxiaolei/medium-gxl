package cn.hds.hvmall.service.order;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * 订单统计VO对象
 * @author wangxinhua
 * @date 2019-05-12
 * @since 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderAnalyseVO {

    private String date;
    private int total;
    private BigDecimal totalFee;
    private int totalNewOrderCount;
    private int totalVipOrderCount;
    private Long newRegisteredUserCount;
    private Long reserveCount;
    private Long openPieceCount;
    private Long joinPieceCount;

    public OrderAnalyseVO(String date) {
        this.date = date;
    }
}
