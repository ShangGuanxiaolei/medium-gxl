package cn.hds.hvmall.service.order;

import cn.hds.hvmall.enums.CareerLevelType;
import cn.hds.hvmall.mapper.order.OrderAnalyseMapper;
import cn.hds.hvmall.type.PromotionType;
import cn.hds.hvmall.utils.list.GlobalErrorCode;
import cn.hds.hvmall.utils.list.qiniu.BizException;
import org.mockito.internal.util.collections.Sets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author wangxinhua
 * @date 2019-05-12
 * @since 1.0
 */
@Service
public class OrderAnalyseService {

    private final OrderAnalyseMapper orderAnalyseMapper;

    private final static PromotionType[] INCLUDE_TYPES
            = new PromotionType[]{PromotionType.PIECE, PromotionType.PIECE_GROUP,
            PromotionType.FLASHSALE, PromotionType.RESERVE};


    private final static Set<String> TYPE_SET = Sets.newSet(
            "ALL", PromotionType.FLASHSALE.toString(),PromotionType.RESERVE.toString(),PromotionType.PIECE.toString());
    private final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    @Autowired
    public OrderAnalyseService(OrderAnalyseMapper orderAnalyseMapper) {
        this.orderAnalyseMapper = orderAnalyseMapper;
    }


    public List<OrderAnalyseVO> analyseByType(LocalDateTime startDate, LocalDateTime endDate, String type) {

        // 总览
        if (!TYPE_SET.contains(type)){
            throw BizException.build(GlobalErrorCode.INVALID_ARGUMENT,"类型不正确").get();
        }

        // 总数
        Map<String, TotalDTO> countMap = orderAnalyseMapper.selectByIdentitys(startDate, endDate, type)
                .stream().collect(Collectors.toMap(TotalDTO::getDate, s -> s));

        // 非白人数据
        Map<String, TotalDTO> vipMap = orderAnalyseMapper.selectByIdentitys(startDate, endDate, type, CareerLevelType.DS, CareerLevelType.SP)
                .stream().collect(Collectors.toMap(TotalDTO::getDate, s -> s));

        // 白人数据
        Map<String, TotalDTO> newMap = orderAnalyseMapper.selectByIdentitys(startDate, endDate, type, CareerLevelType.RC, CareerLevelType.PW)
                .stream().collect(Collectors.toMap(TotalDTO::getDate, s -> s));


        List<OrderAnalyseVO> result = new ArrayList<>(endDate.compareTo(startDate) + 1);
        while (endDate.compareTo(startDate) >= 0){

            String date = startDate.format(dateTimeFormatter);

            TotalDTO countAll = countMap.getOrDefault(date, new TotalDTO(date,0,BigDecimal.valueOf(0)));
            TotalDTO countNew = newMap.getOrDefault(date, new TotalDTO(date,0,BigDecimal.valueOf(0)));
            TotalDTO countVip = vipMap.getOrDefault(date, new TotalDTO(date,0,BigDecimal.valueOf(0)));

            OrderAnalyseVO analyseVO = createOrderAnalyseVO(date,type);
            analyseVO.setTotal(countAll.getTotal());
            analyseVO.setTotalFee(countAll.getTotalFee());
            analyseVO.setTotalNewOrderCount(countNew.getTotal());
            analyseVO.setTotalVipOrderCount(countVip.getTotal());
            result.add(analyseVO);
            startDate = startDate.plusDays(1);
        }

        return result;
    }

    private OrderAnalyseVO createOrderAnalyseVO(String date,String type){
        OrderAnalyseVO oav = new OrderAnalyseVO(date);
        switch (type){
            case "ALL" :
                oav.setNewRegisteredUserCount(orderAnalyseMapper.selectNewUsersTotalByDay(date));
                return oav;
            case "RESERVE" :
                oav.setReserveCount(orderAnalyseMapper.countReserveByDate(date));
                return oav;
            case "PIECE":
                oav.setOpenPieceCount(orderAnalyseMapper.countPieceByDate(date,1));
                oav.setJoinPieceCount(orderAnalyseMapper.countPieceByDate(date,0));
                return oav;
            default: return oav;
        }
    }

}
