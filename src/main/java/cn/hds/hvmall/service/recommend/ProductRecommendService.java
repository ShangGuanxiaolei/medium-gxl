package cn.hds.hvmall.service.recommend;

import cn.hds.hvmall.base.BaseExecuteResult;
import cn.hds.hvmall.entity.list.ProductSearchResult;
import cn.hds.hvmall.entity.recommend.SelectCondition;
import cn.hds.hvmall.pojo.list.ProductInfo;
import cn.hds.hvmall.pojo.list.RecommendProductInfo;

import java.util.List;

public interface ProductRecommendService {


	//按条件查询商品
	  List<ProductSearchResult> searchProduct(SelectCondition productInfo);

	 //选择商品  批量添加商品
	 Boolean insertProduct(long recommendId,List<Long> productIds);

	//查看已选择的商品
	 BaseExecuteResult<List<ProductSearchResult>> getselectedList(long recommendId,int pageCount,int pageSize);


	//查询商品的数量
	 BaseExecuteResult<Integer> searchRecommendProductCount(long recommendId);

	//查看已选商品的数量
	 BaseExecuteResult<Integer> searchSelectedCount(long recommendId);

	//查询已选商品
	 BaseExecuteResult<List<ProductSearchResult>> getselectedList(long recommendId);

	//删除推荐商品
	 boolean  deleteRecommendProduct(long recommendId,long productId);


}
