package cn.hds.hvmall.service.recommend;

import cn.hds.hvmall.base.BaseExecuteResult;
import cn.hds.hvmall.entity.list.ProductLists;
import cn.hds.hvmall.entity.list.RecommendProductResult;
import cn.hds.hvmall.entity.recommend.Recommend;

import java.util.List;

public interface RecommendService {

	/**
	 * 分页查询推荐
	 */
	BaseExecuteResult<List<Recommend>> getRecommnedList(int pageNum, int pageSize);

	/**
	 * 查询所有选中的商品
	 */
	List<Long> listAllSelected(long recommendId);

	/**
	 * 删除推荐
	 */
	Boolean deleteRecommend(long recommendId);

	/**
	 * 修改推荐
	 */
	Boolean updateRecommend(ProductLists recommend);


	//添加推荐
	Boolean addRecommend(Recommend recommend);

	//复制
	Boolean copyOther(Long id);

	//查询推荐总的条数
	Integer countRecommend();

	//根据推荐id将推荐商品数据回显出来
	BaseExecuteResult<RecommendProductResult> getRecommendProductById(long recommendId,int pageNum,int pageSize);
}
