package cn.hds.hvmall.service.recommend.impl;

import cn.hds.hvmall.base.BaseExecuteResult;
import cn.hds.hvmall.entity.list.ProductLists;
import cn.hds.hvmall.entity.list.ProductSearchResult;
import cn.hds.hvmall.entity.list.RecommendProductResult;
import cn.hds.hvmall.entity.recommend.Recommend;
import cn.hds.hvmall.mapper.recommend.RecommendMapper;
import cn.hds.hvmall.mapper.recommend.RecommendProductMapper;
import cn.hds.hvmall.service.recommend.ProductRecommendService;
import cn.hds.hvmall.service.recommend.RecommendService;
import cn.hds.hvmall.utils.list.GlobalErrorCode;
import cn.hds.hvmall.utils.list.qiniu.BizException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class RecommendServiceImpl implements RecommendService {

  private final RecommendMapper recommendMapper;

  private final ProductRecommendService productRecommendService;

  private final RecommendProductMapper recommendProductMapper;

  @Autowired
  public RecommendServiceImpl(RecommendMapper recommendMapper, ProductRecommendService productRecommendService, RecommendProductMapper recommendProductMapper) {
    this.recommendMapper = recommendMapper;
    this.productRecommendService = productRecommendService;
    this.recommendProductMapper = recommendProductMapper;
  }

  @Override
  public BaseExecuteResult<List<Recommend>> getRecommnedList(int pageNum, int pageSize) {
    if (pageNum < 1) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "入参应该大于0");
    }
    int rowIndex = (pageNum - 1) * pageSize;
    List<Recommend> recommends = recommendMapper.selectRecommendList(rowIndex, pageSize);
    return new BaseExecuteResult<>(1, recommends);
  }

  @Override
  public List<Long> listAllSelected(long recommendId) {
    return recommendMapper.listAllSelected(recommendId);
  }

  /**
   * 删除推荐
   *
   * @param recommendId
   * @return
   */
  @Override
  @Transactional
  public Boolean deleteRecommend(long recommendId) {
    if (recommendMapper.deleteByPrimaryKey(recommendId) > 0) {
      if (recommendMapper.isRecommend(recommendId) > 0) {
        if (recommendProductMapper.deleteById(recommendId) > 0) {
          return true;
        }
        throw new BizException(GlobalErrorCode.UNKNOWN, "删除失败");
      }
      return true;
    }
    throw new BizException(GlobalErrorCode.UNKNOWN, "删除失败");
  }

  @Override
  @Transactional
  public Boolean updateRecommend(ProductLists recommend) {
    if (recommend == null || recommend.getRecommendId() == null) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "推荐不存在");
    }
    if (recommend.getCode() == null || recommend.getCode().toString().length() > 20 || recommend.getCode().length() < 0) {
      throw new BizException(GlobalErrorCode.UNKNOWN, "推荐页面名称应在20字符之内");
    }
    Recommend recommend1 = new Recommend();
    recommend1.setId(recommend.getRecommendId());
    recommend1.setName(recommend.getName());
    recommend1.setCode(recommend.getCode());
    //判断是强推还是普推
      if(1==recommend.getStrongPush()){
        if (recommendMapper.checkCodesStrongUpdate(recommend.getCode(), Long.valueOf(recommend.getRecommendId()))==1) {
          List<Long> list;
          list = recommend.getProductLists();
          recommend1.setNummber(list.size());
          if (recommendMapper.updateByPrimaryKeySelective(recommend1) < 1) {
            throw new BizException(GlobalErrorCode.UNKNOWN, "更新出错");
          }
          if (recommendMapper.isRecommend(recommend.getRecommendId()) > 0) {
            if (recommendProductMapper.deleteById(recommend.getRecommendId()) < 1) {
              throw new BizException(GlobalErrorCode.UNKNOWN, "更新异常");
            }
          }
          if (list.size() > 0) {
            if (recommendProductMapper.batchProduct(list, recommend.getRecommendId()) < 1) {
              throw new BizException(GlobalErrorCode.UNKNOWN, "更新异常");
            }
          }
          return true;
        }
        throw new BizException(GlobalErrorCode.UNKNOWN, "编辑强推页面出错");

      }

      else {
        if (recommendMapper.checkCodesUpdate(recommend.getCode(), Long.valueOf(recommend.getRecommendId())) ==1) {
          List<Long> list;
          list = recommend.getProductLists();
          recommend1.setNummber(list.size());
          if (recommendMapper.updateByPrimaryKeySelective(recommend1) < 1) {
            throw new BizException(GlobalErrorCode.UNKNOWN, "更新出错");
          }
          if (recommendMapper.isRecommend(recommend.getRecommendId()) > 0) {
            if (recommendProductMapper.deleteById(recommend.getRecommendId()) < 1) {
              throw new BizException(GlobalErrorCode.UNKNOWN, "更新异常");
            }
          }
          if (list.size() > 0) {
            if (recommendProductMapper.batchProduct(list, recommend.getRecommendId()) < 1) {
              throw new BizException(GlobalErrorCode.UNKNOWN, "更新异常");
            }
          }
          return true;
        }
        throw new BizException(GlobalErrorCode.UNKNOWN, "编辑普推页面出错");
      }


  }

  @Override
  public Boolean addRecommend(Recommend recommend) {
    if (recommend.getCode().length() > 20) {
      throw new BizException(GlobalErrorCode.UNKNOWN, "推荐页面名称应在20个字符以内");
    }

    if (null!=recommend.getStrongPush()){
      if (recommendMapper.checkCodeStrong(recommend.getCode())<1) {
        return recommendMapper.addRecommend(recommend) > 0;
      } else {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "该强推关联推荐页面已存在");
      }
    }
    else {
      if (recommendMapper.checkCode(recommend.getCode()) < 1) {
        return recommendMapper.addRecommend(recommend) > 0;
      } else {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "该普推关联推荐页面已存在");
      }
    }

  }

  @Override
  public Boolean copyOther(Long id) {
    Recommend recommend = recommendMapper.selectByPrimaryKey(id);
    if (recommend == null) {
      throw new BizException(GlobalErrorCode.UNKNOWN, "复制出错");
    }
    //关联界面置空
    recommend.setCode(null);
    //复制目标的id
    Long rid = recommend.getId();
    if (recommendMapper.addRecommend(recommend) < 1) {
      throw new BizException(GlobalErrorCode.UNKNOWN, "复制出错,请重新尝试");
    }
    //新增id
    Long recommendId = recommend.getId();
    List<Long> recommendProductList = recommendProductMapper.selectById(rid);
    if (recommendProductList == null || recommendProductList.size() == 0) {
      return true;
    }
    return recommendProductMapper.batchProduct(recommendProductList, recommendId) > 0;
  }

  @Override
  public Integer countRecommend() {
    return recommendMapper.count();
  }

  @Override
  public BaseExecuteResult<RecommendProductResult> getRecommendProductById(long recommendId, int pageNum, int pageSize) {
    if (recommendId <= 0) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "推荐id小于0");
    }
    Recommend recommend = recommendMapper.selectByPrimaryKey(recommendId);
    if (recommend == null) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "推荐不存在");
    }
    RecommendProductResult recommendProductResult = new RecommendProductResult();
    recommendProductResult.setName(recommend.getName());
    recommendProductResult.setCode(recommend.getCode());
    BaseExecuteResult<List<ProductSearchResult>> selectedResult = productRecommendService.getselectedList(recommendId, pageNum, pageSize);
    if (selectedResult == null) {
      recommendProductResult.setProductSearchResultList(new ArrayList<>());
    }
    List<ProductSearchResult> result = selectedResult.getResult();
    recommendProductResult.setProductSearchResultList(result);
    return new BaseExecuteResult<>(1, recommendProductResult);
  }


}
