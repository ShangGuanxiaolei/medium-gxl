package cn.hds.hvmall.service.recommend.impl;

import cn.hds.hvmall.base.BaseExecuteResult;
import cn.hds.hvmall.entity.list.ProductSearchResult;
import cn.hds.hvmall.entity.recommend.SelectCondition;
import cn.hds.hvmall.mapper.list.ProductMapper;
import cn.hds.hvmall.mapper.recommend.RecommendProductMapper;
import cn.hds.hvmall.service.recommend.ProductRecommendService;
import cn.hds.hvmall.utils.list.GlobalErrorCode;
import cn.hds.hvmall.utils.list.qiniu.BizException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductRecommendServiceImpl implements ProductRecommendService {


	private final ProductMapper productMapper;

	private final RecommendProductMapper recommendProductMapper;

	@Autowired
	public ProductRecommendServiceImpl(ProductMapper productMapper, RecommendProductMapper recommendProductMapper) {
		this.productMapper = productMapper;
		this.recommendProductMapper = recommendProductMapper;
	}

	/**
	 * 按条件查询商品
	 *
	 * @param
	 * @param
	 * @return
	 */
	@Override
	public List<ProductSearchResult> searchProduct(SelectCondition selectCondition) {

		int pageNumber = 0;
		int pageSize = 0;

		if (selectCondition != null) {
			pageSize = selectCondition.getPageSize();
			pageNumber = (selectCondition.getPageNum() - 1) * selectCondition.getPageSize();
		}
		return productMapper.searchProductResult(selectCondition, pageNumber, pageSize);
	}

	/**
	 * 批量添加
	 *
	 * @param recommendId
	 * @param productIds
	 * @return
	 */

	@Override
	public Boolean insertProduct(long recommendId, List<Long> productIds) {
		if (recommendId == 0 || recommendId < 0) {
			throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "推荐页面ID不能为空");
		}
		if (productIds == null || productIds.size() <= 0) {
			throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "商品Id不能为空");
		}
		return recommendProductMapper.batchProduct(productIds, recommendId) > 0;
	}

	/**
	 * 分页查询已选择的商品
	 *
	 * @param recommendId
	 * @return
	 */
	@Override
	public BaseExecuteResult<List<ProductSearchResult>> getselectedList(long recommendId, int pageCount, int pageSize) {
		BaseExecuteResult<List<ProductSearchResult>> result;
		int pageNumber = ((pageCount - 1) * pageSize);
		//非空校验
		if (recommendId == 0 || recommendId < 0) {
			throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "推荐ID不存在");
		}
		//查询已经选择的商品
		List<ProductSearchResult> productSearchResults;
		try {
			productSearchResults = recommendProductMapper.searchSelectedProduct(recommendId, pageNumber, pageSize);
		} catch (Exception e) {
			throw new RuntimeException("查询已选择商品失败");
		}
		result = new BaseExecuteResult<>();
		result.setResult(productSearchResults);
		return result;
	}

	@Override
	public BaseExecuteResult<Integer> searchRecommendProductCount(long recommendId) {
		BaseExecuteResult<Integer> result = new BaseExecuteResult<>();
		int i = recommendProductMapper.RecommentProductCount(recommendId);
		int res = recommendProductMapper.updateRecommendNummber(i, recommendId);
		if (res != 1) {
			throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "更新推荐商品数量失败");
		}
		int num = 0;
		try {
			num = recommendProductMapper.selectProductNummber(recommendId);
		} catch (Exception e) {
			throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "查询推荐商品数量失败");
		}
		result.setResult(num);
		return result;
	}

	public BaseExecuteResult<Integer> searchSelectedCount(long recommendId) {
		if (recommendId == 0 || recommendId < 0) {
			throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "推荐页面ID不能为空");
		}
		BaseExecuteResult<Integer> result = new BaseExecuteResult<>();
		int count = recommendProductMapper.selectedCount(recommendId);
		result.setResult(count);
		return result;
	}

	@Override
	public BaseExecuteResult<List<ProductSearchResult>> getselectedList(long recommendId) {
		BaseExecuteResult<List<ProductSearchResult>> result = null;
		if (recommendId == 0 || recommendId < 0) {
			throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "推荐ID不存在");
		}
		//查询已经选择的商品
		List<ProductSearchResult> productSearchResults;
		try {
			productSearchResults = recommendProductMapper.SelectedProductById(recommendId);
		} catch (Exception e) {
			throw new RuntimeException("查询已选择商品失败");
		}
		result = new BaseExecuteResult<>();
		result.setResult(productSearchResults);
		return result;
	}

	@Override
	public boolean deleteRecommendProduct(long recommendId, long productId) {
		if (recommendId <= 0 || productId <= 0) {
			throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "推荐id或商品id不合法");
		}
		int result = recommendProductMapper.deleteSeletedProduct(recommendId, productId);
		return result == 1;
	}


}