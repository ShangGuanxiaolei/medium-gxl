package cn.hds.hvmall.service.customer;

import cn.hds.hvmall.base.BaseExecuteResult;
import cn.hds.hvmall.entity.customer.CustomerQuestion;
import cn.hds.hvmall.entity.customer.QuestionTypeVo;

import java.util.List;
import java.util.Map;

public interface CustomerQuestionService {

    List<CustomerQuestion> selectQuestionAll(String typeId,String title);

    List<QuestionTypeVo> selectQueType();

    int deleteQuestion(int id);

    BaseExecuteResult<?> sort(String json);

    boolean insertQuestion(String json);

    boolean updateQuestion(String json);

    BaseExecuteResult<?> selectContentById(String id);

    List<QuestionTypeVo> selectQueTypeDetail(String type);

    boolean deleteQuestionType(int id);

    boolean updateQuestionType(String type,String icon,String id);

    boolean insertQuestionType(String json);

    BaseExecuteResult<?> typeSort(String json);

}
