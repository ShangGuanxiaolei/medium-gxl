package cn.hds.hvmall.service.customer.impl;

import cn.hds.hvmall.base.BaseExecuteResult;
import cn.hds.hvmall.entity.customer.CustomerQuestion;
import cn.hds.hvmall.entity.customer.QuestionTypeVo;
import cn.hds.hvmall.mapper.customer.CustomerQuestionMapper;
import cn.hds.hvmall.service.customer.CustomerQuestionService;
import cn.hds.hvmall.utils.ConstantUtil;
import cn.hds.hvmall.utils.JSONUtil;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import javax.persistence.Id;
import java.util.*;

@Service("customerQuestionService")
public class CustomerQuestionServiceImpl implements CustomerQuestionService {

        private static final Logger logger = LoggerFactory.getLogger(CustomerQuestionServiceImpl.class);


    @Autowired
    private CustomerQuestionMapper customerQuestionMapper;



    @Override
    public List<CustomerQuestion> selectQuestionAll(String typeId,String title) {

        List<CustomerQuestion> customerQuestionList = customerQuestionMapper.selectQuestionAll(typeId,title);
        return customerQuestionList;

    }

    @Override
    public List<QuestionTypeVo> selectQueType() {
        return customerQuestionMapper.selectQueType();
    }

    @Override
    public int deleteQuestion(int id) {
        int a=customerQuestionMapper.deleteQuestion(id);
        return a;
    }

    /**
     * 问题排序
     * @param json
     * @return
     */
    @Override
    @Transactional
    public BaseExecuteResult<?> sort(String json) {
        BaseExecuteResult<Object> result=null;
        try {
            Map<String, Object> map = JSONUtil.toMap(json);
            //当前分类id
            String NowSortNum =map.get("nowSortNum").toString();
            String Id=map.get("id").toString();
            String sort=map.get("sort").toString();
            String typeId=map.get("typeId").toString();

            //下一个分类id
            Integer NextId=0;
            Integer NextSortNum=0;
            //判断themeId的最小值和最大值是否逾界
            Integer count = customerQuestionMapper.selectCount(typeId);

            //计算下个分类id
            List<Integer> IdList=customerQuestionMapper.selectId(typeId);

            List<Integer> NextSortNumList=customerQuestionMapper.selectSortNum(typeId);
            for (int i = 0; i < IdList.size(); i++) {
                Integer DetailId=IdList.get(i);
                String StringSort=DetailId.toString()+".0";

                if (StringSort.equals(Id)) {
                    //当排序为向下的时候
                    if(sort.equals("down")) {
                        NextId = IdList.get(i + 1);
                        NextSortNum=NextSortNumList.get(i+1);
                        break;
                    }
                    //当排序为向上的时候
                    else if(sort.equals("up")){
                        NextId=IdList.get(i-1);
                        NextSortNum=NextSortNumList.get(i-1);
                        break;
                    }
                }
            }

//            if ( < 1 || Integer.parseInt(NowSortNum) > count || NextSortNum < 1 || NextSortNum > count
//                    || NextSortNum.equals(NowSortNum)) {
//                //非法参数
//                result = new BaseExecuteResult<>(ConstantUtil.failed,
//                        ConstantUtil.ResponseError.THEME_ID_ERROR.getCode(),
//                        ConstantUtil.ResponseError.THEME_ID_ERROR.toString());
//            }
            //把编号为themePresent的id设为0
            Integer num = customerQuestionMapper.updateZeroBySortNum(Id,typeId);
            if (num != 1) {
                //update失败
                result = new BaseExecuteResult<>(ConstantUtil.failed,
                        ConstantUtil.ResponseError.THEME_ID_SORT_ERROR.getCode(),
                        ConstantUtil.ResponseError.THEME_ID_SORT_ERROR.toString());
            }
            //再把编号为themePresent的id和themeNext的id对调,后台刷新页面即可看到效果
            Integer num1 = customerQuestionMapper.updateNowSortByNext(NowSortNum,NextId,typeId);
            if (num1 != 1) {
                //update失败
                result = new BaseExecuteResult<>(ConstantUtil.failed,
                        ConstantUtil.ResponseError.THEME_ID_SORT_ERROR.getCode(),
                        ConstantUtil.ResponseError.THEME_ID_SORT_ERROR.toString());
            }
            Integer num2 = customerQuestionMapper.updatNextByZero(NextSortNum,typeId);
            if (num2 != 1) {
                //update失败
                result = new BaseExecuteResult<>(ConstantUtil.failed,
                        ConstantUtil.ResponseError.THEME_ID_SORT_ERROR.getCode(),
                        ConstantUtil.ResponseError.THEME_ID_SORT_ERROR.toString());
            }
            //排序成功
            result = new BaseExecuteResult<>(ConstantUtil.success,
                    ConstantUtil.ResponseMSG.THEME_ID_SORT_OK.getCn());
        }catch (Exception e){
            e.printStackTrace();
            logger.info(ConstantUtil.ERROR_FORMAT, this.getClass().getSimpleName(), "sort", e.getMessage());
            result = new BaseExecuteResult<Object>(
                    ConstantUtil.failed,
                    ConstantUtil.ResponseError.SYS_ERROR.getCode(),
                    ConstantUtil.ResponseError.SYS_ERROR.toString());
            // 针对多条数据操作需要手动开启事务
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        }

        return result;
    }


    /**
     * 添加客服问题
     * @param json
     * @return
     */
    @Override
    public boolean insertQuestion(String json) {
        Map<String ,Object> map=JSONUtil.toMap(json);
        String typeId=map.get("typeId").toString();
        String title=map.get("title").toString();
        String content=map.get("content").toString();
        String maxsort=customerQuestionMapper.selectMaxSortNum(typeId);
        int sort=1;
        int a;
        if(maxsort!=null){
            sort=Integer.parseInt(customerQuestionMapper.selectMaxSortNum(typeId))+1;
             a=customerQuestionMapper.insertQuestion(typeId,title,content,sort);
        }else {

            a = customerQuestionMapper.insertQuestion(typeId, title, content, sort);
        }
        boolean isTrue=true;
        if(a<0){
            isTrue=false;
            return isTrue;
        }
        return isTrue;

    }

    /**
     * 修改客服问题
     * @param json
     * @return
     */
    @Override
    public boolean updateQuestion(String json) {
        Map<String ,Object> map=JSONUtil.toMap(json);
        String typeId=map.get("typeId").toString();
        String title=map.get("title").toString();
        String content=map.get("content").toString();
        String id=map.get("id").toString();
        String typeId2=customerQuestionMapper.typeIdById(id);
        String StringtypeId=typeId2+".0";
        int a;
        if(typeId.equals(StringtypeId)){
             a=customerQuestionMapper.updateQuestion2(typeId, title, content, id);
        }else {
            String maxsort=customerQuestionMapper.selectMaxSortNum(typeId);
            if(maxsort!=null) {
                int MaxSortNum = Integer.parseInt(maxsort) + 1;
                a = customerQuestionMapper.updateQuestion(typeId, MaxSortNum, title, content, id);
            }else{
                int MaxSortNum = Integer.parseInt(maxsort);
                a = customerQuestionMapper.updateQuestion(typeId, MaxSortNum, title, content, id);
            }
        }
        boolean isTrue=true;
        if(a<0){
            isTrue=false;
            return isTrue;
        }
        return isTrue;
    }

    @Override
    public BaseExecuteResult<?> selectContentById(String id){
        BaseExecuteResult<String> result=new BaseExecuteResult<>();
        String content=customerQuestionMapper.selectContentById(id);
        result.setResult(content);
        return result;
    }

    /**
     * 查询客服问题类型
     * @param type
     * @return
     */
    @Override
    public List<QuestionTypeVo> selectQueTypeDetail(String type) {
        return customerQuestionMapper.selectQueTypeDetail(type);
    }

    /**
     * 逻辑删除问题类型
     * @param id
     * @return
     */
    @Override
    public boolean deleteQuestionType(int id) {
        boolean a=false;
        int delete=customerQuestionMapper.deleteQuestionType(id);
        if(delete>0){
            a=true;
            return a;
        }
        return a;
    }

    /**
     * 编辑问题类型信息
     * @param type
     * @param icon
     * @return
     */
    @Override
    public boolean updateQuestionType(String type, String icon,String id) {
        boolean a=false;
        int update=customerQuestionMapper.updateQuestionType(type,icon,id);
        if(update>0){
            a=true;
            return a;
        }
        return a;
    }

    /**
     * 添加问题类型信息
     * @return
     */
    @Override
    public boolean insertQuestionType(String json) {
        boolean a=false;
        Map<String,Object> map=JSONUtil.toMap(json);
        String type=map.get("type").toString();
        String icon=map.get("icon").toString();
        int sort=(customerQuestionMapper.selectMaxSort())+1;
        int insert=customerQuestionMapper.insertQuestionType(type,icon,sort);
        if(insert>0){
            a=true;
            return a;
        }
        return a;
    }


    /**
     * 类型排序
     * @param json
     * @return
     */
    @Override
    @Transactional
    public BaseExecuteResult<?> typeSort(String json) {
        BaseExecuteResult<Object> result=null;
        try {
            Map<String, Object> map = JSONUtil.toMap(json);
            //当前分类id
            String NowSortNum =map.get("nowSort").toString();
            String Id=map.get("id").toString();
            String sort=map.get("sort").toString();

            //下一个分类id
            Integer NextId=0;
            Integer NextSortNum=0;

            //计算下个分类id
            List<Integer> IdList=customerQuestionMapper.selectTypeId();

            List<Integer> NextSortNumList=customerQuestionMapper.selectSort();
            for (int i = 0; i < IdList.size(); i++) {
                Integer DetailId=IdList.get(i);
                String StringSort=DetailId.toString()+".0";

                if (StringSort.equals(Id)) {
                    //当排序为向下的时候
                    if(sort.equals("down")) {
                        NextId = IdList.get(i + 1);
                        NextSortNum=NextSortNumList.get(i+1);
                        break;
                    }
                    //当排序为向上的时候
                    else if(sort.equals("up")){
                        NextId=IdList.get(i-1);
                        NextSortNum=NextSortNumList.get(i-1);
                        break;
                    }
                }
            }

//            if ( < 1 || Integer.parseInt(NowSortNum) > count || NextSortNum < 1 || NextSortNum > count
//                    || NextSortNum.equals(NowSortNum)) {
//                //非法参数
//                result = new BaseExecuteResult<>(ConstantUtil.failed,
//                        ConstantUtil.ResponseError.THEME_ID_ERROR.getCode(),
//                        ConstantUtil.ResponseError.THEME_ID_ERROR.toString());
//            }
            //把编号为themePresent的id设为0
            Integer num = customerQuestionMapper.updateZeroBySort(Id);
            if (num != 1) {
                //update失败
                result = new BaseExecuteResult<>(ConstantUtil.failed,
                        ConstantUtil.ResponseError.THEME_ID_SORT_ERROR.getCode(),
                        ConstantUtil.ResponseError.THEME_ID_SORT_ERROR.toString());
            }
            //再把编号为themePresent的id和themeNext的id对调,后台刷新页面即可看到效果
            Integer num1 = customerQuestionMapper.updateNowSortByNextId(NowSortNum,NextId);
            if (num1 != 1) {
                //update失败
                result = new BaseExecuteResult<>(ConstantUtil.failed,
                        ConstantUtil.ResponseError.THEME_ID_SORT_ERROR.getCode(),
                        ConstantUtil.ResponseError.THEME_ID_SORT_ERROR.toString());
            }
            Integer num2 = customerQuestionMapper.updatNextSortByZero(NextSortNum);
            if (num2 != 1) {
                //update失败
                result = new BaseExecuteResult<>(ConstantUtil.failed,
                        ConstantUtil.ResponseError.THEME_ID_SORT_ERROR.getCode(),
                        ConstantUtil.ResponseError.THEME_ID_SORT_ERROR.toString());
            }
            //排序成功
            result = new BaseExecuteResult<>(ConstantUtil.success,
                    ConstantUtil.ResponseMSG.THEME_ID_SORT_OK.getCn());
        }catch (Exception e){
            e.printStackTrace();
            logger.info(ConstantUtil.ERROR_FORMAT, this.getClass().getSimpleName(), "sort", e.getMessage());
            result = new BaseExecuteResult<Object>(
                    ConstantUtil.failed,
                    ConstantUtil.ResponseError.SYS_ERROR.getCode(),
                    ConstantUtil.ResponseError.SYS_ERROR.toString());
            // 针对多条数据操作需要手动开启事务
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        }

        return result;
    }






}
