package cn.hds.hvmall.service.piece.impl;

import cn.hds.hvmall.entity.piece.XquarkSku;
import cn.hds.hvmall.mapper.piece.XquarkSkuMapper;
import cn.hds.hvmall.pojo.piece.XquarkSkus;
import cn.hds.hvmall.service.piece.XquarkSkuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("xquarkSkuService")
public class XquarkSkuServiceImpl implements XquarkSkuService{

    private final XquarkSkuMapper xquarkSkuMapper;

    @Autowired
    public XquarkSkuServiceImpl(XquarkSkuMapper xquarkSkuMapper) {
        this.xquarkSkuMapper = xquarkSkuMapper;
    }

    @Override
    public List<XquarkSkus> findXquarkSku(XquarkSku xquarkSkuParam) {
        return xquarkSkuMapper.findXquarkSku(xquarkSkuParam);
    }

    @Override
    public boolean updateXquarkSku(Integer amount, String skuCode) {
        return xquarkSkuMapper.updateXquarkSku(amount, skuCode);
    }

    @Override
    public Integer selectAmountByskuCode(String skuCode) {
        return xquarkSkuMapper.selectAmountByskuCode(skuCode);
    }
}
