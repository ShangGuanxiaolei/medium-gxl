package cn.hds.hvmall.service.piece;

import cn.hds.hvmall.entity.piece.PromotionTempStock;

/**
 * 活动库存扣减明细表(promotion_stock_detail)
 * @描述: 拼团 .
 * @程序猿: guoxia .
 * @日期: 2017年3月3日 下午2:29:46 .
 * @版本号: V1.0 .
 */
public interface PromotionTempStockService {
    int insert(PromotionTempStock promotionTempStockParam);
}
