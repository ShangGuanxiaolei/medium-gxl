package cn.hds.hvmall.service.piece.impl;

import cn.hds.hvmall.base.BaseExecuteResult;
import cn.hds.hvmall.entity.piece.GroupSuccessSort;
import cn.hds.hvmall.entity.piece.OrderListVO;
import cn.hds.hvmall.entity.piece.PieceFast;
import cn.hds.hvmall.entity.piece.PromotionTranInfo;
import cn.hds.hvmall.mapper.activity.ActivityMapper;
import cn.hds.hvmall.mapper.piece.PromotionCountMapper;
import cn.hds.hvmall.mapper.piece.PromotionTranInfoMapper;
import cn.hds.hvmall.pojo.activity.ActivityVO;
import cn.hds.hvmall.pojo.list.ExecuteResult;
import cn.hds.hvmall.service.piece.PromotionTranInfoService;
import cn.hds.hvmall.utils.JSONUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service("promotionTranInfoService")
public class PromotionTranInfoServiceImpl implements PromotionTranInfoService {

    @Autowired
    private PromotionTranInfoMapper promotionTranInfoMapper;
    @Autowired
    private PromotionCountMapper promotionCountMapper;
    @Autowired
    private ActivityMapper activityMapper;



    @Override
    public PromotionTranInfo selectTranInfo(String pCode) {
        PromotionTranInfo promotionTranInfo=new PromotionTranInfo();
        promotionTranInfo.setEffectFrom(promotionTranInfoMapper.selectTimeBypCode(pCode).getEffectFrom());
        promotionTranInfo.setEffectTo(promotionTranInfoMapper.selectTimeBypCode(pCode).getEffectTo());
        promotionTranInfo.setpName(promotionTranInfoMapper.selectTimeBypCode(pCode).getpName());
        promotionTranInfo.setCount(promotionTranInfoMapper.selectPiecePeopleCount(pCode));
        PromotionTranInfo pSkuNum=promotionTranInfoMapper.selectStock(pCode);
        if(pSkuNum==null){
            promotionTranInfo.setpSkuNum("0");
            promotionTranInfo.setpUsableSkuNum("0");
        }else {
            promotionTranInfo.setpSkuNum(promotionTranInfoMapper.selectStock(pCode).getpSkuNum());
            promotionTranInfo.setpUsableSkuNum(promotionTranInfoMapper.selectStock(pCode).getpUsableSkuNum());
        }

        promotionTranInfo.setPieceFailCount(promotionTranInfoMapper.selectPieceStatusAndCount(pCode));
        promotionTranInfo.setSuccessPieceCount(promotionTranInfoMapper.selectSuccessPieceCount(pCode));
        List<OrderListVO> list=promotionTranInfoMapper.selectOrderCountAndStatus(pCode);
        int paid=0;
        int shipped=0;
        int postSale=0;
        for (int i = 0; i < list.size(); i++) {
            String status=list.get(i).getStatus();
            if(status.equals("SUBMITTED")){
                promotionTranInfo.setSubmit(list.get(i).getCount());
            }
            if(status.equals("PAIDNOSTOCK")){
                promotionTranInfo.setPaidNoStock(list.get(i).getCount());
            }
            if(status.equals("PAID")){
                paid+=list.get(i).getCount();
            }
            if(status.equals("DELIVERY")){
                paid+=list.get(i).getCount();
                shipped+=list.get(i).getCount();
            }
            if(status.equals("REISSUING")){
                paid+=list.get(i).getCount();
                postSale+=list.get(i).getCount();
                shipped+=list.get(i).getCount();
            }
            if(status.equals("SHIPPED")){
                paid+=list.get(i).getCount();
                shipped+=list.get(i).getCount();
            }
            if(status.equals("SUCCESS")){
                paid+=list.get(i).getCount();
                shipped+=list.get(i).getCount();
            }
            if(status.equals("COMMENT")){
                paid+=list.get(i).getCount();
                shipped+=list.get(i).getCount();
            }
            if(status.equals("CANCELLED")){
                promotionTranInfo.setCancelled(list.get(i).getCount());
            }

        }
        promotionTranInfo.setSuccessGroup(paid);
        promotionTranInfo.setShipped(shipped);
        promotionTranInfo.setPostSale(postSale);

        promotionTranInfo.setOrderList(promotionTranInfoMapper.selectOrderCountAndStatus(pCode));
        promotionTranInfo.setPieceSuccess(promotionTranInfoMapper.selectGroupSuccess(pCode));
        promotionTranInfo.setFast(promotionTranInfoMapper.selectFastSuccess(pCode));
        promotionTranInfo.setHourRanks(promotionTranInfoMapper.selectHourTime(pCode));
        promotionTranInfo.setSuccessCount(promotionTranInfoMapper.selectSuccessCount(pCode));
        promotionTranInfo.setPaidOrderCount(promotionTranInfoMapper.selectPaidOrder(pCode));
        promotionTranInfo.setPieceSuccessOrderCount(promotionCountMapper.selectPieceSuccessOrderCount(pCode));
        promotionTranInfo.setFailOrderCount(promotionTranInfoMapper.selectFailOrder(pCode));
        return promotionTranInfo;
    }

    @Override
    public BaseExecuteResult<List<ActivityVO>> selectTranInfoByOrderOrCpId(String json) {

        ExecuteResult<List<ActivityVO>> result = new ExecuteResult<>();
        Map<String, Object> map = JSONUtil.toMap(json);
        String pCode=map.get("pCode").toString();
        String  cpid = "";
        String  orderNo="";
        if(null!=map.get("cpid")){
            cpid= map.get("cpid").toString();
        }
        if(null!=map.get("orderNo")){
            orderNo= map.get("orderNo").toString();
        }
        List<ActivityVO> list = promotionTranInfoMapper.selectTranInfoByOrderOrCpId(pCode,cpid,orderNo);

        if(list.size()==0){
            //throw new BizException(GlobalErrorCode.NOT_FOUND,"未找到该项目");
            result.setIsSuccess(0);
        }else{
            result.setIsSuccess(1);
        }
        for (int i = 0; i <list.size() ; i++) {
            list.get(i).setProductName(activityMapper.searchSkuname(list.get(i).getPieceGroupTranCode()));
            list.get(i).setGroupOpenMemberNickName(activityMapper.searchUserName(list.get(i).getGroupOpenMemberId()+""));
            list.get(i).setGroupHeadIdName(activityMapper.searchUserName(list.get(i).getGroupHeadId()+""));
            //成团状态才有成团时间 赋值成团时间
            if(list.get(i).getPieceStatus().equals("2")){
                list.get(i).setPromotionPgInfoUpdatedAt(activityMapper.successGroupTime(list.get(i).getPieceGroupTranCode()));
            }
     /* if(activityMapper.searchPtTime(list.get(i).getPieceGroupTranCode()).equals(activityInfo.getOpenTime())){
          list1.set(i,list.get(i));
      }*/
        }
        result.setResult(list);
        return result;
    }
}
