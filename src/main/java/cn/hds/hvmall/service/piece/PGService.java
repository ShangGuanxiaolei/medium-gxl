package cn.hds.hvmall.service.piece;

import cn.hds.hvmall.base.BaseExecuteResult;
import cn.hds.hvmall.entity.list.ProductAmountInfo;
import cn.hds.hvmall.entity.piece.EditGroupBaseInfoVO;
import cn.hds.hvmall.entity.piece.GroupAmount;
import cn.hds.hvmall.entity.piece.GroupBaseInfoVO;
import cn.hds.hvmall.pojo.piece.BackPGThreeVO;
import cn.hds.hvmall.pojo.piece.PGVO;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author qiuchuyi
 * @date 2018/9/3 16:05
 */
@Service
public interface PGService {
  /**
   * 创建订单接口
   *
   * @param pgVO
   */
  void create(PGVO pgVO);

  /**
   * 查询拼团活动列表接口
   *
   * @param selectConditions
   * @return
   */
  BaseExecuteResult<List<BackPGThreeVO>> queryBackPGVOList(Map<String, Object> selectConditions);

  /**
   * 更改活动状态
   *
   * @param pCode
   * @param statusCode
   * @return
   */
  BaseExecuteResult<Object> publishPieceGroup(String pCode, String statusCode);

  /**
   * 根据pCode查询拼团活动信息
   *
   * @param pCode
   * @return
   */
  BaseExecuteResult<List<GroupBaseInfoVO>> select(String pCode);

  /**
   * 根据pCode和传入的参数对象update相应字段
   *
   * @param info
   * @return
   */
  BaseExecuteResult<Object> update(EditGroupBaseInfoVO info);

  /**
   * 根据pCode获取拼团信息
   *
   * @param pCode
   * @return
   */
  PGVO getInfo(String pCode);

  String updateInfo(PGVO pgvo);


  boolean updateActivityExclusiveStatus(String pCode);

  /**
   * 根据pCode获取信息
   */
  List<GroupAmount> getGroupAmount(String pCode);

  /**
   * 修改库存
   */
  boolean updateAmount(List<ProductAmountInfo> infoList);

  /**
   * 删除拼团活动
   */

  boolean deleteActivity(String pCode);
}
