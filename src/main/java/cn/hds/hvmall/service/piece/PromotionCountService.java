package cn.hds.hvmall.service.piece;

import cn.hds.hvmall.entity.piece.PieceSuccess;
import cn.hds.hvmall.entity.piece.PieceSuccessOrderCount;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author LiHaoYang
 * @ClassName PromotionCountService
 * @date 2018/12/10 0010
 */

public interface PromotionCountService {

    /**
     *成团总数
     * @param pCode
     * @return
     */
    int selectPieceSuccessCount(String pCode);

    /**
     *成团总订单数
     * @param pCode
     * @return
     */
    int selectPieceSuccessOrderCount(String pCode);

    /**
     *几人团成团数
     * @param pCode
     * @return
     */
    List<PieceSuccess> selectPieceSuccessList(String pCode);

    /**
     *几人团订单数
     * @param pCode
     * @return
     */
    List<PieceSuccessOrderCount> selectPieceSuccessOrderList(String pCode);

    /**
     *失败团总数
     * @param pCode
     * @return
     */
    int selectPieceFailCount(String pCode);


    /**
     *失败订单数
     * @param pCode
     * @return
     */
    int selectPieceFailOrderCount(String pCode);


}
