package cn.hds.hvmall.service.piece.impl;

import cn.hds.hvmall.entity.piece.PieceSuccess;
import cn.hds.hvmall.entity.piece.PieceSuccessOrderCount;
import cn.hds.hvmall.mapper.piece.PromotionCountMapper;
import cn.hds.hvmall.service.piece.PromotionCountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service("promotionCountService")
public class PromotionCountServiceImpl implements PromotionCountService {

    @Autowired
    private PromotionCountMapper promotionCountMapper;

    @Override
    public int selectPieceSuccessCount(String pCode) {
        return promotionCountMapper.selectPieceSuccessCount(pCode);
    }

    @Override
    public int selectPieceSuccessOrderCount(String pCode) {
        return promotionCountMapper.selectPieceSuccessOrderCount(pCode);
    }

    @Override
    public List<PieceSuccess> selectPieceSuccessList(String pCode) {
        return promotionCountMapper.selectPieceSuccessList(pCode);
    }

    @Override
    public List<PieceSuccessOrderCount> selectPieceSuccessOrderList(String pCode) {
        return promotionCountMapper.selectPieceSuccessOrderList(pCode);
    }

    @Override
    public int selectPieceFailCount(String pCode) {
        return promotionCountMapper.selectPieceFailCount(pCode);
    }

    @Override
    public int selectPieceFailOrderCount(String pCode) {
        return promotionCountMapper.selectPieceFailOrderCount(pCode);
    }
}
