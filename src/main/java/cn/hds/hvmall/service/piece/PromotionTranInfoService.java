package cn.hds.hvmall.service.piece;

import cn.hds.hvmall.base.BaseExecuteResult;
import cn.hds.hvmall.entity.piece.GroupSuccessSort;
import cn.hds.hvmall.entity.piece.PieceFast;
import cn.hds.hvmall.entity.piece.PromotionTranInfo;
import cn.hds.hvmall.pojo.activity.ActivityVO;
import cn.hds.hvmall.pojo.list.ExecuteResult;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PromotionTranInfoService {


   PromotionTranInfo selectTranInfo(String pCode);
   /**
    * 根据订单号或cpid查询团的信息
    * @return
    */

   BaseExecuteResult<List<ActivityVO>> selectTranInfoByOrderOrCpId(String json);

}
