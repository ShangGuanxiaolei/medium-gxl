package cn.hds.hvmall.service.piece.impl;

import cn.hds.hvmall.entity.piece.PromotionTempStock;
import cn.hds.hvmall.mapper.piece.PTempStockMapper;
import cn.hds.hvmall.service.piece.PromotionTempStockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 9-22修改
 */
@Service("promotionTempStockService")
public class PromotionTempStockServiceImpl implements PromotionTempStockService {

    private final PTempStockMapper pTempStockMapper;

    @Autowired
    public PromotionTempStockServiceImpl(PTempStockMapper pTempStockMapper) {
        this.pTempStockMapper = pTempStockMapper;
    }

    @Override
    public int insert(PromotionTempStock promotionTempStockParam){
        return pTempStockMapper.insert(promotionTempStockParam);
    }

}
