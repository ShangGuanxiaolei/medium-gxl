package cn.hds.hvmall.service.piece;

import cn.hds.hvmall.entity.piece.XquarkSku;
import cn.hds.hvmall.pojo.piece.XquarkSkus;

import java.util.List;

public interface XquarkSkuService {
    //获取商品实体
    List<XquarkSkus> findXquarkSku(XquarkSku xquarkSkuParam);
    //更新商品库存
    boolean updateXquarkSku(Integer amount, String skuCode);

    /**
     * 根据skuCode查询商品库存
     * @param skuCode skuCode
     * @return Integer
     */
    Integer selectAmountByskuCode(String skuCode);
}
