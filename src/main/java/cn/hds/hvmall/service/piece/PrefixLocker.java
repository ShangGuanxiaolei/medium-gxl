package cn.hds.hvmall.service.piece;

import cn.hds.hvmall.entity.piece.PrefixLockerBean;
import cn.hds.hvmall.entity.piece.PromotionStockDetail;
import cn.hds.hvmall.entity.piece.PromotionTempStock;
import cn.hds.hvmall.entity.piece.XquarkSku;
import cn.hds.hvmall.pojo.piece.XquarkSkus;
import cn.hds.hvmall.utils.list.GlobalErrorCode;
import cn.hds.hvmall.utils.list.qiniu.BizException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
public class PrefixLocker {
  private static final Logger logger = LoggerFactory.getLogger(PrefixLocker.class);

  private final XquarkSkuService xquarkSkuService;
  private final PromotionStockDetailService promotionStockDetailService;
  private final PromotionTempStockService promotionTempStockService;

  @Autowired
  public PrefixLocker(XquarkSkuService xquarkSkuService, PromotionStockDetailService promotionStockDetailService,
                      PromotionTempStockService promotionTempStockService) {
    this.xquarkSkuService = xquarkSkuService;
    this.promotionStockDetailService = promotionStockDetailService;
    this.promotionTempStockService = promotionTempStockService;
  }

  /*人工预锁库存*/
  @Transactional
  public boolean prefixLocker(PrefixLockerBean prefixLockerBean) {
    String skuCode = prefixLockerBean.getSkuCode();
    int pSkuNum = prefixLockerBean.getP_sku_num();
    String pCode = prefixLockerBean.getP_code();
    if (null == pCode || null == skuCode || pSkuNum <= 0) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "活动商品SKU配置参数异常，请检查数据");
    }

    //汉薇商城查询商品库存
    Integer amount = xquarkSkuService.selectAmountByskuCode(skuCode);
    if (null == amount || amount <= 0 || amount < pSkuNum) {
      logger.info("商品唯一标识码是" + skuCode + "的商品库存不足");
      return false;
    }

    int skuAmount = amount - pSkuNum;
    xquarkSkuService.updateXquarkSku(skuAmount, skuCode);
    logger.info("更新汉薇商城商品库存");

    PromotionTempStock promotionTempStockParam = new PromotionTempStock();
    promotionTempStockParam.setP_code(pCode);//活动编码
    promotionTempStockParam.setSku_code(skuCode);//Sku编码(商品唯一标识)
    promotionTempStockParam.setP_sku_num(pSkuNum);//活动Sku数量
    promotionTempStockParam.setP_usable_sku_num(pSkuNum);//可用Sku数量
    promotionTempStockService.insert(promotionTempStockParam);
    logger.info("保存活动库存");

    PromotionStockDetail promotionStockDetailParam = new PromotionStockDetail();
    promotionStockDetailParam.setP_code(pCode);
    promotionStockDetailParam.setSku_code(skuCode);
    promotionStockDetailParam.setTran_sku_num(pSkuNum);//交易sku数量
    promotionStockDetailParam.setTran_type(3);//交易类型3：人工预锁库存
    promotionStockDetailService.insert(promotionStockDetailParam);
    logger.info("记录活动库存扣减明细");
    return true;
  }
}

