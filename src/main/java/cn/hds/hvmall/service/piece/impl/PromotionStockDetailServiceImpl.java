package cn.hds.hvmall.service.piece.impl;

import cn.hds.hvmall.entity.piece.PromotionStockDetail;
import cn.hds.hvmall.mapper.piece.PromotionStockDetailMapper;
import cn.hds.hvmall.service.piece.PromotionStockDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PromotionStockDetailServiceImpl implements PromotionStockDetailService{
    private final PromotionStockDetailMapper promotionStockDetailMapper;

    @Autowired
    public PromotionStockDetailServiceImpl(PromotionStockDetailMapper promotionStockDetailMapper) {
        this.promotionStockDetailMapper = promotionStockDetailMapper;
    }

    @Override
    public int insert(PromotionStockDetail promotionStockDetailParam) {
        return promotionStockDetailMapper.insert(promotionStockDetailParam);
    }
}
