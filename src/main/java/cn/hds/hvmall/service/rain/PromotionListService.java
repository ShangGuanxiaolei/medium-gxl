package cn.hds.hvmall.service.rain;

import cn.hds.hvmall.entity.rain.RainPromotionForm;
import cn.hds.hvmall.entity.rain.RainResult;

public interface PromotionListService {

	/**
	 * 添加红包雨活动
	 * @param showForm
	 * @return
	 */
  Boolean addPromotionList(RainPromotionForm showForm);

	/**
	 * 更新红包雨活动信息
	 */
	boolean updatePromotionPacketRain(RainPromotionForm showForm);

	/**
	 * 删除红包雨活动相关信息
	 */
	boolean deletePromotionById(Long promotionId);

	/**
	 * 红包雨信息数据回显
	 */
	RainResult getRainDetailMessage(Long promotionId);


}
