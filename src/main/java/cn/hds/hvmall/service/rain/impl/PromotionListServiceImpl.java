package cn.hds.hvmall.service.rain.impl;

import cn.hds.hvmall.entity.rain.*;
import cn.hds.hvmall.mapper.rain.PromotionListMapper;
import cn.hds.hvmall.mapper.rain.RainTimeMapper;
import cn.hds.hvmall.mapper.rain.RedRainMapper;
import cn.hds.hvmall.service.grandsale.GrandSaleModuleService;
import cn.hds.hvmall.service.rain.PromotionListService;
import cn.hds.hvmall.type.PromotionType;
import cn.hds.hvmall.utils.list.GlobalErrorCode;
import cn.hds.hvmall.utils.list.qiniu.BizException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@Service
public class PromotionListServiceImpl implements PromotionListService {

    private final PromotionListMapper promotionListMapper;
    private final RedRainMapper rainMapper;
    private final RainTimeMapper rainTimeMapper;
    private final GrandSaleModuleService grandSaleModuleService;

    @Autowired
    public PromotionListServiceImpl(PromotionListMapper promotionListMapper,
                                    RedRainMapper rainMapper,
                                    RainTimeMapper rainTimeMapper,
                                    GrandSaleModuleService grandSaleModuleService
    ) {
        this.promotionListMapper = promotionListMapper;
        this.rainMapper = rainMapper;
        this.rainTimeMapper = rainTimeMapper;
        this.grandSaleModuleService = grandSaleModuleService;
    }

    @Override
    @Transactional
    public Boolean addPromotionList(RainPromotionForm showForm) {
        if (showForm == null) {
            throw new BizException(GlobalErrorCode.NOT_FOUND, "红包雨信息不能为空");
        }
        //查询红包雨所有场次时间
        List<RainTimResult> rainTimeList = promotionListMapper.getUnClosedTimeList(0);
        if (rainTimeList.size() != 0) {
            for (RainTimResult rainTimResult : rainTimeList) {
                if ((showForm.getTriggerStartTime().getTime() <= rainTimResult.getTriggerEndTime().getTime() &&
                        showForm.getTriggerStartTime().getTime() >= rainTimResult.getTriggerStartTime().getTime()) ||
                        (showForm.getTriggerEndTime().getTime() <= rainTimResult.getTriggerEndTime().getTime() &&
                                showForm.getTriggerEndTime().getTime() >= rainTimResult.getTriggerStartTime().getTime())) {
                    throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "添加红包雨活动时间不能交叉");
                }
            }
        }
        if (promotionListMapper.insertRain(showForm) <= 0) {
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "添加红包雨活动基本信息失败");
        }
        Long promotionId = showForm.getId();
        List<RainConfigListBean> rainConfigList = showForm.getRainConfigList();
        for (RainConfigListBean rain : rainConfigList) {
            rain.setPromotionId(promotionId);
            if (rainMapper.insertPromotionPacketRain(rain) <= 0) {
                throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "添加红包雨活动德分配置失败");
            }
            Long rainId = rain.getRainId();
            List<PromotionPacketRainTime> rainTimeConfig = rain.getRainTimeConfig();
            for (PromotionPacketRainTime rainTime : rainTimeConfig) {
                rainTime.setRainId(rainId);
                if (rainTimeMapper.insertPacketRainTime(rainTime) <= 0) {
                    throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "添加红包雨场次时间失败");
                }
            }
        }

        // 将红包雨关联到当前主会场
        grandSaleModuleService.bindGrand(PromotionType.PACKET_RAIN,promotionId);

        return true;
    }


    @Override
    @Transactional
    public boolean updatePromotionPacketRain(RainPromotionForm showForm) {
        if (showForm == null || showForm.getId() <= 0) {
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "传入的活动信息id不正确");
        }
        // 更新主活动
        if (promotionListMapper.updatePromotionList(showForm) <= 0) {
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "更新活动基本信息失败");
        }
        // 每一天的红包雨配置
        List<RainConfigListBean> rainConfigList = showForm.getRainConfigList();
        if (!CollectionUtils.isEmpty(rainConfigList)) {
            for (RainConfigListBean rainConfigListBean : rainConfigList) {
                rainConfigListBean.setPromotionId(showForm.getId());
                if (rainMapper.updateRed(rainConfigListBean) <= 0) {
                    throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "更新红包雨活动失败");
                }
                List<PromotionPacketRainTime> rainTimeConfig = rainConfigListBean.getRainTimeConfig();
                // 查询所有的活动id
                int delRet = rainTimeMapper.deleteByRainId(rainConfigListBean.getRainId());
                if (delRet <= 0) {
                    throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "保存红包雨失败");
                }
                rainTimeConfig.forEach(rt -> {
                    rt.setId(null);
                    rt.setRainId(rainConfigListBean.getRainId());
                    rainTimeMapper.insertPacketRainTime(rt);
                });
            }
        }
        return true;
    }

    @Override
    @Transactional
    public boolean deletePromotionById(Long promotionId) {
        if (promotionId <= 0) {
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "活动id不存在");
        }
        // 根据活动id查询红包雨当天配置id
        List<Long> packetRainListId = rainMapper.selectIdByPromotionListId(promotionId);
        for (Long packetRainId : packetRainListId) {
            // 直接删除红包雨场次时间的配置
            if (rainTimeMapper.deletePacketRainTimeByRainId(packetRainId) <= 0) {
                throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "删除红包雨场次时间失败");
            }
        }
        if (promotionListMapper.deleteById(promotionId) <= 0 ||
                rainMapper.deleteByPromotionId(promotionId) <= 0) {
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "删除红包雨活动失败");
        }
        return true;
    }

    @Override
    public RainResult getRainDetailMessage(Long promotionId) {
        if (promotionId <= 0) {
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "活动id不存在");
        }
        RainResult rainResult = new RainResult();
        PromotionPacketListVO promotionPacketListVO = promotionListMapper.findById(promotionId);
        List<RainConfigListBean> rainConfigListBeans = rainMapper.selectById(promotionId);
        List<RainConfigListBean> rainList = new ArrayList<>();
        for (RainConfigListBean rainConfigListBean : rainConfigListBeans) {
            Long rainId = rainConfigListBean.getRainId();
            List<PromotionPacketRainTime> rainTimeList = rainTimeMapper.selectByListId(rainId);
            rainConfigListBean.setRainTimeConfig(rainTimeList);
            rainList.add(rainConfigListBean);
        }
        rainResult.setPromotionPacketListVO(promotionPacketListVO);
        promotionPacketListVO.setRainConfigList(rainList);
        return rainResult;
    }

}
