package cn.hds.hvmall.service.tovip.impl;

import cn.hds.hvmall.mapper.tovip.PromotionToVipMapper;
import cn.hds.hvmall.pojo.tovip.PromotionToVip;
import cn.hds.hvmall.pojo.tovip.PromotionToVipType;
import cn.hds.hvmall.service.tovip.PromotionToVipService;
import cn.hds.hvmall.utils.list.GlobalErrorCode;
import cn.hds.hvmall.utils.list.qiniu.BizException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;

@Service
public class PromotionToVipServiceImpl implements PromotionToVipService {
  @Autowired
  private PromotionToVipMapper promotionToVipMapper;

  @Override
  public List<PromotionToVip> selectList(Pageable page) {
    List<PromotionToVip> promotionList = promotionToVipMapper.selectList(page);
    if (null != promotionList && !promotionList.isEmpty()) {
      return promotionList;
    } else {
      return new ArrayList<>();
    }
  }

  @Override
  public Boolean updateRule(PromotionToVip promotionToVip) {
    return promotionToVipMapper.updateByPrimaryKey(notNull(promotionToVip)) > 0;
  }

  @Override
  public Boolean updateAndUse(PromotionToVip promotionToVip) {
    return promotionToVipMapper.updateAndUse(notNull(promotionToVip)) > 0;
  }

  @Override
  public Boolean changeState(PromotionToVip promotionToVip) {
    return promotionToVipMapper.changeState(promotionToVip) > 0;
  }

  @Override
  public Boolean addRule(PromotionToVip promotionToVip) {
    if (promotionToVipMapper.isExist(promotionToVip.getType()) < 1){
      return promotionToVipMapper.insert(notNull(promotionToVip)) > 0;
    }
    throw new BizException(GlobalErrorCode.UNKNOWN,"亲,已存在同类配置.如需改动请编辑!!!");
  }

  @Override
  public PromotionToVip loadById(String id) {
    return promotionToVipMapper.loadById(Long.valueOf(id));
  }

  private PromotionToVip notNull(PromotionToVip promotionToVip){
    if (promotionToVip.getTitle().isEmpty()) {
      throw new BizException(GlobalErrorCode.UNKNOWN,"活动标题不能为空");
    }
    if (promotionToVip.getDescribes().isEmpty()) {
      throw new BizException(GlobalErrorCode.UNKNOWN,"活动描述不能为空");
    }
    if (promotionToVip.getEffectiveBegin() == null || promotionToVip.getEffectiveEnd() == null) {
      throw new BizException(GlobalErrorCode.UNKNOWN, "有效期时间不能为空");
    }
    if (promotionToVip.getToNumber() == null){
      throw new BizException(GlobalErrorCode.UNKNOWN,"亲，你的成团次数还未填写");
    }
    return promotionToVip;
  }
}
