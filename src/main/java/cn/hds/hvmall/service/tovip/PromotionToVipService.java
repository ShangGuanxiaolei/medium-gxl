package cn.hds.hvmall.service.tovip;


import cn.hds.hvmall.pojo.tovip.PromotionToVip;
import org.springframework.data.domain.Pageable;
import java.util.List;

public interface PromotionToVipService {
    List<PromotionToVip> selectList(Pageable page);

    Boolean updateRule(PromotionToVip promotionToVip);

    Boolean updateAndUse(PromotionToVip promotionToVip);

    Boolean changeState(PromotionToVip promotionToVip);

    Boolean addRule(PromotionToVip promotionToVip);

    PromotionToVip loadById(String id);
}
