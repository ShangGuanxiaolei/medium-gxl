package cn.hds.hvmall.service;

import cn.hds.hvmall.entity.BaseEntityImpl;
import cn.hds.hvmall.utils.Transformer;
import cn.hds.hvmall.utils.list.GlobalErrorCode;
import cn.hds.hvmall.utils.list.qiniu.BizException;
import com.google.common.base.Preconditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * @author gxl
 * @version 1.0.0 $ 2019/4/25
 */
public interface UpdateAble<T extends BaseEntityImpl> {

    Logger LOGGER = LoggerFactory.getLogger(UpdateAble.class);

    /**
     * 更新基础实体
     * 使用默认的 {@link UpdateAble#update(BaseEntityImpl)} update更新
     *
     * @param source   源实体
     * @param clazz    T的字节码类型, 用来执行反射复制
     * @param modifier 属性修改方法
     * @return 更新后的实体
     */
    default T update(T source, Class<T> clazz, Consumer<T> modifier) {
        return update(source, clazz, modifier, this::update);
    }

    /**
     * 更新基础实体
     * 同时执行保存操作
     *
     * @param source   源实体
     * @param clazz    T的字节码类型, 用来执行反射复制
     * @param modifier 属性修改方法
     * @param updater  更新方法
     * @return 复制后的实体
     */
    default T update(T source, Class<T> clazz, Consumer<T> modifier, Function<T, Boolean> updater) {
        Preconditions.checkNotNull(source, "源对象不能为空");
        final T dest = Transformer.fromBean(source, clazz);
        if (modifier != null) {
            // 在此处修改自定义的属性
            modifier.accept(dest);
        }
        if (updater == null) {
            LOGGER.warn("更新实体方法未定义, 不执行保存方法");
            return dest;
        }
        final Boolean ret = updater.apply(dest);
        if (!ret) {
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "数据修改失败");
        }
        LOGGER.info("对象修改成功 {} -> {}", source, dest);
        return dest;
    }

    /**
     * 更新属性, 目标service必须实现改方法
     * @param t 待更新对象
     * @return 更新结果
     */
    boolean update(T t);

}
