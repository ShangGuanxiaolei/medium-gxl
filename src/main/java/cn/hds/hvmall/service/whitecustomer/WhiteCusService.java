package cn.hds.hvmall.service.whitecustomer;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

import cn.hds.hvmall.pojo.whitecustomer.WhiteCus;
import cn.hds.hvmall.pojo.whitecustomer.WhiteListExcelVo;

/**
 * Created with IDEA
 * author:Yarm.Yang
 * Date:2019/1/15
 * Time:18:20
 * Des:白人管理业务接口
 */
public interface WhiteCusService {
    /**
     *
     * @param type
     * @param ws
     * @return
     */
    int insert(String type, WhiteCus ws);

    int updateWhiteStatus(WhiteCus whiteCus);

    int deleteByCpId(Long cpId);

    int update(String type, WhiteCus ws);

    List<WhiteCus> selectByCpIdAndPcode(Long cpId, String pCode);

    Map<String, Object> select(Pageable pageable, String pCode,Long cpId);

    boolean checkCpIdAndPcodeExist(Long cpId, String pCode);

    int getExcelData(Workbook work,String pCode);

    /**
     * 改变大会状态
     * @param whiteCus
     * @return
     */
    int updatePromotionStatus(WhiteCus whiteCus);
}
