package cn.hds.hvmall.service.whitecustomer.impl;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.hds.hvmall.entity.whitecustomer.PromotionWhiteListVo;
import cn.hds.hvmall.mapper.whitecustomer.PromotionWhiteListMapper;
import cn.hds.hvmall.pojo.whitecustomer.WhiteCus;
import cn.hds.hvmall.pojo.whitecustomer.WhiteListExcelVo;
import cn.hds.hvmall.service.whitecustomer.WhiteCusService;

/**
 * Created with IDEA
 * author:Yarm.Yang
 * Date:2019/1/15
 * Time:18:20
 * Des:实现类
 */
@Service
public class WhiteCusServiceImpl  implements WhiteCusService {

    private static final Long IMAGE_CPID = 9999999L; //默认7个9
    @Autowired
    private PromotionWhiteListMapper promotionWhiteListMapper;

    @Override
    public int insert(String type, WhiteCus ws) {
        PromotionWhiteListVo promotionWhiteListVo = new PromotionWhiteListVo();
        promotionWhiteListVo.setCpId(Long.parseLong(ws.getCpId()));
        if ("group".equals(type)){
            promotionWhiteListVo.setpCode("piece");//拼团默认是piece
        }else {
            promotionWhiteListVo.setpCode(ws.getpCode());
        }
        if(ws.getStatus() == null){
            promotionWhiteListVo.setStatus(1);//若为空默认状态启动
        }
        return this.promotionWhiteListMapper.insert(promotionWhiteListVo);
    }

    @Override
    public int updateWhiteStatus(WhiteCus whiteCus) {
        PromotionWhiteListVo promotionWhiteListVo = new PromotionWhiteListVo();
        promotionWhiteListVo.setStatus(whiteCus.getStatus());
        promotionWhiteListVo.setCpId(Long.parseLong(whiteCus.getCpId()));
        promotionWhiteListVo.setpCode(whiteCus.getpCode());
        return this.promotionWhiteListMapper.updateStatusByCpIdAndPcode(promotionWhiteListVo);
    }

    @Override
    public int deleteByCpId(Long cpId) {//TODO 少pCode
        PromotionWhiteListVo promotionWhiteListVo = new PromotionWhiteListVo();
        promotionWhiteListVo.setStatus(0);//删除更新为0
        promotionWhiteListVo.setCpId(cpId);
        return this.promotionWhiteListMapper.updateStatusByCpIdAndPcode(promotionWhiteListVo);
    }

    @Override
    public int update(String type, WhiteCus ws) {
        PromotionWhiteListVo promotionWhiteListVo = new PromotionWhiteListVo();
        promotionWhiteListVo.setCpId(Long.parseLong(ws.getCpId()));

        if ("group".equals(type)){
            promotionWhiteListVo.setpCode("piece");//拼团默认是piece
        }else {
            promotionWhiteListVo.setpCode(ws.getpCode());
        }

        if(ws.getStatus() == null){
            promotionWhiteListVo.setStatus(1);//若为空默认状态启动
        }else {
            promotionWhiteListVo.setStatus(ws.getStatus());
        }

        return this.promotionWhiteListMapper.update(promotionWhiteListVo);
    }

    @Override
    public List<WhiteCus> selectByCpIdAndPcode(Long cpId, String pCode) {
        PromotionWhiteListVo p = new PromotionWhiteListVo();
        List<WhiteCus> listWc = new  ArrayList<>();
        p.setCpId(cpId);
        p.setpCode(pCode);
        List<PromotionWhiteListVo> pWlVoList = this.promotionWhiteListMapper.selectByCpIdAndPcode(p);

        if(pWlVoList != null && !pWlVoList.isEmpty()){
            for (PromotionWhiteListVo pWlVo: pWlVoList){
                WhiteCus wc = this.setPromotionWhiteListVoToWhiteCus(pWlVo);
                if(wc != null){
                    listWc.add(wc);
                }
            }
        }
        return listWc;
    }

    @Override
    public Map<String, Object> select(Pageable pageable,String pCode,Long cpId) {
        Map<String, Object> map = new HashMap<>();
        //若cpId不为空说明是单个查询
        if(cpId != null){
            List<WhiteCus> whiteCusList = this.selectByCpIdAndPcode(cpId, pCode);
            if(whiteCusList != null){
                map.put("total",1);//总条数
                map.put("pageInfo",whiteCusList);
                return map;
            }
            map.put("total",0);//总条数
            map.put("pageInfo","");
            return map;
        }
        //以下分页
        //计算页数
        int total = 0;
        List<PromotionWhiteListVo> listTemp = this.promotionWhiteListMapper.select(null, pCode);
        if (listTemp != null){
            total= listTemp.size();
        }
        //int pageCount = (int) Math.ceil((double)total / (double)pageable.getPageSize()); //分页
        List<PromotionWhiteListVo> listP = this.promotionWhiteListMapper.select(pageable, pCode);
        List<WhiteCus> list = this.convertPromotionWhiteListVoList(listP);
        map.put("total",total);//总条数
        if(list != null){
            map.put("pageInfo",list);//分页数据
        }else {
            map.put("pageInfo","");
        }
        return map;
    }

    @Override
    public boolean checkCpIdAndPcodeExist(Long cpId, String pCode) {
        return this.promotionWhiteListMapper.checkCpIdAndPcodeExist(cpId, pCode);
    }

    @Override
    public int getExcelData(Workbook work,String pCode) {
        List<WhiteListExcelVo> list = new ArrayList<>();
        int count = 0;
        Sheet sheet = work.getSheetAt(0);
        //遍历行(不包含首行)
        for (int i = sheet.getFirstRowNum() + 1; i < sheet.getLastRowNum() + 1; i++) {
            Row row= sheet.getRow(i);
            if (row == null) {
                continue;
            }
            //遍历列
            WhiteListExcelVo wev = new WhiteListExcelVo();
            for (int j = row.getFirstCellNum(); j < row.getLastCellNum(); j++) {
                Cell cell = row.getCell(j);
                Object cellValue = this.getCellValue(cell);
                if(cell != null){
                    switch (j){
                        case 0 : //第一列
                            wev.setCpId(cellValue.toString());
                            wev.setpCode(pCode);
                            break;
                        default:
                            continue;
                    }
                }
            }
            if(wev != null){
                list.add(wev);
            }
        }
        //开始插入或更新数据
        count = this.insertOrUpdateExcelData(list);
        return count;
    }

    @Override
    public int updatePromotionStatus(WhiteCus whiteCus) {
        int count = 0;
        PromotionWhiteListVo promotionWhiteListVo = new PromotionWhiteListVo();
        promotionWhiteListVo.setStatus(whiteCus.getStatus());
        promotionWhiteListVo.setpCode(whiteCus.getpCode());
        promotionWhiteListVo.setCpId(IMAGE_CPID);//大会默认是七个9
        boolean b = this.checkCpIdAndPcodeExist(IMAGE_CPID, promotionWhiteListVo.getpCode());
        if(b){//存在则更新
            count = this.promotionWhiteListMapper.updateStatusByCpIdAndPcode(promotionWhiteListVo);
        }else {//插入
            count = this.promotionWhiteListMapper.insert(promotionWhiteListVo);
        }
        return count;
    }

    private int insertOrUpdateExcelData(List<WhiteListExcelVo> list) {
        int count = 0;
        for (WhiteListExcelVo vo : list){
            if(vo != null){
                PromotionWhiteListVo promotionWhiteListVo = new PromotionWhiteListVo();
                promotionWhiteListVo.setCpId(Long.parseLong(vo.getCpId()));
                promotionWhiteListVo.setpCode(vo.getpCode());
                promotionWhiteListVo.setStatus(1);
                boolean b = this.checkCpIdAndPcodeExist(Long.parseLong(vo.getCpId()), vo.getpCode());
                if(b){//已存在则更新
                    this.promotionWhiteListMapper.update(promotionWhiteListVo);
                }else {//插入
                    this.promotionWhiteListMapper.insert(promotionWhiteListVo);
                }
                count ++;
            }
        }
        return count;
    }

    private Object getCellValue(Cell cell) {
        Object value = null;
        DecimalFormat df = new DecimalFormat("0"); // 格式化number String字符
        SimpleDateFormat sdf = new SimpleDateFormat("yyy-MM-dd"); // 日期格式化
        DecimalFormat df2 = new DecimalFormat("0.00"); // 格式化数字
        switch (cell.getCellType()) {
            case Cell.CELL_TYPE_STRING:
                value = cell.getRichStringCellValue().getString();
                break;
            case Cell.CELL_TYPE_NUMERIC:
                if ("General".equals(cell.getCellStyle().getDataFormatString())) {
                    value = df.format(cell.getNumericCellValue());
                } else if ("m/d/yy".equals(cell.getCellStyle().getDataFormatString())) {
                    value = sdf.format(cell.getDateCellValue());
                } else {
                    value = df2.format(cell.getNumericCellValue());
                }
                break;
            case Cell.CELL_TYPE_BOOLEAN:
                value = cell.getBooleanCellValue();
                break;
            case Cell.CELL_TYPE_BLANK:
                value = "";
                break;
            default:
                break;
        }
        return value;
    }
    private List<WhiteCus> convertPromotionWhiteListVoList(List<PromotionWhiteListVo> listP) {
        if(listP == null || listP.isEmpty()){
            return null;
        }
        List<WhiteCus> list = new ArrayList<>();
        for (PromotionWhiteListVo p : listP){
            if (p != null){
                WhiteCus whiteCus = this.setPromotionWhiteListVoToWhiteCus(p);
                list.add(whiteCus);
            }
        }
        return list;
    }

    private WhiteCus setPromotionWhiteListVoToWhiteCus(PromotionWhiteListVo pWlVo) {
        if(pWlVo == null){
            return null;
        }
        WhiteCus wc = new WhiteCus();
        wc.setId(pWlVo.getId());
        wc.setCpId(pWlVo.getCpId().toString());
        wc.setStatus(pWlVo.getStatus());
        if(pWlVo.getStatus() == 1){
            wc.setStatusName("生效");
        }
        if(pWlVo.getStatus() == 0){
            wc.setStatusName("失效");
        }
        wc.setpCode(pWlVo.getpCode());
        wc.setCreatedAt(pWlVo.getCreatedAt());
        wc.setUpdatedAt(pWlVo.getUpdatedAt());
        return wc;
    }
}
