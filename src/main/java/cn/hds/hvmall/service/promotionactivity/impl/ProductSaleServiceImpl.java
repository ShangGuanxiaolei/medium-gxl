package cn.hds.hvmall.service.promotionactivity.impl;

import cn.hds.hvmall.mapper.promotionActivity.PromotionActivityMapper;
import cn.hds.hvmall.pojo.grandsale.PromotionVO;
import cn.hds.hvmall.service.promotionactivity.ProductSaleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @类名: ProductSaleServiceImpl
 * @描述: TODO .
 * @程序猿: LuXiaoLing
 * @日期: 2019/4/19 14:24
 * @版本号: V1.0 .
 */

@Service
public class ProductSaleServiceImpl implements ProductSaleService{
    private final PromotionActivityMapper promotionActivityMapper;

    @Autowired
    public ProductSaleServiceImpl(PromotionActivityMapper promotionActivityMapper) {
        this.promotionActivityMapper = promotionActivityMapper;
    }

    @Override
    public List<PromotionVO> selectEffectSalePromotion() {
        return promotionActivityMapper.selectEffectSalePromotion();
    }

    @Override
    public List<PromotionVO> selectEffectSalePromotion2() {
        return promotionActivityMapper.selectEffectSalePromotion2();
    }

    @Override
    public PromotionVO selectByPCode(String pCode) {
        return promotionActivityMapper.selectByPCode(pCode);
    }

    @Override
    public boolean endProductSales(String pCode) {
        return promotionActivityMapper.endProductSales(pCode) > 0;
    }
}
