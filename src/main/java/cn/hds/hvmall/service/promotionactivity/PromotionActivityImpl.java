package cn.hds.hvmall.service.promotionactivity;

import cn.hds.hvmall.base.PageBase;
import cn.hds.hvmall.entity.freshman.ActivityProduct;
import cn.hds.hvmall.entity.freshman.ProductType;
import cn.hds.hvmall.entity.freshman.XquarkSku;
import cn.hds.hvmall.mapper.cuxiao.*;
import cn.hds.hvmall.mapper.freshmantab.FreshManPromotionSkusCopyMapper;
import cn.hds.hvmall.mapper.freshmantab.FreshManXquarkProductCopyMapper;
import cn.hds.hvmall.mapper.freshmantab.FreshManXquarkSkuCopyMapper;
import cn.hds.hvmall.mapper.freshmantab.FreshmanMapper;
import cn.hds.hvmall.mapper.promotionActivity.PromotionActivityMapper;
import cn.hds.hvmall.pojo.freshmantab.FreshmanPromotionSku;
import cn.hds.hvmall.pojo.freshmantab.XquarkSkuCombine;
import cn.hds.hvmall.pojo.freshmantab.XquarkSkuCombineExtra;
import cn.hds.hvmall.pojo.list.ExecuteResult;
import cn.hds.hvmall.pojo.promotion.PromotionMessage;
import cn.hds.hvmall.service.stockcheck.PromotionStockCheckService;
import cn.hds.hvmall.utils.list.GlobalErrorCode;
import cn.hds.hvmall.utils.list.qiniu.BizException;
import cn.hds.hvmall.utils.list.qiniu.IdTypeHandler;
import cn.hds.hvmall.utils.piece.UUIDGenerator;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created with IntelliJ IDEA.
 * User: WangXiao
 * Date: 2019/3/25
 * Time: 17:15
 * Description:
 */
@Service
public class PromotionActivityImpl {
    @Autowired
    private PromotionActivityImpl promotionActivity;
    @Autowired
    private PromotionBaseInfoCopyMapper   promotionBaseInfoCopyMapper;
    @Autowired
    private XquarkProductCopyMapper  xquarkProductCopyMapper;
    @Autowired
    private  PromotionSkusCopyMapper  promotionSkusCopyMapper;
    @Autowired
    private XquarkSkuCopyMapper xquarkSkuCopyMapper;
    @Autowired
    private  PromotionExclusiveCopyMapper promotionExclusiveCopyMapper;

    private static final Logger logger = LoggerFactory.getLogger(PromotionActivityImpl.class);
    @Autowired
    private PromotionActivityMapper promotionActivityMapper;

    @Autowired
    private FreshmanMapper freshmanMapper;

    @Autowired
    private FreshManXquarkSkuCopyMapper freshManXquarkSkuCopyMapper;

    @Autowired
    private FreshManXquarkProductCopyMapper copyMapper;

    @Autowired
    private FreshManPromotionSkusCopyMapper promotionSkusMapper;

    @Autowired
    private PromotionStockCheckService promotionStockCheckService;


    /**
     * 根据条件查询商品信息
     * @return
     */
    public ExecuteResult<PageBase<ActivityProduct>> selectProductInfo(String productName,String status,Integer productId,String skuCode,Integer type,Integer pageNo,Integer pageSize) {
        ExecuteResult<PageBase<ActivityProduct>> result=new ExecuteResult<>();
        PageBase<ActivityProduct> pageBase=new PageBase<>(pageSize,0,pageNo);
        int offSet= (pageNo-1)*pageSize;
        List<ActivityProduct> selectProductInfo=promotionActivityMapper.selectActivityProduct(productId,skuCode,status,productName,type,offSet,pageSize);
        for (ActivityProduct activityProduct : selectProductInfo) {
            Integer amonut =0;
            for (XquarkSku xquarkSku : activityProduct.getXquarkSkulist()) {
                amonut= amonut + xquarkSku.getStock();
            }
            activityProduct.setProductamount(amonut);
        }
        int count=promotionActivityMapper.selectProductInfoCount(productId,skuCode,productName,status,type);
        int totalPage = count % pageSize == 0 ? count / pageSize : count / pageSize + 1;

        pageBase.setTotalPages(totalPage);
        pageBase.setPageNum(offSet);
        pageBase.setTotalCount(count);
        pageBase.setPageSize(pageSize);
        pageBase.setResultList(selectProductInfo);
        result.setResult(pageBase);


        return result;
    }
    /**
     * 查询类型
     * @param parentId
     * @return
     */
    public ExecuteResult<List<ProductType>> selectType(Integer parentId) {
        ExecuteResult<List<ProductType>> result=new ExecuteResult<>();
        if(parentId!=null){
            List<ProductType> detailType=promotionActivityMapper.selectDetailType(parentId);
            result.setResult(detailType);
        }else{
            List<ProductType> parentType=promotionActivityMapper.selectParentType();
            result.setResult(parentType);
        }
        return result;
    }


    /**
     * 删除商品促销活动
     */
    public  Integer deleteActivity(String pCode){
        return  promotionActivityMapper.deleteActivity(pCode);
    }

    /**
     * 终止商品促销活动
     */
    public  Integer stopActivity(String pCode){
        return  promotionActivityMapper.stopActivity(pCode);
    }
    /**
     * 查询活动信息
     */

    public PromotionBaseInfo queryActivityByPcode(String pCode){
        return promotionActivityMapper.queryActivityByPcode(pCode);
    }

    /**
     * 查询活动下相关商品
     */
    public List<ActivityProduct> queryActivityProductByPcode(String pCode){
        return promotionActivityMapper.queryActivityProductByPcode(pCode);
    }

    /**
     * 新建商品促销活动相关信息
     */
    public Integer insertPromotionBase(PromotionBaseInfo promotionBaseInfo){
        Integer integer = promotionActivityMapper.insertPromotionBase(promotionBaseInfo);
        return integer;
    }
    /**
     * 更新活动信息表
     */
    public Integer updatePromotionBaseBypCode(PromotionBaseInfo promotionBaseInfo){
        Integer integer = promotionActivityMapper.updatePromotionBaseBypCode(promotionBaseInfo);
        return integer;
    }

    /**
     * 逻辑删除XquarkSku表与促销活动有关商品sku
     */
    public Integer deleteXquarkSkuByPcode(String pCode){
        Integer integer = promotionActivityMapper.deleteXquarkSkuByPcode(pCode);
        return integer;
    }

    /**
     * 逻辑删除XquarkProduct表与促销活动有关商品sku
     */
    public Integer deleteXquarkProductByPcode(String pCode){
        Integer integer = promotionActivityMapper.deleteXquarkProductByPcode(pCode);
        return integer;
    }
    /**
     * 逻辑删除XPromotionSkus表与促销活动有关商品sku
     */
    public Integer deletePromotionSkus(String pCode){
        Integer integer = promotionActivityMapper.deletePromotionSkus(pCode);
        return integer;
    }
    /**
     * 逻辑删除PromotionExclusive表与促销活动有关商品sku
     */
    public Integer deletePromotionExclusive(String pCode){
        Integer integer = promotionActivityMapper.deletePromotionExclusive(pCode);
        return integer;
    }

     @Transactional(rollbackFor = BizException.class)
    public void insertActivity(Map map){


        //判断有无pCode有则更新商品信息表然后删除所属相关商品再添加商品信息
        String pCode = (String)map.get("pCode");
        if(null!=pCode&&!"".equals(pCode)){
            PromotionBaseInfo promotionBaseInfo = new PromotionBaseInfo();
            String pName = (String)map.get("pName");
          if(null!=pName&&!"".equals(pName)){promotionBaseInfo.setpName(pName);}

            promotionBaseInfo.setpCode(pCode);

            String  effectFrom = (String)map.get("effectFrom");
            Date startdate = timeStamp2Date(effectFrom, null);
            if(null!=startdate){
                promotionBaseInfo.setEffectFrom(startdate);
            }
            String effectTo = (String)map.get("effectTo");
            Date enddate = timeStamp2Date(effectTo, null);
            if (null!=enddate){
                promotionBaseInfo.setEffectTo(enddate);
            }

            String cornerTab = (String)map.get("cornerTab");
            promotionBaseInfo.setCornerTab(cornerTab);
            promotionBaseInfo.setpStatus(5);
            String customerType = (String)map.get("customerType");
            if(null!=customerType){
                promotionBaseInfo.setCustomerType(customerType);
            }
            Integer integer = promotionActivity.updatePromotionBaseBypCode(promotionBaseInfo);
            if(integer==0){
                throw  new BizException(GlobalErrorCode.valueOf("商品促销活动更新PromotionBaseInfo表失败:"+promotionBaseInfo.getpCode()));
            }

            if(null!=map.get("activityProductList")&&!"".equals(map.get("activityProductList"))){
                List<ActivityProduct>   proList = JSONArray.parseArray(JSON.toJSONString(map.get("activityProductList")),ActivityProduct.class);
                for (ActivityProduct product : proList) {
                            if(null == product.getSourceId() || "".equals(product.getSourceId())){
                                copyProduct(product,pCode);
                            }else{
                                List<XquarkSku>   skuList  = product.getXquarkSkulist();
                                for (XquarkSku sku : skuList) {
                                    if(sku.getIsDeleted() == false){
                                        //删除promotionSkus表中的商品规格数据
                                        promotionActivityMapper.deleteSku(sku.getFzSkuCode(),sku.getProductId().longValue());
                                    }else{
                                        cn.hds.hvmall.pojo.freshmantab.XquarkSku xquarkSku = new cn.hds.hvmall.pojo.freshmantab.XquarkSku();
                                        xquarkSku.setSecureAmount(0);
                                        xquarkSku.setProductId(sku.getProductId().longValue());
                                        xquarkSku.setSkuCode(sku.getFzSkuCode());
                                        if(null!=sku.getActivityprice()){         xquarkSku.setPrice(sku.getActivityprice());}
                                        if(null!=sku.getReduction()){   xquarkSku.setPoint(sku.getReduction());}
                                        if(null!=sku.getPromoAmt()){     xquarkSku.setPromoAmt(sku.getPromoAmt());}
                                        if(null!=sku.getServerAmt()){  xquarkSku.setServerAmt(sku.getServerAmt());}
                                        if(null!=sku.getNetWorth()){   xquarkSku.setNetWorth(sku.getNetWorth());}
                                        if(null!=sku.getSkucode()){      xquarkSku.setSourceSkuCode(sku.getSkucode());}
                                        if(null!=sku.getPoint()){xquarkSku.setDeductionDPoint(sku.getPoint());}
//                        if(null!=sku.getActivitystock()){    xquarkSku.setAmount(sku.getActivitystock());}
                                        xquarkSku.setUpdatedAt(new Date());
                                        int i = freshManXquarkSkuCopyMapper.updateByProductIdSkuCode(xquarkSku);
                                        if(i==0){
                                            throw  new BizException(GlobalErrorCode.valueOf("更新xquarkSku表失败:"+sku.getFzSkuCode()));
                                        }
                                        FreshmanPromotionSku promotionSkus = new FreshmanPromotionSku();
                                        promotionSkus.setProductId(sku.getProductId().toString());
                                        promotionSkus.setpCode(pCode);
                                        promotionSkus.setSkuCode(sku.getFzSkuCode());
                                        promotionSkus.setUpdatedAt(new Date());
                                        promotionSkus.setIsDeleted((byte)1);
                                        promotionSkus.setBuyLimit(sku.getBuylimit().longValue());
                                        promotionSkus.setAmount(sku.getActivitystock());
                                        promotionSkus.setShow(sku.getShow());
                                        int i1 = promotionSkusMapper.updatePromotionByProductSku(promotionSkus);
                                        if(i1==0){
                                            throw  new BizException(GlobalErrorCode.valueOf("更新promotionSkus表失败:"+sku.getProductId()));
                                        }
                                    }

                                    List<PromotionSkus> promotionMessages = promotionActivityMapper.getPromotionMessages(sku.getProductId());
                                    Boolean flag = false;
                                    for (int i7 = 0; i7 < promotionMessages.size(); i7++) {
                                        if(promotionMessages.get(i7).getIsDeleted()==1){
                                            flag=true;
                                        }
                                    }
                                    if(!flag){
                                        int i9 = copyMapper.updateProductINSTOCK(sku.getProductId().toString());
                                        if(i9==0){
                                            throw  new BizException(GlobalErrorCode.valueOf("更新XquarkProduct下架状态失败:"+sku.getProductId()));
                                        }
                                    }

                                }
                        }
                }
            }

        }else{
            //不是更新 则直接插入促销活动信息 已经相关商品信息
            String pName = (String)map.get("pName");
            //转换DATE格式活动开始时间
            String  effectFrom = (String)map.get("effectFrom");
            //转换DATE格式活动结束时间
            String effectTo = (String)map.get("effectTo");
            Boolean type = get( map.get("type"));
            String bannerImgHome = (String)map.get("bannerImgHome");
            String bannerImgList = (String)map.get("bannerImgList");
            String customerType = (String)map.get("customerType");
            Boolean timeShow = get(map.get("timeShow"));
            Boolean calculateYield = get(map.get("calculateYield"));
            Boolean carriageFree = get(map.get("carriageFree"));
            String cornerTab = (String)map.get("cornerTab");
            String pCode1 = UUIDGenerator.getUUID();
            PromotionBaseInfo promotionBaseInfo = new PromotionBaseInfo();
            promotionBaseInfo.setpCode(pCode1);


            if (null!=pName&&!pName.equals("")) {
                promotionBaseInfo.setpName(pName);
            }
            promotionBaseInfo.setpType("PRODUCT_SALES");


            Date startdate = timeStamp2Date(effectFrom, null);
            if(null!=startdate){
                promotionBaseInfo.setEffectFrom(startdate);
            }

            Date enddate = timeStamp2Date(effectTo, null);
            if(null!=enddate){
                promotionBaseInfo.setEffectTo(enddate);
            }

            if(null!=type){
                promotionBaseInfo.setType(type);
            }

            if(null!=bannerImgHome&&!bannerImgHome.equals("")){
                promotionBaseInfo.setBannerImgHome(bannerImgHome);
            }
            if(null!=bannerImgList&&!bannerImgList.equals("")){
                promotionBaseInfo.setBannerImgList(bannerImgList);
            }

            if(null!=customerType&&!customerType.equals("")){
                promotionBaseInfo.setCustomerType(customerType);
            }
            if(null!=timeShow){
                promotionBaseInfo.setTimeShow(timeShow);
            }
            if(null!=calculateYield){promotionBaseInfo.setCalculateYield(calculateYield);}
            if(null!=carriageFree){            promotionBaseInfo.setCarriageFree(carriageFree);}
             if(null!=cornerTab){ promotionBaseInfo.setCornerTab(cornerTab);}
            promotionBaseInfo.setpStatus(5);
            Integer integer = promotionActivity.insertPromotionBase(promotionBaseInfo);
            if (integer==0){
                throw  new BizException(GlobalErrorCode.valueOf("商品促销活动添加PromotionBaseInfo表失败:"+promotionBaseInfo.getpCode()));
            }

            //添加商品
            if(null!=map.get("activityProductList")&&!"".equals(map.get("activityProductList"))){
                List<ActivityProduct>   proList = JSONArray.parseArray(JSON.toJSONString(map.get("activityProductList")),ActivityProduct.class);
                for (ActivityProduct s :  proList) {
                    copyProduct(s,pCode1);
                }
            }
        }
     }

    /**
     * 商品促销活动更新有新商品添加 没有则更新
     * @param map
     */
    @Transactional(rollbackFor = BizException.class)
    public  void  updateActivity(Map map){
        //判断有无pCode有则更新商品信息表然后删除所属相关商品再添加商品信息
            String pCode = (String)map.get("pCode");
            PromotionBaseInfo promotionBaseInfo = new PromotionBaseInfo();
            String pName = (String)map.get("pName");
            if(null!=pName&&!"".equals(pName)){promotionBaseInfo.setpName(pName);}

            promotionBaseInfo.setpCode(pCode);

            String  effectFrom = (String)map.get("effectFrom");
            Date startdate = timeStamp2Date(effectFrom, null);
            if(null!=startdate){
                promotionBaseInfo.setEffectFrom(startdate);
            }
            String effectTo = (String)map.get("effectTo");
            Date enddate = timeStamp2Date(effectTo, null);
            if (null!=enddate){
                promotionBaseInfo.setEffectTo(enddate);
            }

            String cornerTab = (String)map.get("cornerTab");
            if(null!=cornerTab){
                promotionBaseInfo.setCornerTab(cornerTab);
            }
            promotionBaseInfo.setpStatus(5);
            String customerType = (String)map.get("customerType");
            if(null!=customerType){
                promotionBaseInfo.setCustomerType(customerType);
            }
            Integer integer = promotionActivity.updatePromotionBaseBypCode(promotionBaseInfo);
            if(integer==0){
                throw  new BizException(GlobalErrorCode.valueOf("商品促销活动更新PromotionBaseInfo表失败:"+promotionBaseInfo.getpCode()));
            }

            //更新商品
            List<Base> promotionBaseInfos = promotionBaseInfoCopyMapper.queryActivityByPcode(pCode);
            //添加商品
            List<ActivityProduct>   proList = JSONArray.parseArray(JSON.toJSONString(map.get("activityProductList")),ActivityProduct.class);
            for (ActivityProduct s :  proList) {
                if (null!= s.getpId()){
                    XquarkProductWithBLOBs xquarkProductWithBLOBs = xquarkProductCopyMapper.selectByPrimaryKey(s.getpId().longValue());
                    xquarkProductWithBLOBs.setSourceId(s.getpId().longValue());
                    xquarkProductWithBLOBs.setId(null);
                    xquarkProductWithBLOBs.setStatus("ONSALE");
                    if (null!=s.getImg()){ xquarkProductWithBLOBs.setImg(s.getImg());}
                    if(null!=s.getName()){      xquarkProductWithBLOBs.setName(s.getName());}
                    xquarkProductWithBLOBs.setCreatedAt(new Date());

                    if(null!=s.getProductStatus()){    xquarkProductWithBLOBs.setStatus(s.getProductStatus());}
                    if(null==s.getSourceId()||0==s.getSourceId()){
                        int i  = xquarkProductCopyMapper.insertSelective(xquarkProductWithBLOBs);
                        if(i==0){
                            throw  new BizException(GlobalErrorCode.valueOf("添加XquarkProduct表失败:"+xquarkProductWithBLOBs.getName()));
                        }
                    }
                    //更新加密product商品code
                    String Code = IdTypeHandler.encode(xquarkProductWithBLOBs.getId());
                    int i5 = xquarkProductCopyMapper.updateCodeById(Code,xquarkProductWithBLOBs.getId());
                    if(i5==0){
                        throw  new BizException(GlobalErrorCode.valueOf("更新XquarkProduct表Code失败:"+xquarkProductWithBLOBs.getName()));
                    }
                    //活动商品过滤表
                    PromotionExclusive promotionExclusive  = new PromotionExclusive();
                    promotionExclusive.setpCode(pCode);
                    promotionExclusive.setProductId(xquarkProductWithBLOBs.getId());
                    promotionExclusive.setStatus(1);
                    promotionExclusive.setCreatedAt(new Date());
                    int i3  = promotionExclusiveCopyMapper.insertSelective(promotionExclusive);
                    if(i3==0){
                        throw  new BizException(GlobalErrorCode.valueOf("添加promotionExclusive表失败:"+xquarkProductWithBLOBs.getId()));
                    }

                    List<XquarkSku>   skuList  = s.getXquarkSkulist();
                    for (XquarkSku xquarkSku : skuList) {
                        List<cn.hds.hvmall.mapper.cuxiao.XquarkSku> bySkuCode  = xquarkSkuCopyMapper.findBySkuCode(xquarkSku.getSkucode());
                        if(null==bySkuCode){
                            throw  new BizException(GlobalErrorCode.NOT_FOUND,"复制失败，未找到对应sku");
                        }
                        cn.hds.hvmall.mapper.cuxiao.XquarkSku  xquarkSku1=bySkuCode.get(0);
                        PromotionSkus promotionSkus = new PromotionSkus();
                        xquarkSku1.setProductId(xquarkProductWithBLOBs.getId());
                        xquarkSku1.setSkuCode(xquarkSku.getSkucode() + "_cx");
                        xquarkSku1.setSecureAmount(0);
                        if (null!=xquarkSku.getSpec()){
                            xquarkSku1.setSpec(xquarkSku.getSpec());
                        }
                        if(null!=xquarkSku.getPoint()){
                            xquarkSku1.setDeductionDPoint(xquarkSku.getPoint());
                        }
                        if(null!=xquarkSku.getActivityprice()){
                            xquarkSku1.setPrice(xquarkSku.getActivityprice());
                        }
                        if(null!=xquarkSku.getReduction()){
                            xquarkSku1.setPoint(xquarkSku.getReduction());
                        }
                        if(null!=xquarkSku.getPromoAmt()){
                            xquarkSku1.setPromoAmt(xquarkSku.getPromoAmt());
                        }
                        if(null!=xquarkSku.getServerAmt()){
                            xquarkSku1.setServerAmt(xquarkSku.getServerAmt());
                        }
                        if(null!=xquarkSku.getNetWorth()){
                            xquarkSku1.setNetWorth(xquarkSku.getNetWorth());
                        }
                        if(null!=xquarkSku.getSkucode()){    xquarkSku1.setSourceSkuCode(xquarkSku.getSkucode());}
                        if(null!=xquarkSku.getActivitystock()){xquarkSku1.setAmount(xquarkSku.getActivitystock());}
                        if(null!=xquarkSku.getOriginalprice()){ xquarkSku1.setOriginalPrice(xquarkSku.getOriginalprice());}
                        xquarkSku1.setCreatedAt(new Date());
                        //                        //复制sku库存信息
//                        List<Integer>  amountBYSourceSkuCode = xquarkSkuCopyMapper.findAmountBYSourceSkuCodeS(xquarkSku.getSkucode());
//
//                        //sku原库存信息
                        List<Integer>  amountBySkuCodea   = xquarkSkuCopyMapper.findAmountBySkuCode(xquarkSku.getSkucode());
//
//
//                        if(0==amountBySkuCodea.size()){
//                            throw  new  BizException(GlobalErrorCode.NOT_FOUND,"库存信息有误");
//                        }
//                        //商品库存保存量
                        Integer activitystock  = xquarkSku.getActivitystock();

                        if(activitystock>amountBySkuCodea.get(0)){
                            throw  new BizException(GlobalErrorCode.UNKNOWN,"活动库存不能大于商品库存");
                        }

                        if(null==xquarkSku.getSourceSkuCode()||"".equals(xquarkSku.getSkucode())){
                            int i1 = xquarkSkuCopyMapper.insert(xquarkSku1);
                            if(i1==0){
                                throw  new BizException(GlobalErrorCode.valueOf("添加xquarkSku1表失败:"+xquarkProductWithBLOBs.getId()));
                            }
                        }else{
                            xquarkSkuCopyMapper.updateByPrimaryKey(xquarkSku1);
                        }

                        //更新SKUCode加密字段
                        String code = IdTypeHandler.encode(xquarkSku1.getId());
                        int i6 = xquarkSkuCopyMapper.updateSkuCodeById(code,xquarkSku1.getId());
                        if(i6==0){
                            throw  new BizException(GlobalErrorCode.valueOf("更新xquarkSkuCode表失败:"+xquarkProductWithBLOBs.getId()));
                        }


                        //  插入  promotionSkus表
                        promotionSkus.setProductId(xquarkProductWithBLOBs.getId().toString());
                        promotionSkus.setpCode(pCode);
                        promotionSkus.setSkuCode(xquarkSku.getSkucode()+ "_cx");
                        promotionSkus.setIsDeleted((byte) 1);
                        if(null!=xquarkSku.getBuylimit()){    promotionSkus.setBuyLimit(xquarkSku.getBuylimit().longValue());}

                        if(null!=xquarkSku.getActivitystock()){   promotionSkus.setAmount(xquarkSku.getActivitystock());}
                        if(null!=xquarkSku.getShow()){       promotionSkus.setShow(xquarkSku.getShow());}
                        promotionSkus.setCreatedAt(new Date());
                        int i2 = promotionSkusCopyMapper.insertSelective(promotionSkus);
                        if(i2==0){
                            throw  new BizException(GlobalErrorCode.valueOf("添加promotionSkus表失败:"+xquarkProductWithBLOBs.getId()));
                        }
                    }
                }
            }
    }


    public  static   Boolean get(Object type){
        if (type.toString().equals("1")){
            return  true;
        }
        return false;
    }

    /** 
          * 时间戳转换成日期格式字符串 
          * @param seconds 精确到秒的字符串 
          * @param formatStr 
          * @return 
          */
    public  static Date timeStamp2Date(String seconds, String format) {
        if(seconds == null || seconds.isEmpty() || seconds.equals("null")){
            return  null;
        }
        if(format == null ||  format.isEmpty()) format = "yyyy-MM-dd HH:mm:ss";
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return  new Date(Long.valueOf(seconds));
    }

    /**
     * 活动结束后释放搜索商品
     */
    public List<PromotionBaseInfo> releaseProduct(){
        List<PromotionBaseInfo> promotionBaseInfo = promotionActivityMapper.releaseProduct();
        return promotionBaseInfo;
    }
    /**
     * 活动结束后释放搜索商品
     */
    public Integer releaseDeleteProduct(String pCode){
        Integer integer = promotionActivityMapper.releaseDeleteProduct(pCode);
        return integer;
    }
    /**
     * 活动结束后删除PromotionSku商品
     */
    public Integer DeletePromotionSku(String pCode){
        Integer integer = promotionActivityMapper.DeletePromotionSku(pCode);
        return integer;
    }
    /**
     * 判断促销活动下有没有商品信息
     */
    public Integer queryProductByPcode(String pCode){
        Integer integer = promotionActivityMapper.queryProductByPcode(pCode);
        return integer;
    }
    /**
     * 促销编辑时删除商品
     */
    public Integer deleteSku(String skuCode, Long productId){
        Integer integer = promotionActivityMapper.deleteSku(skuCode,productId);
        return integer;
    }

    /**
     * 判断促销活动下有没有商品信息
     */
    public PromotionMessage getProductMessage(String skuCode){
        PromotionMessage productMessage = promotionActivityMapper.getProductMessage(skuCode);
        return productMessage;
    }

    public void copyProduct(ActivityProduct  s, String pCode1){
        if (null!= s.getpId()){
            XquarkProductWithBLOBs xquarkProductWithBLOBs = xquarkProductCopyMapper.selectByPrimaryKey(s.getpId().longValue());
            if(null==xquarkProductWithBLOBs){
                throw new BizException(GlobalErrorCode.PIDNOTFOUND,"未找到产品信息，请核对商品Id");
            }
            xquarkProductWithBLOBs.setSourceId(s.getpId().longValue());
            xquarkProductWithBLOBs.setId(null);
            xquarkProductWithBLOBs.setStatus("ONSALE");
            if(null!=s.getImg()){  xquarkProductWithBLOBs.setImg(s.getImg());}
            if(null!=s.getName())  {       xquarkProductWithBLOBs.setName(s.getName());}
            xquarkProductWithBLOBs.setCreatedAt(new Date());
//            if(null!=s.getProductStatus()){       xquarkProductWithBLOBs.setStatus(s.getProductStatus());}
            int i  = xquarkProductCopyMapper.insertSelective(xquarkProductWithBLOBs);
            if (i==0){
                throw  new BizException(GlobalErrorCode.valueOf("商品促销活动添加XquarkProduct表失败:"+xquarkProductWithBLOBs.getName()));
            }
            //更新加密product商品code
            String Code = IdTypeHandler.encode(xquarkProductWithBLOBs.getId());
            int i5 = xquarkProductCopyMapper.updateCodeById(Code,xquarkProductWithBLOBs.getId());
            if(i5==0){
                throw  new BizException(GlobalErrorCode.valueOf("更新XquarkProduct表Code失败:"+xquarkProductWithBLOBs.getName()));
            }
            //活动商品过滤表
            PromotionExclusive promotionExclusive  = new PromotionExclusive();
            promotionExclusive.setpCode(pCode1);
            promotionExclusive.setProductId(xquarkProductWithBLOBs.getId());
            promotionExclusive.setStatus(1);
            promotionExclusive.setCreatedAt(new Date());
            int i3  = promotionExclusiveCopyMapper.insertSelective(promotionExclusive);
            if(i3==0){
                throw  new BizException(GlobalErrorCode.valueOf("添加promotionExclusive表失败:"+xquarkProductWithBLOBs.getId()));
            }

            List<XquarkSku>   skuList  = s.getXquarkSkulist();
            for (XquarkSku xquarkSku : skuList) {
                List<cn.hds.hvmall.mapper.cuxiao.XquarkSku> bySkuCode  = xquarkSkuCopyMapper.findBySkuCode(xquarkSku.getSkucode());
                if(0==bySkuCode.size()){
                    throw  new BizException(GlobalErrorCode.PIDNOTFOUND,"复制失败，未找到对应sku");
                }
                cn.hds.hvmall.mapper.cuxiao.XquarkSku  xquarkSku1=bySkuCode.get(0);
                PromotionSkus promotionSkus = new PromotionSkus();
                if(null!=xquarkSku.getOriginalprice()){ xquarkSku1.setOriginalPrice(xquarkSku.getOriginalprice());}
                if(null!=xquarkSku.getPoint()){   xquarkSku1.setDeductionDPoint(xquarkSku.getPoint());}
                xquarkSku1.setProductId(xquarkProductWithBLOBs.getId());

                String substring = UUID.randomUUID().toString().replaceAll("-", "").substring(0, 10);
                String sss=substring+"CX";
                List<cn.hds.hvmall.mapper.cuxiao.XquarkSku> bySkuCode1  = xquarkSkuCopyMapper.findBySkuCode(sss);
                while (0!=bySkuCode1.size()){
                    sss=UUID.randomUUID().toString().replaceAll("-", "").substring(0, 10);
                    sss=sss+"CX";
                }
                xquarkSku1.setSkuCode(sss);
                xquarkSku1.setSecureAmount(0);
//                            if(null!=xquarkSku.getSpec()){     xquarkSku1.setSpec(xquarkSku.getSpec());}
                if(null!=xquarkSku.getActivityprice()){         xquarkSku1.setPrice(xquarkSku.getActivityprice());}
                if(null!=xquarkSku.getReduction()){   xquarkSku1.setPoint(xquarkSku.getReduction());}
                if(null!=xquarkSku.getPromoAmt()){     xquarkSku1.setPromoAmt(xquarkSku.getPromoAmt());}
                if(null!=xquarkSku.getServerAmt()){  xquarkSku1.setServerAmt(xquarkSku.getServerAmt());}
                if(null!=xquarkSku.getNetWorth()){   xquarkSku1.setNetWorth(xquarkSku.getNetWorth());}
                if(null!=xquarkSku.getSkucode()){      xquarkSku1.setSourceSkuCode(xquarkSku.getSkucode());}
                if(null != xquarkSku.getActivitystock()){
                    Integer skuId = freshmanMapper.selectSourceSkuIdBySkuCode(xquarkSku.getSkucode());
                    //校验原库存数量
                    Boolean result = promotionStockCheckService.stockCheck(xquarkSku.getActivitystock(), skuId,null,"",null);
                    if(result == true){
                        xquarkSku1.setAmount(xquarkSku.getActivitystock());
                    }
                }
                xquarkSku1.setCreatedAt(new Date());

//                            //sku原库存信息
//                            List<Integer>  amountBySkuCodeb   = xquarkSkuCopyMapper.findAmountBySkuCode(xquarkSku.getSkucode());
//                            if(  0==amountBySkuCodeb.size()){
//                                throw  new  BizException(GlobalErrorCode.NOT_FOUND,"skuCode错误");
//                            }
//
//                            Integer activitystockt = xquarkSku.getActivitystock();
                //原商品sku应该保存的量
//                            Integer upStock=0;
//                            upStock=amountBySkuCodeb.get(0)-activitystockt;
//
//                            if(upStock<0){
//                                throw  new BizException(GlobalErrorCode.UNKNOWN,"普通商品库存不足");
//                            }
//                            //更新原库存信息
//                            int i4 = xquarkSkuCopyMapper.updateAmountBySkuCode(upStock, xquarkSku.getSkucode());
//
//                            if(i4==0){
//                                throw  new BizException(GlobalErrorCode.valueOf("库存信息更新失败"));
//                            }
//                            xquarkSku1.setArchive(false);
                //复制sku
                int i1 = xquarkSkuCopyMapper.insert(xquarkSku1);
                if(i1==0){
                    throw  new BizException(GlobalErrorCode.valueOf("添加xquarkSku1表失败:"+xquarkProductWithBLOBs.getId()));
                }
                //更新SKUCode加密字段
                String code = IdTypeHandler.encode(xquarkSku1.getId());
                int i6 = xquarkSkuCopyMapper.updateSkuCodeById(code,xquarkSku1.getId());
                if(i6==0){
                    throw  new BizException(GlobalErrorCode.valueOf("更新xquarkSkuCode表失败:"+xquarkProductWithBLOBs.getId()));
                }
                // 商品库存扣减

                //  插入  promotionSkus表
                promotionSkus.setProductId(xquarkProductWithBLOBs.getId().toString());
                promotionSkus.setpCode(pCode1);

                promotionSkus.setSkuCode(sss);
                promotionSkus.setIsDeleted((byte) 1);
                if(null!=xquarkSku.getBuylimit()){          promotionSkus.setBuyLimit(xquarkSku.getBuylimit().longValue());}
                if(null!=xquarkSku.getActivitystock()){          promotionSkus.setAmount(xquarkSku.getActivitystock());}
                if(null!=xquarkSku.getShow()){         promotionSkus.setShow(xquarkSku.getShow());}
                promotionSkus.setCreatedAt(new Date());
                int i2 = promotionSkusCopyMapper.insertSelective(promotionSkus);
                if(i2==0){
                    throw  new BizException(GlobalErrorCode.valueOf("添加promotionSkus表失败:"+xquarkProductWithBLOBs.getId()));
                }
            }
            //product表库存更新到sku最大库存
            int i6 = freshmanMapper.updateProductAmount(xquarkProductWithBLOBs.getId());
            if (i6==0){
                throw  new BizException(GlobalErrorCode.valueOf("更新XquarkProduct表库存amount失败:"+xquarkProductWithBLOBs.getId()));
            }
            Boolean combineProduct = freshmanMapper.isCombineProduct(xquarkProductWithBLOBs.getSourceId());
            if(combineProduct==true){
                cn.hds.hvmall.pojo.freshmantab.XquarkSku xquarkSku = freshManXquarkSkuCopyMapper.selectSkuIdByProductId(xquarkProductWithBLOBs.getId());
                XquarkSkuCombine xquarkSkuCombine = new XquarkSkuCombine();
                xquarkSkuCombine.setProductId(xquarkProductWithBLOBs.getId());
                xquarkSkuCombine.setSkuId(xquarkSku.getId());
                //插入套装表xquark_sku_combine
                int i4 = freshmanMapper.insertXquarkSkuCombine(xquarkSkuCombine);
                if (i4==0){
                    throw  new BizException(GlobalErrorCode.valueOf("插入xquark_sku_combine表数据失败:"+s.getpId()));
                }
                XquarkSkuCombine sourceCombineId = freshmanMapper.selectCombineIdByProductId(xquarkProductWithBLOBs.getSourceId());
                List<XquarkSkuCombineExtra> xquarkSkuCombineExtras = freshmanMapper.selectCombineExtraByMasterId(sourceCombineId.getId());
                for (XquarkSkuCombineExtra xquarkSkuCombineExtra : xquarkSkuCombineExtras) {
                    XquarkSkuCombineExtra xquarkSkuCombineExtra1 = new XquarkSkuCombineExtra();
                    xquarkSkuCombineExtra1.setMasterId(xquarkSkuCombine.getId());
                    xquarkSkuCombineExtra1.setSlaveId(xquarkSkuCombineExtra.getSlaveId());
                    xquarkSkuCombineExtra1.setAmount(xquarkSkuCombineExtra.getAmount());
                    Integer integer2 = freshmanMapper.insertCombineExtra(xquarkSkuCombineExtra1);
                    if (integer2==0){
                        throw  new BizException(GlobalErrorCode.valueOf("插入xquark_sku_combine_extra表数据失败:"+s.getpId()));
                    }
                }
            }
        }
    }
}