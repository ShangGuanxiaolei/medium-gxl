package cn.hds.hvmall.service.promotionactivity;

import cn.hds.hvmall.pojo.grandsale.PromotionVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @类名: ProductSaleService
 * @描述: TODO .
 * @程序猿: LuXiaoLing
 * @日期: 2019/4/19 14:18
 * @版本号: V1.0 .
 */
public interface ProductSaleService {

    /**
     * 查询有效的折扣促销活动
     * @return 活动VO对象
     */
    List<PromotionVO> selectEffectSalePromotion();

    List<PromotionVO>  selectEffectSalePromotion2();

    /**
     * 根据pCode查询折扣促销活动
     * @param pCode 活动编码
     * @return 活动对象
     */
    PromotionVO selectByPCode(String pCode);

    /**
     * 根据pCode结束折扣促销活动
     * @param pCode pCode
     * @return 操作结果
     */
    boolean endProductSales(String pCode);
}
