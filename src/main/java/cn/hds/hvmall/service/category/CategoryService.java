package cn.hds.hvmall.service.category;

import cn.hds.hvmall.entity.category.CategoryForPromotion;
import cn.hds.hvmall.pojo.list.ProductSource;
import cn.hds.hvmall.pojo.list.Taxonomy;
import cn.hds.hvmall.pojo.list.TermRelationship;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CategoryService {

  /**
   * 新增分类
   * @param name 分类名称
   * @param taxonomy 类目
   * @param parentId 父类
   * @param userId 创建者
   * @param shopId 商家
   * @param source 平台
   * @param sourceId 平台id
   * @return 分类
   */
  CategoryForPromotion save(String name, Taxonomy taxonomy, String parentId,
                            String userId, String shopId, ProductSource source,
                            String sourceId);

  /**
   * 商品添加到分类（一个商品只能属于一个分类）
   */
  void addProductCategory(String productId, String categoryId, String objType, Integer grandSaleId);

  List<CategoryForPromotion> selectCategoryName(String objType, Integer grandSaleId);

  /**
   * 删除所有复制的会场商品
   * @param type 商品id
   * @return 删除结果
   */
  int deleteProduct(String type);

  /**
   * 删除所有复制的会场商品sku
   * @param type 商品id
   * @return 删除结果
   */
  int deleteSku(String type);

  /**
   * 删除所有新建的分类数据
   * @param type 商品id
   * @return 删除结果
   */
  int deleteCategory(String type);

  /**
   * 根据类型获取商品id
   * @param type 分类活动类型
   * @return 分类中间表对象
   */
  List<TermRelationship> selectByObjType(String type);

  /**
   * 根据id更新分类类型
   * @param objType 分类类型
   * @param id 分类中间表id
   * @return 更新结果
   */
  int updateTermRelationshipByType(@Param("objType") String objType, @Param("id") String id);

  /**
   * 检查 category 的名字是否已经存在
   * @param categoryName 类目名称
   * @return 如果存在返回 category 的 id
   */
  Integer checkIsCreate(String categoryName);
}
