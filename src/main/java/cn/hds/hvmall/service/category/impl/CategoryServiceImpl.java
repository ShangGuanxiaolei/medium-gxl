package cn.hds.hvmall.service.category.impl;

import cn.hds.hvmall.entity.category.CategoryForPromotion;
import cn.hds.hvmall.mapper.category.CategoryMapper;
import cn.hds.hvmall.pojo.list.ProductSource;
import cn.hds.hvmall.pojo.list.Taxonomy;
import cn.hds.hvmall.pojo.list.Term;
import cn.hds.hvmall.pojo.list.TermRelationship;
import cn.hds.hvmall.service.category.CategoryService;
import cn.hds.hvmall.utils.list.GlobalErrorCode;
import cn.hds.hvmall.utils.list.qiniu.BizException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

@Slf4j
@Service
public class CategoryServiceImpl implements CategoryService {

  private final CategoryMapper categoryMapper;

  @Autowired
  public CategoryServiceImpl(CategoryMapper categoryMapper) {
    this.categoryMapper = categoryMapper;
  }

  @Override
  @Transactional
  public CategoryForPromotion save(String name, Taxonomy taxonomy, String parentId,
                                   String userId, String shopId, ProductSource source,
                                   String sourceId) {

    if (StringUtils.isEmpty(name)) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "分类的名称不能为空");
    }

    // 控制分类的名称的长度，全中文8位，中文+英文/英文 长度为16
    int nameLength;
    if (!Pattern.matches("/^[\u4E00-\u9FA5]+$/", name)) {
      // 不全是中文
      nameLength = 16;
    } else {
      //全是中文
      nameLength = 8;
    }

    if (name.length() > nameLength) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "分类的名称不能大于 " + nameLength + " 位");
    }

    if (taxonomy == null) {
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "分类的类别不能为空");
    }

    Term term = categoryMapper.loadByName(name);
    if (term == null) {
      term = new Term();
      term.setName(name);
      categoryMapper.insertTerm(term);
    }

    CategoryForPromotion cat = categoryMapper.selectByTermAndTaxonomy(term.getId(), taxonomy, shopId);
    if (cat == null) {
      cat = new CategoryForPromotion();
      cat.setTermId(term.getId());
      cat.setName(term.getName());
      cat.setTaxonomy(taxonomy);
      cat.setCreatorId(userId);
      cat.setShopId(shopId);
      // default value
      cat.setTreePath("");
      cat.setSource(source);
      cat.setSourceCategoryId(sourceId);
      if (StringUtils.isNotEmpty(parentId)) {
        CategoryForPromotion parent = categoryMapper.load(parentId);
        if (parent == null) {
          throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "分类的父分类不存在");
        }
        cat.setParentId(parentId);
        cat.setTreePath(parent.getTreePath() + parentId + ">");
      }
      Integer maxIdx = categoryMapper.selectMaxIdx(parentId, shopId);
      cat.setIdx(maxIdx == null ? 0 : maxIdx + 1);
      categoryMapper.insertCategory(cat);
    } else {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "类目[" + name + "]已经存在，abc与Abc是一样的哦~");
    }
    return cat;
  }


  @Override
  public Integer checkIsCreate(String categoryName){
    return categoryMapper.checkIsCreate(categoryName);
  }

  @Override
  @Transactional
  public void addProductCategory(String pid, String categoryId, String objType, Integer grandSaleId) {
    List<TermRelationship> t = categoryMapper.checkProductIdExists(objType, pid);
    if (t != null && t.size() > 0) {
      log.info("termRelationshipMapper size :" + t.size());
      for (TermRelationship aT : t) {
        log.info("termRelationshipMapper:" + aT.getId());
        categoryMapper.delete(aT.getId());
      }
    }
    // 商品分类支持多选
    String[] categoryIdList = categoryId.split(",");
    for (String acid : categoryIdList) {
      if (!"".equals(acid)) {
        TermRelationship tr = new TermRelationship();
        tr.setCategoryId(acid);
        tr.setObjType(objType);
        tr.setObjId(pid);
        tr.setGrandSaleId(grandSaleId);
        categoryMapper.insertRelationShip(tr);
      }
    }
  }

  @Override
  public List<CategoryForPromotion> selectCategoryName(String objType, Integer grandSaleId) {
    return categoryMapper.selectCategoryName(objType, grandSaleId);
  }

  @Override
  @Transactional
  public int deleteProduct(String type) {
    List<TermRelationship> termRelationships = getTermRelationships(type);
    for (TermRelationship termRelationship:
         termRelationships) {
      String objId = termRelationship.getObjId();
      if (StringUtils.isBlank(objId)) {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "商品id不能为空");
      }
      //删除商品和sku
      if (categoryMapper.deleteProductById(objId) <= 0) {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "删除商品失败");
      }
    }
    return 1;
  }

  @Override
  @Transactional
  public int deleteSku(String type) {
    List<TermRelationship> termRelationships = getTermRelationships(type);
    for (TermRelationship termRelationship:
            termRelationships) {
      String objId = termRelationship.getObjId();
      if (StringUtils.isBlank(objId)) {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "商品id不能为空");
      }
      //删除sku
      if (categoryMapper.deleteSkuByProductId(objId) <= 0) {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "删除sku失败");
      }
    }
    return 1;
  }

  @Override
  @Transactional(rollbackFor = Exception.class)
  public int deleteCategory(String type) {
    List<TermRelationship> termRelationships = getTermRelationships(type);
    Set<String> categoryIds = new HashSet<>();
    for (TermRelationship termRelationship:termRelationships) {
      String id = termRelationship.getId();
      if (StringUtils.isBlank(id)) {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "relationshipId不能为空");
      }
      String categoryId = termRelationship.getCategoryId();
      if (StringUtils.isBlank(categoryId)) {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "分类id为空");
      }
      categoryIds.add(categoryId);
      if (categoryMapper.delete(id) <= 0){
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "删除relationship失败");
      }
    }
    for (String categoryId:
         categoryIds) {
      CategoryForPromotion categoryForPromotion = categoryMapper.selectByCategoryId(categoryId);
      if (categoryForPromotion == null) {
        log.error("查询分类失败,categoryId:{}", categoryId);
        continue;
      }
      String termId = categoryForPromotion.getTermId();
      if (StringUtils.isBlank(termId)) {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "termId不能为空");
      }
      //删除分类
      categoryMapper.deleteTermById(termId);
      if (categoryMapper.deleteCategoryById(categoryId) <= 0) {
        throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "删除分类失败");
      }
    }
    return 1;
  }

  @Override
  public List<TermRelationship> selectByObjType(String type) {
    return categoryMapper.selectByObjType(type);
  }

  @Override
  public int updateTermRelationshipByType(String objType, String id) {
    return categoryMapper.updateTermRelationshipByType(objType, id);
  }

  private List<TermRelationship> getTermRelationships(String type) {
    if (StringUtils.isBlank(type)) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "活动类型不能为空");
    }
    return categoryMapper.selectByObjType(type);
  }
}
