package cn.hds.hvmall.service.invite;
/**
 * 邀请码服务
 * @author LuXiaoLing
 *
 */
public interface PromotionInviteCodeService {

	/**
	 * 添加活动类型活动邀请码
     * @param pCode
     * @param codeNum
     */
	int insertInviteCode(String pCode, int codeNum);
}
