package cn.hds.hvmall.service.invite.impl;

import cn.hds.hvmall.mapper.invite.PromotionInviteCodeMapper;
import cn.hds.hvmall.service.invite.PromotionInviteCodeService;
import cn.hds.hvmall.utils.invitecode.InviteCodeUtil;
import cn.hds.hvmall.utils.list.GlobalErrorCode;
import cn.hds.hvmall.utils.list.qiniu.BizException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class PromotionInviteCodeServiceImpl implements PromotionInviteCodeService {

	@Autowired 
	private PromotionInviteCodeMapper inviteCodeMapper;

	@Override
	@Transactional
	public int insertInviteCode(String pCode, int codeNum) {
		int res = 0;
		if (codeNum <=0){
			throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "未输入邀请码生成数量");
		}

		int sum = inviteCodeMapper.getPromotionInviteCodeNum(pCode);
		if(sum > 0){
			throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "邀请码已生成");
		}
		if (null != pCode && !(pCode.equals(""))) {
			Set<String> codes = InviteCodeUtil.generateInviteCode(codeNum);
			long begin = System.currentTimeMillis();
			List<Map<String,String>> listCodes = new ArrayList<>();

			for (String code : codes) {
				Map<String,String> map = new HashMap<>();
				map.put("pCode",pCode);
				map.put("code",code);
				listCodes.add(map);
			}
			long end = System.currentTimeMillis();

			int num = inviteCodeMapper.insertAll(listCodes);
			if(num>0){
				res=1;
			}
			System.out.println("time2 = "+(end-begin));

		} else {
			throw new RuntimeException("活动类型参数不可为空");
		}
		return res;
	}
}
