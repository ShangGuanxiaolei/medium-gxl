package cn.hds.hvmall.service.bargain;

import cn.hds.hvmall.entity.bargain.PromotionBargainRule;
import cn.hds.hvmall.enums.CareerLevelType;
import cn.hds.hvmall.pojo.bargain.BargainRuleVO;

import java.util.List;

/**
 * @author wangxinhua
 * @since 1.0
 */
public interface PromotionBargainRuleService {

    List<PromotionBargainRule> listAll();

    boolean update(BargainRuleVO rule);

}
