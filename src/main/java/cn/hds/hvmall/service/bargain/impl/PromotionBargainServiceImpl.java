package cn.hds.hvmall.service.bargain.impl;

import cn.hds.hvmall.entity.bargain.BargainListVO;
import cn.hds.hvmall.entity.bargain.BargainSku;
import cn.hds.hvmall.entity.bargain.SearchCondation;
import cn.hds.hvmall.mapper.bargain.XquarkPromotionBargainMapper;
import cn.hds.hvmall.mapper.bargain.XquarkPromotionBargainProductMapper;
import cn.hds.hvmall.service.bargain.PromotionBargainService;
import cn.hds.hvmall.utils.list.GlobalErrorCode;
import cn.hds.hvmall.utils.list.qiniu.BizException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class PromotionBargainServiceImpl implements PromotionBargainService {

  private final XquarkPromotionBargainMapper xquarkPromotionHaggleMapper;

  private final XquarkPromotionBargainProductMapper xquarkPromotionBargainProductMapper;

  @Autowired
  public PromotionBargainServiceImpl(XquarkPromotionBargainMapper xquarkPromotionHaggleMapper, XquarkPromotionBargainProductMapper xquarkPromotionBargainProductMapper) {
    this.xquarkPromotionHaggleMapper = xquarkPromotionHaggleMapper;
    this.xquarkPromotionBargainProductMapper = xquarkPromotionBargainProductMapper;
  }

  @Override
  public Map<String, Object> searchList(SearchCondation searchCondation) {
    if (searchCondation.getPageNum() == null){
      throw new BizException(GlobalErrorCode.UNKNOWN,"分页当前页不能为空");
    }
    if (searchCondation.getPageSize() == null){
      throw new BizException(GlobalErrorCode.UNKNOWN,"分页大小不能为空");
    }
    Map<String,Object> map = new HashMap<>(2);
    List<BargainListVO> list =xquarkPromotionHaggleMapper.select(searchCondation);
    long count = xquarkPromotionHaggleMapper.count();
    map.put("list",list);
    map.put("count",count);
    return map;
  }

  @Transactional
  @Override
  public Boolean addBargain(BargainListVO bargain) {
    //设置64位id
    check(bargain);
    if (bargain.getSkus() == null || bargain.getSkus().isEmpty()){
      throw new BizException(GlobalErrorCode.UNKNOWN,"请添加活动商品");
    }
    boolean ret = xquarkPromotionHaggleMapper.insertVO(bargain) > 0;
    //取出商品底价，活动库存信息
    List<BargainSku> list = bargain.getSkus();
    for (BargainSku p: list
         ) {
      checkout(p);
      p.setBargainId(bargain.getBargainId());
      p.setProductId(bargain.getProductId());
      if (xquarkPromotionBargainProductMapper.insert(p) < 1){
        throw new BizException(GlobalErrorCode.UNKNOWN,"添加商品异常请重试");
      }
    }
    return ret;
  }

  @Override
  public BargainListVO selectInfo(String id) {
    BargainListVO bargain = xquarkPromotionHaggleMapper.selectById(id);
    bargain.setSkus(xquarkPromotionBargainProductMapper.selectProducts(id));
    return bargain;
  }

  @Transactional
  @Override
  public boolean updateBargain(BargainListVO bargain) {
    check(bargain);
    if (bargain.getBargainId() == null || bargain.getBargainId().isEmpty()){
      throw new BizException(GlobalErrorCode.UNKNOWN,"更新活动id不能为空");
    }
    if (xquarkPromotionHaggleMapper.updateByPrimaryKeySelective(bargain) > 0){
      if (xquarkPromotionBargainProductMapper.deleteById(bargain.getBargainId()) > 0){
        List<BargainSku> list = bargain.getSkus();
        for (BargainSku p:list
             ) {
          p.setId(null);
          p.setBargainId(bargain.getBargainId());
          p.setProductId(bargain.getProductId());
          checkout(p);
          if (xquarkPromotionBargainProductMapper.insert(p) < 1){
            throw new BizException(GlobalErrorCode.UNKNOWN,"更新失败");
          }
        }
        return true;
      }
    }
    throw new BizException(GlobalErrorCode.UNKNOWN,"更新失败");
  }

  @Override
  public boolean changeBargainState(@RequestParam("id") String id,@RequestParam("state") Integer state) {
    //人工失效为4
    return xquarkPromotionHaggleMapper.updateState(id,state) > 0;
  }

  private void check(BargainListVO bargain){
    if (bargain.getPromotionName().isEmpty()){
      throw new BizException(GlobalErrorCode.UNKNOWN,"请检查活动名称，不能为空");
    }
    if (bargain.getStartTime() == null || bargain.getStartTime().toString().isEmpty()){
      throw new BizException(GlobalErrorCode.UNKNOWN,"请检查活动时间，不能为空");
    }
    if (bargain.getEndTime() == null || bargain.getEndTime().toString().isEmpty()){
      throw new BizException(GlobalErrorCode.UNKNOWN,"请检查活动时间，不能为空");
    }
    if (bargain.getSupportLimit() == null){
      throw new BizException(GlobalErrorCode.UNKNOWN,"请检查助力规则，不能为空");
    }
    if (bargain.getProductId().isEmpty()){
      throw new BizException(GlobalErrorCode.UNKNOWN,"请选择活动商品");
    }
  }
  private void checkout(BargainSku p){
    if (p.getPriceEnd() == null){
      throw new BizException(GlobalErrorCode.UNKNOWN,"活动底价不能为空");
    }
    if (p.getStock() == null){
      throw new BizException(GlobalErrorCode.UNKNOWN,"活动库存不能为空");
    }
    if (p.getStock() <= 0){
      throw new BizException(GlobalErrorCode.UNKNOWN,"活动库存不能小于0");
    }
  }
}
