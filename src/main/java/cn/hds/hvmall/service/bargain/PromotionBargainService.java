package cn.hds.hvmall.service.bargain;

import cn.hds.hvmall.entity.bargain.BargainListVO;
import cn.hds.hvmall.entity.bargain.SearchCondation;

import java.util.Map;

public interface PromotionBargainService {
  Map<String, Object> searchList(SearchCondation searchCondation);

  Boolean addBargain(BargainListVO bargain);

  BargainListVO selectInfo(String id);

  boolean updateBargain(BargainListVO bargain);

  boolean changeBargainState(String id, Integer state);
}
