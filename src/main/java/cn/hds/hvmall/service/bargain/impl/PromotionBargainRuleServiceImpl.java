package cn.hds.hvmall.service.bargain.impl;

import cn.hds.hvmall.entity.bargain.PromotionBargainRule;
import cn.hds.hvmall.mapper.piece.PromotionBargainRuleMapper;
import cn.hds.hvmall.pojo.bargain.BargainRuleVO;
import cn.hds.hvmall.service.bargain.PromotionBargainRuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author wangxinhua
 * @since 1.0
 */
@Service
public class PromotionBargainRuleServiceImpl implements PromotionBargainRuleService {

    private final PromotionBargainRuleMapper bargainRuleMapper;

    @Autowired
    public PromotionBargainRuleServiceImpl(PromotionBargainRuleMapper bargainRuleMapper) {
        this.bargainRuleMapper = bargainRuleMapper;
    }

    @Override
    public List<PromotionBargainRule> listAll() {
        return bargainRuleMapper.listAll();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean update(BargainRuleVO rule) {
        return bargainRuleMapper.updateForNew(rule) > 0
                && bargainRuleMapper.updateForOld(rule) > 0;
    }

}
