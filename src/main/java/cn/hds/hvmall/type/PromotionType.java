package cn.hds.hvmall.type;

/**
 * 活动类型
 */
public enum PromotionType {

  FLASHSALE("", "", "秒杀", 1),
  PIECE("", "", "拼团", 3),
  PIECE_GROUP("", "", "拼团", 4),
  PACKET_RAIN("", "", "红包雨", 5),
  LOTTERY_DRAW("", "", "抽奖", 6),
  PRODUCT_SALES("", "", "折扣促销", 7),
  RESERVE("", "", "预售", 8),
  MEETING("", "", "大会", 9),
  MEETING_GIFT("", "", "大会", 10),
  APPLY_CHANGE_GOODS("", "", "", 11),
  APPLY_REISSUE_GOODS("", "", "", 12);

  /**
   * 路径
   */
  private String url;

  /**
   * 活动名称
   */
  private String serviceName;

  /**
   * service名称
   */
  private String cName;

  /**
   * 排序优先级
   */
  private Integer sortNum;

  PromotionType(String url, String serviceName, String cName, int sortNum) {
    this.url = url;
    this.serviceName = serviceName;
    this.cName = cName;
    this.sortNum = sortNum;
  }

  public String getUrl() {
    return url;
  }

  public String getServiceName() {
    return serviceName;
  }

  public String getcName() {
    return cName;
  }

  public Integer getSortNum() {
    return sortNum;
  }
}
