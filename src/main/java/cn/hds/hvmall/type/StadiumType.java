package cn.hds.hvmall.type;

import java.util.Objects;

/**
 * 分会场类型
 *
 * @author gxl
 */
public enum StadiumType {

  /**
   * 家居类
   */
  FURNITURE("家居类"),

  /**
   * 宠物类
   */
  PET("宠物类"),

  /**
   * 美妆类
   */
  MAKEUP("美妆类"),

  /**
   * 洗护类
   */
  NURSING("洗护类"),

  /**
   * 母婴类
   */
  BABY("母婴类"),

  /**
   * 伊夫黎雪
   */
  SNOW("伊夫黎雪"),

  /**
   * 纤体/营养类
   */
  NUTRITION("健康类"),

  /**
   * 食品类
   */
  FOOD("食品类");

  public final String desc;

  StadiumType(String desc){
    this.desc = desc;
  }

  public static StadiumType valueOfDesc(String desc){
    desc = desc.replace("类","");
    desc = desc.replace("分会场","");
    for (StadiumType st : StadiumType.values()){
      if (Objects.equals(st.desc.replace("类",""),desc)){
        return st;
      }
    }
    return null;
  }
}
