package cn.hds.hvmall.enums;

/**
 * Created by IntelliJ IDEA. User: huangjie Date: 2018/6/8. Time: 下午4:28 长度单位
 */
public enum LengthUnit {
  CM,//厘米
  DM,//分米
  M;//米
}
