package cn.hds.hvmall.enums;

import org.apache.commons.lang3.StringUtils;

import java.util.Optional;

import static cn.hds.hvmall.enums.IdentityType.NEW;
import static cn.hds.hvmall.enums.IdentityType.VIP;

public enum CareerLevelType {

  PW(NEW),

  RC(NEW),

  DS(VIP),

  SP(VIP);

  private final IdentityType identityType;

  CareerLevelType(IdentityType identityType) {
    this.identityType = identityType;
  }

  public static CareerLevelType getFinalLevelType(CareerLevelType level1, CareerLevelType level2) {
    level1 = Optional.ofNullable(level1).orElse(RC);
    level2 = Optional.ofNullable(level2).orElse(RC);
    return level1.compareTo(level2) > 0 ? level1 : level2;
  }

  public static CareerLevelType getFinalLevelType(String level1, String level2) {
    CareerLevelType levelType1;
    CareerLevelType levelType2;
    try {
      levelType1 = StringUtils.isBlank(level1) ? null : CareerLevelType.valueOf(level1);
      levelType2 = StringUtils.isBlank(level2) ? null : CareerLevelType.valueOf(level2);
    } catch (Exception e) {
      throw new IllegalArgumentException(e);
    }
    return getFinalLevelType(levelType1, levelType2);
  }

  public IdentityType getIdentityType() {
    return identityType;
  }
}