package cn.hds.hvmall.enums;

/**
 * @author wangxinhua
 * @since 1.0
 */
public enum IdentityType {
    /**
     * 新人
     */
    NEW,

    /**
     * VIP
     */
    VIP
}
