package cn.hds.hvmall.enums;

public enum OrderStatus {
  // PREORDER, // 预订
  SUBMITTED(false), // 已提交 ，未付款
  CANCELLED(false), // 取消
  PAID(true), // 已付款  未发货
  DELIVERY(true),//发货中
  SHIPPED(true), // 已发货
  SUCCESS(true), // 交易成功
  CHANGING(false), //换货申请中
  REISSUING(false), //补货申请中
  REFUNDING(false), // 退款申请中
  COMMENT(true), // 待评价ff
  PAIDNOSTOCK(false),   ////添加另一种状态   2018-9-11 17:19
  PENDING(false),
  CLOSED(false); // 交易关闭

  private final boolean cancelable;

  OrderStatus(boolean cancelable) {
    this.cancelable = cancelable;
  }

  public boolean isCancelable() {
    return cancelable;
  }
}
