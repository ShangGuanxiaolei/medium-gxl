package cn.hds.hvmall.enums;

/**
 * 运费类型
 *
 * @author chh
 */
public enum LogisticsType {
  UNIFORM,  // 统一运费
  TEMPLATE, // 新运费模版
  SF // 顺丰发货
}
