package cn.hds.hvmall.enums;

import org.apache.commons.lang3.ArrayUtils;

import java.util.BitSet;

/**
 * Created by dongsongjie on 15/12/3.
 */
public class ProductFeatures {

  private BitSet featuresSet;

  public ProductFeatures(Long... features) {
    for (int i = 0; i < features.length; i++) {
      //padding default value for featuresSet
      if (i == 0 && (features[0] == null || features[0].equals(0l))) {
        //padding first non-zero word, cause BitSet.valueOf cut zero words in Long array until find a non-zero word.
        features[0] = 1l;
      } else if (features[i] == null) {
        features[i] = 0l;
      }
    }
    this.featuresSet = BitSet.valueOf(ArrayUtils.toPrimitive(features));
  }

  public boolean getFeature(ProductFeaturesKeyType featureKey) {
    return featuresSet.get(featureKey.getIndex());
  }

  public void setFeature(ProductFeaturesKeyType featureKey) {
    featuresSet.set(featureKey.getIndex());
  }

  public void setFeature(ProductFeaturesKeyType featureKey, boolean fVal) {
    if (fVal) {
      featuresSet.set(featureKey.getIndex());
    } else {
      featuresSet.clear(featureKey.getIndex());
    }
  }

  public Long getWord(ProductFeaturesKeyType featureKey) {
    int wordIndex = getWordIndex(featureKey);
    long[] lfeatures = featuresSet.toLongArray();
    return lfeatures[wordIndex];
  }

  public int getWordIndex(ProductFeaturesKeyType featureKey) {
    return featureKey.getIndex() >> 6;
  }
}
