package cn.hds.hvmall.enums;

/**
 * Created by wangxinhua on 18-2-8. DESC:
 */
public enum ProductType {
  /**
   * 普通商品
   */
  NORMAL("商品"),

  /**
   * 组合商品
   */
  COMBINE("组合商品");

  private String name;

  ProductType(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }
}
