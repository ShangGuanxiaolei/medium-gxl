package cn.hds.hvmall.enums;

/**
 * Created by wangxinhua on 18-3-15. DESC:滤芯规格
 */
public enum FilterSpec {
    SINGLE,
    DOZEN
}
