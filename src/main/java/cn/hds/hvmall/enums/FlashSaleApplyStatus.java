package cn.hds.hvmall.enums;

/**
 * Created by wangxinhua on 17-12-5. DESC:
 */
public enum FlashSaleApplyStatus {
  APPLYING, SUCCESS, REJECTED
}
