package cn.hds.hvmall.enums;

public enum ProductStatus {

  DRAFT("草稿"), // 草稿
  FORSALE("待上架"), // 待上架发布
  DELAY("计划发布"), //计划发布
  ONSALE("已上架"), // 上架
  SOLDOUT("已售罄"), // 已售罄
  INSTOCK("已下架"); // 下架

  private final String namecn;

  ProductStatus(String namecn) {
    this.namecn = namecn;
  }

  public String getNamecn() {
    return namecn;
  }

}
