package cn.hds.hvmall.enums;

/**
 * @author wangxinhua
 * @date 2019-04-24
 * @since 1.0
 */
public enum PromotionStatus {

    /**
     * 已关闭
     */
    CLOSED("已关闭"),

    /**
     * 即将开始
     */
    WAITING("即将开始"),

    /**
     * 待发布
     */
    UPLOADABLE("待发布"),

    /**
     * 进行中
     */
    PROCESSING("进行中");

    private final String desc;

    PromotionStatus(String desc) {
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }
}
