package cn.hds.hvmall.enums;

/**
 * 商品的审核状态
 */
public enum ProductReviewStatus {
  NO_NEED_TO_CHECK, //未需审核
  WAIT_CHECK, //待审核
  CHECK_FAIL, //审核未通过
  CHECK_PASS, //审核通过
}
