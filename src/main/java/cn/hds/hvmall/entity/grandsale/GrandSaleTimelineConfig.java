package cn.hds.hvmall.entity.grandsale;

import java.util.Date;
import java.util.List;

/**
 * @author LiHaoYang
 * @ClassName GrandSaleTimelineConfig
 * @date 2019/6/25 0025
 */
public class GrandSaleTimelineConfig {

    /**
     * 大型促销活动表id
     */
    private Integer grandSaleId;

    /**
     * 是否在首页展示时间轴，0->不展示，1->展示
     */
    private Integer isShowTimeline;

    /**
     * 时间轴背景图片
     */
    private String timelineImg;

    /**
     * 活动开始时间
     */
    private String beginTime;

    /**
     * 活动结束时间
     */
    private String endTime;

    List<GrandSaleTimeline> grandSaleTimelineList;

    public Integer getGrandSaleId() {
        return grandSaleId;
    }

    public void setGrandSaleId(Integer grandSaleId) {
        this.grandSaleId = grandSaleId;
    }

    public Integer getShowTimeline() {
        return isShowTimeline;
    }

    public void setisShowTimeline(Integer isShowTimeline) {
        this.isShowTimeline = isShowTimeline;
    }

    public String getTimelineImg() {
        return timelineImg;
    }

    public void setTimelineImg(String timelineImg) {
        this.timelineImg = timelineImg;
    }

    public String getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(String beginTime) {
        this.beginTime = beginTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public List<GrandSaleTimeline> getGrandSaleTimelineList() {
        return grandSaleTimelineList;
    }

    public void setGrandSaleTimelineList(List<GrandSaleTimeline> grandSaleTimelineList) {
        this.grandSaleTimelineList = grandSaleTimelineList;
    }
}
