package cn.hds.hvmall.entity.grandsale;

import java.io.Serializable;
import java.util.Date;

/**
 * @author 
 */
public class GrandSaleShare implements Serializable {

    public GrandSaleShare() {
    }

    public GrandSaleShare(String content) {
        this.content = content;
    }

    private Long id;

    /**
     * 展示文案
     */
    private String content;

    /**
     * 文案开始时间
     */
    private Date startAt;

    /**
     * 对应文案结束
     */
    private Date endAt;

    /**
     * 活动类型
     */
    private String type;

    /**
     * 创建时间
     */
    private Date createdAt;

    /**
     * 更新时间
     */
    private Date updatedAt;


    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getStartAt() {
        return startAt;
    }

    public void setStartAt(Date startAt) {
        this.startAt = startAt;
    }

    public Date getEndAt() {
        return endAt;
    }

    public void setEndAt(Date endAt) {
        this.endAt = endAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
}