package cn.hds.hvmall.entity.grandsale;

import java.io.Serializable;
import java.util.Date;

/**
 * @author gxl
 */
public class GrandSaleTimeline implements Serializable {

    private Integer id;
    /**
     * 大型促销活动表id
     */
    private Integer grandSaleId;

    /**
     * 活动节点名称
     */
    private String name;

    /**
     * 活动时间节点
     */
    private String timeNode;

    /**
     * 活动持续时间（天为单位）
     */
    private String duration;



    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getGrandSaleId() {
        return grandSaleId;
    }

    public void setGrandSaleId(Integer grandSaleId) {
        this.grandSaleId = grandSaleId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTimeNode() {
        return timeNode;
    }

    public void setTimeNode(String timeNode) {
        this.timeNode = timeNode;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

}