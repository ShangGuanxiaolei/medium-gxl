package cn.hds.hvmall.entity;

import cn.hds.hvmall.utils.list.qiniu.IdTypeHandler;

import java.util.Date;
import java.util.Optional;

public abstract class BaseEntityImpl implements BaseEntity {

    private Long id;
    private Date createdAt;
    private Date updatedAt;

    @Override
    public Long getId() {
        return id;
    }

    /**
     * 兼容汉薇后台的IdHandler, 返回编码后的id, 若id为空则返回空字符串
     * @return 编码后的id
     */
    public String getIdEncoded() {
        return Optional.ofNullable(getId()).map(IdTypeHandler::encode)
                .orElse("");
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

}
