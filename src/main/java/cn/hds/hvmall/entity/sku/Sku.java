package cn.hds.hvmall.entity.sku;

import cn.hds.hvmall.entity.BaseEntityArchivable;
import lombok.ToString;

import java.math.BigDecimal;
import java.math.BigInteger;
@ToString
public class Sku extends BaseEntityArchivable {

    private static final long serialVersionUID = 1L;

    private String productId;

    private String spec;

    private String spec1;

    private String spec2;

    private String spec3;

    private String spec4;

    private String spec5;

    private BigDecimal marketPrice;

    private BigDecimal price;

    private BigDecimal originalPrice; //原始价格（用户录入价格）

    private Integer amount;

    private Integer secureAmount;

    private Integer order;

    private String skuUrl;

    private BigInteger partnerProductId; //第三方同步id
    private BigDecimal partnerProductPrice;

    private String skuId;

    private String barCode;

    private BigDecimal point;//积分

    private BigDecimal deductionDPoint;//德分

    private BigDecimal netWorth;//净值

    private String skuCode; // 第三方商品唯一编码

    private String skuCodeResources; // 第三方商品编码来源

    private String sourceSkuId; // 原商品skuid

    private Integer itemAmount; // 用于记录订单里本sku的数量，非数据库字段，仅前台计算逻辑需要

    private String attributes; // sku对应的规格属性值id，如白色，S码等

    private BigDecimal serverAmt = BigDecimal.ZERO; // 服务费
    private BigDecimal promoAmt = BigDecimal.ZERO; // 推广费

    private BigDecimal memberPrice = BigDecimal.ZERO;//会员价
    private BigDecimal proxyPrice = BigDecimal.ZERO;//代理价
    private BigDecimal changePrice = BigDecimal.ZERO;//兑换价现金部分
    private BigDecimal reducedPrice = BigDecimal.ZERO;//已减价格

    // 临时变量, 有活动的时候把活动价设置到里面
    private BigDecimal promotionPrice;

    //SKUS的图片
    private String skuImgUrl;

    private String skuImg;

    private Double height = 0.0;
    private Double width = 0.0;
    private Double length = 0.0;

    /**
     * 限购件数
     */
    private Integer buyLimit;

    /**
     * 原skuCode
     */
    private String sourceSkuCode;

    /**
     * 活动类型
     */
    private String promotionType;

    /**
     * 规格编码
     */
    private String code;

    public String getSkuImgUrl() {
        return skuImgUrl;
    }

    public void setSkuImgUrl(String skuImgUrl) {
        this.skuImgUrl = skuImgUrl;
    }

    public Double getHeight() {
        return height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public Double getWidth() {
        return width;
    }

    public void setWidth(Double width) {
        this.width = width;
    }

    public Double getLength() {
        return length;
    }

    public void setLength(Double length) {
        this.length = length;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public Integer getNumInPackage() {
        return numInPackage;
    }

    public void setNumInPackage(Integer numInPackage) {
        this.numInPackage = numInPackage;
    }

    private Integer weight;

    private Integer numInPackage = 0;

    public String getSkuImg() {
        return skuImg;
    }

    public void setSkuImg(String skuImg) {
        this.skuImg = skuImg;
    }


    public String getAttributes() {
        return attributes;
    }

    public void setAttributes(String attributes) {
        this.attributes = attributes;
    }

    public Integer getItemAmount() {
        return itemAmount;
    }

    public void setItemAmount(Integer itemAmount) {
        this.itemAmount = itemAmount;
    }

    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    public String getSkuCodeResources() {
        return skuCodeResources;
    }

    public void setSkuCodeResources(String skuCodeResources) {
        this.skuCodeResources = skuCodeResources;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getSpec() {
        return spec;
    }

    public void setSpec(String spec) {
        this.spec = spec;
    }

    public String getSpec1() {
        return spec1;
    }

    public void setSpec1(String spec1) {
        this.spec1 = spec1;
    }

    public String getSpec2() {
        return spec2;
    }

    public void setSpec2(String spec2) {
        this.spec2 = spec2;
    }

    public String getSpec3() {
        return spec3;
    }

    public void setSpec3(String spec3) {
        this.spec3 = spec3;
    }

    public String getSpec4() {
        return spec4;
    }

    public void setSpec4(String spec4) {
        this.spec4 = spec4;
    }

    public String getSpec5() {
        return spec5;
    }

    public void setSpec5(String spec5) {
        this.spec5 = spec5;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getPromotionPrice() {
        return promotionPrice;
    }

    public void setPromotionPrice(BigDecimal promotionPrice) {
        this.promotionPrice = promotionPrice;
    }

    public BigDecimal getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(BigDecimal originalPrice) {
        this.originalPrice = originalPrice;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public String getSkuUrl() {
        return skuUrl;
    }

    public void setSkuUrl(String skuUrl) {
        this.skuUrl = skuUrl;
    }

    public BigDecimal getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(BigDecimal marketPrice) {
        this.marketPrice = marketPrice;
    }

    public BigInteger getPartnerProductId() {
        return partnerProductId;
    }

    public void setPartnerProductId(BigInteger partnerProductId) {
        this.partnerProductId = partnerProductId;
    }

    public BigDecimal getPoint() {
        return point;
    }

    public void setPoint(BigDecimal point) {
        this.point = point;
    }

    public BigDecimal getDeductionDPoint() {
        return deductionDPoint;
    }

    public void setDeductionDPoint(BigDecimal deductionDPoint) {
        this.deductionDPoint = deductionDPoint;
    }

    public BigDecimal getNetWorth() {
        if (netWorth == null) {
            return BigDecimal.ZERO;
        }
        return netWorth;
    }

    public void setNetWorth(BigDecimal netWorth) {
        this.netWorth = netWorth;
    }

    public String getSkuId() {
        return skuId;
    }

    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    public String getSourceSkuId() {
        return sourceSkuId;
    }

    public void setSourceSkuId(String sourceSkuId) {
        this.sourceSkuId = sourceSkuId;
    }

    public Integer getSecureAmount() {
        return secureAmount;
    }

    public void setSecureAmount(Integer secureAmount) {
        this.secureAmount = secureAmount;
    }

    public String getBarCode() {
        return barCode;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

    public BigDecimal getPartnerProductPrice() {
        return partnerProductPrice;
    }

    public void setPartnerProductPrice(BigDecimal partnerProductPrice) {
        this.partnerProductPrice = partnerProductPrice;
    }

    public BigDecimal getServerAmt() {
        return serverAmt;
    }

    public void setServerAmt(BigDecimal serverAmt) {
        this.serverAmt = serverAmt;
    }

    public BigDecimal getPromoAmt() {
        return promoAmt;
    }

    public void setPromoAmt(BigDecimal promoAmt) {
        this.promoAmt = promoAmt;
    }

    public BigDecimal getMemberPrice() {
        return memberPrice;
    }

    public void setMemberPrice(BigDecimal memberPrice) {
        this.memberPrice = memberPrice;
    }

    public BigDecimal getProxyPrice() {
        return proxyPrice;
    }

    public void setProxyPrice(BigDecimal proxyPrice) {
        this.proxyPrice = proxyPrice;
    }

    public BigDecimal getChangePrice() {
        return changePrice;
    }

    public void setChangePrice(BigDecimal changePrice) {
        this.changePrice = changePrice;
    }

    public BigDecimal getReducedPrice() {
        return reducedPrice;
    }

    public void setReducedPrice(BigDecimal reducedPrice) {
        this.reducedPrice = reducedPrice;
    }

    public Integer getBuyLimit() {
        return buyLimit;
    }

    public void setBuyLimit(Integer buyLimit) {
        this.buyLimit = buyLimit;
    }

    public String getSourceSkuCode() {
        return sourceSkuCode;
    }

    public void setSourceSkuCode(String sourceSkuCode) {
        this.sourceSkuCode = sourceSkuCode;
    }

    public String getPromotionType() {
        return promotionType;
    }

    public void setPromotionType(String promotionType) {
        this.promotionType = promotionType;
    }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  /**
     * 获取兑换价
     */
    public BigDecimal getConversionPrice() {
        return this.getPrice()
                .subtract(
                        this.getDeductionDPoint().divide(BigDecimal.valueOf(10), 2, BigDecimal.ROUND_DOWN));
    }
}
