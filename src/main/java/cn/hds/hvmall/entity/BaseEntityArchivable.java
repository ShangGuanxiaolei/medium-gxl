package cn.hds.hvmall.entity;

/**
 * @author Jack Zhu
 * @date 2018/12/10
 */
public class BaseEntityArchivable extends BaseEntityImpl implements Archivable {

    protected boolean archive;

    @Override
    public Boolean getArchive() {
        return this.archive;
    }

    @Override
    public void setArchive(Boolean archive) {
        this.archive = archive;
    }
}