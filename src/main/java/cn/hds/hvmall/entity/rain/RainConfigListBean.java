package cn.hds.hvmall.entity.rain;

import org.hibernate.validator.constraints.NotBlank;

import java.util.Date;
import java.util.List;

public class RainConfigListBean {

    /**
     * rainId : 0
     * promotionDate : 1557504731000
     * pointTotal : 500
     * normalLimit : 15
     * vipLimit : 30
     */
    //红包雨id
    private Long rainId;
    //清单表id
    private Long promotionId;
    //红包雨日期
    private Date promotionDate;
    //总德分数
    private Integer pointTotal;
    //白人上限
    private Integer normalLimit;
    //非白人上线
    private Integer vipLimit;

    @NotBlank
    private List<PromotionPacketRainTime> rainTimeConfig;

    public List<PromotionPacketRainTime> getRainTimeConfig() {
        return rainTimeConfig;
    }

    public void setRainTimeConfig(List<PromotionPacketRainTime> rainTimeConfig) {
        this.rainTimeConfig = rainTimeConfig;
    }

    public Long getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(Long promotionId) {
        this.promotionId = promotionId;
    }

    public Long getRainId() {
        return rainId;
    }

    public void setRainId(Long rainId) {
        this.rainId = rainId;
    }

    public Date getPromotionDate() {
        return promotionDate;
    }

    public void setPromotionDate(Date promotionDate) {
        this.promotionDate = promotionDate;
    }

    public Integer getPointTotal() {
        return pointTotal;
    }

    public void setPointTotal(Integer pointTotal) {
        this.pointTotal = pointTotal;
    }

    public Integer getNormalLimit() {
        return normalLimit;
    }

    public void setNormalLimit(Integer normalLimit) {
        this.normalLimit = normalLimit;
    }

    public Integer getVipLimit() {
        return vipLimit;
    }

    public void setVipLimit(Integer vipLimit) {
        this.vipLimit = vipLimit;
    }

}
