package cn.hds.hvmall.entity.rain;

import java.util.Date;

/**
 * 活动清单的查询条件
 */
public class RainCondition {
  //活动清单id
  private String id;
  //活动类型
  private String promotionType;
  //活动状态
  private String state;
  //活动开始时间
  private Date triggerStartTime;
  //活动结束时间
  private Date triggerEndTime;
  //分页大小
  private Integer pageSize;
  //当前分页
  private Integer pageNum;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }


  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public String getPromotionType() {
    return promotionType;
  }

  public void setPromotionType(String promotionType) {
    this.promotionType = promotionType;
  }

  public Date getTriggerStartTime() {
    return triggerStartTime;
  }

  public void setTriggerStartTime(Date triggerStartTime) {
    this.triggerStartTime = triggerStartTime;
  }

  public Date getTriggerEndTime() {
    return triggerEndTime;
  }

  public void setTriggerEndTime(Date triggerEndTime) {
    this.triggerEndTime = triggerEndTime;
  }

  public Integer getPageSize() {
    return pageSize;
  }

  public void setPageSize(Integer pageSize) {
    this.pageSize = pageSize;
  }

  public Integer getPageNum() {
    return pageNum;
  }

  public void setPageNum(Integer pageNum) {
    this.pageNum = pageNum;
  }
}
