package cn.hds.hvmall.entity.rain;

import java.io.Serializable;
import java.util.Date;

public class RainTimResult implements Serializable {

	private Date triggerStartTime;

	private Date triggerEndTime;

	public Date getTriggerStartTime() {
		return triggerStartTime;
	}

	public void setTriggerStartTime(Date triggerStartTime) {
		this.triggerStartTime = triggerStartTime;
	}

	public Date getTriggerEndTime() {
		return triggerEndTime;
	}

	public void setTriggerEndTime(Date triggerEndTime) {
		this.triggerEndTime = triggerEndTime;
	}
}
