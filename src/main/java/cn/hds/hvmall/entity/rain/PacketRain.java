package cn.hds.hvmall.entity.rain;
import java.util.Date;

public class PacketRain  {
    /**
     * 订单id
     */
    private String id;
    /**
     * 订单创建时间
     */
    private Date createdAt;
    /**
     * 订单更新时间
     */
    private Date updatedAt;

    private int promotiomListId;
    /**
     * 活动日期
     */
    private Date promotionDate;
    private int pointTotle;
    private int normalLimit;
    private int vipLimit;
    private Boolean archive;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getPromotiomListId() {
        return promotiomListId;
    }

    public void setPromotiomListId(int promotiomListId) {
        this.promotiomListId = promotiomListId;
    }

    public Date getPromotionDate() {
        return promotionDate;
    }

    public void setPromotionDate(Date promotionDate) {
        this.promotionDate = promotionDate;
    }

    public int getPointTotle() {
        return pointTotle;
    }

    public void setPointTotle(int pointTotle) {
        this.pointTotle = pointTotle;
    }

    public int getNormalLimit() {
        return normalLimit;
    }

    public void setNormalLimit(int normalLimit) {
        this.normalLimit = normalLimit;
    }

    public int getVipLimit() {
        return vipLimit;
    }

    public void setVipLimit(int vipLimit) {
        this.vipLimit = vipLimit;
    }

    public Boolean getArchive() {
        return archive;
    }

    public void setArchive(Boolean archive) {
        this.archive = archive;
    }
}
