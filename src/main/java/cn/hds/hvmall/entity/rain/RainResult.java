package cn.hds.hvmall.entity.rain;

import java.io.Serializable;

/**
 * 用于数据回显
 */
public class RainResult implements Serializable {

	private PromotionPacketListVO promotionPacketListVO;

	public PromotionPacketListVO getPromotionPacketListVO() {
		return promotionPacketListVO;
	}

	public void setPromotionPacketListVO(PromotionPacketListVO promotionPacketListVO) {
		this.promotionPacketListVO = promotionPacketListVO;
	}
}
