package cn.hds.hvmall.entity.rain;

import java.io.Serializable;
import java.util.Date;

public class PromotionPacketRainTime implements Serializable {
  private Long id;
  //活动清单id
  private Long promotionPacketRainId;
  //红包雨每场开始时间
  private Date startTime;
  //红包雨每场结束时间
  private Date endTime;

  private Long rainId;

	public Long getRainId() {
		return rainId;
	}

	public void setRainId(Long rainId) {
		this.rainId = rainId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getPromotionPacketRainId() {
		return promotionPacketRainId;
	}

	public void setPromotionPacketRainId(Long promotionPacketRainId) {
		this.promotionPacketRainId = promotionPacketRainId;
	}

  public Date getStartTime() {
    return startTime;
  }

  public void setStartTime(Date startTime) {
    this.startTime = startTime;
  }

  public Date getEndTime() {
    return endTime;
  }

  public void setEndTime(Date endTime) {
    this.endTime = endTime;
  }

}
