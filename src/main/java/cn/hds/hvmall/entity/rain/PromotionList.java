package cn.hds.hvmall.entity.rain;
import java.util.Date;
public class PromotionList {
    /**
     * 订单id
     */
    private String id;
    /**
     * 订单创建时间
     */
    private Date createdAt;
    /**
     * 订单更新时间
     */
    private Date updatedAt;
    /**
     * 订单状态
     */
    private int state;
    /**
     * 触发开始时间
     */
    private Date triggerStartTime;
    /**
     *触发结束时间
     */
    private Date triggerEndTime;
    /**
     *
     */
    private Boolean archive;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public Date getTriggerStartTime() {
        return triggerStartTime;
    }

    public void setTriggerStartTime(Date triggerStartTime) {
        this.triggerStartTime = triggerStartTime;
    }

    public Date getTriggerEndTime() {
        return triggerEndTime;
    }

    public void setTriggerEndTime(Date triggerEndTime) {
        this.triggerEndTime = triggerEndTime;
    }

    public Boolean getArchive() {
        return archive;
    }

    public void setArchive(Boolean archive) {
        this.archive = archive;
    }
}
