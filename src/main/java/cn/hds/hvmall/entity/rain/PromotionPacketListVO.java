package cn.hds.hvmall.entity.rain;

import cn.hds.hvmall.entity.lottery.PromotionLottery;

import java.util.Date;
import java.util.List;

public class PromotionPacketListVO {
    //活动清单id
    private Long id;
    //活动开始时间
    private Date triggerStartTime;
    //活动结束时间
    private Date triggerEndTime;
    //红包雨活动规则
    private String activityStrategy;
    //红包雨活动攻略
    private String activityRule;
    //红包雨活动KV主图
    private String banner;
    //红包雨分享文案
    private String shareContent;

		private List<RainConfigListBean> rainConfigList;

	List<PromotionLottery> promotionLottery;

    public String getShareContent() {
        return shareContent;
    }

    public void setShareContent(String shareContent) {
        this.shareContent = shareContent;
    }

    public List<PromotionLottery> getPromotionLottery() {
		return promotionLottery;
	}

	public void setPromotionLottery(List<PromotionLottery> promotionLottery) {
		this.promotionLottery = promotionLottery;
	}

	public List<RainConfigListBean> getRainConfigList() {
		return rainConfigList;
	}

	public void setRainConfigList(List<RainConfigListBean> rainConfigList) {
		this.rainConfigList = rainConfigList;
	}

	public String getActivityStrategy() {
        return activityStrategy;
    }

    public void setActivityStrategy(String activityStrategy) {
        this.activityStrategy = activityStrategy;
    }

    public String getActivityRule() {
        return activityRule;
    }

    public void setActivityRule(String activityRule) {
        this.activityRule = activityRule;
    }

    public String getBanner() {
        return banner;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getTriggerStartTime() {
        return triggerStartTime;
    }

    public void setTriggerStartTime(Date triggerStartTime) {
        this.triggerStartTime = triggerStartTime;
    }

    public Date getTriggerEndTime() {
        return triggerEndTime;
    }

    public void setTriggerEndTime(Date triggerEndTime) {
        this.triggerEndTime = triggerEndTime;
    }

}
