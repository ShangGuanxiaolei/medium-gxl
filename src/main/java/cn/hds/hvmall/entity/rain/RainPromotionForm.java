package cn.hds.hvmall.entity.rain;

import cn.hds.hvmall.controller.RainLotteryPromotionForm;

import java.util.List;

public class RainPromotionForm extends RainLotteryPromotionForm {

  //红包雨基本配置
  private List<RainConfigListBean> rainConfigList;
  //红包雨场次时间配置

  public List<RainConfigListBean> getRainConfigList() {
    return rainConfigList;
  }

  public void setRainConfigList(List<RainConfigListBean> rainConfigList) {
    this.rainConfigList = rainConfigList;
  }
}
