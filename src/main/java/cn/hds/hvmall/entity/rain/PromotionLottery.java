package cn.hds.hvmall.entity.rain;

import java.util.Date;

/**
 * 抽奖配置
 */
public class PromotionLottery {
    /**
     * 订单id
     */
    private String id;
    /**
     * 订单创建时间
     */
    private Date createdAt;
    /**
     * 订单更新时间
     */
    private Date updatedAt;

    private Boolean archive;

    private int promotionListId;

    private Date promotionDate;
    /**
     * 第一等级得分
     */
    private int firstScore;
    /**
     * 第一等级得分概率
     */
    private double firstProbability;

    private int secondScore;

    private double secondProbability;

    private int thirdScore;

    private double thirdProbability;
    /**
     * 一等奖分数
     */
    private int firstRank;

    private int secomdRank;

    private int thirdRank;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Boolean getArchive() {
        return archive;
    }

    public void setArchive(Boolean archive) {
        this.archive = archive;
    }

    public int getPromotionListId() {
        return promotionListId;
    }

    public void setPromotionListId(int promotionListId) {
        this.promotionListId = promotionListId;
    }

    public Date getPromotionDate() {
        return promotionDate;
    }

    public void setPromotionDate(Date promotionDate) {
        this.promotionDate = promotionDate;
    }

    public int getFirstScore() {
        return firstScore;
    }

    public void setFirstScore(int firstScore) {
        this.firstScore = firstScore;
    }

    public int getSecondScore() {
        return secondScore;
    }

    public void setSecondScore(int secondScore) {
        this.secondScore = secondScore;
    }

    public int getThirdScore() {
        return thirdScore;
    }

    public void setThirdScore(int thirdScore) {
        this.thirdScore = thirdScore;
    }

    public int getFirstRank() {
        return firstRank;
    }

    public void setFirstRank(int firstRank) {
        this.firstRank = firstRank;
    }

    public int getSecomdRank() {
        return secomdRank;
    }

    public void setSecomdRank(int secomdRank) {
        this.secomdRank = secomdRank;
    }

    public int getThirdRank() {
        return thirdRank;
    }

    public void setThirdRank(int thirdRank) {
        this.thirdRank = thirdRank;
    }

    public double getFirstProbability() {
        return firstProbability;
    }

    public void setFirstProbability(double firstProbability) {
        this.firstProbability = firstProbability;
    }

    public double getSecondProbability() {
        return secondProbability;
    }

    public void setSecondProbability(double secondProbability) {
        this.secondProbability = secondProbability;
    }

    public double getThirdProbability() {
        return thirdProbability;
    }

    public void setThirdProbability(double thirdProbability) {
        this.thirdProbability = thirdProbability;
    }
}
