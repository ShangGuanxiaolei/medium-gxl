package cn.hds.hvmall.entity.rain;

import java.util.Date;

public class RainVO {
    private int rainId;
    private int promotionId;
    private Date promotionDate;
    private int pointTotal;
    private int normalLimit;
    private int vipLimit;

    public int getRainId() {
        return rainId;
    }

    public void setRainId(int rainId) {
        this.rainId = rainId;
    }

    public int getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(int promotionId) {
        this.promotionId = promotionId;
    }

    public Date getPromotionDate() {
        return promotionDate;
    }

    public void setPromotionDate(Date promotionDate) {
        this.promotionDate = promotionDate;
    }

    public int getPointTotal() {
        return pointTotal;
    }

    public void setPointTotal(int pointTotal) {
        this.pointTotal = pointTotal;
    }

    public int getNormalLimit() {
        return normalLimit;
    }

    public void setNormalLimit(int normalLimit) {
        this.normalLimit = normalLimit;
    }

    public int getVipLimit() {
        return vipLimit;
    }

    public void setVipLimit(int vipLimit) {
        this.vipLimit = vipLimit;
    }
}
