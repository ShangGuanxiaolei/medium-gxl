package cn.hds.hvmall.entity;

import java.io.Serializable;

public interface BaseEntity extends Serializable {

  Long getId();

}
