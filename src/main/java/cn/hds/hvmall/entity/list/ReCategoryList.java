package cn.hds.hvmall.entity.list;

import java.io.Serializable;
import java.util.List;

public class ReCategoryList implements Serializable {

	private String categoryName;

	private List<ReserveProductDetailInfo> reserveProductDetailInfo;

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public List<ReserveProductDetailInfo> getReserveProductDetailInfo() {
		return reserveProductDetailInfo;
	}

	public void setReserveProductDetailInfo(List<ReserveProductDetailInfo> reserveProductDetailInfo) {
		this.reserveProductDetailInfo = reserveProductDetailInfo;
	}
}
