package cn.hds.hvmall.entity.list;

import cn.hds.hvmall.utils.list.qiniu.json.JsonResourceUrlSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.io.Serializable;

/**
 * 用于数据回显的商品基本信息
 */
public class SearchProductInfoResult implements Serializable {

	private Long id;

	@JsonSerialize(using = JsonResourceUrlSerializer.class)
	private String productImg;

	private String productName;

	private Double price;

	private int amount;

	public String getProductImg() {
		return productImg;
	}

	public void setProductImg(String productImg) {
		this.productImg = productImg;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
