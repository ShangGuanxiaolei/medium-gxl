package cn.hds.hvmall.entity.list;

import java.io.Serializable;

public class ReserveProductResult implements Serializable {

	private SearchProductInfoResult searchProductInfoResult;

	private ReserveProductDetailInfo reserveProductDetailInfo;

	public SearchProductInfoResult getSearchProductInfoResult() {
		return searchProductInfoResult;
	}

	public void setSearchProductInfoResult(SearchProductInfoResult searchProductInfoResult) {
		this.searchProductInfoResult = searchProductInfoResult;
	}

	public ReserveProductDetailInfo getReserveProductDetailInfo() {
		return reserveProductDetailInfo;
	}

	public void setReserveProductDetailInfo(ReserveProductDetailInfo reserveProductDetailInfo) {
		this.reserveProductDetailInfo = reserveProductDetailInfo;
	}
}
