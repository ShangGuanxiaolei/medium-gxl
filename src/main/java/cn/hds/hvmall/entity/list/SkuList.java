package cn.hds.hvmall.entity.list;

public class SkuList {

  /**
   * sku表主键ID
   */
  private int skuId;

  /**
   * sku规格
   */
  private String spec;

  /**
   * sku规格1
   */
  private String spec1;

  /**
   * sku售价
   */
  private double skuPrice;

  /**
   * sku兑换价
   */
  private double conversionPrice;

  /**
   * sku立减
   */
  private double point;

  /**
   * sku净值
   */
  private double netWorth;

  /**
   * sku推广费
   */
  private double promoAmt;

  /**
   * sku服务费
   */
  private double serverAmt;

  /**
   * sku库存
   */
  private int skuAmount;

  public int getSkuId() {
    return skuId;
  }

  public void setSkuId(int skuId) {
    this.skuId = skuId;
  }

  public String getSpec() {
    return spec;
  }

  public void setSpec(String spec) {
    this.spec = spec;
  }

  public String getSpec1() {
    return spec1;
  }

  public void setSpec1(String spec1) {
    this.spec1 = spec1;
  }

  public double getSkuPrice() {
    return skuPrice;
  }

  public void setSkuPrice(double skuPrice) {
    this.skuPrice = skuPrice;
  }

  public double getConversionPrice() {
    return conversionPrice;
  }

  public void setConversionPrice(double conversionPrice) {
    this.conversionPrice = conversionPrice;
  }

  public double getPoint() {
    return point;
  }

  public void setPoint(double point) {
    this.point = point;
  }

  public double getNetWorth() {
    return netWorth;
  }

  public void setNetWorth(double netWorth) {
    this.netWorth = netWorth;
  }

  public double getPromoAmt() {
    return promoAmt;
  }

  public void setPromoAmt(double promoAmt) {
    this.promoAmt = promoAmt;
  }

  public double getServerAmt() {
    return serverAmt;
  }

  public void setServerAmt(double serverAmt) {
    this.serverAmt = serverAmt;
  }

  public int getSkuAmount() {
    return skuAmount;
  }

  public void setSkuAmount(int skuAmount) {
    this.skuAmount = skuAmount;
  }

}
