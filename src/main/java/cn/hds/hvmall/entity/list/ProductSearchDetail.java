package cn.hds.hvmall.entity.list;

import java.util.Objects;

/**
 * @Author: zzl
 * @Date: 2018/9/11 14:31
 * @Version
 * @description 封装返回给页面的数据对象
 */
public class ProductSearchDetail {

  /**
   * sku名称
   */
  private String name;
  /**
   * sku价格
   */
  private double price;
  /**
   * sku德分
   */
  private double DPoint;
  /**
   * 商品供应商
   */
  private String supplier;
  /**
   *  商品分类
   */
  private String category;
  /**
   *  商品品牌
   */
  private String brand;
  /**
   * 商品库存
   */
  private int amount;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public double getPrice() {
    return price;
  }

  public void setPrice(double price) {
    this.price = price;
  }

  public double getDPoint() {
    return DPoint;
  }

  public void setDPoint(double DPoint) {
    this.DPoint = DPoint;
  }

  public String getSupplier() {
    return supplier;
  }

  public void setSupplier(String supplier) {
    this.supplier = supplier;
  }

  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  public String getBrand() {
    return brand;
  }

  public void setBrand(String brand) {
    this.brand = brand;
  }

  public int getAmount() {
    return amount;
  }

  public void setAmount(int amount) {
    this.amount = amount;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ProductSearchDetail that = (ProductSearchDetail) o;
    return Double.compare(that.price, price) == 0 &&
        Double.compare(that.DPoint, DPoint) == 0 &&
        amount == that.amount &&
        Objects.equals(name, that.name) &&
        Objects.equals(supplier, that.supplier) &&
        Objects.equals(category, that.category) &&
        Objects.equals(brand, that.brand);
  }


}
