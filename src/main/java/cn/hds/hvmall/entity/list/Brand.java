package cn.hds.hvmall.entity.list;

import cn.hds.hvmall.pojo.list.ProductSource;
import cn.hds.hvmall.utils.list.qiniu.JsonResourceUrlSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.util.Date;
import java.util.Objects;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.Range;

/**
 * @Author: zzl
 * @Date: 2018/9/12 14:48
 * @Version
 */
public class Brand {

  private int id;

  /**
   * 品牌名
   */
  @NotBlank
  private String name;

  /**
   * 品牌主页
   */
  private String siteHost;

  /**
   * 品牌描述
   */
  private String description;

  /**
   * 排序字段
   */
  @NotNull
  @Range(min = 0, max = 999)
  private Integer sortOrder;

  /**
   * 品牌图片
   */
  @NotBlank
  @JsonSerialize(using = JsonResourceUrlSerializer.class )
  private String logo;

  private ProductSource source;

  private String sourceId;

  /**
   * 是否展示
   */
  private Boolean isShow = true;



  private Date createdAt;

  private Date updatedAt;

  private int archive;


  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSiteHost() {
    return siteHost;
  }

  public void setSiteHost(String siteHost) {
    this.siteHost = siteHost;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  @NotNull
  public Integer getSortOrder() {
    return sortOrder;
  }

  public void setSortOrder(@NotNull Integer sortOrder) {
    this.sortOrder = sortOrder;
  }

  public String getLogo() {
    return logo;
  }

  public void setLogo(String logo) {
    this.logo = logo;
  }

  public ProductSource getSource() {
    return source;
  }

  public void setSource(ProductSource source) {
    this.source = source;
  }

  public String getSourceId() {
    return sourceId;
  }

  public void setSourceId(String sourceId) {
    this.sourceId = sourceId;
  }

  public Boolean getShow() {
    return isShow;
  }

  public void setShow(Boolean show) {
    isShow = show;
  }

  public Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  public Date getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(Date updatedAt) {
    this.updatedAt = updatedAt;
  }

  public int getArchive() {
    return archive;
  }

  public void setArchive(int archive) {
    this.archive = archive;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Brand brand = (Brand) o;
    return id == brand.id &&
        archive == brand.archive &&
        Objects.equals(name, brand.name) &&
        Objects.equals(siteHost, brand.siteHost) &&
        Objects.equals(description, brand.description) &&
        Objects.equals(sortOrder, brand.sortOrder) &&
        Objects.equals(logo, brand.logo) &&
        source == brand.source &&
        Objects.equals(sourceId, brand.sourceId) &&
        Objects.equals(isShow, brand.isShow) &&
        Objects.equals(createdAt, brand.createdAt) &&
        Objects.equals(updatedAt, brand.updatedAt);
  }


}
