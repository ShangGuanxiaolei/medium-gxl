package cn.hds.hvmall.entity.list;

import java.io.Serializable;

/**
 * 用于传入的修改库存的信息
 */
public class ProductAmountInfo implements Serializable {

	private String pCode;

	private Integer skuId;

	/**
	 * 修改的新库存
	 */
	private Integer pAmount;

	public String getpCode() {
		return pCode;
	}

	public void setpCode(String pCode) {
		this.pCode = pCode;
	}

	public Integer getSkuId() {
		return skuId;
	}

	public void setSkuId(Integer skuId) {
		this.skuId = skuId;
	}

	public Integer getpAmount() {
		return pAmount;
	}

	public void setpAmount(Integer pAmount) {
		this.pAmount = pAmount;
	}
}
