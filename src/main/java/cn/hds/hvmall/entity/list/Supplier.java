package cn.hds.hvmall.entity.list;

import java.util.Date;
import java.util.Objects;

/**
 * @Author: zzl
 * @Date: 2018/9/12 9:25
 * @Version
 */
public class Supplier {

  /**
   * id
   */
  private int id;
  /**
   *
   */
  private int archive;
  /**
   * 更新时间
   */
  private Date updatedAt;
  /**
   * 创建时间
   */
  private Date createdAt;
  /**
   * 供应商名称
   */
  private String name;
  /**
   * 供应商编码
   */
  private String code;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public int getArchive() {
    return archive;
  }

  public void setArchive(int archive) {
    this.archive = archive;
  }

  public Date getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(Date updatedAt) {
    this.updatedAt = updatedAt;
  }

  public Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Supplier supplier = (Supplier) o;
    return id == supplier.id &&
        archive == supplier.archive &&
        Objects.equals(updatedAt, supplier.updatedAt) &&
        Objects.equals(createdAt, supplier.createdAt) &&
        Objects.equals(name, supplier.name) &&
        Objects.equals(code, supplier.code);
  }


}
