package cn.hds.hvmall.entity.list;

import cn.hds.hvmall.utils.list.qiniu.json.JsonResourceUrlSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.List;

/**
 * @Author: zzl
 * @Date: 2018/9/11 15:52
 * @Version
 * @description 封装查询所需数据
 */
public class ProductSearchSimple {

  /**
   * sku表商品ID
   */
  private int productId;

  /**
   * 分类
   */
  private String category;

  /**
   * 品牌
   */
  private String brand;

  /**
   * 商品名称
   */
  private String name;

  /**
   * 商品状态
   */
  private String status;

  /**
   * 商品价格
   */
  private String price;

  /**
   * 商品图片
   * */
  @JsonSerialize(using = JsonResourceUrlSerializer.class)
  private String img;

  private String spec;

  /**
   * 商品兑换价格
   */
  private String conversionPrice;

  /**
   * 商品库存
   */
  private int amount;

  /**
   * 商品销售量
   */
  private int sales;

  /**
   * 供应商
   */
  private String supplier;

  /**
   * 商品规格
   */
  private List<SkuList> skuList;

	public int getProductId() {
    return productId;
  }

  public void setProductId(int productId) {
    this.productId = productId;
  }

  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  public String getBrand() {
    return brand;
  }

  public void setBrand(String brand) {
    this.brand = brand;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getPrice() {
    return price;
  }

  public void setPrice(String price) {
    this.price = price;
  }

  public String getConversionPrice() {
    return conversionPrice;
  }

  public void setConversionPrice(String conversionPrice) {
    this.conversionPrice = conversionPrice;
  }

  public int getAmount() {
    return amount;
  }

  public void setAmount(int amount) {
    this.amount = amount;
  }

  public int getSales() {
    return sales;
  }

  public void setSales(int sales) {
    this.sales = sales;
  }

  public List<SkuList> getSkuList() {
    return skuList;
  }

  public void setSkuList(List<SkuList> skuList) {
    this.skuList = skuList;
  }

  public String getSupplier() {
    return supplier;
  }

  public void setSupplier(String supplier) {
    this.supplier = supplier;
  }

  public String getImg() {
    return img;
  }

  public void setImg(String img) {
    this.img = img;
  }

  public String getSpec() {
    return spec;
  }

  public void setSpec(String spec) {
    this.spec = spec;
  }
}
