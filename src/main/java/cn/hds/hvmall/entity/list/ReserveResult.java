package cn.hds.hvmall.entity.list;

import java.util.Date;

/**
 * 用于数据回显的基本类
 */
public class ReserveResult {

	private Long promotionId;

	private String name;

	private Date effectFromTime;

	private Date effectToTime;

	private Date payFromTime;

	private Date payToTime;

	private Integer targetPerson;

	private Integer isCountEarning;

	private Integer isDeliveryReduction;

	public Long getPromotionId() {
		return promotionId;
	}

	public void setPromotionId(Long promotionId) {
		this.promotionId = promotionId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getEffectFromTime() {
		return effectFromTime;
	}

	public void setEffectFromTime(Date effectFromTime) {
		this.effectFromTime = effectFromTime;
	}

	public Date getEffectToTime() {
		return effectToTime;
	}

	public void setEffectToTime(Date effectToTime) {
		this.effectToTime = effectToTime;
	}

	public Date getPayFromTime() {
		return payFromTime;
	}

	public void setPayFromTime(Date payFromTime) {
		this.payFromTime = payFromTime;
	}

	public Date getPayToTime() {
		return payToTime;
	}

	public void setPayToTime(Date payToTime) {
		this.payToTime = payToTime;
	}

	public Integer getTargetPerson() {
		return targetPerson;
	}

	public void setTargetPerson(Integer targetPerson) {
		this.targetPerson = targetPerson;
	}

	public Integer getIsCountEarning() {
		return isCountEarning;
	}

	public void setIsCountEarning(Integer isCountEarning) {
		this.isCountEarning = isCountEarning;
	}

	public Integer getIsDeliveryReduction() {
		return isDeliveryReduction;
	}

	public void setIsDeliveryReduction(Integer isDeliveryReduction) {
		this.isDeliveryReduction = isDeliveryReduction;
	}
}
