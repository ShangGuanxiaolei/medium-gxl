package cn.hds.hvmall.entity.list;

import java.util.List;

public class ProductLists {

	private Long recommendId;

	private String name;

	private String code;

	private Integer strongPush;

	public Integer getStrongPush() {
		return strongPush;
	}

	public void setStrongPush(Integer strongPush) {
		this.strongPush = strongPush;
	}

	public Long getRecommendId() {
		return recommendId;
	}

	public void setRecommendId(Long recommendId) {
		this.recommendId = recommendId;
	}

	private List<Long> productLists;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public List<Long> getProductLists() {
		return productLists;
	}

	public void setProductLists(List<Long> productLists) {
		this.productLists = productLists;
	}




}
