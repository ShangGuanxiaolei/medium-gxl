package cn.hds.hvmall.entity.list;

import cn.hds.hvmall.utils.list.qiniu.json.JsonResourceUrlSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.math.BigDecimal;

public class ProductSkuSimple {

  /**
   * sku表商品ID
   */
  private Integer productId;

  /**
   * 商品名称
   */
  private String name;

  /**
   * 商品状态
   */
  private String status;

  /**
   * 商品价格
   */
  private BigDecimal price;

  /**
   * 商品库存
   */
  private int amount;

  /**
   * sku编码
   */
  private String skuCode;

  /**
   * sku规格
   */
  private String spec;

  /**
   * 商品图片
   */
  @JsonSerialize(using = JsonResourceUrlSerializer.class)
  private String img;

  /**
   * skuId
   */
  private Integer skuId;

  public Integer getProductId() {
    return productId;
  }

  public void setProductId(Integer productId) {
    this.productId = productId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public BigDecimal getPrice() {
    return price;
  }

  public void setPrice(BigDecimal price) {
    this.price = price;
  }

  public int getAmount() {
    return amount;
  }

  public void setAmount(int amount) {
    this.amount = amount;
  }

  public String getSkuCode() {
    return skuCode;
  }

  public void setSkuCode(String skuCode) {
    this.skuCode = skuCode;
  }

  public String getSpec() {
    return spec;
  }

  public void setSpec(String spec) {
    this.spec = spec;
  }

  public String getImg() {
    return img;
  }

  public void setImg(String img) {
    this.img = img;
  }

  public Integer getSkuId() {
    return skuId;
  }

  public void setSkuId(Integer skuId) {
    this.skuId = skuId;
  }
}
