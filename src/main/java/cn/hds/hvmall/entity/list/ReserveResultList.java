package cn.hds.hvmall.entity.list;

import java.io.Serializable;
import java.util.Date;

/**
 * 用于页面展示的预约预售列表的类
 */
public class ReserveResultList implements Serializable {

	private String reserveId;

	public String getReserveId() {
		return reserveId;
	}

	public void setReserveId(String reserveId) {
		this.reserveId = reserveId;
	}

	private String name;

	private Date effectFromTime;

	private Date effectToTime;

	private Date payFromTime;

	private Date payToTime;

	private Integer status;

	private Date publishTime;


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getEffectFromTime() {
		return effectFromTime;
	}

	public void setEffectFromTime(Date effectFromTime) {
		this.effectFromTime = effectFromTime;
	}

	public Date getEffectToTime() {
		return effectToTime;
	}

	public void setEffectToTime(Date effectToTime) {
		this.effectToTime = effectToTime;
	}

	public Date getPayFromTime() {
		return payFromTime;
	}

	public void setPayFromTime(Date payFromTime) {
		this.payFromTime = payFromTime;
	}

	public Date getPayToTime() {
		return payToTime;
	}

	public void setPayToTime(Date payToTime) {
		this.payToTime = payToTime;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Date getPublishTime() {
		return publishTime;
	}

	public void setPublishTime(Date publishTime) {
		this.publishTime = publishTime;
	}
}
