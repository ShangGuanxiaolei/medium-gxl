package cn.hds.hvmall.entity.list;

import java.io.Serializable;
import java.util.List;

/**
 * 用于封装需要显示预约活动详情数据回显
 */
public class ReserveDetailResult implements Serializable {

	private ReserveResult reserveResult;

	private List<ReCategoryList> ReCategoryList;

	public List<ReCategoryList> getReCategoryList() {
		return ReCategoryList;
	}

	public void setReCategoryList(List<ReCategoryList> reCategoryList) {
		ReCategoryList = reCategoryList;
	}

	public ReserveResult getReserveResult() {
		return reserveResult;
	}

	public void setReserveResult(ReserveResult reserveResult) {
		this.reserveResult = reserveResult;
	}

}
