package cn.hds.hvmall.entity.list;

import java.math.BigDecimal;

public class ProductSkuSimpleVO extends ProductSkuSimple {


  /**
   * 德分
   */
  private BigDecimal deductionDPoint = BigDecimal.ZERO;

  /**
   * 立减
   */
  private BigDecimal point = BigDecimal.ZERO;

  /**
   * 净值
   */
  private BigDecimal netWorth = BigDecimal.ZERO;

  /**
   * 服务费
   */
  private BigDecimal serverAmt = BigDecimal.ZERO;

  /**
   * 推广费
   */
  private BigDecimal promoAmt = BigDecimal.ZERO;

  /**
   * 限购件数
   */
  private Integer buyLimit;

  /**
   * 兑换价
   */
  private BigDecimal convertPrice;

  /**
   * 源sku编码
   */
  private String sourceSkuCode;

  /**
   * 原商品库存
   */
  private Integer skuAmount;

  /**
   * 源商品价格
   */
  private BigDecimal skuPrice;

  public BigDecimal getDeductionDPoint() {
    return deductionDPoint;
  }

  public void setDeductionDPoint(BigDecimal deductionDPoint) {
    this.deductionDPoint = deductionDPoint;
  }

  public BigDecimal getPoint() {
    return point;
  }

  public void setPoint(BigDecimal point) {
    this.point = point;
  }

  public BigDecimal getNetWorth() {
    return netWorth;
  }

  public void setNetWorth(BigDecimal netWorth) {
    this.netWorth = netWorth;
  }

  public BigDecimal getServerAmt() {
    return serverAmt;
  }

  public void setServerAmt(BigDecimal serverAmt) {
    this.serverAmt = serverAmt;
  }

  public BigDecimal getPromoAmt() {
    return promoAmt;
  }

  public void setPromoAmt(BigDecimal promoAmt) {
    this.promoAmt = promoAmt;
  }

  public Integer getBuyLimit() {
    return buyLimit;
  }

  public void setBuyLimit(Integer buyLimit) {
    this.buyLimit = buyLimit;
  }

  public BigDecimal getConvertPrice() {
    return convertPrice;
  }

  public void setConvertPrice(BigDecimal convertPrice) {
    this.convertPrice = convertPrice;
  }

  public String getSourceSkuCode() {
    return sourceSkuCode;
  }

  public void setSourceSkuCode(String sourceSkuCode) {
    this.sourceSkuCode = sourceSkuCode;
  }

  public Integer getSkuAmount() {
    return skuAmount;
  }

  public void setSkuAmount(Integer skuAmount) {
    this.skuAmount = skuAmount;
  }

  public BigDecimal getSkuPrice() {
    return skuPrice;
  }

  public void setSkuPrice(BigDecimal skuPrice) {
    this.skuPrice = skuPrice;
  }
}
