package cn.hds.hvmall.entity.list;

/**
 * Created with IntelliJ IDEA.
 * User: chp
 * Date: 2019/4/12
 * Time: 14:39
 * Description: 推荐商品新增字段
 */
public class ProductListsVo extends ProductLists {

    private  Integer  strongPush ;

    public Integer getStrongPush() {
        return strongPush;
    }

    public void setStrongPush(Integer strongPush) {
        this.strongPush = strongPush;
    }
}
