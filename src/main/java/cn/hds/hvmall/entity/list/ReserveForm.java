package cn.hds.hvmall.entity.list;
import cn.hds.hvmall.entity.reserve.PromotionReserve;
import cn.hds.hvmall.entity.reserve.PromotionReserveProduct;

import java.io.Serializable;
import java.util.List;

/**
 * 添加预约所需要传入的内容类
 */
public class ReserveForm implements Serializable {

	private PromotionReserve promotionReserve;

	private List<PromotionReserveProduct> reserveProductList;

	public List<PromotionReserveProduct> getReserveProductList() {
		return reserveProductList;
	}

	public void setReserveProductList(List<PromotionReserveProduct> reserveProductList) {
		this.reserveProductList = reserveProductList;
	}

	public PromotionReserve getPromotionReserve() {
		return promotionReserve;
	}

	public void setPromotionReserve(PromotionReserve promotionReserve) {
		this.promotionReserve = promotionReserve;
	}


}
