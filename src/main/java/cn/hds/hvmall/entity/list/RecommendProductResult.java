package cn.hds.hvmall.entity.list;

import java.util.List;

/**
 * 修改时返回的推荐类
 */
public class RecommendProductResult {

	private String name;

	private String code;

	private List<ProductSearchResult> productSearchResultList;

	private List<Long> ids;

	public List<ProductSearchResult> getProductSearchResultList() {
		return productSearchResultList;
	}

	public void setProductSearchResultList(List<ProductSearchResult> productSearchResultList) {
		this.productSearchResultList = productSearchResultList;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public List<Long> getIds() {
		return ids;
	}

	public void setIds(List<Long> ids) {
		this.ids = ids;
	}
}
