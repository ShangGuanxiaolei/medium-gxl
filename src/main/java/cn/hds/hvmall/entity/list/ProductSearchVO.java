package cn.hds.hvmall.entity.list;

import cn.hds.hvmall.utils.list.qiniu.JsonResourceUrlSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * @Author: zzl
 * @Date: 2018/9/17 14:00
 * @Version
 */
public class ProductSearchVO{

  /**
   * sku名称
   */
  private String name;
  /**
   * sku售价
   */
  private double price;
  /**
   * sku德分
   */
  private double DPoint;
  /**
   * 商品库存
   */
  private int amount;
  /**
   * 商品图片
   */
  @JsonSerialize(using = JsonResourceUrlSerializer.class )
  private String img;

  /**
   * 商品唯一标识码
   */
  private String skuCode;

  /**
   * sku表主键ID
   */
  private int skuId;
  /**
   * sku表商品ID
   */
  private int skuProductId;

  /**
   * 供应商名称
   */
  private String supname;

  /**
   * 品牌
   */
  private String brand;

  /**
   * 分类
   */
  private String category;

  public String getBrand() {
    return brand;
  }

  public void setBrand(String brand) {
    this.brand = brand;
  }

  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  public String getSkuCode() {
    return skuCode;
  }

  public void setSkuCode(String skuCode) {
    this.skuCode = skuCode;
  }

  public int getSkuId() {
    return skuId;
  }

  public void setSkuId(int skuId) {
    this.skuId = skuId;
  }

  public int getSkuProductId() {
    return skuProductId;
  }

  public void setSkuProductId(int skuProductId) {
    this.skuProductId = skuProductId;
  }

  public String getSupname() {
    return supname;
  }

  public void setSupname(String supname) {
    this.supname = supname;
  }


  public String getName() {
    return name;
  }


  public void setName(String name) {
    this.name = name;
  }


  public double getPrice() {
    return price;
  }


  public void setPrice(double price) {
    this.price = price;
  }


  public double getDPoint() {
    return DPoint;
  }


  public void setDPoint(double DPoint) {
    this.DPoint = DPoint;
  }


  public int getAmount() {
    return amount;
  }


  public void setAmount(int amount) {
    this.amount = amount;
  }


  public String getImg() {
    return img;
  }


  public void setImg(String img) {
    this.img = img;
  }


}
