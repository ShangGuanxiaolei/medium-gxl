package cn.hds.hvmall.entity.list;

import cn.hds.hvmall.utils.list.qiniu.json.JsonResourceUrlSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 预约预售商品详细的信息
 */
public class ReserveProductDetailInfo implements Serializable {

	private Long id;

	private Long productId;

	private Long skuId;

	private int preReserveLimit;

	public int getPreReserveLimit() {
		return preReserveLimit;
	}

	public void setPreReserveLimit(int preReserveLimit) {
		this.preReserveLimit = preReserveLimit;
	}

	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	private Long reserveId;

	public Long getReserveId() {
		return reserveId;
	}

	public void setReserveId(Long reserveId) {
		this.reserveId = reserveId;
	}

	@JsonSerialize(using = JsonResourceUrlSerializer.class)
	private String productImg;

	private String productName;

	private Double price;

	private int amount;

	private BigDecimal reservePrice;

	private BigDecimal conversionPrice;

	private Long point;

	private int amountLimit;

	private int buyLimit;

	private int reserveLimit;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getProductImg() {
		return productImg;
	}

	public void setProductImg(String productImg) {
		this.productImg = productImg;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public BigDecimal getReservePrice() {
		return reservePrice;
	}

	public void setReservePrice(BigDecimal reservePrice) {
		this.reservePrice = reservePrice;
	}

	public BigDecimal getConversionPrice() {
		return conversionPrice;
	}

	public void setConversionPrice(BigDecimal conversionPrice) {
		this.conversionPrice = conversionPrice;
	}

	public Long getPoint() {
		return point;
	}

	public void setPoint(Long point) {
		this.point = point;
	}

	public int getAmountLimit() {
		return amountLimit;
	}

	public void setAmountLimit(int amountLimit) {
		this.amountLimit = amountLimit;
	}

	public int getBuyLimit() {
		return buyLimit;
	}

	public void setBuyLimit(int buyLimit) {
		this.buyLimit = buyLimit;
	}

	public int getReserveLimit() {
		return reserveLimit;
	}

	public void setReserveLimit(int reserveLimit) {
		this.reserveLimit = reserveLimit;
	}
}
