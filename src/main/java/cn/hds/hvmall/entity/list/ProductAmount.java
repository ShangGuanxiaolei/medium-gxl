package cn.hds.hvmall.entity.list;

import cn.hds.hvmall.entity.piece.PromotionSkus;
import cn.hds.hvmall.entity.piece.XquarkSku;

import java.util.List;

public class ProductAmount {

	private long productId;


	private long skuId;

	private String productName;

	private String spec;

	private String spec1;

	private String spec2;

	private String spec3;

	private String spec4;

	private String spec5;

	public String getSpec1() {
		return spec1;
	}

	public void setSpec1(String spec1) {
		this.spec1 = spec1;
	}



	public String getSpec2() {
		return spec2;
	}

	public void setSpec2(String spec2) {
		this.spec2 = spec2;
	}

	public String getSpec3() {
		return spec3;
	}

	public void setSpec3(String spec3) {
		this.spec3 = spec3;
	}

	public String getSpec4() {
		return spec4;
	}

	public void setSpec4(String spec4) {
		this.spec4 = spec4;
	}

	public String getSpec5() {
		return spec5;
	}

	public void setSpec5(String spec5) {
		this.spec5 = spec5;
	}

	public String getSpec() {
		return spec;
	}

	public void setSpec(String spec) {
		this.spec = spec;
	}

	private Double groupPrice;

	private int amount;


	public long getSkuId() {
		return skuId;
	}

	public void setSkuId(long skuId) {
		this.skuId = skuId;
	}



	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}


	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}


	public Double getGroupPrice() {
		return groupPrice;
	}

	public void setGroupPrice(Double groupPrice) {
		this.groupPrice = groupPrice;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}


}
