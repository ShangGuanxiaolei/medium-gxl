package cn.hds.hvmall.entity.news;

import java.util.List;

/**
 * @author LiHaoYang
 * @ClassName NewsInfo
 * @date 2019-8-13
 */
public class NewsInfo {

    private String type;
    private List<NewsDetail> detailList;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<NewsDetail> getDetailList() {
        return detailList;
    }

    public void setDetailList(List<NewsDetail> detailList) {
        this.detailList = detailList;
    }
}
