package cn.hds.hvmall.entity.news;

/**
 * @author LiHaoYang
 * @ClassName News
 * @date 2019-8-6
 */
public class NewsType {

    private int id;
    private String typeName;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }
}
