package cn.hds.hvmall.entity.pieceorder;

import cn.hds.hvmall.utils.list.qiniu.JsonResourceUrlSerializer;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.util.Date;
import java.util.Objects;

/**
 * @Author: zzl
 * @Date: 2018/9/17 19:45
 * @Version
 *
 * 封装查询结果信息对象
 */
public class OrderSearchVO {

  /**
   * 订单编号
   */
  private String orderNo;
  /**
   * 商品名称(xquark_order_item表中的product_name
   */
  private String productName;
  /**
   * 商品图片
   */
  @JsonSerialize(using = JsonResourceUrlSerializer.class )
  private String productImg;
  /**
   * 买家姓名
   */
  private String buyerName;
  /**
   * 买家号码
   */
  private String buyerPhone;
  /**
   * 订单创建时间
   */
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
  private Date createdAt;
  /**
   * 订单状态
   */
  private String status;
  /**
   * 商品购买数量
   */
  private int amount;
  /**
   * 商品价格(price
   */
  private double price;
  /**
   * 收件人姓名
   */
  private String consignee;

  public OrderSearchVO() {
  }

  public OrderSearchVO(String orderNo, String productName, String productImg,
      String buyerName, String buyerPhone, Date createdAt, String status, int amount, double price,
      String consignee) {
    this.orderNo = orderNo;
    this.productName = productName;
    this.productImg = productImg;
    this.buyerName = buyerName;
    this.buyerPhone = buyerPhone;
    this.createdAt = createdAt;
    this.status = status;
    this.amount = amount;
    this.price = price;
    this.consignee = consignee;
  }

  public String getOrderNo() {
    return orderNo;
  }

  public void setOrderNo(String orderId) {
    this.orderNo = orderId;
  }

  public String getProductName() {
    return productName;
  }

  public void setProductName(String productName) {
    this.productName = productName;
  }

  public String getProductImg() {
    return productImg;
  }

  public void setProductImg(String productImg) {
    this.productImg = productImg;
  }

  public String getBuyerName() {
    return buyerName;
  }

  public void setBuyerName(String buyerName) {
    this.buyerName = buyerName;
  }

  public String getBuyerPhone() {
    return buyerPhone;
  }

  public void setBuyerPhone(String buyerPhone) {
    this.buyerPhone = buyerPhone;
  }

  public Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public int getAmount() {
    return amount;
  }

  public void setAmount(int amount) {
    this.amount = amount;
  }

  public double getPrice() {
    return price;
  }

  public void setPrice(double price) {
    this.price = price;
  }

  public String getConsignee() {
    return consignee;
  }

  public void setConsignee(String consignee) {
    this.consignee = consignee;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    OrderSearchVO that = (OrderSearchVO) o;
    return amount == that.amount &&
        Double.compare(that.price, price) == 0 &&
        Objects.equals(orderNo, that.orderNo) &&
        Objects.equals(productName, that.productName) &&
        Objects.equals(productImg, that.productImg) &&
        Objects.equals(buyerName, that.buyerName) &&
        Objects.equals(buyerPhone, that.buyerPhone) &&
        Objects.equals(createdAt, that.createdAt) &&
        Objects.equals(status, that.status) &&
        Objects.equals(consignee, that.consignee);
  }

  @Override
  public String toString() {
    return "OrderSearchVO{" +
        "orderNo='" + orderNo + '\'' +
        ", productName='" + productName + '\'' +
        ", productImg='" + productImg + '\'' +
        ", buyerName='" + buyerName + '\'' +
        ", buyerPhone='" + buyerPhone + '\'' +
        ", createdAt=" + createdAt +
        ", status='" + status + '\'' +
        ", amount=" + amount +
        ", price=" + price +
        ", consignee='" + consignee + '\'' +
        '}';
  }
}
