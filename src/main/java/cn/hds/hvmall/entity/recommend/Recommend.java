package cn.hds.hvmall.entity.recommend;

import java.io.Serializable;
import java.util.Date;

/**
 * @author 
 */
public class Recommend implements Serializable {
    private Long id;

    /**
     * 推荐名称
     */
    private String name;

    /**
     * 关联推荐页面
     */
    private String code;

    /**
     * 推荐商品数量
     */
    private Integer nummber;

    /**
     * 创建时间
     */
    private Date createdAt;

    /**
     * 修改时间
     */
    private Date updatedAt;

    /**
     * 逻辑删除 0-正常  1-已删除
     */
    private Byte archive;

    private Integer strongPush;

    private static final long serialVersionUID = 1L;

    public Integer getStrongPush() {
        return strongPush;
    }

    public void setStrongPush(Integer strongPush) {
        this.strongPush = strongPush;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getNummber() {
        return nummber;
    }

    public void setNummber(Integer nummber) {
        this.nummber = nummber;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Byte getArchive() {
        return archive;
    }

    public void setArchive(Byte archive) {
        this.archive = archive;
    }

}