package cn.hds.hvmall.entity.recommend;

/**
 * Created with IntelliJ IDEA.
 * User: chp
 * Date: 2019/4/12
 * Time: 14:48
 * Description: 商品推荐新增强推
 */
public class RecommendVo  extends Recommend{
    private  Integer strongPush;

    public Integer getStrongPush() {
        return strongPush;
    }

    public void setStrongPush(Integer strongPush) {
        this.strongPush = strongPush;
    }
}
