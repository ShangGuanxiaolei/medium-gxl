package cn.hds.hvmall.entity.recommend;

public class SelectCondition {

  private int pageSize;

  private Long recommendId;

  private int pageNum;   //分页码

  private long id;

  private String name;//商品名称

  private Long supplier;//供应商

  private Long brand;//商品品牌

  //分类id
  private Long category;

  public int getPageSize() {
    return pageSize;
  }

  public void setPageSize(int pageSize) {
    this.pageSize = pageSize;
  }

  public Long getRecommendId() {
    return recommendId;
  }

  public void setRecommendId(Long recommendId) {
    this.recommendId = recommendId;
  }

  public int getPageNum() {
    return pageNum;
  }

  public void setPageNum(int pageNum) {
    this.pageNum = pageNum;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Long getSupplier() {
    return supplier;
  }

  public void setSupplier(Long supplier) {
    this.supplier = supplier;
  }

  public Long getBrand() {
    return brand;
  }

  public void setBrand(Long brand) {
    this.brand = brand;
  }

  public Long getCategory() {
    return category;
  }

  public void setCategory(Long category) {
    this.category = category;
  }
}
