package cn.hds.hvmall.entity.reserve;

import java.io.Serializable;
import java.util.List;

public class PromotionForm implements Serializable {

	private Long reserveId;

	private List<PromotionAmount> promotionAmountList;

	public Long getReserveId() {
		return reserveId;
	}

	public void setReserveId(Long reserveId) {
		this.reserveId = reserveId;
	}

	public List<PromotionAmount> getPromotionAmountList() {
		return promotionAmountList;
	}

	public void setPromotionAmountList(List<PromotionAmount> promotionAmountList) {
		this.promotionAmountList = promotionAmountList;
	}
}
