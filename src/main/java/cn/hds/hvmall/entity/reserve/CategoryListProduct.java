package cn.hds.hvmall.entity.reserve;

import java.io.Serializable;
import java.util.List;

public class CategoryListProduct implements Serializable {

	private String categoryName;

	private List<PromotionReserveProduct> productList;

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public List<PromotionReserveProduct> getProductList() {
		return productList;
	}

	public void setProductList(List<PromotionReserveProduct> productList) {
		this.productList = productList;
	}
}
