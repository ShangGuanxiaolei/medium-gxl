package cn.hds.hvmall.entity.reserve;

import java.io.Serializable;
import java.util.Date;

/**
 * @author
 */
public class PromotionReserve implements Serializable {


	private String reserveCode;

	public String getReserveCode() {
		return reserveCode;
	}

	public void setReserveCode(String reserveCode) {
		this.reserveCode = reserveCode;
	}

	/**
	 * 活动编号
	 */
	private Long id;

	/**
	 * 活动名称
	 */
	private String name;

	/**
	 * 活动标题
	 */
	private String title;

	/**
	 * 活动状态(１.即将开始,2.进行中,3.已结束,4.已终止)
	 */
	private Integer status;

	/**
	 * 预约活动开始时间
	 */
	private Date effectFromTime;

	/**
	 * 预约活动结束时间
	 */
	private Date effectToTime;

	/**
	 * 支付开始时间
	 */
	private Date payFromTime;

	/**
	 * 支付结束时间
	 */
	private Date payToTime;

	/**
	 * 发布时间
	 */
	private Date publishTime;

	/**
	 * 定向人群(1.不限,2.非会员(新人＋白人),3.会员(vip+店主))
	 */
	private Integer targetPerson;

	/**
	 * 是否计算收益(0.不计算,1.计算)
	 */
	private Integer isCountEarning;

	/**
	 * 是否包邮(0.不包邮,1.包邮)
	 */
	private Integer isDeliveryReduction;

	private Date createTime;

	private Date updateTime;

	private boolean isArchive;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public boolean isArchive() {
		return isArchive;
	}

	public void setArchive(boolean archive) {
		isArchive = archive;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	private static final long serialVersionUID = 1L;


	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Date getEffectFromTime() {
		return effectFromTime;
	}

	public void setEffectFromTime(Date effectFromTime) {
		this.effectFromTime = effectFromTime;
	}

	public Date getEffectToTime() {
		return effectToTime;
	}

	public void setEffectToTime(Date effectToTime) {
		this.effectToTime = effectToTime;
	}

	public Date getPayFromTime() {
		return payFromTime;
	}

	public void setPayFromTime(Date payFromTime) {
		this.payFromTime = payFromTime;
	}

	public Date getPayToTime() {
		return payToTime;
	}

	public void setPayToTime(Date payToTime) {
		this.payToTime = payToTime;
	}

	public Date getPublishTime() {
		return publishTime;
	}

	public void setPublishTime(Date publishTime) {
		this.publishTime = publishTime;
	}

	public Integer getTargetPerson() {
		return targetPerson;
	}

	public void setTargetPerson(Integer targetPerson) {
		this.targetPerson = targetPerson;
	}

	public Integer getIsCountEarning() {
		return isCountEarning;
	}

	public void setIsCountEarning(Integer isCountEarning) {
		this.isCountEarning = isCountEarning;
	}

	public Integer getIsDeliveryReduction() {
		return isDeliveryReduction;
	}

	public void setIsDeliveryReduction(Integer isDeliveryReduction) {
		this.isDeliveryReduction = isDeliveryReduction;
	}
}