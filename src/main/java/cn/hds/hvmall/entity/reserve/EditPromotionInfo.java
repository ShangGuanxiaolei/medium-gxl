package cn.hds.hvmall.entity.reserve;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 编辑活动传入信息的类
 */
public class EditPromotionInfo implements Serializable {

	private String reserveId;

	public String getReserveId() {
		return reserveId;
	}

	public void setReserveId(String reserveId) {
		this.reserveId = reserveId;
	}

	private String name;

	private Date effectFromTime;

	private Date effectToTime;

	private Date payFromTime;

	private Date payToTime;

	private Integer targetPerson;

	private int amountLimit;

	private List<PromotionReserveProduct> promotionReserveProduct;

	public List<PromotionReserveProduct> getPromotionReserveProduct() {
		return promotionReserveProduct;
	}

	public void setPromotionReserveProduct(List<PromotionReserveProduct> promotionReserveProduct) {
		this.promotionReserveProduct = promotionReserveProduct;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getEffectFromTime() {
		return effectFromTime;
	}

	public void setEffectFromTime(Date effectFromTime) {
		this.effectFromTime = effectFromTime;
	}

	public Date getEffectToTime() {
		return effectToTime;
	}

	public void setEffectToTime(Date effectToTime) {
		this.effectToTime = effectToTime;
	}

	public Date getPayFromTime() {
		return payFromTime;
	}

	public void setPayFromTime(Date payFromTime) {
		this.payFromTime = payFromTime;
	}

	public Date getPayToTime() {
		return payToTime;
	}

	public void setPayToTime(Date payToTime) {
		this.payToTime = payToTime;
	}

	public Integer getTargetPerson() {
		return targetPerson;
	}

	public void setTargetPerson(Integer targetPerson) {
		this.targetPerson = targetPerson;
	}

	public int getAmountLimit() {
		return amountLimit;
	}

	public void setAmountLimit(int amountLimit) {
		this.amountLimit = amountLimit;
	}
}
