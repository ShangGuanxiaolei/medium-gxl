package cn.hds.hvmall.entity.reserve;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author 
 */
public class PromotionReserveProduct implements Serializable {
    /**
     * 预约活动商品id
     */
    private Long id;

    /**
     * 活动预约id
     */
    private Long reserveId;

	public Long getReserveId() {
		return reserveId;
	}

	public void setReserveId(Long reserveId) {
		this.reserveId = reserveId;
	}

	/**
     * 商品id
     */
    private Long skuId;

    private Long productId;

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	/**
     * 类目名称
     */
    private String categoryName;

    /**
     * 预约价
     */
    private BigDecimal reservePrice;

    /**
     * 兑换价
     */
    private BigDecimal conversionPrice;

    /**
     * 德分
     */
    private Long point;

    /**
     * 活动库存上限
     */
    private Integer amountLimit;

    /**
     * 每人限购数
     */
    private Integer buyLimit;

	/**
     * 已预约数量
     */
    private Integer reserveLimit;

	/**
	 * code
	 * @return
	 */
	private String reserveProductCode;

	public String getReserveProductCode() {
		return reserveProductCode;
	}

	public void setReserveProductCode(String reserveProductCode) {
		this.reserveProductCode = reserveProductCode;
	}

	public Integer getPreReserveLimit() {
		return preReserveLimit;
	}

	public void setPreReserveLimit(Integer preReserveLimit) {
		this.preReserveLimit = preReserveLimit;
	}

	/**
	 * 前端已预约数量
	 * @return
	 */
	private Integer preReserveLimit;

	public Integer getReserveLimit() {
		return reserveLimit;
	}

	public void setReserveLimit(Integer reserveLimit) {
		this.reserveLimit = reserveLimit;
	}

	private Date createTime;

		private Date updateTime;

		private boolean isArchive;

	public Date getCreateTime() {
		return createTime;
	}

	public Integer getBuyLimit() {
		return buyLimit;
	}

	public void setBuyLimit(Integer buyLimit) {
		this.buyLimit = buyLimit;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public boolean isArchive() {
		return isArchive;
	}

	public void setArchive(boolean archive) {
		isArchive = archive;
	}

	private static final long serialVersionUID = 1L;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public BigDecimal getReservePrice() {
		return reservePrice;
	}

	public void setReservePrice(BigDecimal reservePrice) {
		this.reservePrice = reservePrice;
	}

	public BigDecimal getConversionPrice() {
		return conversionPrice;
	}

	public void setConversionPrice(BigDecimal conversionPrice) {
		this.conversionPrice = conversionPrice;
	}

	public Long getPoint() {
		return point;
	}

	public void setPoint(Long point) {
		this.point = point;
	}

	public Integer getAmountLimit() {
		return amountLimit;
	}

	public void setAmountLimit(Integer amountLimit) {
		this.amountLimit = amountLimit;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}
}