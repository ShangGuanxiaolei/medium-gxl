package cn.hds.hvmall.entity.reserve;

import java.io.Serializable;

public class PromotionAmount implements Serializable {

	private Long skuId;

	private int pAmount;

	public Long getSkuId() {
		return skuId;
	}

	public void setSkuId(Long skuId) {
		this.skuId = skuId;
	}

	public int getpAmount() {
		return pAmount;
	}

	public void setpAmount(int pAmount) {
		this.pAmount = pAmount;
	}
}
