package cn.hds.hvmall.entity.category;

import cn.hds.hvmall.pojo.list.CategoryStatus;
import cn.hds.hvmall.pojo.list.ProductSource;
import cn.hds.hvmall.pojo.list.Taxonomy;

import java.util.Date;

public class CategoryForPromotion {

  private String id;

  private String termId;

  private String name;

  private Taxonomy taxonomy;

  private String parentId;

  private String treePath;

  private String shopId;

  private int idx;

  private String creatorId;

  private Date createdAt;

  private Date updatedAt;

  private String img;

  private Boolean archive;

  private String sourceCategoryId;

  private CategoryStatus status;

  private ProductSource source;

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }

  public String getSourceCategoryId() {
    return sourceCategoryId;
  }

  public void setSourceCategoryId(String sourceCategoryId) {
    this.sourceCategoryId = sourceCategoryId;
  }

  public CategoryStatus getStatus() {
    return status;
  }

  public void setStatus(CategoryStatus status) {
    this.status = status;
  }

  public String getImg() {
    return img;
  }

  public void setImg(String img) {
    this.img = img;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getTermId() {
    return termId;
  }

  public void setTermId(String termId) {
    this.termId = termId;
  }

  public Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  public Date getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(Date updatedAt) {
    this.updatedAt = updatedAt;
  }

  public Taxonomy getTaxonomy() {
    return taxonomy;
  }

  public void setTaxonomy(Taxonomy taxonomy) {
    this.taxonomy = taxonomy;
  }

  public String getParentId() {
    return parentId;
  }

  public void setParentId(String parentId) {
    this.parentId = parentId;
  }

  public String getTreePath() {
    return treePath;
  }

  public void setTreePath(String treePath) {
    this.treePath = treePath;
  }

  public String getCreatorId() {
    return creatorId;
  }

  public void setCreatorId(String creatorId) {
    this.creatorId = creatorId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getShopId() {
    return shopId;
  }

  public void setShopId(String shopId) {
    this.shopId = shopId;
  }

  public int getIdx() {
    return idx;
  }

  public void setIdx(int idx) {
    this.idx = idx;
  }

  public ProductSource getSource() {
    return source;
  }

  public void setSource(ProductSource source) {
    this.source = source;
  }
}