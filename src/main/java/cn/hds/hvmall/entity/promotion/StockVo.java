package cn.hds.hvmall.entity.promotion;

public class StockVo {
    private String pSkuNum;
    private String pUsableSkuNum;
    private String skuCode;
    private String name;

    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getpSkuNum() {
        return pSkuNum;
    }

    public void setpSkuNum(String pSkuNum) {
        this.pSkuNum = pSkuNum;
    }

    public String getpUsableSkuNum() {
        return pUsableSkuNum;
    }

    public void setpUsableSkuNum(String pUsableSkuNum) {
        this.pUsableSkuNum = pUsableSkuNum;
    }
}
