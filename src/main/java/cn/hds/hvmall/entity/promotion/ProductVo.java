package cn.hds.hvmall.entity.promotion;

import cn.hds.hvmall.utils.list.qiniu.json.JsonResourceUrlSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * @author: create by daiweiwei
 * @version: v1.0
 * @description:
 * @date: 2018/10/23 12:59
 */
public class ProductVo {

    private int productId;
    private String code;
    private String type;
    private String name;
    @JsonSerialize(using = JsonResourceUrlSerializer.class)
    private String img;
    private String status;
    private Double marketPrice;
    private Double price;
    private int amount;
    private String skuCode;
    private int isShow;
    private int skuNum;
    private int usableSkuNum;
    private int restriction;
    private int gift;
    //是否生效
    private  int isDeleted;


    public int getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(int isDeleted) {
        this.isDeleted = isDeleted;
    }


    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Double getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(Double marketPrice) {
        this.marketPrice = marketPrice;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    public int getIsShow() {
        return isShow;
    }

    public void setIsShow(int isShow) {
        this.isShow = isShow;
    }

    public int getSkuNum() {
        return skuNum;
    }

    public void setSkuNum(int skuNum) {
        this.skuNum = skuNum;
    }

    public int getUsableSkuNum() {
        return usableSkuNum;
    }

    public void setUsableSkuNum(int usableSkuNum) {
        this.usableSkuNum = usableSkuNum;
    }

    public int getRestriction() {
        return restriction;
    }

    public void setRestriction(int restriction) {
        this.restriction = restriction;
    }

    public int getGift() {
        return gift;
    }

    public void setGift(int gift) {
        this.gift = gift;
    }
}
