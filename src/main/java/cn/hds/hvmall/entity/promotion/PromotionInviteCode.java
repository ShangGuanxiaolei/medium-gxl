package cn.hds.hvmall.entity.promotion;

/**
 * @author: create by daiweiwei
 * @version: v1.0
 * @description:
 * @date: 2018/10/28 10:30
 */
public class PromotionInviteCode {
    private String code;
    private int cpId;
    private String pCode;
    private String bindAt;

    public String getBindAt() {
        return bindAt;
    }

    public void setBindAt(String bindAt) {
        this.bindAt = bindAt;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getCpId() {
        return cpId;
    }

    public void setCpId(int cpId) {
        this.cpId = cpId;
    }

    public String getpCode() {
        return pCode;
    }

    public void setpCode(String pCode) {
        this.pCode = pCode;
    }
}
