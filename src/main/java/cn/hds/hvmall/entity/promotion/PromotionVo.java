package cn.hds.hvmall.entity.promotion;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author: create by daiweiwei
 * @version: v1.0
 * @description:
 * @date: 2018/10/18 16:55
 */
public class PromotionVo {
    /**
     * 活动名称
     */
    private String prmotionName;
    /**
     * 活动开始时间
     */
    private Date beginDate;
    /**
     * 活动结束时间
     */
    private Date endDate;
    /**
     * 邀请码
     */
    private List<Map<String,String>> configs;

    public List<Map<String, String>> getConfigs() {
        return configs;
    }

    public void setConfigs(List<Map<String, String>> configs) {
        this.configs = configs;
    }
    //    private String inviteCode;
    /**
     * 邀请码次数
     */
//    private String codeNum;
    /**
     * 限制金额
     */
//    private double limitAccount;
    /**
     * 产品详情
     */
    private List<Map<String,String>> products;
    /**
     * 资源位类型
     */
//    private String postType;
    /**
     * 广告型图片链接
     */
    private String broadUrl;
    /**
     * 橱窗型图片链接
     */
    private String showUrl;
    /**
     * 活动主键
     */
    private long id;
    /**
     * 活动编码
     */
    private String projectCode;
    /**
     * 活动类型
     */
    private String type;
    /**
     * 活动状态
     */
    private int status;
    /**
     * 创建者
     */
    private String creator;
    /**
     * 审核者
     */
    private String auditor;
    /**
     * 修改者
     */
    private String updator;
    /**
     * 活动创建时间
     */
    private Date createTime;
    /**
     * 活动更新时间
     */
    private Date updateTime;
    /**
     * 删除标记
     */
    private int isDeleted;

    public String getPrmotionName() {
        return prmotionName;
    }

    public void setPrmotionName(String prmotionName) {
        this.prmotionName = prmotionName;
    }

    public Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public List<Map<String, String>> getProducts() {
        return products;
    }

    public void setProducts(List<Map<String, String>> products) {
        this.products = products;
    }

    public String getBroadUrl() {
        return broadUrl;
    }

    public void setBroadUrl(String broadUrl) {
        this.broadUrl = broadUrl;
    }

    public String getShowUrl() {
        return showUrl;
    }

    public void setShowUrl(String showUrl) {
        this.showUrl = showUrl;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getProjectCode() {
        return projectCode;
    }

    public void setProjectCode(String projectCode) {
        this.projectCode = projectCode;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getAuditor() {
        return auditor;
    }

    public void setAuditor(String auditor) {
        this.auditor = auditor;
    }

    public String getUpdator() {
        return updator;
    }

    public void setUpdator(String updator) {
        this.updator = updator;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public int getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(int isDeleted) {
        this.isDeleted = isDeleted;
    }

    @Override
    public String toString() {
        return "PromotionVo{" +
                "prmotionName='" + prmotionName + '\'' +
                ", beginDate=" + beginDate +
                ", endDate=" + endDate +
                ", configs=" + configs +
                ", products=" + products +
                ", broadUrl='" + broadUrl + '\'' +
                ", showUrl='" + showUrl + '\'' +
                ", id=" + id +
                ", projectCode='" + projectCode + '\'' +
                ", type='" + type + '\'' +
                ", status=" + status +
                ", creator='" + creator + '\'' +
                ", auditor='" + auditor + '\'' +
                ", updator='" + updator + '\'' +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                ", isDeleted=" + isDeleted +
                '}';
    }
}
