package cn.hds.hvmall.entity.promotion;

import java.util.List;

/**
 * @author: create by daiweiwei
 * @version: v1.0
 * @description:
 * @date: 2018/10/20 16:43
 */
public class PromotionProduct {

    /**
     * 商品状态
     */
    private String status;

    /**
     * 商品分类
     */
    private String type;

    /**
     * 商品名称
     */
    private String name;

    /**
     * 商品编码
     */
    private String code;

    /**
     * 项目编码
     */
    private String projectCode;

    /**
     * 活动库存
     */
    private int skuNum;

    /**
     * 限购数量
     */
    private int restriction;

    /**
     * 是否橱窗展示
     */
    private int isShow;

    /**
     * 是否买一赠一
     */
    private int gift;
    /**
     * sku编码
     */
    private String skuCode;
    /**
     * 商品数组
     */
    private List<String> skuCodes;
    /**
     * 页码
     */
    private int pageNum;

    /**
     * 每页数量
     */
    private int pageSize;

    //追加库存
    private int addStock;

    public int getAddStock() {
        return addStock;
    }

    public void setAddStock(int addStock) {
        this.addStock = addStock;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getProjectCode() {
        return projectCode;
    }

    public void setProjectCode(String projectCode) {
        this.projectCode = projectCode;
    }

    public int getSkuNum() {
        return skuNum;
    }

    public void setSkuNum(int skuNum) {
        this.skuNum = skuNum;
    }

    public int getRestriction() {
        return restriction;
    }

    public void setRestriction(int restriction) {
        this.restriction = restriction;
    }

    public int getIsShow() {
        return isShow;
    }

    public void setIsShow(int isShow) {
        this.isShow = isShow;
    }

    public int getGift() {
        return gift;
    }

    public void setGift(int gift) {
        this.gift = gift;
    }

    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public List<String> getSkuCodes() {
        return skuCodes;
    }

    public void setSkuCodes(List<String> skuCodes) {
        this.skuCodes = skuCodes;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }


}
