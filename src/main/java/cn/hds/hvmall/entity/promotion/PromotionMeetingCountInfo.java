package cn.hds.hvmall.entity.promotion;

import cn.hds.hvmall.entity.piece.HourRank;
import cn.hds.hvmall.entity.piece.OrderListVO;

import java.util.Date;
import java.util.List;

/**
 * @author LiHaoYang
 * @ClassName PromotionMeetingCountInfo
 * @date 2019/1/3 0003
 */
public class PromotionMeetingCountInfo {
    private String pName;

    public String getpName() {
        return pName;
    }

    public void setpName(String pName) {
        this.pName = pName;
    }

    public Date getEffectFrom() {
        return effectFrom;
    }

    public void setEffectFrom(Date effectFrom) {
        this.effectFrom = effectFrom;
    }

    public Date getEffectTo() {
        return effectTo;
    }

    public void setEffectTo(Date effectTo) {
        this.effectTo = effectTo;
    }



    private Date effectFrom;
    private Date effectTo;



    public List<StockVo> getStock() {
        return Stock;
    }

    public void setStock(List<StockVo> stock) {
        Stock = stock;
    }

    List<StockVo> Stock;

    private int failOrderCount;
    private int paidOrderCount;

    //待付款
    private int submit;
    //已支付
    private int paid;
    //已发货
    private int shipped;
    //已取消
    private int cancelled;

    public int getSubmit() {
        return submit;
    }

    public void setSubmit(int submit) {
        this.submit = submit;
    }

    public int getPaid() {
        return paid;
    }

    public void setPaid(int paid) {
        this.paid = paid;
    }

    public int getShipped() {
        return shipped;
    }

    public void setShipped(int shipped) {
        this.shipped = shipped;
    }

    public int getCancelled() {
        return cancelled;
    }

    public void setCancelled(int cancelled) {
        this.cancelled = cancelled;
    }

    public int getFailOrderCount() {
        return failOrderCount;
    }

    public void setFailOrderCount(int failOrderCount) {
        this.failOrderCount = failOrderCount;
    }

    public int getPaidOrderCount() {
        return paidOrderCount;
    }

    public void setPaidOrderCount(int paidOrderCount) {
        this.paidOrderCount = paidOrderCount;
    }


    private List<HourRank> hourRanks;

    public List<HourRank> getHourRanks() {
        return hourRanks;
    }

    public void setHourRanks(List<HourRank> hourRanks) {
        this.hourRanks = hourRanks;
    }
}
