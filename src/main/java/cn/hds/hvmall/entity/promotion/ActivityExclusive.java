package cn.hds.hvmall.entity.promotion;

import java.util.Date;

/**
 * 过滤普通商品的活动商品实体
 */
public class ActivityExclusive {

    public ActivityExclusive() {
    }

    public ActivityExclusive(Long productId, String projectCode) {
        this(productId, projectCode, 1);
    }

    public ActivityExclusive(Long productId, String projectCode, Integer status) {
        this.productId = productId;
        this.projectCode = projectCode;
        this.status = status;
        this.createdAt = new Date();
    }

    private Long productId;
    private String projectCode;
    private Integer status;
    private Date createdAt;

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getProjectCode() {
        return projectCode;
    }

    public void setProjectCode(String projectCode) {
        this.projectCode = projectCode;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
}
