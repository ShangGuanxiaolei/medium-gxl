package cn.hds.hvmall.entity.bargain;

import java.math.BigDecimal;

public class BargainSku {

  private String id;

  /**
   *活动列表id
   */
  private String bargainId;

  /**
   *商品sku
   */
  private String skuId;

  /**
   * 商品id
   */
  private String productId;

  private BigDecimal priceStart;
  /**
   *商品底价
   */
  private BigDecimal priceEnd;
  /**
   *活动库存
   */
  private Integer stock;

  public BigDecimal getPriceStart() {
    return priceStart;
  }

  public void setPriceStart(BigDecimal priceStart) {
    this.priceStart = priceStart;
  }

  public String getBargainId() {
    return bargainId;
  }

  public void setBargainId(String bargainId) {
    this.bargainId = bargainId;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getSkuId() {
    return skuId;
  }

  public void setSkuId(String skuId) {
    this.skuId = skuId;
  }

  public String getProductId() {
    return productId;
  }

  public void setProductId(String productId) {
    this.productId = productId;
  }

  public BigDecimal getPriceEnd() {
    return priceEnd;
  }

  public void setPriceEnd(BigDecimal priceEnd) {
    this.priceEnd = priceEnd;
  }

  public Integer getStock() {
    return stock;
  }

  public void setStock(Integer stock) {
    this.stock = stock;
  }

  @Override
  public String toString() {
    return "BargainSku{" +
            "id='" + id + '\'' +
            ", bargainId='" + bargainId + '\'' +
            ", skuId='" + skuId + '\'' +
            ", productId='" + productId + '\'' +
            ", priceStart=" + priceStart +
            ", priceEnd=" + priceEnd +
            ", stock=" + stock +
            '}';
  }
}
