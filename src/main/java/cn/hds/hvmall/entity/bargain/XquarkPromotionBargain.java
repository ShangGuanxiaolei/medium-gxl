package cn.hds.hvmall.entity.bargain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author
 */
public class XquarkPromotionBargain implements Serializable {
  private String id;

  /**
   * 砍价活动id
   */
  private String promotionId;

  /**
   * 砍价活动名称
   */
  private String promotionName;

  /**
   * 活动开始时间
   */
  private String startTime;

  /**
   * 活动结束时间
   */
  private String endTime;

  /**
   * 砍价商品id
   */
  private String productId;

  /**
   * 砍价金额起始数
   */
  private BigDecimal priceStart;

  /**
   * 砍价金额结束数
   */
  private BigDecimal priceEnd;

  /**
   * 活动状态
   */
  private Integer state;

  /**
   *参与限制
   */
  private Boolean supportLimit;

  private Date createdAt;

  private Date updatedAt;

  private Boolean archive;

  private static final long serialVersionUID = 1L;



  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getPromotionId() {
    return promotionId;
  }

  public void setPromotionId(String promotionId) {
    this.promotionId = promotionId;
  }

  public String getProductId() {
    return productId;
  }

  public void setProductId(String productId) {
    this.productId = productId;
  }

  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }

  public String getPromotionName() {
    return promotionName;
  }

  public void setPromotionName(String promotionName) {
    this.promotionName = promotionName;
  }

  public String getStartTime() {
    return startTime;
  }

  public void setStartTime(String startTime) {
    this.startTime = startTime;
  }

  public String getEndTime() {
    return endTime;
  }

  public void setEndTime(String endTime) {
    this.endTime = endTime;
  }

  public BigDecimal getPriceStart() {
    return priceStart;
  }

  public void setPriceStart(BigDecimal priceStart) {
    this.priceStart = priceStart;
  }

  public static long getSerialVersionUID() {
    return serialVersionUID;
  }

  public BigDecimal getPriceEnd() {
    return priceEnd;
  }

  public void setPriceEnd(BigDecimal priceEnd) {
    this.priceEnd = priceEnd;
  }


  public Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  public Date getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(Date updatedAt) {
    this.updatedAt = updatedAt;
  }

  public Integer getState() {
    return state;
  }

  public void setState(Integer state) {
    this.state = state;
  }

  public Boolean getSupportLimit() {
    return supportLimit;
  }

  public void setSupportLimit(Boolean supportLimit) {
    this.supportLimit = supportLimit;
  }
}