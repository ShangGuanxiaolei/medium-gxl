package cn.hds.hvmall.entity.bargain;

/**
 * 砍价活动列表id
 */
public class SearchCondation {
  /**
   * 砍价活动列表id
   */
  private String bargainId;
  /**
   * 砍价商品名称
   */
  private String productName;
  /**
   *砍价活动状态
   */
  private String state;
  /**
   *分页数
   */
  private Integer pageSize;
  /**
   *分页大小
   */
  private Integer pageNum;

  public String getBargainId() {
    return bargainId;
  }

  public void setBargainId(String bargainId) {
    this.bargainId = bargainId;
  }

  public String getProductName() {
    return productName;
  }

  public void setProductName(String productName) {
    this.productName = productName;
  }

  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public Integer getPageSize() {
    return pageSize;
  }

  public void setPageSize(Integer pageSize) {
    this.pageSize = pageSize;
  }

  public Integer getPageNum() {
    return pageNum;
  }

  public void setPageNum(Integer pageNum) {
    this.pageNum = pageNum;
  }
}
