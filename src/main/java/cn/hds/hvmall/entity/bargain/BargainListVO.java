package cn.hds.hvmall.entity.bargain;

import cn.hds.hvmall.utils.list.qiniu.JsonResourceUrlSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.Date;
import java.util.List;

/**
 * 活动清单VO
 */
public class BargainListVO {
  /**
   * 砍价活动id
   */
  private String bargainId;
  private String promotionId;
  /**
   * 活动开始时间
   */
  private Date startTime;
  /**
   * 活动结束时间
   */
  private Date endTime;
  /**
   * 活动名称
   */
  private String promotionName;

  List<BargainSku> skus;
/**
 * 活动限制规则
 */
  private Integer supportLimit;
  private String productId;
  private String productName;

  @JsonSerialize(using = JsonResourceUrlSerializer.class)
  private String productImg;

  /**
   * 分类
   */
  private String category;

  /**
   * 供应商
   */
  private String supplierName;

  /**
   * 品牌
   */
  private String brandName;

  private String state;

  public String getProductImg() {
    return productImg;
  }

  public void setProductImg(String productImg) {
    this.productImg = productImg;
  }

  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  public String getSupplierName() {
    return supplierName;
  }

  public void setSupplierName(String supplierName) {
    this.supplierName = supplierName;
  }

  public String getBrandName() {
    return brandName;
  }

  public void setBrandName(String brandName) {
    this.brandName = brandName;
  }

  public String getBargainId() {
    return bargainId;
  }

  public void setBargainId(String bargainId) {
    this.bargainId = bargainId;
  }

  public String getPromotionName() {
    return promotionName;
  }

  public void setPromotionName(String promotionName) {
    this.promotionName = promotionName;
  }

  public Integer getSupportLimit() {
    return supportLimit;
  }

  public void setSupportLimit(Integer supportLimit) {
    this.supportLimit = supportLimit;
  }

  public String getPromotionId() {
    return promotionId;
  }

  public void setPromotionId(String promotionId) {
    this.promotionId = promotionId;
  }

  public Date getStartTime() {
    return startTime;
  }

  public void setStartTime(Date startTime) {
    this.startTime = startTime;
  }

  public Date getEndTime() {
    return endTime;
  }

  public void setEndTime(Date endTime) {
    this.endTime = endTime;
  }

  public String getProductId() {
    return productId;
  }

  public void setProductId(String productId) {
    this.productId = productId;
  }

  public String getProductName() {
    return productName;
  }

  public void setProductName(String productName) {
    this.productName = productName;
  }

  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public List<BargainSku> getSkus() {
    return skus;
  }

  public void setSkus(List<BargainSku> skus) {
    this.skus = skus;
  }

}
