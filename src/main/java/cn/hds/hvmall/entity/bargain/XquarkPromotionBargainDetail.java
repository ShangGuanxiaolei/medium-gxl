package cn.hds.hvmall.entity.bargain;

import java.io.Serializable;
import java.util.Date;

/**
 * @author
 */
public class XquarkPromotionBargainDetail implements Serializable {

  private String id;

  private String bargainId;

  /**
   * 砍价发起人id
   */
  private String userId;

  /**
   * 发起折扣
   */
  private Double launchDiscount;

  /**
   * 当前砍价次数
   */
  private Integer currTimes;

  /**
   *当前金额
   */
  private String currDiscount;


  private Date createdAt;

  private Date updateAt;

  private Boolean archive;

  private static final long serialVersionUID = 1L;


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }



  public Boolean getArchive() {
    return archive;
  }

  public void setArchive(Boolean archive) {
    this.archive = archive;
  }

  public String getBargainId() {
    return bargainId;
  }

  public void setBargainId(String bargainId) {
    this.bargainId = bargainId;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public Double getLaunchDiscount() {
    return launchDiscount;
  }

  public void setLaunchDiscount(Double launchDiscount) {
    this.launchDiscount = launchDiscount;
  }

  public Integer getCurrTimes() {
    return currTimes;
  }

  public void setCurrTimes(Integer currTimes) {
    this.currTimes = currTimes;
  }

  public String getCurrDiscount() {
    return currDiscount;
  }

  public void setCurrDiscount(String currDiscount) {
    this.currDiscount = currDiscount;
  }

  public Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  public Date getUpdateAt() {
    return updateAt;
  }

  public void setUpdateAt(Date updateAt) {
    this.updateAt = updateAt;
  }

}