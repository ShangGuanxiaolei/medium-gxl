package cn.hds.hvmall.entity;

public interface Archivable extends BaseEntity {

  Boolean getArchive();

  void setArchive(Boolean archive);

}
