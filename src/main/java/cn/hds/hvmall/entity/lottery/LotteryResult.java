package cn.hds.hvmall.entity.lottery;

import cn.hds.hvmall.entity.rain.PromotionPacketListVO;

import java.io.Serializable;

public class LotteryResult implements Serializable {

	private PromotionPacketListVO promotionPacketListVO;


	public PromotionPacketListVO getPromotionPacketListVO() {
		return promotionPacketListVO;
	}

	public void setPromotionPacketListVO(PromotionPacketListVO promotionPacketListVO) {
		this.promotionPacketListVO = promotionPacketListVO;
	}
}
