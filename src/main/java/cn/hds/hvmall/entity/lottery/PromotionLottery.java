package cn.hds.hvmall.entity.lottery;

import cn.hds.hvmall.entity.BaseEntityArchivable;
import org.hibernate.validator.constraints.NotBlank;

import java.util.Date;
import java.util.List;

/**
 * @author wangxinhua
 */
public class PromotionLottery extends BaseEntityArchivable {

	/**
	 * 抽奖id
	 */
	private Long lotteryId;

	/**
     * 对应的活动列表的id
     */
    private Long promotionListId;

    /**
     * 活动日期
     */
    @NotBlank
    private Date promotionDate;

    /**
     * 当天抽奖次数
     */
    @NotBlank
    private Integer lotteryTimes;

    /**
     * 展示样式
     */
    @NotBlank
    private String showStyle;

    /**
     * 抽奖消耗的德分
     */
    @NotBlank
    private Integer consumptionScore;

    private List<LotteryProbability> lotteryProbabilityList;

	public List<LotteryProbability> getLotteryProbabilityList() {
		return lotteryProbabilityList;
	}

	public void setLotteryProbabilityList(List<LotteryProbability> lotteryProbabilityList) {
		this.lotteryProbabilityList = lotteryProbabilityList;
	}

	private static final long serialVersionUID = 1L;

		public Long getLotteryId() {
			return lotteryId;
		}

		public void setLotteryId(Long lotteryId) {
			this.lotteryId = lotteryId;
		}

    public Long getPromotionListId() {
        return promotionListId;
    }

    public void setPromotionListId(Long promotionListId) {
        this.promotionListId = promotionListId;
    }

    public Date getPromotionDate() {
        return promotionDate;
    }

    public void setPromotionDate(Date promotionDate) {
        this.promotionDate = promotionDate;
    }

    public Integer getLotteryTimes() {
        return lotteryTimes;
    }

    public void setLotteryTimes(Integer lotteryTimes) {
        this.lotteryTimes = lotteryTimes;
    }

    public String getShowStyle() {
        return showStyle;
    }

    public void setShowStyle(String showStyle) {
        this.showStyle = showStyle;
    }

    public Integer getConsumptionScore() {
        return consumptionScore;
    }

    public void setConsumptionScore(Integer consumptionScore) {
        this.consumptionScore = consumptionScore;
    }
}