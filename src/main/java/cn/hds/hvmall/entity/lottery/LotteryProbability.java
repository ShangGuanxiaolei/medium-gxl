package cn.hds.hvmall.entity.lottery;

import cn.hds.hvmall.entity.BaseEntityArchivable;

import java.math.BigDecimal;

/**
 * @author wangxinhua
 */
public class LotteryProbability extends BaseEntityArchivable {

		/**
		 * 活动概率id
		 */
		private Long probabilityId;

	/**
     * 对应抽奖活动id
     */
    private Long promotionLotteryId;

    /**
     * 奖品 根据抽奖类型确定 0 - 多少德分 1 - 几等奖
     */
    private Integer prize;

    /**
     * 中奖概率
     */
    private BigDecimal probability;

    /**
     * 份数
     */
    private Integer amount;

    /**
     * 0 - 德分类型 1 - 奖品类型
     */
    private Integer lotteryType;

    /**
     * 关联sku id
     */
    private String skuId;

    /**
     * 前端展示顺序
     */
    private Integer sortOrder;

    /**
     * 商品或者红包图片
     */
    private String image;

    private static final long serialVersionUID = 1L;

		public Long getProbabilityId() {
			return probabilityId;
		}

		public void setProbabilityId(Long probabilityId) {
			this.probabilityId = probabilityId;
		}

    public Long getPromotionLotteryId() {
        return promotionLotteryId;
    }

    public void setPromotionLotteryId(Long promotionLotteryId) {
        this.promotionLotteryId = promotionLotteryId;
    }

    public Integer getPrize() {
        return prize;
    }

    public void setPrize(Integer prize) {
        this.prize = prize;
    }

    public BigDecimal getProbability() {
        return probability;
    }

    public void setProbability(BigDecimal probability) {
        this.probability = probability;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getLotteryType() {
        return lotteryType;
    }

    public void setLotteryType(Integer lotteryType) {
        this.lotteryType = lotteryType;
    }

    public String getSkuId() {
        return skuId;
    }

    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Boolean getArchive() {
        return archive;
    }

    public void setArchive(Boolean archive) {
        this.archive = archive;
    }
}