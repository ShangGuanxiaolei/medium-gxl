package cn.hds.hvmall.entity.lottery;

import lombok.val;

import java.io.Serializable;
import java.util.Date;

/**
 * 用于页面展示的活动清单
 */
public class ActivityList implements Serializable {

	/**
	 *  活动id
	 */
	private Long promotionId;

	/**
	 * 活动类型
	 */
	private Integer promotionType;

	/**
	 * 活动状态
	 */
	private Integer state;

	/**
	 * 活动开始日期
	 */
	private Date triggerStartTime;

	/**
	 * 活动结束日期
	 */
	private Date triggerEndTime;

	/**
	 * 创建账号
	 */
	private String createUser;

	/**
	 * 创建时间
	 */
	private Date createdAt;

	public Long getPromotionId() {
		return promotionId;
	}

	public void setPromotionId(Long promotionId) {
		this.promotionId = promotionId;
	}

	public Integer getPromotionType() {
		return promotionType;
	}

	public void setPromotionType(Integer promotionType) {
		this.promotionType = promotionType;
	}

	public Integer getState() {
		if (state == null) {
			return null;
		}
		val start = getTriggerStartTime();
		val end = getTriggerEndTime();
		if (start == null || end == null) {
			return state;
		}
		val now = new Date();
		if (now.before(start) && state == 1) {
		    return 0;
		}
		if (now.after(end) && state != 2) {
			return 2;
		}
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Date getTriggerStartTime() {
		return triggerStartTime;
	}

	public void setTriggerStartTime(Date triggerStartTime) {
		this.triggerStartTime = triggerStartTime;
	}

	public Date getTriggerEndTime() {
		return triggerEndTime;
	}

	public void setTriggerEndTime(Date triggerEndTime) {
		this.triggerEndTime = triggerEndTime;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
}
