package cn.hds.hvmall.entity.lottery;

import java.io.Serializable;
import java.util.Date;

public class ActivityForm implements Serializable {

	/**
	 * 活动id
	 */
	private Long promotionId;

	/**
	 * 活动状态
	 */
	private Integer state;

	/**
	 * 活动类型
	 */
	private Integer promotionType;

	/**
	 * 活动开始时间
	 */
	private Date triggerStartTime;

	/**
	 * 活动结束时间
	 */
	private Date triggerEndTime;

	/**
	 * 分页参数
	 */
	private Integer pageIndex;

	private Integer pageSize;

	public Long getPromotionId() {
		return promotionId;
	}

	public void setPromotionId(Long promotionId) {
		this.promotionId = promotionId;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Integer getPromotionType() {
		return promotionType;
	}

	public void setPromotionType(Integer promotionType) {
		this.promotionType = promotionType;
	}

	public Date getTriggerStartTime() {
		return triggerStartTime;
	}

	public void setTriggerStartTime(Date triggerStartTime) {
		this.triggerStartTime = triggerStartTime;
	}

	public Date getTriggerEndTime() {
		return triggerEndTime;
	}

	public void setTriggerEndTime(Date triggerEndTime) {
		this.triggerEndTime = triggerEndTime;
	}

	public Integer getPageIndex() {
		return pageIndex;
	}

	public void setPageIndex(Integer pageIndex) {
		this.pageIndex = pageIndex;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
}
