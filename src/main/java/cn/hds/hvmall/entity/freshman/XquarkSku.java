package cn.hds.hvmall.entity.freshman;

import java.io.Serializable;
import java.math.BigDecimal;

public class XquarkSku implements Serializable {
    private  Integer sku;
    private String skucode;

    private String spec;

    private String sourceSkuCode;


    private BigDecimal originalprice;

    private BigDecimal activityprice;//活动价

    private BigDecimal conversionprice;//兑换价

    private BigDecimal point;//德分

    private BigDecimal reduction;//立减

    private BigDecimal serverAmt;//服务费

    private BigDecimal promoAmt;//推广费

    private BigDecimal netWorth;//净值

    private Integer activitystock;//活动库存

    private Integer buylimit;//限购数量

    private Boolean show;//是否在首页展示

    private Boolean isDeleted;//删除标记

    private Integer stock;//原有库存

    private String fzSkuCode;//复制的skuCode

    private Integer productId;//sku表productId



    public String getFzSkuCode() {
        return fzSkuCode;
    }

    public void setFzSkuCode(String fzSkuCode) {
        this.fzSkuCode = fzSkuCode;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public BigDecimal getOriginalprice() {
        return originalprice;
    }

    public void setOriginalprice(BigDecimal originalprice) {
        this.originalprice = originalprice;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }



    public String getSourceSkuCode() {
        return sourceSkuCode;
    }

    public void setSourceSkuCode(String sourceSkuCode) {
        this.sourceSkuCode = sourceSkuCode;
    }

    public Boolean getShow() {
        return show;
    }

    public void setShow(Boolean show) {
        this.show = show;
    }

    public Integer getSku() {
        return sku;
    }

    public void setSku(Integer sku) {
        this.sku = sku;
    }



    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean deleted) {
        isDeleted = deleted;
    }

    public String getSkucode() {
        return skucode;
    }

    public void setSkucode(String skucode) {
        this.skucode = skucode;
    }

    public String getSpec() {
        return spec;
    }

    public void setSpec(String spec) {
        this.spec = spec;
    }

    public BigDecimal getActivityprice() {
        return activityprice;
    }

    public void setActivityprice(BigDecimal activityprice) {
        this.activityprice = activityprice;
    }

    public BigDecimal getConversionprice() {
        return conversionprice;
    }

    public void setConversionprice(BigDecimal conversionprice) {
        this.conversionprice = conversionprice;
    }

    public BigDecimal getPoint() {
        return point;
    }

    public void setPoint(BigDecimal point) {
        this.point = point;
    }

    public BigDecimal getReduction() {
        return reduction;
    }

    public void setReduction(BigDecimal reduction) {
        this.reduction = reduction;
    }

    public BigDecimal getServerAmt() {
        return serverAmt;
    }

    public void setServerAmt(BigDecimal serverAmt) {
        this.serverAmt = serverAmt;
    }

    public BigDecimal getPromoAmt() {
        return promoAmt;
    }

    public void setPromoAmt(BigDecimal promoAmt) {
        this.promoAmt = promoAmt;
    }

    public BigDecimal getNetWorth() {
        return netWorth;
    }

    public void setNetWorth(BigDecimal netWorth) {
        this.netWorth = netWorth;
    }

    public Integer getActivitystock() {
        return activitystock;
    }

    public void setActivitystock(Integer activitystock) {
        this.activitystock = activitystock;
    }

    public Integer getBuylimit() {
        return buylimit;
    }

    public void setBuylimit(Integer buylimit) {
        this.buylimit = buylimit;
    }
}