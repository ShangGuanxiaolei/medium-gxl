package cn.hds.hvmall.entity.freshman;

/**
 * Created by wangxinhua on 2018/4/12. DESC: 活动针对人群
 */
public enum PromotionUserScope {

  ALL, EMPLOYEE

}
