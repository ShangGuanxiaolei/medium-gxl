package cn.hds.hvmall.entity.freshman;

import cn.hds.hvmall.pojo.freshmantab.FreshmanProductSku;

import java.math.BigDecimal;
import java.util.List;

public class Banner {
    private String  brandName;
    private int productId;
    private String productName;
    private String typeName;
    private int status;
    private BigDecimal price;
    private BigDecimal conversionPrice;
    private int stock;
    private BigDecimal freshprice;//划线价格
    private String productStatus;
    private Long sourceId;
    private List<FreshmanProductSku> freshmanProductSkus;

    public Long getSourceId() {
        return sourceId;
    }

    public void setSourceId(Long sourceId) {
        this.sourceId = sourceId;
    }

    public List<FreshmanProductSku> getFreshmanProductSkus() {
        return freshmanProductSkus;
    }

    public void setFreshmanProductSkus(List<FreshmanProductSku> freshmanProductSkus) {
        this.freshmanProductSkus = freshmanProductSkus;
    }

    public String getProductStatus() {
        return productStatus;
    }

    public void setProductStatus(String productStatus) {
        this.productStatus = productStatus;
    }

    public BigDecimal getFreshprice() {
        return freshprice;
    }

    public void setFreshprice(BigDecimal freshprice) {
        this.freshprice = freshprice;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public BigDecimal getConversionPrice() {
        return conversionPrice;
    }

    public void setConversionPrice(BigDecimal conversionPrice) {
        this.conversionPrice = conversionPrice;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }



}
