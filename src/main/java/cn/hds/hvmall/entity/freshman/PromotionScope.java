package cn.hds.hvmall.entity.freshman;

public enum PromotionScope {
  SHOP, ALL, PRODUCT
}
