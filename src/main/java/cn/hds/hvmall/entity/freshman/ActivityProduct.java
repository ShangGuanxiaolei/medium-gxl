package cn.hds.hvmall.entity.freshman;

import cn.hds.hvmall.utils.list.qiniu.JsonResourceUrlSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: WangXiao
 * Date: 2019/3/25
 * Time: 13:55
 * Description: 商品促销查询实体类
 */
public class ActivityProduct implements Serializable {
    @JsonSerialize(using = JsonResourceUrlSerializer.class )
    private String img;
    private Long fzId;//促销复制的ID
//    private  Integer proId;
//    private String  pCode;

        private Integer productamount;
    private BigDecimal productprice;
//    private String code;
    private String name;
  private Integer pId;
    private String productStatus;
    private Integer sourceId;
    private List<XquarkSku> xquarkSkulist;

    public Long getFzId() {
        return fzId;
    }

    public void setFzId(Long fzId) {
        this.fzId = fzId;
    }

    public Integer getProductamount() {
        return productamount;
    }

    public void setProductamount(Integer productamount) {
        this.productamount = productamount;
    }

    public BigDecimal getproductprice() {
        return productprice;
    }

    public void setproductprice(BigDecimal productprice) {
        this.productprice = productprice;
    }

    public Integer getSourceId() {
        return sourceId;
    }

    public void setSourceId(Integer sourceId) {
        this.sourceId = sourceId;
    }
//    public Integer getProId() {
//        return proId;
//    }
//
//    public void setProId(Integer proId) {
//        this.proId = proId;
//    }

//    public String getpCode() {
//        return pCode;
//    }
//
//    public void setpCode(String pCode) {
//        this.pCode = pCode;
//    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getpId() {
        return pId;
    }

    public void setpId(Integer pId) {
        this.pId = pId;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

//    public String getCode() {
//        return code;
//    }
//
//    public void setCode(String code) {
//        this.code = code;
//    }



    public String getProductStatus() {
        return productStatus;
    }

    public void setProductStatus(String productStatus) {
        this.productStatus = productStatus;
    }

    public List<XquarkSku> getXquarkSkulist() {
        return xquarkSkulist;
    }

    public void setXquarkSkulist(List<XquarkSku> xquarkSkulist) {
        this.xquarkSkulist = xquarkSkulist;
    }
}