package cn.hds.hvmall.entity.freshman;

public class FreshmanSaleCount {

    private String productId;
    private String productName;
    private int amount;
    private int skuId;
    private String skuCode;
    private int monthSales;
    private int yesterdaySales;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getSkuId() {
        return skuId;
    }

    public void setSkuId(int skuId) {
        this.skuId = skuId;
    }

    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    public int getMonthSales() {
        return monthSales;
    }

    public void setMonthSales(int monthSales) {
        this.monthSales = monthSales;
    }

    public int getYesterdaySales() {
        return yesterdaySales;
    }

    public void setYesterdaySales(int yesterdaySales) {
        this.yesterdaySales = yesterdaySales;
    }
}
