package cn.hds.hvmall.entity.stockcheck;

/**
 * @author LiHaoYang
 * @ClassName FlashSale
 * @date 2019/6/27 0027
 */
public class FlashSale {

    private String skuCode;

    private int skuId;

    private int promotionId;

    public int getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(int promotionId) {
        this.promotionId = promotionId;
    }

    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    public int getSkuId() {
        return skuId;
    }

    public void setSkuId(int skuId) {
        this.skuId = skuId;
    }

}
