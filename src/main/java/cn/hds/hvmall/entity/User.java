package cn.hds.hvmall.entity;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonFormat;


/**
 * entity类
 * 
 * @类名: User .
 * @描述: TODO .
 * @程序猿: chenjingwu .
 * @日期: 2017年3月3日 下午2:29:46 .
 * @版本号: V1.0 .
 */

public class User implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8510243381921221315L;

	@NotNull(message = "用户ID不能为空！")
	private Long id;

	//是否离职
	private String is_job;

	//mcode
	private String mcode;
	// 用户昵称
	@NotEmpty(message = "用户昵称不能为空！")
	@NotNull(message = "用户昵称不能为空！")
	private String username;

	

	//生成Uid
//	@NotNull(message = "头像不能为空！")
	private Long insert_uid;

//	// 真实姓名
//	@NotEmpty(message = "真实姓名不能为空！")
//	@NotNull(message = "真实姓名不能为空！")
//	private String insert_time;

	// 登陆密码
	@NotNull(message = "登陆密码不能为空！")
	private String password;

	// 是否删除
	private Date is_del;

	// 邮箱
	@Pattern(regexp = "^\\w+((-\\w+)|(\\.\\w+))*\\@[A-Za-z0-9]+((\\.|-)[A-Za-z0-9]+)*\\.[A-Za-z0-9]+$", message = "email格式不正确")
	private String email;

	// 手机号码
	@NotNull(message = "手机号码不能为空！")
	@Pattern(regexp = "^((13[0-9])|(15[^4,\\D])|(17[0-9])|(18[0-9]))\\d{8}$", message = "手机号格式不正确")
	private String mobile;

//	// 性别 1 男 2 女 3 未知
//	@NotNull(message = "性别不能为空！")
//	private Integer sex;
//
//	private Integer age; // 通过身份证信息计算出年龄
//
//	// 身高
//	private String height;
//
//	// 插入时间
//	private String weight;
	@NotNull(message = "插入时间不能为空！")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date insert_time;
	// 发送时间
	@NotNull(message = "发送时间不能为空！")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date send_time;

	// 更新时间
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date update_time;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIs_job() {
		return is_job;
	}

	public void setIs_job(String is_job) {
		this.is_job = is_job;
	}

	public String getMcode() {
		return mcode;
	}

	public void setMcode(String mcode) {
		this.mcode = mcode;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Long getInsert_uid() {
		return insert_uid;
	}

	public void setInsert_uid(Long insert_uid) {
		this.insert_uid = insert_uid;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getIs_del() {
		return is_del;
	}

	public void setIs_del(Date is_del) {
		this.is_del = is_del;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public Date getInsert_time() {
		return insert_time;
	}

	public void setInsert_time(Date insert_time) {
		this.insert_time = insert_time;
	}

	public Date getSend_time() {
		return send_time;
	}

	public void setSend_time(Date send_time) {
		this.send_time = send_time;
	}

	public Date getUpdate_time() {
		return update_time;
	}

	public void setUpdate_time(Date update_time) {
		this.update_time = update_time;
	}

//	// 推荐者编号
//	private Integer recommendUserId;
//
//	// 职业
//	private String profession;
//
//	// 兴趣爱好
//	private String tastesDiffer;
//
//	// 手机验证码
//	@NotEmpty(message = "验证码不能为空！")
//	@NotNull(message = "验证码不能为空！")
//	private String verifCode;
//
//	// 身份证
//	@NotEmpty(message = "身份证号码不能为空")
//	@NotNull(message = "身份证号码不能为空")
//	@Pattern(regexp = "([1-9]\\d{13,16}[a-zA-Z0-9]{1})", message = "身份证号码格式不正确")
//	private String identityCard;
//
//	@NotEmpty(message = "openID不能为空")
//	@NotNull(message = "openID不能为空")
//	private String activityOpenId;
//
//	// 是否认证
//	private Integer identityCardVerified;
//	// 用户名
//	@NotNull(message = "用户编号不能为空！")
//	@NotEmpty(message = "用户编号不能为空！")
//	private String userId;
//	// code键
//	private String captchaCode;
//	// 值队
//	private String captchaValue;
//
//	@NotNull(message = "地址不能为空")
//	private String address;
//
//	// 手机绑定唯一码
//	@NotNull(message = "手机绑定唯一码不能为空！")
//	@NotEmpty(message = "手机绑定唯一码不能为空！")
//	private String mobileCode;
//
//	//管理员用户名
//	private String userName;
//	
//	//支付密码是否为空 0 空  1 已设置
//	private String isPayPwd;
//	
//	@NotNull(message = "登陆入口标记不能为空！")
//	@NotEmpty(message = "登陆入口标记不能为空！")
//	private String pageTitle;

	

}