package cn.hds.hvmall.entity.customer;

import cn.hds.hvmall.entity.customer.QuestionTypeVo;

import java.util.Date;
import java.util.List;

public class CustomerQuestion {
    public int getHelpful() {
        return helpful;
    }

    public void setHelpful(int helpful) {
        this.helpful = helpful;
    }

    public int getUnhelpful() {
        return unhelpful;
    }

    public void setUnhelpful(int unhelpful) {
        this.unhelpful = unhelpful;
    }

    private String title;
    private int helpful;
    private int unhelpful;
    private int sortNum;
    private Date createdAt;
    private String content;
    private String type;
    private int typeId;

    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getSortNum() {
        return sortNum;
    }

    public void setSortNum(int sortNum) {
        this.sortNum = sortNum;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }


}



