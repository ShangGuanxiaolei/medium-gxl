package cn.hds.hvmall.entity.piece;

import java.util.Date;

public class GroupSuccessSort {
    private String nickname;
    private String cpid;
    private int pgCount;
    private int orderCount;
    private Date groupedAt;
    private int newCount;

    public int getNewCount() {
        return newCount;
    }

    public void setNewCount(int newCount) {
        this.newCount = newCount;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getCpid() {
        return cpid;
    }

    public void setCpid(String cpid) {
        this.cpid = cpid;
    }

    public int getPgCount() {
        return pgCount;
    }

    public void setPgCount(int pgCount) {
        this.pgCount = pgCount;
    }

    public int getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(int orderCount) {
        this.orderCount = orderCount;
    }

    public Date getGroupedAt() {
        return groupedAt;
    }

    public void setGroupedAt(Date groupedAt) {
        this.groupedAt = groupedAt;
    }
}
