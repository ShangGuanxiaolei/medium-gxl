package cn.hds.hvmall.entity.piece;

public class PrefixLockerBean {
  private String p_code;//活动编码
  private int id;//汉薇商城商品库存主键,skuid
  private int p_sku_num;//活动库存
  private String skuCode;

  public String getP_code() {
    return p_code;
  }

  public void setP_code(String p_code) {
    this.p_code = p_code;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public int getP_sku_num() {
    return p_sku_num;
  }

  public void setP_sku_num(int p_sku_num) {
    this.p_sku_num = p_sku_num;
  }

  public String getSkuCode() {
    return skuCode;
  }

  public void setSkuCode(String skuCode) {
    this.skuCode = skuCode;
  }
}
