package cn.hds.hvmall.entity.piece;

import org.hibernate.validator.constraints.Length;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author qiuchuyi
 * @date 2018/9/4 10:43
 */
@Table(name = "promotion_pg_detail")
public class PromotionPgDetail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    //拼团详情编码
    private String pDetailCode;
    //活动编号
    private String pCode;
    //开团人数
    private Integer pieceGroupNum;
    //sku折扣
    private Double skuDiscount;
    //删除标记位
    @Length(max = 1, message = "删除位必须在0-1位")
    private Integer isDeleted;
    //创建人
    private String creator;
    //更新人
    private String updator;
    //拼主价
    private BigDecimal openMemberPrice;
    //是否免单('true'免单,""不免单)
    private Boolean freshPromotion;
    //创建时间
    private Date createdAt;
    //更新时间
    private Date updatedAt;

    private Integer promotionPriceLimit;

    public Integer getPromotionPriceLimit() {
        return promotionPriceLimit;
    }

    public void setPromotionPriceLimit(Integer promotionPriceLimit) {
        this.promotionPriceLimit = promotionPriceLimit;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPDetailCode() {
        return pDetailCode;
    }

    public void setPDetailCode(String pDetailCode) {
        this.pDetailCode = pDetailCode;
    }
    
    public String getpCode() {
        return pCode;
    }

    public void setpCode(String pCode) {
        this.pCode = pCode;
    }

    public BigDecimal getOpenMemberPrice() {
        return openMemberPrice;
    }

    public void setOpenMemberPrice(BigDecimal openMemberPrice) {
        this.openMemberPrice = openMemberPrice;
    }

    public Boolean getFreshPromotion() {
        return freshPromotion;
    }

    public void setFreshPromotion(Boolean freshPromotion) {
        this.freshPromotion = freshPromotion;
    }

    public Integer getPieceGroupNum() {
        return pieceGroupNum;
    }

    public void setPieceGroupNum(Integer pieceGroupNum) {
        this.pieceGroupNum = pieceGroupNum;
    }

    public Double getSkuDiscount() {
        return skuDiscount;
    }

    public void setSkuDiscount(Double skuDiscount) {
        this.skuDiscount = skuDiscount;
    }

    public Integer getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getUpdator() {
        return updator;
    }

    public void setUpdator(String updator) {
        this.updator = updator;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}
