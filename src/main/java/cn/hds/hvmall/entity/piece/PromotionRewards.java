package cn.hds.hvmall.entity.piece;

import org.hibernate.validator.constraints.Length;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.DatabaseMetaData;
import java.util.Date;

/**
 * @author qiuchuyi
 * @date 2018/9/3 17:14
 */
@Table(name = "promotion_rewards")
public class PromotionRewards {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    //活动编码
    private String pCode;
    //活动类型
    private String pType;
    //创建人
    private String creator;
    //审核人
    private String auditor;
    //更新人
    private String updator;
    //创建时间
    private Date createdAt;
    //更新时间
    private Date updatedAt;
    //删除标记位
    @Length(min = 0, max = 1, message = "删除位必须在0-1位")
    private Integer isDeleted;
    //活动详情编码
    private String pDetailCode;
    //奖励编码
    private String rewardCode;
    //奖励条件
    private String rewardCondition;
    //奖励规则
    private String rewardRule;
    //奖励类型
    private String rewardType;
    //奖励子奖励
    private String rewardSubType;

    @Override
    public String toString() {
        return "PromotionRewards{" +
                "id=" + id +
                ", pCode='" + pCode + '\'' +
                ", pType='" + pType + '\'' +
                ", creator='" + creator + '\'' +
                ", auditor='" + auditor + '\'' +
                ", updator='" + updator + '\'' +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                ", isDeleted=" + isDeleted +
                ", pDetailCode='" + pDetailCode + '\'' +
                ", rewardCode='" + rewardCode + '\'' +
                ", rewardCondition='" + rewardCondition + '\'' +
                ", rewardRule='" + rewardRule + '\'' +
                ", rewardType='" + rewardType + '\'' +
                ", rewardSubType='" + rewardSubType + '\'' +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getpCode() {
        return pCode;
    }

    public void setpCode(String pCode) {
        this.pCode = pCode;
    }

    public String getpType() {
        return pType;
    }

    public void setpType(String pType) {
        this.pType = pType;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getAuditor() {
        return auditor;
    }

    public void setAuditor(String auditor) {
        this.auditor = auditor;
    }

    public String getUpdator() {
        return updator;
    }

    public void setUpdator(String updator) {
        this.updator = updator;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getpDetailCode() {
        return pDetailCode;
    }

    public void setpDetailCode(String pDetailCode) {
        this.pDetailCode = pDetailCode;
    }

    public String getRewardCode() {
        return rewardCode;
    }

    public void setRewardCode(String rewardCode) {
        this.rewardCode = rewardCode;
    }

    public String getRewardCondition() {
        return rewardCondition;
    }

    public void setRewardCondition(String rewardCondition) {
        this.rewardCondition = rewardCondition;
    }

    public String getRewardRule() {
        return rewardRule;
    }

    public void setRewardRule(String rewardRule) {
        this.rewardRule = rewardRule;
    }

    public String getRewardType() {
        return rewardType;
    }

    public void setRewardType(String rewardType) {
        this.rewardType = rewardType;
    }

    public String getRewardSubType() {
        return rewardSubType;
    }

    public void setRewardSubType(String rewardSubType) {
        this.rewardSubType = rewardSubType;
    }
}
