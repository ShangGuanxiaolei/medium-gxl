package cn.hds.hvmall.entity.piece;

/**
 * @Author: zzl
 * @Date: 2018/9/20 20:37
 * @Version
 */
public class PromotionPgDetailSimple {

  private String pDetailCode;

  private String pCode;

  private int pieceGroupNum;

  private int skuDiscount;

  private String creator;

  private String updator;

  public PromotionPgDetailSimple() {
  }

  public PromotionPgDetailSimple(String pDetailCode, String pCode, int pieceGroupNum,
      int skuDiscount,
      String creator, String updator) {
    this.pDetailCode = pDetailCode;
    this.pCode = pCode;
    this.pieceGroupNum = pieceGroupNum;
    this.skuDiscount = skuDiscount;
    this.creator = creator;
    this.updator = updator;
  }

  public String getpDetailCode() {
    return pDetailCode;
  }

  public void setpDetailCode(String pDetailCode) {
    this.pDetailCode = pDetailCode;
  }

  public String getpCode() {
    return pCode;
  }

  public void setpCode(String pCode) {
    this.pCode = pCode;
  }

  public int getPieceGroupNum() {
    return pieceGroupNum;
  }

  public void setPieceGroupNum(int pieceGroupNum) {
    this.pieceGroupNum = pieceGroupNum;
  }

  public int getSkuDiscount() {
    return skuDiscount;
  }

  public void setSkuDiscount(int skuDiscount) {
    this.skuDiscount = skuDiscount;
  }

  public String getCreator() {
    return creator;
  }

  public void setCreator(String creator) {
    this.creator = creator;
  }

  public String getUpdator() {
    return updator;
  }

  public void setUpdator(String updator) {
    this.updator = updator;
  }
}
