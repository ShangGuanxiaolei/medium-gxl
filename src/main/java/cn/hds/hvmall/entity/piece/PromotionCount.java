package cn.hds.hvmall.entity.piece;

import java.util.List;

/**
 * @author LiHaoYang
 * @ClassName PromotionCount
 * @date 2018/12/10 0010
 */
public class PromotionCount {

    private int pieceSuccessCount;
    private int pieceSuccessOrderCount;

    public int getPieceSuccessCount() {
        return pieceSuccessCount;
    }

    public void setPieceSuccessCount(int pieceSuccessCount) {
        this.pieceSuccessCount = pieceSuccessCount;
    }

    public int getPieceSuccessOrderCount() {
        return pieceSuccessOrderCount;
    }

    public void setPieceSuccessOrderCount(int pieceSuccessOrderCount) {
        this.pieceSuccessOrderCount = pieceSuccessOrderCount;
    }

    public List<PieceSuccess> getPieceSuccessList() {
        return pieceSuccessList;
    }

    public void setPieceSuccessList(List<PieceSuccess> pieceSuccessList) {
        this.pieceSuccessList = pieceSuccessList;
    }

    public int getPieceFailCount() {
        return pieceFailCount;
    }

    public void setPieceFailCount(int pieceFailCount) {
        this.pieceFailCount = pieceFailCount;
    }

    public int getPieceFailOrderCount() {
        return pieceFailOrderCount;
    }

    public void setPieceFailOrderCount(int pieceFailOrderCount) {
        this.pieceFailOrderCount = pieceFailOrderCount;
    }

    private List<PieceSuccess> pieceSuccessList;

    public List<PieceSuccessOrderCount> getPieceSuccessOrderCountList() {
        return pieceSuccessOrderCountList;
    }

    public void setPieceSuccessOrderCountList(List<PieceSuccessOrderCount> pieceSuccessOrderCountList) {
        this.pieceSuccessOrderCountList = pieceSuccessOrderCountList;
    }

    private List<PieceSuccessOrderCount> pieceSuccessOrderCountList;
    private int pieceFailCount;
    private int pieceFailOrderCount;

}
