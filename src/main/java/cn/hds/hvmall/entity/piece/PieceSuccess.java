package cn.hds.hvmall.entity.piece;

/**
 * @author LiHaoYang
 * @ClassName PieceSuccess
 * @date 2018/12/10 0010
 */
public class PieceSuccess {
    private int pieceGroupNum;

    public int getPieceGroupNum() {
        return pieceGroupNum;
    }

    public void setPieceGroupNum(int pieceGroupNum) {
        this.pieceGroupNum = pieceGroupNum;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    private int count;


}
