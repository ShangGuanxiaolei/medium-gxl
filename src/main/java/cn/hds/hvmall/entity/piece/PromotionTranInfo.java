package cn.hds.hvmall.entity.piece;

import java.util.Date;
import java.util.List;

public class PromotionTranInfo {

    public String getpName() {
        return pName;
    }

    public void setpName(String pName) {
        this.pName = pName;
    }

    private String pName;


    private Date effectFrom;
    private Date effectTo;
    private String pSkuNum;
    private String pUsableSkuNum;
    private int successCount;
    private int successPieceCount;

    public int getSuccessPieceCount() {
        return successPieceCount;
    }

    public void setSuccessPieceCount(int successPieceCount) {
        this.successPieceCount = successPieceCount;
    }

    private int pieceSuccessOrderCount;
    private int failOrderCount;
    private int paidOrderCount;


    private int submit=0;
    private int paidNoStock=0;
    private int successGroup=0;
    private int shipped=0;
    private int cancelled=0;
    private int postSale=0;

    public int getPostSale() {
        return postSale;
    }

    public void setPostSale(int postSale) {
        this.postSale = postSale;
    }

    public int getSubmit() {
        return submit;
    }

    public void setSubmit(int submit) {
        this.submit = submit;
    }

    public int getPaidNoStock() {
        return paidNoStock;
    }

    public void setPaidNoStock(int paidNoStock) {
        this.paidNoStock = paidNoStock;
    }

    public int getSuccessGroup() {
        return successGroup;
    }

    public void setSuccessGroup(int successGroup) {
        this.successGroup = successGroup;
    }

    public int getShipped() {
        return shipped;
    }

    public void setShipped(int shipped) {
        this.shipped = shipped;
    }

    public int getCancelled() {
        return cancelled;
    }

    public void setCancelled(int cancelled) {
        this.cancelled = cancelled;
    }

    public int getPieceSuccessOrderCount() {
        return pieceSuccessOrderCount;
    }

    public void setPieceSuccessOrderCount(int pieceSuccessOrderCount) {
        this.pieceSuccessOrderCount = pieceSuccessOrderCount;
    }

    public int getFailOrderCount() {
        return failOrderCount;
    }

    public void setFailOrderCount(int failOrderCount) {
        this.failOrderCount = failOrderCount;
    }

    public int getPaidOrderCount() {
        return paidOrderCount;
    }

    public void setPaidOrderCount(int paidOrderCount) {
        this.paidOrderCount = paidOrderCount;
    }

    public int getSuccessCount() {
        return successCount;
    }

    public void setSuccessCount(int successCount) {
        this.successCount = successCount;
    }

    private int pieceFailCount;

    private int count;

    private List<OrderListVO> orderList;

    private List<GroupSuccessSort> pieceSuccess;

    private List<PieceFast> Fast;

    private List<HourRank> hourRanks;

    public List<HourRank> getHourRanks() {
        return hourRanks;
    }

    public void setHourRanks(List<HourRank> hourRanks) {
        this.hourRanks = hourRanks;
    }

    public List<PieceFast> getFast() {
        return Fast;
    }

    public void setFast(List<PieceFast> fast) {
        Fast = fast;
    }

    public Date getEffectFrom() {
        return effectFrom;
    }

    public void setEffectFrom(Date effectFrom) {
        this.effectFrom = effectFrom;
    }

    public Date getEffectTo() {
        return effectTo;
    }

    public void setEffectTo(Date effectTo) {
        this.effectTo = effectTo;
    }

    public String getpSkuNum() {
        return pSkuNum;
    }

    public List<OrderListVO> getOrderList() {
        return orderList;
    }

    public void setOrderList(List<OrderListVO> orderList) {
        this.orderList = orderList;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public void setpSkuNum(String pSkuNum) {
        this.pSkuNum = pSkuNum;
    }

    public String getpUsableSkuNum() {
        return pUsableSkuNum;
    }
    public List<GroupSuccessSort> getPieceSuccess() {
        return pieceSuccess;
    }

    public void setPieceSuccess(List<GroupSuccessSort> pieceSuccess) {
        this.pieceSuccess = pieceSuccess;
    }
    public void setpUsableSkuNum(String pUsableSkuNum) {
        this.pUsableSkuNum = pUsableSkuNum;
    }

    public int getPieceFailCount() {
        return pieceFailCount;
    }

    public void setPieceFailCount(int pieceFailCount) {
        this.pieceFailCount = pieceFailCount;
    }
}
