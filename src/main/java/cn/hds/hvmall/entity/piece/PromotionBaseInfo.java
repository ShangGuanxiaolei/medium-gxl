package cn.hds.hvmall.entity.piece;

import org.hibernate.validator.constraints.Length;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * @author qiuchuyi
 * @date 2018/9/3 17:15
 */
@Table(name = "promotion_base_info")
public class PromotionBaseInfo {

    private String id;
    //UUID 活动编码
    private String pCode;
    //活动名称
    private String pName;
    //默认：piece，活动类型
    private String pType;
    //活动开始时间
    private Date effectFrom;
    //活动结束时间
    private Date effectTo;
    //活动状态1：草稿2：待锁库存3：待发布4：已发布待生效5：已生效6：人工下线7：已失效
    private Integer pStatus;
    //创建人
    private String creator;
    //审核人
    private String auditor;
    //更新人
    private String updator;
    //创建时间
    private Date createdAt;
    //修改时间
    private Date updatedAt;
    //删除标记
    @Length(max = 1, message = "删除位必须在0-1位")
    private Integer isDeleted;
    //是否启动邀请码
    private String isCode;
    //活动限额
    private String top_value;
    //展示类型
    private String showType;
    //是否推送WMS
    private String isWMS;
    //是否已生成邀请码
    private String initCode;
    //活动购买限制数量
    private Integer buyLimit;
    //大会的状态 0：失效，1：生效
    private Integer pCodeStatus;

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public Integer getpCodeStatus() {
        return pCodeStatus;
    }
    public void setpCodeStatus(Integer pCodeStatus) {
        this.pCodeStatus = pCodeStatus;
    }

    public Integer getBuyLimit() {
        return buyLimit;
    }

    public void setBuyLimit(Integer buyLimit) {
        this.buyLimit = buyLimit;
    }

    public String getpCode() {
        return pCode;
    }

    public void setpCode(String pCode) {
        this.pCode = pCode;
    }

    public String getpName() {
        return pName;
    }

    public void setpName(String pName) {
        this.pName = pName;
    }

    public String getpType() {
        return pType;
    }

    public void setpType(String pType) {
        this.pType = pType;
    }

    public Date getEffectFrom() {
        return effectFrom;
    }

    public void setEffectFrom(Date effectFrom) {
        this.effectFrom = effectFrom;
    }

    public Date getEffectTo() {
        return effectTo;
    }

    public void setEffectTo(Date effectTo) {
        this.effectTo = effectTo;
    }

    public Integer getpStatus() {
        return pStatus;
    }

    public void setpStatus(Integer pStatus) {
        this.pStatus = pStatus;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getAuditor() {
        return auditor;
    }

    public void setAuditor(String auditor) {
        this.auditor = auditor;
    }

    public String getUpdator() {
        return updator;
    }

    public void setUpdator(String updator) {
        this.updator = updator;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getIsCode() {
        return isCode;
    }

    public void setIsCode(String isCode) {
        this.isCode = isCode;
    }

    public String getTop_value() {
        return top_value;
    }

    public void setTop_value(String top_value) {
        this.top_value = top_value;
    }

    public String getShowType() {
        return showType;
    }

    public void setShowType(String showType) {
        this.showType = showType;
    }

    public String getIsWMS() {
        return isWMS;
    }

    public void setIsWMS(String isWMS) {
        this.isWMS = isWMS;
    }

    public String getInitCode() {
        return initCode;
    }

    public void setInitCode(String initCode) {
        this.initCode = initCode;
    }
}
