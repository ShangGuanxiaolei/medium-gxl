package cn.hds.hvmall.entity.piece;

/**
 * @author qiuchuyi
 * @date 2018/9/7 19:24
 */
public class BackPOJOPartSpec {
    private Integer pieceGroupNum;
    private Double promotionPrice;
    private Double point;

    public Integer getPieceGroupNum() {
        return pieceGroupNum;
    }

    public void setPieceGroupNum(Integer pieceGroupNum) {
        this.pieceGroupNum = pieceGroupNum;
    }

    public Double getPromotionPrice() {
        return promotionPrice;
    }

    public void setPromotionPrice(Double promotionPrice) {
        this.promotionPrice = promotionPrice;
    }

    public Double getPoint() {
        return point;
    }

    public void setPoint(Double point) {
        this.point = point;
    }

    @Override
    public String toString() {
        return "BackPOJOPartSpec{" +
                "pieceGroupNum=" + pieceGroupNum +
                ", promotionPrice=" + promotionPrice +
                ", point=" + point +
                '}';
    }
}
