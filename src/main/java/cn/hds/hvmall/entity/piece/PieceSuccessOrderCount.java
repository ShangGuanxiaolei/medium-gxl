package cn.hds.hvmall.entity.piece;

public class PieceSuccessOrderCount {
    private int pieceGroupNum;
    private int count;

    public int getPieceGroupNum() {
        return pieceGroupNum;
    }

    public void setPieceGroupNum(int pieceGroupNum) {
        this.pieceGroupNum = pieceGroupNum;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
