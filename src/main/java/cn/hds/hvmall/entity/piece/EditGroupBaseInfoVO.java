package cn.hds.hvmall.entity.piece;

import java.util.Date;

/**
 * 用于更新,特意将pieceGroupNum,skuDiscount字段改成String类型
 * @Author: zzl
 * @Date: 2018/9/20 13:34
 * @Version
 */
public class EditGroupBaseInfoVO{


  /**
   * 活动名称
   */
  private String pName;

  /**
   * 拼团活动时间  是两个字段effect_from 和 effect_to
   */

  private Date effectFrom;

  /**
   * 拼团活动时间  交给前端显示即可
   */
  private Date effectTo;

  /**
   * 成团有效时间
   */
  private int pieceEffectTime;

  /**
   * 组团资格
   */
  private String orgGroupQua;

  /**
   * 参团资格
   */
  private String joinGroupQua;

  /**
   * 可用德分
   */
  private int isWithPoint;

  /**
   * 计算业绩
   */
  private int isAchiev;


  /**
   * 限购件数
   */
  private int skuLimit;

  public int getSkuLimit() {
    return skuLimit;
  }

  public void setSkuLimit(int skuLimit) {
    this.skuLimit = skuLimit;
  }

  public String getpName() {
    return pName;
  }

  public void setpName(String pName) {
    this.pName = pName;
  }

  public Date getEffectFrom() {
    return effectFrom;
  }

  public void setEffectFrom(Date effectFrom) {
    this.effectFrom = effectFrom;
  }

  public Date getEffectTo() {
    return effectTo;
  }

  public void setEffectTo(Date effectTo) {
    this.effectTo = effectTo;
  }

  public int getPieceEffectTime() {
    return pieceEffectTime;
  }

  public void setPieceEffectTime(int pieceEffectTime) {
    this.pieceEffectTime = pieceEffectTime;
  }

  public String getOrgGroupQua() {
    return orgGroupQua;
  }

  public void setOrgGroupQua(String orgGroupQua) {
    this.orgGroupQua = orgGroupQua;
  }

  public String getJoinGroupQua() {
    return joinGroupQua;
  }

  public void setJoinGroupQua(String joinGroupQua) {
    this.joinGroupQua = joinGroupQua;
  }

  public int getIsWithPoint() {
    return isWithPoint;
  }

  public void setIsWithPoint(int isWithPoint) {
    this.isWithPoint = isWithPoint;
  }

  public int getIsAchiev() {
    return isAchiev;
  }

  public void setIsAchiev(int isAchiev) {
    this.isAchiev = isAchiev;
  }

  /**
   * 活动编码
   */
  private String pCode;

    /**
   * 成团人数
   */
  private String pieceGroupNum;

  /**
   * 拼团折扣
   */
  private String skuDiscount;

  public String getPieceGroupNum() {
    return pieceGroupNum;
  }

  public void setPieceGroupNum(String pieceGroupNum) {
    this.pieceGroupNum = pieceGroupNum;
  }

  public String getSkuDiscount() {
    return skuDiscount;
  }

  public void setSkuDiscount(String skuDiscount) {
    this.skuDiscount = skuDiscount;
  }

  public String getpCode() {
    return pCode;
  }

  public void setpCode(String pCode) {
    this.pCode = pCode;
  }
}
