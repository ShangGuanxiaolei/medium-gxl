package cn.hds.hvmall.entity.piece;

public class PieceTranInfoVO {
    private String pieceStatus;
    private int count;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getPieceStatus() {
        return pieceStatus;
    }

    public void setPieceStatus(String pieceStatus) {
        this.pieceStatus = pieceStatus;
    }
}
