package cn.hds.hvmall.entity.piece;

import org.hibernate.validator.constraints.Length;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * @author qiuchuyi
 * @date 2018/9/3 17:15
 */
@Table(name = "promotion_pg")
public class PromotionPg {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    //活动编号
    private String pCode;
    //是否包邮
    private Integer isDeliveryReduction;
    //拼团有效时间
    private Integer pieceEffectTime;
    //插队有效时间
    private Integer jumpQueueTime;
    //组团资格
    private String orgGroupQua;
    //参团资格
    private String joinGroupQua;
    //每人限购数量
    private Integer skuLimit;
    //德分可用
    @Length(max = 1, message = "可用德分位必须在0-1位")
    private Integer isWithPoint;
    //是否计算业绩
    @Length(max = 1, message = "计算业绩开关位必须在0-1位")
    private Integer isAchiev;
    //删除标记位
    @Length(max = 1, message = "删除位必须在0-1位")
    private Integer isDeleted;
    //创建人
    private String creator;
    //更新人
    private String updator;
    //创建时间
    private Date createdAt;
    //更新时间
    private Date updatedAt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getpCode() {
        return pCode;
    }

    public void setpCode(String pCode) {
        this.pCode = pCode;
    }

    public Integer getPieceEffectTime() {
        return pieceEffectTime;
    }

    public void setPieceEffectTime(Integer pieceEffectTime) {
        this.pieceEffectTime = pieceEffectTime;
    }

    public String getOrgGroupQua() {
        return orgGroupQua;
    }

    public Integer getIsDeliveryReduction() {
        return isDeliveryReduction;
    }

    public void setIsDeliveryReduction(Integer isDeliveryReduction) {
        this.isDeliveryReduction = isDeliveryReduction;
    }

    public void setOrgGroupQua(String orgGroupQua) {
        this.orgGroupQua = orgGroupQua;
    }

    public String getJoinGroupQua() {
        return joinGroupQua;
    }

    public void setJoinGroupQua(String joinGroupQua) {
        this.joinGroupQua = joinGroupQua;
    }

    public Integer getSkuLimit() {
        return skuLimit;
    }

    public void setSkuLimit(Integer skuLimit) {
        this.skuLimit = skuLimit;
    }

    public Integer getIsWithPoint() {
        return isWithPoint;
    }

    public void setIsWithPoint(Integer isWithPoint) {
        this.isWithPoint = isWithPoint;
    }

    public Integer getIsAchiev() {
        return isAchiev;
    }

    public void setIsAchiev(Integer isAchiev) {
        this.isAchiev = isAchiev;
    }

    public Integer getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getUpdator() {
        return updator;
    }

    public void setUpdator(String updator) {
        this.updator = updator;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getJumpQueueTime() {
        return jumpQueueTime;
    }

    public void setJumpQueueTime(Integer jumpQueueTime) {
        this.jumpQueueTime = jumpQueueTime;
    }

    @Override
    public String toString() {
        return "PromotionPg{" +
                "id=" + id +
                ", pCode='" + pCode + '\'' +
                ", pieceEffectTime=" + pieceEffectTime +
                ", orgGroupQua='" + orgGroupQua + '\'' +
                ", joinGroupQua='" + joinGroupQua + '\'' +
                ", skuLimit=" + skuLimit +
                ", isWithPoint=" + isWithPoint +
                ", isAchiev=" + isAchiev +
                ", isDeleted=" + isDeleted +
                ", creator='" + creator + '\'' +
                ", updator='" + updator + '\'' +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                '}';
    }
}
