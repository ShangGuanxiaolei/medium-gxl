package cn.hds.hvmall.entity.piece;

import java.util.Date;

/**
 * @Author: zzl
 * @Date: 2018/9/22 23:24
 * @Version
 */
public class EffectTimePo {

  private Date effectFrom;

  private Date effectTo;

  public Date getEffectFrom() {
    return effectFrom;
  }

  public void setEffectFrom(Date effectFrom) {
    this.effectFrom = effectFrom;
  }

  public Date getEffectTo() {
    return effectTo;
  }

  public void setEffectTo(Date effectTo) {
    this.effectTo = effectTo;
  }
}
