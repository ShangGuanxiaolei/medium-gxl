package cn.hds.hvmall.entity.piece;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

/**
 * entity类--活动库存扣减明细表(promotion_stock_detail)
 * @类名: PromotionStockDetail .
 * @描述: 拼团 .
 * @程序猿: guoxia .
 * @日期: 2017年3月3日 下午2:29:46 .
 * @版本号: V1.0 .
 */
public class PromotionStockDetail {
    private long id;
    private String p_code;//活动编码
    private String p_detail_code;//活动详情编码
    private String sku_code;//Sku编码
    private String member_id;//会员ID
    private Integer tran_sku_num;//交易Sku数量
    private Integer tran_type;//交易类型 1:剩余库存释放;2:拼团扣除，3:人工预锁库存
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date tran_time;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date created_at;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updated_at;
    private Integer is_deleted;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getP_code() {
        return p_code;
    }

    public void setP_code(String p_code) {
        this.p_code = p_code;
    }

    public String getP_detail_code() {
        return p_detail_code;
    }

    public void setP_detail_code(String p_detail_code) {
        this.p_detail_code = p_detail_code;
    }

    public String getSku_code() {
        return sku_code;
    }

    public void setSku_code(String sku_code) {
        this.sku_code = sku_code;
    }

    public String getMember_id() {
        return member_id;
    }

    public void setMember_id(String member_id) {
        this.member_id = member_id;
    }

    public Integer getTran_sku_num() {
        return tran_sku_num;
    }

    public void setTran_sku_num(Integer tran_sku_num) {
        this.tran_sku_num = tran_sku_num;
    }

    public Integer getTran_type() {
        return tran_type;
    }

    public void setTran_type(Integer tran_type) {
        this.tran_type = tran_type;
    }

    public Date getTran_time() {
        return tran_time;
    }

    public void setTran_time(Date tran_time) {
        this.tran_time = tran_time;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public Date getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(Date updated_at) {
        this.updated_at = updated_at;
    }

    public Integer getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(Integer is_deleted) {
        this.is_deleted = is_deleted;
    }
}
