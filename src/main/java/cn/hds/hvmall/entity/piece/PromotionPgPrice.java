package cn.hds.hvmall.entity.piece;

import cn.hds.hvmall.entity.BaseEntityArchivable;
import cn.hds.hvmall.type.PromotionType;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * @author gxl
 */
public class PromotionPgPrice extends BaseEntityArchivable {

    /**
     * 拼团活动详情编码
     */
    private String pDetailCode;

    private Long skuId;

    private Long promotionId;

    /**
     * 活动价
     */
    private BigDecimal promotionPrice = BigDecimal.ZERO;

    /**
     * 德分
     */
    private BigDecimal point = BigDecimal.ZERO;

    /**
     * 立减
     */
    private BigDecimal reduction = BigDecimal.ZERO;

    /**
     * 净值
     */
    private BigDecimal netWorth = BigDecimal.ZERO;

    /**
     * 推广费
     */
    private BigDecimal promoAmt = BigDecimal.ZERO;

    /**
     * 服务费
     */
    private BigDecimal serverAmt = BigDecimal.ZERO;

    /**
     * 新人免单
     */
    private Boolean newFree = false;

    /**
     * 活动类型, 默认拼团
     */
    private PromotionType type = PromotionType.PIECE;

    /**
     * 拼主价次数设置类型,0->,不设置1->统一设置,2->分别设置
     */
    private Integer unifiedSetup = 0;

    private static final long serialVersionUID = 1L;

    public String getpDetailCode() {
        return pDetailCode;
    }

    public void setpDetailCode(String pDetailCode) {
        this.pDetailCode = pDetailCode;
    }

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    public Long getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(Long promotionId) {
        this.promotionId = promotionId;
    }

    public BigDecimal getPromotionPrice() {
        return promotionPrice;
    }

    public void setPromotionPrice(BigDecimal promotionPrice) {
        this.promotionPrice = promotionPrice;
    }

    public BigDecimal getPoint() {
        return point;
    }

    public BigDecimal getConversionPrice() {
        if (promotionPrice == null || point == null) {
            return BigDecimal.ZERO;
        }
        return promotionPrice.subtract(point.divide(BigDecimal.TEN, 2, RoundingMode.HALF_EVEN));
    }

    public void setPoint(BigDecimal point) {
        this.point = point;
    }

    public BigDecimal getReduction() {
        return reduction;
    }

    public void setReduction(BigDecimal reduction) {
        this.reduction = reduction;
    }

    public BigDecimal getNetWorth() {
        return netWorth;
    }

    public void setNetWorth(BigDecimal netWorth) {
        this.netWorth = netWorth;
    }

    public BigDecimal getPromoAmt() {
        return promoAmt;
    }

    public void setPromoAmt(BigDecimal promoAmt) {
        this.promoAmt = promoAmt;
    }

    public BigDecimal getServerAmt() {
        return serverAmt;
    }

    public void setServerAmt(BigDecimal serverAmt) {
        this.serverAmt = serverAmt;
    }

    public Boolean getNewFree() {
        return newFree;
    }

    public void setNewFree(Boolean newFree) {
        this.newFree = newFree;
    }

    public Integer getUnifiedSetup() {
        return unifiedSetup;
    }

    public void setUnifiedSetup(Integer unifiedSetup) {
        this.unifiedSetup = unifiedSetup;
    }

    public PromotionType getType() {
        return type;
    }

    public void setType(PromotionType type) {
        this.type = type;
    }
}