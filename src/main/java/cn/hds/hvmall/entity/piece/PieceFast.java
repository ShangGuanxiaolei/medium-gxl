package cn.hds.hvmall.entity.piece;

import java.util.Date;

public class PieceFast {
    private String nickname;
    private String groupHeadId;
    private String groupOpenMemberId;
    private String pieceGroupTranCode;

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getGroupHeadId() {
        return groupHeadId;
    }

    public void setGroupHeadId(String groupHeadId) {
        this.groupHeadId = groupHeadId;
    }

    public String getGroupOpenMemberId() {
        return groupOpenMemberId;
    }

    public void setGroupOpenMemberId(String groupOpenMemberId) {
        this.groupOpenMemberId = groupOpenMemberId;
    }

    public String getPieceGroupTranCode() {
        return pieceGroupTranCode;
    }

    public void setPieceGroupTranCode(String pieceGroupTranCode) {
        this.pieceGroupTranCode = pieceGroupTranCode;
    }

    public String getDuring() {
        return during;
    }

    public void setDuring(String during) {
        this.during = during;
    }

    private String during;
}
