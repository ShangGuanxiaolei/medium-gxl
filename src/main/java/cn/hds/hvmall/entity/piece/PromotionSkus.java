package cn.hds.hvmall.entity.piece;

import org.hibernate.validator.constraints.Length;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * @author qiuchuyi
 * @date 2018/9/6 14:18
 */
@Table(name = "promotion_skus")
public class PromotionSkus {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    //产品编号
    private String productId;
    //活动编码
    private String pCode;
    //sku编码
    private String skuCode;
    //创建人
    private String creator;
    //审核人
    private String auditor;
    //更新人
    private String updator;
    //创建时间
    private Date createdAt;
    //更新时间
    private Date updatedAt;
    //删除标记位
    @Length(min = 0, max = 1, message = "删除位必须在0-1位")
    private Integer isDeleted;
    //拼团活动skuNum
    private Integer skuNum;

    private Integer gift;

    public Integer getSkuNum() {
        return skuNum;
    }

    public void setSkuNum(Integer skuNum) {
        this.skuNum = skuNum;
    }

    @Override
    public String toString() {
        return "PromotionSkus{" +
                "id=" + id +
                ", productId='" + productId + '\'' +
                ", pCode='" + pCode + '\'' +
                ", skuCode='" + skuCode + '\'' +
                ", creator='" + creator + '\'' +
                ", auditor='" + auditor + '\'' +
                ", updator='" + updator + '\'' +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                ", isDeleted=" + isDeleted +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getpCode() {
        return pCode;
    }

    public void setpCode(String pCode) {
        this.pCode = pCode;
    }

    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getAuditor() {
        return auditor;
    }

    public void setAuditor(String auditor) {
        this.auditor = auditor;
    }

    public String getUpdator() {
        return updator;
    }

    public void setUpdator(String updator) {
        this.updator = updator;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Integer getGift() {
        return gift;
    }

    public void setGift(Integer gift) {
        this.gift = gift;
    }

}
