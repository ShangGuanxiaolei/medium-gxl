package cn.hds.hvmall.entity.piece;

/**
 * entity类--汉薇商品库存表(xquark_sku)
 *
 * @类名: XquarkSkus .
 * @描述: TODO .
 * @程序猿: guoxia .
 * @日期: 2017年3月3日 下午2:29:46 .
 * @版本号: V1.0 .
 */
public class XquarkSku {
  private long id;//主键
  private int amount;//库存
  private String sku_code;//商品唯一标识码
  private int archive;//是否删除：0=删除，1=正常

  public int getAmount() {
    return amount;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public void setAmount(int amount) {
    this.amount = amount;
  }

  public String getSku_code() {
    return sku_code;
  }

  public void setSku_code(String sku_code) {
    this.sku_code = sku_code;
  }

  public int getArchive() {
    return archive;
  }

  public void setArchive(int archive) {
    this.archive = archive;
  }
}
