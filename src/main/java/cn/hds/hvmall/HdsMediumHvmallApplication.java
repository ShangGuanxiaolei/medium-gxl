package cn.hds.hvmall;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
public class HdsMediumHvmallApplication {

	public static void main(String[] args) {
		SpringApplication.run(HdsMediumHvmallApplication.class, args);
	}
}
