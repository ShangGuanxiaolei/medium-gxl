package cn.hds.hvmall.utils.list.qiniu.json;

/**
 * Created by wangxinhua on 18-3-29. DESC:
 */
public class JsonImgMediaSerialize extends AbstractJsonImgQuailtySerialize {

  @Override
  protected String getExtFormat() {
    return "?imageMogr2/auto-orient/thumbnail/640x/quality/50";
  }

}
