package cn.hds.hvmall.utils.list.qiniu;

import cn.hds.hvmall.utils.list.UpLoadFileVO;
import com.qiniu.api.auth.AuthException;
//import com.xquark.dal.type.FileBelong;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import org.json.JSONException;
import org.springframework.web.multipart.MultipartFile;

public interface Qiniu {

  static String SPLIT_SYMBOL = "|";

  /**
   * 根据文件所属，获得空间名
   */
  String genBucketName(FileBelong belong);

  /**
   * 根据空间名获取上传授权凭证 服务端上传所用
   */
  String genUpToken(String bucketName) throws AuthException, JSONException;

  /**
   * 上传图片
   */
  List<UpLoadFileVO> uploadImg(List<MultipartFile> files, FileBelong belong) throws AuthException,
      JSONException, IOException;


  /**
   * 客户端上传，须获取token
   */
  String genUpTokenForClient(String bucketName) throws AuthException, JSONException;

  /**
   * 流的方式上传
   */
  List<UpLoadFileVO> uploadImgStream(List<InputStream> ins, FileBelong belong)
      throws AuthException, JSONException, IOException;

  /**
   * 把七牛的key转换成本地数据库的key
   */
  String qiniuKeyToLocalKey(String qiniuKey, String bucketName);

  /**
   * 得到图片的完整url
   */
  String genQiniuFileUrl(String localKey);

  /**
   * 得到图片原图url
   */
  String genQiniuOrig(String localKey);

}
