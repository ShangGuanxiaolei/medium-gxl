package cn.hds.hvmall.utils.list.qiniu;

/**
 * 图片所属 不同的图片类型放入不同的空间
 *
 * @author huxaya
 */
public enum FileBelong {
  //	ECMOHOO2O,
  MERCHANT,
  PRODUCT,
  SHOP,
  STAT, //统计类
  RESOURCE,
  LOG,  //日志上传Crash
  OTHER //未归类

}
