package cn.hds.hvmall.utils.list.qiniu;

//import com.xquark.dal.mybatis.IdTypeHandler;
import java.util.Date;
import org.apache.commons.lang3.StringUtils;

public abstract class BaseEntityImpl implements BaseEntity {

  private static final long serialVersionUID = 1L;
  private String id;
  private long idRaw = -1;

  private Date createdAt; //创建时间，插入逻辑在 mysql 实现，只查询
  private Date updatedAt; //更新时间，更新逻辑在 mysql 实现，只查询

  public String getId() {
    if (StringUtils.isBlank(id) && idRaw > 0) {
      return IdTypeHandler.encode(idRaw);
    }

    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public long getIdRaw() {
    return idRaw;
  }

  public void setIdRaw(long idRaw) {
    this.idRaw = idRaw;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  public Date getCreatedAt() {
    return createdAt;
  }

  public void setUpdatedAt(Date updatedAt) {
    this.updatedAt = updatedAt;
  }

  public Date getUpdatedAt() {
    return updatedAt;
  }


}
