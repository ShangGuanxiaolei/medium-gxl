package cn.hds.hvmall.utils.list.qiniu;

import java.io.Serializable;

public interface BaseEntity extends Serializable {

  String getId();

}
