package cn.hds.hvmall.utils.list.qiniu.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import cn.hds.hvmall.utils.list.qiniu.SpringContextUtil;
import cn.hds.hvmall.utils.list.qiniu.ResourceResolver;

import java.io.IOException;

public class JsonResourceUrlSerializer extends JsonSerializer<String> {

  //	private final static ThreadLocal<Map<String, String>> imageSize = new ThreadLocal<Map<String, String>>();
  final private String qiniuExtFmt = "/@w/$w$@/@h/$h$@";

  @Override
  public void serialize(String value, JsonGenerator jgen, SerializerProvider provider)
      throws IOException, JsonProcessingException {
    if (value == null || !value.startsWith("qn|")) {
      jgen.writeString(value);
      return;
    }
    ResourceResolver resourceFacade = (ResourceResolver) SpringContextUtil
        .getBean("resourceFacade");
    String url = resourceFacade.resolveUrl(value);
    //后缀不应该在这里直接添加
    jgen.writeString(url + qiniuExtFmt);
  }
}
