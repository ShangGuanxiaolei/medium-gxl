package cn.hds.hvmall.utils.list;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.HashMap;
import java.util.Map;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum GlobalErrorCode {
  //
  SUCESS(200, "Success"),
  //
  UNAUTHORIZED(401, "Unauthorized"),
  //
  NOT_FOUND(404, "Resource not found"),
  //
  INTERNAL_ERROR(500, "Server internal error"),
  //
  INVALID_ARGUMENT(11001, "Invalid argument"),
  //错误的参数，原参数已修改， 页面需重新刷新
  INVALID_ARGUMENT_2(11002, "Invalid argument"),
  //
  THIRDPLANT_BUZERROR(700, "Business error"),
  //
  DUPLICATE_PROMOTION_PRODUCT(800, "不能创建同类型商品的活动,请失效之前的活动"),
  UNKNOWN(-1, "Unknown error"),
  PIDNOTFOUND(302, "pid not found");

    private static final Map<Integer, GlobalErrorCode> values = new HashMap<Integer, GlobalErrorCode>();

  static {
    for (GlobalErrorCode ec : GlobalErrorCode.values()) {
      values.put(ec.errorCode, ec);
    }
  }

  private int errorCode;
  private String error;

  private GlobalErrorCode(int errorCode, String error) {
    this.errorCode = errorCode;
    this.error = error;
  }

  public static GlobalErrorCode valueOf(int code) {
    GlobalErrorCode ec = values.get(code);
    if (ec != null) {
      return ec;
    }
    return UNKNOWN;
  }

  public int getErrorCode() {
    return errorCode;
  }

  public String getError() {
    return error;
  }
}
