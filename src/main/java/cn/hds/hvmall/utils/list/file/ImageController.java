package cn.hds.hvmall.utils.list.file;

import cn.hds.hvmall.base.BaseExecuteResult;
import cn.hds.hvmall.utils.list.GlobalErrorCode;
import cn.hds.hvmall.utils.list.UpLoadFileVO;
import cn.hds.hvmall.utils.list.qiniu.BizException;
import cn.hds.hvmall.utils.list.qiniu.FileBelong;
import cn.hds.hvmall.utils.list.qiniu.ResourceFacade;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.minidev.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.support.RequestContext;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@Controller
@Api(value = "image", description = "上传图片管理", produces = MediaType.APPLICATION_JSON_VALUE)
public class ImageController {

  @Autowired
  private ResourceFacade resourceFacade;

  @Value("${product.size.limit}")
  private String sizeLimit;

  private Logger log = LoggerFactory.getLogger(getClass());

  @InitBinder
  protected void initBinder(WebDataBinder binder) {
    binder.setValidator(new ImageUploadValidator());
  }

  /**
   * 上传富文本图片，返回的结果是error message url
   * <br>/_f/u-desc
   */
  @ResponseBody
  @RequestMapping(value = ResourceFacade.RES_PREFIX_UP_FILE
      + "/u_desc", method = RequestMethod.POST)
  @ApiIgnore
  public JSONObject uploadDesc(ImageUploadForm form, MultipartHttpServletRequest request)
      throws Exception {
    JSONObject obj = new JSONObject();

    List<MultipartFile> fileList = request.getFiles("imgFile");
    List<UpLoadFileVO> result = resourceFacade.uploadFile(fileList, FileBelong.PRODUCT);

    if (result.isEmpty()) {
      obj.put("error", 1);
      obj.put("message", "上传图片失败");
    } else {
      obj.put("error", 0);
      obj.put("message", "上传图片成功");
      obj.put("url", result.get(0).getUrl());
    }
		/*
		// 定义图片的格式
		HashMap<String, String> extMap = new HashMap<String, String>();
		extMap.put("image", "gif,jpg,jpeg,png,bmp");
		String dirName = request.getParameter("dir"); // 文件的后缀
		if(!extMap.containsKey(dirName)){
			 throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "图片格式不正确");
		}
		
		FileItemFactory factory = new DiskFileItemFactory();
		ServletFileUpload upload = new ServletFileUpload(factory);
		upload.setHeaderEncoding("UTF-8");
		List<FileItem> items =null;
		try{
			 items = upload.parseRequest(request);
		}catch(Exception e){
			log.debug("获取表单字段异常");
		}
		Iterator itr = items.iterator();
		while (itr.hasNext()) {
			FileItem item = (FileItem) itr.next();
			
			if (item.isFormField()) {
				continue;
			}
			
			String fileName = item.getName();
			//检查扩展名
			String fileExt = fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase();
			if(!Arrays.<String>asList(extMap.get(dirName).split(",")).contains(fileExt)){
				throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "上传文件扩展名是不允许的扩展名。\n只允许" + extMap.get(dirName) + "格式。");
			}
			
			UpLoadFileVO  vo=resourceFacade.uploadFileStream(item.getInputStream(),FileBelong.PRODUCT);
			if(vo!=null){
				obj.put("error", "0");
				obj.put("url", vo.getUrl() );
			}else{
				obj.put("error", "1");
				obj.put("message", "上传图片失败");
			}
			return obj;
		}*/

    return obj;
  }

  /**
   * pc 上传图片/_f/u
   */
  @ResponseBody
  @RequestMapping(value = ResourceFacade.FILE_STORE_KEY_V2 + "/u", method = RequestMethod.POST)
  @ApiOperation(value = "上传本地图片", notes = "上传本地图片，返回qiniu相关信息", httpMethod = "POST", produces = MediaType.APPLICATION_JSON_VALUE)
  public BaseExecuteResult<List<UpLoadFileVO>> upload(@Valid @ModelAttribute ImageUploadForm form,
                                                       HttpServletRequest request)
      throws Exception {
    //上传文件大小控制
    Long sizeLongLimit = Long.valueOf(sizeLimit);
    List<MultipartFile> listFiles = form.getFile();
    for (MultipartFile file : listFiles) {
      long fileSize = file.getSize();
      if (fileSize > sizeLongLimit) {
        throw new BizException(GlobalErrorCode.UNKNOWN, ("新增图片大小不能超过" + sizeLongLimit + "字节"));
      }

    }
    if (form.getBelong() == null) {
      log.warn("upload warning 文件belong为空  file length=[" + form.getFile().size() + "]");
      RequestContext requestContext = new RequestContext(request);
      throw new BizException(GlobalErrorCode.UNKNOWN,
          requestContext.getMessage("valid.fileBelong.message"));
    }
    return new BaseExecuteResult<>(1,resourceFacade.uploadFile(form.getFile(), form.getBelong()));
  }

  @ResponseBody
  @RequestMapping(value = ResourceFacade.RES_PREFIX_UP_FILE + "/getUpToken")
  @ApiIgnore
  public BaseExecuteResult<String> uploadToken(FileBelong fileBelong, HttpServletRequest req)
      throws Exception {
    if (fileBelong == null) {
      RequestContext requestContext = new RequestContext(req);
      throw new BizException(GlobalErrorCode.INVALID_ARGUMENT,
          requestContext.getMessage("valid.fileBelong.message"));
    }
    return new BaseExecuteResult<>(1,resourceFacade.genUpTokenForClient(fileBelong));
  }

  /**
   * 上传日志不用检查登录
   */
  @ResponseBody
  @RequestMapping(value = ResourceFacade.RES_PREFIX_UP_FILE + "/u/log", method = RequestMethod.POST)
  @ApiIgnore
  public BaseExecuteResult<Boolean> uploadLog(@Valid @ModelAttribute ImageUploadForm form,
                                           Errors errors, HttpServletRequest request)
      throws Exception {
    if (form.getBelong() == null) {
      RequestContext requestContext = new RequestContext(request);
      throw new BizException(GlobalErrorCode.UNKNOWN,
          requestContext.getMessage("valid.fileBelong.message"));
    }
    return new BaseExecuteResult<>(1,
        resourceFacade.uploadFile(form.getFile(), form.getBelong()).size() > 0);
  }
}
