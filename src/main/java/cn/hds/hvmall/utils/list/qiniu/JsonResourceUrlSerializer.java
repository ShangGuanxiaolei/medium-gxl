package cn.hds.hvmall.utils.list.qiniu;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.io.IOException;

public class JsonResourceUrlSerializer extends JsonSerializer<String> {

  final private String qiniuExtFmt = "/@w/$w$@/@h/$h$@";

  @Autowired
  private Qiniu qiniu;

  @Value("${site.web.host.name}")
  String siteHost;

  static final String RES_PREFIX_SYS = "/_resources";
  /**
   * 七牛文件存储标识(第二版本)
   */
  static final String FILE_STORE_KEY_V2 = "qn" + Qiniu.SPLIT_SYMBOL;
  /**
   * 用户上传的文件资源的url前缀
   */
  static final String RES_PREFIX_UP_FILE = "/_f";
  /**
   * 系统自带的静态资源的key的前缀
   */
  static final String EXT_RES_PREFIX = "http://";

  @Override
  public void serialize(String value, JsonGenerator jgen, SerializerProvider provider)
      throws IOException, JsonProcessingException {
    if (value == null || !value.startsWith("qn|")) {
      jgen.writeString(value);
      return;
    }
//    ResourceResolver resourceFacade = (ResourceResolver) SpringContextUtil
//        .getBean("resourceFacade");
//    String url = resourceFacade.resolveUrl(value);
    String ddddd = resolveUrl(value);
    //后缀不应该在这里直接添加
    jgen.writeString(ddddd + qiniuExtFmt);
  }

  private String resolveUrl(String resKey) {
    if (StringUtils.isBlank(resKey)) {
      resKey = RES_PREFIX_SYS + "/images/404.png";
      return resKey;
    } else if (resKey.startsWith(EXT_RES_PREFIX)) {
      return resKey;
    } else if (resKey.startsWith(RES_PREFIX_SYS)) {
      return siteHost + resKey;
    } else if (resKey.startsWith(FILE_STORE_KEY_V2)) {
      return qiniu.genQiniuFileUrl(resKey);// 返回七牛的URL，直接去七牛服务器上取
    }
    return siteHost + RES_PREFIX_UP_FILE + (resKey.startsWith("/") ? "" : "/") + resKey;
  }
}
