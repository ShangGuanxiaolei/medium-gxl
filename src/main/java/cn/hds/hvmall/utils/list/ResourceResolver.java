package cn.hds.hvmall.utils.list;

public interface ResourceResolver {

  /**
   * 给定资源key，转换为相应的url
   */
  String resolveUrl(String resKey);

  String resolveUrl2Orig(String resKey);
}
