package cn.hds.hvmall.utils.list.qiniu.json;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by wangxinhua on 2018/4/16. DESC: 前段日期反序列化
 */
public class JsonStringDateDeserializer extends JsonDeserializer<Date> {

  private static SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");

  @Override
  public Date deserialize(JsonParser jp, DeserializationContext ctxt)
      throws IOException {
    String date = jp.getText();
    try {
      return format.parse(date);
    } catch (ParseException e1) {
      format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
      try {
        return format.parse(date);
      } catch (ParseException e2) {
        format = new SimpleDateFormat("yyyy-MM-dd");
        try {
          return format.parse(date);
        } catch (ParseException e3) {
          throw new RuntimeException(e3);
        }
      }
    }
  }
}
