package cn.hds.hvmall.utils.list.qiniu.json;

import cn.hds.hvmall.utils.list.qiniu.ResourceResolver;
import cn.hds.hvmall.utils.list.qiniu.SpringContextUtil;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import java.io.IOException;

/**
 * Created by wangxinhua on 18-3-29. DESC:
 */
public abstract class AbstractJsonImgQuailtySerialize extends JsonSerializer<String> {

  /**
   * 通过分割符 - 加样式名称(样式名称在七牛管理中配置) 指定压缩参数 添加七牛图片压缩处理
   *
   * @return 图片压缩参数后缀
   */
  protected abstract String getExtFormat();

  @Override
  public void serialize(String value, JsonGenerator jgen, SerializerProvider provider)
      throws IOException {
    if (value == null || !value.startsWith("qn|")) {
      jgen.writeString(value);
      return;
    }
    ResourceResolver resourceFacade = (ResourceResolver) SpringContextUtil
        .getBean("resourceFacade");
    String url = resourceFacade.resolveUrl2Orig(value);
    //后缀不应该在这里直接添加
    jgen.writeString(url + getExtFormat());
  }

}
