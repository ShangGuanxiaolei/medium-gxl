package cn.hds.hvmall.utils.list.qiniu;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class SpringContextUtil implements ApplicationContextAware {

  private static ApplicationContext context;// 声明一个静态变量保存

  @Override
  public void setApplicationContext(ApplicationContext contex) throws BeansException {
    context = contex;
  }

  public static ApplicationContext getContext() {
    return context;
  }

  /**
   * 直接获取bean
   */
  public static Object getBean(String beanName) {
    return context.getBean(beanName);
  }

  public static <T> T getBean(Class<T> requiredType) {
    return context.getBean(requiredType);
  }

  public static <T> T getBean(String beanName, Class<T> requiredType) {
    return context.getBean(beanName, requiredType);
  }
}

