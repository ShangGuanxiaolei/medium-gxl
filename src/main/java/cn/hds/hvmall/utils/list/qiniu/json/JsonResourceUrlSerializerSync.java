package cn.hds.hvmall.utils.list.qiniu.json;

import cn.hds.hvmall.utils.list.qiniu.ResourceResolver;
import cn.hds.hvmall.utils.list.qiniu.SpringContextUtil;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import java.io.IOException;

public class JsonResourceUrlSerializerSync extends JsonSerializer<String> {

  //	private final static ThreadLocal<Map<String, String>> imageSize = new ThreadLocal<Map<String, String>>();
  @Override
  public void serialize(String value, JsonGenerator jgen, SerializerProvider provider)
      throws IOException, JsonProcessingException {
    ResourceResolver resourceFacade = (ResourceResolver) SpringContextUtil
        .getBean("resourceFacade");
    String url = resourceFacade.resolveUrl2Orig(value);
    jgen.writeString(url);
  }
}
