package cn.hds.hvmall.utils.list;

public interface BaseEntityService<E> {

  /**
   * 检查默认的not null，和default value等约束要求，
   */
  int insert(E e);

  E load(String id);

}
