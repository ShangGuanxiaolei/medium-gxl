package cn.hds.hvmall.utils;

import cn.hds.hvmall.mapper.cuxiao.PromotionBaseInfo;
import cn.hds.hvmall.mapper.cuxiao.PromotionBaseInfoCopyMapper;
import cn.hds.hvmall.utils.list.GlobalErrorCode;
import cn.hds.hvmall.utils.list.qiniu.BizException;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * Created with IntelliJ IDEA.
 * User: chp
 * Date: 2019/3/26
 * Time: 14:02
 * Description: 促销商品相关
 */
public class ActivityUtil {
  @Autowired
  private PromotionBaseInfoCopyMapper promotionBaseInfoCopyMapper;

    /**
   * @Author chp
   * @Description //获取活动的状态
   * @Date
   * @Param
   * @return
   **/
  public  String  getActivityStatus(String pCode) throws  Exception{
      List<PromotionBaseInfo> byPCode   = promotionBaseInfoCopyMapper.findByPCode(pCode);
      if(byPCode!=null){
          Date effectFrom = byPCode.get(0).getEffectFrom();
          Date effectTo =  byPCode.get(0).getEffectTo();
          Date date = new Date();
         if (date.before(effectTo)&&date.after( effectFrom )){
             if (byPCode.get(0).getpStatus()==6){   return   "已终止"; }
             byPCode.get(0).getpStatus();
             return   "进行中";
         }
         else if (date.before(effectFrom))    { return  "即将开始";}
         else if (date.after(effectTo))   {return  "已结束";}
      }
      return null;
  }


}
