package cn.hds.hvmall.utils.sql;

import lombok.val;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * @author wangxinhua
 * @date 2019-05-12
 * @since 1.0
 */
public class SqlHelper {

    private final static String IN = " IN ";

    public static String isIn(Number... args) {
        val in = isInNonWrapper(args);
        return IN + String.format("(%s)", in);
    }

    public static String isIn(Enum... args) {
        return isInWithString((Object[]) args);
    }

    public static String isIn(String... args) {
        return isInWithString((Object[]) args);
    }

    private static String isInWithString(Object... args) {
        val in = Arrays.stream(args)
                .map(SqlHelper::wrapper)
                .collect(Collectors.joining(","));
        return IN + String.format("(%s)", in);
    }

    private static String isInNonWrapper(Number... args) {
        return StringUtils.join(args, ",");
    }

    private static String wrapper(Object o) {
        return String.format("'%s'", o);
    }
}
