package cn.hds.hvmall.utils;

import cn.hds.hvmall.entity.sku.Sku;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

/**
 * @author luqing
 * @since 2019-05-09
 */
public class BeanUtil {
    public static String[] getNullPropertyNames (Object source) {
        final BeanWrapper src = new BeanWrapperImpl(source);
        java.beans.PropertyDescriptor[] pds = src.getPropertyDescriptors();

        Set<String> emptyNames = new HashSet<>();
        for(java.beans.PropertyDescriptor pd : pds) {
            Object srcValue = src.getPropertyValue(pd.getName());
            if (srcValue == null) {
                emptyNames.add(pd.getName());
            }
        }
        String[] result = new String[emptyNames.size()];
        return emptyNames.toArray(result);
    }

    public static void main(String[] args) {
        Sku sku1 = new Sku();
        Sku sku2 = new Sku();

        sku1.setSkuCode("123");
        sku1.setPromotionType("123");
        sku1.setPrice(new BigDecimal(10));
        sku1.setDeductionDPoint(new BigDecimal(10));

        sku2.setSkuCode("1234");
        sku2.setPrice(BigDecimal.TEN);

        BeanUtils.copyProperties(sku1,sku2,BeanUtil.getNullPropertyNames(sku1));

        System.out.println(sku2);
    }
}
