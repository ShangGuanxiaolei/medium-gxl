package cn.hds.hvmall.utils;

/**枚举类
 * @类名: ConstantUtil .
 * @描述: TODO .
 * @程序猿: chenjingwu .
 * @日期: 2017年3月3日 上午11:49:10 .
 * @版本号: V1.0 .
 */
public class ConstantUtil {

	public static int success = 1;
	public static int failed = 0;
	public static int vfailed = -1;
	public static final String ENCRYPT_PWD = "A/S62x7'cr,FRK4vjO5s";//加密字符串，用于注册和登录
	public static final String IN_PARAMETER_FORMAT = "========>>>>{}.{}->in params:{}";
    public static final String OUT_PARAMETER_FORMAT = "========>>>>{}.{}->out params:{}";
    public static final String ERROR_FORMAT = "========>>>>{}.{}->error:{}";
	
	//1.使用 2.续存 3.清空
	public enum ISUSERD  {
		USEING(1,"使用"),
		USED(2,"续存"),
		CLEAR(3,"清空");
	    	
	    	private Integer status;
	    	private String nameCn;
	    	ISUSERD(Integer status, String nameCn) {
	            this.status = status;
	            this.nameCn = nameCn;
	       	}
	    
	    public Integer getStatus() {
				return status;
			}

			public void setStatus(Integer status) {
				this.status = status;
			}

			public String getNameCn() {
				return nameCn;
			}

			public void setNameCn(String nameCn) {
				this.nameCn = nameCn;
			}
	    }
	
	//职位，1普通职员，2管理员，3店长，4值班经理 5.合伙人 6.保洁 7.园丁
	public enum DOORMESSAGE  {
		ERRORPT(-8,"对不起，场馆已满，教练已帮您调换场地，详情请联系PT"),
		WARINGPT(-7,"对不起，上课期间不能进入！"),
		NULLPT(-6,"尊敬的PT，请在开课前15分进入，谢谢！"),
		NULLOBJECT(-5,"对不起,您还没有人脸数据!"),
		MEMBER(-4,"尊敬的会员您好,欢迎光临!"),
		NULL(-3,"尊敬的会员，对不起，您还没有购课！！"),
		WARING(-2,"尊敬的会员，您不在上课时间内\n请确认时间后再来！"),
		ERROR(-1,"对不起，您的权限不够，无法进入！"),
		PT(0,"PT您好,欢迎光临!"),
		PUTONG(1,"员工您好,欢迎光临!"),
		ADMIN(2,"尊敬的管理员您好，欢迎光临!"),
		DINGZHANG(3,"尊敬的店长您好，欢迎光临!"),
		MANAGER(4,"尊敬的值班经理您好，欢迎光临!"),
		HEHUO(5,"尊敬的合伙人您好，欢迎光临!"),
		CLEAR(6,"保洁您好,欢迎光临!"),
		YUANDING(7,"园丁您好,欢迎光临!"),;
	    	
	    	private Integer status;
	    	private String nameCn;
	    	DOORMESSAGE(Integer status, String nameCn) {
	            this.status = status;
	            this.nameCn = nameCn;
	       	}
	    
	    public Integer getStatus() {
				return status;
			}

			public void setStatus(Integer status) {
				this.status = status;
			}

			public String getNameCn() {
				return nameCn;
			}

			public void setNameCn(String nameCn) {
				this.nameCn = nameCn;
			}
	    }
	
	
	/**
     * 消息
     */
    public enum ResponseMSG {
    	UP_OK("success", "30分定时扫描2天内的预约表记录,更新数据---%s条", "1000"),
    	RELEASE_OK("OK", "教练占用时间释放成功", "1001"),
    	UP_WAIT("success", "扫描结束，查无数据,继续等候处理--------", "1002"),
    	SAVE_OK("save_ok","存储成功！","1003"),
    	USEDING_OK("useing_ok","使用成功！","1004"),
    	USED_OK("used_ok","续存成功！","1005"),
    	CLEAR_OK("clear_ok","清空离场！","1006"),
    	UNBUND_OK("unbund_ok","解绑成功！","1007"),
		THEME_ID_SORT_OK("theme_id_sort_ok","分类排序成功","10015");

        private String msg_en;
        private String msg_cn;
        private String code;

        ResponseMSG(String msg_en, String msg_cn, String code) {
            this.msg_en = msg_en;
            this.msg_cn = msg_cn;
            this.code = code;
        }

        @Override
        public String toString() {
            return this.msg_cn;
        }

        public String getEn() {
            return msg_en;
        }

        public String getCn() {
            return msg_cn;
        }

        public String getCode() {
            return this.code;
        }
    }

    /**
     * 错误
     */
    public enum ResponseError {
    	FAILED("失败", "1111"),
    	SYS_ERROR("系统错误,入参格式有误!", "1000"),
    	INVALID_CLIENTID("无效的客户端标识","30003"),
    	INVALID_PASSWORD("手机号码输入有误，请重新输入","30004" ),
    	INVALID_CAPTCHA("无效的令牌或者令牌已过期,请重新登陆","30005"),
    	INVALID_TOKEN("无效的令牌","30006"),
    	VALIDATE_COACH_TIME("字典数据为空，或教练未发布时间对应的时间，请联系管理员！","1003"),
    	SAVE_FAILED("存储失败！","1004"),
        VERIFY_CODE_ERR4("密码输入有误,请重新输入!","1011"),
        VERIFY_CODE_ERR5("上次离场，未清空衣柜，请联系管理员或前台进行解绑操作，谢谢配合!","1012"),
        VERIFY_CODE_ERR6("密码为空，请去APP设置密码!","1013"),
        VERIFY_CODE_ERR7("您的权限不够，请找管理员或值班经理处理，谢谢!","1014"),
        VERIFY_CODE_ERR8("您今天未购买训练课程，请购课后再使用!","1015"),
		THEME_ID_ERROR("分类id异常","1055"),
		THEME_ID_SORT_ERROR("排序失败","1056"),
		CHANGE_THEME_ERROR("再次分类失败","1057"),
    	OBJECTNULL("查询不到数据!","9998");
        
        private String error;
        private String code;

        ResponseError(String error, String code) {
            this.error = error;
            this.code = code;
        }

        @Override
        public String toString() {
            return this.error;
        }

        public String getCode() {
            return this.code;
        }

        public String getError() {
            return this.error;
        }
    }

	
	
}
