package cn.hds.hvmall.utils.invitecode;

import org.springframework.beans.factory.annotation.Value;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/**
 * 根据活动类型随机生成邀请码
 */
public class InviteCodeUtil {

    public InviteCodeUtil() {}

    public static String getUUID() {
        UUID uuid = UUID.randomUUID();
        String str = uuid.toString();
        // 去掉"-"符号
        String temp = str.substring(0, 8) + str.substring(9, 13) + str.substring(14, 18) + str.substring(19, 23) + str.substring(24);
        return temp;
    }

    public static Set<String> generateInviteCode(int codeNum) {
        Set<String> inviteCodeSet = new HashSet<>();
        while (true) {
            String uuid = InviteCodeUtil.getUUID();
            if (inviteCodeSet.size() > codeNum+100) {
                return inviteCodeSet;
            } else {
                String lowerCase = uuid.substring(0, 6).toLowerCase();
                if (lowerCase.contains("0") || lowerCase.contains("1") || lowerCase.contains("i") || lowerCase.contains("o")) {
                    continue;
                } else {
                    inviteCodeSet.add(lowerCase);
                }
            }
        }
    }
}
