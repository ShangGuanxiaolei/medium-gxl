package cn.hds.hvmall.utils.excel;

import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.util.HSSFColor;

/**
 * Created by wangxinhua. Date: 2018/8/22 Time: 下午3:05
 */
public class SheetHeader {

  /**
   * 标题数据
   */
  private final String[] contents;

  /**
   * 字体大小, 默认15
   */
  private final short fontSize;

  /**
   * 字体名称, 默认楷体
   */
  private final String fontName;

  /**
   * 默认字体粗细
   */
  private final short boldWeight;

  /**
   * 默认字体颜色
   */
  private final short fontColor;

  public String[] getContents() {
    return contents;
  }

  public short getFontSize() {
    return fontSize;
  }

  public String getFontName() {
    return fontName;
  }

  public short getBoldWeight() {
    return boldWeight;
  }

  public short getFontColor() {
    return fontColor;
  }

  public SheetHeader(String[] contents, short fontSize, String fontName, short boldWeight,
                     short fontColor) {
    this.contents = contents;
    this.fontSize = fontSize;
    this.fontName = fontName;
    this.boldWeight = boldWeight;
    this.fontColor = fontColor;
  }

  public static final class Builder {

    private final String[] contents;
    private short fontSize = 15;
    private String fontName = "楷体";
    private short boldWeight = HSSFFont.BOLDWEIGHT_BOLD;
    private short fontColor = HSSFColor.BLACK.index;

    private Builder(String[] contents) {
      this.contents = contents;
    }

    public static Builder start(String[] contents) {
      return new Builder(contents);
    }

    public Builder fontSize(short fontSize) {
      this.fontSize = fontSize;
      return this;
    }

    public Builder fontName(String fontName) {
      this.fontName = fontName;
      return this;
    }

    public Builder boldWeight(short boldWeight) {
      this.boldWeight = boldWeight;
      return this;
    }

    public Builder fontColor(short fontColor) {
      this.fontColor = fontColor;
      return this;
    }

    public SheetHeader build() {
      return new SheetHeader(contents, fontSize, fontName, boldWeight, fontColor);
    }

  }
}
