package cn.hds.hvmall.utils.excel;

import com.baomidou.mybatisplus.toolkit.CollectionUtils;
import com.google.common.base.Preconditions;

import java.util.List;

/**
 * Created by wangxinhua. Date: 2018/8/20 Time: 下午12:30 包含excel中sheet信息的对象
 */
public class SheetContent<T> {

  /**
   * sheet的标题
   */
  private final String title;

  /**
   * 表格数据集合
   */
  private final List<T> objList;

  /**
   * 表格数据类型
   */
  private final Class<T> objType;

  /**
   * 数据对应的get方法
   */
  private final String[] dataBody;

  /**
   * 标题集合
   */
  private final List<SheetHeader> headers;

  public SheetContent(String title, List<T> objList, Class<T> objType,
                      String[] dataBody, List<SheetHeader> headers) {
    this.title = title;
    this.objList = objList;
    this.objType = objType;
    this.dataBody = dataBody;
    Preconditions.checkArgument(CollectionUtils.isNotEmpty(headers));
    this.headers = headers;
  }

  public String getTitle() {
    return title;
  }

  public List<T> getObjList() {
    return objList;
  }

  public Class<T> getObjType() {
    return objType;
  }

  public String[] getDataTitle() {
    return headers.get(headers.size() - 1).getContents();
  }

  public String[] getDataBody() {
    return dataBody;
  }

  public List<SheetHeader> getHeaders() {
    return headers;
  }
}
