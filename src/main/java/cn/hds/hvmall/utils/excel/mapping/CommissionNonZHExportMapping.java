package cn.hds.hvmall.utils.excel.mapping;

import com.google.common.collect.ImmutableMap;

import java.util.Arrays;
import java.util.Map;

/**
 * Created by wangxinhua. Date: 2018/8/20 Time: 下午3:40
 */
public class CommissionNonZHExportMapping extends BaseExportMapping {

  private static final Map<String, String> INNER_MAP = ImmutableMap.<String, String>builder()
      .put("序号", "getGenerator.next")
      .put("转入账号", "getBankAccount")
      .put("转入名称", "getName")
      .put("金额", "getAmount")
      .put("转入行行名", "getBankName")
      .put("转入行行号", "getBankNumber")
      .put("备注", "getRemark")
      .put("错误标识", "getErrorMsg")
      .put("证件号码", "getTinCode")
      .put("手机号", "getPhone")
      .build();

  @Override
  protected Map<String, String> provideMapping() {
    return INNER_MAP;
  }

  public static void main(String[] args) {
    System.out.println(Arrays.toString(new CommissionNonZHExportMapping().title()));
  }
}
