package cn.hds.hvmall.utils.excel;


import cn.hds.hvmall.utils.functional.StringToDecimal;
import cn.hds.hvmall.utils.functional.StringToInteger;
import cn.hds.hvmall.utils.functional.StringToString;
import com.google.common.base.Function;

import java.math.BigDecimal;

/**
 * @author wangxinhua on 2018/7/28. DESC:
 */
public class ColumnMapping<T> {

  private String name;

  private com.google.common.base.Function<String, T> transformer;

  public static Function<String, String> STRING_TO_STRING = new StringToString();

  public static Function<String, BigDecimal> STRING_TO_DECIMAL = new StringToDecimal();

  public static Function<String, Integer> STRING_TO_INTEGER = new StringToInteger();

  public ColumnMapping(String name,
                       Function<String, T> transformer) {
    this.name = name;
    this.transformer = transformer;
  }

  public String getName() {
    return name;
  }

  public Function<String, T> getTransformer() {
    return transformer;
  }
}
