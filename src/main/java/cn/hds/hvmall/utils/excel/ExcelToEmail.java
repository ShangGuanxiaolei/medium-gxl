package cn.hds.hvmall.utils.excel;

import java.io.*;

import java.util.*;

import cn.hds.hvmall.entity.freshman.FreshmanSaleCount;
import org.apache.poi.hssf.usermodel.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * 导出并发送到邮箱内
 *
 *
 *
 * */
public class ExcelToEmail {



    private final Logger logger = LoggerFactory.getLogger(ExcelToEmail.class);


    public static HSSFCell getCell(HSSFSheet sheet, int row, int col) {

        HSSFRow sheetRow = sheet.getRow(row);

        if (sheetRow == null) {

            sheetRow = sheet.createRow(row);

        }

        HSSFCell cell = sheetRow.getCell(col);

        if (cell == null) {

            cell = sheetRow.createCell(col);

        }

        return cell;

    }

    public static void setText(HSSFCell cell, String text) {

        cell.setCellType(HSSFCell.CELL_TYPE_STRING);

        cell.setCellValue(text);

    }

    public static void toExcel(List<String> titles, List<FreshmanSaleCount> list,String filePath) throws Exception {

        HSSFWorkbook wb = new HSSFWorkbook(); // 定义一个新的工作簿

        HSSFSheet sheet = wb.createSheet("第一个Sheet页");  // 创建第一个Sheet页

        // 第四步，创建单元格，并设置值表头 设置表头居中

        HSSFCellStyle style = wb.createCellStyle();

        style.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 创建一个居中格式

        HSSFRow row = sheet.createRow(0);// 创建一个行

        HSSFCell cell = row.createCell(0); // 创建一个单元格  第1列

        //cell.setCellValue(new Date());  // 给单元格设置值

        for (int i = 0; i < titles.size(); i++) { //设置标题

            String title = titles.get(i);

            cell = getCell(sheet, 0, i);

            setText(cell, title);

            cell.setCellStyle(style);

        }

        for (int i = 0; i < list.size(); i++) {

            FreshmanSaleCount vpd = list.get(i);

            for (int j = 0; j < titles.size(); j++) {
                String varstr="";
                if(j==0) {
                    varstr = list.get(i).getProductId();
                }
                if(j==1) {
                    varstr = list.get(i).getProductName();
                }
                if(j==2) {
                    varstr = String.valueOf(list.get(i).getAmount());
                }
                if(j==3) {
                    varstr = String.valueOf(list.get(i).getSkuId());
                }
                if(j==4) {
                    varstr = list.get(i).getSkuCode();
                }
                if(j==5) {
                    varstr = String.valueOf(list.get(i).getMonthSales());
                }
                if(j==6) {
                    varstr = String.valueOf(list.get(i).getYesterdaySales());
                }

                cell = getCell(sheet, i + 1, j);

                setText(cell, varstr);

                cell.setCellStyle(style);

            }

        }

            FileOutputStream fileOut = new FileOutputStream(filePath);

            wb.write(fileOut);

            fileOut.close();



    }




}