package cn.hds.hvmall.utils.piece;

import cn.hds.hvmall.pojo.Users;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

import java.sql.SQLOutput;

/**
 * @author qiuchuyi
 * @date 2018/9/6 9:51
 */
public class RestTempleUtil {
    public static void main(String[] args) {
        RestTemplate restTemplate = new RestTemplate();
        String uri = "http://login.handeson.com/users/userLogin";
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Accept","application/json");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        String request = "{'username': 'admin','password': '123456'}";
        HttpEntity<String> entity = new HttpEntity<String>(request, httpHeaders);
        Users users = new Users();
        users.setUsername("admin");
        users.setPassword("123456");
        restTemplate.postForObject(uri,entity,Users.class);
    }
}

