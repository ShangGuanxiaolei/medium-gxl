package cn.hds.hvmall.mapper.tovip;


import cn.hds.hvmall.pojo.tovip.PromotionToVip;
import cn.hds.hvmall.pojo.tovip.PromotionToVipType;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;
import java.util.List;

public interface PromotionToVipMapper {
    int deleteByPrimaryKey(Long id);

    /**
     * 添加
     * @param record
     * @return
     */

    int insert(PromotionToVip record);

    int insertSelective(PromotionToVip record);

    PromotionToVip selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(PromotionToVip record);

    /**
     * 更新
     * @param record
     * @return
     */

    int updateByPrimaryKey(PromotionToVip record);

    Integer selectByType(@Param("type") PromotionToVipType type);

    /**
     *
     * @param
     * @return
     * 分页查找
     */

    List<PromotionToVip> selectList(@Param("page") Pageable page);

    /**
     * 分页总记录条数
     * @return
     */

    Integer countList();

    /**
     * 更新并使用
     * @param promotionToVip
     * @return
     */

    int updateAndUse(PromotionToVip promotionToVip);

    /**
     * 停用或启用
     * @param promotionToVip
     * @return
     */

    int changeState(PromotionToVip promotionToVip);

    /**
     * 查找
     * @param id
     * @return
     */

    PromotionToVip loadById(Long id);

    /**
     *
     *查看是否已存在配置
     */
    Integer isExist(@Param("type") String type);
}