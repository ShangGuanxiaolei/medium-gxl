package cn.hds.hvmall.mapper.sku;

import cn.hds.hvmall.entity.sku.Sku;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public interface SkuMapper {

    int insert(Sku record);

    Sku selectByPrimaryKey(Long id);

    Sku selectBySkuCode(String code);

    int updateByPrimaryKeySelective(Sku record);

    List<Sku> selectByProductId(Long prodId);

    Boolean checkIsNotDelete(@Param("skuCode") String skuCode);

    String selectSkuCode(@Param("id") Long id);

    List<Long> loadProductBySkuCodes(Collection<String> skuCodes);

    List<Sku> loadAlreadyCopyBySkuCodes(@Param("skuCodes") Collection<String> skuCodes,
                                        @Param("grandId") Integer grandId);


}