package cn.hds.hvmall.mapper.piece;

import cn.hds.hvmall.entity.piece.PromotionPgPrice;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface PromotionPgPriceMapper {

    int deleteByPrimaryKey(Integer id);

    int insert(PromotionPgPrice record);

    int insertSelective(PromotionPgPrice record);

    PromotionPgPrice selectByPrimaryKey(Integer id);

    PromotionPgPrice selectByDetailCode(String detailCode);

    PromotionPgPrice selectByPromotionIdAndSkuId(@Param("promotionId") Long promotionId,
                                                 @Param("skuId") Long skuId);

    int updateByPrimaryKeySelective(PromotionPgPrice record);

    int updateByPromotionIdAndSkuId(PromotionPgPrice record);

    int updateByPrimaryKey(PromotionPgPrice record);

    int deleteByPDetailCode(String PDetailCode);
}