package cn.hds.hvmall.mapper.piece;

import cn.hds.hvmall.entity.bargain.PromotionBargainRule;
import cn.hds.hvmall.enums.CareerLevelType;
import cn.hds.hvmall.pojo.bargain.BargainRuleVO;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PromotionBargainRuleMapper {

    int deleteByPrimaryKey(String id);

    int insert(PromotionBargainRule record);

    PromotionBargainRule selectByPrimaryKey(String id);

    /**
     * 根据用户身份配置查询砍价规则
     * @param identity 用户的身份
     * @return 砍价规则列表
     */
    PromotionBargainRule selectByIdentity(CareerLevelType identity);

    /**
     * 查询所有配置的规则
     * @return 规则列表
     */
    List<PromotionBargainRule> listAll();

    int updateByPrimaryKeySelective(PromotionBargainRule record);

    int updateByPrimaryKey(PromotionBargainRule record);

    int updateByCareerLevelType(PromotionBargainRule rule);

    int updateForNew(BargainRuleVO rule);

    int updateForOld(BargainRuleVO rule);
}