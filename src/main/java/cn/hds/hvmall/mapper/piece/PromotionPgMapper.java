package cn.hds.hvmall.mapper.piece;

import cn.hds.hvmall.entity.piece.PromotionPg;
import com.baomidou.mybatisplus.mapper.BaseMapper;


/**
 * @author qiuchuyi
 * @date 2018/9/4 10:57
 */
public interface PromotionPgMapper extends BaseMapper<PromotionPg> {

}
