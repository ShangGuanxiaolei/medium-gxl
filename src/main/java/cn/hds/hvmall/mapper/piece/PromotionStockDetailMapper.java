package cn.hds.hvmall.mapper.piece;
import cn.hds.hvmall.entity.piece.PromotionStockDetail;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.Map;

/**
 * 活动库存扣减明细表(promotion_stock_detail)
 * @描述: 拼团 .
 * @程序猿: guoxia .
 * @日期: 2017年3月3日 下午2:29:46 .
 * @版本号: V1.0 .
 */
public interface PromotionStockDetailMapper extends BaseMapper<PromotionStockDetail> {
}
