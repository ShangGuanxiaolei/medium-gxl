package cn.hds.hvmall.mapper.piece;

import cn.hds.hvmall.entity.piece.PromotionTempStock;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;

/**
 * @Author: zzl
 * @Date: 2018/9/22 12:45
 * @Version
 */
public interface PTempStockMapper {

  int insert(@Param("pts") PromotionTempStock pts);
}
