package cn.hds.hvmall.mapper.piece;

import cn.hds.hvmall.entity.piece.PromotionTempStock;
import cn.hds.hvmall.entity.promotion.PromotionProduct;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import javax.persistence.Table;
import java.util.List;
import java.util.Map;

/**
 * 活动库存类(promotion_temp_stock)
 * @描述: 拼团 .
 * @程序猿: guoxia .
 * @日期: 2017年3月3日 下午2:29:46 .
 * @版本号: V1.0 .
 */
public interface PromotionTempStockMapper{
    int insertOne(Map product);

    int editPromotionTempStock(PromotionProduct product);

    int addPromotionTempStock(PromotionProduct product);

    int deleteStock(Map map);

    int updateTempStock(PromotionTempStock stock);

    int disablePromotionTempStock(String pCode);

    List<PromotionTempStock> selectByMap(Map<String,Object> temp);

}
