package cn.hds.hvmall.mapper.piece;

import cn.hds.hvmall.entity.piece.PromotionSkus;
import cn.hds.hvmall.pojo.promotion.PromotionSkuVo;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.jdbc.SQL;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.special.InsertListMapper;

import java.util.List;

/**
 * @author qiuchuyi
 * @date 2018/9/6 14:28
 */
@Repository
public interface PromotionSkusMapper extends BaseMapper<PromotionSkus>, InsertListMapper<PromotionSkus> {
  @Select("select * from promotion_skus where p_code=#{pCode}")
  List<PromotionSkus> selectPromotionSkusInfoByPCode(@Param("pCode") String pCode);

  /**
   * FIXME 使用 selectProvider
   */
  @Select("select " +
          "ps.id, ps.product_id,ps.p_code,ps.sku_code,ps.creator,ps.auditor," +
          "ps.updator,ps.created_at,ps.updated_at,ps.is_deleted,ps.sku_num,ps.gift " +
          "FROM " +
          "promotion_skus ps " +
          "WHERE ps.is_deleted = 1 " +
          "AND ps.product_id=#{productId}")
  List<PromotionSkuVo> getPromotionByProductId(Long productId);
}
