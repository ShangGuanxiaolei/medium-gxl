package cn.hds.hvmall.mapper.piece;

import cn.hds.hvmall.pojo.piece.BackPGThreeVO;
import cn.hds.hvmall.pojo.grandsale.PromotionVO;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author qiuchuyi
 * @date 2018/9/6 17:15
 */
public interface ComplexQueryMapper {
    /**
     * 查询拼团清单75%
     *
     * @param name
     * @param productId
     * @param pStatus
     * @return
     */
    @Select("<script>SELECT pbi.id,pbi.p_name,pbi.p_code,ps.product_id,xp.`name`,xp.price,pbi.effect_from,pbi.effect_to,pp.piece_effect_time,pbi.p_status\n" +
            "\tfrom promotion_base_info pbi,promotion_skus ps,promotion_pg pp,xquark_product xp\n" +
            "\t\twhere pbi.p_code = ps.p_code\n" +
            "\t\t\tAND pp.p_code = pbi.p_code\n" +
            "\t\t\t AND xp.id = ps.product_id\n" +
            "\t\t\t AND pbi.is_deleted = 1\n" +
            "and ps.is_deleted=1\n" +
            "\t\t\t\t\t\tand pp.is_deleted=1\n" +
            "\t\t\t\t\t\tand xp.archive=0"+
            "<if test='name != null and name != \"\"'> " +
            "AND xp.`name` like CONCAT('%',#{name},'%') " +
            "</if> " +
            "<if test='productId != null and productId != \"\"'> " +
            "AND ps.product_id=#{productId} " +
            "</if> " +
            "<if test='pStatus != null and pStatus != \"\"'> " +
            "AND pbi.p_status=#{pStatus} " +
            "</if> " +
            "group by pbi.p_code" +
            "\t ORDER BY pbi.created_at desc\n" +
            "\t limit #{first},#{pageSize}\n" +
            "</script> ")
    List<BackPGThreeVO> queryPGInventory(
            @Param("name") String name,
            @Param("productId") String productId,
            @Param("pStatus") Integer pStatus,
            @Param("first") Integer first,
            @Param("pageSize") Integer pageSize);





    /**
     * 查询拼团清单总数
     *
     * @param name
     * @param productId
     * @param pStatus
     * @return
     */
    @Select("<script>SELECT count(distinct pbi.p_code) from promotion_base_info pbi,promotion_skus ps,promotion_pg pp,xquark_product xp \n" +
            "\t\twhere  pbi.p_code=  ps.p_code \n" +
            "\t\t\tAND  pp.p_code= pbi.p_code\n" +
            "\t\t\t AND xp.id = ps.product_id\n" +
            "\t\t\t AND pbi.is_deleted = 1\n" +
            "<if test='name != null and name != \"\"'> " +
            "AND xp.`name` like CONCAT('%',#{name},'%') " +
            "</if> " +
            "<if test='productId != null and productId != \"\"'> " +
            "AND ps.product_id=#{productId} " +
            "</if> " +
            "<if test='pStatus != null and pStatus != \"\"'> " +
            "AND pbi.p_status=#{pStatus} " +
            "</if> " +
            "</script> ")
    int listCount(@Param("name") String name,
                  @Param("productId") String productId,
                  @Param("pStatus") Integer pStatus);


}
