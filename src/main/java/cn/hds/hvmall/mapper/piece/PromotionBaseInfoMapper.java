package cn.hds.hvmall.mapper.piece;

import cn.hds.hvmall.entity.piece.EffectTimePo;
import cn.hds.hvmall.entity.piece.PromotionBaseInfo;
import cn.hds.hvmall.entity.piece.PromotionPg;
import cn.hds.hvmall.entity.piece.PromotionPgMasterPrice;
import cn.hds.hvmall.pojo.grandsale.PromotionVO;
import cn.hds.hvmall.pojo.piece.PGVO;
import cn.hds.hvmall.pojo.piece.PartSpec;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * @author qiuchuyi
 * @date 2018/9/3 17:05
 */
@Repository
public interface PromotionBaseInfoMapper extends BaseMapper<PromotionBaseInfo> {
    /**
     * 后台更新活动状态
     * @param pCode
     * @param pStatus
     * @return
     */
    @Update("UPDATE promotion_base_info set p_status=#{pStatus} where p_code=#{pCode}")
    Integer updatePStatus(@Param("pCode") String pCode, @Param("pStatus") String pStatus);

    @Select("select effect_from,effect_to from promotion_base_info where p_code=#{pCode}")
    EffectTimePo selectEffectTimeFromAndEffectTimeTo(@Param("pCode")String pCode);

    List<PromotionBaseInfo> queryPromotionBaseInfoList(@Param("pName")String pName,@Param("pStatus")String pStatus,@Param("productName")String productName,@Param("skuCode")String skuCode,@Param("OffSet")int OffSet, @Param("pageSize")int pageSize);

    int queryPromotionBaseInfoCount(@Param("pName")String pName,@Param("pStatus")String pStatus,@Param("productName")String productName,@Param("skuCode")String skuCode);

    @Insert("INSERT INTO promotion_base_info "
        + "(p_code, p_name, p_type, effect_from, effect_to, p_status, created_at, updated_at, is_deleted, buy_limit) "
        + "VALUES (#{promotionBaseInfo.pCode}, #{promotionBaseInfo.pName}, #{promotionBaseInfo.pType}, "
        + "#{promotionBaseInfo.effectFrom}, #{promotionBaseInfo.effectTo}, #{promotionBaseInfo.pStatus}, "
        + "now(), now(), #{promotionBaseInfo.isDeleted}, #{promotionBaseInfo.buyLimit})")
    int insertPromotionBaseInfo(@Param("promotionBaseInfo") PromotionBaseInfo promotionBaseInfo);

    PGVO selectByProCode(@Param("pCode")String pCode);

    List<Map<String, Object>> getPgProducts(@Param("pCode")String pCode);

    PartSpec getPartSpec(@Param("pDetailCode")String pDetailCode);

    PromotionPgMasterPrice getMasterPrice(@Param("pDetailCode")String pDetailCode);

    int updateBaseInfo(PromotionBaseInfo promotionBaseInfo);

    int updatePg(PromotionPg promotionPg);

    Boolean isDuplicatePromotion(String productId);

    int DetelPromotion(@Param("pCode")String pCode);

    int DetelSkus(@Param("pCode")String pCode);

    int DetelTempStock(@Param("pCode")String pCode);

    PromotionBaseInfo selectByPCode(String pCode);

    PromotionVO selectPromotionVOByPCode(String pCode);
}
