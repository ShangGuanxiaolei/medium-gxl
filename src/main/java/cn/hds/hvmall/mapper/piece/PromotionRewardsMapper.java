package cn.hds.hvmall.mapper.piece;

import cn.hds.hvmall.entity.piece.PromotionRewards;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.special.InsertListMapper;

/**
 * @author qiuchuyi
 * @date 2018/9/3 17:13
 */

public interface PromotionRewardsMapper extends BaseMapper<PromotionRewards>, InsertListMapper<PromotionRewards> {
    @Delete("delete from promotion_rewards WHERE\n" +
            "        p_code = #{pCode}")
    int deleteByPCode(@Param("pCode") String pCode);


//    int insertByRewards(PromotionRewards promotionRewards);
}
