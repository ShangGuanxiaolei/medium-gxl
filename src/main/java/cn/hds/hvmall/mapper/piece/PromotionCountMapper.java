package cn.hds.hvmall.mapper.piece;

import cn.hds.hvmall.entity.piece.PieceSuccess;
import cn.hds.hvmall.entity.piece.PieceSuccessOrderCount;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author LiHaoYang
 * @ClassName PromotionCountMapper
 * @date 2018/12/10 0010
 */
public interface PromotionCountMapper {
    /**
     *成团总数
     * @param pCode
     * @return
     */
    int selectPieceSuccessCount(@Param("pCode")String pCode);

    /**
     *成团总订单数
     * @param pCode
     * @return
     */
    int selectPieceSuccessOrderCount(@Param("pCode")String pCode);

    /**
     *几人团成团数
     * @param pCode
     * @return
     */
    List<PieceSuccess> selectPieceSuccessList(@Param("pCode")String pCode);

    /**
     *几人团订单数
     * @param pCode
     * @return
     */
    List<PieceSuccessOrderCount> selectPieceSuccessOrderList(@Param("pCode")String pCode);


    /**
     *失败团总数
     * @param pCode
     * @return
     */
    int selectPieceFailCount(@Param("pCode")String pCode);


    /**
     *失败订单数
     * @param pCode
     * @return
     */
    int selectPieceFailOrderCount(@Param("pCode")String pCode);





}
