package cn.hds.hvmall.mapper.piece;

import cn.hds.hvmall.entity.piece.*;
import cn.hds.hvmall.pojo.activity.ActivityVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author LiHaoYang
 * @ClassName PromotionTranInfoMapper
 * @date 2018/12/18 0018
 */
public interface PromotionTranInfoMapper {

    /**
     * 查询开始时间以及结束时间
     * @param pCode
     * @return
     */
    PromotionTranInfo selectTimeBypCode(@Param("pCode")String pCode);

    /**
     * 查询剩余库存以及活动库存
     * @param pCode
     * @return
     */
    PromotionTranInfo selectStock(@Param("pCode")String pCode);

    /**
     * 根据拼团状态查询数量
     * @param pCode
     * @return
     */
    int selectPieceStatusAndCount(@Param("pCode")String pCode);

    /**
     * 拼团人次
     * @param pCode
     * @return
     */
    int selectPiecePeopleCount(@Param("pCode")String pCode);

    /**
     * 根据订单状态查询数量
     * @param pCode
     * @return
     */
    List<OrderListVO> selectOrderCountAndStatus(@Param("pCode")String pCode);

    /**
     * 拼团排名
     * @param pCode
     * @return
     */
    List<GroupSuccessSort> selectGroupSuccess(@Param("pCode")String pCode);

    /**
     * 成团排名
     * @param pCode
     * @return
     */
    List<PieceFast> selectFastSuccess(@Param("pCode")String pCode);

    /**
     * 小时订单排名
     * @param pCode
     * @return
     */
    List<HourRank> selectHourTime(@Param("pCode")String pCode);


    /**
     * 已开团数
     * @param pCode
     * @return
     */
    int selectSuccessCount(@Param("pCode")String pCode);

    /**
     * 已支付订单数
     * @param pCode
     * @return
     */
    int selectPaidOrder(@Param("pCode")String pCode);

    /**
     * 已取消订单数
     * @param pCode
     * @return
     */
    int selectFailOrder(@Param("pCode")String pCode);

    /**
     * 根据订单号或cpid查询团的信息
     * @return
     */
    List<ActivityVO> selectTranInfoByOrderOrCpId(@Param("pCode")String pCode,@Param("cpid")String cpid,@Param("orderNo")String orderNo);

    int selectSuccessPieceCount(@Param("pCode")String pCode);
}
