package cn.hds.hvmall.mapper.piece;

import cn.hds.hvmall.entity.piece.XquarkSku;
import cn.hds.hvmall.pojo.piece.XquarkSkus;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 汉薇商品库存类(xquark_sku)
 * @描述: TODO .
 * @程序猿: guoxia .
 * @日期: 2017年3月3日 下午2:29:46 .
 * @版本号: V1.0 .
 */
public interface XquarkSkuMapper extends BaseMapper<XquarkSku> {
    // 获取商品实体
    List<XquarkSkus> findXquarkSku(XquarkSku xquarkSkuParam);

    //更新商品库存
    boolean updateXquarkSku(@Param("amount") Integer amount, @Param("skuCode") String skuCode);

    String selectSkuCodeById(Long skuId);

    Integer selectAmountByskuCode(String skuCode);

    /**
     * 查询sku的库存
     */
    Integer selectSkuAmount(@Param("skuId")Long skuId);

    /**
     * 修改商品sku的库存
     */
    int updateSkuAmount(@Param("skuAmount")int shuAmount,@Param("skuId")Long skuId);

    /**
     * 根据skuID查询商品的id
     */
    Long selectProductIdBySkuId(@Param("skuId")Long skuId);
}
