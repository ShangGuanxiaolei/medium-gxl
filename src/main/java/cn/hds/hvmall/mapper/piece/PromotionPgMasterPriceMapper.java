package cn.hds.hvmall.mapper.piece;

import cn.hds.hvmall.entity.piece.PromotionPgMasterPrice;

public interface PromotionPgMasterPriceMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(PromotionPgMasterPrice record);

    int insertSelective(PromotionPgMasterPrice record);

    PromotionPgMasterPrice selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(PromotionPgMasterPrice record);

    int updateByPrimaryKey(PromotionPgMasterPrice record);

    int deleteByPDetailCode(String PDetailCode);

    PromotionPgMasterPrice selectByPDetailCode(String PDetailCode);
}