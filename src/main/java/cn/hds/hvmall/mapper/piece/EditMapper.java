package cn.hds.hvmall.mapper.piece;

import cn.hds.hvmall.entity.piece.GroupAmount;
import cn.hds.hvmall.entity.piece.GroupBaseInfoVO;
import cn.hds.hvmall.entity.piece.PromotionPgDetailSimple;
import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface EditMapper {

  /**
   * 依据pCode查询活动数据
   */
  List<GroupBaseInfoVO> select(@Param("pCode")String pCode);

  /**
   * 根据pCode和参数更改PromotionBaseInfo对应字段
   */
  int updatePromotionBaseInfo(@Param("pName") String pName,
      @Param("effectFrom") Date effectFrom,
      @Param("effectTo") Date effectTo,@Param("pCode")String pCode);


  /**
   * 根据pCode和参数更改PromotionPg对应字段
   */
  int updatePromotionPg(@Param("pieceEffectTime") int pieceEffectTime,
      @Param("pCode") String pCode, @Param("orgGroupQua") String orgGroupQua,
      @Param("joinGroupQua") String joinGroupQua,
      @Param("isWithPoint") int isWithPoint,
      @Param("isAchiev") int isAchiev);

  /**
   * 根据pCode和参数更改PromotionPgDetail对应字段
   */
  int updatePromotionPgDetail(@Param("pieceGroupNum") int pieceGroupNum,
      @Param("pCode") String pCode, @Param("skuDiscount") double skuDiscount,
      @Param("skuLimit") int skuLimit);

  /**
   * 通过PCode查询活动创建人和更改人
   */
  List<String> selectPromotionPG(@Param("pCode")String pCode);

  /**
   * 新增活动detail信息
   */
  void insertPromotionPGDetail(@Param("promotionPgDetailSimple")PromotionPgDetailSimple promotionPgDetailSimple);

	/**
	 * 根据pCode查询活动商品信息
	 */
	List<GroupAmount> getProductAmountBypCode(@Param("pCode")String pCode);

	/**
	 * 通过pCode查询活动状态
	 */
	Integer checkPromotionStatus(@Param("pCode")String pCode);

	/**
	 * 通过skuId查询skuCode
	 */
	String selectSkuCodeBySkuId(@Param("skuId")Integer skuId);

	/**
	 * 更新库存
	 */
	int updatePromotionSkuNum(@Param("skuCode")String skuCode,@Param("newSkuNum")int newSkuCode);

	/**
	 * 更新库存表
	 */
	int updatePromotionTempStock(@Param("skuCode")String skuCode,@Param("newSkuNum")int newSkuCode);

	/**
	 * 查询可用库存
	 */
	Integer selectUsableSkuNum(@Param("skuCode")String skuCode,@Param("pCode")String pCode);

	/**
	 * 更新可用库存
	 */
	int updateUsableSkuNum(@Param("skuCode")String skuCode,@Param("usableNum")int usableNum);

	/**
	 * 通过skuCode查询原来的库存
	 */
	Integer selectOldNum(@Param("skuCode")String skuCode,@Param("pCode")String pCode);

	/**
	 * 根据skuCode查询出sku的库存
	 */
	Integer selectAmountBySkuCode(@Param("skuCode")String skuCode);

}
