package cn.hds.hvmall.mapper.piece;

import cn.hds.hvmall.entity.piece.HourRank;
import cn.hds.hvmall.entity.piece.OrderListVO;
import cn.hds.hvmall.entity.promotion.MeetingInfoVo;
import cn.hds.hvmall.entity.promotion.PromotionMeetingCountInfo;
import cn.hds.hvmall.entity.promotion.StockVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author LiHaoYang
 * @ClassName PromotionMeetingCountInfoMapper
 * @date 2019/1/3 0003
 */
public interface PromotionMeetingCountInfoMapper {
    /**
     *查询不同状态的订单数量
     * @param pCode
     * @return
     */
    List<OrderListVO> selectOrderCountAndStatus(@Param("pCode")String pCode);

    /**
     * 查询支付成功的订单数
     * @param pCode
     * @return
     */
    int selectPaidOrder(@Param("pCode")String pCode);

    /**
     * 查询取消的订单数量
     * @param pCode
     * @return
     */
    int selectFailOrder(@Param("pCode")String pCode);

    /**
     * 查询每小时的订单数
     * @param pCode
     * @return
     */
    List<HourRank> selectHourTime(@Param("pCode")String pCode);


    PromotionMeetingCountInfo selectTimeBypCode(@Param("pCode")String pCode);

    List<StockVo> selectStock(@Param("pCode")String pCode);

    /**
     * 查询大会订单详情
     * @param pCode
     * @param memberId
     * @param orderCode
     * @return
     */
    List<MeetingInfoVo> selectMeetingInfo(@Param("pCode")String pCode,@Param("cpid")String cpid,@Param("orderNo")String orderNo);

    List<MeetingInfoVo> selectGiftAmount(@Param("pCode")String pCode,@Param("cpid")String cpid,@Param("orderNo")String orderNo);

}
