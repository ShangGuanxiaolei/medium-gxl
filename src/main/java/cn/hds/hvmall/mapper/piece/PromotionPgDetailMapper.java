package cn.hds.hvmall.mapper.piece;

import cn.hds.hvmall.entity.piece.PromotionPgDetail;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import tk.mybatis.mapper.common.special.InsertListMapper;

import java.util.List;


/**
 * @author qiuchuyi
 * @date 2018/9/3 17:12
 */
public interface PromotionPgDetailMapper extends BaseMapper<PromotionPgDetail>, InsertListMapper<PromotionPgDetail> {

    @Delete("delete from promotion_pg_detail\n" +
            "        where p_code = #{pCode}")
    int deleteByPCode(@Param("pCode") String pCode);

    @Select("select * from promotion_pg_detail where p_code = #{pCode} and is_deleted=1")
    List<PromotionPgDetail> selectByPCode(@Param("pCode") String pCode);

}
