package cn.hds.hvmall.mapper.news;

import cn.hds.hvmall.entity.news.NewsDetail;
import cn.hds.hvmall.entity.news.NewsType;
import org.apache.ibatis.annotations.Param;
import org.springframework.security.access.method.P;

import java.util.List;

/**
 * @author LiHaoYang
 * @ClassName NewsMapper
 * @date 2019-8-7
 */
public interface NewsMapper {

    /**
     * 查询新闻类型
     * @return
     */
    List<NewsType> selectNewsType();

    /**
     * 新增新闻类型
     * @param typeName
     * @return
     */
    boolean insertNewsType(String typeName);

    /**
     * 查询文章列表
     * @param typeId
     * @param status
     * @return
     */
    List<NewsDetail> selectNewsDetailList(@Param("typeId")String typeId,@Param("status")String status,@Param("pageNo")int pageNo,@Param("pageSize")int pageSize);


    /**
     * 根据条件查询文章总数
     * @param typeId
     * @param status
     * @return
     */
    int selectNewsDetailCount(@Param("typeId")String typeId,@Param("status")String status);

    /**
     * 新增文章详情
     * @param typeId
     * @param title
     * @param img
     * @param status
     * @param showTime
     * @param mediaSources
     * @param content
     * @param releaseUser
     * @return
     */
    boolean insertNewsDetail(@Param("typeId")Integer typeId,@Param("title")String title,@Param("img")String img,@Param("status")Integer status,@Param("showTime")String showTime,@Param("mediaSources")String mediaSources,@Param("content")String content,@Param("releaseUser")String releaseUser);

    /**
     * 修改文章详情
     * @param typeId
     * @param title
     * @param img
     * @param status
     * @param showTime
     * @param mediaSources
     * @param content
     * @param newsId
     * @return
     */
    boolean updateNewsDetail(@Param("typeId")Integer typeId,@Param("title")String title,@Param("img")String img,@Param("status")Integer status,@Param("showTime")String showTime,@Param("mediaSources")String mediaSources,@Param("content")String content,@Param("newsId") Integer newsId);

    /**
     * 修改单个新闻的状态
     * @param status
     * @param newsId
     * @return
     */
    boolean updateNewsStatus(@Param("status")Integer status,@Param("newsId")Integer newsId);

    /**
     * 查询新闻具体信息
     * @return
     */
    NewsDetail selectNewsDetail(@Param("detailId") Integer detailId);

    /**
     * 根据typeId查询新闻列表
     * @param typeId
     * @return
     */
    List<NewsDetail> selectNewsListByType(@Param("typeId")Integer typeId);

}
