package cn.hds.hvmall.mapper;

import cn.hds.hvmall.entity.lottery.PromotionLottery;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PromotionLotteryMapper {
    int deleteByPrimaryKey(Long id);

    int insert(@Param("promotionLottery") PromotionLottery promotionLottery);

    PromotionLottery selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(@Param("record")PromotionLottery record);

    int updateByPrimaryKey(PromotionLottery record);

    //删除该活动
	int deletePromotionLottery(@Param("id")Long id);

	List<PromotionLottery> selectLottery(@Param("promotionId")Long promotionId);

}