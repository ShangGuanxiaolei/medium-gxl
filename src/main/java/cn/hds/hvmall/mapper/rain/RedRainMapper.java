package cn.hds.hvmall.mapper.rain;

import cn.hds.hvmall.entity.rain.RainConfigListBean;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RedRainMapper {
  //插入红包雨基本配置
  int insertRain(@Param("list") List<RainConfigListBean> list);

  //根据清单id删除红包雨基本配置
  int deleteByPromotionId(@Param("promotionId") Long promotionId);

  //根据清单id查找红包雨基本配置
  List<RainConfigListBean> selectById(@Param("pId") Long id);

  //更新红包雨基本配置
  int updateRed(@Param("rainConfigListBean") RainConfigListBean rainConfigListBean);

	/**
	 * 添加红包雨德分配置
	 */
	int insertPromotionPacketRain(@Param("rain")RainConfigListBean rain);

	/**
	 * 查询当天活动id列表
	 */
	List<Long> selectIdByPromotionListId(@Param("promotionListId")Long promotionListId);

}
