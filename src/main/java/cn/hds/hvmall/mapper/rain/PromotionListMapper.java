package cn.hds.hvmall.mapper.rain;

import cn.hds.hvmall.controller.RainLotteryPromotionForm;
import cn.hds.hvmall.entity.lottery.ActivityForm;
import cn.hds.hvmall.entity.lottery.ActivityList;
import cn.hds.hvmall.entity.rain.PromotionPacketListVO;
import cn.hds.hvmall.entity.rain.RainTimResult;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface PromotionListMapper {


  int deleteById(@Param("id") Long id);

  int selectStateById(@Param("id") String id);

  //插入list表信息
  int insertList(@Param("promotionListVO") PromotionPacketListVO promotionListVO);

  Date maxEndTime();

  int changeState(@Param("id") String id);

	List<PromotionPacketListVO> selectById(@Param("id") Long id);

  //根据id查找活动开始生效时间与结束时间,promotionId
//  List<PromotionPacketListVO> selectById(@Param("id") Long id);

  //根据id查找基本信息，回显用
  PromotionPacketListVO findById(@Param("id") Long id);

  //更新promotion_list信息
  int updateById(@Param("promotionListVO") PromotionPacketListVO promotionPacketListVO);

  int insertRain(@Param("rainLotteryPromotionForm") RainLotteryPromotionForm rainLotteryPromotionForm);

	// 根据id查询出抽奖表id
	List<Long> selectPromotionLotteryId(@Param("promotionListId")Long promotionListId);

	// 根据id 删除该活动
	int deletePromotionListById(@Param("id")Long id);

	/**
	 * 查询活动清单
	 */
	List<ActivityList> getActivityList(@Param("activityForm")ActivityForm activityForm,
																		 @Param("countIndex")int countIndex,
																		 @Param("pageSize")int pageSize);

	Integer getActivityCount(@Param("activityForm")ActivityForm activityForm);

	/**
	 * 改变活动状态
	 */
	int updateActivityStatus(@Param("promotionId")Long promotionId);

	/**
	 * 更新活动主表信息
	 */
	int updatePromotionList(@Param("promotionList")RainLotteryPromotionForm promotionList);

	/**
	 * 查询所有红包雨的所有时间
	 */
	List<RainTimResult> getUnClosedTimeList(int type);

	/**
	 * 查询抽奖所有时间
	 */
    List<RainTimResult> getLotteryTimeList();
}
