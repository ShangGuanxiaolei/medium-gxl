package cn.hds.hvmall.mapper.rain;

import cn.hds.hvmall.entity.rain.PromotionPacketRainTime;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RainTimeMapper {
  //插入基本信息
  int insert(@Param("list") List<PromotionPacketRainTime> list);

  List<PromotionPacketRainTime> selectByListId(@Param("id") Long id);

  int deleteByRainId(@Param("rainId") Long rainId);

	/**
	 * 添加红包雨场次时间
	 */
	int insertPacketRainTime(@Param("promotionPacketRainTime")PromotionPacketRainTime promotionPacketRainTime);

	/**
	 * 通过当天红包雨id删除红包雨场次
	 */
	int deletePacketRainTimeByRainId(@Param("rainId")Long rainId);

	/**
	 * 更新
	 */
	int updateRainTime(@Param("packetRainTime")PromotionPacketRainTime packetRainTime);

	/**
	 * 查询红包雨场次的id集合
	 */
	List<Long> selectRainTime(@Param("packetRainId")Long packetRainId);

	/**
	 * 查询红包雨时间场次的类
	 */
	List<PromotionPacketRainTime> selectRainTimeList(@Param("packetRainId")Long packetRainId);

	/**
	 * 查询红包雨时间id
	 */
	List<Long> selectRainIdList(@Param("promotionId")Long promotionId);

	/**
	 * 删除红包雨活动时间场次
	 */
	int deletePacketRainTime(@Param("id")Long id);
}
