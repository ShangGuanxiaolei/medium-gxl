package cn.hds.hvmall.mapper.cuxiao;
import org.apache.ibatis.annotations.Param;
import java.util.List;

import cn.hds.hvmall.mapper.cuxiao.PromotionExclusive;

public interface PromotionExclusiveCopyMapper {
    int deleteByPrimaryKey(Long id);

    int insert(PromotionExclusive record);

    int insertSelective(PromotionExclusive record);

    PromotionExclusive selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(PromotionExclusive record);

    int updateByPrimaryKey(PromotionExclusive record);

    List<PromotionExclusive> findByProductIdAndPCode(@Param("productId")Long productId,@Param("pCode")String pCode);







}