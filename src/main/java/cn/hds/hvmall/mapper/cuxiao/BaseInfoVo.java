package cn.hds.hvmall.mapper.cuxiao;

import java.sql.Timestamp;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: chp
 * Date: 2019/3/27
 * Time: 14:11
 * Description:
 */
public class BaseInfoVo {
    private Long id;

    /**
     * 活动编码
     */
    private String pCode;

    /**
     * 活动名称
     */
    private String pName;

    private String status;

    /**
     * 发布时间
     */

//    private Timestamp publish;
    private Date publish;
    /**
     * 开始时间
     */
//    private String effectFrom;
    private Date effectFrom;
    /**
     * 结束时间
     */
    private Date effectTo;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getpCode() {
        return pCode;
    }

    public void setpCode(String pCode) {
        this.pCode = pCode;
    }

    public String getpName() {
        return pName;
    }

    public void setpName(String pName) {
        this.pName = pName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getPublish() {
        return publish;
    }

    public void setPublish(Date publish) {
        this.publish = publish;
    }

    public Date getEffectFrom() {
        return effectFrom;
    }

    public void setEffectFrom(Date effectFrom) {
        this.effectFrom = effectFrom;
    }

    public Date getEffectTo() {
        return effectTo;
    }

    public void setEffectTo(Date effectTo) {
        this.effectTo = effectTo;
    }
}
