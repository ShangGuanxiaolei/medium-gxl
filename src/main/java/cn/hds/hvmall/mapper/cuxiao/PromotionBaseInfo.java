package cn.hds.hvmall.mapper.cuxiao;

import cn.hds.hvmall.entity.freshman.ActivityProduct;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * promotion_base_info
 * @author 
 */
public class PromotionBaseInfo implements Serializable {
    private Long id;

    /**
     * 活动编码
     */
    private String pCode;

    /**
     * 活动名称
     */
    private String pName;

    /**
     * 活动类型
     */
    private String pType;

    /**
     * 开始时间
     */
    private Date effectFrom;

    /**
     * 结束时间
     */
    private Date effectTo;

    /**
     * 活动状态,1：草稿
2：待锁库存
3：待发布
4：已发布待生效
5：已生效
6：人工下线
7：已失效
     */
    private Integer pStatus;

    /**
     * 创建者
     */
    private String creator;

    /**
     * 审核人
     */
    private String auditor;

    /**
     * 首页banner图
     */
    private String bannerImgHome;

    /**
     * 二级页面banner图
     */
    private String bannerImgList;

    /**
     * 是否显示倒计时，0为不显示，1为显示，默认0
     */
    private Boolean timeShow;

    /**
     * 活动类型：0为橱窗型，1为banner型，默认,0
     */
    private Boolean type;

    /**
     * 是否计算收益:0为不计算，1为计算，默认为0
     */
    private Boolean calculateYield;

    /**
     * 是否包邮：0为不包邮，1为包邮，默认为0
     */
    private Boolean carriageFree;

    /**
     * 受众类型,新人(1),白人加新人(2),Vip加店主(3),不限(0)
     */
    private String  customerType;

    /**
     * 角标文字，最多三个字
     */
    private String cornerTab;

    private String updator;

    private Date createdAt;

    private Date updatedAt;

    /**
     * 删除标记,1有效，0已删除
     */
    private Byte isDeleted;

    /**
     * 拼团活动购买限制
     */
    private Integer buyLimit;



    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getpCode() {
        return pCode;
    }

    public void setpCode(String pCode) {
        this.pCode = pCode;
    }

    public String getpName() {
        return pName;
    }

    public void setpName(String pName) {
        this.pName = pName;
    }

    public String getpType() {
        return pType;
    }

    public void setpType(String pType) {
        this.pType = pType;
    }

    public Date getEffectFrom() {
        return effectFrom;
    }

    public void setEffectFrom(Date effectFrom) {
        this.effectFrom = effectFrom;
    }

    public Date getEffectTo() {
        return effectTo;
    }

    public void setEffectTo(Date effectTo) {
        this.effectTo = effectTo;
    }

    public Integer getpStatus() {
        return pStatus;
    }

    public void setpStatus(Integer pStatus) {
        this.pStatus = pStatus;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getAuditor() {
        return auditor;
    }

    public void setAuditor(String auditor) {
        this.auditor = auditor;
    }

    public String getBannerImgHome() {
        return bannerImgHome;
    }

    public void setBannerImgHome(String bannerImgHome) {
        this.bannerImgHome = bannerImgHome;
    }

    public String getBannerImgList() {
        return bannerImgList;
    }

    public void setBannerImgList(String bannerImgList) {
        this.bannerImgList = bannerImgList;
    }

    public Boolean getTimeShow() {
        return timeShow;
    }

    public void setTimeShow(Boolean timeShow) {
        this.timeShow = timeShow;
    }

    public Boolean getType() {
        return type;
    }

    public void setType(Boolean type) {
        this.type = type;
    }

    public Boolean getCalculateYield() {
        return calculateYield;
    }

    public void setCalculateYield(Boolean calculateYield) {
        this.calculateYield = calculateYield;
    }

    public Boolean getCarriageFree() {
        return carriageFree;
    }

    public void setCarriageFree(Boolean carriageFree) {
        this.carriageFree = carriageFree;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public String getCornerTab() {
        return cornerTab;
    }

    public void setCornerTab(String cornerTab) {
        this.cornerTab = cornerTab;
    }

    public String getUpdator() {
        return updator;
    }

    public void setUpdator(String updator) {
        this.updator = updator;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Byte getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Byte isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Integer getBuyLimit() {
        return buyLimit;
    }

    public void setBuyLimit(Integer buyLimit) {
        this.buyLimit = buyLimit;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        PromotionBaseInfo other = (PromotionBaseInfo) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getpCode() == null ? other.getpCode() == null : this.getpCode().equals(other.getpCode()))
            && (this.getpName() == null ? other.getpName() == null : this.getpName().equals(other.getpName()))
            && (this.getpType() == null ? other.getpType() == null : this.getpType().equals(other.getpType()))
            && (this.getEffectFrom() == null ? other.getEffectFrom() == null : this.getEffectFrom().equals(other.getEffectFrom()))
            && (this.getEffectTo() == null ? other.getEffectTo() == null : this.getEffectTo().equals(other.getEffectTo()))
            && (this.getpStatus() == null ? other.getpStatus() == null : this.getpStatus().equals(other.getpStatus()))
            && (this.getCreator() == null ? other.getCreator() == null : this.getCreator().equals(other.getCreator()))
            && (this.getAuditor() == null ? other.getAuditor() == null : this.getAuditor().equals(other.getAuditor()))
            && (this.getBannerImgHome() == null ? other.getBannerImgHome() == null : this.getBannerImgHome().equals(other.getBannerImgHome()))
            && (this.getBannerImgList() == null ? other.getBannerImgList() == null : this.getBannerImgList().equals(other.getBannerImgList()))
            && (this.getTimeShow() == null ? other.getTimeShow() == null : this.getTimeShow().equals(other.getTimeShow()))
            && (this.getType() == null ? other.getType() == null : this.getType().equals(other.getType()))
            && (this.getCalculateYield() == null ? other.getCalculateYield() == null : this.getCalculateYield().equals(other.getCalculateYield()))
            && (this.getCarriageFree() == null ? other.getCarriageFree() == null : this.getCarriageFree().equals(other.getCarriageFree()))
            && (this.getCustomerType() == null ? other.getCustomerType() == null : this.getCustomerType().equals(other.getCustomerType()))
            && (this.getCornerTab() == null ? other.getCornerTab() == null : this.getCornerTab().equals(other.getCornerTab()))
            && (this.getUpdator() == null ? other.getUpdator() == null : this.getUpdator().equals(other.getUpdator()))
            && (this.getCreatedAt() == null ? other.getCreatedAt() == null : this.getCreatedAt().equals(other.getCreatedAt()))
            && (this.getUpdatedAt() == null ? other.getUpdatedAt() == null : this.getUpdatedAt().equals(other.getUpdatedAt()))
            && (this.getIsDeleted() == null ? other.getIsDeleted() == null : this.getIsDeleted().equals(other.getIsDeleted()))
            && (this.getBuyLimit() == null ? other.getBuyLimit() == null : this.getBuyLimit().equals(other.getBuyLimit()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getpCode() == null) ? 0 : getpCode().hashCode());
        result = prime * result + ((getpName() == null) ? 0 : getpName().hashCode());
        result = prime * result + ((getpType() == null) ? 0 : getpType().hashCode());
        result = prime * result + ((getEffectFrom() == null) ? 0 : getEffectFrom().hashCode());
        result = prime * result + ((getEffectTo() == null) ? 0 : getEffectTo().hashCode());
        result = prime * result + ((getpStatus() == null) ? 0 : getpStatus().hashCode());
        result = prime * result + ((getCreator() == null) ? 0 : getCreator().hashCode());
        result = prime * result + ((getAuditor() == null) ? 0 : getAuditor().hashCode());
        result = prime * result + ((getBannerImgHome() == null) ? 0 : getBannerImgHome().hashCode());
        result = prime * result + ((getBannerImgList() == null) ? 0 : getBannerImgList().hashCode());
        result = prime * result + ((getTimeShow() == null) ? 0 : getTimeShow().hashCode());
        result = prime * result + ((getType() == null) ? 0 : getType().hashCode());
        result = prime * result + ((getCalculateYield() == null) ? 0 : getCalculateYield().hashCode());
        result = prime * result + ((getCarriageFree() == null) ? 0 : getCarriageFree().hashCode());
        result = prime * result + ((getCustomerType() == null) ? 0 : getCustomerType().hashCode());
        result = prime * result + ((getCornerTab() == null) ? 0 : getCornerTab().hashCode());
        result = prime * result + ((getUpdator() == null) ? 0 : getUpdator().hashCode());
        result = prime * result + ((getCreatedAt() == null) ? 0 : getCreatedAt().hashCode());
        result = prime * result + ((getUpdatedAt() == null) ? 0 : getUpdatedAt().hashCode());
        result = prime * result + ((getIsDeleted() == null) ? 0 : getIsDeleted().hashCode());
        result = prime * result + ((getBuyLimit() == null) ? 0 : getBuyLimit().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", pCode=").append(pCode);
        sb.append(", pName=").append(pName);
        sb.append(", pType=").append(pType);
        sb.append(", effectFrom=").append(effectFrom);
        sb.append(", effectTo=").append(effectTo);
        sb.append(", pStatus=").append(pStatus);
        sb.append(", creator=").append(creator);
        sb.append(", auditor=").append(auditor);
        sb.append(", bannerImgHome=").append(bannerImgHome);
        sb.append(", bannerImgList=").append(bannerImgList);
        sb.append(", timeShow=").append(timeShow);
        sb.append(", type=").append(type);
        sb.append(", calculateYield=").append(calculateYield);
        sb.append(", carriageFree=").append(carriageFree);
        sb.append(", customerType=").append(customerType);
        sb.append(", cornerTab=").append(cornerTab);
        sb.append(", updator=").append(updator);
        sb.append(", createdAt=").append(createdAt);
        sb.append(", updatedAt=").append(updatedAt);
        sb.append(", isDeleted=").append(isDeleted);
        sb.append(", buyLimit=").append(buyLimit);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}