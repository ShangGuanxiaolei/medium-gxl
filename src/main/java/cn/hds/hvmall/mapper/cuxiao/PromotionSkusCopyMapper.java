package cn.hds.hvmall.mapper.cuxiao;

import cn.hds.hvmall.mapper.cuxiao.PromotionSkus;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PromotionSkusCopyMapper {
    int deleteByPrimaryKey(Long id);

    int insert(PromotionSkus record);

    int insertSelective(PromotionSkus record);

    PromotionSkus selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(PromotionSkus record);

    int updateByPrimaryKey(PromotionSkus record);

    List<PromotionSkus> queryProductById(@Param("productId")String ProductId);

}