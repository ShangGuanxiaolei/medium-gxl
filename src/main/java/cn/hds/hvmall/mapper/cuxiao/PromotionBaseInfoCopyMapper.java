package cn.hds.hvmall.mapper.cuxiao;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Date;
import java.util.Map;

import cn.hds.hvmall.mapper.cuxiao.PromotionBaseInfo;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.data.domain.Pageable;

@Mapper
public interface PromotionBaseInfoCopyMapper {
    int deleteByPrimaryKey(Long id);

    int insert(PromotionBaseInfo record);

    int insertSelective(PromotionBaseInfo record);

    PromotionBaseInfo selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(PromotionBaseInfo record);

     int updateByPrimaryKey(PromotionBaseInfo record);

     List<PromotionBaseInfo> findByPCode(@Param("pCode")String pCode);

     List<BaseInfoVo> findByPName(@Param("pName")String pName, @Param("type")String type, @Param("page")Pageable page);

     int  getActivityCount(@Param("pName")String pName, @Param("type")String type);


     List<Base> queryActivityByPcode(@Param("pCode") String pCode);


     List<Long> findIdByPCode(@Param("pCode")String pCode);

     Base queryPromotionBasePcode(@Param("pCode")String pCode);


    List<Base> queryDeleteActivityByPcode(@Param("pCode")String pCode);

    PromotionBaseInfo queryActivityStatusByPcode(@Param("pCode")String pCode);

}