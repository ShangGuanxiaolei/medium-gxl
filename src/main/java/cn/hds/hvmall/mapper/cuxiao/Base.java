package cn.hds.hvmall.mapper.cuxiao;

import cn.hds.hvmall.entity.freshman.ActivityProduct;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: chp
 * Date: 2019/3/27
 * Time: 16:57
 * Description:
 */
public class Base {
    private String   pCode;
    private String  pName;
    private String  effectFrom;
    private String  effectTo;
    private String  customerType;
    private String  cornerTab;
    private Boolean type;
    private List<ActivityProduct> activityProductList;

    public Boolean getType() {
        return type;
    }

    public void setType(Boolean type) {
        this.type = type;
    }

    public String getpCode() {
        return pCode;
    }

    public void setpCode(String pCode) {
        this.pCode = pCode;
    }

    public List<ActivityProduct> getActivityProductList() {
        return activityProductList;
    }

    public void setActivityProductList(List<ActivityProduct> activityProductList) {
        this.activityProductList = activityProductList;
    }

    public String getpName() {
        return pName;
    }

    public void setpName(String pName) {
        this.pName = pName;
    }

    public String getEffectFrom() {
        return effectFrom;
    }

    public void setEffectFrom(String effectFrom) {
        this.effectFrom = effectFrom;
    }

    public String getEffectTo() {
        return effectTo;
    }

    public void setEffectTo(String effectTo) {
        this.effectTo = effectTo;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public String getCornerTab() {
        return cornerTab;
    }

    public void setCornerTab(String cornerTab) {
        this.cornerTab = cornerTab;
    }
}
