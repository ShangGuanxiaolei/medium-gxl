package cn.hds.hvmall.mapper.cuxiao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface XquarkSkuCopyMapper {
    int deleteByPrimaryKey(Long id);

    int insert(XquarkSku record);

    int insertSelective(XquarkSku record);

    XquarkSku selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(XquarkSku record);

    int updateByPrimaryKey(XquarkSku record);

    List<XquarkSku> findBySkuCode(@Param("skuCode")String skuCode);

    List<String> findSkuCodeBySourceSkuCode(@Param("sourceSkuCode")String sourceSkuCode);

    List<Integer> findAmountBySkuCode(@Param("skuCode")String skuCode);

    List<Integer> findAmountBYSourceSkuCode(@Param("sourceSkuCode")String sourceSkuCode);

    List<Integer> findAmountBYSourceSkuCodeS(@Param("sourceSkuCode")String sourceSkuCode);

    int updateAmountBySkuCode(@Param("updatedAmount")Integer updatedAmount,@Param("skuCode")String skuCode);


    int updateSkuCodeById(@Param("code") String code,@Param("id") Long id);

    List<Long> findProductIdBySkuCode(@Param("skuCode")String skuCode);





}