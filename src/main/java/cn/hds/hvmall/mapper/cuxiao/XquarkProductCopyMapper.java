package cn.hds.hvmall.mapper.cuxiao;
import org.apache.ibatis.annotations.Param;

import cn.hds.hvmall.mapper.cuxiao.XquarkProduct;
import cn.hds.hvmall.mapper.cuxiao.XquarkProductWithBLOBs;

import java.util.List;

public interface XquarkProductCopyMapper {
    int deleteByPrimaryKey(Long id);

    int insert(XquarkProductWithBLOBs record);

    int insertSelective(XquarkProductWithBLOBs record);

    XquarkProductWithBLOBs selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(XquarkProductWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(XquarkProductWithBLOBs record);

    int updateByPrimaryKey(XquarkProduct record);

    Long findSourceIdById(@Param("id")Long id);

    Boolean getPid(Long  id);

    XquarkProductWithBLOBs queryprodyuctById(Long id);

    int updateCodeById(@Param("code")   String code,@Param("id") Long id);


}