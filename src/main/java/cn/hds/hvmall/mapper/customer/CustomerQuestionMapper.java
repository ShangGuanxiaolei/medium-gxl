package cn.hds.hvmall.mapper.customer;

import cn.hds.hvmall.entity.customer.CustomerQuestion;
import cn.hds.hvmall.entity.customer.QuestionTypeVo;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface CustomerQuestionMapper {

    /**
     * 查询问题类型
     * @return
     */
    List<QuestionTypeVo> selectQueType();


    /**
     * 查询当前类型下的问题详情
     * @param typeId
     * @param title
     * @return
     */
    List<CustomerQuestion> selectQuestionAll(@Param("typeId") String typeId,@Param("title")String title);

    /**
     * 逻辑删除客服问题
     * @param id
     * @return
     */
    int deleteQuestion(@Param("id")int id);

    /**
     * 把当前的sort_num修改为0
     * @param NowSortNum
     * @param typeId
     * @return
     */
    @Update("update xquark_kefu_question_detail set sort_num=0,updated_at=NOW() where id=#{id} and type_id=#{typeId} and is_deleted=1 ")
    int updateZeroBySortNum(@Param("id")String id ,@Param("typeId")String typeId);


    /**
     * 把下一个sort_num修改为当前的sort_num
     * @param NowSortNum
     * @param NextSortNum
     * @param typeId
     * @return
     */
    @Update("update xquark_kefu_question_detail set sort_num=#{NowSortNum},updated_at=NOW() where id=#{NextId} and type_id=#{typeId} and is_deleted=1")
    int updateNowSortByNext(@Param("NowSortNum")String NowSortNum ,@Param("NextId")int NextId ,@Param("typeId")String typeId);

    /**
     * 把sort_num为0的修改为下一个sort_num
     * @param NextSortNum
     * @param typeId
     * @return
     */
    @Update("update xquark_kefu_question_detail set sort_num=#{NextSortNum},updated_at=NOW() where sort_num=0 and type_id=#{typeId} and is_deleted=1")
    int updatNextByZero(@Param("NextSortNum")int NextSortNum ,@Param("typeId")String typeId);

    /**
     * 根据type查询总数
     * @param typeId
     * @return
     */
    @Select("select COUNT(*) count from xquark_kefu_question_detail where type_id=#{typeId} and is_deleted=1")
    int selectCount(@Param("typeId")String typeId);

    /**
     * 根据type查询id
     * @param typeId
     * @return
     */
    @Select("select id from xquark_kefu_question_detail where type_id=#{typeId} and is_deleted=1 ORDER BY sort_num")
    List<Integer> selectId(@Param("typeId")String typeId);

    /**
     * 根据type查询sort_num
     * @param typeId
     * @return
     */
    @Select("select sort_num from xquark_kefu_question_detail where type_id=#{typeId} and is_deleted=1 ORDER BY sort_num")
    List<Integer> selectSortNum(@Param("typeId")String typeId);


    /**
     * 根据type查询sort_num最大的数字
     * @param typeId
     * @return
     */
    @Select("select max(sort_num) from xquark_kefu_question_detail where type_id=#{typeId} and is_deleted=1")
    String selectMaxSortNum(@Param("typeId")String typeId);

    /**
     * 添加问题内容
     * @param typeId
     * @param title
     * @param content
     * @param sortNum
     * @return
     */
    @Insert("insert into xquark_kefu_question_detail (type_id,title,content,sort_num,created_at,is_deleted) VALUES (#{typeId},#{title},#{content},#{sortNum},NOW(),1)")
    int insertQuestion(@Param("typeId") String typeId,@Param("title")String title,@Param("content")String content,@Param("sortNum")int sortNum);


    /**
     * 修改客服问题内容
     * @param typeId
     * @param title
     * @param content
     * @param id
     * @return
     */
    @Update("UPDATE xquark_kefu_question_detail set type_id=#{typeId},sort_num=#{sortNum},title=#{title},content=#{content},updated_at=NOW() where id=#{id}")
    int updateQuestion(@Param("typeId")String typeId,@Param("sortNum")int sortNum,@Param("title")String title,@Param("content")String content,@Param("id")String id);

    /**
     * 修改客服问题内容2
     * @param typeId
     * @param title
     * @param content
     * @param id
     * @return
     */
    @Update("UPDATE xquark_kefu_question_detail set type_id=#{typeId},title=#{title},content=#{content},updated_at=NOW() where id=#{id}")
    int updateQuestion2(@Param("typeId")String typeId,@Param("title")String title,@Param("content")String content,@Param("id")String id);

    /**
     * 根据问题详情id查询问题内容
     * @param id
     * @return
     */
    String selectContentById(@Param("id")String id);


    @Select("select type_id from xquark_kefu_question_detail where id=#{id}")
    String typeIdById(@Param("id")String id);


    /**
     * 查询客服问题类型
     * @param type
     * @return
     */
    List<QuestionTypeVo> selectQueTypeDetail(@Param("type")String type);

    /**
     * 逻辑删除问题类型
     * @param id
     * @return
     */
    int deleteQuestionType(@Param("id")int id);

    /**
     * 编辑问题类型信息
     * @param type
     * @param icon
     * @param id
     * @return
     */
    int updateQuestionType(@Param("type")String type,@Param("icon")String icon,@Param("id")String id);


    /**
     * 添加问题类型信息
     * @param type
     * @param icon
     * @param sort
     * @return
     */
    int insertQuestionType(@Param("type")String type,@Param("icon")String icon,@Param("sort")int sort);

    /**
     * 查询sort最大的数字
     * @return
     */
    @Select("select max(sort) from xquark_kefu_question_type where is_deleted=1")
    int selectMaxSort();



    /**
     * 把当前的sort修改为0
     * @return
     */
    @Update("update xquark_kefu_question_type set sort=0,updated_at=NOW() where id=#{id} and is_deleted=1")
    int updateZeroBySort(@Param("id")String id);


    /**
     * 把下一个sort修改为当前的sort
     * @return
     */
    @Update("update xquark_kefu_question_type set sort=#{NowSort},updated_at=NOW() where id=#{NextId} and is_deleted=1")
    int updateNowSortByNextId(@Param("NowSort")String NowSort ,@Param("NextId")int NextId );

    /**
     * 把sort为0的修改为下一个sort
     * @return
     */
    @Update("update xquark_kefu_question_type set sort=#{NextSort},updated_at=NOW() where sort=0  and is_deleted=1")
    int updatNextSortByZero(@Param("NextSort")int NextSort);



    /**
     * 查询sort
     * @return
     */
    @Select("select sort from xquark_kefu_question_type where is_deleted=1 ORDER BY sort")
    List<Integer> selectSort();


    /**
     * 查询所有类型id
     * @return
     */
    @Select("select id from xquark_kefu_question_type where  is_deleted=1 ORDER BY sort")
    List<Integer> selectTypeId();

}
