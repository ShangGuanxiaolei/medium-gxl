package cn.hds.hvmall.mapper.promotionActivity;

import cn.hds.hvmall.entity.freshman.ActivityProduct;
import cn.hds.hvmall.entity.freshman.Banner;
import cn.hds.hvmall.entity.freshman.BannerInfo;
import cn.hds.hvmall.entity.freshman.ProductType;
import cn.hds.hvmall.mapper.cuxiao.PromotionBaseInfo;
import cn.hds.hvmall.mapper.cuxiao.PromotionSkus;
import cn.hds.hvmall.pojo.grandsale.PromotionVO;
import cn.hds.hvmall.pojo.promotion.PromotionMessage;
import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: WangXiao
 * Date: 2019/3/25
 * Time: 17:09
 * Description:
 */
@Repository
public interface PromotionActivityMapper {
    /**
     * 根据id禁用、启用Banner
     * @param status
     * @param id
     * @return
     */
    boolean updateStatus(@Param("status") int status, @Param("id") String id);

    /**
     * 根据id查询状态
     * @param id
     * @return
     */
    int selectStatusById(@Param("id") String id);

    /**
     * 添加Banner
     * @param imgUrl
     * @param startAt
     * @param endAt
     * @param redirectType
     * @param redirectUrl
     * @return
     */
    boolean insertBanner(@Param("imgUrl")String imgUrl,@Param("startAt") String startAt,@Param("endAt") String endAt,@Param("redirectType")int redirectType,@Param("redirectUrl")String redirectUrl,@Param("status")int status);

    /**
     * 根据条件查询商品信息
     * @param productId
     * @param productName
     * @param typeId
     * @param pageNo
     * @param pageSize
     * @return
     */
    List<ActivityProduct> selectActivityProduct(@Param("productId")Integer productId,@Param("skuCode")String skuCode,@Param("status")String status, @Param("productName")String productName, @Param("typeId")Integer typeId, @Param("pageNo")Integer pageNo, @Param("pageSize")Integer pageSize);

    /**
     * 根据条件查询商品总数
     * @param productId
     * @param productName
     * @param typeId
     * @return
     */
    int selectProductInfoCount(@Param("productId")Integer productId,@Param("skuCode")String skuCode,@Param("productName")String productName,@Param("status")String status,@Param("typeId")Integer typeId);


    /**
     * 查询父级商品类型
     * @return
     */
    List<ProductType> selectParentType();

    /**
     * 查询子级商品类型
     * @return
     */
    List<ProductType> selectDetailType(@Param("parentId")Integer parentId);

    /**
     * 查询出品牌
     * @return
     */
    List<ProductType> selectBrand();

    /**
     * 查询Banner信息
     * @return
     */
    List<BannerInfo> selectBannerInfo();

    /**
     * 修改Banner信息
     * @param bannerInfo
     * @return
     */
    boolean updateBannerInfo(BannerInfo bannerInfo);

    Integer deleteActivity(@Param("pCode") String pCode);

    Integer stopActivity(@Param("pCode") String pCode);

    PromotionBaseInfo queryActivityByPcode(@Param("pCode")String pCode);

    List<ActivityProduct> queryActivityProductByPcode(@Param("pCode")String pCode);

    Integer insertPromotionBase(PromotionBaseInfo promotionBaseInfo);

    Integer updatePromotionBaseBypCode(PromotionBaseInfo promotionBaseInfo);

    Integer deleteXquarkSkuByPcode(@Param("pCode")String pCode);

    Integer deleteXquarkProductByPcode(@Param("pCode")String pCode);

    Integer deletePromotionSkus(@Param("pCode")String pCode);

    Integer deletePromotionExclusive(@Param("pCode")String pCode);

    List<PromotionBaseInfo> releaseProduct();

    Integer releaseDeleteProduct(@Param("pCode") String pCode);

    Integer DeletePromotionSku(@Param("pCode") String pCode);

    Integer queryProductByPcode(@Param("pCode") String pCode);

    List<PromotionVO>  selectEffectSalePromotion();

    List<PromotionVO>  selectEffectSalePromotion2();

    PromotionVO selectByPCode(String pCode);

    int endProductSales(@Param("pCode") String pCode);

    Integer deleteSku(@Param("skuCode") String skuCode,@Param("productId") Long productId);

    List<PromotionSkus> getPromotionMessages(@Param("productId") Integer productId);

    PromotionMessage getProductMessage(@Param("skuCode") String skuCode);

}