package cn.hds.hvmall.mapper.order;

import cn.hds.hvmall.enums.CareerLevelType;
import cn.hds.hvmall.enums.OrderStatus;
import cn.hds.hvmall.pojo.analyse.Order;
import cn.hds.hvmall.service.order.TotalDTO;
import cn.hds.hvmall.type.PromotionType;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 订单数据分析
 *
 * @author wangxinhua
 * @date 2019-05-12
 * @since 1.0
 */
@Repository
public interface OrderAnalyseMapper {

    /**
     * 查询某段时间某活动类型的订单总金额
     * 不包含分会场及复制属性
     *
     * @param start 开始时间
     * @param end   结束时间
     * @param types 活动类型, 支持多个
     * @return 总金额
     */
    BigDecimal selectSalesTotalFee(@Param("start") LocalDateTime start,
                                   @Param("end") LocalDateTime end,
                                   @Param("types") PromotionType... types);

    /**
     * 查询某段时间某活动类型的订单总金额
     * 包含分会场及复制属性
     *
     * @param start 开始时间
     * @param end   结束时间
     * @param types 活动类型, 支持多个
     * @return 总金额
     */
    BigDecimal selectSalesTotalFeeAll(@Param("start") LocalDateTime start,
                                   @Param("end") LocalDateTime end,
                                   @Param("types") PromotionType... types);

    /**
     * 查询某段时间的总下单量
     */
    List<Order> ListPromotionOrder(@Param("start") LocalDateTime start,
                                   @Param("end") LocalDateTime end,
                                   @Param("status") OrderStatus status, @Param("types") PromotionType... types);

    /**
     * 查询所有大型促销包括的订单数据
     * 会包含分会场及大促活动复制sku的订单
     */
    List<Order> listPromotionOrderAll(@Param("start") LocalDateTime start,
                                      @Param("end") LocalDateTime end,
                                      @Param("types") PromotionType... types);

    /**
     * 查询某段时间的新用户总数
     *
     * @param start 开始时间
     * @param end   结束时间
     * @return 总人数
     */
    Long selectNewUsersTotal(@Param("start") LocalDateTime start, @Param("end") LocalDateTime end);

    Long selectNewUsersTotalByDay(@Param("day") String day);

    List<TotalDTO> selectByIdentitys(@Param("start") LocalDateTime start,
                               @Param("end") LocalDateTime end,
                               @Param("orderType") String orderType,
                               @Param("types") CareerLevelType... types);

    Long countReserveByDate(@Param("date") String date);

    Long countPieceByDate(@Param("date") String date,
                          @Param("isGroupHead") int isGroupHead);

}
