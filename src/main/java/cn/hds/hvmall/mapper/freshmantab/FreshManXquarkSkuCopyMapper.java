package cn.hds.hvmall.mapper.freshmantab;

import cn.hds.hvmall.pojo.freshmantab.XquarkSku;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FreshManXquarkSkuCopyMapper {
    int deleteByPrimaryKey(Long id);

    int insert(XquarkSku record);

    int insertSelective(XquarkSku record);

    XquarkSku selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(XquarkSku record);

    int updateByPrimaryKey(XquarkSku record);

    List<XquarkSku> findBySkuCode(@Param("skuCode") String skuCode);

    int updateSkuCodeById(@Param("code") String code, @Param("id") Long id);

    Integer querySkuCountByProductIdSkuCode(@Param("Id")Long Id,@Param("productId") Long productId, @Param("skuCode") String  skuCode);

    Integer updateByProductIdSkuCode(XquarkSku xquarkSku);

    List<Integer> findAmountBySkuCode(@Param("skuCode")String skuCode);

    List<Integer> findAmountBYSourceSkuCode(@Param("sourceSkuCode")String sourceSkuCode);

    Integer updateStockByskuCodeAndId(@Param("id") Integer  id,@Param("stock") Integer  stock, @Param("skuId") Integer skuId);

    Integer getSkuAmount(@Param("id") Integer  id, @Param("skuId") Integer skuId);

    XquarkSku selectSkuIdByProductId(@Param("productId")Long ProductId);
}