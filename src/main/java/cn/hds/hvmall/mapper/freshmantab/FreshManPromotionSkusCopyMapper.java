package cn.hds.hvmall.mapper.freshmantab;

import cn.hds.hvmall.pojo.freshmantab.FreshmanPromotionSku;
import org.apache.ibatis.annotations.Param;


public interface FreshManPromotionSkusCopyMapper {
    int deleteByPrimaryKey(Long id);

    int insert(FreshmanPromotionSku record);

    int insertSelective(FreshmanPromotionSku record);

    FreshmanPromotionSku selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(FreshmanPromotionSku record);

    int updateByPrimaryKey(FreshmanPromotionSku record);

    Integer queryPromotionSkuByProductIdSku(@Param("Id") String Id ,@Param("productId")String productId,@Param("skuCode")String skuCode);

    int updatePromotionByProductSku(FreshmanPromotionSku record);

}