package cn.hds.hvmall.mapper.freshmantab;

import cn.hds.hvmall.pojo.freshmantab.*;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: WangXiao
 * Date: 2019/3/5
 * Time: 15:27
 * Description:
 */
@Repository
public interface FreshmanMapper {

    int updateFreshConfig(@Param("value") String value, @Param("id") Integer id);

    int updateFreshAreattab(@Param("name") String name, @Param("tabId") Integer tabId);

    int  updateFreshProduct(FreshmanProduct product);

    List<FreshmanAreaProduct> queryTabProduct();

    int selectAmountBySkuId(@Param("skuId")String skuId);

    List<Map<String,String>> queryTitle();

    int deleteFreshProduct(@Param("productId") String productId,@Param("tabId") Long tabId);

    int deletePromotionExclusive(@Param("productId") String productId);

    int AddTabProduct(FreshmanProduct freshmanProduct);

    int insertFreshProduct(FreshmanProduct freshmanProduct);

    int insertPromotionExclusive(FreshmanProduct freshmanProduct);

    List<FreshmanProduct> queryProductByid(@Param("productId")String ProductId);

    /**
     * 根据skucode查询库存
     * @param skuCode
     * @return
     */
    int selectSkuCodeAmount(@Param("skuCode")String skuCode, @Param("skuId") Integer skuId);

    /**
     * 根据SourceSkucode查询库存
     * @param sourceSkuCode
     * @return
     */
    int selectSourceSkuCodeAmount(@Param("sourceSkuCode")String sourceSkuCode);

    /**
     * 根据复制出来的skuCode查询源skuCode
     * @param skuCode
     * @return
     */
    String selectSourceSkuCodeBySkuCode(@Param("skuCode")String skuCode, @Param("skuId") Integer skuId);


    Integer selectSourceSkuIdBySkuCode(@Param("firstSkuCode") String firstSkucode);

    /**
     * 修改库存
     * @param skuCode
     * @return
     */
    int updateAmount(@Param("count")int count,@Param("skuCode")String skuCode);

    int updateFreshProductByCode(FreshmanProduct freshmanProduct);

    int deletePromotionExculByProductId(@Param("productId")String ProductId);

    int updateProductAmount(@Param("productId")Long ProductId);

    XquarkSku selectProductIdBySkuCode(@Param("skuCode") String skuCode, @Param("skuId") Integer skuId);

    Boolean isCombineProduct(@Param("productId")Long ProductId);

    int insertXquarkSkuCombine(XquarkSkuCombine xquarkSkuCombine);

    XquarkSkuCombine selectCombineIdByProductId(@Param("productId")Long ProductId);

    List<XquarkSkuCombineExtra> selectCombineExtraByMasterId(@Param("masterId")Long masterId);

    Integer insertCombineExtra(XquarkSkuCombineExtra xquarkSkuCombineExtra);

    Integer selectStadiumSourceSkuId (@Param("skuId") Integer skuId);
}