package cn.hds.hvmall.mapper.freshmantab;
import cn.hds.hvmall.pojo.freshmantab.PromotionExclusive;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import org.springframework.stereotype.Repository;

@Repository
public interface FreshManPromotionExclusiveCopyMapper {
    int deleteByPrimaryKey(Long id);

    int insert(PromotionExclusive record);

    int insertSelective(PromotionExclusive record);

    PromotionExclusive selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(PromotionExclusive record);

    int updateByPrimaryKey(PromotionExclusive record);

    List<PromotionExclusive> findByProductIdAndPCode(@Param("productId") Long productId, @Param("pCode") String pCode);

    Integer queryProductByid(@Param("productId") Long productId,@Param("Id") Long Id);

}