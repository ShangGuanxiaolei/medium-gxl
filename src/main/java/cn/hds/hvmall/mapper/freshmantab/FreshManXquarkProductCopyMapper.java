package cn.hds.hvmall.mapper.freshmantab;

import cn.hds.hvmall.pojo.freshmantab.XquarkProduct;
import cn.hds.hvmall.pojo.freshmantab.XquarkProductWithBLOBs;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface FreshManXquarkProductCopyMapper {
    int deleteByPrimaryKey(Long id);

    int insert(XquarkProductWithBLOBs record);

    int insertSelective(XquarkProductWithBLOBs record);

    XquarkProductWithBLOBs selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(XquarkProductWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(XquarkProductWithBLOBs record);

    int updateByPrimaryKey(XquarkProduct record);

    int updateCodeById(@Param("code") String code, @Param("id") Long id);

    int updateProductINSTOCK(@Param("productId")String productId);

    int updateProductONSALE(@Param("productId")Long  productId);
}