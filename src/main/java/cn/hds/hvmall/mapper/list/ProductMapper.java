package cn.hds.hvmall.mapper.list;

import cn.hds.hvmall.entity.list.*;
import cn.hds.hvmall.entity.product.Product;
import cn.hds.hvmall.entity.promotion.PromotionProduct;
import cn.hds.hvmall.entity.recommend.SelectCondition;
import cn.hds.hvmall.entity.reserve.ReserveProductForm;
import cn.hds.hvmall.pojo.list.Category;
import cn.hds.hvmall.pojo.list.InfoSimple;
import cn.hds.hvmall.pojo.list.SearchTerm;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * @Author: zzl
 * @Date: 2018/9/11 14:45
 * @Version
 */
@Repository
public interface ProductMapper {


    /**
     * 保存商品
     * @param record 商品实体
     * @return 保存结构 0 - 失败， 1 - 成功
     */
    int insert(Product record);

    /**
     * 查询商品
     * @param id 商品id
     * @return 商品对象
     */
    Product selectByPrimaryKey(Long id);

    /**
     * 根据id更新商品数据
     * @param record 商品对象
     * @return 更新结构 0 - 失败， 1 - 成功
     */
    int updateByPrimaryKeySelective(Product record);

    /**
     * 根据条件查询商品
     *
     * @return
     */
    List<ProductSearchSimple> searchProduct(@Param("info") InfoSimple info, @Param("pageNumber") int pageNumber,
                                            @Param("pageSize") int pageSize);

    /**
     * 根据条件查询商品sku
     * @param searchTerm 查询条件
     * @param page 分页数据
     * @return 商品列表
     */
    List<ProductSkuSimple> searchProductSku(@Param("searchTerm") SearchTerm searchTerm, @Param("page") Pageable page);

    /**
     * 根据条件查询商品sku数量
     * @param searchTerm 查询条件
     * @return 商品列表
     */
    int countProductSku(@Param("searchTerm") SearchTerm searchTerm);

    /**
     * 分页查询分会场商品数据
     * @param type 会场类型
     * @param categoryId 分类id
     * @param page 分页
     * @return 分会场商品列表
     */
    List<ProductSkuSimpleVO> searchPromotionProduct(@Param("type") String type,
                                                  @Param("categoryId") String categoryId,
                                                  @Param("page") Pageable page);

    /**
     * 统计数量分页查询分会场商品数据
     * @param type 分会场类型
     * @param categoryId 分类id
     * @return 分会场商品列表
     */
    int countPromotionProduct(@Param("type") String type, @Param("categoryId") String categoryId);

    /**
     * 根据条件查询规格
     *
     * @return
     */
    List<SkuList> searchSku(int productId);

    /**
     * 根据条件查询商品总数
     *
     * @param info
     * @return
     */
    int countProduct(@Param("info") InfoSimple info);

    /**
     * 查询供应商名称列表
     *
     * @return
     */
    List<Supplier> selectSupplier();


    /**
     * 查询商品一级分类列表
     *
     * @return
     */
    List<Category> selectCategory();

    /**
     * 查询商品二级分类列表
     *
     * @return
     */
    List<Category> selectTwoCategory(String parentId);

    /**
     * 查询商品品牌列表
     *
     * @return
     */
    List<Brand> selectBrand();

    /**
     * 查询活动商品信息
     *
     * @param page
     * @return
     */
    List<Map> getPromotionProduct(PromotionProduct page);

    /**
     * 根据特定的条件类查询
     */
    List<ProductSearchResult> searchProductResult(@Param("productInfo") SelectCondition selectCondition, @Param("pageNumber") int pageNumber, @Param("pageSize") int pageSize);

    //查询没有删除的推荐商品
    int selectCount(@Param("productInfo") SelectCondition productInfo);

	//根据条件查询商品
	List<SearchProductResult> selectReserveProduct(@Param("reserveProductForm")ReserveProductForm reserveProductForm,@Param("rowIndex")int rowIndex,@Param("pageSize")int pageSize);
	//根据条件查询商品的数量
	Integer selectReserveProductCount(@Param("reserveProductForm")ReserveProductForm reserveProductForm);

    /**
     * 根据productId查询基本信息
     */
    SearchProductInfoResult getProductInfo(@Param("productId")Long productId);

    boolean exists(Long id);

    /**
     * 查询商品名称
     */
    String selectProductNameById(@Param("productId")Long productId);
}
