package cn.hds.hvmall.mapper.grandsale;

import cn.hds.hvmall.entity.grandsale.GrandSaleStadium;
import cn.hds.hvmall.type.StadiumType;
import org.apache.ibatis.annotations.Param;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public interface GrandSaleStadiumMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(GrandSaleStadium record);

    int insertSelective(GrandSaleStadium record);

    GrandSaleStadium selectOne(Integer id);

    int selectByType(String type);

    List<GrandSaleStadium> selectAllByGrandSaleId(Integer grandSaleId);

    int updateByPrimaryKeySelective(GrandSaleStadium record);

    int updateByPrimaryKey(GrandSaleStadium record);

    int updateSortById(@Param("id") Integer id, @Param("sort") Integer sort);

    boolean checkStadiumIsCreate(@Param("type") String type,@Param("grandId") Integer grandId);

    Integer updateStadiumInstock(@Param("type") String type, @Param("categoryId") String categoryId);

    List<String> getStadiumProductId(@Param("type") String type, @Param("categoryId") String categoryId);

    Integer updateStadiumProductId(@Param("id") String id);
}