package cn.hds.hvmall.mapper.grandsale;

import cn.hds.hvmall.entity.grandsale.GrandSaleStadiumType;

public interface GrandSaleStadiumTypeMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(GrandSaleStadiumType record);

    int insertSelective(GrandSaleStadiumType record);

    GrandSaleStadiumType selectByPrimaryKey(Integer id);

    GrandSaleStadiumType selectByType(String type);

    int updateByPrimaryKeySelective(GrandSaleStadiumType record);

    int updateByPrimaryKey(GrandSaleStadiumType record);
}