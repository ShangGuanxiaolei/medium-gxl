package cn.hds.hvmall.mapper.grandsale;

import cn.hds.hvmall.entity.grandsale.GrandSaleKv;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface GrandSaleKvMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(GrandSaleKv record);

    int insertSelective(GrandSaleKv record);

    GrandSaleKv selectByPrimaryKey(Integer id);

    List<GrandSaleKv> selectAllByGrandSaleId(@Param("grandSaleId") Integer grandSaleId,
                                             @Param("page") Pageable page);

    int countByGrandSaleId(@Param("grandSaleId") Integer grandSaleId);

    int updateByPrimaryKeySelective(GrandSaleKv record);

    int updateByPrimaryKey(GrandSaleKv record);

    int logicalDelete(Integer id);
}