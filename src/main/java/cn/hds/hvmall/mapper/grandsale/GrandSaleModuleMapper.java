package cn.hds.hvmall.mapper.grandsale;

import cn.hds.hvmall.entity.grandsale.GrandSaleModule;
import cn.hds.hvmall.pojo.grandsale.PromotionVO;
import cn.hds.hvmall.type.PromotionType;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GrandSaleModuleMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(GrandSaleModule record);

    int insertSelective(GrandSaleModule record);

    GrandSaleModule selectByPrimaryKey(Integer id);

    List<GrandSaleModule> selectAll(Integer grandSaleId);

    GrandSaleModule selectOneByGrandSaleId(Integer grandSaleId);

    List<PromotionVO> selectAllEffective();

    List<PromotionVO> selectAllEffective2();

    int updateByPrimaryKeySelective(GrandSaleModule record);

    int updateByPrimaryKey(GrandSaleModule record);

    int updateStatusById(@Param("promotionId") String promotionId, @Param("status") Integer status);

    int updateSortByType(@Param("promotionType") PromotionType promotionType, @Param("sort") Integer sort);

    int updateFlashSaleSortByPromotionId(@Param("promotionId") String promotionId, @Param("sortIng") Integer sortIng);

    int bindGrand(@Param("promotionType") PromotionType promotionType, @Param("promotionId") Long promotionId);
}