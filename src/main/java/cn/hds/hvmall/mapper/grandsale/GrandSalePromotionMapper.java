package cn.hds.hvmall.mapper.grandsale;

import cn.hds.hvmall.entity.grandsale.GrandSalePromotion;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface GrandSalePromotionMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(GrandSalePromotion record);

    int insertSelective(GrandSalePromotion record);

    GrandSalePromotion selectByPrimaryKey(Integer id);

    List<GrandSalePromotion> selectEffectivePromotion();

    GrandSalePromotion selectStartedPromotion();

    List<GrandSalePromotion> selectAll(@Param("page") Pageable page);

    int updateByPrimaryKeySelective(GrandSalePromotion record);

    int updateByPrimaryKey(GrandSalePromotion record);

    int updateStatusById(Integer id);

    int deleteGrandSaleById(Integer id);

    GrandSalePromotion selectGrandSale();

    boolean existsGrandSalePromotionById(@Param("grandId") Integer grandId);

    boolean existsActivityGrand();

}