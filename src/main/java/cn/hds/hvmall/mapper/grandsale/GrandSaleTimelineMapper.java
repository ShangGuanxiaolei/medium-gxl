package cn.hds.hvmall.mapper.grandsale;

import cn.hds.hvmall.entity.grandsale.GrandSaleTimeline;
import cn.hds.hvmall.entity.grandsale.GrandSaleTimelineConfig;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import java.util.Date;
import java.util.List;

public interface GrandSaleTimelineMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(GrandSaleTimeline record);

    int insertConfig(@Param("grandSaleId") Integer grandSaleId, @Param("beginTime")Date beginTime,@Param("endTime") Date endTime,@Param("isShowTimeline")Integer isShowTimeline,@Param("timelineImg")String timelineImg);

    int insertSelective(GrandSaleTimeline record);

    GrandSaleTimeline selectByPrimaryKey(Integer id);

    GrandSaleTimelineConfig selectTimelinConfigByGrandSaleId(@Param("grandSaleId") Integer grandSaleId);

    List<GrandSaleTimeline> selectAllByGrandSaleId(@Param("grandSaleId") Integer grandSaleId,
                                                   @Param("page") Pageable page);

    int updateByPrimaryKeySelective(GrandSaleTimeline record);

    int updateByPrimaryKey(GrandSaleTimeline record);

    int logicalDelete(Integer id);

    int updateTimelineConfig(@Param("grandSaleId") Integer grandSaleId, @Param("beginTime")Date beginTime,@Param("endTime") Date endTime,@Param("isShowTimeline")Integer isShowTimeline,@Param("timelineImg")String timelineImg);
}