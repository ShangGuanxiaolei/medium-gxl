package cn.hds.hvmall.mapper.grandsale;


import cn.hds.hvmall.entity.grandsale.GrandSaleShare;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import java.util.Date;
import java.util.List;

public interface GrandSaleShareMapper {
    int deleteByPrimaryKey(Long id);

    int insert(GrandSaleShare record);

    int insertSelective(GrandSaleShare record);

    GrandSaleShare selectByPrimaryKey(Long id);

    GrandSaleShare selectStadiumByType(String type);

    boolean selectMainByDate(@Param("date") Date date);

    int updateByPrimaryKeySelective(GrandSaleShare record);

    int updateByPrimaryKey(GrandSaleShare record);

    int updateByPrimaryKey(@Param("page") Pageable page);

    List<GrandSaleShare> selectAllMainShare(@Param("page") Pageable page);

    Long countAllMainShare();

    Date selectLastMainShare();
}