package cn.hds.hvmall.mapper.invite;

import cn.hds.hvmall.entity.promotion.PromotionInviteCode;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface PromotionInviteCodeMapper {

//	void insertInviteCodeBypCode(@Param("code") String code, @Param("pCode") String pCode);

    int insertAll(List<Map<String,String>> configs);

    int getPromotionInviteCodeNum(@Param("pCode") String pCode);

    /**
     * 根据参数查询邀请码信息
     * @param pCode
     * @param cpid
     * @param code
     * @param pageNo
     * @param pageSize
     * @return
     */
    List<PromotionInviteCode> selectAllBypCode(@Param("pCode")String pCode,@Param("cpid") String cpid,@Param("code")String code,@Param("pageNo") int pageNo,@Param("pageSize") int pageSize);
    int selectAllBypCodeCount(@Param("pCode")String pCode,@Param("cpid") String cpid,@Param("code")String code);
}
