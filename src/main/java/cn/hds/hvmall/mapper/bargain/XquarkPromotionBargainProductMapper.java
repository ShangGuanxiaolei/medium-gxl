package cn.hds.hvmall.mapper.bargain;


import cn.hds.hvmall.entity.bargain.BargainSku;

import java.util.List;

public interface XquarkPromotionBargainProductMapper {

  //添加
  int insert(BargainSku bargainSku);

  //根据id查找
  List<BargainSku> selectProducts(String id);

  int deleteById(String bargainId);
}