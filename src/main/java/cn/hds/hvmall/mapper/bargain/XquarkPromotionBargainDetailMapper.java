package cn.hds.hvmall.mapper.bargain;


import cn.hds.hvmall.entity.bargain.XquarkPromotionBargainDetail;

public interface XquarkPromotionBargainDetailMapper {
  int deleteByPrimaryKey(String id);

  int insert(XquarkPromotionBargainDetail record);

  int insertSelective(XquarkPromotionBargainDetail record);

  XquarkPromotionBargainDetail selectByPrimaryKey(String id);

  int updateByPrimaryKeySelective(XquarkPromotionBargainDetail record);

  int updateByPrimaryKey(XquarkPromotionBargainDetail record);
}