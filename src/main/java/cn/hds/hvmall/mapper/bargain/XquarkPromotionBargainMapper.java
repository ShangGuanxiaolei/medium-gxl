package cn.hds.hvmall.mapper.bargain;


import cn.hds.hvmall.entity.bargain.BargainListVO;
import cn.hds.hvmall.entity.bargain.SearchCondation;
import cn.hds.hvmall.entity.bargain.XquarkPromotionBargain;
import cn.hds.hvmall.pojo.grandsale.PromotionVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface XquarkPromotionBargainMapper {

  int deleteByPrimaryKey(String id);

  int insert(XquarkPromotionBargain record);

  //添加
  int insertVO(BargainListVO record);

  XquarkPromotionBargain selectByPrimaryKey(String id);

  //更新
  int updateByPrimaryKeySelective(BargainListVO record);

  int updateByPrimaryKey(XquarkPromotionBargain record);

  //砍价清单按条件分页
  List<BargainListVO> select(@Param("cond") SearchCondation searchCondation);

  int count();

  List<PromotionVO> selectEffectivePromotion();

  List<PromotionVO> selectEffectivePromotion2();

  PromotionVO selectPromotionVOById(String id);

  //查找活动详情
  BargainListVO selectById(String id);

  //更改活动状态
  int updateState(@Param("id") String id,@Param("state") Integer state);
}