package cn.hds.hvmall.mapper.category;

import cn.hds.hvmall.entity.category.CategoryForPromotion;
import cn.hds.hvmall.pojo.list.Taxonomy;
import cn.hds.hvmall.pojo.list.Term;
import cn.hds.hvmall.pojo.list.TermRelationship;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CategoryMapper {

    Term loadByName(String term);

    int insertTerm(Term term);

    int insertCategory(CategoryForPromotion category);

    int insertRelationShip(TermRelationship termRelationship);

    CategoryForPromotion selectByTermAndTaxonomy(@Param("termId") String termId,
                                                 @Param("taxonomy") Taxonomy taxonomy,
                                                 @Param("shopId") String shopId);

    CategoryForPromotion load(String id);

    Integer selectMaxIdx(@Param("parentId") String parentId, @Param("shopId") String shopId);

    List<TermRelationship> checkProductIdExists(@Param("objType") String objType,
                                                @Param("objId") String objId);

    List<CategoryForPromotion> selectCategoryName(@Param("objType") String objType,
                                                  @Param("grandSaleId") Integer grandSaleId);

    int delete(String id);

    int deleteTermById(String id);

    int deleteCategoryById(String id);

    int deleteProductById(String id);

    int deleteSkuByProductId(String productId);

    List<TermRelationship> selectByObjType(String type);

    TermRelationship selectTermRelationshipById(String id);

    int updateTermRelationshipByType(@Param("objType") String objType, @Param("id") String id);

    Term selectByTermId(String id);

    CategoryForPromotion selectByCategoryId(String id);

    Integer checkIsCreate(@Param("categoryName") String categoryName);
}