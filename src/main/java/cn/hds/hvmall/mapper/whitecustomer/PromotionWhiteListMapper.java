package cn.hds.hvmall.mapper.whitecustomer;

import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import java.util.List;

import cn.hds.hvmall.entity.whitecustomer.PromotionWhiteListVo;

/**
 * Created with IDEA
 * author:Yarm.Yang
 * Date:2019/1/17
 * Time:17:09
 * Des:大会拼团白名单
 */
public interface PromotionWhiteListMapper {
    int insert(PromotionWhiteListVo promotionWhiteListVo);

    int updateStatusByCpIdAndPcode(PromotionWhiteListVo promotionWhiteListVo);

    int update(PromotionWhiteListVo promotionWhiteListVo);

    List<PromotionWhiteListVo> selectByCpIdAndPcode(PromotionWhiteListVo promotionWhiteListVo);

    boolean checkCpIdExist(Long cpId);

    List<PromotionWhiteListVo> select(@Param("pager")Pageable pageable, @Param("pCode")String pCode);

    boolean checkPcodeExist(String pCode);

    boolean checkCpIdAndPcodeExist(@Param("cpId")Long cpId, @Param("pCode")String pCode);
}
