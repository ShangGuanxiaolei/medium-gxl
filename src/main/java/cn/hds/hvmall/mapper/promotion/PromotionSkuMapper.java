package cn.hds.hvmall.mapper.promotion;

import cn.hds.hvmall.entity.piece.PromotionSkus;
import cn.hds.hvmall.entity.promotion.PromotionProduct;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author: create by daiweiwei
 * @version: v1.0
 * @description:
 * @date: 2018/10/18 14:42
 */
public interface PromotionSkuMapper {

    int insertAll(List<Map> products);

    int insert(Map product);

    List<Map> getAllSku(Map param);

    int editPromotionProduct(PromotionProduct page);

    int delete(Map map);

    List<PromotionSkus> selectByPCode(Map map);

    int insertByPromotionSkus(PromotionSkus promotionSkus);

    int updateByPromotionSkus(PromotionSkus promotionSkus);

    int deleteByCode(Map map);

    int meetingProductGrounding(Map map);

    int deleteByProductId(@Param("productId") String productId);

    List<Integer> selectPromotionSkusByPCode(String pCode);

    List<Integer> getSkuNum(String pCode);

}
