package cn.hds.hvmall.mapper.promotion;

import cn.hds.hvmall.entity.freshman.FreshmanSaleCount;
import cn.hds.hvmall.entity.promotion.ActivityExclusive;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PromotionExclusiveMapper {

    /**
     * 添加活动商品时，同时将活动商品保存到活动表中
     * @param activityExclusive
     */
    void insertActivityProduct(ActivityExclusive activityExclusive);

    /**
     * 更新活动商品状态(在普通商品中是否展示)
     *
     */
    void updateActivityExclusive(ActivityExclusive activityExclusive);

    int updateStatus(@Param("productId") Long productId, @Param("pCode") String pCode,
                     @Param("status") int status);

    int overWriteStatus(@Param("productId") Long productId, @Param("pCode") String pCode,
                     @Param("status") int status);

    void deleteProducts(ActivityExclusive activityExclusive);

    int updateActivityExclusiveStatus(@Param("status")Integer status,@Param("pCode")String pCode);

    int updateBaseInfoStatus(@Param("pCode")String pCode);

    boolean selectExistsByProductId(@Param("productId") Long productId);

    int deleteActivity(@Param("pCode")String pCode);

    /**
     * 统计新人礼商品销量信息
     * @return
     */
    List<FreshmanSaleCount> selectFreshmanOrderSales();

    /**
     * 统计vip商品销量信息
     * @return
     */
    List<FreshmanSaleCount> selectVipSaleInfo();

    String selectEmailsTo();

    String selectEmailsCC();

    String selectIsSend();

    String selectSendTime();

    boolean updateIsSend(@Param("value")String value);

    boolean updateSendTime(@Param("value")String value);

    /**
     * 根据productId查询商品状态
     * @param productId
     * @return
     */
    String selectStatusByProductId(@Param("productId") int productId);

    /**
     * 根据productId查询商品的amont
     * @param productId
     * @return
     */
    int selectAmountByProductId(@Param("isStatus")String isStatus,@Param("productId")int productId);
}
