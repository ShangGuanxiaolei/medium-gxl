package cn.hds.hvmall.mapper.promotion;

import cn.hds.hvmall.entity.promotion.PromotionVo;
import cn.hds.hvmall.entity.promotion.Test;
import cn.hds.hvmall.pojo.promotion.Promotion;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author: create by daiweiwei
 * @version: v1.0
 * @description:
 * @date: 2018/10/18 14:42
 */
public interface PromotionMapper{

    Promotion selectByPrimaryKey(Long id);

    int insert(PromotionVo promotionVo);

    int insertHVPromotion(Promotion promotion);

    int addtest(Test test);

    List<Map> getAll();

    Map getBaseInfo(Map pCode);

    int editPromotion(PromotionVo promotionVo);

    int update(Promotion promotion);

    Promotion selectByProductId(@Param("productId") Long productId, @Param("skuId") Long skuId, @Param("date") Date date);

    int close(Long id);

    int deletedFlashSale(Long id);

    int unClose(Long id);

}
