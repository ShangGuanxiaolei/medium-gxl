package cn.hds.hvmall.mapper.promotion;

import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

import cn.hds.hvmall.entity.promotion.PromotionConfigVo;

/**
 * @author: create by daiweiwei
 * @version: v1.0
 * @description:
 * @date: 2018/10/18 14:42
 */
public interface PromotionConfigMapper {

    int insert(List<Map<String,String>> configs);

    List<Map> getPromotionConfigs(Map param);

    Map getPromotionConfigByName(Map param);

    int update(Map<String, String> stringStringMap);

    int insertByVo(PromotionConfigVo pcVO);

    boolean checkConfigName(@Param("configName") String configName);

    int updateByVo(PromotionConfigVo pcVO);

    PromotionConfigVo selectByVo(PromotionConfigVo pcVO);
}
