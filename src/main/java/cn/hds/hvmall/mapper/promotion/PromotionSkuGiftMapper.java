package cn.hds.hvmall.mapper.promotion;

import cn.hds.hvmall.entity.promotion.PromotionProduct;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author: create by daiweiwei
 * @version: v1.0
 * @description:
 * @date: 2018/10/18 14:42
 */
public interface PromotionSkuGiftMapper {

    int insert(Map product);

    int deleteOne(PromotionProduct product);

    int delete(Map map);

    List<Map> getAllProducts(@Param("projectCode") String projectCode);
}
