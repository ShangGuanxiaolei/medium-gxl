package cn.hds.hvmall.mapper.recommend;

import cn.hds.hvmall.entity.list.ProductSearchResult;
import org.apache.ibatis.annotations.Param;

import java.util.List;


public interface RecommendProductMapper {


	//批量添加商品
	int batchProduct(@Param("productIds") List<Long> productIds,@Param("recommendId")long recommendId);

	/**
	 * 查询已经选择的商品
	 */
	List<ProductSearchResult> searchSelectedProduct(@Param("id")long id,@Param("pageNumber")int pageNumber,@Param("pageSize")int pageSize);


	/**
	 * 查询推荐的商品数量
	 */
	int RecommentProductCount(@Param("recommendId")long recommendId);

	//更新商品的数量
	int updateRecommendNummber(@Param("nummber")int nummber,@Param("recommendId")long recommendId);

	int selectProductNummber(@Param("recommendId")long recommendId);


	List<Long> selectById(@Param("id") Long id);

	//查询已选商品数量
	int selectedCount(@Param("recommendId")long recommendId);

	//查询已选商品
	List<ProductSearchResult> SelectedProductById(@Param("id")long id);

	//删除已选择的商品
	int deleteSeletedProduct(@Param("recommendId")long recommendId,@Param("productId")long productId);

	//根据推荐id查询已选择的商品id
	List<Long> selectProductIds(@Param("recommendId")long recommendId);

	int deleteById(@Param("recommendId") long recommendId);

	List<Long> selectByRecommend(@Param("recommendId") long recommendId);
}