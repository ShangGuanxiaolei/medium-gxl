package cn.hds.hvmall.mapper.recommend;

import cn.hds.hvmall.entity.recommend.Recommend;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RecommendMapper {
  int deleteByPrimaryKey(Long id);

  int insert(Recommend record);

  int insertSelective(Recommend record);

  Recommend selectByPrimaryKey(Long id);

  int updateByPrimaryKeySelective(@Param("recommend") Recommend recommend);

  int updateByPrimaryKey(Recommend record);

  List<Recommend> selectRecommendList(@Param("rowIndex") int rowIndex, @Param("pageSize") int pageSize);

  List<Long> listAllSelected(@Param("recommendId") Long recommendId);

  //查询推荐的id
  List<Integer> selectRecommendIds();

  //添加推荐
  int addRecommend(Recommend recommend);

  int count();

  //校验code
  int checkCode(@Param("code") String code);

  int checkCodeStrong(@Param("code") String code);

  Integer countRecommend();

  //更新时的code校验
  int checkCodes(@Param("code") String code, @Param("recommendId") long recommendId);

  int checkCodesStrong(@Param("code") String code, @Param("recommendId") long recommendId);


  //更新时的code校验
  int checkCodesUpdate(@Param("code") String code, @Param("recommendId") long recommendId);

  int checkCodesStrongUpdate(@Param("code") String code, @Param("recommendId") long recommendId);

  //更新nummber
  int updateNumber(@Param("id") Long id,@Param("number") int number);

  //查看是否存在已选商品

  int isRecommend(@Param("id") Long id);
}