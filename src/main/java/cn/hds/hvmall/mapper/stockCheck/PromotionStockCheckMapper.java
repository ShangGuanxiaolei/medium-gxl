package cn.hds.hvmall.mapper.stockCheck;

import cn.hds.hvmall.entity.stockcheck.FlashSale;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author LiHaoYang
 * @ClassName PromotionStockCheckMapper
 * @date 2019/6/27 0027
 */
public interface PromotionStockCheckMapper {

    /**
     * 查询秒杀活动的sku信息
     * @return
     */
    List<FlashSale> selectFlashSale(@Param("promotionId") String promotionId);


    /**
     * 根据skuCode查询库存
     * @param skuId
     * @return
     */
    Integer selectAmountBySkuCode(@Param("skuId") int skuId);

    /**
     * 根据skuCode查询商品是否为套装商品
     * @param skuId
     * @return
     */
    Boolean selectIsBindedSlave(@Param("skuId")int skuId);

    /**
     * 根据skuCode查询商品绑定到套装中的数量
     * @param skuId
     * @return
     */
    int sumBindedSlaveAmount(@Param("skuId")int skuId);

    /**
     * 根据skuCode查询抽奖商品设置的件数
     * @param skuId
     * @return
     */
    int selectLotteryAmount(@Param("skuId")int skuId,@Param("probabilityId") String probabilityId);

    /**
     * 根据skuCode查询拼团剩余库存
     * @param skuId
     * @return
     */
    int selectPieceAmountBySkuCode(@Param("skuId")int skuId,@Param("pCode") String pCode);

    /**
     * 根据skuCode查询新人、折扣、分会场的商品剩余库存
     * @param skuId
     * @return
     */
    int selectFreshmanAndDiscountAndStadiumAmount(@Param("skuId")int skuId);

    /**
     * 根据复制的skuId查询分会场的库存
     * @param skuId
     * @return
     */
    int selectStadiumAmountBySkuId(@Param("skuId")int skuId);

    /**
     * 根据skuId查询新人库存
     * @param skuId
     * @return
     */
    int selectFreshmanAmount(@Param("skuId")int skuId);


    /**
     * 根据p_code和sku_id查询促销商品库存
     * @param pCode
     * @param skuId
     * @return
     */
    int selectProductSaleAmount(@Param("pCode") String pCode,@Param("skuId")int skuId);

    /**
     * 根据skuCode查询skuId
     * @param skuCode
     * @return
     */
    int selectSkuIdBySkuCode(@Param("skuCode") String skuCode);


    int selectStadiumAmount(@Param("type") String type ,@Param("grandSaleId")int grandSaleId,@Param("skuId")int skuId);

}
