package cn.hds.hvmall.mapper.activity;

import cn.hds.hvmall.pojo.activity.ActivityInfo;
import cn.hds.hvmall.pojo.activity.ActivityVO;

import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.List;

import cn.hds.hvmall.pojo.activity.PtDetail;
import org.apache.ibatis.annotations.Param;

/**
 * @Author: zzl
 * @Date: 2018/9/19 13:37
 * @Version
 */
public interface ActivityMapper {


  /**
   * 根据条件查询活动
   * @param activityInfo
   * @param pageNumber
   * @param pageSize
   * @return
   */
  List<ActivityVO> searchActivity(@Param("activityInfo") ActivityInfo activityInfo, @Param("pageNumber") int pageNumber, @Param("pageSize") int pageSize);

  /**
   * 统计满足条件的活动总数
   * @param activityInfo
   * @return
   */
  int countActivity(@Param("activityInfo") ActivityInfo activityInfo);
  List<ActivityVO> searchPtActivity(@Param("activityInfo") ActivityInfo activityInfo, @Param("pageNumber") int pageNumber, @Param("pageSize") int pageSize);
  String searchUserName(String id);
  String searchSkuname(String code);
  Timestamp successGroupTime(String code);
  Timestamp searchPtTime(@Param("tranCode") String tranCode,@Param("code") String code);
  List<PtDetail> searchPtOrder(String code);
}
