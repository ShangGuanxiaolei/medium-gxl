package cn.hds.hvmall.mapper;

import cn.hds.hvmall.entity.lottery.LotteryProbability;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface LotteryProbabilityMapper {
    int deleteByPrimaryKey(Long id);

    int insert(@Param("record") LotteryProbability record);

    LotteryProbability selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(@Param("probability")LotteryProbability probability);

    int updateByPrimaryKey(LotteryProbability record);

    // 查询id
	List<Long> selectProbabilityId(@Param("promotionLotteryId")Long promotionLotteryId);

	//删除改活动
	int deletePromotionLotteryProbability(@Param("id") Long id);

	List<LotteryProbability> selectByLotteryId(@Param("lotteryId") Long lotteryId);
}