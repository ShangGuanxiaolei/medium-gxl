package cn.hds.hvmall.mapper.freshman;

import cn.hds.hvmall.entity.freshman.Banner;
import cn.hds.hvmall.entity.freshman.BannerInfo;
import cn.hds.hvmall.entity.freshman.ProductType;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface FreshmanBannerMapper {

    /**
     * 根据id禁用、启用Banner
     * @param status
     * @param id
     * @return
     */
    boolean updateStatus(@Param("status") int status, @Param("id") String id);

    /**
     * 根据id查询状态
     * @param id
     * @return
     */
    int selectStatusById(@Param("id") String id);

    /**
     * 添加Banner
     * @param imgUrl
     * @param startAt
     * @param endAt
     * @param redirectType
     * @param redirectUrl
     * @return
     */
    boolean insertBanner(@Param("imgUrl")String imgUrl,@Param("startAt") String startAt,@Param("endAt") String endAt,@Param("redirectType")int redirectType,@Param("redirectUrl")String redirectUrl,@Param("status")int status);

    /**
     * 根据条件查询商品信息
     * @param productId
     * @param productName
     * @param typeId
     * @param brandId
     * @param pageNo
     * @param pageSize
     * @return
     */
    List<Banner> selectProductInfo(@Param("productId")Integer productId,@Param("productName")String productName,@Param("typeId")Integer typeId,@Param("brandId")Integer brandId,@Param("productList")String productList,@Param("pageNo")Integer pageNo,@Param("pageSize")Integer pageSize);

    /**
     * 根据条件查询商品总数
     * @param productId
     * @param productName
     * @param typeId
     * @param brandId
     * @return
     */
    int selectProductInfoCount(@Param("productId")Integer productId,@Param("productName")String productName,@Param("typeId")Integer typeId,@Param("brandId")Integer brandId,@Param("productList")String productList);


    /**
     * 查询父级商品类型
     * @return
     */
    List<ProductType> selectParentType();

    /**
     * 查询子级商品类型
     * @return
     */
    List<ProductType> selectDetailType(@Param("parentId")Integer parentId);

    /**
     * 查询出品牌
     * @return
     */
    List<ProductType> selectBrand();

    /**
     * 查询Banner信息
     * @return
     */
    List<BannerInfo> selectBannerInfo();

    /**
     * 修改Banner信息
     * @param bannerInfo
     * @return
     */
    boolean updateBannerInfo(BannerInfo bannerInfo);

}
