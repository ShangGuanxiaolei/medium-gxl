package cn.hds.hvmall.mapper.freshman;


import cn.hds.hvmall.entity.freshman.FreshmanModule;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface FreshmanModuleMapper {

    /**
     * 查询首页
     * @return
     */
    List<FreshmanModule> selectFreshmanModule();

    /**
     * 首页禁用、启用接口
     * @param status
     * @param id
     * @return
     */
    boolean updateStatus(@Param("status") int status, @Param("id") String id);

    /**
     * 根据id查询状态
     * @param id
     * @return
     */
    int selectStatusById(@Param("id") String id);

    /**
     * 把当前的sort修改为0
     * @return
     */
    @Update("update xquark_freshman_module set sort=0 ,updated_at=NOW() where id=#{id}")
    int updateZeroBySortNum(@Param("id")String id );

    /**
     * 把下一个sort修改为当前的sort
     * @return
     */
    @Update("update xquark_freshman_module set sort=#{NowSort},updated_at=NOW() where id=#{NextId}")
    int updateNowSortByNext(@Param("NowSort")String NowSort ,@Param("NextId")int NextId );

    /**
     * 把sort为0的修改为下一个sort
     * @return
     */
    @Update("update xquark_freshman_module set sort=#{NextSortNum},updated_at=NOW() where sort=0 ")
    int updatNextByZero(@Param("NextSortNum")int NextSortNum);

    /**
     * 查询id
     * @return
     */
    @Select("select id from xquark_freshman_module  ORDER BY sort")
    List<Integer> selectId();

    /**
     * 查询sort
     * @return
     */
    @Select("select sort from xquark_freshman_module where type!=3 ORDER BY sort")
    List<Integer> selectSortNum();

    /**
     * 修改人姓名
     * @param userName
     * @param id
     * @return
     */
    boolean updateUser(@Param("userName")String userName,@Param("id") String id);


}
