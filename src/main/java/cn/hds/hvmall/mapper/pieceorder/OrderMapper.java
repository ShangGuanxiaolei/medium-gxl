package cn.hds.hvmall.mapper.pieceorder;

import cn.hds.hvmall.entity.pieceorder.OrderSearchVO;
import cn.hds.hvmall.pojo.pieceorder.OrderInfo;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

/**
 * @Author: zzl
 * @Date: 2018/9/17 17:44
 * @Version
 */
@Component
public interface OrderMapper {

  /**
   * 通过指定条件筛选订单
   * 条件封装在orderinfo中
   * @param orderInfo
   * @param pageSize
   * @param pageNumber
   * @return
   */
  List<OrderSearchVO> searchOrder(@Param("orderInfo") OrderInfo orderInfo,
      @Param("pageSize") int pageSize, @Param("pageNumber") int pageNumber);

  /**
   * 查询满足条件的订单总数
   * @param orderInfo
   * @return
   */
  int countOrder(@Param("orderInfo") OrderInfo orderInfo);
}
