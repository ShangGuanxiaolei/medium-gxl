package cn.hds.hvmall.mapper.reserve;

import cn.hds.hvmall.entity.list.ReserveProductDetailInfo;
import cn.hds.hvmall.entity.reserve.EditPromotionInfo;
import cn.hds.hvmall.entity.reserve.PromotionReserveProduct;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface PromotionReserveProductMapper {
    int deleteByPrimaryKey(Long id);

    int insert(PromotionReserveProduct record);

    int insertSelective(PromotionReserveProduct record);

    PromotionReserveProduct selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(PromotionReserveProduct record);

    int updateByPrimaryKey(PromotionReserveProduct record);


		//批量添加预约商品
		int addReserveProductList(@Param("promotionReserveProduct")PromotionReserveProduct promotionReserveProduct);

		//删除预约预售商品
	int deleteReserveProduct(@Param("reserveId")String reserveId,@Param("skuId")Long skuId);


	//根据id和类目名称查询商品id
	List<Long> getProductIdsByReserveId(@Param("reserveId")String reserveId);

	//根据商品id和活动id查询商品活动的详细信息
	ReserveProductDetailInfo getProductDetailInfo(@Param("reserveId")Long reserveId,@Param("skuId")Long skuId);

	/**
	 * 通过reserveId删除
	 */
	int deleteReserve(@Param("reserveId")Long reserveId);

	/**
	 * 修改购买库存上限
	 */
	int updateAmountLimit(@Param("editPromotionInfo")EditPromotionInfo editPromotionInfo);

	/**
	 * 通过预约id查询类目名称
	 */
	List<String> getCategoryList(@Param("reserveId")Long reserveId);

	/**
	 * 通过预约id和类目名称查询商品的集合
	 */
	List<Long> getProductIdsByReserveAndCategory(@Param("category")String category,@Param("reserveId")Long reserveId);

	/**
	 * 通过预约预售id查询原有的库存
	 */
	Integer selectResersveAmount(@Param("reserveId") Long reserveId,@Param("skuId") Long skuId);

	/**
	 * 修改库存
	 */
	int updateReserveAmount(@Param("reserveId")Long reserveId,@Param("skuId")Long skuId,@Param("result")int result);

	/**
	 * 更新设置已预约数量
	 */
	int updateReserveLimit(@Param("reserveId")String reserveId,@Param("skuId")Long skuId,@Param("reserveLimit")int reserveLimit);

	/**
	 * 添加类目
	 */
	int addCategory(@Param("categoryName")String categoryName,@Param("reserveId")String reserveId);

	/**
	 * 查询预约预售所有的商品的skuId
	 */
	List<Long> selectAllSkuId();

	/**
	 * 查询活动的开始时间
	 */
	Date selectPromotionBegin(@Param("reserveId")Long reserveId);
	/**
	 * 查询活动结束时间
	 */
	Date selectPromotionEnd(@Param("reserveId")Long reserveId);

	/**
	 * 查询当前活动的所有的skuId
	 */
	List<Long> selectSKuIdById(@Param("reserveId")Long reserveId);

	/**
	 * 终止启用活动修改活动库存
	 */
	int updatePromotionAmount(@Param("reserveId")Long reserveId,@Param("skuId")Long skuId,@Param("pAmount")int pAmount);

	/**
	 * 更新预售商品信息
	 */
	int updateReserveProduct(@Param("promotionReserveProduct")PromotionReserveProduct promotionReserveProduct);

}