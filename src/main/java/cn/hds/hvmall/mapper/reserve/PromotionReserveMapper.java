package cn.hds.hvmall.mapper.reserve;

import cn.hds.hvmall.entity.list.ReserveResult;
import cn.hds.hvmall.entity.list.ReserveResultList;
import cn.hds.hvmall.entity.reserve.PromotionReserve;
import cn.hds.hvmall.pojo.grandsale.PromotionVO;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PromotionReserveMapper extends BaseMapper<PromotionReserve> {

    Long selectIdByCode(@Param("reserveCode") String reserveCode);

    int deleteByPrimaryKey(Long id);

    int insertSelective(PromotionReserve record);

    PromotionReserve selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(PromotionReserve record);

    int updateByPrimaryKey(PromotionReserve record);

    //添加预约
    int addReserve(@Param("promotionReserve") PromotionReserve promotionReserve);

    /**
     * 获取预约预售列表
     */
    List<ReserveResultList> getReserveList(@Param("name") String name, @Param("status") Integer status,
                                           @Param("rowIndex") int rowIndex, @Param("pageSize") int pageSize);

    Integer getReserveListCount(@Param("name") String name, @Param("status") Integer status);

    //根据id获取活动信息
    ReserveResult getPromotionReserveById(@Param("reserveId") Long reserveId);

    /**
     * 通过id删除活动信息
     */
    int deleteReserveById(@Param("reserveId") Long reserveId);

    /**
     * 根据id查询活动状态
     */
    Integer selectStatusById(@Param("reserveId") Long reserveId);

    /**
     * 将进行中的活动状态终止
     */
    int updateStatusById(@Param("reserveId") Long reserveId);

    /**
     * 将终止的活动状态改成进行中
     */
    int updateStartStatus(@Param("reserveId") Long reserveId);

    /**
     * 修改活动信息
     */
    int updatePromotionReserve(@Param("promotionReserve") PromotionReserve promotionReserve);

    /**
     * 查询已存在的skuId
     */

    List<Long> selectSkuIdById(@Param("reserveId") String reserveId);

    /**
     * 查询所有的预约名称
     */

    List<String> getReserveNameList();

    /**
     * 根据活动id查询出活动的库存
     */
    int selectPromotionAmountById(@Param("reserveId") Long reserveId, @Param("skuId") Long skuId);

    /**
     * 查询所以活动未删除的skuId
     */
    List<Long> selectSkuIdByPromotionId(@Param("reserveId") Long reserveId);

    /**
     * 查询有效的活动
     */
    List<PromotionVO> getEffectivePromotion();

    List<PromotionVO> getEffectivePromotion2();

    /**
     * 根据活动id查询预售活动
     *
     * @param id 活动id
     * @return 活动对象
     */
    PromotionVO selectByPromotionId(String id);

    /**
     * 根据活动id查询预售活动状态
     *
     * @param id 活动id
     * @return 活动对象
     */
    Integer selectStatusByPromotionId(String id);

    /**
     * 更新预购订单状态
     *
     * @param promotionId 活动id
     * @param status      状态
     * @param orgiStatus  原状态
     * @return 更新结果
     */
    int updateStatusByPromotionId(@Param("promotionId") Long promotionId, @Param("status") String status,
                                  @Param("orgiStatus") String orgiStatus);
}