package cn.hds.hvmall.mapper;

import java.util.Map;

import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.BaseMapper;

import cn.hds.hvmall.entity.User;
import cn.hds.hvmall.pojo.Users;



/**用户接口类
 * @类名: UserMapper .
 * @描述: TODO .
 * @程序猿: chenjingwu .
 * @日期: 2017年3月3日 下午1:54:32 .
 * @版本号: V1.0 .
 */
@Transactional(rollbackFor=NumberFormatException.class)
public interface UsersMapper extends BaseMapper<User> {
	
    // 获取用户信息实体
	public Users findSysUsersByParameter(Map<String, Object> parameterMap);


	
}
