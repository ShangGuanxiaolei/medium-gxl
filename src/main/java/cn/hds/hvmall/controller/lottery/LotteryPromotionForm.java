package cn.hds.hvmall.controller.lottery;

import cn.hds.hvmall.controller.RainLotteryPromotionForm;
import cn.hds.hvmall.pojo.lottery.PromotionLotteryVO;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 抽奖前台配置表单
 *
 * @author wangxinhua
 * @date 2019-04-23
 * @since 1.0
 */
public class LotteryPromotionForm extends RainLotteryPromotionForm {

    /**
     * 抽奖对象, 其中还要关联多个奖品及德分配置
     */
    @NotNull
    private List<PromotionLotteryVO> lotteryVOList;

	public List<PromotionLotteryVO> getLotteryVOList() {
		return lotteryVOList;
	}

	public void setLotteryVOList(List<PromotionLotteryVO> lotteryVOList) {
		this.lotteryVOList = lotteryVOList;
	}
}
