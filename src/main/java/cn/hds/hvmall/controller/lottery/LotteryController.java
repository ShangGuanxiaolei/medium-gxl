package cn.hds.hvmall.controller.lottery;

import cn.hds.hvmall.entity.lottery.ActivityForm;
import cn.hds.hvmall.pojo.ResponseObject;
import cn.hds.hvmall.service.lottery.LotteryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 抽奖配置
 * @author wangxinhua
 * @date 2019-04-23
 * @since 1.0
 */
@RestController
@RequestMapping("/lottery")
public class LotteryController {

	private final LotteryService lotteryService;

	@Autowired
	public LotteryController(LotteryService lotteryService) {
		this.lotteryService = lotteryService;
	}

	/**
     * 这个接口同时支持保存和更新
     * @param
     * @return 保存结果 - true or false
     */
		@RequestMapping(value = "/save", method = RequestMethod.POST)
		public ResponseObject<Boolean> save(@RequestBody LotteryPromotionForm form) {
			final Long id = form.getId();
			boolean ret = false;
			if (id == null) {
				ret = lotteryService.save(form);
			} else {
				ret = lotteryService.updatePromotion(form);
			}
			return new ResponseObject<>(ret);
		}

		/**
		 * 根据活动id查询活动详情
		 */
		@RequestMapping(value = "/getDetailMessage/{promotionId}",method = RequestMethod.GET)
		public ResponseObject getDetailMessage(@PathVariable("promotionId")Long promotionId){
			return new ResponseObject<>(lotteryService.getDetailMessage(promotionId));
		}

	/**
	 * 查询活动清单
	 */
	@RequestMapping(value = "/getActivityList",method = RequestMethod.POST)
	public ResponseObject getActivityList(@RequestBody ActivityForm activityForm){
		return new ResponseObject<>(lotteryService.getActivityList(activityForm));
	}

	/**
	 * 删除抽奖活动
	 */
	@RequestMapping(value = "/deleteActivity/{promotionId}",method = RequestMethod.POST)
	public ResponseObject deleteActivityById(@PathVariable("promotionId")Long promotionId){
		return new ResponseObject<>(lotteryService.deletePromotion(promotionId));
	}

	/**
	 * 结束活动
	 */
	@RequestMapping(value = "/endActivity/{promotionId}",method = RequestMethod.POST)
	public ResponseObject endActivityStatus(@PathVariable("promotionId")Long promotionId){
		return new ResponseObject<>(lotteryService.endActivityStatus(promotionId));
	}

}
