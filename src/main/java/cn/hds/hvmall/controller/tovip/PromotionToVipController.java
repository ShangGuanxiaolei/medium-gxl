package cn.hds.hvmall.controller.tovip;

import cn.hds.hvmall.mapper.tovip.PromotionToVipMapper;
import cn.hds.hvmall.pojo.ResponseObject;
import cn.hds.hvmall.pojo.tovip.PromotionToVip;
import cn.hds.hvmall.service.tovip.PromotionToVipService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class PromotionToVipController {
  @Autowired
  private PromotionToVipService promotionToVipService;
  @Autowired
  private PromotionToVipMapper promotionToVipMapper;

  /**
   * 展示
   */
  @ResponseBody
  @RequestMapping(value = "/toVip/list", method = RequestMethod.GET)
  public ResponseObject<Map<String, Object>> getToVipList(Pageable page) {
    Map<String, Object> toVipMap = new HashMap<>();
    List<PromotionToVip> promotionToVipList = promotionToVipService.selectList(page);
    Integer total = promotionToVipMapper.countList();
    toVipMap.put("promotionToVipList", promotionToVipList);
    toVipMap.put("total", total);
    return new ResponseObject<>(toVipMap);
  }

  /**
   * 保存
   *
   * @param promotionToVip
   * @return
   */
  @ResponseBody
  @RequestMapping(value = "/toVip/edit", method = RequestMethod.POST)
  public ResponseObject<String> edit(@RequestBody PromotionToVip promotionToVip) {
    promotionToVip.setUpdater(getUserName());
    if (promotionToVipService.updateRule(promotionToVip)) {
      return new ResponseObject<>("编辑成功");
    }
    return new ResponseObject<>("更新失败");
  }

  /**
   * 保存并使用
   *
   * @param promotionToVip
   * @return
   */
  @ResponseBody
  @RequestMapping(value = "/toVip/editAndUse", method = RequestMethod.POST)
  public ResponseObject<String> editAndUse(@RequestBody PromotionToVip promotionToVip) {
    promotionToVip.setUpdater(getUserName());
    if (promotionToVipService.updateAndUse(promotionToVip)) {
      return new ResponseObject<>("编辑成功");
    }
    return new ResponseObject<>("更新失败");
  }

  /**
   * 停用或使用
   */
  @ResponseBody
  @RequestMapping(value = "/toVip/changeState", method = RequestMethod.POST)
  public ResponseObject<String> changeState(@RequestBody PromotionToVip promotionToVip) {
    if (promotionToVipService.changeState(promotionToVip)) {
      if (promotionToVip.getState()) {
        return new ResponseObject<>("已启用");
      }
      return new ResponseObject<>("已停用");
    }
    return new ResponseObject<>("停用或启用失败");
  }

  /**
   * 添加
   */
  @ResponseBody
  @RequestMapping(value = "/toVip/addRule", method = RequestMethod.POST)
  public ResponseObject<String> addRule(@RequestBody PromotionToVip promotionToVip) {
    promotionToVip.setUpdater(getUserName());
    if (promotionToVipService.addRule(promotionToVip)) {
      return new ResponseObject<>("添加成功");
    }
    return new ResponseObject<>("添加失败");
  }

  //用户登录信息？？ RestTempleUtil 默认返回'admin'吧
  private String getUserName() {
        /*String user = getCurrentUser().getName();
        if (user == null) {
            user = String.valueOf(getCurrentUser().getCpId());
        }*/
    return "admin";
  }

  @ResponseBody
  @RequestMapping(value = "/toVip/selectById", method = RequestMethod.GET)
  public ResponseObject<PromotionToVip> selectById(@RequestParam("id") String id) {
    return new ResponseObject<>(promotionToVipService.loadById(id));
  }
}
