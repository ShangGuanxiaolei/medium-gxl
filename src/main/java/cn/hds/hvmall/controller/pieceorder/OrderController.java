package cn.hds.hvmall.controller.pieceorder;

import cn.hds.hvmall.entity.pieceorder.OrderSearchVO;
import cn.hds.hvmall.pojo.list.ExecuteResult;
import cn.hds.hvmall.pojo.pieceorder.OrderInfo;
import cn.hds.hvmall.service.pieceorder.OrderService;
import cn.hds.hvmall.utils.JSONUtil;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: zzl
 * @Date: 2018/9/14 15:37
 * @Version
 */
@RestController
@RequestMapping("/order")
@ResponseBody
public class OrderController {


  @Autowired
  private OrderService orderService;


  /**
   * 所有订单入口
   * @return
   */
  @RequestMapping(value = "/all",method = RequestMethod.POST)
  public ExecuteResult<List<OrderSearchVO>> searchOrder(@RequestBody String json){
    OrderInfo orderInfo = JSONUtil.toBean(json, OrderInfo.class);
    ExecuteResult<List<OrderSearchVO>> result = orderService.searchOrder(orderInfo,6);
    return result;
  }


}
