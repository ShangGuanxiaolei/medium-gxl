package cn.hds.hvmall.controller.customer;

import cn.hds.hvmall.base.BaseExecuteResult;
import cn.hds.hvmall.entity.customer.CustomerQuestion;
import cn.hds.hvmall.entity.customer.QuestionTypeVo;
import cn.hds.hvmall.service.customer.CustomerQuestionService;
import cn.hds.hvmall.utils.JSONUtil;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/Question")
public class CustomerQuestionController {

    @Autowired
    private CustomerQuestionService customerQuestionService;


    /**
     * 查询当前类型下的问题详情
     * @param json
     * @return
     */
    @RequestMapping("/customerDetail")
    @ResponseBody
    public List<CustomerQuestion> selectAll(@RequestParam("json")String json){
        Map<String,Object> map=JSONUtil.toMap(json);
        String title="";
        if(null!=map.get("title")){
           title=map.get("title").toString();
        }
        String typeId=map.get("typeId").toString();
        return customerQuestionService.selectQuestionAll(typeId,title);
    }


    /**
     * 查询问题类型
     * @return
     */
    @RequestMapping("/customerType")
    @ResponseBody
    public List<QuestionTypeVo> selectQueType(){
        return customerQuestionService.selectQueType();
    }


    /**
     * 逻辑删除问题内容
     * @param id
     * @return
     */
    @RequestMapping("/deleteDetail")
    @ResponseBody
    public boolean deleteDetail(int id){
        int a=customerQuestionService.deleteQuestion(id);
        boolean isdeleted;
        if(a>0){
            isdeleted=true;
        }
        else{
            isdeleted=false;
        }
        return isdeleted;
    }


    /**
     * 当前类型下的问题排序
     * @param json
     * @return
     */
    @RequestMapping("/sortNum")
    @ResponseBody
    public BaseExecuteResult<?> sort(@RequestParam("json") String json) {
        BaseExecuteResult<?> result = customerQuestionService.sort(json);
        return result;
    }


    /**
     * 添加客服问题
     * @param json
     * @return
     */
    @RequestMapping("/insertQuestion")
    @ResponseBody
    public boolean insertQuestion(@RequestParam("json")String json){
        boolean a=customerQuestionService.insertQuestion(json);
        return a;
    }


    /**
     * 修改客服问题
     * @param json
     * @return
     */
    @RequestMapping("/updateQuestion")
    @ResponseBody
    public boolean updatetQuestion(@RequestParam("json")String json){
        boolean a=customerQuestionService.updateQuestion(json);
        return a;
    }

    @RequestMapping("/selectContentById")
    @ResponseBody
    public BaseExecuteResult<?> selectContentById(String id){
        BaseExecuteResult<?> content=customerQuestionService.selectContentById(id);
        return content;
    }

    /**
     * 查询客服问题类型
     * @param type
     * @return
     */
    @RequestMapping("/selectAllType")
    @ResponseBody
    public List<QuestionTypeVo> selectAllType(String type){
        return customerQuestionService.selectQueTypeDetail(type);
    }

    /**
     * 逻辑删除问题类型
     * @param id
     * @return
     */
    @RequestMapping("/deletedQuestionType")
    @ResponseBody
    public boolean deletedQuestionType(int id){
        boolean a=customerQuestionService.deleteQuestionType(id);
        return a;
    }



    /**
     * 编辑问题类型信息
     * @return
     */
    @RequestMapping("/updateQuestionType")
    @ResponseBody
    public boolean updateQuestionType(@RequestParam("json")String json){
        Map<String ,Object> map=JSONUtil.toMap(json);
        String type=map.get("type").toString();
        String icon=map.get("icon").toString();
        String id=map.get("id").toString();
        boolean a=customerQuestionService.updateQuestionType(type,icon,id);
        return a;
    }


    /**
     * 添加问题类型信息
     * @return
     */
    @RequestMapping("/insertQuestionType")
    @ResponseBody
    public boolean insertQuestionType(@RequestParam("json")String json){
        boolean a=customerQuestionService.insertQuestionType(json);
        return a;
    }

    /**
     * 当前类型下的问题排序
     * @param json
     * @return
     */
    @RequestMapping("/typeSort")
    @ResponseBody
    public BaseExecuteResult<?> typeSort(@RequestParam("json") String json) {
        BaseExecuteResult<?> result = customerQuestionService.typeSort(json);
        return result;
    }

}
