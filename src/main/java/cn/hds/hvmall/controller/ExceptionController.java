package cn.hds.hvmall.controller;

import cn.hds.hvmall.pojo.ResponseObject;
import cn.hds.hvmall.utils.list.GlobalErrorCode;
import cn.hds.hvmall.utils.list.qiniu.BizException;
import org.slf4j.Logger;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.sql.SQLException;
import java.util.function.Function;

/**
 * @author wangxinhua
 * @date 2019-04-24
 * @since 1.0
 */
public interface ExceptionController {

    /**
     * 处理Controller抛出的异常
     *
     * @param e 异常实例
     * @return Controller层的返回值
     */
    @ExceptionHandler
    @ResponseBody
    default Object expHandler(Exception e) {
        final Logger logger = getLogger();
        if (logger != null) {
            logger.error("业务异常: ", e);
        }
        BizException be = ((Function<Throwable, BizException>) e1 -> {
            Throwable e2 = e1;
            do {
                if (e2 instanceof BizException) {
                    return (BizException) e2;
                }
                e1 = e2;
                e2 = e1.getCause();
            } while (e2 != null && e2 != e1);
            return null;
        }).apply(e);
        GlobalErrorCode ec;
        String moreInfo;
        if (e instanceof SQLException) {
            ec = GlobalErrorCode.INTERNAL_ERROR;
            moreInfo = e.getClass().getName();
        } else if (be == null) {
            ec = GlobalErrorCode.UNKNOWN;
            moreInfo = e.getClass().getName();
        } else {
            ec = be.getErrorCode();
            moreInfo = be.getMessage();
        }
        return new ResponseObject<>(moreInfo, ec);
    }

    Logger getLogger();

}
