package cn.hds.hvmall.controller.news;

import cn.hds.hvmall.base.PageBase;
import cn.hds.hvmall.entity.news.NewsDetail;
import cn.hds.hvmall.entity.news.NewsInfo;
import cn.hds.hvmall.entity.news.NewsType;
import cn.hds.hvmall.pojo.ResponseObject;
import cn.hds.hvmall.service.news.NewsService;
import cn.hds.hvmall.utils.list.GlobalErrorCode;
import cn.hds.hvmall.utils.list.qiniu.BizException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author LiHaoYang
 * @ClassName NewsController
 * @date 2019-8-6
 */
@Controller
@RequestMapping("/news")
public class NewsController {

    @Autowired
    private NewsService newsService;

    /**
     * 查询新闻类型
     * @return
     */
    @ResponseBody
    @RequestMapping("/selectType")
    public ResponseObject<List<NewsType>> selectType(){
        return new ResponseObject<>( newsService.selectNewsType());
    }

    /**
     * 查询文章列表
     * @return
     */
    @RequestMapping(value = "/selectNewsDetailList")
    @ResponseBody
    public PageBase<NewsDetail> selectNewsDetail(String  typeId,String status,@RequestParam(value = "pageNo",defaultValue = "1") Integer pageNo,@RequestParam(value = "pageSize",defaultValue = "10") Integer pageSize ){
        return newsService.selectNewsDetailList(typeId,status,pageNo,pageSize);
    }

    /**
     * 新增新闻类型
     * @return
     */
    @ResponseBody
    @RequestMapping("/insertType")
    public ResponseObject<Object> insertType(@RequestBody  Map map){
        String typeName=map.get("typeName").toString();
        List<NewsType> typeList=newsService.selectNewsType();
        List<String> typeNameList=new ArrayList<>();
        for (NewsType typeNames:typeList) {
            typeNameList.add(typeNames.getTypeName());
            if(typeNames.getTypeName().equals(typeName)){
                throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "类型不能重复添加");
            }
        }

        boolean a=newsService.insertNewsType(typeName);
        return new ResponseObject<>(a);
    }

    /**
     * 修改文章状态
     * @return
     */
    @RequestMapping(value = "/updateNewsStatus",method = RequestMethod.POST)
    @ResponseBody
    public ResponseObject<Object> updateNewsStatus(@RequestBody Map map){
        Integer status= (Integer) map.get("status");
        Integer newsId= (Integer) map.get("newsId");
        return new ResponseObject<>(newsService.updateNewsStatus(status,newsId));
    }

    /**
     * 新增文章
     * @return
     */
    @RequestMapping(value = "/insertNews",method = RequestMethod.POST)
    @ResponseBody
    public ResponseObject<Object> insertNews( @RequestBody Map map){

        Integer typeId=(Integer) map.get("typeId");
        Integer status=(Integer) map.get("status");
        String title=map.get("title").toString();
        String img=map.get("img").toString();
        String showTime=map.get("showTime").toString();
        String mediaSources=map.get("mediaSources").toString();
        String content=map.get("content").toString();
        String releaseUser=map.get("releaseUser").toString();
        Integer newsId=(Integer) map.get("newsId");
        return new ResponseObject<>(newsService.insertNewsDetail(typeId,status,title,img,showTime,mediaSources,content,releaseUser, newsId));
    }

    /**
     * 查询新闻具体信息
     * @return
     */
    @RequestMapping(value = "/selectNewsDetail")
    @ResponseBody
    public ResponseObject<NewsDetail> selectNewsDetail( Integer detailId){
        return new ResponseObject<>( newsService.selectNewsDetail(detailId));
    }


    /**
     * 查询新闻具体信息
     * @return
     */
    @RequestMapping(value = "/selectNewsList")
    @ResponseBody
    public ResponseObject<List<NewsInfo>> selectNewsList(){
        return new ResponseObject<>( newsService.selectNewsListByType());
    }

}
