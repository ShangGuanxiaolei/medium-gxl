package cn.hds.hvmall.controller.recommend;

import cn.hds.hvmall.entity.list.ProductSearchResult;
import cn.hds.hvmall.entity.recommend.SelectCondition;
import cn.hds.hvmall.mapper.list.ProductMapper;
import cn.hds.hvmall.pojo.ResponseObject;
import cn.hds.hvmall.service.recommend.ProductRecommendService;
import cn.hds.hvmall.utils.list.GlobalErrorCode;
import cn.hds.hvmall.utils.list.qiniu.BizException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/recommendProduct")
public class RecommendProductController {

  private final ProductRecommendService productRecommendService;

  private final ProductMapper productMapper;

  @Autowired
  public RecommendProductController(ProductRecommendService productRecommendService, ProductMapper productMapper) {
    this.productRecommendService = productRecommendService;
    this.productMapper = productMapper;
  }

  /**
   * 查询推荐所需要的商品
   */
  @ResponseBody
  @RequestMapping(value = "/getProductList", method = RequestMethod.POST)
  public ResponseObject<Map<String, Object>> getProductList(@RequestBody SelectCondition selectCondition) {
    Map<String, Object> resultMap = new HashMap<>();
    List<ProductSearchResult> resultList = productRecommendService.searchProduct(selectCondition);
    if (resultList == null) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "商品不存在");
    }
    int selectCount = productMapper.selectCount(selectCondition);
    resultMap.put("selectCount", selectCount);
    resultMap.put("productSearchResult", resultList);
    return new ResponseObject<>(resultMap);
  }

  /**
   * 已选推荐商品支持删除
   */
  @RequestMapping(value = "/deleteProduct", method = RequestMethod.POST)
  public ResponseObject<Boolean> deleteRecommendProduct(@RequestParam("recommendId") long recommendId, @RequestParam("productId") long productId) {
    if (recommendId <= 0) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "推荐id不合法");
    }
    return new ResponseObject<>(productRecommendService.deleteRecommendProduct(recommendId, productId));
  }
}
