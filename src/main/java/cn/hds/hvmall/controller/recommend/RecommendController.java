package cn.hds.hvmall.controller.recommend;


import cn.hds.hvmall.base.BaseExecuteResult;
import cn.hds.hvmall.entity.list.ProductLists;
import cn.hds.hvmall.entity.list.ProductListsVo;
import cn.hds.hvmall.entity.list.RecommendProductResult;
import cn.hds.hvmall.entity.recommend.Recommend;
import cn.hds.hvmall.mapper.recommend.RecommendMapper;
import cn.hds.hvmall.mapper.recommend.RecommendProductMapper;
import cn.hds.hvmall.pojo.ResponseObject;
import cn.hds.hvmall.service.recommend.ProductRecommendService;
import cn.hds.hvmall.service.recommend.RecommendService;
import cn.hds.hvmall.utils.list.GlobalErrorCode;
import cn.hds.hvmall.utils.list.qiniu.BizException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/recommend")
public class  RecommendController {

  private final RecommendService recommendService;

  private final ProductRecommendService productRecommendService;

  private final RecommendProductMapper recommendProductMapper;

  @Autowired
  private RecommendMapper recommendMapper;

  @Autowired
  public RecommendController(RecommendService recommendService, ProductRecommendService productRecommendService, RecommendProductMapper recommendProductMapper) {
    this.recommendService = recommendService;
    this.productRecommendService = productRecommendService;
    this.recommendProductMapper = recommendProductMapper;
  }

  /**
   * 添加推荐
   */
  @RequestMapping(value = "/addRecommend", method = RequestMethod.POST)
  @Transactional(rollbackFor = Exception.class)
  public ResponseObject<Long> addRecommend(@RequestBody ProductListsVo recommend) {
    if (recommend == null || recommend.getName() == null || recommend.getCode() == null || recommend.getProductLists() == null || recommend.getProductLists().size() == 0) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "请重新添加");
    }
    Recommend recommend1 = new Recommend();
    recommend1.setName(recommend.getName());
    recommend1.setCode(recommend.getCode());
    if (null!=recommend.getStrongPush()){
      if(1==recommend.getStrongPush()){
        recommend1.setStrongPush(1);
      }
    }
    if (recommendService.addRecommend(recommend1)) {
      if (productRecommendService.insertProduct(recommend1.getId(), recommend.getProductLists())) {
        if (recommendMapper.updateNumber(recommend1.getId(),recommend.getProductLists().size()) > 0) {
          return new ResponseObject<>(recommend1.getId());
        }
        return new ResponseObject<>("添加商品失败", GlobalErrorCode.INTERNAL_ERROR);
      }
      return new ResponseObject<>("添加失败", GlobalErrorCode.INTERNAL_ERROR);
    } else {
      throw new BizException(GlobalErrorCode.UNKNOWN, "添加失败，请核对推荐名称与推荐页面");
    }
  }

  /**
   * 复制接口
   *
   * @param id
   * @return
   */
  @ResponseBody
  @RequestMapping(value = "cpOther", method = RequestMethod.GET)
  public ResponseObject<String> copy(@RequestParam("id") Long id) {
    if (recommendService.copyOther(id)) {
      return new ResponseObject<>("复制成功，请及时修改相关信息");
    }
    return new ResponseObject<>("请重新复制");
  }

  /**
   * 修改推荐
   */
  @Transactional(rollbackFor = Exception.class)
  @RequestMapping(value = "/updateRecommend", method = RequestMethod.POST)
  public ResponseObject<String> updateRecommend(@RequestBody ProductLists recommend) {
    if (recommendService.updateRecommend(recommend)) {
      return new ResponseObject<>("更新成功");
    } else {
      return new ResponseObject<>("跟新失败");
    }
  }

  /**
   * 删除推荐
   */
  @RequestMapping(value = "/deleteRecommend", method = RequestMethod.POST)
  public ResponseObject<String> deleteRecommend(@RequestParam("recommendId") long recommendId) {
    if (recommendService.deleteRecommend(recommendId)) {
      return new ResponseObject<>("删除成功");
    } else {
      return new ResponseObject<>("删除失败");
    }
  }

  /**
   * 分页查询推荐
   */
  @RequestMapping(value = "/getRecommendList", method = RequestMethod.GET)
  public ResponseObject<Map<String, Object>> listRemmend(@RequestParam("pageNum") Integer pageNum, @RequestParam("pageSize") Integer pageSize) {
    BaseExecuteResult<List<Recommend>> recommnedList = recommendService.getRecommnedList(pageNum, pageSize);
    if (recommnedList == null) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "查询推荐列表失败");
    }
    List<Recommend> resultList = recommnedList.getResult();
    List<Recommend> recommendList = new ArrayList<>();
    Map<String, Object> recommendMap = new HashMap<>();
    for (Recommend recommend : resultList) {
      BaseExecuteResult<Integer> integerBaseExecuteResult = productRecommendService.searchRecommendProductCount(recommend.getId());
      recommend.setNummber(integerBaseExecuteResult.getResult());
      recommendList.add(recommend);
    }
    Integer pageTotle = recommendService.countRecommend();
    recommendMap.put("recommendList", recommendList);
    recommendMap.put("pageTotle", pageTotle);
    return new ResponseObject<>(recommendMap);
  }

  /**
   * 根据推荐id查询推荐商品
   */
  @RequestMapping(value = "/getRecommendById", method = RequestMethod.GET)
  public ResponseObject<Map<String, Object>> getRecommendProduct(@RequestParam("recommendId") Long recommendId, @RequestParam("pageNum") int pageNum, @RequestParam("pageSize") int pageSize) {
    BaseExecuteResult<RecommendProductResult> recommendProductResult = recommendService.getRecommendProductById(recommendId, pageNum, pageSize);
    Map<String,Object> map = new HashMap();
    if (recommendProductResult == null) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "该推荐不存在");
    }
    RecommendProductResult result = recommendProductResult.getResult();
    int totle = recommendProductMapper.RecommentProductCount(recommendId);
    List<Long> ids = recommendMapper.listAllSelected(recommendId);
    if (result == null) {
      return new ResponseObject<>();
    } else {
      map.put("result",result);
      map.put("totle",totle);
      map.put("ids", ids);
      return new ResponseObject<>(map);
    }
  }
}
