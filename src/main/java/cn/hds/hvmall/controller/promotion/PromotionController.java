package cn.hds.hvmall.controller.promotion;

import cn.hds.hvmall.base.BaseExecuteResult;
import cn.hds.hvmall.base.PageBase;
import cn.hds.hvmall.entity.piece.PromotionBaseInfo;
import cn.hds.hvmall.entity.promotion.PromotionInviteCode;
import cn.hds.hvmall.entity.promotion.PromotionProduct;
import cn.hds.hvmall.entity.promotion.PromotionVo;
import cn.hds.hvmall.service.invite.PromotionInviteCodeService;
import cn.hds.hvmall.service.promotion.PromotionService;
import cn.hds.hvmall.utils.ConstantUtil;
import cn.hds.hvmall.utils.JSONUtil;
import cn.hds.hvmall.utils.list.qiniu.Qiniu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @author: create by daiweiwei
 * @version: v1.0
 * @description:
 * @date: 2018/10/18 14:37
 */
@RestController
@RequestMapping("/promotion")
public class PromotionController {

    @Autowired
    private PromotionService promotionService;

    @Autowired
    private PromotionInviteCodeService promotionInviteCodeService;

    @Autowired
    private Qiniu qiniu;

    /**
     * 创建活动
     * @param json
     * @return
     */
    @RequestMapping(value = "/promotion",method = RequestMethod.POST)
    @ResponseBody
    public BaseExecuteResult addPromotion(@RequestBody String json){
        PromotionVo promotionVo = JSONUtil.toBean(json,PromotionVo.class);
        String res = promotionService.addPromotion(promotionVo);

        BaseExecuteResult result= new BaseExecuteResult<>(1,res);
        return result;
    }

    /**
     * 获取活动信息
     * @param pCode
     * @param pageNum
     * @param pageSize
     * @return
     */
    @RequestMapping(value = "/promotion",method = RequestMethod.GET)
    @ResponseBody
    public BaseExecuteResult getPromotion(String pCode,
                                          @RequestParam(required = false) int pageNum,
                                          @RequestParam(required = false) int pageSize){
        Map promotion = promotionService.getPromotion(pCode,pageNum,pageSize);
        BaseExecuteResult result= new BaseExecuteResult<>(1,promotion);
        return result;
    }

    /**
     * 活动信息修改接口
     * @param json
     * @return
     */
    @RequestMapping(value = "/promotionEdit",method = RequestMethod.POST)
    @ResponseBody
    public BaseExecuteResult editPromotion(@RequestBody String json){
        PromotionVo promotionVo = JSONUtil.toBean(json,PromotionVo.class);
        String res = promotionService.editPromotion(promotionVo);
        BaseExecuteResult result =  new BaseExecuteResult<>(1,res);
        return result;
    }

    /**
     * 获取活动商品信息
     * @return
     */
    @RequestMapping(value = "/promotionProduct",method = RequestMethod.POST)
    @ResponseBody
    public BaseExecuteResult getPromotionProduct(@RequestBody String json){
        PromotionProduct page = JSONUtil.toBean(json,PromotionProduct.class);
        BaseExecuteResult result= new BaseExecuteResult<>(1,promotionService.getPromotionProduct(page));
        return result;
    }

    /**
     * 活动配置信息列表查询
     * @param params
     * @return
     */
    @RequestMapping(value = "/queryPromotionBaseInfoList", method = RequestMethod.POST)
    public BaseExecuteResult<?> queryPromotionBaseInfoList(@RequestBody Map<String,Object> params){
        //BaseExecuteResult<?> @RequestParam(value = "pageNum") Integer pageNum,@RequestParam(value = "pageSize") Integer pageSize
        String pName="";
        if(null!=params.get("pName")){
            pName=params.get("pName").toString();
        }
        String pStatus="";
        if(null!=params.get("pStatus")){
            pStatus=params.get("pStatus").toString();
        }
        String productName="";
        if(null!=params.get("productName")){
            productName=params.get("productName").toString();
        }
        String skuCode="";
        if(null!=params.get("skuCode")){
            skuCode=params.get("skuCode").toString();
        }

        int pageNum = Integer.parseInt(params.get("pageNum").toString());
        int pageSize = Integer.parseInt(params.get("pageSize").toString());

        PageBase<PromotionBaseInfo> promotionBaseInfo = promotionService.queryPromotionBaseInfoList(pName,pStatus,productName,skuCode,pageNum, pageSize);
        return new BaseExecuteResult<Object>(ConstantUtil.success, promotionBaseInfo);

    }


    /**
     * 根据pCode更新活动状态
     * @return
     */
    @RequestMapping(value = "/updatePromotionStatusByPcode", method = RequestMethod.POST)
    public BaseExecuteResult<?> updatePromotionStatusByPcode(@RequestBody Map<String,Object> params){
        //BaseExecuteResult<?>
        String pCode = params.get("pCode").toString();
        String pStatus = params.get("pStatus").toString();

        Boolean result = promotionService.updatePromotionStatusByPcode(pCode, pStatus);
        if(result){
            return new BaseExecuteResult<Object>(ConstantUtil.success,result);
        }else{
            return new BaseExecuteResult<Object>(ConstantUtil.failed,result);
        }

    }

    @RequestMapping("/deletedPromotion")
    public BaseExecuteResult<?> DetelPromotion(String pCode){
        Boolean result = promotionService.DetelPromotion(pCode);
        if(result){
            return new BaseExecuteResult<Object>(ConstantUtil.success,result);
        }else{
            return new BaseExecuteResult<Object>(ConstantUtil.failed,result);
        }
    }

    @RequestMapping(value = "/initInviteCode", method = RequestMethod.POST)
    public BaseExecuteResult<?> initInviteCode(String pCode,
                                               @RequestParam(required = false,defaultValue = "0") int codeNum){
        BaseExecuteResult baseExecuteResult = new BaseExecuteResult();
        int num = promotionService.initInviteCode(pCode,codeNum);
        if(num==1){
            baseExecuteResult.setIsSuccess(1);
            baseExecuteResult.setResult("添加邀请码成功");
        }else {
            baseExecuteResult.setIsSuccess(0);
            baseExecuteResult.setErrorCode("添加邀请码失败");
        }
        return baseExecuteResult;
    }


    /**
     * 修改单个商品
     * @param json
     * @return
     */
    @RequestMapping(value = "/promotionProduct",method = RequestMethod.PUT)
    @ResponseBody
    public BaseExecuteResult editPromotionProduct(@RequestBody String json){
        PromotionProduct page = JSONUtil.toBean(json,PromotionProduct.class);
        BaseExecuteResult result = new BaseExecuteResult<>(1,promotionService.editPromotionProduct(page));
        return result;
    }

    /**
     * 删除单个产品
     * @param skuCode
     * @param projectCode
     * @return
     */
    @RequestMapping(value = "/promotionProduct/{skuCode}/{projectCode}",method = RequestMethod.DELETE)
    @ResponseBody
    public BaseExecuteResult deletePromotionProduct(@PathVariable String skuCode,@PathVariable String projectCode){
        BaseExecuteResult result = new BaseExecuteResult<>(1,promotionService.deletePromotionProduct(skuCode,projectCode));
        return result;
    }

    /**
     * 查询所有活动商品
     * @param projectCode
     * @param pageNum
     * @param pageSize
     * @return
     */
    @RequestMapping(value = "/promotionProduct" ,method = RequestMethod.GET)
    @ResponseBody
    public BaseExecuteResult getAllProduct(String projectCode,
                                           @RequestParam(required = false,defaultValue = "1") int pageNum,
                                           @RequestParam(required = false,defaultValue = "10") int pageSize){
        BaseExecuteResult result = new BaseExecuteResult<>(1,promotionService.getAllProducts(projectCode,pageNum,pageSize));
        return result;
    }

    @ResponseBody
    @RequestMapping(value = "/promotionCodeInfo")
    public PageBase<PromotionInviteCode> selectAllBypCode(@RequestBody  Map<String ,Object> params ) {
        String pCode=params.get("pCode").toString();
        boolean isTrue=(Boolean) params.get("isTrue");
        String code=params.get("code").toString();
        Integer pageNo=(Integer) params.get("pageNo");
        Integer pageSize=(Integer) params.get("pageSize");
        String cpid = null;
         if (false==isTrue){
            cpid="1";
        }
        PageBase<PromotionInviteCode> pageBase=promotionService.selectAllBypCode(pCode,cpid,code,pageNo,pageSize);
         return pageBase;
    }


    /**
     * 追加大会活动库存
     * @param params
     * @return
     */
    @RequestMapping(value = "/addMeetingStock",method = RequestMethod.POST)
    @ResponseBody
    public BaseExecuteResult addMeetingStock(@RequestBody Map<String ,Object> params){
        String pCode=params.get("pCode").toString();
        Integer addStock=(Integer)params.get("addStock");
        String skuCode=params.get("skuCode").toString();

        String res = null;
        try {
            res = promotionService.addMeetingStock(pCode,skuCode,addStock);
        } catch (Exception e) {
            e.printStackTrace();
            return new BaseExecuteResult<>(-1,"-1","追加库存失败");

        }

        BaseExecuteResult result= new BaseExecuteResult<>(1,res);
        return result;

    }



    /**
     * 大会活动单品上下架
     * @param params
     * @return
     */
    @RequestMapping(value = "/meetingProductGrounding",method = RequestMethod.POST)
    @ResponseBody
    public BaseExecuteResult meetingProductGrounding(@RequestBody Map<String ,Object> params){
        String pCode=params.get("pCode").toString();
        String groundType=params.get("groundType").toString();
        String skuCode=params.get("skuCode").toString();

        String res = null;
        try {
            res = promotionService.meetingProductGrounding(pCode,skuCode,groundType);
        } catch (Exception e) {
            e.printStackTrace();
            return new BaseExecuteResult<>(-1,"-1","活动单品上下架失败");

        }

        BaseExecuteResult result= new BaseExecuteResult<>(1,res);
        return result;

    }

}
