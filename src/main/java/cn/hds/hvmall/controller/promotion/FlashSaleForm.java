package cn.hds.hvmall.controller.promotion;

import cn.hds.hvmall.entity.piece.PromotionPgPrice;
import cn.hds.hvmall.pojo.promotion.PromotionFlashSale;
import cn.hds.hvmall.type.PromotionType;
import cn.hds.hvmall.utils.Transformer;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by wangxinhua on 17-11-30. DESC:
 */
public class FlashSaleForm {

    private Long id;

    private Long promotionId;

    private boolean force;

    private String title;

    private String img;

    private String subImg;

    @NotNull(message = "活动开始日期不能为空")
    private Date validFrom;

    @NotNull(message = "活动结束日期不能为空")
    private Date validTo;

    private Boolean isPointUsed;

    private boolean isShowTime;

    private boolean isShowProgress;

    private boolean isFreeDelivery;

    private boolean isCountEarning;

    private String identities;

    @NotEmpty
    private List<PricingSkuForm> skus;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(Long promotionId) {
        this.promotionId = promotionId;
    }

    public boolean isForce() {
        return force;
    }

    public void setForce(boolean force) {
        this.force = force;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getSubImg() {
        return subImg;
    }

    public void setSubImg(String subImg) {
        this.subImg = subImg;
    }

    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    public Date getValidTo() {
        return validTo;
    }

    public void setValidTo(Date validTo) {
        this.validTo = validTo;
    }

    /**
     * 把表单转为数据库需要的保存对象
     */
    public List<PromotionFlashSale> getPromotionProduct() {
        return this.getSkus().stream().map(s -> {
            // 先拷贝活动属性
            final PromotionFlashSale pf = Transformer.fromBean(s, PromotionFlashSale.class);
            // 再设置商品属性
            final PromotionPgPrice price = Transformer.fromBean(s, PromotionPgPrice.class);
            price.setPromotionId(this.getPromotionId());
            price.setPromotionPrice(s.getPrice());
            price.setPoint(s.getPoint());
            price.setType(PromotionType.FLASHSALE);
            pf.setSkuPricing(price);
            pf.setSales(0L);
            return pf;
        }).collect(Collectors.toList());
    }

    public Boolean getIsPointUsed() {
        return isPointUsed;
    }

    public void setIsPointUsed(Boolean pointUsed) {
        isPointUsed = pointUsed;
    }

    public boolean getIsShowTime() {
        return isShowTime;
    }

    public void setIsShowTime(boolean showTime) {
        isShowTime = showTime;
    }

    public boolean getIsShowProgress() {
        return isShowProgress;
    }

    public void setIsShowProgress(boolean showProgress) {
        isShowProgress = showProgress;
    }

    public String getIdentities() {
        return identities;
    }

    public void setIdentities(String identities) {
        this.identities = identities;
    }

    public boolean getIsFreeDelivery() {
        return isFreeDelivery;
    }

    public void setIsFreeDelivery(boolean freeDelivery) {
        isFreeDelivery = freeDelivery;
    }

    public boolean getIsCountEarning() {
        return isCountEarning;
    }

    public void setIsCountEarning(boolean countEarning) {
        isCountEarning = countEarning;
    }

    public List<PricingSkuForm> getSkus() {
        return skus;
    }

    public void setSkus(List<PricingSkuForm> skus) {
        this.skus = skus;
    }
}
