package cn.hds.hvmall.controller.promotion;

import cn.hds.hvmall.base.BaseExecuteResult;
import cn.hds.hvmall.entity.promotion.MeetingInfoVo;
import cn.hds.hvmall.entity.promotion.PromotionMeetingCountInfo;
import cn.hds.hvmall.service.promotion.PromotionMeetingCountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * @author LiHaoYang
 * @ClassName PromotionMeetingCountController
 * @date 2019/1/4 0004
 */
@RequestMapping("/meeting")
@Controller
public class PromotionMeetingCountController {

    @Autowired
    private PromotionMeetingCountService promotionMeetingCountService;

    @ResponseBody
    @RequestMapping("/selectMeetingInfo")
    public PromotionMeetingCountInfo selectMeetingInfo(String pCode){
        return promotionMeetingCountService.selectMeetingInfo(pCode);
    }

    @ResponseBody
    @RequestMapping("/selectMeetingInfoDetail")
    public BaseExecuteResult<List<MeetingInfoVo>> selectMeeting(@RequestParam("json")String json){
            return promotionMeetingCountService.selectMeeting(json);
    }

}
