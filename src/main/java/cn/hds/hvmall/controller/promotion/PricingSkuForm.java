package cn.hds.hvmall.controller.promotion;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * 支持价格体系的sku表单
 * @author wangxinhua
 * @date 2019-04-23
 * @since 1.0
 */
@Data
public class PricingSkuForm {

    private Long id;

    /**
     * skuId
     */
    @NotNull
    private Long skuId;

    @NotNull
    private Long productId;

    /**
     * 立减
     */
    @NotNull
    private BigDecimal reduction;

    /**
     * 净值
     */
    @NotNull
    private BigDecimal netWorth;

    /**
     * 服务费
     */
    @NotNull
    private BigDecimal serverAmt;

    /**
     * 推广费
     */
    @NotNull
    private BigDecimal promoAmt;

    /**
     * 活动价
     */
    @NotNull
    private BigDecimal price;

    /**
     * 兑换价
     */
    @NotNull
    private BigDecimal conversionPrice;

    /**
     * 活动库存
     */
    @NotNull
    private Long amount;

    /**
     * 限购数量
     */
    @NotNull
    private Long limitAmount;

    /**
     * 德分兑换部分
     */
    public BigDecimal getPoint() {
        if (price != null && conversionPrice != null) {
            return price.subtract(conversionPrice).multiply(BigDecimal.TEN);
        }
        return null;
    }

}
