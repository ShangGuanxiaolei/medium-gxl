package cn.hds.hvmall.controller.promotion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cn.hds.hvmall.pojo.ResponseObject;
import cn.hds.hvmall.pojo.promotion.PromotionConfig;
import cn.hds.hvmall.service.promotion.PromotionConfigService;

/**
 * Created with IDEA
 * author:Yarm.Yang
 * Date:2019/1/25
 * Time:10:50
 * Des:活动配置表controller
 */
@Controller
@RequestMapping("/promotion/config")
public class PromotionConfigController {

    @Autowired
    private PromotionConfigService promotionConfigService;

    @RequestMapping(value = "add",method = RequestMethod.POST)
    public ResponseObject<String> commit(@RequestBody PromotionConfig p){
        int count = this.promotionConfigService.insert(p);
        return null;
    }

    @RequestMapping(value = "update",method = RequestMethod.POST)
    public ResponseObject<String> update(@RequestBody PromotionConfig p){
        int count = this.promotionConfigService.update(p);
        return null;
    }

    @RequestMapping(value = "query",method = RequestMethod.GET)
    public ResponseObject<String> query(){
        PromotionConfig p = new PromotionConfig();
        PromotionConfig pc= this.promotionConfigService.select(p);
        return null;
    }
}