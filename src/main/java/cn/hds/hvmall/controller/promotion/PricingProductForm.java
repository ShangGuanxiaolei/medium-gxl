package cn.hds.hvmall.controller.promotion;

import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.List;

/**
 * @author wangxinhua
 * @date 2019-04-23
 * @since 1.0
 */
@Data
public class PricingProductForm {

    @NotBlank
    private Long productId;

    @NotEmpty
    private List<PricingSkuForm> skus;

}
