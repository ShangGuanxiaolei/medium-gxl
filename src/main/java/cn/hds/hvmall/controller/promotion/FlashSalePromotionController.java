package cn.hds.hvmall.controller.promotion;

import cn.hds.hvmall.base.RedisService;
import cn.hds.hvmall.config.PromotionConstants;
import cn.hds.hvmall.controller.ExceptionController;
import cn.hds.hvmall.entity.piece.PromotionBaseInfo;
import cn.hds.hvmall.entity.sku.Sku;
import cn.hds.hvmall.mapper.piece.PromotionBaseInfoMapper;
import cn.hds.hvmall.mapper.piece.PromotionSkusMapper;
import cn.hds.hvmall.mapper.sku.SkuMapper;
import cn.hds.hvmall.pojo.ResponseObject;
import cn.hds.hvmall.pojo.prod.ProductBasicVO;
import cn.hds.hvmall.pojo.promotion.*;
import cn.hds.hvmall.service.flashsale.FlashSalePromotionProductService;
import cn.hds.hvmall.service.promotion.PromotionService;
import cn.hds.hvmall.service.stockcheck.PromotionStockCheckService;
import cn.hds.hvmall.utils.DateUtils;
import cn.hds.hvmall.utils.Transformer;
import cn.hds.hvmall.utils.list.GlobalErrorCode;
import cn.hds.hvmall.utils.list.qiniu.BizException;
import cn.hds.hvmall.utils.list.qiniu.IdTypeHandler;
import com.google.common.collect.ImmutableMap;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.function.BiFunction;

/**
 * Created by wangxinhua on 17-11-23. DESC:
 */
@RestController
@RequestMapping("/flashSale")
@Slf4j
public class FlashSalePromotionController implements ExceptionController {

    private final FlashSalePromotionProductService flashSalePromotionService;

    private final RedisService redisService;

    private final PromotionBaseInfoMapper baseInfoMapper;

    private final PromotionSkusMapper promotionSkusMapper;

    private final PromotionService promotionService;

    private final SkuMapper skuMapper;

    private final PromotionStockCheckService promotionStockCheckService;

    @Autowired
    public FlashSalePromotionController(
            FlashSalePromotionProductService flashSalePromotionService, RedisService redisService, PromotionBaseInfoMapper baseInfoMapper,
            PromotionSkusMapper promotionSkusMapper, PromotionService promotionService, SkuMapper skuMapper, PromotionStockCheckService promotionStockCheckService) {
        this.flashSalePromotionService = flashSalePromotionService;
        this.redisService = redisService;
        this.baseInfoMapper = baseInfoMapper;
        this.promotionSkusMapper = promotionSkusMapper;
        this.promotionService = promotionService;
        this.skuMapper = skuMapper;
        this.promotionStockCheckService=promotionStockCheckService;
    }

    @RequestMapping(value = "/lockerSwitch")
    public ResponseObject<Boolean> lockerSwitch(@RequestParam("switch") String switcher) {
        if (StringUtils.equals(switcher, "on")) {
            redisService.set(PromotionConstants.FLASH_SALE_LOCKER, "1");
        } else if (StringUtils.equals(switcher, "off")) {
            redisService.del(PromotionConstants.FLASH_SALE_LOCKER);
        }
        return new ResponseObject<>(Boolean.TRUE);
    }

    @RequestMapping(value = "/lockerStatus")
    public ResponseObject<String> lockerStatus() {
        String locker = redisService.get(PromotionConstants.FLASH_SALE_LOCKER);
        if (locker != null) {
            return new ResponseObject<>(locker);
        }
        return new ResponseObject<>();
    }

    @RequestMapping(value = "/saveProduct", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseObject<Boolean> save(@Validated @RequestBody FlashSaleForm form) {
        return new ResponseObject<>(doList(form, flashSalePromotionService::savePromotionProduct));
    }

    @RequestMapping(value = "/updateProduct", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseObject<Boolean> update(@Validated @RequestBody FlashSaleForm form) {
        return new ResponseObject<>(doList(form, (promotion, promotionProducts) -> flashSalePromotionService.updatePromotionProduct(promotion, promotionProducts,
                form.isForce())));
    }

    /**
     * 针对表单进行操作
     *
     * @param form   表单
     * @param doFunc 操作方法
     * @return 结果
     */
    private Boolean doList(FlashSaleForm form, BiFunction<Promotion, List<PromotionFlashSale>, Boolean> doFunc) {
        if (form.getId() == null) {
            for (PricingSkuForm skuForm : form.getSkus()) {
                final Long productId = skuForm.getProductId();
                if (!check(productId, skuForm.getSkuId())) {
                    throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "商品 " + productId + " 已经参加其他活动, 请不要重复添加");
                }
            }
        }
        Promotion promotion = buildPromotionFromForm(form);
        List<PromotionFlashSale> pdList = form.getPromotionProduct();
        boolean result = doFunc.apply(promotion, pdList);
        // 将值放到redis中
        if (result) {
            pdList.forEach(pd -> updateRedisVal(promotion, pd));
        }
        return result;
    }

    private void updateRedisVal(Promotion promotion, PromotionFlashSale pd) {
        String promotionId = promotion.getIdEncoded();
        String skuId = IdTypeHandler.encode(pd.getSkuId());
        String productId = IdTypeHandler.encode(pd.getProductId());
        String amount = String.valueOf(pd.getAmount());
        String limitAmt = String.valueOf(pd.getLimitAmount());
        String flashSaleAmountKey = PromotionConstants.getFlashSaleAmountKey(promotionId, skuId);
        String flashSaleLimitKey = PromotionConstants.getFlashSaleLimitAmountKey(promotionId, productId);
        String flashSaleTotalAmountKey = PromotionConstants.getFlashSaleTotalAmountKey(promotionId, skuId);

        String redisAmtStr = redisService.get(flashSaleAmountKey);
        String redisTotalAmtStr = redisService.get(flashSaleTotalAmountKey);
        Long redisAmount = redisAmtStr == null || redisAmtStr.equals("null")
                ? null : Long.valueOf(redisAmtStr);
        Long redisTotalAmount = redisTotalAmtStr == null || redisTotalAmtStr.equals("null")
                ? null : Long.valueOf(redisTotalAmtStr);

        Date expireDate = DateUtils.addDay(promotion.getValidTo(), 10);
        long expire = Duration.between(new Date().toInstant(), expireDate.toInstant())
                .getSeconds();

        if (redisAmount == null || redisTotalAmount == null) {
            redisService.set(flashSaleAmountKey, String.valueOf(amount));
            redisService.set(flashSaleTotalAmountKey, String.valueOf(amount));
            redisService.expire(flashSaleAmountKey, expire);
            redisService.expire(flashSaleTotalAmountKey, expire);
        } else {
            Long betweenAmt = Long.valueOf(amount) - redisAmount;
            if (betweenAmt != 0) {
                redisService.set(flashSaleAmountKey, amount);
                redisService.set(flashSaleTotalAmountKey, String.valueOf(redisTotalAmount + betweenAmt));
            }
        }

        redisService.set(flashSaleLimitKey, String.valueOf(limitAmt));

        // 活动结束后一天过期
        long expireAt = promotion.getValidTo().getTime() + 24 * 60 * 60;
        redisService.expire(flashSaleLimitKey, expireAt);
        redisService.expire(flashSaleAmountKey, expireAt);
        redisService.expire(flashSaleTotalAmountKey, expireAt);
    }

    @RequestMapping(value = "/listPc", method = RequestMethod.GET)
    public ResponseObject<Map<String, ?>> listForPc(
            @RequestParam(defaultValue = "created_at") String order,
            @RequestParam(defaultValue = "desc") String direction,
            Pageable pageable) {
        List<Promotion> flashSalePromotionSkuVOS = flashSalePromotionService
                .listPromotion(order, Direction.fromString(direction), pageable, null);
        Long total = flashSalePromotionService.countPromotionSkuVOs(null);
        Map<String, ?> ret = ImmutableMap.of("list", flashSalePromotionSkuVOS,
                "total", total);
        return new ResponseObject<>(ret);
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ResponseObject<Map<String, Object>> listProducts(Integer grandSaleId, Pageable pageable) {
        log.info("暂时没有使用参数,Pageable:" + pageable.toString());
        return listFlashSale(grandSaleId);
    }

    @RequestMapping(value = "/listApply", method = RequestMethod.GET)
    public ResponseObject<Map<String, Object>> listApplyOrderVO(
            @RequestParam(defaultValue = "created_at") String order,
            @RequestParam(defaultValue = "DESC") String direction, Pageable pageable) {
        List<FlashSalePromotionOrderVO> list;
        Long count;
        try {
            list = flashSalePromotionService
                    .listOrderVO(order, Direction.fromString(direction), pageable);
            count = flashSalePromotionService.countOrderVO();
        } catch (BizException e) {
            throw e;
        } catch (Exception e) {
            String msg = "获取抢购申请列表失败";
            log.error(msg, e);
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, msg);
        }
        Map<String, Object> result = new HashMap<>();
        result.put("list", list);
        result.put("total", count);
        return new ResponseObject<>(result);
    }

    @RequestMapping(value = "/view/{id}")
    public ResponseObject<FlashSalePromotionSkuVO> view(@PathVariable("id") String id) {
        FlashSalePromotionSkuVO vo = flashSalePromotionService.loadPromotionSkuVO(id);
        return new ResponseObject<>(vo);
    }

    /**
     * 删除秒杀活动
     * @param id
     * @return
     */
    @RequestMapping(value = "/deletedFlashSale")
    public ResponseObject<Boolean> deletedFlashSale(@RequestParam("id") Long id) {
        boolean b = flashSalePromotionService.deletedFlashSale(id);
        return  new ResponseObject<>(b);
    }

    @RequestMapping(value = "/close")
    public ResponseObject<Boolean> close(@RequestParam("id") Long id) {
        boolean result = flashSalePromotionService.close(id);
        return new ResponseObject<>(result);
    }

    @RequestMapping(value = "/upload")
    public ResponseObject<Boolean> unClose(@RequestParam("id") Long id) {
        boolean ret = flashSalePromotionService.unClose(id);
        return new ResponseObject<>(ret);
    }

    @RequestMapping(value = "/checkInPromotion")
    public ResponseObject<Boolean> inPromotion(Long productId, Long skuId) {
        boolean result = flashSalePromotionService.inPromotion(productId, skuId);
        return new ResponseObject<>(result);
    }

    @RequestMapping(value = "/audit")
    public ResponseObject<Boolean> updateStatus(@RequestParam("applyId") String applyId,
                                                @RequestParam("status") String status) {
        boolean result = flashSalePromotionService.updateStatus(applyId, status);
        return new ResponseObject<>(result);
    }

    private Promotion buildPromotionFromForm(FlashSaleForm form) {
        Promotion promotion = Transformer.fromBean(form, Promotion.class);
        // 使用价格体系的价格, 这两个值设置为默认值
        promotion.setDiscount(BigDecimal.ZERO);
        promotion.setPrice(BigDecimal.ZERO);
        return promotion;
    }

    private ResponseObject<Map<String, Object>> listFlashSale(Integer grandSaleId) {
        List<FlashSalePromotionProductVO> list;
        try {
            list = flashSalePromotionService.listProductVOs(grandSaleId);
            setPriceAndCacheAmount(list);
        } catch (BizException e) {
            throw e;
        } catch (Exception e) {
            String msg = "获取秒杀商品列表失败";
            log.error(msg, e);
            throw new BizException(GlobalErrorCode.INTERNAL_ERROR, msg);
        }
        Map<String, Object> result = new HashMap<>();
        result.put("list", list);
        return new ResponseObject<>(result);
    }


    private boolean check(Long productId, Long skuId) {
        if (productId == null)
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "商品id为空");

        List<PromotionSkuVo> promotionSkuVo = promotionSkusMapper.getPromotionByProductId(productId);
        if (promotionSkuVo != null) {
            for (PromotionSkuVo skuVo :
                    promotionSkuVo) {
                if (skuVo.getpCode() != null) {
                    PromotionBaseInfo promotionBaseInfo = baseInfoMapper.selectByPCode(skuVo.getpCode());
                    if (promotionBaseInfo != null && "5".equals(promotionBaseInfo.getpStatus()))
                        return true;
                }
            }
        }

        Promotion promotion = promotionService.loadBySkuId(productId, skuId);
        return promotion == null;
    }

    private void setPriceAndCacheAmount(List<FlashSalePromotionProductVO> list) {
        for (FlashSalePromotionProductVO pdVO : list) {
            ProductBasicVO productBasicVO = pdVO.getProduct();
            List<Sku> skuList = skuMapper.selectByProductId(productBasicVO.getId());
            if (skuList != null && !skuList.isEmpty()) {
                productBasicVO.setPrice(skuList.get(0).getPrice());
            }
            setCacheAmountToPromotion(pdVO);
        }
    }

    private void setCacheAmountToPromotion(FlashSalePromotionProductVO pdVO) {
        String promotionId = IdTypeHandler.encode(pdVO.getPromotionId());
        String skuId = IdTypeHandler.encode(pdVO.getSkuId());
        String productId = IdTypeHandler.encode(pdVO.getProductId());

        String flashSaleAmountKey = PromotionConstants.getFlashSaleAmountKey(promotionId, skuId);
        String flashSaleLimitKey = PromotionConstants.getFlashSaleLimitAmountKey(promotionId, productId);
        String flashSaleTotalAmountKey = PromotionConstants
                .getFlashSaleTotalAmountKey(promotionId, skuId);
        Integer amount = Optional.ofNullable(redisService.get(flashSaleAmountKey)).map(Integer::valueOf)
                .orElse(0);
        Integer limitAmount = Optional.ofNullable(redisService.get(flashSaleLimitKey)).map(Integer::valueOf)
                .orElse(0);
        Integer totalAmount = Optional.ofNullable(redisService.get(flashSaleTotalAmountKey)).map(Integer::valueOf)
                .orElse(0);
        if (amount != 0 && limitAmount != 0 && totalAmount != 0) {
            pdVO.setAmount(Long.valueOf(amount));
            pdVO.setSales((long) (totalAmount - amount));
            pdVO.setLimitAmount(Long.valueOf(limitAmount));
        }

    }
    @RequestMapping(value = "/selectSystemSkuId")
    public ResponseObject<List<Long>> lockerSwitch(){
        String skuCode = flashSalePromotionService.selectSystemSkuId();
        String[] split = skuCode.split(",");
        List<String> skuCodeList = Arrays.asList(split);
        ArrayList<Long> skuIdList = new ArrayList<>();
        for (String s : skuCodeList) {
            List<Long> skuId = flashSalePromotionService.selectXquarkSkuId(s);
            for (Long aLong : skuId) {
                skuIdList.add(aLong);
            }
        }
        return new ResponseObject<>(skuIdList);
    }




    @Override
    public Logger getLogger() {
        return log;
    }


}
