package cn.hds.hvmall.controller.promotion;

import org.apache.commons.lang3.StringUtils;
import org.omg.PortableServer.POA;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.hds.hvmall.pojo.ResponseObject;
import cn.hds.hvmall.pojo.promotion.PromotionConfig;
import cn.hds.hvmall.service.promotion.PromotionConfigService;

/**
 * Created with IDEA
 * author:Yarm.Yang
 * Date:2019/1/25
 * Time:13:23
 * Des:拼团
 */
@Controller
@RequestMapping("promotion/group")
public class PromotionGroupController {

    public static final String PIECE_TITLE_STR="piece_title"; //配置表中name
    public static final String PIECE_TITLE_TYPE_STR="piece_title_type";//配置表中type
    @Autowired
    private PromotionConfigService promotionConfigService;

    @RequestMapping("add/title")
    @ResponseBody
    public ResponseObject<String> addGroupPromotionTitle(@RequestBody PromotionConfig p){
        ResponseObject ro = new ResponseObject();
        if(p == null){
            ro.setMoreInfo("参数不能为空！");
            ro.setData("");
            return ro;
        }
        if(StringUtils.isBlank(p.getConfigValue())){
            ro.setMoreInfo("configValue不能为空！");
            ro.setData("");
            return ro;
        }
        p.setConfigName(PIECE_TITLE_STR);
        p.setConfigType(PIECE_TITLE_TYPE_STR);
        int count = promotionConfigService.insert(p);
        if(count >= 1){
            ro.setData("提交成功！");
        }else {
            ro.setData("已存在，无需插入，如需更改，请执行更新操作！");
        }
        return ro;
    }

    @RequestMapping(value = "update/title",method = RequestMethod.POST)
    @ResponseBody
    public ResponseObject<String> updateGroupPromotionTitle(@RequestBody PromotionConfig p){
        ResponseObject ro = new ResponseObject();
        if(p == null){
            ro.setMoreInfo("参数不能为空！");
            ro.setData("");
            return ro;
        }
        if(StringUtils.isBlank(p.getConfigValue())){
            ro.setMoreInfo("configValue不能为空！");
            ro.setData("");
            return ro;
        }
        p.setConfigName(PIECE_TITLE_STR);
        p.setConfigType(PIECE_TITLE_TYPE_STR);
        int count = promotionConfigService.update(p);
        if(count >= 1){
            ro.setData("更新成功！");
        }else {
            ro.setData("更新失败！");
        }
        return ro;
    }

    @RequestMapping(value = "query/title",method = RequestMethod.GET)
    @ResponseBody
    public ResponseObject<String> queryGroupPromotionTitle(){
        ResponseObject ro = new ResponseObject();
        PromotionConfig p = new PromotionConfig();
        p.setConfigName(PIECE_TITLE_STR);
        p.setConfigType(PIECE_TITLE_TYPE_STR);
        PromotionConfig promotionConfig = this.promotionConfigService.select(p);
        if(promotionConfig == null){
            ro.setData("");
        }else {
            ro.setData(promotionConfig);
        }
        return ro;
    }
}