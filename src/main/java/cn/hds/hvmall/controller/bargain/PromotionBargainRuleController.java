package cn.hds.hvmall.controller.bargain;

import cn.hds.hvmall.pojo.ResponseObject;
import cn.hds.hvmall.pojo.bargain.BargainRuleVO;
import cn.hds.hvmall.service.bargain.PromotionBargainRuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 砍价规则Controller
 * @author wangxinhua
 * @since 1.0
 */
@RestController
@RequestMapping("/bargain/rule")
public class PromotionBargainRuleController {

    private final PromotionBargainRuleService bargainRuleService;

    @Autowired
    public PromotionBargainRuleController(PromotionBargainRuleService bargainRuleService) {
        this.bargainRuleService = bargainRuleService;
    }

    @RequestMapping(value = "/default", method = RequestMethod.GET)
    public ResponseObject<BargainRuleVO> current() {
        return new ResponseObject<>(new BargainRuleVO(bargainRuleService.listAll()));
    }

    @RequestMapping(value = "/update", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseObject<Boolean> update(@RequestBody @Validated BargainRuleVO rule) {
        return new ResponseObject<>(bargainRuleService.update(rule));
    }

}
