package cn.hds.hvmall.controller.bargain;

import cn.hds.hvmall.entity.bargain.BargainListVO;
import cn.hds.hvmall.entity.bargain.SearchCondation;
import cn.hds.hvmall.pojo.ResponseObject;
import cn.hds.hvmall.service.bargain.PromotionBargainService;
import cn.hds.hvmall.utils.list.GlobalErrorCode;
import cn.hds.hvmall.utils.list.qiniu.BizException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@Controller
public class BargainController {

    private final PromotionBargainService promotionHaggleService;

    @Autowired
    public BargainController(PromotionBargainService promotionHaggleService) {
        this.promotionHaggleService = promotionHaggleService;
    }

    @ResponseBody
    @RequestMapping(value = "/bargain/activityList", method = RequestMethod.GET)
    //获取砍价活动清单
    public ResponseObject<Map<String, Object>> getBargainLists(SearchCondation searchCondation) {
        return new ResponseObject<>(promotionHaggleService.searchList(searchCondation));
    }

    @ResponseBody
    @RequestMapping(value = "/bargain/addBargain", method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    //添加砍价活动
    public ResponseObject<String> addBargain(@RequestBody @Validated BargainListVO bargain) {
        if (promotionHaggleService.addBargain(bargain)) {
            return new ResponseObject<>("添加成功");
        }
        return new ResponseObject<>("添加失败，请重试");
    }

    @ResponseBody
    @RequestMapping(value = "/bargain/selectInfo", method = RequestMethod.GET)
    //查看砍价活动
    public ResponseObject<BargainListVO> selectBargain(@RequestParam("id") String id) {
        if (id.isEmpty()) {
            throw new BizException(GlobalErrorCode.UNKNOWN, "请传递需要查询的清单id");
        }
        return new ResponseObject<>(promotionHaggleService.selectInfo(id));
    }

    @ResponseBody
    @RequestMapping(value = "/bargain/updateBargain", method = RequestMethod.POST)
    //修改砍价活动
    public ResponseObject<String> updateBargain(@RequestBody BargainListVO bargain) {
        if (promotionHaggleService.updateBargain(bargain)) {
            return new ResponseObject<>("修改成功");
        }
        return new ResponseObject<>("修改失败，请重试");
    }

    @ResponseBody
    @RequestMapping(value = "/bargain/changeState", method = RequestMethod.POST)
    //修改砍价活动
    public ResponseObject<String> changeBargainState(String id, Integer state) {
        if (promotionHaggleService.changeBargainState(id, state)) {
            return new ResponseObject<>("作废失效成功");
        }
        return new ResponseObject<>("作废失效失败，请重试");
    }
}
