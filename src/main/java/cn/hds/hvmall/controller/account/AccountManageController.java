package cn.hds.hvmall.controller.account;

import cn.hds.hvmall.utils.HttpClientUtils;
import cn.hds.hvmall.utils.list.GlobalErrorCode;
import cn.hds.hvmall.utils.list.qiniu.BizException;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * 账号管理, 清除用户账号接口
 */
@RestController
@RequestMapping("/account")
public class AccountManageController {

    private static final String CLEAR_ACCOUNT_HVMALL_DEV = "http://hwsc-dev.handeson.com/sellerpc";
    private static final String CLEAR_ACCOUNT_HVMALL_UAT = "https://uathwsc.handeson.com/sellerpc";
    private static final String CLEAR_ACCOUNT_HVMALL_PROD = "https://hwsc.handeson.com/sellerpc";

    @Value("${profiles.active}")
    private String activeProfile;


    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public JSONObject getUserInfoBySearchKey(@RequestBody Map map) {
        String domain = CLEAR_ACCOUNT_HVMALL_UAT;
        if ("prod".equals(activeProfile)) {
            domain = CLEAR_ACCOUNT_HVMALL_PROD;
        }
        return HttpClientUtils.httpPost(domain + "/account/search",  JSONObject.parseObject(JSON.toJSONString(map)));
    }

    @RequestMapping("/clear/{cpId}")
    public JSONObject deleteCustomerProfile(@PathVariable("cpId") String cpId) {
        if ("prod".equals(activeProfile)) {
            throw new BizException(GlobalErrorCode.UNKNOWN, "该接口只能在测试环境使用");
        }
        return HttpClientUtils.httpGet(CLEAR_ACCOUNT_HVMALL_UAT + "/account/clear/" + cpId);
    }
}
