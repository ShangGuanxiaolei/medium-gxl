package cn.hds.hvmall.controller.whitecustomer;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import cn.hds.hvmall.pojo.ResponseObject;
import cn.hds.hvmall.pojo.whitecustomer.WhiteCus;
import cn.hds.hvmall.service.whitecustomer.WhiteCusService;

/**
 * Created with IDEA
 * author:Yarm.Yang
 * Date:2019/1/15
 * Time:16:53
 * Des:白人后台管理接口
 */
@RequestMapping("cus/white")
@Controller
public class WhiteCusController {

    private final static String EXCEL_2003_DOWN = ".xls"; // 2003- 版本的excel
    private final static String EXCEL_2007_UP = ".xlsx"; // 2007+ 版本的excel
    private static final String IMAGE_CPID_STR = "9999999"; //默认7个9
    @Autowired
    private WhiteCusService whiteCusService;

    /**
     *
     * @param type group拼团 meeting大会
     * @param ws
     * @return
     */
    @RequestMapping(value = "commit/{type}", method = RequestMethod.POST)
    @ResponseBody
    public ResponseObject<String> insert(@PathVariable("type") String type, @RequestBody WhiteCus ws){
        ResponseObject ro = new ResponseObject();

        if(StringUtils.isBlank(type)){
            ro.setMoreInfo("请求参数type不能为空！");
            return ro;
        }
        if(ws == null){
            ro.setMoreInfo("白人信息不能为空！");
            return  ro;
        }
        //大会校验pCode，拼团默认是piece
        if("meeting".equals(ws.getpCode()) && StringUtils.isBlank(ws.getpCode())){
            ro.setMoreInfo("pCode不能为空!");
            return ro;
        }
        if(StringUtils.isBlank(ws.getCpId())){
            ro.setMoreInfo("cpId不能为空！");
            return ro;
        }
        //去除前后空格
        ws.setCpId(ws.getCpId().trim());
        ws.setpCode(ws.getpCode().trim());
        //校验是否存在
        boolean b = whiteCusService.checkCpIdAndPcodeExist(Long.parseLong(ws.getCpId()),ws.getpCode());
        if(b){
            ro.setData("cpId："+ws.getCpId()+"已经存在！");
            return ro;
        }

        int count = whiteCusService.insert(type, ws);
        ro.setData("提交数据"+count + "条");
        return ro;
    }

    @RequestMapping(value = "del/{cpId}", method = RequestMethod.POST)
    @ResponseBody
    public ResponseObject<String> delete(@PathVariable("cpId") Long cpId){
        ResponseObject ro = new ResponseObject();
        if(cpId == null){
            ro.setMoreInfo("cpId不能为空");
            return ro;
        }
        int count = this.whiteCusService.deleteByCpId(cpId);
        ro.setData("删除数据"+count+"条");
        return ro;
    }

    @RequestMapping(value = "edit/{type}", method = RequestMethod.POST)
    @ResponseBody
    public ResponseObject<String> update(@PathVariable("type") String type, @RequestBody WhiteCus ws){
        ResponseObject ro = new ResponseObject();

        if(StringUtils.isBlank(type)){
            ro.setMoreInfo("请求参数type不能为空！");
            return ro;
        }
        if(ws == null){
            ro.setMoreInfo("白人信息不能为空！");
            return  ro;
        }
        //大会校验pCode，拼团默认是piece
        if("meeting".equals(ws.getpCode()) && StringUtils.isBlank(ws.getpCode())){
            ro.setMoreInfo("pCode不能为空!");
            return ro;
        }
        if(StringUtils.isBlank(ws.getCpId())){
            ro.setMoreInfo("cpId不能为空！");
            return ro;
        }
        //去除前后空格
        ws.setCpId(ws.getCpId().trim());
        ws.setpCode(ws.getpCode().trim());
        int count = whiteCusService.update(type, ws);
        ro.setData("更新数据"+count + "条");
        return ro;
    }

    @RequestMapping(value = "update/{pCode}", method = RequestMethod.POST)
    @ResponseBody
    public ResponseObject<String> updateByCpId(@RequestBody WhiteCus whiteCus, @PathVariable("pCode") String pCode){
        ResponseObject ro = new ResponseObject();
        if(whiteCus == null){
            ro.setMoreInfo("提交数据不能为空！");
            return  ro;
        }

        if(whiteCus.getCpId() == null){
            ro.setMoreInfo("cpId不能为空！");
            return  ro;
        }
        if(whiteCus.getStatus() == null){
            ro.setMoreInfo("status不能为空！");
            return  ro;
        }
        whiteCus.setpCode(pCode);
        int count = this.whiteCusService.updateWhiteStatus(whiteCus);
        ro.setData("更新数据"+ count + "条");
        return ro;
    }

    //更新大会状态
    @RequestMapping(value = "update/promotion/defaultCpid", method = RequestMethod.POST)
    @ResponseBody
    public ResponseObject<String> updatePromotionStatus(@RequestBody WhiteCus whiteCus){
        ResponseObject ro = new ResponseObject();
        if(whiteCus == null){
            ro.setMoreInfo("提交数据不能为空！");
            return  ro;
        }

        if(StringUtils.isBlank(whiteCus.getpCode())){
            ro.setMoreInfo("pCode不能为空！");
            return  ro;
        }
        if(whiteCus.getStatus() == null){
            ro.setMoreInfo("status不能为空！");
            return  ro;
        }
        int count = this.whiteCusService.updatePromotionStatus(whiteCus);
        ro.setMoreInfo("更新或插入"+count+"条");
        ro.setData("操作成功！");
        return ro;
    }

    @RequestMapping(value = "query/{cpId}/{pCode}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseObject<String> queryByCpId(@PathVariable("cpId") Long cpId, @PathVariable("pCode")String pCode){
        ResponseObject ro = new ResponseObject();
        if(cpId == null){
            ro.setMoreInfo("请求参数cpId不能为空！");
            return ro;
        }
        if(StringUtils.isBlank(pCode)){
            ro.setMoreInfo("请求参数pCode不能为空！");
            return ro;
        }
        if(IMAGE_CPID_STR.equals(cpId.toString())){ //直接过滤掉
            ro.setData("");
            return ro;
        }
        List<WhiteCus> wcList = this.whiteCusService.selectByCpIdAndPcode(cpId,pCode);
        if(wcList != null){
            ro.setData(wcList);
        }else {
            ro.setData("");
        }
        return ro;
    }

    @RequestMapping(value = "query", method = RequestMethod.GET)
    @ResponseBody
    public ResponseObject<String> query(Pageable pageable,String pCode,Long cpId){
        ResponseObject ro = new ResponseObject();
        if(StringUtils.isBlank(pCode)){
            ro.setMoreInfo("pCode不能为空！");
            return ro;
        }
        Map<String, Object> map = this.whiteCusService.select(pageable, pCode, cpId);
        if(map == null || map.isEmpty()){
            ro.setData("");
        }else {
            ro.setData(map);
        }
        return ro;
    }

    @RequestMapping(value = "query/status/{pCode}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseObject<String> queryStatusByPcode(@PathVariable("pCode") String pCode,Long cpId){
        ResponseObject ro = new ResponseObject();
        if(StringUtils.isBlank(pCode)){
            ro.setMoreInfo("pCode不能为空！");
            return ro;
        }
        if(cpId == null){
            cpId = Long.parseLong(IMAGE_CPID_STR);
        }
        List<WhiteCus> whiteCus = this.whiteCusService.selectByCpIdAndPcode(cpId, pCode);
        if(whiteCus == null){
            ro.setData("");
        }else {
            ro.setData(whiteCus);
        }
        return ro;
    }

    @RequestMapping("up/excel/{pCode}")
    @ResponseBody
    public ResponseObject<String> excelUpload(@RequestParam("file") MultipartFile file,@PathVariable("pCode") String pCode){
        ResponseObject ro = new ResponseObject();
        if(StringUtils.isBlank(pCode)){
            ro.setMoreInfo("pCode字段不能为空！");
            return ro;
        }
        String fileName = file.getOriginalFilename();
        try {
            InputStream in = file.getInputStream();
            if(in == null){
                ro.setMoreInfo("获取excel文件流为空！");
                return ro;
            }
            //创建Excel工作薄
            Workbook work = this.getWorkbook(in, fileName);
            if (null == work) {
                ro.setMoreInfo("创建Excel工作薄为空或excel格式不对！");
                return ro;
            }
            int count = this.whiteCusService.getExcelData(work,pCode);
            ro.setData("成功插入或更新"+count+"条！");
        } catch (IOException e) {
            ro.setMoreInfo("解析excel出现IO异常");
        }

        return ro;
    }

    public Workbook getWorkbook(InputStream inStr, String fileName) throws IOException {
        Workbook wb = null;
        String fileType = fileName.substring(fileName.lastIndexOf("."));
        if (EXCEL_2003_DOWN.equals(fileType)) {
            wb = new HSSFWorkbook(inStr); // 2003-
        } else if (EXCEL_2007_UP.equals(fileType)) {
            wb = new XSSFWorkbook(inStr); //2007+
        }
        return wb;
    }
}
