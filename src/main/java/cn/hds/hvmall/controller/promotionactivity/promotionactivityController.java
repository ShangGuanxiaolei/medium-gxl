package cn.hds.hvmall.controller.promotionactivity;

import cn.hds.hvmall.base.PageBase;
import cn.hds.hvmall.entity.freshman.ActivityProduct;
import cn.hds.hvmall.entity.freshman.ProductType;
import cn.hds.hvmall.entity.freshman.XquarkSku;
import cn.hds.hvmall.mapper.cuxiao.*;
import cn.hds.hvmall.pojo.ResponseObject;
import cn.hds.hvmall.pojo.list.ExecuteResult;
import cn.hds.hvmall.pojo.promotion.PromotionMessage;
import cn.hds.hvmall.service.impl.piece.PGServiceImpl;
import cn.hds.hvmall.service.promotionactivity.ProductSaleService;
import cn.hds.hvmall.service.promotionactivity.PromotionActivityImpl;
import cn.hds.hvmall.utils.list.GlobalErrorCode;
import cn.hds.hvmall.utils.list.qiniu.BizException;
import cn.hds.hvmall.utils.piece.UUIDGenerator;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import io.swagger.models.auth.In;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: WangXiao
 * Date: 2019/3/22
 * Time: 17:03
 * Description: 商品促销后台管理新建更新
 */
@Controller
@RequestMapping("/promotion")
@EnableScheduling
public class promotionactivityController {
     @Autowired
     private PromotionActivityImpl promotionActivity;
     @Autowired
     private PromotionBaseInfoCopyMapper   promotionBaseInfoCopyMapper;
     @Autowired
     private XquarkProductCopyMapper  xquarkProductCopyMapper;
     @Autowired
     private  PromotionSkusCopyMapper  promotionSkusCopyMapper;
      @Autowired
     private XquarkSkuCopyMapper xquarkSkuCopyMapper;
     @Autowired
     private  PromotionExclusiveCopyMapper promotionExclusiveCopyMapper;
     @Autowired
     private PromotionActivityImpl promotionActivityImpl;
     @Autowired
     private ProductSaleService productSaleService;

    private static final Logger logger = LoggerFactory.getLogger(PGServiceImpl.class);

    /**
     * 根据条件查询商品信息
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/selectActivityProduct",method = RequestMethod.POST)
    public ExecuteResult<PageBase<ActivityProduct>> selectProductInfo(@RequestBody Map  map) {
        String productName="";
        if(null!=map.get("productName") && !(map.get("productName").equals(""))){
            productName=map.get("productName").toString();
        }
        String status = "";

        if(null!=map.get("status")&&!(map.get("status").equals(""))){
            status=map.get("status").toString();
        }
        Integer productId = null;
        String skuCode="";
        if(null!=map.get("productId") && !(map.get("productId").equals(""))){
            String str  = map.get("productId")+"";
            if (str.matches("[0-9]+")){
               productId= Integer.parseInt(str);
            }
            else {skuCode=str; }
        }
        Integer type=null;
        if(null!=map.get("type") && !(map.get("type").equals(""))){
            type=(Integer) map.get("type");
        }
        Integer pageSize=null;
        if(null!=map.get("pageSize") && !(map.get("pageSize").equals(""))){
            pageSize=(Integer) map.get("pageSize");
        }
        Integer pageNo=null;
        if(null!=map.get("pageNo") && !(map.get("pageNo").equals(""))){
            pageNo=(Integer) map.get("pageNo");
        }
        return promotionActivity.selectProductInfo(productName,status,productId,skuCode,type,pageNo,pageSize);
    }

    /**
     * 查询类型
     * @param parentId
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/selectType",method = RequestMethod.GET)
    public ExecuteResult<List<ProductType>> selectType(@RequestParam(required = false) Integer parentId) {
        return promotionActivity.selectType(parentId);
    }

    /**
     * 删除终止商品促销活动
     */
    @ResponseBody
    @RequestMapping(value = "/deleteactivity",method = RequestMethod.GET)
    public ResponseObject<Object> deleteActivity(@Param("pCode") String pCode,@Param("status")Integer status){
        //status=1时删除商品促销活动,status=2时终止商品促销活动
            if(status == 1){
                Integer integer = promotionActivity.deleteActivity(pCode);
                Integer count = promotionActivity.queryProductByPcode(pCode);
                if(count!=0){
                    Integer integer1 = promotionActivityImpl.releaseDeleteProduct(pCode);
                    Integer integer2 = promotionActivityImpl.DeletePromotionSku(pCode);
                }
                if(integer==0){
                    throw  new BizException(GlobalErrorCode.valueOf("删除商品促销活动失败:"+pCode));
                }
            }
            if(status == 2){
                Integer integer = promotionActivity.stopActivity(pCode);
                Integer count = promotionActivity.queryProductByPcode(pCode);
                if(count!=0){
                    Integer integer1 = promotionActivityImpl.releaseDeleteProduct(pCode);
                    Integer integer2 = promotionActivityImpl.DeletePromotionSku(pCode);
                }
                if(integer==0){
                    throw  new BizException(GlobalErrorCode.valueOf("终止商品促销活动失败:"+pCode));
                }
            }
                return new ResponseObject<>(true);
    }
     /**
      * @Author chp
      * @Description //查询活动列表
      * @Date
      * @Param
      * @return
      **/
     @RequestMapping(value = "/getActivityList",method =RequestMethod.GET)
     @ResponseBody
     public ResponseObject<Map<String,Object>>   getActivityList(String name, String status, Pageable page){
         if (!status.matches("['0','1','2','3','4']")){
           throw  new  BizException(GlobalErrorCode.NOT_FOUND,"参数status范围为0-4字符串");
         };
         if(name.equals("null")||name.equals("")){name=null  ; }
         List<BaseInfoVo>  byPName   = promotionBaseInfoCopyMapper.findByPName(name, status, page);
         int activityCount  = promotionBaseInfoCopyMapper.getActivityCount(name, status);
         Map<String,Object> map = new LinkedHashMap<>();
         map.put("activityList",byPName);
         map.put("count",activityCount);
        return  new ResponseObject<>(map);
     }

       @RequestMapping(value = "/getPidBySkuCode",method = RequestMethod.GET)
       public ResponseObject<Long>   getPidBySkuCode(@RequestParam String skuCode){
           List<Long> productIdBySkuCode = xquarkSkuCopyMapper.findProductIdBySkuCode(skuCode);
           if(0==productIdBySkuCode.size()){
              throw  new  BizException(GlobalErrorCode.NOT_FOUND,"未找到该sku对应产品信息，请核对sku");
           }
           return  new ResponseObject<Long>(productIdBySkuCode.get(0));
       }


      /**
       * @Author chp
       * @Description //sku库存校验
       * @Date
       * @Param
       * @return
       **/
      @RequestMapping(value = "/checkStock",method =RequestMethod.GET)
      @ResponseBody
      public  ResponseObject<Boolean>   checkStock(@RequestParam Integer count,@RequestParam String skuCode) {

          Integer activityAmount=0;
          //校验商品库存是否足够
          List<Integer> amountBySkuCode = xquarkSkuCopyMapper.findAmountBySkuCode(skuCode);
          if (null == amountBySkuCode) {
              throw new BizException(GlobalErrorCode.NOT_FOUND, "未找到skuCode相关信息，请核对sku");
          }
          //判断是否有复制sku
          List<Integer> skuCodeBySourceSkuCode = xquarkSkuCopyMapper.findAmountBYSourceSkuCode(skuCode);
          if (0== skuCodeBySourceSkuCode.size()) {
              activityAmount=0;
          }else {
              activityAmount = skuCodeBySourceSkuCode.get(0);
          }
          //总的库存信息
          Integer amount = amountBySkuCode.get(0);
          // 当前活动库存信息
          if (count > 0) {
              if (count > (amount)) {
                  ResponseObject<Boolean> objectResponseObject = new ResponseObject<>();
                  objectResponseObject.setData(false);
                  objectResponseObject.setMoreInfo("增加库存不能超过" + (amount) + "个");
                  return objectResponseObject;
              }
          }
          if (count < 0) {
              if (-count > (activityAmount)) {
                  ResponseObject<Boolean> objectResponseObject = new ResponseObject<>();
                  objectResponseObject.setData(false);
                  objectResponseObject.setMoreInfo("减除库存不能超过" + (-activityAmount) + "个");
                  return objectResponseObject;
              }
          }
              return new ResponseObject<>(true);
          }





//    @RequestMapping(value = "/getActivityLists",method =RequestMethod.GET)
//    @ResponseBody
//    public ResponseObject<List<Base>>   getActivityLists(String pCode, String status, Pageable page){
//        if (!status.matches("['0','1','2','3','4']")){
//            throw  new  BizException(GlobalErrorCode.NOT_FOUND,"参数status范围为0-4字符串");
//        };
//
//        List<Base> promotionBaseInfos = promotionBaseInfoCopyMapper.queryActivityByPcode(pCode);
//
//
//        return  new ResponseObject<>( promotionBaseInfos);
//    }


    /**
     * 编辑活动商品信息 返回查询数据
     * @param
     * @return
     */
    @RequestMapping(value = "/queryActivityProduct",method =RequestMethod.GET)
    @ResponseBody
    public ResponseObject<Object>   getActivityList(@Param("pCode")String pCode){
//        HashMap<String, Object> map = new HashMap<>();
//        ArrayList<Object> list = new ArrayList<>();
//        PromotionBaseInfo promotionBaseInfo = promotionActivity.queryActivityByPcode(pcode);
//        List<ActivityProduct> activityProducts = promotionActivity.queryActivityProductByPcode(pcode);
//        map.put("activity",promotionBaseInfo);
//        map.put("product",activityProducts);
//        list.add(map);
        //判断该活动是否删除或终止返回查询信息体
        PromotionBaseInfo promotionBaseInfo = promotionBaseInfoCopyMapper.queryActivityStatusByPcode(pCode);
        if(promotionBaseInfo.getIsDeleted()==0||promotionBaseInfo.getpStatus()==6){
            List<Base> base1 = promotionBaseInfoCopyMapper.queryDeleteActivityByPcode(pCode);
            return new ResponseObject<>(base1);
        }
        List<Base> promotionBaseInfos = promotionBaseInfoCopyMapper.queryActivityByPcode(pCode);
        if(promotionBaseInfos.size()==0){
            ArrayList<Base> list = new ArrayList<>();
            Base base = promotionBaseInfoCopyMapper.queryPromotionBasePcode(pCode);
            list.add(base);
            return new ResponseObject<>(list);
        }else{
            return new ResponseObject<>(promotionBaseInfos);
        }

    }
    /**
     * 创建活动相关信息
     */
    @RequestMapping(value = "/creatPromotionBaseA")
    @ResponseBody
    public ResponseObject<PromotionBaseInfo>  creatActivityA(@RequestBody Map  map) throws  Exception{

        String pName = (String)map.get("pName");
        //转换DATE格式活动开始时间
        String  effectFrom = (String)map.get("effectFrom");
        //转换DATE格式活动结束时间
        String effectTo = (String)map.get("effectTo");
        Boolean type = get( map.get("type"));
        String bannerImgHome = (String)map.get("bannerImgHome");
        String bannerImgList = (String)map.get("bannerImgList");
        String customerType = (String)map.get("customerType");
        Boolean timeShow = get(map.get("timeShow"));
        Boolean calculateYield = get(map.get("calculateYield"));
        Boolean carriageFree = get(map.get("carriageFree"));
        String cornerTab = (String)map.get("cornerTab");




        String pCode = UUIDGenerator.getUUID();
        PromotionBaseInfo promotionBaseInfo = new PromotionBaseInfo();
        promotionBaseInfo.setpCode(pCode);

        if (null!=pName&&!pName.equals("")) {
            promotionBaseInfo.setpName(pName);
        }
        promotionBaseInfo.setpType("PRODUCT_SALES");


        Date startdate = timeStamp2Date(effectFrom, null);
        promotionBaseInfo.setEffectFrom(startdate);
        Date enddate = timeStamp2Date(effectTo, null);
        promotionBaseInfo.setEffectTo(enddate);
        if(null!=type){
            promotionBaseInfo.setType(type);
        }

        if(null!=bannerImgHome&&!bannerImgHome.equals("")){
            promotionBaseInfo.setBannerImgHome(bannerImgHome);
        }
        if(null!=bannerImgList&&!bannerImgList.equals("")){
            promotionBaseInfo.setBannerImgList(bannerImgList);
        }

        if(null!=customerType&&!customerType.equals("")){
            promotionBaseInfo.setCustomerType(customerType);
        }
        if(null!=timeShow){
            promotionBaseInfo.setTimeShow(timeShow);
        }
        promotionBaseInfo.setCalculateYield(calculateYield);
        promotionBaseInfo.setCarriageFree(carriageFree);
        promotionBaseInfo.setCornerTab(cornerTab);

//        是否创建活动



        Boolean  isCreate=true;

        //如果pCode不为空，则更新活动
        if(null!= map.get("pCode")&&!map.get("pCode").equals("")){
          isCreate=false;
        }


        if(isCreate){
            Object active  = map.get("activityProductList");
            //创建活动
            Integer integer = promotionActivity.insertPromotionBase(promotionBaseInfo);
            if (integer==0){
                throw  new BizException(GlobalErrorCode.valueOf("商品促销活动添加PromotionBaseInfo表失败:"+promotionBaseInfo.getpCode()));
            }
            if(null==active||"".equals(active)){
             return  new ResponseObject<>();
            }


        }
        else {
            Object active  = map.get("activityProductList");
            PromotionBaseInfo promotionBaseInfo1 = new PromotionBaseInfo();
            List<Long> pCode1  = promotionBaseInfoCopyMapper.findIdByPCode((String) map.get("pCode"));
            Optional.of(pCode1).orElseThrow(BizException.build(GlobalErrorCode.NOT_FOUND,"pCode错误"));
            promotionBaseInfo.setId(pCode1.get(0));
            promotionBaseInfoCopyMapper.updateByPrimaryKeySelective(promotionBaseInfo1);
            //没有数据传过来说明是直接保存活动
            if(null==active||"".equals(active)){
                return  new ResponseObject<>();
            }

        }

        List<ActivityProduct>   proList = JSONArray.parseArray(JSON.toJSONString(map.get("activityProductList")),ActivityProduct.class);

        for (ActivityProduct s :  proList) {
            {
                if (null!= s.getpId()){
                    if(isCreate){
                    XquarkProductWithBLOBs xquarkProductWithBLOBs = xquarkProductCopyMapper.selectByPrimaryKey(s.getpId().longValue());
                    xquarkProductWithBLOBs.setSourceId(s.getpId().longValue());
                    xquarkProductWithBLOBs.setId(null);

                        int i  = xquarkProductCopyMapper.insertSelective(xquarkProductWithBLOBs);
                        //活动商品过滤表
                        PromotionExclusive promotionExclusive  = new PromotionExclusive();
                        promotionExclusive.setpCode(pCode);
                        promotionExclusive.setProductId(xquarkProductWithBLOBs.getId());
                        promotionExclusive.setStatus(1);
                        int i3  = promotionExclusiveCopyMapper.insertSelective(promotionExclusive);

                        List<XquarkSku>   skuList  = s.getXquarkSkulist();
                        for (XquarkSku xquarkSku : skuList) {
                            cn.hds.hvmall.mapper.cuxiao.XquarkSku xquarkSku1 = new cn.hds.hvmall.mapper.cuxiao.XquarkSku();
                            PromotionSkus promotionSkus = new PromotionSkus();

                            xquarkSku1.setProductId(xquarkProductWithBLOBs.getId());
                            xquarkSku1.setSkuCode(xquarkSku.getSkucode()+"_cx");
                            xquarkSku1.setSpec(xquarkSku.getSpec());
                            xquarkSku1.setPrice(xquarkSku.getActivityprice());
                            xquarkSku1.setPoint(xquarkSku.getReduction());
                            xquarkSku1.setPromoAmt(xquarkSku.getPromoAmt());
                            xquarkSku1.setServerAmt(xquarkSku.getServerAmt());
                            xquarkSku1.setNetWorth(xquarkSku.getNetWorth());
                            xquarkSku1.setSourceSkuCode(xquarkSku.getSkucode());
                            xquarkSku1.setAmount(xquarkSku.getActivitystock());
                            int i1  = xquarkSkuCopyMapper.insertSelective(xquarkSku1);
                            //  插入  promotionSkus表
                            promotionSkus.setProductId(xquarkProductWithBLOBs.getId().toString());
                            promotionSkus.setpCode(pCode);
                            promotionSkus.setSkuCode(xquarkSku.getSkucode());
                            promotionSkus.setIsDeleted((byte)1);
                            promotionSkus.setBuyLimit( xquarkSku.getBuylimit().longValue());
                            promotionSkus.setAmount(xquarkSku.getActivitystock());
                            promotionSkus.setShow(xquarkSku.getShow());
                            int i2  = promotionSkusCopyMapper.insertSelective(promotionSkus);

                        }

                    }
                    else {
                        //执行更新或插入    插入新的sku 或更新sku
                        //判断是否有父id
                        //判断是否有父id 有则更新，没有则插入
                        Long sourceIdById = xquarkProductCopyMapper.findSourceIdById(s.getpId().longValue());
                        if (null!=sourceIdById){
//                                执行更新工作
//                                更新产品表
//                                更新活动商品过滤表
//                            PromotionExclusive promotionExclusive  = new PromotionExclusive();
//                            promotionExclusive.setpCode(pCode);
//                            promotionExclusive.setStatus(1);
//                            List<PromotionExclusive> pCode1   = promotionExclusiveCopyMapper.findByProductIdAndPCode(sourceIdById, (String) map.get("pCode"));
//                            if (null!=pCode1){
//                                //找到执行更新操作
//                                PromotionExclusive promotionExclusive = pCode1.get(0);
//                                promotionExclusive.setStatus(1);
//                                promotionExclusiveCopyMapper.updateByPrimaryKeySelective(promotionExclusive);
//                            }
                            List<XquarkSku> xquarkSkulist  = s.getXquarkSkulist();
                            for (XquarkSku xquarkSku : xquarkSkulist) {
                                  //若果有数据则更新，没有数据创建sku
                            List<String> skuCodeBySourceSkuCode  = xquarkSkuCopyMapper.findSkuCodeBySourceSkuCode(xquarkSku.getSkucode());
                            if(null!=skuCodeBySourceSkuCode){
//                                有数据执行更新工作
                                cn.hds.hvmall.mapper.cuxiao.XquarkSku xquarkSku1 = new cn.hds.hvmall.mapper.cuxiao.XquarkSku();
                                PromotionSkus promotionSkus = new PromotionSkus();

                                xquarkSku1.setProductId(s.getpId().longValue());
                                xquarkSku1.setSkuCode(xquarkSku.getSkucode());
                                xquarkSku1.setSpec(xquarkSku.getSpec());
                                xquarkSku1.setPrice(xquarkSku.getActivityprice());
                                xquarkSku1.setPoint(xquarkSku.getReduction());
                                xquarkSku1.setPromoAmt(xquarkSku.getPromoAmt());
                                xquarkSku1.setServerAmt(xquarkSku.getServerAmt());
                                xquarkSku1.setNetWorth(xquarkSku.getNetWorth());
                                xquarkSku1.setSourceSkuCode(xquarkSku.getSkucode());
                                xquarkSku1.setAmount(xquarkSku.getActivitystock());
//                                int i1  = xquarkSkuCopyMapper.insertSelective(xquarkSku1);
                                xquarkSkuCopyMapper.updateByPrimaryKeySelective(xquarkSku1);

                                //    更新 promotionSkus表
                                promotionSkus.setProductId(s.getpId().toString());
                                promotionSkus.setpCode(pCode);
                                promotionSkus.setSkuCode(xquarkSku.getSkucode());
                                promotionSkus.setIsDeleted((byte)1);
                                promotionSkus.setBuyLimit( xquarkSku.getBuylimit().longValue());
                                promotionSkus.setAmount(xquarkSku.getActivitystock());
                                promotionSkus.setShow(xquarkSku.getShow());
//                                int i2  = promotionSkusCopyMapper.insertSelective(promotionSkus);
                                promotionSkusCopyMapper.updateByPrimaryKeySelective(promotionSkus);



                            }
                            else {
                                 //有数据执行插入工作
                                cn.hds.hvmall.mapper.cuxiao.XquarkSku xquarkSku1 = new cn.hds.hvmall.mapper.cuxiao.XquarkSku();
                                PromotionSkus promotionSkus = new PromotionSkus();

                                xquarkSku1.setProductId(s.getpId().longValue());
                                xquarkSku1.setSkuCode(xquarkSku.getSkucode()+"_cx");
                                xquarkSku1.setSpec(xquarkSku.getSpec());
                                xquarkSku1.setPrice(xquarkSku.getActivityprice());
                                xquarkSku1.setPoint(xquarkSku.getReduction());
                                xquarkSku1.setPromoAmt(xquarkSku.getPromoAmt());
                                xquarkSku1.setServerAmt(xquarkSku.getServerAmt());
                                xquarkSku1.setNetWorth(xquarkSku.getNetWorth());
                                xquarkSku1.setSourceSkuCode(xquarkSku.getSkucode());
                                xquarkSku1.setAmount(xquarkSku.getActivitystock());
                                int i1  = xquarkSkuCopyMapper.insertSelective(xquarkSku1);
                                //  插入  promotionSkus表
                                promotionSkus.setProductId(s.getpId().toString());
                                promotionSkus.setpCode(pCode);
                                promotionSkus.setSkuCode(xquarkSku.getSkucode()+"_cx");
                                promotionSkus.setIsDeleted((byte)1);
                                promotionSkus.setBuyLimit( xquarkSku.getBuylimit().longValue());
                                promotionSkus.setAmount(xquarkSku.getActivitystock());
                                promotionSkus.setShow(xquarkSku.getShow());
                                int i2  = promotionSkusCopyMapper.insertSelective(promotionSkus);
                            }
                            }
                        }
                        else {
//                            没有父id执行插入工作
                            XquarkProductWithBLOBs xquarkProductWithBLOBs = xquarkProductCopyMapper.selectByPrimaryKey(s.getpId().longValue());
                            xquarkProductWithBLOBs.setSourceId(s.getpId().longValue());
                            xquarkProductWithBLOBs.setId(null);

                            int i = xquarkProductCopyMapper.insertSelective(xquarkProductWithBLOBs);
                            //活动商品过
                            PromotionExclusive promotionExclusive = new PromotionExclusive();
                            promotionExclusive.setpCode(pCode);
                            promotionExclusive.setProductId(xquarkProductWithBLOBs.getId());
                            promotionExclusive.setStatus(1);
                            int i5 = promotionExclusiveCopyMapper.insertSelective(promotionExclusive);

                            List<XquarkSku> skuList = s.getXquarkSkulist();
                            for (XquarkSku xquarkSku : skuList) {
                                cn.hds.hvmall.mapper.cuxiao.XquarkSku xquarkSku1 = new cn.hds.hvmall.mapper.cuxiao.XquarkSku();
                                PromotionSkus promotionSkus = new PromotionSkus();

                                xquarkSku1.setProductId(xquarkProductWithBLOBs.getId());
                                xquarkSku1.setSkuCode(xquarkSku.getSkucode() + "_cx");
                                xquarkSku1.setSpec(xquarkSku.getSpec());
                                xquarkSku1.setPrice(xquarkSku.getActivityprice());
                                xquarkSku1.setPoint(xquarkSku.getReduction());
                                xquarkSku1.setPromoAmt(xquarkSku.getPromoAmt());
                                xquarkSku1.setServerAmt(xquarkSku.getServerAmt());
                                xquarkSku1.setNetWorth(xquarkSku.getNetWorth());
                                xquarkSku1.setSourceSkuCode(xquarkSku.getSkucode());
                                xquarkSku1.setAmount(xquarkSku.getActivitystock());
                                int i6 = xquarkSkuCopyMapper.insertSelective(xquarkSku1);
                                //  插入  promotionSkus表
                                promotionSkus.setProductId(xquarkProductWithBLOBs.getId().toString());
                                promotionSkus.setpCode(pCode);
                                promotionSkus.setSkuCode(xquarkSku.getSkucode()+"_cx");
                                promotionSkus.setIsDeleted((byte) 1);
                                promotionSkus.setBuyLimit(xquarkSku.getBuylimit().longValue());
                                promotionSkus.setAmount(xquarkSku.getActivitystock());
                                promotionSkus.setShow(xquarkSku.getShow());
                                int i7 = promotionSkusCopyMapper.insertSelective(promotionSkus);
                            }

                        }

                    }

                }

            }
        }

        return new ResponseObject<>();
    }
    /**
     * 创建活动相关信息2删除原数据重新插入数据逻辑删除
     */
    @RequestMapping(value = "/creatPromotionBaseB")
    @ResponseBody
    public ResponseObject<Boolean>  creatActivityB(@RequestBody Map  map) throws  Exception{
         promotionActivityImpl.insertActivity(map);
        return new ResponseObject<Boolean> (true);
    }
    /**
     * 更新活动商品信息
     */
    @RequestMapping(value = "/updatePromotionBaseB")
    @ResponseBody
    public ResponseObject<Boolean>  updateActivityB(@RequestBody Map  map) throws  Exception{
        promotionActivityImpl. updateActivity(map);
        return new ResponseObject<>(true);
    }


    /**
     *  核对pid
     */
    @RequestMapping(value = "/checkPid",method = RequestMethod.POST)
    @ResponseBody
    public ResponseObject<Boolean>  checkPid(@RequestBody  Long[] pid) throws  Exception{
       List<Long> longs = Arrays.asList(pid);
       List<Long> ids= new ArrayList<>();
        for (Long aLong : longs) {
            Boolean pid1 = xquarkProductCopyMapper.getPid(aLong);
           if(!pid1){
             ids.add(aLong);
           }
        }
         if (null!=ids){
            throw new BizException(GlobalErrorCode.NOT_FOUND,"未找到以下pid"+ids.toString()) ;
         }
        return new ResponseObject<>(true);
    }

    /**
     *  促销导入接受商品skuDode返回商品信息
     */
    @RequestMapping(value = "/getPromotionMessage",method = RequestMethod.POST)
    @ResponseBody
    public ResponseObject<Object> getPromotionMessage(@RequestBody  String[] skuCode) {
        ArrayList<PromotionMessage> promotionMessages = new ArrayList<>();
        ResponseObject<Object> result = new ResponseObject<>();
        String nullCode = "";
        for (String sku : skuCode) {
            PromotionMessage productMessage = promotionActivityImpl.getProductMessage(sku);
            if(null==productMessage){
                nullCode = nullCode+sku+",";
            }
            result.setMoreInfo("&skuCode="+nullCode);
            promotionMessages.add(productMessage);
        }
        result.setData(promotionMessages);
        return result;
    }

    //定时任务活动结束后释放源商品搜索
    @Scheduled(cron = "0 */3 * * * ?")
    public void   releaseProduct(){
        Date date = new Date();
        long datetime = date.getTime();
        List<PromotionBaseInfo> promotionBaseInfo = promotionActivityImpl.releaseProduct();
        for (PromotionBaseInfo baseInfo : promotionBaseInfo) {
            Date effectTo = baseInfo.getEffectTo();
            long effectTotime = effectTo.getTime();
            if(datetime>=effectTotime){
                Integer integer2 = promotionActivity.stopActivity(baseInfo.getpCode());
                Integer integer = promotionActivityImpl.releaseDeleteProduct(baseInfo.getpCode());
                Integer integer1 = promotionActivityImpl.DeletePromotionSku(baseInfo.getpCode());
            }
        }
    }


    public  static   Boolean get(Object type){
        if (type.toString().equals("1")){
            return  true;
        }
       return false;
    }

           /** 
      * 时间戳转换成日期格式字符串 
      * @param seconds 精确到秒的字符串 
      * @param formatStr 
      * @return 
      */
            public  static Date timeStamp2Date(String seconds, String format) {
            if(seconds == null || seconds.isEmpty() || seconds.equals("null")){
                    return  null;
        }
            if(format == null ||  format.isEmpty()) format = "yyyy-MM-dd HH:mm:ss";
                SimpleDateFormat sdf = new SimpleDateFormat(format);
            return  new Date(Long.valueOf(seconds));
        }


}


