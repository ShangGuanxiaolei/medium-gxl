package cn.hds.hvmall.controller.list;

import cn.hds.hvmall.base.BaseExecuteResult;
import cn.hds.hvmall.entity.list.Brand;
import cn.hds.hvmall.entity.list.ProductSearchSimple;
import cn.hds.hvmall.entity.list.Supplier;
import cn.hds.hvmall.pojo.ResponseObject;
import cn.hds.hvmall.pojo.list.Category;
import cn.hds.hvmall.pojo.list.ExecuteResult;
import cn.hds.hvmall.pojo.list.InfoSimple;
import cn.hds.hvmall.pojo.list.SearchTerm;
import cn.hds.hvmall.service.list.ProductService;
import cn.hds.hvmall.utils.JSONUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/product")
public class ProductController {

  private final ProductService productService;

  @Autowired
  public ProductController(ProductService productService) {
    this.productService = productService;
  }


  /**
   * 该方法中不仅要返回按条件查询到的结果，还要返回满足条件的sku总条数
   * 统一使用BaseExecuteResult形式返回
   * 修改:将页码参数也封装到json参数中
   * @param json json格式参数
   * @return 商品列表
   */
  @RequestMapping(value = "/list",method = RequestMethod.POST)
  public ExecuteResult<List<ProductSearchSimple>> productList(@RequestBody String  json) {
    InfoSimple infoSimple = JSONUtil.toBean(json, InfoSimple.class);
    return productService.searchProduct(infoSimple,6);
  }

  /**
   * 查询商品列表
   * @param searchTerm 查询条件
   * @return 查询结果
   */
  @RequestMapping(value = "/searchSku", method = RequestMethod.GET)
  public ResponseObject<Map> productList(SearchTerm searchTerm, Pageable pageable) {
    Map map = productService.searchSku(searchTerm, pageable);
    if (map.isEmpty()) {
      return new ResponseObject<>();
    }
    return new ResponseObject<>(map);
  }



  @RequestMapping(value = "/find", method = RequestMethod.GET)
  public ResponseEntity<String> getTime() {
    return ResponseEntity.ok("OK");
  }



  /**
   * 后台下拉框显示商品供应商
   * 前台用了该接口
   * @return Supplier
   */
  @RequestMapping(value = "/supplier",method = RequestMethod.GET)
  public BaseExecuteResult<List<Supplier>> searchSupplier(){
    return productService.searchSupplier();
  }



  /**
   * 后台下拉框显示商品分类
   * @return Category
   */
  @RequestMapping(value = "/category",method = RequestMethod.GET)
  public BaseExecuteResult<List<Category>> searchCategory(){
    return productService.searchCategory();
  }

  /**
   * 后台下拉框显示商品品牌
   * @return Brand
   */
  @RequestMapping(value = "/brand",method = RequestMethod.GET)
  public BaseExecuteResult<List<Brand>> searchBrand(){
    return productService.searchBrand();
  }

}
