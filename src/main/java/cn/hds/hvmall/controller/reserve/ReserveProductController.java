package cn.hds.hvmall.controller.reserve;

import cn.hds.hvmall.entity.list.ReserveDetailResult;
import cn.hds.hvmall.entity.list.SearchProductResult;
import cn.hds.hvmall.entity.reserve.PromotionAmountInfo;
import cn.hds.hvmall.entity.reserve.ReserveProductForm;
import cn.hds.hvmall.pojo.ResponseObject;
import cn.hds.hvmall.service.reserve.ReserveProductService;
import cn.hds.hvmall.utils.JSONUtil;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/reserveProduct")
public class ReserveProductController {


	private final ReserveProductService reserveProductService;

	@Autowired
	public ReserveProductController(ReserveProductService reserveProductService) {
		this.reserveProductService = reserveProductService;
	}

	/**
	 * 删除预约预售选择商品的接口
	 */
	@RequestMapping(value = "/deleteProduct",method = RequestMethod.POST)
	public ResponseObject deleteReserveProduct(@RequestParam("reserveId")String reserveId,@RequestParam("productId") Long skuId){
		boolean result = reserveProductService.deleteReserveProduct(reserveId, skuId);
		return new ResponseObject<>(result);
	}

	/**
	 * 查询商品的接口
	 */
	@RequestMapping(value = "/searchProduct",method = RequestMethod.POST)
	public ResponseObject searchProduct(@RequestBody  String json){
		ReserveProductForm reserveProductForm = JSONUtil.toBean(json, ReserveProductForm.class);
		return new ResponseObject<>(reserveProductService.searchProduct(reserveProductForm));
	}

	/**
	 * 预约预售商品信息数据回显
	 */
	@RequestMapping(value = "/getReserveDetail",method = RequestMethod.GET)
	public ResponseObject getReserveDetail(@RequestParam("reserveId")Long reserveId){
		ReserveDetailResult reserveDetailResult = reserveProductService.getReserveResultById(reserveId);
		return new ResponseObject<>(reserveDetailResult);
	}

	/**
	 * 预约预售修改库存接口
	 */
	@RequestMapping(value = "/updateAmount",method = RequestMethod.POST)
	public ResponseObject updateReserveAmount(@RequestParam("reserveId")Long reserveId,
																						@RequestParam("skuId")Long skuId,@RequestParam("pAmount")int pAmount){
		boolean isBoolean = reserveProductService.updateReserveAmount(reserveId, skuId, pAmount);
		return new ResponseObject<>(isBoolean);
	}

	/**
	 * 预约预售校验已经在活动中的商品不能删除
	 */
	@RequestMapping(value = "/checkPromotionTime",method = RequestMethod.GET)
	public ResponseObject checkPromotionTIme(@RequestParam("reserveId")Long reserveId, @RequestParam("skuList") List<Long> skuList){
		boolean isBoolean = reserveProductService.checkDelatePromotion(reserveId, skuList);
		return new ResponseObject<>(isBoolean);
	}

	/**
	 * 查询出活动的商品信息
	 */
	@RequestMapping(value = "/getPromotionProductInfo",method = RequestMethod.POST)
	public ResponseObject getPromotionProductList(@RequestParam("reserveId")Long reserveId){
		return new ResponseObject<>(reserveProductService.getProductAmount(reserveId));
	}

}
