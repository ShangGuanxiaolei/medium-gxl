package cn.hds.hvmall.controller.reserve;

import cn.hds.hvmall.entity.list.ReserveForm;
import cn.hds.hvmall.pojo.ResponseObject;
import cn.hds.hvmall.service.reserve.ReserveService;
import cn.hds.hvmall.utils.list.GlobalErrorCode;
import cn.hds.hvmall.utils.list.qiniu.BizException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * 预约预售
 */
@RestController
@RequestMapping(value = "/reserve")
public class ReserveController {

	@Autowired
	private ReserveService reserveService;

	@RequestMapping(value = "/addReserve",method = RequestMethod.POST)
	public ResponseObject<Boolean> addReserve(@RequestBody ReserveForm reserveForm){
	/*	ReserveForm reserveForm = JSONUtil.toBean(json,ReserveForm.class);*/
		Boolean isBoolean = false;
		if(reserveForm.getPromotionReserve().getId() == null){
			isBoolean = reserveService.addReserve(reserveForm);
		}else {
			reserveService.updatePromotionReserve(reserveForm);
		}
		return new ResponseObject<>(isBoolean);
	}

	/**
	 * 查询预约预售列表
	 */
	@RequestMapping(value = "/getReserveList",method = RequestMethod.GET)
	public ResponseObject<Map<String,Object>> getReserveList(@RequestParam(value = "name",required = false)String name,
																													 @RequestParam(value = "status",required = false)Integer status,
																													 @RequestParam(value = "pageIndex") int pageIndex,
																													 @RequestParam(value = "pageSize")int pageSize){
		Map<String,Object> reserveMap = reserveService.getReserveList(name, status,pageIndex,pageSize);
		return new ResponseObject<>(reserveMap);
	}

	/**
	 * 删除预约的接口
	 */
	@RequestMapping(value = "/deleteReserve",method = RequestMethod.POST)
	public ResponseObject deleteReserve(@RequestParam("reserveId")Long reserveId){
		boolean isBoolean = reserveService.deleteReserveById(reserveId);
		return new ResponseObject<>(isBoolean);
	}

	/**
	 * 进行中的预约手动终止的接口
	 */
	@RequestMapping(value = "/endStatus",method = RequestMethod.POST)
	public ResponseObject updateEndStatus(@RequestParam("reserveId")Long reserveId){
		boolean isBoolean = reserveService.endReserveStatus(reserveId);
		return new ResponseObject<>(isBoolean);
	}

	/**
	 * 已经终止的活动手动启用
	 */
	@RequestMapping(value = "/startStatus",method = RequestMethod.POST)
		public ResponseObject updateStartStatus(Long reserveId){
	//	List<PromotionAmount> promotionList = JSONUtil.toList(json, PromotionAmount.class);
		boolean isBoolean = reserveService.startReserveStatus(reserveId);
		return new ResponseObject<>(isBoolean);
	}

	/**
	 * 编辑活动的接口
	 */
	@RequestMapping(value = "/editPromotionInfo",method = RequestMethod.POST)
	public ResponseObject updatePromotionInfo(@RequestBody ReserveForm reserveForm){
		Long reserveId = reserveForm.getPromotionReserve().getId();
		if(reserveId <= 0){
			throw new BizException(GlobalErrorCode.INTERNAL_ERROR,"预约活动id不能为空");
		}
		boolean bool = reserveService.deleteReserveById(reserveId);
		if(!bool){
			throw new BizException(GlobalErrorCode.INTERNAL_ERROR,"预约活动修改失败");
		}
		Boolean reserve = reserveService.addReserve(reserveForm);
		return new ResponseObject<>(reserve);
	}
}
