package cn.hds.hvmall.controller.rain;

import cn.hds.hvmall.entity.rain.RainPromotionForm;
import cn.hds.hvmall.pojo.ResponseObject;
import cn.hds.hvmall.service.rain.PromotionListService;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.http.client.fluent.Request;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;


@RestController
@RequestMapping("/packetRain")
@Slf4j
public class PromotionRainController implements InitializingBean {

	@Value("${site.hv.api.host}")
	private String hvSite;

	private final static String REFRESH_RAIN_JOB_URL = "/v2/openapi/job/rainInit";

	@Autowired
	private PromotionListService promotionListService;

	/**
	 * 删除红包雨配置
	 */
	@RequestMapping(value = "/deletePromotion/{promotionId}", method = RequestMethod.POST)
	public ResponseObject delete(@PathVariable("promotionId") Long promotionId) {
		return new ResponseObject<>(promotionListService.deletePromotionById(promotionId));
	}

	/**
	 * 插入数据
	 */
	@RequestMapping(value = "/savePacketRain", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseObject add(@RequestBody @Validated RainPromotionForm showForm) {
		boolean ret;
		if (showForm.getId() == null || showForm.getId() <= 0) {
			ret = promotionListService.addPromotionList(showForm);
		} else {
			ret = promotionListService.updatePromotionPacketRain(showForm);
		}

		// 调用汉薇接口刷新红包雨数据
		val url = hvSite + REFRESH_RAIN_JOB_URL;
		new Thread(() -> callHvJobRefresh(url)).start();
		return new ResponseObject<>(ret);
	}

	/**
	 * 红包雨数据回显
	 */
	@RequestMapping(value = "/getDetailMessage/{promotionId}",method = RequestMethod.POST)
	public ResponseObject getDetailMessage(@PathVariable("promotionId")Long promotionId){
		return new ResponseObject<>(promotionListService.getRainDetailMessage(promotionId));
	}

	private static void callHvJobRefresh(String url) {
		try {
			val ret = Request.Post(url).execute().returnContent()
					.asString();
			val hvRes = JSON.parseObject(ret, ResponseObject.class);
			final boolean isSuccess = (boolean) hvRes.getData();
			if (isSuccess) {
				log.info("同步红包雨数据成功");
				return;
			}
			val moreInfo = hvRes.getMoreInfo();
			log.warn("同步红包雨结果: " + moreInfo);
		} catch (IOException e) {
			log.error("请求汉薇同步红包雨数据接口错误", e);
		}
	}

	public static void main(String[] args) {
		callHvJobRefresh("http://hwapi-dev.handeson.com" + REFRESH_RAIN_JOB_URL);
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		System.out.printf("");
	}
}