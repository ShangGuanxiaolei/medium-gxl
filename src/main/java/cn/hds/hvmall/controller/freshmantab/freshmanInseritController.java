package cn.hds.hvmall.controller.freshmantab;

import cn.hds.hvmall.mapper.cuxiao.PromotionSkus;
import cn.hds.hvmall.mapper.cuxiao.PromotionSkusCopyMapper;
import cn.hds.hvmall.mapper.freshmantab.*;
import cn.hds.hvmall.pojo.ResponseObject;
import cn.hds.hvmall.pojo.freshmantab.*;
import cn.hds.hvmall.service.freshman.FreshmanModuleService;
import cn.hds.hvmall.service.freshman.impl.FreshmanModuleServiceImpl;
import cn.hds.hvmall.service.freshmantab.FreshManServiceImpl;
import cn.hds.hvmall.service.stockcheck.PromotionStockCheckService;
import cn.hds.hvmall.utils.list.GlobalErrorCode;
import cn.hds.hvmall.utils.list.qiniu.BizException;
import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.time.OffsetDateTime.now;

/**
 * Created with IntelliJ IDEA.
 * User: WangXiao
 * Date: 2019/3/5
 * Time: 14:12
 * Description: 新人专区后台增加更新接口
 */
@Controller
@RequestMapping("/freshman")
public class freshmanInseritController {
    private static final Logger logger = LoggerFactory.getLogger(FreshmanModuleServiceImpl.class);

    @Autowired
    private FreshManServiceImpl freshManService;

    @Autowired
    private FreshManXquarkProductCopyMapper xquarkProductCopyMapper;

    @Autowired
    private FreshManPromotionExclusiveCopyMapper promotionExclusiveCopyMapper;

    @Autowired
    private FreshManXquarkSkuCopyMapper freshManXquarkSkuCopyMapper;

    @Autowired
    private FreshManPromotionSkusCopyMapper promotionSkusCopyMapper;

    @Autowired
    private FreshmanMapper freshmanMapper;

    @Autowired
    private FreshmanModuleService freshmanModuleService;

    @Autowired
    private PromotionSkusCopyMapper promotionSkusMapper;

    @Autowired
    private PromotionStockCheckService promotionStockCheckService;
    @RequestMapping(value = "/update/freshman")
    @ResponseBody
    public ResponseObject<Object> insertUpdateFreshman(@RequestBody Map  map){
        freshManService.inertOrUpdateProduct(map);
        return new ResponseObject<>(true);
    }

    //查询新人专区tabProduct数据
    @RequestMapping(value = "/query/tabproduct")
    @ResponseBody
    public ResponseObject<Object> queryTabProduct(){
        //查询新人tab商品
        List<FreshmanAreaProduct> freshmanAreaProducts = freshManService.queryTabProduct();
        for (FreshmanAreaProduct freshmanAreaProduct : freshmanAreaProducts) {
            List<FreshmanProduct> freshmanProducts = freshmanAreaProduct.getFreshmanProducts();
            for (FreshmanProduct freshmanProduct : freshmanProducts) {
//                if(null==freshmanProduct.getProductId()||"".equals(freshmanProduct.getProductId())){
//                    freshmanProduct.setProductId(freshmanProduct.getId().toString());
//                }
                List<FreshmanProductSku> freshmanProductSkus = freshmanProduct.getFreshmanProductSkus();
                for (FreshmanProductSku productSkus : freshmanProductSkus) {
                    if(null==productSkus.getSourceSkuCode()||"".equals(productSkus.getSourceSkuCode())){
                        productSkus.setAmount(productSkus.getSourceStock());
                        productSkus.setSourcePrice(productSkus.getFreshManPrice());
                        productSkus.setSourceConversionPrice(productSkus.getFreshConversionPrice());
                        productSkus.setFreshConversionPrice(null);
                        productSkus.setFreshManPrice(null);
                        productSkus.setFreshPoint(null);
                        productSkus.setFreshStock(null);
                        productSkus.setPromoAmt(null);
                        productSkus.setServerAmt(null);
                        productSkus.setReduction(null);
                        productSkus.setNetWorth(null);
                        productSkus.setNewFreshStock(null);
//                        productSkus.setSales(null);
                    }else{
//                        productSkus.setSales(freshmanMapper.selectAmountBySkuId(productSkus.getSkuId()));
                        if(null!=productSkus.getFreshManPrice() && null==productSkus.getSales()){
                            productSkus.setSales(0);
                        }
                        productSkus.setNewFreshStock(productSkus.getFreshStock());
                    }
                }
            }

        }
        HashMap<String, Object> map = new HashMap<>();
        for (FreshmanAreaProduct freshmanAreaProduct : freshmanAreaProducts) {
            map.put(freshmanAreaProduct.getTabId(),freshmanAreaProduct);
        }
        //查询新人专区标题
        List<Map<String, String>> maps = freshManService.queryTabtitle();
        HashMap<Object, Object> map1 = new HashMap<>();
        map1.put("tabproduct",map);
        map1.put("title",maps);

        return new ResponseObject<>(map1);
    }


    //校验库存
    @RequestMapping(value = "/checkStock",method = RequestMethod.GET)
    @ResponseBody
    public  ResponseObject<Boolean>   checkStock(@RequestParam Integer count, @RequestParam String skuCode) {

        Integer activityAmount=0;
        //校验商品库存是否足够
        List<Integer> amountBySkuCode = freshManXquarkSkuCopyMapper.findAmountBySkuCode(skuCode);
        if (null == amountBySkuCode) {
            throw new BizException(GlobalErrorCode.NOT_FOUND, "未找到skuCode相关信息，请核对sku");
        }
        //判断是否有复制sku
        List<Integer> skuCodeBySourceSkuCode = freshManXquarkSkuCopyMapper.findAmountBYSourceSkuCode(skuCode);
        if (0== skuCodeBySourceSkuCode.size()) {
            activityAmount=0;
        }else {
            activityAmount = skuCodeBySourceSkuCode.get(0);//活动库存
        }
        //总的库存信息
        Integer amount = amountBySkuCode.get(0);//源SKU库存
        // 当前活动库存信息
        if (count > 0) {
            if (count > (amount)) {
                ResponseObject<Boolean> objectResponseObject = new ResponseObject<>();
                objectResponseObject.setData(false);
                objectResponseObject.setMoreInfo("增加库存不能超过" + (amount) + "个");
                return objectResponseObject;
            }
        }
        if (count < 0) {
            if (-count > (activityAmount)) {
                ResponseObject<Boolean> objectResponseObject = new ResponseObject<>();
                objectResponseObject.setData(false);
                objectResponseObject.setMoreInfo("减除库存不能超过" + (-activityAmount) + "个");
                return objectResponseObject;
            }
        }
        return new ResponseObject<>(true);
    }


    //删除新人专区tabProduct数据
    @RequestMapping(value = "/delete/tabproduct")
    @ResponseBody
    public ResponseObject<Object> deleteTabProduct(@Param("productId")String productId,@Param("tabId")Long tabId) {
        Integer integer1 = freshManService.deletePromotionExclusive(productId);
        if (integer1==0){
            throw  new BizException(GlobalErrorCode.valueOf("商品删除PromotionExclusive失败"));
        }
        Integer integer = freshManService.deleteTabProduct(productId,tabId);
        if (integer==0){
            throw  new BizException(GlobalErrorCode.valueOf("商品删除失败"));
        }
        boolean flag = false;
        if(integer>0){
            flag = true;
        }
        return  new ResponseObject<>(flag);
    }

    @RequestMapping(value = "/insert/tabproduct")
    @ResponseBody
    public ResponseObject<Object> insertTabProduct(List<FreshmanProduct> freshmanProduct) {
        for (int i = 0; i < freshmanProduct.size(); i++) {
            freshmanProduct.get(i).setStatus(1);
            Integer integer1 = freshManService.insertPromotionExclusive(freshmanProduct.get(i));
            if (integer1==0){
                throw  new BizException(GlobalErrorCode.valueOf("商品添加PromotionExclusive表失败:"+freshmanProduct.get(i).getProductName()));
            }
            Integer integer = freshManService.insertFreshProduct(freshmanProduct.get(i));
            if (integer==0){
                throw  new BizException(GlobalErrorCode.valueOf("商品添加失败:"+freshmanProduct.get(i).getProductName()));
            }
        }
        return new ResponseObject<>(true);
    }

    /**
     * 判断能否保存
     * @param count
     * @param skuCode
     * @return
     */
    @RequestMapping(value = "/isSave")
    @ResponseBody
    public ResponseObject<Object> isSave(int count, @RequestParam(value = "skuCode", required = false) String skuCode,
                                         @RequestParam(value = "userName", required = false) String userName,
                                         @RequestParam(value = "moduleId", required = false) String moduleId,
                                         @RequestParam(value ="type", required =false) String type,
                                         @RequestParam(value ="pCode", required =false) String pCode,
                                         @RequestParam(value ="skuId", required =false) Integer skuId){

        //分会场传已经复制的skuId,新客,促销传已经复制的skuCode

        String FirstSkuCode=freshmanMapper.selectSourceSkuCodeBySkuCode(skuCode, skuId);//获取新客,促销,分会场复制的原skuCode
        Integer skuId1 = freshmanMapper.selectSourceSkuIdBySkuCode(FirstSkuCode);
        Integer id = null;
        if(null != skuCode && !"".equals(skuCode)){
            id = freshmanMapper.selectSourceSkuIdBySkuCode(skuCode);
        }


        int skuCodeAmount=freshmanMapper.selectSkuCodeAmount(skuCode, skuId);
        skuCodeAmount+=count;

        String promotiontype = null;
        String promotionId = null;
        if("productSale".equals(type)){
            promotiontype = "PRODUCTSALE";
            promotionId = pCode;
        }else if(null != skuId){
            promotiontype = "STADIUM2";
            promotionId = pCode;
        }else{
            promotiontype = "FRESHMAN";
            promotionId = null;
        }

        ResponseObject<Object> objectResponseObject = new ResponseObject<>();
        boolean a=false;
        int sourceCodeAmount=freshmanMapper.selectSourceSkuCodeAmount(FirstSkuCode);
        HashMap<Object, Object> map = new HashMap<>();
        if(promotionStockCheckService.stockCheck(skuCodeAmount, skuId1,promotionId,promotiontype,null) && skuCodeAmount>=0){
            a=true;
            map.put("flag",a);
            map.put("result",skuCodeAmount);
            objectResponseObject.setData(map);
            Integer integer = freshManService.updateStock(id, skuCodeAmount, skuId);
            if (integer==0){
                throw  new BizException(GlobalErrorCode.valueOf("更新商品库存失败:"+skuCode+skuId));
            }
            XquarkSku xquarkSku = freshmanMapper.selectProductIdBySkuCode(skuCode, skuId);
            int i2 = freshmanMapper.updateProductAmount(xquarkSku.getProductId());
            if (i2==0){
                throw  new BizException(GlobalErrorCode.valueOf("更新XquarkProduct表库存amount失败:"+xquarkSku.getProductId()));
            }
            //将库存>0 并且使用中下架的商品更新为上架状态
            if(null != skuCode && !"".equals(skuCode)){
                updateONSALE(type, xquarkSku.getProductId());
            }
//            objectResponseObject.setData(a);
            freshmanModuleService.updateUser(userName,moduleId);
            logger.info("库存修改成功，操作人："+userName+",操作时间："+now());
        }else if(skuCodeAmount>sourceCodeAmount){
            map.put("flag",false);
            map.put("result",skuCodeAmount);
            objectResponseObject.setMoreInfo("活动库存大于原库存");
            objectResponseObject.setData(map);
        }else if(skuCodeAmount<0){
            map.put("flag",false);
            map.put("result",skuCodeAmount);
            objectResponseObject.setMoreInfo("活动库存小于0");
            objectResponseObject.setData(map);
        }
        return objectResponseObject ;
    }

    /**
     * 获取活动库存
     * @param skuCode
     * @return
     */
    @RequestMapping(value = "/getStock")
    @ResponseBody
    public ResponseObject<Object> getStock(@RequestParam(value = "skuCode", required = false) String skuCode,
                                           @RequestParam(value ="skuId", required =false) Integer skuId){
        int i = freshmanMapper.selectSkuCodeAmount(skuCode, skuId);
        HashMap<Object, Object> map = new HashMap<>();
        map.put("activityStock",i);
        return new ResponseObject<>(map) ;
    }
    /**
     * 将库存>0 并且使用中下架的商品更新为上架状态
     */

    public void updateONSALE(String type, Long productId){
        if("productSale".equals(type)){
            List<PromotionSkus> promotionSkuses = promotionSkusMapper.queryProductById(productId.toString());
            XquarkProductWithBLOBs xquarkProductWithBLOBs1 = xquarkProductCopyMapper.selectByPrimaryKey(productId);
            for (int i7 = 0; i7 < promotionSkuses.size(); i7++) {
                if(promotionSkuses.get(i7).getIsDeleted() == 1 && xquarkProductWithBLOBs1.getAmount()>0 && xquarkProductWithBLOBs1.getStatus().equals("INSTOCK")){
                    int i = xquarkProductCopyMapper.updateProductONSALE(productId);
                    if (i==0){
                        throw  new BizException(GlobalErrorCode.valueOf("更新促销商品XquarkProduct表上架状态失败:"+productId));
                    }
                }
            }
        }else {
            List<FreshmanProduct> freshmanProducts = freshmanMapper.queryProductByid(productId.toString());
            XquarkProductWithBLOBs xquarkProductWithBLOBs1 = xquarkProductCopyMapper.selectByPrimaryKey(productId);
            for (int i7 = 0; i7 < freshmanProducts.size(); i7++) {
                if(freshmanProducts.get(i7).getStatus()==1 && xquarkProductWithBLOBs1.getAmount()>0 && xquarkProductWithBLOBs1.getStatus().equals("INSTOCK")){
                    int i = xquarkProductCopyMapper.updateProductONSALE(productId);
                    if (i==0){
                        throw  new BizException(GlobalErrorCode.valueOf("更新XquarkProduct表上架状态失败:"+productId));
                    }
                }
            }
        }
    }
}