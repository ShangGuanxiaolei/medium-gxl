package cn.hds.hvmall.controller;

import lombok.ToString;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @author wangxinhua
 * @date 2019-04-23
 * @since 1.0
 */
@ToString
public class RainLotteryPromotionForm {

    /**
     * 活动id
     */
    private Long id;

    /**
     * 活动开始时间
     */
    @NotNull
    private Date triggerStartTime;

    /**
     * 活动结束时间
     */
    @NotNull
    private Date triggerEndTime;

	/**
	 * 活动类型
	 */
	private Integer promotionType;


	/**
	 * 活动规则
     */
    private String activityRule;

    /**
     * 活动攻略
     */
    private String activityStrategy;

    /**
     * 活动主图
     */
    @NotBlank
    private String banner;

    /**
     * 分享文案
     */
    private String shareContent;

    public String getShareContent() {
        return shareContent;
    }

    public void setShareContent(String shareContent) {
        this.shareContent = shareContent;
    }

    public Integer getPromotionType() {
		return promotionType;
	}

	public void setPromotionType(Integer promotionType) {
		this.promotionType = promotionType;
	}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getTriggerStartTime() {
        return triggerStartTime;
    }

    public void setTriggerStartTime(Date triggerStartTime) {
        this.triggerStartTime = triggerStartTime;
    }

    public Date getTriggerEndTime() {
        return triggerEndTime;
    }

    public void setTriggerEndTime(Date triggerEndTime) {
        this.triggerEndTime = triggerEndTime;
    }

    public String getActivityRule() {
        return activityRule;
    }

    public void setActivityRule(String activityRule) {
        this.activityRule = activityRule;
    }

    public String getActivityStrategy() {
        return activityStrategy;
    }

    public void setActivityStrategy(String activityStrategy) {
        this.activityStrategy = activityStrategy;
    }

    public String getBanner() {
        return banner;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }
}
