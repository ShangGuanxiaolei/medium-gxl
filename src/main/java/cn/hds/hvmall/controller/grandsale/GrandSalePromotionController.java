package cn.hds.hvmall.controller.grandsale;

import cn.hds.hvmall.controller.ExceptionController;
import cn.hds.hvmall.entity.grandsale.*;
import cn.hds.hvmall.pojo.ResponseObject;
import cn.hds.hvmall.pojo.grandsale.*;
import cn.hds.hvmall.service.grandsale.*;
import cn.hds.hvmall.service.grandsale.vo.GrandSaleModuleSortVO;
import cn.hds.hvmall.service.grandsale.vo.GrandSaleStadiumSortVO;
import cn.hds.hvmall.type.PromotionType;
import cn.hds.hvmall.utils.list.GlobalErrorCode;
import cn.hds.hvmall.utils.list.qiniu.BizException;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.validation.Valid;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/grandSale")
public class GrandSalePromotionController implements ExceptionController {

  private final GrandSalePromotionService grandSalePromotionService;

  private final GrandSaleModuleService grandSaleModuleService;

  private final GrandSaleStadiumService grandSaleStadiumService;

  private final GrandSaleKvService grandSaleKvService;

  private final GrandSaleTimelineService grandSaleTimelineService;

  @Autowired
  public GrandSalePromotionController(GrandSalePromotionService grandSalePromotionService,
                                      GrandSaleModuleService grandSaleModuleService,
                                      GrandSaleStadiumService grandSaleStadiumService,
                                      GrandSaleKvService grandSaleKvService,
                                      GrandSaleTimelineService grandSaleTimelineService) {
    this.grandSalePromotionService = grandSalePromotionService;
    this.grandSaleModuleService = grandSaleModuleService;
    this.grandSaleStadiumService = grandSaleStadiumService;
    this.grandSaleKvService = grandSaleKvService;
    this.grandSaleTimelineService = grandSaleTimelineService;
  }

  /**
   * 创建活动
   * @param grandSalePromotionVO 活动对象
   * @return 创建结果
   */
  @RequestMapping(value = "/insertGrandSalePromotion", method = RequestMethod.POST)
  public ResponseObject insertGrandSalePromotion(@Valid @RequestBody GrandSalePromotionVO grandSalePromotionVO) {
    if (grandSalePromotionService.insert(grandSalePromotionVO) > 0) {
      return new ResponseObject<>(HttpStatus.OK);
    }
    return new ResponseObject<>(HttpStatus.INTERNAL_SERVER_ERROR);
  }

  /**
   * 失效活动
   * @param id 大促活动id
   * @return 更新结果
   */
  @RequestMapping(value = "/updateGrandSalePromotionStatus", method = RequestMethod.GET)
  public ResponseObject updateGrandSalePromotionStatus(@RequestParam Integer id) {
    if (grandSalePromotionService.updateStatusById(id) > 0) {
      return new ResponseObject<>(HttpStatus.OK);
    }
    return new ResponseObject<>(HttpStatus.INTERNAL_SERVER_ERROR);
  }

  /**
   * 删除活动
   */
  @RequestMapping(value = "/deleteGrandSale", method = RequestMethod.GET)
  public ResponseObject deleteGrandSale(@RequestParam Integer id) {
    if (grandSalePromotionService.deleteGrandSaleById(id) > 0) {
      return new ResponseObject<>(HttpStatus.OK);
    }
    return new ResponseObject<>(HttpStatus.INTERNAL_SERVER_ERROR);
  }

  /**
   * 查询所有活动
   * @param pageable 分页
   * @return Map
   */
  @RequestMapping(value = "/selectGrandSalePromotion", method = RequestMethod.GET)
  public ResponseObject<Map> selectGrandSalePromotion(Pageable pageable) {
    Map<String, Object> map = new HashMap<>();
    List<GrandSalePromotion> grandSalePromotions = grandSalePromotionService.selectAll(pageable);
    map.put("grandSalePromotions", grandSalePromotions);
    map.put("count", grandSalePromotionService.selectAll(null).size());
    return new ResponseObject<>(map);
  }

  /**
   * 查询大促活动模块
   * @param grandSaleId 大促活动id
   * @return 活动模块数据
   */
  @RequestMapping(value = "/selectGrandSaleModule", method = RequestMethod.GET)
  public ResponseObject<Map> selectGrandSaleModule(@RequestParam Integer grandSaleId) {
    return new ResponseObject<>(grandSaleModuleService.selectAll(grandSaleId));
  }

  /**
   * 添加活动形式
   * @param grandSaleModuleVO 活动形式模块
   * @return 添加结果
   */
  @RequestMapping(value = "/insertGrandSaleModule", method = RequestMethod.POST)
  public ResponseObject insertGrandSaleModule(@Valid @RequestBody GrandSaleModuleVO grandSaleModuleVO) {
    if (grandSaleModuleService.insert(grandSaleModuleVO) > 0) {
      return new ResponseObject<>(HttpStatus.OK);
    }
    return new ResponseObject<>(HttpStatus.INTERNAL_SERVER_ERROR);
  }


  /**
   * 新增主会场分享文案
   * @param grandSaleMainShareVOs
   * @return
   */
  @RequestMapping(value = "/insertGrandSaleShare", method = RequestMethod.POST)
  public ResponseObject insertGrandSaleShare(@Valid @RequestBody GrandSaleMainShareVO[] grandSaleMainShareVOs) {
    if (CollectionUtils.isEmpty(Arrays.asList(grandSaleMainShareVOs)))
      return new ResponseObject<>(HttpStatus.INTERNAL_SERVER_ERROR);
    int result = grandSaleStadiumService.insertGrandSaleShare(grandSaleMainShareVOs);
    if (result == 1)
      return new ResponseObject<>(HttpStatus.OK);
    else
      return new ResponseObject<>(HttpStatus.EXPECTATION_FAILED);
  }

  /**
   * 获取所有主会场文案
   * @param pageable
   * @return
   */
  @RequestMapping(value = "/selectGrandSaleMainShare", method = RequestMethod.GET)
  public ResponseObject<Map<String, Object>> selectGrandSaleMainShare(Pageable pageable) {
    return new ResponseObject<>(grandSaleStadiumService.selectAllMainShare(pageable));
  }

  /**
   * 更新主会场分享文案. 不支持修改起始时间
   * @param grandSaleMainShareVO
   * @return
   */
  @RequestMapping(value = "/updateGrandSaleMainShare", method = RequestMethod.POST)
  public ResponseObject updateGrandSaleMainShare(
          @Valid @RequestBody GrandSaleMainShareVO grandSaleMainShareVO) {
    int result = grandSaleStadiumService.updateGrandSaleMainShare(grandSaleMainShareVO);
    if (result > 0)
      return new ResponseObject<>(HttpStatus.OK);
    else
      return new ResponseObject<>(HttpStatus.EXPECTATION_FAILED);
  }

  /**
   * 获取最晚主会场分享文案时间
   * @return
   */
  @RequestMapping(value = "/selectLastMainShare", method = RequestMethod.GET)
  public ResponseObject selectLastMainShare() {
    return new ResponseObject<>(grandSaleStadiumService.selectLastMainShare());
  }

  /**
   * 更新活动模块状态
   * @param promotionId 活动id
   * @param status 活动状态
   * @return 更新接口
   */
  @RequestMapping(value = "/updateGrandSaleModuleStatus", method = RequestMethod.GET)
  public ResponseObject updateGrandSaleModuleStatus(@RequestParam String promotionId,
                                                    @RequestParam Integer status,
                                                    @RequestParam PromotionType promotionType) {
    if (grandSaleModuleService.updateStatusById(promotionId, status, promotionType)) {
      return new ResponseObject<>(HttpStatus.OK);
    }
    return new ResponseObject<>(HttpStatus.INTERNAL_SERVER_ERROR);
  }

  /**
   * 保存活动模块排序
   * @param grandSaleModuleSortVO 排序VO
   * @return 更新结果
   */
  @RequestMapping(value = "/updateGrandSaleModuleSort", method = RequestMethod.POST)
  public ResponseObject updateGrandSaleModuleSort(@Valid @RequestBody GrandSaleModuleSortVO grandSaleModuleSortVO) {
    if (grandSaleModuleService.updateSortByType(grandSaleModuleSortVO)) {
      return new ResponseObject<>(HttpStatus.OK);
    }
    return new ResponseObject<>(HttpStatus.INTERNAL_SERVER_ERROR);
  }

  /**
   * 添加分会场
   * @param grandSaleStadiumVO 分会场对象
   * @return 添加结果
   */
  @RequestMapping(value = "/insertGrandSaleStadium", method = RequestMethod.POST)
  public ResponseObject insertGrandSaleStadium(@Valid @RequestBody GrandSaleStadiumVO grandSaleStadiumVO) {
    if (grandSaleStadiumService.insert(grandSaleStadiumVO) > 0) {
      return new ResponseObject<>(HttpStatus.OK);
    }
    return new ResponseObject<>(HttpStatus.INTERNAL_SERVER_ERROR);
  }

  /**
   * 查询分会场列表
   * @param grandSaleId 大促活动id
   * @return 分会场对象
   */
  @RequestMapping(value = "/selectGrandSaleStadium", method = RequestMethod.GET)
  public ResponseObject<List<GrandSaleStadium>> selectGrandSaleStadium(@RequestParam Integer grandSaleId) {
    List<GrandSaleStadium> grandSaleStadiums = grandSaleStadiumService.selectAllByGrandSaleId(grandSaleId);
    if (grandSaleStadiums.isEmpty()) {
      return new ResponseObject<>();
    }
    return new ResponseObject<>(grandSaleStadiums);
  }

  /**
   * 查询分会场配置
   * @param stadiumId 会场id
   * @return 会场对象
   */
  @RequestMapping(value = "/selectStadium", method = RequestMethod.GET)
  public ResponseObject<GrandSaleStadiumVOMap> selectStadium(@RequestParam Integer stadiumId,Pageable pageable,
                                                             @RequestParam(defaultValue = "true") boolean paging) {
    pageable = paging?pageable:null;
    GrandSaleStadiumVOMap grandSaleStadiumVOMap = grandSaleStadiumService.selectOne(stadiumId, pageable);
    if (grandSaleStadiumVOMap == null) {
      return new ResponseObject<>();
    }
    return new ResponseObject<>(grandSaleStadiumVOMap);
  }

  /**
   * 删除分会场
   * @param stadiumId 分会场id
   * @return 删除结果
   */
  @RequestMapping(value = "/deleteStadium", method = RequestMethod.GET)
  public ResponseObject<HttpStatus> deleteStadium(@RequestParam Integer stadiumId) {
    if (grandSaleStadiumService.deleteByPrimaryKey(stadiumId) > 0) {
      return new ResponseObject<>(HttpStatus.OK);
    }
    return new ResponseObject<>(HttpStatus.INTERNAL_SERVER_ERROR);
  }

  /**
   * 更新分会场信息接口
   * @param grandSaleStadiumVO 分会场对象
   * @return 更新结果
   */
  @RequestMapping(value = "/updateGrandSaleStadium", method = RequestMethod.POST)
  public ResponseObject updateGrandSaleStadium(@Valid @RequestBody GrandSaleStadiumVOId grandSaleStadiumVO) {
//    if (grandSaleStadiumService.update(grandSaleStadiumVO) > 0) {
      grandSaleStadiumService.updateGrandSaleStadium(grandSaleStadiumVO);
      return new ResponseObject<>(HttpStatus.OK);
//    }
//    return new ResponseObject<>(HttpStatus.INTERNAL_SERVER_ERROR);
  }

  /**
   * 保存分会场排序
   * @param grandSaleStadiumSortVO 分会场排序对象
   * @return 更新结果
   */
  @RequestMapping(value = "/updateGrandSaleStadiumSort", method = RequestMethod.POST)
  public ResponseObject updateGrandSaleStadiumSort(@Valid @RequestBody GrandSaleStadiumSortVO grandSaleStadiumSortVO) {
    if (grandSaleStadiumService.updateSortById(grandSaleStadiumSortVO)) {
      return new ResponseObject<>(HttpStatus.OK);
    }
    return new ResponseObject<>(HttpStatus.INTERNAL_SERVER_ERROR);
  }

  /**
   * 添加主KV
   * @param grandSaleKvVO 主KV对象
   * @return 添加结果
   */
  @RequestMapping(value = "/insertGrandSaleKv", method = RequestMethod.POST)
  public ResponseObject insertGrandSaleKv(@Valid @RequestBody GrandSaleKvVO grandSaleKvVO) {
    if (grandSaleKvService.insert(grandSaleKvVO) > 0) {
      return new ResponseObject<>(HttpStatus.OK);
    }
    return new ResponseObject<>(HttpStatus.INTERNAL_SERVER_ERROR);
  }

  /**
   * 获取主KV列表
   * @param grandSaleId 大促活动id
   * @param pageable 分页
   * @return Map
   */
  @RequestMapping(value = "/selectGrandSaleKv", method = RequestMethod.GET)
  public ResponseObject<Map> selectGrandSaleKv(@RequestParam Integer grandSaleId, Pageable pageable) {
    Map<String, Object> map = new HashMap<>();
    List<GrandSaleKv> grandSaleKvs = grandSaleKvService.selectAllByGrandSaleId(grandSaleId, pageable);
    map.put("grandSaleKvs", grandSaleKvs);
    map.put("count", grandSaleKvService.countByGrandSaleId(grandSaleId));
    return new ResponseObject<>(map);
  }

  /**
   * 删除主KV
   * @param id 主键
   * @return 删除结果
   */
  @RequestMapping(value = "/deleteGrandSaleKv", method = RequestMethod.GET)
  public ResponseObject deleteGrandSaleKv(@RequestParam Integer id) {
    if (grandSaleKvService.logicalDelete(id) > 0) {
      return new ResponseObject<>(HttpStatus.OK);
    }
    return new ResponseObject<>(HttpStatus.INTERNAL_SERVER_ERROR);
  }

  /**
   * 添加时间轴节点
   * @param grandSaleTimelineConfig 时间轴对象
   * @return 添加结果
   */
  @RequestMapping(value = "/insertGrandSaleTimeline", method = RequestMethod.POST)
  public ResponseObject insertGrandSaleTimeline(@Valid @RequestBody GrandSaleTimelineConfig grandSaleTimelineConfig) {
    if (grandSaleTimelineService.insert(grandSaleTimelineConfig)) {
      return new ResponseObject<>(HttpStatus.OK);
    }
    return new ResponseObject<>(HttpStatus.INTERNAL_SERVER_ERROR);
  }

  /**
   * 获取时间轴列表
   * @param grandSaleId 大促活动id
   * @param pageable 分页
   * @return Map
   */
  @RequestMapping(value = "/selectGrandSaleTimeline", method = RequestMethod.GET)
  public ResponseObject<Map> selectGrandSaleTimeline(@RequestParam Integer grandSaleId, Pageable pageable) {
    Map<String, Object> map = new HashMap<>();
    GrandSaleTimelineConfig grandSaleTimeline = grandSaleTimelineService.selectAllByGrandSaleId(grandSaleId, pageable);
    map.put("grandSaleTimelineList", grandSaleTimeline);
    map.put("count", grandSaleTimelineService.selectAllByGrandSaleId(grandSaleId, null).getGrandSaleTimelineList().size());
    return new ResponseObject<>(map);
  }

  /**
   * 删除时间轴
   * @param id 主键
   * @return 删除结果
   */
  @RequestMapping(value = "/deleteGrandSaleTimeline", method = RequestMethod.GET)
  public ResponseObject deleteGrandSaleTimeline(@RequestParam Integer id) {
    if (grandSaleTimelineService.logicalDelete(id) > 0) {
      return new ResponseObject<>(HttpStatus.OK);
    }
    return new ResponseObject<>(HttpStatus.INTERNAL_SERVER_ERROR);
  }

  /**
   * 根据活动类型查询活动
   * @param promotionType 活动类型
   * @return 活动列表
   */
  @RequestMapping(value = "/selectPromotion", method = RequestMethod.GET)
  public ResponseObject<List<PromotionVO>> selectPromotion(@RequestParam PromotionType promotionType) {
    List<PromotionVO> promotionVOS = grandSaleModuleService.selectPromotion(promotionType);
    if (promotionVOS.isEmpty()) {
      return new ResponseObject<>();
    }
    return new ResponseObject<>(promotionVOS);
  }

  /**
   * 获取所有活动类型
   * @return 活动类型列表
   */
  @RequestMapping(value = "/selectPromotionType", method = RequestMethod.GET)
  public ResponseObject<Map> selectPromotionType() {
    Map map = grandSalePromotionService.selectPromotionType();
    if (map.isEmpty()) {
      return new ResponseObject<>();
    }
    return new ResponseObject<>(map);
  }

  /**
   * 获取所有的分会场类型
   * @return 分会场类型列表
   */
  @RequestMapping(value = "/selectStadiumType", method = RequestMethod.GET)
  public ResponseObject<Map> selectStadiumType() {
    Map map = grandSalePromotionService.selectStadiumType();
    if (map.isEmpty()) {
      return new ResponseObject<>();
    }
    return new ResponseObject<>(map);
  }

  @RequestMapping(value = "importStadiumProduct",method = RequestMethod.POST)
  public ResponseObject<String> importGrandSaleStadiumProduct(
          @RequestParam(value = "file") MultipartFile file,
          @RequestParam(value = "grandId") Integer grandId) {
      InputStream is;

      try {
          is = file.getInputStream();
          grandSaleStadiumService.importData(is,grandId);
      } catch (IOException | InvalidFormatException e) {
          throw BizException.build(GlobalErrorCode.INTERNAL_ERROR,"导入失败,错误信息为:" + e.getMessage()).get();
      }

      return new ResponseObject<>("导入成功",GlobalErrorCode.SUCESS);
  }

  @Override
  public Logger getLogger() {
    return log;
  }
}
