package cn.hds.hvmall.controller.freshman;

import cn.hds.hvmall.base.PageBase;
import cn.hds.hvmall.entity.freshman.Banner;
import cn.hds.hvmall.entity.freshman.BannerInfo;
import cn.hds.hvmall.entity.freshman.BannerProduct;
import cn.hds.hvmall.entity.freshman.ProductType;
import cn.hds.hvmall.pojo.list.ExecuteResult;
import cn.hds.hvmall.service.freshman.FreshmanBannerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/banner")
public class FreshmanBannerController {

    @Autowired
    private FreshmanBannerService freshmanBannerService;



    /**
     * 添加Banner
     * @param json
     * @return
     */
    @RequestMapping(value = "/insertBanner",method = RequestMethod.POST)
    @ResponseBody
    public ExecuteResult<Integer> insertBanner(String json) {
        return freshmanBannerService.insertBanner(json);
    }

    /**
     * 根据条件查询商品信息
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/selectProductInfo",method = RequestMethod.POST)
    public ExecuteResult<PageBase<Banner>> selectProductInfo(@RequestBody BannerProduct json) {
        return freshmanBannerService.selectProductInfo(json);
    }


    /**
     * 查询类型
     * @param parentId
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/selectType",method = RequestMethod.GET)
    public ExecuteResult<List<ProductType>> selectType(@RequestParam(required = false) Integer parentId) {
        return freshmanBannerService.selectType(parentId);
    }

    /**
     * 查询品牌
     */
    @ResponseBody
    @RequestMapping(value = "/selectBrand",method = RequestMethod.GET)
    public ExecuteResult<List<ProductType>> selectBrand() {
        return freshmanBannerService.selectBrand();
    }


    /**
     * 查询Banner信息
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/selectBannerInfo" ,method = RequestMethod.GET)
    public ExecuteResult<List<BannerInfo>> selectBannerInfo() {
    return freshmanBannerService.selectBannerInfo();
    }


    /**
     * 修改Banner信息
     * @param bannerInfo
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/updateBannerInfo",method = RequestMethod.POST)
    public ExecuteResult<Boolean> updateBannerInfo(@RequestBody BannerInfo bannerInfo) {
        return freshmanBannerService.updateBannerInfo(bannerInfo);
    }


}

