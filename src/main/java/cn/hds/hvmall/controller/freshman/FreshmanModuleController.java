package cn.hds.hvmall.controller.freshman;

import cn.hds.hvmall.base.BaseExecuteResult;
import cn.hds.hvmall.entity.freshman.FreshmanModule;
import cn.hds.hvmall.pojo.activity.PtDetail;
import cn.hds.hvmall.pojo.list.ExecuteResult;
import cn.hds.hvmall.service.freshman.FreshmanModuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/freshman")
@CrossOrigin(allowCredentials="true",maxAge = 3600)
public class FreshmanModuleController {

    @Autowired
    private FreshmanModuleService freshmanModuleService;

    /**
     * 首页查询
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/selectFreshmanModule",method = RequestMethod.GET)
    public ExecuteResult<List<FreshmanModule>> selectFreshmanModule(){
        ExecuteResult<List<FreshmanModule>> result = new ExecuteResult<>();
        List<FreshmanModule> freshmanModuleList=freshmanModuleService.selectFreshmanModule();
        result.setResult(freshmanModuleList);
        result.setCount(freshmanModuleList.size());
        return result;
    }


    /**
     * 禁用、启用按钮
     * @param id
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/updateStatus",method = RequestMethod.POST)
    public ExecuteResult<Boolean> updateStatus(String json){
        ExecuteResult<Boolean> result = new ExecuteResult<>();
        boolean a=freshmanModuleService.updateStatus(json);
        result.setResult(a);
        return result;
    }

    /**
     * 当前类型下的问题排序
     * @param json
     * @return
     */
    @RequestMapping(value = "/sortNum",method = RequestMethod.POST)
    @ResponseBody
    public BaseExecuteResult<?> sort(@RequestParam("json") String json) {
        BaseExecuteResult<?> result = freshmanModuleService.sort(json);
        return result;
    }



}
