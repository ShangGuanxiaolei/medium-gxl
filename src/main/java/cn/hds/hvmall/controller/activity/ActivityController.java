package cn.hds.hvmall.controller.activity;

import cn.hds.hvmall.pojo.activity.ActivityInfo;
import cn.hds.hvmall.pojo.activity.ActivityVO;
import cn.hds.hvmall.pojo.list.ExecuteResult;
import cn.hds.hvmall.service.activity.ActivityService;
import cn.hds.hvmall.utils.JSONUtil;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: zzl
 * @Date: 2018/9/19 10:31
 * @Version
 */
@RestController
@ResponseBody
@RequestMapping("/activity")
public class ActivityController {

  @Autowired
  private ActivityService activityService;

  public ExecuteResult<List<ActivityVO>> searchActivity(@RequestBody String  json){
    ActivityInfo activityInfo = JSONUtil.toBean(json, ActivityInfo.class);
    ExecuteResult<List<ActivityVO>> result = activityService.searchActivity(activityInfo,6);
    return result;

  }

}
