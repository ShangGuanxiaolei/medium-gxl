package cn.hds.hvmall.controller.activity;

import cn.hds.hvmall.pojo.activity.ActivityInfo;
import cn.hds.hvmall.pojo.activity.ActivityVO;
import cn.hds.hvmall.pojo.activity.PtDetail;
import cn.hds.hvmall.pojo.list.ExecuteResult;
import cn.hds.hvmall.service.activity.ActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ActivityPtController {
  private final ActivityService activityService;

  @Autowired
  public ActivityPtController(ActivityService activityService) {
    this.activityService = activityService;
  }

  //监控活动
  @RequestMapping("/ptActivity")
  public ExecuteResult<List<ActivityVO>> searchActivity(@RequestParam("json") String json) {
    ActivityInfo activityInfo = cn.hds.hvmall.utils.JSONUtil.toBean(json, ActivityInfo.class);
    return activityService.searchPtActivity(activityInfo, 20);
  }

  @RequestMapping("/PtDetail/{ptCode}")
  public ExecuteResult<List<PtDetail>> checkPtDetail(@PathVariable(value = "ptCode") String ptCode) {
    return activityService.searchPtOrder(ptCode);
  }
}
