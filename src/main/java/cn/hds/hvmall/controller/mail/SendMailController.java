package cn.hds.hvmall.controller.mail;

import cn.hds.hvmall.service.mail.SendMailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class SendMailController {

    @Autowired
    private SendMailService sendMailService;


    @Scheduled(cron = "0 0 10 * * ?")
    public void vipAndFreshmanSalesCount(){
        sendMailService.vipAndFreshmanSalesCount();
    }


}
