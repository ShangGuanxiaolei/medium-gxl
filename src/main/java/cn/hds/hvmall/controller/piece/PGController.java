package cn.hds.hvmall.controller.piece;

import cn.hds.hvmall.base.BaseExecuteResult;
import cn.hds.hvmall.entity.list.ProductAmountInfo;
import cn.hds.hvmall.entity.piece.*;
import cn.hds.hvmall.pojo.ResponseObject;
import cn.hds.hvmall.pojo.activity.ActivityVO;
import cn.hds.hvmall.pojo.piece.BackPGThreeVO;
import cn.hds.hvmall.pojo.piece.PGVO;
import cn.hds.hvmall.service.piece.PGService;
import cn.hds.hvmall.service.piece.PromotionCountService;
import cn.hds.hvmall.service.piece.PromotionTranInfoService;
import cn.hds.hvmall.utils.JSONUtil;
import cn.hds.hvmall.utils.list.GlobalErrorCode;
import cn.hds.hvmall.utils.list.qiniu.BizException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@Api("后台拼团活动接口")
@RequestMapping("/piece/group")
public class PGController {
  private final PGService pgService;
  private final PromotionCountService promotionCountService;
  private final PromotionTranInfoService promotionTranInfoService;

  @Autowired
  public PGController(PGService pgService, PromotionCountService promotionCountService, PromotionTranInfoService promotionTranInfoService) {
    this.pgService = pgService;
    this.promotionCountService = promotionCountService;
    this.promotionTranInfoService = promotionTranInfoService;
  }

  /**
   * 创建拼团活动
   */
  @PostMapping("/create")
  @ApiOperation(value = "创建拼团，返回pCode？", notes = "创建拼团活动")
  public ResponseObject create(@RequestBody PGVO data) {
    if (data == null) {
      throw new BizException(GlobalErrorCode.INTERNAL_ERROR, "参数为空");
    }
    //进入service层,人工预锁定库存
    pgService.create(data);
    return new ResponseObject();
  }

  /**
   * @param selectConditions selectConditions
   * @return BackPGThreeVO
   */
  @ApiOperation(value = "根据条件后台查询拼团活动列表", notes = "入参：查询条件  返回:活动视图实体列表 例：'selectConditions':{\n" +
      "\t\"pgSkuId\":\"qweasd00120asdasd\",\n" +
      "\t\"pgSkuName\":\"牙刷\",\n" +
      "\t\"pStatus\":\"4\"\n" +
      "}")
  @RequestMapping(value = "/queryBackPGVOList", method = RequestMethod.POST)
  public BaseExecuteResult<List<BackPGThreeVO>> queryBackPGVOList(@RequestBody String selectConditions) {
    Map<String, Object> stringObjectMap = JSONUtil.toMap(selectConditions);
    return pgService.queryBackPGVOList(stringObjectMap);
  }

  /**
   * 后台更新活动状态（发布）
   *
   * @param pCode pCode
   * @return Object
   */
  @RequestMapping(value = "/updatePGStatus/{pCode}", method = RequestMethod.GET)
  public BaseExecuteResult<Object> updatePGStatus(@PathVariable("pCode") String pCode) {
    return pgService.publishPieceGroup(pCode, "5");
  }

  /**
   * 后台失效
   */
  @RequestMapping(value = "/updateStatus/{pCode}", method = RequestMethod.GET)
  public boolean updateStatus(@PathVariable("pCode") String pCode) {
    return pgService.updateActivityExclusiveStatus(pCode);
  }

  /**
   * zzl
   * 根据p_code查要修改的活动信息
   * 依据p_code查询旧数据
   *
   * @return GroupBaseInfoVO
   */
  @RequestMapping("/edit/select")
  public BaseExecuteResult<List<GroupBaseInfoVO>> select(@RequestParam("pCode") @RequestBody String pCode) {
    //根据p_code查询拼团活动信息
    return pgService.select(pCode);
  }

  @RequestMapping("/edit/getInfo")
  public BaseExecuteResult<PGVO> getInfo(@RequestParam("pCode") @RequestBody String pCode) {
    //根据p_code查询拼团活动信息
    BaseExecuteResult<PGVO> result = new BaseExecuteResult<>();
    PGVO pgvo = pgService.getInfo(pCode);
    result.setIsSuccess(1);
    result.setResult(pgvo);
    return result;
  }

  @RequestMapping(value = "/edit/updateInfo", method = RequestMethod.POST)
  public BaseExecuteResult<?> getInfo(@RequestBody PGVO pgvo) {
    //根据p_code查询拼团活动信息
    BaseExecuteResult<String> result = new BaseExecuteResult<>();
    String res = pgService.updateInfo(pgvo);
    result.setIsSuccess(1);
    result.setResult(res);
    return result;
  }


  /**
   * @param json json
   * @return Object
   */
  @RequestMapping("/edit/update")
  public BaseExecuteResult<Object> update(@RequestBody String json) {
    EditGroupBaseInfoVO info = JSONUtil.toBean(json, EditGroupBaseInfoVO.class);
    return pgService.update(info);
  }

  @RequestMapping("/PromotionCount")
  public PromotionCount selectPieceCount(String pCode) {
    PromotionCount promotionCount = new PromotionCount();
    promotionCount.setPieceSuccessCount(promotionCountService.selectPieceSuccessCount(pCode));
    promotionCount.setPieceSuccessOrderCount(promotionCountService.selectPieceSuccessOrderCount(pCode));
    promotionCount.setPieceFailCount(promotionCountService.selectPieceFailCount(pCode));
    promotionCount.setPieceFailOrderCount(promotionCountService.selectPieceFailOrderCount(pCode));
    promotionCount.setPieceSuccessList(promotionCountService.selectPieceSuccessList(pCode));
    promotionCount.setPieceSuccessOrderCountList(promotionCountService.selectPieceSuccessOrderList(pCode));
    return promotionCount;
  }

  @RequestMapping("/selectTranInfo")
  public PromotionTranInfo selectTranInfo(String pCode) {
    return promotionTranInfoService.selectTranInfo(pCode);
  }

  @RequestMapping("/ptActivity")
  public BaseExecuteResult<List<ActivityVO>> searchActivity(@RequestParam("json") String json) {
    return promotionTranInfoService.selectTranInfoByOrderOrCpId(json);
  }

  /**
   * 查询库存数据回显
   *
   * @param pCode pCode
   * @return BaseExecuteResult
   */
  @RequestMapping(value = "/amount/{pCode}", method = RequestMethod.POST)
  public BaseExecuteResult<List<GroupAmount>> getAmount(@PathVariable("pCode") String pCode) {
    BaseExecuteResult<List<GroupAmount>> baseExecuteResult = new BaseExecuteResult<>();
    List<GroupAmount> groupAmount = pgService.getGroupAmount(pCode);
    baseExecuteResult.setResult(groupAmount);
    return baseExecuteResult;
  }

  /**
   * 修改库存
   */
  @RequestMapping(value = "updateAmount", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
  public BaseExecuteResult<Boolean> updateAmount(@RequestBody String json) {
    List<ProductAmountInfo> productAmountInfos = JSONUtil.toList(json, ProductAmountInfo.class);
    BaseExecuteResult<Boolean> baseExecuteResult = new BaseExecuteResult<>();
    boolean b = pgService.updateAmount(productAmountInfos);
    if (b) {
      baseExecuteResult.setResult(true);
    } else {
      baseExecuteResult.setResult(false);
    }
    return baseExecuteResult;
  }
  /**
   * 删除拼团信息
   */
  @RequestMapping(value = "/deleteActivity",method = RequestMethod.GET)
  public ResponseObject<Boolean> deletedActivity (@Param("pCode")String pCode){
    boolean b = pgService.deleteActivity(pCode);
    return new ResponseObject<>(b);
  }
}
