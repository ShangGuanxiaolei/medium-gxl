package cn.hds.hvmall.controller.proxy;
import cn.hds.hvmall.utils.HttpClientUtils;
import cn.hds.hvmall.utils.list.GlobalErrorCode;
import cn.hds.hvmall.utils.list.qiniu.BizException;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;
import java.util.Map;
/**
 * @ClassName: ProxyHwController
 * @Description: 代理请求 hwsc.handeson api
 * @Author: Kwonghom
 * @CreateDate: 2019/5/6 11:44
 * @Version: 1.0
 **/
@RestController
@RequestMapping("/proxy")
public class ProxyHwController {

    private static final String METHOD_GET = "GET";
    private static final String METHOD_POST = "POST";

    public @RequestMapping(value = "/hvmall", method = RequestMethod.POST)
    JSONObject proxyHvmallApi(@RequestBody Map map) {
        final String url = (String) map.get("url");
        final String method = (String) map.get("method");
        final String params = (String) map.get("params");
        final String body = JSON.toJSONString(map.get("body"));

        if (StringUtils.isBlank(url) || StringUtils.isEmpty(method)) {
            throw new BizException(GlobalErrorCode.INVALID_ARGUMENT, "URL 和请求方式不能为 NULL");
        }
        if (METHOD_GET.equals(method)) {
            if (StringUtils.isNotBlank(params) && !"".equals(params)) {
                return HttpClientUtils.httpGet(url + params);
            }
            return HttpClientUtils.httpGet(url);
        } else if (METHOD_POST.equals(method)) {
            if (StringUtils.isNotBlank(params) && !"".equals(params)) {
                return HttpClientUtils.httpPost(url + params, JSONObject.parseObject(body));
            }
            return HttpClientUtils.httpPost(url, JSONObject.parseObject(body));
        }
        return null;
    }
}
