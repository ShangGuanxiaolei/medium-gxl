package cn.hds.hvmall.controller.order;

import cn.hds.hvmall.controller.ExceptionController;
import cn.hds.hvmall.pojo.ResponseObject;
import cn.hds.hvmall.service.order.OrderAnalyseService;
import cn.hds.hvmall.service.order.OrderAnalyseVO;
import cn.hds.hvmall.utils.list.GlobalErrorCode;
import cn.hds.hvmall.utils.list.qiniu.BizException;
import io.vavr.control.Try;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author wangxinhua
 * @date 2019-05-12
 * @since 1.0
 */
@RestController
@RequestMapping("/order/analyse")
@Slf4j
public class OrderAnalyseController implements ExceptionController {

    private final OrderAnalyseService orderAnalyseService;

    @Autowired
    public OrderAnalyseController(OrderAnalyseService orderAnalyseService) {
        this.orderAnalyseService = orderAnalyseService;
    }

    @RequestMapping(value = "/{type}",method = RequestMethod.GET)
    public ResponseObject<List<OrderAnalyseVO>> type(@RequestParam String start,
                                                    @RequestParam String end,
                                                     @PathVariable String type
        ) {

        final LocalDateTime startDate = Try.of(() -> LocalDateTime.parse(start + "T00:00"))
                .getOrElseThrow(() -> new BizException(GlobalErrorCode.INVALID_ARGUMENT, "开始时间格式不正确"));
        // 设置结束时间为当天的 23:59:59.999 避免漏掉数据
        final LocalDateTime endDate = Try.of(() -> LocalDateTime.parse(end +"T23:59:59.999"))
                .getOrElseThrow(() -> new BizException(GlobalErrorCode.INVALID_ARGUMENT, "结束时间格式不正确"));

        return new ResponseObject<>(orderAnalyseService.analyseByType(startDate,endDate,type.toUpperCase()));
    }


    @Override
    public Logger getLogger() {
        return log;
    }

    public static void main(String[] args) {
        System.out.println(LocalDateTime.parse("2019-05-10T00:00:00.000"));
    }
}
