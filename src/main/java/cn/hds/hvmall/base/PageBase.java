package cn.hds.hvmall.base;

import java.io.Serializable;
import java.util.List;

/**
 * 分页用
 * 
 * @类名: PageBase .
 * @描述: TODO .
 * @程序猿: chenjingwu .
 * @日期: 2017年3月3日 上午10:20:23 .
 * @版本号: V1.0 .
 */
public class PageBase<T> implements Serializable {

	/**
	 * @字段：serialVersionUID @功能描述：
	 * @创建人：chenjingwu
	 * @创建时间：2016年3月23日上午10:31:10
	 */

	private static final long serialVersionUID = 1L;
	private int pageSize; // 每页显示的数据量
	private int pageNum; // 当前页码
	private int offSet; // 起始行号
	private int totalPages; // 总页数
	private int totalCount; // 总记录数
	private List<T> resultList;

	public PageBase(int pageSize, int totalCount, int pageNum) {
		this.setPageSize(pageSize);
		this.setTotalCount(totalCount);
		this.setPageNum(pageNum);
	}

	/**
	 * 获取每页显示的数据量
	 * 
	 * @return
	 */
	public int getPageSize() {
		return pageSize;
	}

	/**
	 * 设置每页显示的数据量
	 * 
	 * @param pageSize
	 */
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	/**
	 * 获取当前页码
	 * 
	 * @return
	 */
	public int getPageNum() {
		return pageNum;
	}

	/**
	 * 设置当前页码
	 * 
	 * @param pageNum
	 */
	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
		if (pageNum < 1) {
			this.pageNum = 1;
		} else if (pageNum > getTotalPages()) {
			this.pageNum = getTotalPages();
		}
		// 根据当前页码自动计算起始行号
		setOffSet((this.pageNum - 1) * getPageSize());
	}

	/**
	 * 获取起始行号
	 * 
	 * @return
	 */
	public int getOffSet() {
		return offSet;
	}

	/**
	 * 设置起始行号
	 * 
	 * @param offSet
	 */
	public void setOffSet(int offSet) {
		this.offSet = offSet;
	}

	/**
	 * 获取总页数
	 * 
	 * @return
	 */
	public int getTotalPages() {
		return totalPages;
	}

	/**
	 * 设置总页数
	 * 
	 * @param totalPages
	 */
	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}

	/**
	 * 获取总记录数
	 * 
	 * @return
	 */
	public int getTotalCount() {
		return totalCount;
	}

	/**
	 * 设置总记录数
	 * 
	 * @param totalCount
	 */
	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
		// 根据总记录数自动计算总页数
		int result = totalCount / getPageSize();
		if (totalCount % pageSize != 0) {
			result += 1;
		}
		setTotalPages(result);
	}

	/**
	 * 获取当前页的数据集
	 * 
	 * @return
	 */
	public List<T> getResultList() {
		return resultList;
	}
	/**
	 * 设置当前页的数据集
	 * 
	 * @param result
	 */
	public void setResultList(List<T> resultList) {
		this.resultList = resultList;
	}

}
